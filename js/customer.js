var customerlistDataTable;
$(document).ready(function () {
    customerlistDataTable = $('#customerlisttable').DataTable({
        'bFilter': false,
        'bLengthChange': false,
        'pageLength': 50,
        'processing': true,
        'serverSide': true,
        'bSort': false,
        'serverMethod': 'post',
        'ajax': {
            'url': _base_url + 'customer/list_ajax_customer_list',
            'data': function (data) {
                if ($('#cust_from_date').length) {
                    var regdate = $('#cust_from_date').val();
                    data.regdate = regdate;
                } else {
                    data.regdate = '';
                }
                if ($('#cust_to_date').length) {
                    var regdateto = $('#cust_to_date').val();
                    data.regdateto = regdateto;
                } else {
                    data.regdateto = '';
                }
                data.paytype = $('#payment_type').val();
                data.sourceval = $('#sort_source').val();
                data.customertype = $('#sort_cust_type').val();
                data.useractive = $('#all-customers').val();
                data.keywordsearch = $('#keyword-search').val();
            },
            "complete": function (json, type) {
                var _resp = $.parseJSON(json.responseText);
            }
        },
        'columns': [
            { data: 'slno' },
            { data: 'image' },
            { data: 'name' },
            { data: 'mobile' },
            { data: 'perhr' },
            { data: 'area' },
            { data: 'address' },
            { data: 'source' },
            { data: 'lastjob' },
            { data: 'addeddate' },
            { data: 'action' },
        ],
        "columnDefs": [{
            "targets": 1,
            "visible": false
        }]
    });

    $('#cust_from_date,#cust_to_date').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    });

    $('#cust_from_date').change(function () {
        customerlistDataTable.draw();
    });

    $('#cust_to_date').change(function () {
        customerlistDataTable.draw();
    });

    $('#payment_type').change(function () {
        customerlistDataTable.draw();
    });

    $('#sort_source').change(function () {
        customerlistDataTable.draw();
    });

    $('#sort_cust_type').change(function () {
        customerlistDataTable.draw();
    });

    $('#all-customers').change(function () {
        customerlistDataTable.draw();
    });

    $("#keyword-search").keyup(function () {
        var vall = $('#keyword-search').val().length
        if (vall >= 3) {
            customerlistDataTable.draw();
        }
    });

    $('#customerexcelbtn').click(function () {
        $("#customerexcelbtn").html('<i class="fa fa-spinner fa-spin"></i>');
        var status = $('#all-customers').val();
        var fromdate = $('#cust_from_date').val();
        var todate = $('#cust_to_date').val();
        var payment_type = $('#payment_type').val();
        var source = $('#sort_source').val();
        var sort_cust_type = $('#sort_cust_type').val();
        var search = $('#keyword-search').val();

        $.ajax({
            type: "POST",
            url: _base_url + 'customerexcel/customerexportexcel',
            data: { fromdate: fromdate, todate: todate, status: status, payment_type: payment_type, source: source, sort_cust_type: sort_cust_type, search: search },
            cache: false,
            success: function (response) {
                $("#customerexcelbtn").html('<i class="fa fa-file-excel-o"></i>');
                window.location = response;
            }
        });
    });
});

function delete_customer($this, customer_id, customer_status) {
    var _lblstatus = customer_status == 1 ? 'disable' : 'enable';
    if (confirm('Are you sure you want to ' + _lblstatus + ' this customer')) {
        $.ajax({
            type: "POST",
            url: _base_url + "customer/remove_customer",
            data: { customer_id: customer_id, customer_status: customer_status },
            dataType: "text",
            cache: false,
            success: function (result) {
                //window.location.assign(_base_url + 'customers');
                if (result == 1) {
                    $($this).attr('class', 'btn btn-success btn-small');
                    $($this).html('<i class="btn-icon-only icon-ok"></i>');
                }
                else {
                    if (result == 'exist_bookings') // Edited by Geethu
                    {
                        alert('Warning! Can\'t deactivate this customer, have some active bookings.');
                        result = 0;
                    }
                    else {
                        $($this).attr('class', 'btn btn-danger btn-small');
                        $($this).html('<i class="btn-icon-only icon-remove"> </i>');
                    }

                }
                $($this).attr('onclick', 'delete_customer(this, ' + customer_id + ', ' + result + ')');
            }
        });
    }
}
function showAvatar(name, src) {
    $('.avatar-popup-name').html(name);
    $('#avatar-popup-image').attr('src', src);
    $.fancybox.open({
        autoCenter: true,
        fitToView: false,
        scrolling: false,
        openEffect: 'none',
        openSpeed: 1,
        autoSize: false,
        width: 400,
        height: 'auto',
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.3)'
                },
                closeClick: true
            }
        },
        padding: 0,
        closeBtn: false,
        content: $('#avatar-popup'),
    });
}
function closeFancy() {
    $.fancybox.close();
}
var enable_disable_id = null;
var current_status = null;
var _this = null;
function confirm_disable_enable_modal($this, id, status) {
    _this = $this;
    enable_disable_id = id;
    current_status = status;
    if (status == 0) {
        $.fancybox.open({
            autoCenter: true,
            fitToView: false,
            scrolling: false,
            openEffect: 'none',
            openSpeed: 1,
            autoSize: false,
            width: 450,
            height: 'auto',
            helpers: {
                overlay: {
                    css: {
                        'background': 'rgba(0, 0, 0, 0.3)'
                    },
                    closeClick: false
                }
            },
            padding: 0,
            closeBtn: false,
            content: $('#enable-popup'),
        });
    } else {
        $.fancybox.open({
            autoCenter: true,
            fitToView: false,
            scrolling: false,
            openEffect: 'none',
            openSpeed: 1,
            autoSize: false,
            width: 450,
            height: 'auto',
            helpers: {
                overlay: {
                    css: {
                        'background': 'rgba(0, 0, 0, 0.3)'
                    },
                    closeClick: false
                }
            },
            padding: 0,
            closeBtn: false,
            content: $('#disable-popup'),
        });
    }
}
function confirm_enable_disable() {
    $.ajax({
        type: "POST",
        url: _base_url + "customer/remove_customer",
        data: { customer_id: enable_disable_id, customer_status: current_status },
        dataType: "text",
        cache: false,
        success: function (result) {
            //window.location.assign(_base_url + 'customers');
            if (result == 1) {
                $(_this).attr('class', 'btn btn-success btn-small');
                $(_this).html('<i class="btn-icon-only icon-ok"></i>');
            }
            else {
                if (result == 'exist_bookings') // Edited by Geethu
                {
                    alert('Warning! Can\'t deactivate this customer, have some active bookings.');
                    result = 0;
                }
                else {
                    $(_this).attr('class', 'btn btn-danger btn-small');
                    $(_this).html('<i class="btn-icon-only icon-remove"> </i>');
                }

            }
            $(_this).attr('onclick', 'delete_customer(this, ' + enable_disable_id + ', ' + result + ')');
        }
    });
    closeFancy();
    customerlistDataTable.draw();
}