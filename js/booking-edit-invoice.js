var _saved_booking_rate_per_hour = 0;
var _saved_booking_discount_rate_per_hour = 0;
var _selected_booking_id;
var _new_booking;
var _hours;
var _service_material_cost_per_hr = 10;
var _service_material_eco_cost_per_hr = 20;
var _service_supervisor_cost_per_hr = 60;
var _totalToolCost = 0;
var _planTypeMaterialCost = 0;
var _planTypeSUpervisorCost = 0;
var _serviceMaterialCost = 0;
var _supervisorCost = 0;
var service_material_cost = 0;
$('#booking_date').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true
});

$('body').on('change', '#totimesec', function() {
	$('#rate_per_hr_sec').trigger('input');
});
$('body').on('change', '#fromtimesec', function() {
	$("#totimesec").val('');
	var _selected_index = $("#fromtimesec")[0].selectedIndex;
	var _time_to_options = '<option></option>';
	var _i = 0;
        var _last_index;
	$('#fromtimesec option').each(function(index, option) {
		if(index > _selected_index)
		{
			_time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
                        _last_index = index;
			_i++;
		}
	});
	if(_i == 0)
	{
		_time_to_options += '<option value="">No Time</option>';
	}
	$('#totimesec').html(_time_to_options);
});

$('body').on('input', '#discount_rate_perhrsec', function () {
	$('#rate_per_hr_sec').trigger('input');
});

$('body').on('click', '#free-service', function () {
	$('#rate_per_hr_sec').trigger('input');
});

$('body').on('input', '#rate_per_hr_sec', function () {
	let input = this.value || 0;
	_saved_booking_rate_per_hour = input;
	/************************************************ */
	let _start_time = parseAMDate($("#fromtimesec option:selected").text().toUpperCase());
	let _end_time = parseAMDate($("#totimesec option:selected").text().toUpperCase());
	if (_end_time < _start_time) {
		_end_time = parseAMDate(_end, 1);
	}
	let difference = _end_time - _start_time;
	let hours = (difference / 36e5),
		minutes = (difference % 36e5 / 60000);
	if (parseInt(hours) >= 0) {
		if (minutes == 0) {
			minutes = "00";
		}
	}
	if($('#free-service').is(':checked') == true)
	{
		let service_discount_rate_per_hr = 0;
		hours = 0;
	}
	// if (fixed_service_rate == true) hours = 1;
	/************************************************ */
	let service_discount_rate_per_hr = $('#discount_rate_perhrsec').val() || input;
	if(service_discount_rate_per_hr == 0 || service_discount_rate_per_hr == "")
	{
		service_discount_rate_per_hr = $('#rate_per_hr_sec').val();
	}
	let service_amount_before_discount = input * hours;
	$('#serviceamountcostsec').val(service_amount_before_discount.toFixed(2));
	let service_amount_after_discount = service_discount_rate_per_hr * hours;
	let service_amount_discount = service_amount_before_discount - service_amount_after_discount;
	$('#b_discount_sec').val(service_amount_discount.toFixed(2));
	let service_material_cost = 0;
	if($('#b-cleaning-materials').is(':checked') == true)
	{
		if($('#tools-eco-supplies').is(':checked') == true)
		{
			service_material_cost = (_service_material_eco_cost_per_hr * hours);
		} else {
			service_material_cost = (_service_material_cost_per_hr * hours);
		}
	} else {
		_totalToolCost = 0;
		if($('#free-service').is(':checked') == true)
		{
			_totalToolCost = 0;
		} else {
			$('.w_tool:checked').each(function() {
				// Add the data-amount of the current checkbox to the totalAmount variable
				_totalToolCost += parseInt($(this).attr('data-amount'));
			});
		}
		service_material_cost = _totalToolCost;
	}
	let supervisor_cost = ($('#supervisor_selected').is(':checked') == true) ? (_service_supervisor_cost_per_hr * hours) : 0;
	$('#cleaning_materials_charge_sec').val(service_material_cost.toFixed(2));
	$('#supervisor_charge_sec').val(supervisor_cost.toFixed(2));
	let service_amount_without_vat = service_amount_after_discount + service_material_cost + supervisor_cost;
	$('#tot_amout_sec').val(service_amount_without_vat.toFixed(2));
	let service_vat_amount = (service_vat_percentage / 100) * service_amount_without_vat;
	$('#service_vat_amount_sec').val(service_vat_amount.toFixed(2));
	// let service_taxed_total = service_amount_without_vat + service_vat_amount+_serviceMaterialCost+_supervisorCost;
	let service_taxed_total = service_amount_without_vat + service_vat_amount;
	$('#service_taxed_total_sec').val(service_taxed_total.toFixed(2));
});

$('body').on('input', '#service_taxed_total_sec', function () {
	let service_discount_total = parseFloat($('#b_discount_sec').val() || 0);
	let service_vat_amount = (this.value * service_vat_percentage) / (100 + service_vat_percentage);
	$('#service_vat_amount_sec').val(service_vat_amount.toFixed(2));
	let service_amount_after_discount = this.value - service_vat_amount;
	$('#tot_amout_sec').val(service_amount_after_discount.toFixed(2));
	let service_material_cost = $('#cleaning_materials_charge_sec').val();
	let supervisor_cost = $('#supervisor_charge_sec').val();
	let service_amount_without_material = service_amount_after_discount - service_material_cost - supervisor_cost + service_discount_total;
	/************************************************ */
	let _start_time = parseAMDate($("#fromtimesec option:selected").text().toUpperCase());
	let _end_time = parseAMDate($("#totimesec option:selected").text().toUpperCase());
	if (_end_time < _start_time) {
		_end_time = parseAMDate(_end, 1);
	}
	let difference = _end_time - _start_time;
	let hours = (difference / 36e5),
		minutes = (difference % 36e5 / 60000);
	if (parseInt(hours) >= 0) {
		if (minutes == 0) {
			minutes = "00";
		}
	}
	if($('#free-service').is(':checked') == true)
	{
		let service_discount_rate_per_hr = 0;
		hours = 0;
	}
	//if (fixed_service_rate == true) hours = 1;
	/************************************************ */
	let service_rate_per_hr = service_amount_without_material / hours;
	$('#rate_per_hr_sec').val(service_rate_per_hr.toFixed(2));
	let service_amount_before_discount = service_rate_per_hr * hours;
	$('#serviceamountcostsec').val(service_amount_before_discount.toFixed(2));
	let service_discount_per_hour = service_discount_total / hours;
	let discount_rate_perhr = service_rate_per_hr - service_discount_per_hour;
	$('#discount_rate_perhrsec').val(discount_rate_perhr.toFixed(2));
});

$('#editbookingdetail').on('click', function (e) {
	var service_date = $('#booking_date').val();
	var customerid = $('#customers_vh_rep_new').val();
	var maidid = $('#edit_booking_staff').val();
	if(service_date == '')
	{
		$('#editerror').text('Select service date');
		return false;
	}
	if(customerid == '')
	{
		$('#editerror').text('Select customer');
		return false;
	}
	$('#editerror').text('');
	$('#bookingtable').DataTable().clear().destroy();
	$('#servicestable').DataTable().clear().destroy();
	$('#invoicestable').DataTable().clear().destroy();
	$('#paymentstable').DataTable().clear().destroy();
	$('#invoicedetailsection').html('');
	$("#activeinvoicessection").hide();
	$("#invoicesection").hide();
	$("#allocatedpaymentssection").hide();
	getdatebookings(service_date,customerid,maidid);
	// getdateservicess(service_date,customerid,maidid);
});

function getdatebookings(service_date,customerid,maidid)
{
	$('#invoicedetailsection').html('');
	$("#activeinvoicessection").hide();
	$("#invoicesection").hide();
	$("#allocatedpaymentssection").hide();
	$('#bookingtable').DataTable().clear().destroy();
	var bookinglistDataTable;
	bookinglistDataTable = $('#bookingtable').DataTable({
        'bFilter': false,
        'bLengthChange': false,
		"bPaginate": false,
		"bInfo": false,
        'pageLength': 50,
        'processing': true,
        'serverSide': true,
        'bSort': false,
        'serverMethod': 'post',
        'ajax': {
            'url': _base_url + 'booking_edit/booking_by_servicedate',
            'data': function (data) {
                data.service_date = service_date;
                data.customerid = customerid;
                data.maidid = maidid;
            },
            "complete": function (json, type) {
                var _resp = $.parseJSON(json.responseText);
				$("#activebookingssection").show();
				getdateservicess(service_date,customerid,maidid);
            }
        },
        'columns': [
            { data: 'slno' },
            { data: 'bookingid' },
            { data: 'servicedate' },
            { data: 'customer' },
            { data: 'maid' },
            { data: 'time' },
            { data: 'amount' },
            { data: 'vat' },
            { data: 'net' },
            { data: 'action' },
        ]
    });
}

function getdateservicess(service_date,customerid,maidid)
{
	$('#servicestable').DataTable().clear().destroy();
	var servicelistDataTable;
	servicelistDataTable = $('#servicestable').DataTable({
        'bFilter': false,
        'bLengthChange': false,
		"bPaginate": false,
		"bInfo": false,
        'pageLength': 50,
        'processing': true,
        'serverSide': true,
        'bSort': false,
        'serverMethod': 'post',
        'ajax': {
            'url': _base_url + 'booking_edit/service_by_servicedate',
            'data': function (data) {
                data.service_date = service_date;
                data.customerid = customerid;
                data.maidid = maidid;
            },
            "complete": function (json, type) {
                var _resp = $.parseJSON(json.responseText);
				$("#activeservicessection").show();
            }
        },
        'columns': [
            { data: 'slno' },
            { data: 'bookingid' },
            { data: 'serviceid' },
            { data: 'servicedate' },
            { data: 'customer' },
            { data: 'maid' },
            { data: 'time' },
            { data: 'amount' },
            { data: 'status' },
            { data: 'action' },
        ]
    });
}

function viewServiceInvoice(invoiceid)
{
	$('#invoicedetailsection').html('');
	$("#activeinvoicessection").hide();
	$("#invoicesection").hide();
	$("#allocatedpaymentssection").hide();
	$('#invoicestable').DataTable().clear().destroy();
	var invoicelistDataTable;
	invoicelistDataTable = $('#invoicestable').DataTable({
        'bFilter': false,
        'bLengthChange': false,
		"bPaginate": false,
		"bInfo": false,
        'pageLength': 50,
        'processing': true,
        'serverSide': true,
        'bSort': false,
        'serverMethod': 'post',
        'ajax': {
            'url': _base_url + 'booking_edit/listinvoices',
            'data': function (data) {
                data.invoiceid = invoiceid;
            },
            "complete": function (json, type) {
                var _resp = $.parseJSON(json.responseText);
				$("#activeinvoicessection").show();
				getinvoicepayments(invoiceid);
            }
        },
        'columns': [
            { data: 'slno' },
            { data: 'invnum' },
            { data: 'customer' },
            { data: 'invoicedate' },
            { data: 'duedate' },
            { data: 'amount' },
            { data: 'vat' },
            { data: 'totalamount' },
            { data: 'paidamt' },
            { data: 'balanceamount' },
            { data: 'status' },
            { data: 'action' },
        ]
    });
}

function getinvoicepayments(invoiceid)
{
	$('#paymentstable').DataTable().clear().destroy();
	var paymentslistDataTable;
	paymentslistDataTable = $('#paymentstable').DataTable({
        'bFilter': false,
        'bLengthChange': false,
		"bPaginate": false,
		"bInfo": false,
        'pageLength': 50,
        'processing': true,
        'serverSide': true,
        'bSort': false,
        'serverMethod': 'post',
        'ajax': {
            'url': _base_url + 'booking_edit/paymentlist',
            'data': function (data) {
                data.invoiceid = invoiceid;
            },
            "complete": function (json, type) {
				$("#allocatedpaymentssection").show();
                var _resp = $.parseJSON(json.responseText);
            }
        },
        'columns': [
            { data: 'slno' },
            { data: 'invnum' },
            { data: 'customer' },
            { data: 'paymentdate' },
            { data: 'amount' },
            { data: 'action' },
        ]
    });
}

function newPopup(bookingid)
{
	$("#edit-booking-form")[0].reset();
	$.ajax({
		type: "POST",
		url: _base_url + 'booking_edit/get_booking_detail',
		data: {
			booking_id: bookingid,
			service_date: $("#booking_date").val(),
		},
		cache: false,
		success: function (response) {
			var _response = $.parseJSON(response);
			$('#customersec').text(_response.customer_name+' - '+_response.mobile_number_1);
			$('#fromtimesec').val(_response.time_from).trigger('change');
			$('#totimesec').val(_response.time_to).trigger('change');
			if(_response.cleaning_material == 'Y')
			{
				$('#b-cleaning-materials').prop('checked', true);
			}
			if(_response.supervisor_selected == 'Y')
			{
				$('#supervisor_selected').prop('checked', true);
			}
			if(_response.is_free == 'Y')
			{
				$('#free-service').prop('checked', true);
			}
			getBookingCleaningSupplies(_response.booking_id);
			$('#rate_per_hr_sec').val(_response.price_per_hr);
			if (_response.discount_price_per_hr != "" && _response.discount_price_per_hr > 0)
			{
				$('#discount_rate_perhrsec').val(_response.discount_price_per_hr);
			} else {
				$('#discount_rate_perhrsec').val(_response.price_per_hr);
			}
			if(_response.net_service_cost != "")
			{
				$('#serviceamountcostsec').val(_response.net_service_cost);
			}
			$('#b_discount_sec').val(_response.discount);
			$('#cleaning_materials_charge_sec').val(_response.cleaning_material_fee);
			$('#cleaning_materials_section').val(_response.cleaning_material);
			$('#supervisor_charge_sec').val(_response.supervisor_charge_total);
			$('#tot_amout_sec').val(_response.service_charge);
			$('#service_vat_amount_sec').val(_response.vat_charge || 0);
			$('#service_taxed_total_sec').val(_response.total_amount);
			$('#edit_booking_type').val(_response.booking_type);
			$('#edit_booking_id').val(_response.booking_id);
			$('#edit_booking_lock').val(_response.is_locked);
			$('#edit_booked_by').val(_response.booked_by);
			$('#edit_maid_id').val(_response.maid_id);
			$('#edit_timefrom').val(_response.oldtimefrom);
			$('#edit_timeto').val(_response.oldtimeto);
			$('#edit_book_date').val($('#booking_date').val());
			
			$.fancybox.open({
				autoCenter: true,
				fitToView: false,
				scrolling: false,
				openEffect: 'none',
				openSpeed: 1,
				autoSize: false,
				width: 600,
				height: 'auto',
				helpers: {
					overlay: {
						css: {
							'background': 'rgba(0, 0, 0, 0.3)'
						},
						closeClick: false
					}
				},
				padding: 0,
				closeBtn: false,
				content: $('#new-popup'),
			});
		}
	});
}

function parseAMDate(input, next_day) 
{
	var dateReg = /(\d{1,2}):(\d{2})\s*(AM|PM)/;
	var hour, minute, result = dateReg.exec(input);
	if (result) {
		hour = +result[1];
		minute = +result[2];
		if (result[3] === 'PM' && hour !== 12) {
			hour += 12;
		}
	}
	if (!next_day) {
		return new Date(1970, 01, 01, hour, minute).getTime();
	}else{
		return new Date(1970, 01, 02, hour, minute).getTime();
	}
}

/**====================================================================
 * Function get Cleaning supplies for a particular booking
 ======================================================================*/
getBookingCleaningSupplies = function(booking_id){
	$.ajax({
		type: "GET",
		url: _base_url + 'cleaning_supplies/get_cleaning_supplies_by_booking/' + booking_id,
		cache: false,
		success: function (response) {
		  // Parse the JSON response to an array of supplies
		  var supplies = response;
	
		  // Log the supplies array to verify if data is correct
	
		  // Loop through the supplies and set the corresponding checkboxes as checked
		  $.each(supplies, function(index, supply) {
			var supplyId = supply.id;
			var checkboxId = "tools-" + supply.name.replace(/\s+/g, '-').toLowerCase();
			console.log(checkboxId);
			// Check the checkbox by setting the "checked" property to true
			$("#" + checkboxId).prop("checked", true);

			if($('#b-cleaning-materials').is(':checked')==true){
				$("#cleanPlanType").show();
				$("#customtools").hide();
			}else{
				$("#cleanPlanType").hide();
				$("#customtools").show();
			}

		  });
		},
		error: function(xhr, status, error) {
		  console.log("AJAX Error:", error);
		}
	  });
}
//=====================================================================

/**====================================================================
 * Cleaning materials Toggle functions
 ======================================================================*/
 handleCleaningMaterialsCheckboxClick = function(){
	//--- Calculate hours 
	// let hours = calculateHours();
	if($('#b-cleaning-materials').is(':checked')==true){
		$("#cleanPlanType").show();
		$("#customtools").hide();
		$("input[name='w_tool']:checkbox").prop('checked',false);
	}else{
		$("#cleanPlanType").hide();
		$("#customtools").show();
		$("input[name='w_tooltype']:checkbox").prop('checked',false);
	}
	$('#rate_per_hr_sec').trigger('input');
	// if(_planTypeMaterialCost==0){
		// _planTypeMaterialCost = $("#tools-regular-supplies").attr('data-amount');
	// }
	// setMaterialCost();
	// setSupervisorCost();
	// adjustTotals();
 }
/**====================================================================**/
$('body').on('click', '#b-cleaning-materials', handleCleaningMaterialsCheckboxClick);

$('body').on('change', '.w_tooltype', function() {
	$('#rate_per_hr_sec').trigger('input');
});
$('body').on('click', '.w_tool', function() {
	$('#rate_per_hr_sec').trigger('input');
});
$('body').on('click', '#supervisor_selected', function() {
	$('#rate_per_hr_sec').trigger('input');
});

$('body').on('click', '#edit-booking-form-btn', function() {
	$('#edit-booking-form-btn').attr('id', 'saving-edit-booking-form-btn');
	$('#saving-edit-booking-form-btn').val('Please wait...');
	var selecteddate = $("#booking_date").val();
	var selectedcustomer = $("#customers_vh_rep_new").val();
	var selectedmaid = $("#edit_booking_staff").val();
	var _cleaning_supplies_array = [];
	if($('#b-cleaning-materials').is(':checked')==false){
		$('.w_tool:checked').each(function() {
		 _cleaning_supplies_array.push($(this).val());
		});
	}else{
		const planCleanType = $('.w_tooltype:checked').val();		
		 _cleaning_supplies_array.push(planCleanType);
	}
	$.ajax({
		type: "POST",
		url: _base_url + 'booking_edit/update_booking_data',
		data: $('#edit-booking-form').serialize() + "&cleaning_supplies="+_cleaning_supplies_array,
		cache: false,
		success: function (response) {
			$('#saving-edit-booking-form-btn').attr('id', 'edit-booking-form-btn');
			$('#edit-booking-form-btn').val('Save');
			if(response == "refresh")
			{
				$('#alert-title').html('Error !');
				$('#alert-message').html('An unexpected error. Please try again.');
				fancybox_show('alert-popup');
			} else if(response=="locked")
			{
				$('#alert-title').html('Error !');
				$('#alert-message').html('This booking is locked by another user.');
				fancybox_show('alert-popup');
			} else {
				var _response = $.parseJSON(response);
				if(_response.status == "success")
				{
					$('#alert-title').html('Success !');
					$('#alert-message').html(_response.message);
					fancybox_show('alert-popup');
				} else {
					$('#alert-title').html('Error !');
					$('#alert-message').html(_response.message);
					fancybox_show('alert-popup');
				}
			}
			getdatebookings(selecteddate,selectedcustomer,selectedmaid);
		}
	});
});

function editServiceInvoice(invoiceid)
{
	$("#edit-booking-form")[0].reset();
	$.ajax({
		type: "POST",
		url: _base_url + 'booking_edit/get_invoice_detail',
		data: {
			invoiceid: invoiceid,
		},
		cache: false,
		success: function (response) {
			$('#invoicedetailsection').html(response);
			$("#invoicesection").show();
		}
	});
}

function deleteInvoicePayments(invoiceid,inv_map_id,paymentid,customerid)
{
	$.ajax({
		type: "POST",
		url: _base_url + 'booking_edit/delete_invoice_payments',
		data: {
			invoiceid: invoiceid,
			inv_map_id: inv_map_id,
			paymentid: paymentid,
			customerid: customerid,
		},
		cache: false,
		success: function (response) {
			var _response = $.parseJSON(response);
			if(_response.status == "error")
			{
				$('#alert-title').html('Error !');
				$('#alert-message').html(_response.message);
				fancybox_show('alert-popup');
			} else if(_response.status == "success"){
				$('#alert-title').html('Success !');
				$('#alert-message').html(_response.message);
				viewServiceInvoice(invoiceid);
				fancybox_show('alert-popup');
			}
		}
	});
}

function deleteServiceInvoice(invoiceid)
{
	$.ajax({
		type: "POST",
		url: _base_url + 'booking_edit/cancel_invoice',
		data: {
			invoiceid: invoiceid,
		},
		cache: false,
		success: function (response) {
			var _response = $.parseJSON(response);
			if(_response.status == "error")
			{
				$('#alert-title').html('Error !');
				$('#alert-message').html(_response.message);
				fancybox_show('alert-popup');
			} else if(_response.status == "success"){
				$('#alert-title').html('Success !');
				$('#alert-message').html(_response.message);
				viewServiceInvoice(invoiceid);
				fancybox_show('alert-popup');
			}
		}
	});
}

function deleteService(serviceid)
{
	$.ajax({
		type: "POST",
		url: _base_url + 'booking_edit/delete_service',
		data: {
			serviceid: serviceid,
		},
		cache: false,
		success: function (response) {
			var _response = $.parseJSON(response);
			if(_response.status == "error")
			{
				$('#alert-title').html('Error !');
				$('#alert-message').html(_response.message);
				fancybox_show('alert-popup');
			} else if(_response.status == "success"){
				$('#alert-title').html('Success !');
				$('#alert-message').html(_response.message);
				$('#editbookingdetail').trigger('click');
				fancybox_show('alert-popup');
			}
		}
	});
}

function deleteBooking(bookingid)
{
	var service_date = $("#booking_date").val();
	$.ajax({
		type: "POST",
		url: _base_url + 'booking_edit/delete_booking',
		data: {
			booking_id: bookingid,
			service_date: $("#booking_date").val(),
		},
		cache: false,
		success: function (response) {
			var _response = $.parseJSON(response);
			if(_response.status == "error")
			{
				$('#alert-title').html('Error !');
				$('#alert-message').html(_response.message);
				fancybox_show('alert-popup');
			} else if(_response.status == "success")
			{
				$.fancybox.open({
					autoCenter: true,
					fitToView: false,
					scrolling: false,
					openEffect: 'none',
					openSpeed: 1,
					helpers: {
						overlay: {
							css: {
								'background': 'rgba(0, 0, 0, 0.3)'
							},
							closeClick: true
						}
					},
					padding: 0,
					closeBtn: true,
					content: _alert_html = `<div id="alert-popup">
													<div class="head">Delete<span class="pop_close n-close-btn"></span></div>
													<div class="col-sm-12 pt-3" style="font-size: 18px;text-align: left;">Are you sure want to delete this one day booking ?</div>
													<div class="content">
													<div class="row m-0 n-delete-section-main pl-4 pt-4 pr-4">
														<span id="deleteremarks_book_new" style="color:red; display:none;">Please enter remarks.</span>
														<textarea name="remark" placeholder="Remarks" id="delete_yes_book_perm_remark_new"></textarea>
														<div class="bottom mb-4">
															<div class="col-sm-6 pl-0">
																<input type="button" value="Yes" data-bookID="` + bookingid + `" data-servDate="` + service_date + `" class="n-btn red-btn mb-0 delete_yes_book_daynew"/>
															</div>
															<div class="col-sm-6 pl-0">
																<input type="button" value="Cancel" class="n-btn green-btn mb-0 assign_no pop_close" />
															</div>
														</div>
													</div>
													</div>
											</div>`,
				});
			}
		}
	});
}

$('body').on('click', '.delete_yes_book_daynew', function() {
    var remarks = $.trim($('#delete_yes_book_perm_remark_new').val());
	if(remarks == "")
	{
		$("#delete_yes_book_perm_remark_new").focus();
        $('#deleteremarks_book_new').css('display','block');
	} else {
        $('#deleteremarks_book_new').css('display','none');
        $.fancybox.close();
        var _booking_id=$(this).attr('data-bookID');
        var _servicedate=$(this).attr('data-servDate');
		$.ajax({
			type: "POST",
			url: _base_url + 'booking_edit/delete_booking_process',
			data: {
				bookingid: _booking_id,
				servicedate: _servicedate,
				remarks: remarks,
			},
			cache: false,
			success: function (response) {
				var _response = $.parseJSON(response);
				if(_response.status == "error")
				{
					$('#alert-title').html('Error !');
					$('#alert-message').html(_response.message);
					fancybox_show('alert-popup');
				} else if(_response.status == "success"){
					$('#alert-title').html('Success !');
					$('#alert-message').html(_response.message);
					$('#editbookingdetail').trigger('click');
					fancybox_show('alert-popup');
				}
			}
		});
    }
});