var start_time;
var _saved_booking_rate_per_hour = 0;
var _saved_booking_discount_rate_per_hour = 0;
var _selected_booking_id;
var _new_booking;
var _hours;
var _service_material_cost_per_hr = 10;
var _service_supervisor_cost_per_hr = 60;
var _totalToolCost = 0;
var _planTypeMaterialCost = 0;
var _planTypeSUpervisorCost = 0;
var _serviceMaterialCost = 0;
var _supervisorCost = 0;


$('.datepicker').datepicker().on('changeDate', function(ev){ 
	if (ev.date){
		var _date = new Date(ev.date);
		var _new_date = ('0' + _date.getDate()).slice(-2) + '-' + ('0' + (_date.getMonth()+1)).slice(-2) + '-' + _date.getFullYear();
		window.location = _base_url + 'booking/' + _new_date;
	}
});
$('#freemaids').change(function(){
    window.location = _base_url + 'booking/' + $('#servicesdate').val() + '/' + $('#freemaids').val();
});
$('#team_ids').change(function(){
    window.location = _base_url + 'booking/' + $('#servicesdate').val() + '/' + $('#freemaids').val() + '/' + $('#team_ids').val();
});
var _maids = $.parseJSON($('#all-maids').val());
var _time_slots = $.parseJSON($('#time-slots').val());
var _bpop_open = false;
function apply_selectable() {
	$('.selectable').selectable({
		start: function(event, ui) {
			$('.ui-selected').removeClass('ui-selected');
			_bpop_open = true;
		},		
		stop: function(event, ui) {
			var _maid_id = $(this).parent().attr('id').replace('maid-', '');
			var _time_from_index = $('.ui-selected').first().attr('id').replace('t-', '');
			var _time_to_index = $('.ui-selected').last().attr('id').replace('t-', '');
			_time_to_index = typeof _time_slots['t-' + parseInt(parseInt(_time_to_index) + 1)] != 'undefined' ? parseInt(_time_to_index) + 1 : 0;
			var _schedule_type = 'new';
                        var d = new Date();
                        var month = d.getMonth()+1;
                        var day = d.getDate();
                        var strDate =  ((''+day).length<2 ? '0' : '') + day + '-' + ((''+month).length<2 ? '0' : '') + month + '-' + d.getFullYear();
			var _service_date = $('#servicesdate').val();
                            open_bpop(_maid_id, _time_from_index, _time_to_index, _schedule_type);
		}
	});
}
var screen_width = $('body').innerWidth();
var scheduler_width = screen_width - 12;
var time_grid_width = scheduler_width - 195 - 3 + 8;
$('#schedule').css('width', scheduler_width + 'px');
$('#schedule .time_line').css({'width' : (scheduler_width - 145 - 20 - 34 - 20 -3) + 'px'});
$('#schedule .time_grid').css('width', time_grid_width + 'px');
var _booking_popup = $('#booking-popup').html();
$('#booking-popup').remove();
$('.sel2').select2({ dropdownAutoWidth: true, width: 'resolve' });
$('.book-customer').select2();
function open_bpop(maid_id, time_from_index, time_to_index, schedule_type) {
	// new booking
	_new_booking = true;
	_bpop_open = true;
	var _booking_popup_parsed = $.parseHTML(_booking_popup);
	var _maid_id = maid_id;
	if(typeof _maids['m-' + _maid_id] == 'undefined')
	{
		window.location = _page_url;
	}
         _start_time = parseAMDate(_time_slots['t-' + time_from_index].display.toUpperCase());
        _end_time = parseAMDate(_time_slots['t-' + time_to_index].display.toUpperCase());
        if (_end_time < _start_time) {
            _end_time = parseAMDate(_end, 1);
        }
        var difference = _end_time - _start_time;
        var hours = Math.floor(difference / 36e5),
                minutes = Math.floor(difference % 36e5 / 60000);
        if (parseInt(hours) >= 0) {
            if (minutes == 0) {
                minutes = "00";
            }
        }
	$(_booking_popup_parsed).find('#b-maid-name').text(_maids['m-' + _maid_id]);
	$(_booking_popup_parsed).find('#b-time-slot').text('(' + _time_slots['t-' + time_from_index].display + ' to ' + _time_slots['t-' + time_to_index].display + ')');
	$(_booking_popup_parsed).find('#maid-id').val(_maid_id);
	if(schedule_type == 'new')
	{
		$(_booking_popup_parsed).find('#copy-booking, #delete-booking, #transfer-driver,#move-booking-btn').remove();
	}
	$.fancybox.open({
		autoCenter : true,
		fitToView : false,
		scrolling : false,
		openEffect : 'none',
		openSpeed : 1,
                autoSize: false,
                width:"55%",
                height:730,
		helpers : {
			overlay : {
				css : {
					'background' : 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding : 0,
		closeBtn : false,
		content : _booking_popup_parsed,
		beforeShow: function() {
			$('#b-from-time').val( _time_slots['t-' + time_from_index].stamp);
			refresh_to_time();
			$('#b-to-time').val(_time_slots['t-' + time_to_index].stamp);
			$('.sel2').select2({ dropdownAutoWidth: true, width: 'resolve' });
			$("#b-customer-id").select2({
				language: {
					noResults: function() { return '<a href="javascript:void(0);" id="add-customer-popup" onclick="return add_customer()">Add Customer</a>'; }
				},
				escapeMarkup: function (markup) {
					return markup;
				},
				ajax: { 
				 url: _base_url+"customer/report_srch_usr_news",
				 type: "post",
				 dataType: 'json',
				 delay: 150,
				 data: function (params) {
						return {
						  searchTerm: params.term, // search term
						};
					  },
				 processResults: function (response) {
					 if(response.id === "")
					 { 
					 } else {
						return {results: response};
					 }
				 },
				 cache: true
				},
			});
			$('.fancybox-skin').css({
				'border-radius'  : '10px'
			});
			$('#b-hrs_per_week').val(hours);                 
			$('.end_datepicker').datepicker({
				format: 'dd/mm/yyyy',
				autoclose: true,
				startDate: new Date($('#repeate-end-start').val())
			});
			$('#sms_schedule_date').datetimepicker({
				format: 'yyyy-mm-dd HH:ii P',
				pickerPosition: "top-left",
				autoclose: true,
				showMeridian: true,
			});
			$("#customer-popup").validate({
				rules: {
					customer_name: "required",
					area: "required",
					address: "required",
					mobile_number1: {
						required: true,
						number: true,
						minlength: 9,
						maxlength: 12,
						remote: {
							url: _base_url + "booking/register_contact_exists",
							type: "post",
							data: {
								phone: function(){ return $("#mobile_number1").val(); }
							}
						}
					}
				},
				messages: {
					customer_name: "Enter customer name",
					area: "Select customer area",
					address: "Enter customer address",
					mobile_number1: {
						required : "Enter customer mobile number",
						minlength: "Invalid mobile number",
						remote: "Mobile number already exist."
					}
				}
			});
			$("#popup-add-customer").click(function(){
				if($("#customer-popup").valid()){
					$('.mm-loader').css('display','block');
					var check_mobile = $('#customer-popup #mobile_number1').val();
					$.post(_base_url + 'booking', {action: 'register_contact_exists_new',
						phone: check_mobile,
					},
					function (response) {
						var _json = $.parseJSON(response);
						if(_json.status == 'exist')
						{
							$('.mm-loader').css('display','none');
							$('#b-error').html(_json.message);
							$('#b-error').show();
						} else if(_json.status == 'not'){
							$('#b-error').hide();
							$.post(_page_url+'/add_customer_popup', $("#customer-popup" ).serialize())
							.done(function( data ) {
							  var _json = $.parseJSON(data);
								  $('#customer-popup')[0].reset();
								  $("#b-customer-id").html(_json.html);
								  $("#b-customer-id").val(_json.cust_id);
								  $('#customer-picked-address').html('');
								  $("#customer-add-popup").hide();
								  get_no_of_address(_json.cust_id);
								  get_pending_amount(_json.cust_id);
								  $("#customer-detail-popup").show();
								  $('.mm-loader').css('display','none');
							});  
						}
					}).error(function () {
						alert('Unexpected error!')
					});   
				}
			});
		}
	});
	calculate_cost();
}
    $('body').on('change', '#b-booking-type', function () {
		/***************************** */
		// default check current day
		let dateString = new Date();
		let dayToday = dateString.getDay();
		$("#repeat-days").find("input[type=checkbox][value=" + dayToday + "]").prop("checked", true);
		/***************************** */
		var custid = $('#b-customer-id').val();
        var _booking_type = $(this).val();
        if (_booking_type == 'WE' || _booking_type == 'BW')
        {
            $('#repeat-days').css('display', 'block');
            $('#repeat-ends').css('display', 'block');
            $('body').on('change', '.w_day', function () {
                $('#total_week_days').val($('.w_day:checked').length);
                _start_time = parseAMDate($("#b-from-time option:selected").text().toUpperCase());
                _end_time = parseAMDate($("#b-to-time option:selected").text().toUpperCase());
                if (_end_time < _start_time) {
                    _end_time = parseAMDate(_end, 1);
                }
                var difference = _end_time - _start_time;
                var hours = Math.floor(difference / 36e5),
                        minutes = Math.floor(difference % 36e5 / 60000);
                if (parseInt(hours) >= 0) {
                    if (minutes == 0) {
                        minutes = "00";
                    }
                    var weekdayscount = $('#total_week_days').val();
                      var customer_pre_book_hrs =  $('#total_prebook_hrs').val();
            $('#b-hrs_per_week').val(parseInt(customer_pre_book_hrs)+parseInt(hours*weekdayscount));
                }
            });
        } else
        {
            $('#total_week_days').val('1');
            $('#repeat-days').hide();
            $('#repeat-ends').hide();
        }
        _start_time = parseAMDate($("#b-from-time option:selected").text().toUpperCase());
        _end_time = parseAMDate($("#b-to-time option:selected").text().toUpperCase());
        if (_end_time < _start_time) {
            _end_time = parseAMDate(_end, 1);
        }
        var difference = _end_time - _start_time;
        var hours = Math.floor(difference / 36e5),
                minutes = Math.floor(difference % 36e5 / 60000);
        if (parseInt(hours) >= 0) {
            if (minutes == 0) {
                minutes = "00";
            }
            var weekdayscount = $('#total_week_days').val();
              var customer_pre_book_hrs =  $('#total_prebook_hrs').val();
            $('#b-hrs_per_week').val(parseInt(customer_pre_book_hrs)+parseInt(hours*weekdayscount));
        }
    });
$('body').on('change', 'input[name="repeat_end"]', function() {
	if($(this).val() == 'ondate')
	{
		$('#repeat-end-date').removeAttr('disabled'); 
	}
	else
	{
		$('#repeat-end-date').attr('disabled', 'disabled'); 
	}
});
$('#schedule-grid-rows').on('click', '.schedule_bubble', function(e) {
	// click on existing booking
	_new_booking = false;
	if ($(e.target).attr("class") == "fa fa-clipboard fa-1x"){
		return;
	}
	_bpop_open = true;
        $(".start_status").hide();
        $(".service_started_at").hide();
        $(".finish_status").hide();
	var _all_bookings = $.parseJSON($('#all-bookings').val());
	var _booking_id = $(this).attr('id').replace('booking-', '');
	_selected_booking_id = _booking_id;
	console.log(_all_bookings[_booking_id]);
	var _booking_popup_parsed = $.parseHTML(_booking_popup);
	$(_booking_popup_parsed).find('#b-maid-name').text(_maids['m-' + _all_bookings[_booking_id].maid_id]);
	$(_booking_popup_parsed).find('#b-time-slot').text('(' + _all_bookings[_booking_id].time_from + ' to ' + _all_bookings[_booking_id].time_to + ')');
	$(_booking_popup_parsed).find('#b-booking-id').html('&nbsp;&nbsp;Ref. No. #' + _booking_id);
	/************************************************************************ */
	// use this for get all data abount one booking (do later)
	var booking_data = null;
	$('#extra-services .selected').empty();
	$.ajax({
		type: 'GET',
		url: _base_url + "data/get_booking",
		data: {
			booking_id: _booking_id,
		},
		cache: false,
		success: function (data) {
			booking_data = data.booking;
			var list_html = '';
			$.each(data.extra_services, function (key, extra_service) {
				// show selected extra services
				list_html += `<li>` + extra_service.extra_service_name + `</li>`;
				// $('#extra-services .selected').html(list_html);
				$('#extra-services').html(list_html);
				$('#extra_services_show').show();
			});
			/****************** check cleaning_supplies ****************** */
			$('#booking-form input[name="w_tool[]"]').prop("checked", false); // reset
			if(data.booking.supervisor_type == "Y")
			{
				alert('hai');
				$('#booking-form input[name="supervisor_selected"]').prop('checked', true);
			}
			
			$.each(data.cleaning_supplies, function (key, cleaning_supply) {
				// show selected extra services
				$('#booking-form input[name="w_tool[]"]:checkbox[value=' + cleaning_supply.cleaning_supply_id +']').prop("checked", true);
			});
		},
		error: function (data) {
			alert('error');
		}
	});
	/************************************************************************ */
        var _address_change_html = '';
        $.post(_page_url, { action: 'get-no-of-customer-address', customer_id: _all_bookings[_booking_id].customer_id }, function(response) {
			if(response == 'refresh')
			{
				$.fancybox.open({
					autoCenter : true,
					fitToView : false,
					scrolling : false,
					openEffect : 'fade',
					openSpeed : 100,
                    height:730,
					helpers : {
						overlay : {
							css : {
								'background' : 'rgba(0, 0, 0, 0.3)'
							},
							closeClick: false
						}
					},
					padding : 0,
					closeBtn : false,
					content:  '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>'
				});
				_refresh_page = true;
			}
			else
			{
				var _resp = $.parseJSON(response);
                                get_pending_amount(_all_bookings[_booking_id].customer_id);
                                _start_time = parseAMDate($("#b-from-time option:selected").text().toUpperCase());
                                _end_time = parseAMDate($("#b-to-time option:selected").text().toUpperCase());
                                if (_end_time < _start_time) {
                                     _end_time = parseAMDate(_end, 1);
                                }
                                var difference = _end_time - _start_time;
                                var hours = Math.floor(difference / 36e5),
                                minutes = Math.floor(difference % 36e5 / 60000);
                                $('#b-hrs_per_week').val(hours); 
				if (_all_bookings[_booking_id].priceperhr > 0 || _all_bookings[_booking_id].price_hourly > 0)
								{
					$('#b-rate_per_hr').val(_all_bookings[_booking_id].priceperhr > 0 ? _all_bookings[_booking_id].priceperhr :  _all_bookings[_booking_id].price_hourly);
								} else {
                                   $.ajax({
                    type: "POST",
					url: _base_url + 'settings/check_hourly_price_new',
                    data: {customer_id: _all_bookings[_booking_id].customer_id},
                    cache: false,
                    success: function (response)
                    {
                        if (response == '') {
                            response = '';
                        }
                        if (response != null) {
                            $('#b-rate_per_hr').val(response);
                        }
                    }
                });
								}
			}
                    });
	$(_booking_popup_parsed).find('#save-booking').remove();
	$.fancybox.open({
		autoCenter : true,
		fitToView : false,
		scrolling : false,
		openEffect : 'none',
		openSpeed : 1,
                autoSize: false,
                width:"80%",
                height:730,
		helpers : {
			overlay : {
				css : {
					'background' : 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding : 0,
		closeBtn : false,
		content: _booking_popup_parsed,
		beforeShow: function() {
                        $(".fancybox-skin").css("width","100%").css("margin","0 auto");
                                    $(".fancybox-outer").css("width","100%").css("margin","0 auto");
                                    $(".fancybox-inner").css("width","100%").css("margin","0 auto");
                                    $(".fancybox-wrap").css("width","80%").css("margin","0 auto").css("left","10%");
                                    $("#popup-booking").removeClass("col-md-12 col-sm-12");
                                    $("#popup-booking").addClass("col-md-8 col-sm-7");
                                    $("#customer-add-popup").hide();
                                    $("#customer-detail-popup").show();
                        $.post(_page_url, { action: 'get-details-customer', customer_id: _all_bookings[_booking_id].customer_id }, function(response) {
                                    var _respval = $.parseJSON(response);
                                    if(_respval.payment_type == "D")
                                    {
                                        var paytype = "Daily";
                                    } else if(_respval.payment_type == "W")
                                    {
                                        var paytype = "Weekly";
                                    } else if(_respval.payment_type == "M")
                                    {
                                        var paytype = "Monthly";
                                    } else {
                                        var paytype = "";
                                    }
                                    if(_respval.customer_booktype == '0')
                                    {
                                        var cus_booktype = "Non Regular";
                                    } else if(_respval.customer_booktype == '1')
                                    {
                                        var cus_booktype = "Regular";
                                    } else {
                                        var cus_booktype = "";
                                    }
                                    $(".payment_mode_customer").show();
                                    $('#b-customer-ids-cell').text(_respval.customer_name);
                                    $('#b-customer-mobile-cell').text(_respval.mobile_number_1);
                                    $('#b-customer-email-cell').text(_respval.email_address);
                                    $('#b-customer-notes-cell').text(_respval.customer_notes);
                                    $('#b-customer-apartment-no').text(_all_bookings[_booking_id].apartment_no);
                                    $('#b-customer-paytype-cell').text(paytype);
                                    $('#b-customer-booktype-cell').text(cus_booktype);
                                    $('#b-customer-area-cell').text(_all_bookings[_booking_id].customer_area);
                                    $('#b-customer-zone-cell').text(_all_bookings[_booking_id].customer_zone);
                                    $('#b-customer-reference-cell').val(_all_bookings[_booking_id].justmop_reference);
                                    $('#b-customer-address-cell').text(_all_bookings[_booking_id].customer_address);
									if(_all_bookings[_booking_id].net_service_cost != "")
									{
										$('#serviceamountcost').val(_all_bookings[_booking_id].net_service_cost);
									}
                                    $('#b-discount').val(_all_bookings[_booking_id].discount);
									if (_all_bookings[_booking_id].discount_price_per_hr != "" && _all_bookings[_booking_id].discount_price_per_hr > 0)
									{
										$('#discount_rate_perhr').val(_all_bookings[_booking_id].discount_price_per_hr);
									}
									else{
										$('#discount_rate_perhr').val($('#b-rate_per_hr').val());
									}
									$('#tot_amout').val(_all_bookings[_booking_id].service_charge);
									$('#service_vat_amount').val(_all_bookings[_booking_id].vat_charge || 0);
									$('#service_taxed_total').val(_all_bookings[_booking_id].total_amount);
									$('.sertype_sectn').html(_all_bookings[_booking_id].service_type_names);
                                });
			$.ajax({
				type: "POST",
				url: _base_url + 'booking/get_payment_mode',
				data: {
					booking_id: _booking_id,
					service_date: $("#servicesdate").val()
				},
				cache: false,
				success: function (response) {
					var _response = $.parseJSON(response);
					if (_response.mob != null) {
						$('#b-customer-paymode-cell').val(_response.mob);
					} else if (_response.pay_by != null) {
						$('#b-customer-paymode-cell').val(_response.pay_by);
					} else {
						$('#b-customer-paymode-cell').val(_all_bookings[_booking_id].payment_mode);
					}
				}
			});
			$.ajax({
				type: "POST",
				url: _base_url + 'booking/get_driver_details',
				dataType: "json",
				data: {
					booking_id: _booking_id,
					service_date: $("#servicesdate").val()
				},
				cache: false,
				success: function (data) {
					if (data.booking_driver == null){
						// currently no driver added to this booking
						$('#updatedriversec').show();
						$('.normaldriversec').hide();
					}
					else{
						$('#driver_select_id option[value="' + (data.transferred_tabletid != null ? data.transferred_tabletid : data.booking_tabletid) + '"]').prop('selected', true);
						$('#driver_select_id').trigger('change');
						$("#driver_select_id").prop("disabled", true);
					}
				},
				error: function (data){
				}
			});
            $('#b-customer-id').parent().prepend(_all_bookings[_booking_id].customer_nick_name+" - "+_all_bookings[_booking_id].mobile_number_1);
			$('#b-customer-id').remove();
			$('#b-customer-id-cell').append('<input type="hidden" id="b-customer-id" value="' + _all_bookings[_booking_id].customer_id + '" />');
			$('#customer-address-id').val(_all_bookings[_booking_id].customer_address_id);
			$('#b-service-type-id').parent().prepend($('#b-service-type-id option[value="' + _all_bookings[_booking_id].service_type_id + '"]').text());
			$('#b-service-type-id').remove();
            if(_all_bookings[_booking_id].cleaning_type == 'Y')
			{
				$('#b-cleaning-materials').prop('checked', true);
			}
			if(_all_bookings[_booking_id].supervisor_type == 'Y')
			{
				$('#supervisor_selected').prop('checked', true);
			}
			// Function to get cleaning supply details
			getBookingCleaningSupplies(_booking_id);

			$('#b-from-time').val(_all_bookings[_booking_id].time_from_stamp);
			refresh_to_time();
			$('#b-to-time').val(_all_bookings[_booking_id].time_to_stamp);
			if(_all_bookings[_booking_id].is_locked == 1)
			{
				$('#lock-booking').prop('checked', true);
			}
			if(_all_bookings[_booking_id].booking_type == 'OD')
			{
				$('#b-booking-type').parent().prepend($('#b-booking-type option[value="' + _all_bookings[_booking_id].booking_type + '"]').text() + ' (' + _all_bookings[_booking_id].service_end_date + ')');
			}
			else
			{
				$('#b-booking-type').parent().prepend($('#b-booking-type option[value="' + _all_bookings[_booking_id].booking_type + '"]').text() + ' (' + _all_bookings[_booking_id].service_week_day + 's)');
			}
			$('#b-booking-type').remove();
			if(_all_bookings[_booking_id].booking_type != 'OD')
			{
				$('#repeat-ends').css('display', 'flex');
				$('#repeat-ends').css('padding-bottom', '5px');
				if(_all_bookings[_booking_id].service_end == 1)
				{
					$('#repeat-end-ondate').attr('checked', 'checked');
					$('#repeat-end-date').removeAttr('disabled');
					$('#repeat-end-date').val(_all_bookings[_booking_id].service_end_date);
				}
				else
				{
					$('#repeat-end-never').attr('checked', 'checked');
				}
			}
			if(_all_bookings[_booking_id].crew_in != '')
			{
				$('#crewsecval').html(_all_bookings[_booking_id].crew_in);
				$('#crewsec').css('display', 'table-row');
			} else {
				$('#crewsec').css('display', 'none');
			}
			if(_all_bookings[_booking_id].transfeeredtabletname != "")
			{
				$('#transfeered_to_show').html(_all_bookings[_booking_id].transfeeredtabletname);
				$('#transferred_to').show();
			} else {
				$('#transferred_to').css('display', 'none');
			}
			if(_all_bookings[_booking_id].tabletid > 0)
			{
			} else {
			}
			$('#booking-note').val(_all_bookings[_booking_id].booking_note);
			$('#booking-id').val(_booking_id);
			$('#hiddentabletid').val(_all_bookings[_booking_id].tabletid);
			if(_all_bookings[_booking_id].del_requesttype == "DELETE")
			{
				$('#delete_replace_section_new p').html("Delete Request");
				if(_all_bookings[_booking_id].del_requeststatus == "0")
				{
					$('#approvetextlabelDelete').val("Not Approved");
					$('#delete-cancelbtn').attr('data-id', _all_bookings[_booking_id].del_requestid);
					$('#delete-cancelbtn').show();
				} else if(_all_bookings[_booking_id].del_requeststatus == "1"){
					$('#approvetextlabelDelete').val("Approved");
					$('#delete-cancelbtn').hide();
				} else if(_all_bookings[_booking_id].del_requeststatus == "2"){
					$('#approvetextlabelDelete').val("Rejected");
					$('#delete-cancelbtn').hide();
				} else if(_all_bookings[_booking_id].del_requeststatus == "3"){
					$('#approvetextlabelDelete').val("Cancelled");
					$('#delete-cancelbtn').hide();
				}
				$('#delete_replace_section_new').show();
			}
			$('.sel2').select2({ dropdownAutoWidth: true, width: 'resolve' });
			$('.fancybox-skin').css({
				'border-radius'  : '10px'
			});
			$('.end_datepicker').datepicker({
				format: 'dd/mm/yyyy',
				autoclose: true,
				startDate: new Date($('#repeate-end-start').val())
			});
                        $.post(_page_url, { action: 'check_booking_status', booking_id:_booking_id }, function(response) {
                            if(response!=0){
                          var _response_status = $.parseJSON(response);
                          var _service_status   = _response_status[0]['service_status'];
                          start_time= _response_status[0]['service_date']+" "+_response_status[0]['start_time'];
                          if(_service_status==1)
                          {
                            if(start_time)
                            {
                            $(".start_status").show();
                            $(".service_started_at").show();
                            var _start_t=new Date(start_time);
                            var _started_at=formatAMPM(_start_t);
                            $("#started_time").text(_started_at).css("color","#428bca");
                            startTime();  
                            }
                          }
                          if(_service_status==2)
                            {
                             if(_response_status[0]['end_time']){
                            $(".finish_status").show();
                            var end_time= _response_status[0]['service_date']+" "+_response_status[0]['end_time'];
                            var start_t=new Date(start_time);
                            var end_t=new Date(end_time);;
                            var t=end_t.getTime()- start_t.getTime();
                            var time=convertMS(t,false);
                            var _started_at=formatAMPM(start_t);
                            var _ended_at=formatAMPM(end_t);
                            if(time.h > 0 || time.m > 0 ) { 
                            var worked_hrs_text="&nbsp;<i class='fa fa-clock-o' aria-hidden='true'></i>&nbsp;Took";
                             }
                            if(time.h > 0) { 
                                worked_hrs_text+=" " + time.h;
                                worked_hrs_text+=((time.h>1? ' hours ':' hour '));
                               }
                            if(time.m > 0) { 
                                worked_hrs_text+=((time.h && time.m) ? ' and ':' ');
                                worked_hrs_text+=" " + time.m;
                                worked_hrs_text+=((time.m>1? ' minutes ':' minute '));
                               }   
                            $("#time_started").text(_started_at).css("color","#428bca");
                            $("#time_finished").text(_ended_at).css("color","#428bca");
                            $("#worked_hrs").html(worked_hrs_text).css("font-weight","bold");
                              }
                            }
                        }  
                        });
		}
	});
	$('#cleaning-materials-charge').val(parseFloat(_all_bookings[_booking_id].cleaning_material_fee));
	$('#supervisor-charge').val(parseFloat(_all_bookings[_booking_id].supervisor_charge_total));
	_saved_booking_rate_per_hour = _all_bookings[_booking_id].priceperhr;
	_saved_booking_discount_rate_per_hour = _all_bookings[_booking_id].discount_price_per_hr;
});
$('body').on('click', '#delete-booking', function() {
	$('#delete-booking').val('Loading...');
	var _booking_id = $('#booking-id').val();
	var newbooking_id = $('#booking-id').val();
	var _all_bookings = $.parseJSON($('#all-bookings').val());
	var _booking_type = _all_bookings[_booking_id].booking_type;
	var serv_date = $('#servicesdate').val();
	$.post(_page_url, { action: 'check-delete-permission', serv_date: serv_date, booking_id: _booking_id }, function(response) 
	{
		if(response == "sent_request")
		{
			$("#delete-booking").parent().before('<div id="booking-action-confirm-panel" style="margin: -75px 0 0;"><textarea name="replacereason" id="replacereason" placeholder="Enter your reason..." style="margin-bottom: 5px;"></textarea></br><input type="button" data-id="'+newbooking_id+'" data-reqtype="DELETE" data-servicedate="'+serv_date+'" id="request-approval" value="Sent Request" /><input type="button" id="update-replacecancel" value="Cancel"></div>');
			$('#booking-action-confirm-panel').slideDown();
			$('#delete-booking').val('Delete');
			_refresh_page = true;
			return false;
		} else if(response == "not_approved")
		{
			alert("Your request not yet approved");
			$('#delete-booking').val('Delete');
			return false;
		} else if(response == "rejected")
		{
			alert("Your request rejected");
			$('#delete-booking').val('Delete');
			_refresh_page = true;
			return false;
		} else if(response == "cancelled")
		{
			$("#delete-booking").parent().before('<div id="booking-action-confirm-panel" style="margin: -75px 0 0;"><textarea name="replacereason" id="replacereason" placeholder="Enter your reason..." style="margin-bottom: 5px;"></textarea></br><input type="button" data-id="'+_booking_id+'" data-reqtype="DELETE" data-servicedate="'+serv_date+'" id="request-approval" value="Sent Request" /><input type="button" id="update-replacecancel" value="Cancel"></div>');
			$('#booking-action-confirm-panel').slideDown();
			$('#delete-booking').val('Delete');
			_refresh_page = true;
			return false;
		} else {
			$('#booking-action-confirm-panel').hide();
			if(_booking_type == 'OD')
			{
				var _booking_id = $('#booking-id').val();
				$.fancybox.open({
					autoCenter: true,
					fitToView: false,
					scrolling: false,
					openEffect: 'none',
					openSpeed: 1,
					helpers: {
						overlay: {
							css: {
								'background': 'rgba(0, 0, 0, 0.3)'
							},
							closeClick: true
						}
					},
					padding: 0,
					closeBtn: true,
					content: _alert_html = `<div id="alert-popup">
													<div class="head">Delete<span class="pop_close n-close-btn"></span></div>
													<div class="col-sm-12 pt-3" style="font-size: 18px;text-align: left;">Are you sure want to delete this one day booking ?</div>
													<div class="content">
													<div class="row m-0 n-delete-section-main pl-4 pt-4 pr-4">
														<span id="deleteremarks_book" style="color:red; display:none;">Please enter remarks.</span>
														<textarea name="remark" placeholder="Remarks" id="delete_yes_book_perm_remark"></textarea>
														<div class="bottom mb-4">
															<div class="col-sm-6 pl-0">
																<input type="button" value="Yes" data-bookID="` + _booking_id + `" class="n-btn red-btn mb-0 delete_yes_book_perm"/>
															</div>
															<div class="col-sm-6 pl-0">
																<input type="button" value="Cancel" class="n-btn green-btn mb-0 assign_no pop_close" />
															</div>
														</div>
													</div>
													</div>
											</div>`,
				}); 
			}
			else
			{
				var _booking_id = $('#booking-id').val();
				$.fancybox.open({
					autoCenter: true,
					fitToView: false,
					scrolling: false,
					openEffect: 'none',
					openSpeed: 1,
					helpers: {
						overlay: {
							css: {
								'background': 'rgba(0, 0, 0, 0.3)'
							},
							closeClick: true
						}
					},
					padding: 0,
					closeBtn: true,
					content: _alert_html = '<div id="alert-popup"><div class="head">Delete <span class="pop_close n-close-btn">&nbsp;</span></div><div class="content"><div class="row m-0 n-delete-section-main pl-4 pt-4 pr-4"><div class="col-sm-6 pl-0"><input type="button" value="Delete One Day" class="n-delete-set-left n-btn red-btn"></div><div class="col-sm-6 pr-0"><input type="button" value="Delete Permanently" class="n-delete-set-right n-btn red-btn de-select"></div><div class="n-delete-set-left-cont"><div class="col-sm-12 p-0 pt-3" style="font-size: 18px;text-align: left;">Are you sure want to delete it for one day?</div><span id="deleteremarks_book_day" style="color:red; display:none;"></span><div class="col-sm-12 p-0"><textarea name="remark" placeholder="Please enter remarks." id="delete_remark_day"></textarea></div><div class="col-sm-6 pl-0"><input type="button" value="Yes" data-bookID="' + _booking_id + '" class="delete_yes_book_day n-btn green-btn"/></div></div><div class="n-delete-set-right-cont"><div class="col-sm-12 p-0 pt-3" style="font-size: 18px;text-align: left;">Are you sure want to delete it for permanently?</div><span id="deleteremarks_book_day" style="color:red; display:none;"></span><div class="col-sm-12 p-0"><textarea name="remark" placeholder="Please enter remarks." id="delete_remark_perm"></textarea></div><div class="col-sm-6 pl-0"><input type="button" value="Yes" data-bookID="' + _booking_id + '" class="delete_yes_book n-btn green-btn"/></div></div></div></div></div>'
				});
				$(".n-delete-set-right").click(function () {
					$('.n-delete-set-right').removeClass('de-select');
					$('.n-delete-set-left').addClass('de-select');
					$('.n-delete-set-right-cont').show(500);
					$('.n-delete-set-left-cont').hide(500);
				});
				$(".n-delete-set-left").click(function () {
					$('.n-delete-set-left').removeClass('de-select');
					$('.n-delete-set-right').addClass('de-select');
					$('.n-delete-set-left-cont').show(500);
					$('.n-delete-set-right-cont').hide(500);
				});
			}
			$('#booking-action-confirm-panel').slideDown();
			$('#delete-booking').val('Delete');
			_refresh_page = true;
		}
	});
});
$('body').on('click', '#update-booking', function() {
	var _booking_id = $('#booking-id').val();
	var _all_bookings = $.parseJSON($('#all-bookings').val());
	var _booking_type = _all_bookings[_booking_id].booking_type;
	if(_booking_type == 'OD')
	{
		$(this).parent().before('<div id="booking-action-confirm-panel"><input type="button" id="update-permanent" value="Modify this Booking" class="n-btn purple-btn mb-0" /><input type="button" id="update-cancel" value="Cancel" class="n-btn red-btn mb-0 ml-0" /></div>');
	}
	else
	{
		$(this).parent().before('<div id="booking-action-confirm-panel"><input type="button" id="update-one-day" value="Change One Day" class="n-btn green-btn mb-0" /><input type="button" id="update-permanent" value="Change Permanently" class="n-btn purple-btn mb-0" /><input type="button" id="update-cancel" value="Cancel"  class="n-btn red-btn mb-0 ml-0" /></div>');
	}
	$('#booking-action-confirm-panel').slideDown();
});
$('body').on('click', '#delete-cancel, #update-cancel, #update-replacecancel', function() {
	$('#booking-action-confirm-panel').slideUp(function() { $('#booking-action-confirm-panel').remove(); });
});
$('body').on('click', '#delete-permanent', function() {
        var _booking_id = $('#booking-id').val();
        $.fancybox.open({
		autoCenter : true,
		fitToView : false,
		scrolling : false,
		openEffect : 'none',
		openSpeed : 1,
		helpers : {
			overlay : {
				css : {
					'background' : 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: true
			}
		},
		padding : 0,
		closeBtn : true,
			content: _alert_html = '<div id="alert-popup"><div class="head">Delete<span class="alert-popup-close pop_close"></span></div><div class="content padd20" style="padding:20px 0 5px !important;">Are you sure want to continue?</div><div class="content"><span id="deleteremarks_book" style="color:red; display:none;">Please enter remarks.</span><textarea name="remark" placeholder="Remarks" id="delete_yes_book_perm_remark"></textarea></div><div class="bottom"><input type="button" value="Yes" data-bookID="'+_booking_id+'" class="delete_yes_book_perm" style="background:#b2d157;border:1px solid" />&nbsp;&nbsp;<input type="button" value="No" class="assign_no pop_close" /></div></div>',
		topRatio : 0.2,
	}); 
});
$('body').on('click', '.delete_yes_book', function() {
    var remarks = $.trim($('#delete_remark_perm').val());
   if(remarks == "")
   {
	   $("#delete_remark_perm").focus();
        $('#deleteremarks_book').css('display','block');
   } else {
        $('#deleteremarks_book').css('display','none');
        $.fancybox.close();
	 var _booking_id=$(this).attr('data-bookID');
	$.post(_page_url, { action: 'delete-booking-permanent', booking_id: _booking_id , remarks : remarks}, function(response) {
		_bpop_open = false;
		refresh_grid();
		var _alert_html = '';
		if(response == 'success')
		{
			$('#alert-title').html('Success !');
			$('#alert-message').html('Booking has been deleted successfully.');
			fancybox_show('alert-popup');
			_refresh_page = false
		}
		else
		{
                        if(response == 'locked')
                        {
							$('#alert-title').html('Error !');
							$('#alert-message').html('This booking is locked by another user.');
							fancybox_show('alert-popup');
                                _refresh_page = false;
						} else if (response == 'ongoing') {
							$('#alert-title').html('Error !');
							$('#alert-message').html('Please complete ongoing services and try again.');
							fancybox_show('alert-popup');
							_refresh_page = false;
						}
						else if (response == 'odoorefresh') {
							$('#alert-title').html('Error !');
							$('#alert-message').html('Delete Failed. Booking alreday invoiced.');
							fancybox_show('alert-popup');
							_refresh_page = false;
						} else {
							$('#alert-title').html('Error !');
							$('#alert-message').html('An unexpected error. Please try again.');
							fancybox_show('alert-popup');
                                _refresh_page = true;
                        }
		}
	});
    }
});
$('body').on('click', '.delete_yes_book_perm', function () {
	var remarks = $.trim($('#delete_yes_book_perm_remark').val());
	if (remarks == "") {
		$('#deleteremarks_book').css('display', 'block');
	} else {
		$('#deleteremarks_book').css('display', 'none');
		$.fancybox.close();
		var _booking_id = $(this).attr('data-bookID');
		$.post(_page_url, { action: 'delete-booking-permanent', booking_id: _booking_id, remarks: remarks }, function (response) {
			_bpop_open = false;
			refresh_grid();
			var _alert_html = '';
			if (response == 'success') {
				$('#alert-title').html('Success !');
				$('#alert-message').html('Booking has been deleted successfully.');
				fancybox_show('alert-popup');
				_refresh_page = false
			}
			else {
				if (response == 'locked') {
					$('#alert-title').html('Error !');
					$('#alert-message').html('This booking is locked by another user.');
					fancybox_show('alert-popup');
					_refresh_page = false;
				}
				else if (response == 'ongoing') {
					$('#alert-title').html('Error !');
					$('#alert-message').html('Please complete ongoing services and try again.');
					fancybox_show('alert-popup');
					_refresh_page = false;
				} else if (response == 'odoorefresh') {
					$('#alert-title').html('Error !');
					$('#alert-message').html('Delete Failed. Booking alreday synched to odoo.');
					fancybox_show('alert-popup');
					_refresh_page = false;
				} else {
					$('#alert-title').html('Error !');
					$('#alert-message').html('An unexpected error. Please try again.');
					fancybox_show('alert-popup');
					_refresh_page = true;
				}
			}
		});
	}
});
$('body').on('click', '#delete-one-day', function()
{
    var _booking_id = $('#booking-id').val();
    $.fancybox.open({
            autoCenter : true,
            fitToView : false,
            scrolling : false,
            openEffect : 'none',
            openSpeed : 1,
            helpers : {
                    overlay : {
                            css : {
                                    'background' : 'rgba(0, 0, 0, 0.3)'
                            },
                            closeClick: true
                    }
            },
            padding : 0,
            closeBtn : true,
		content: _alert_html = '<div id="alert-popup"><div class="head">Delete <span class="pop_close n-close-btn">&nbsp;</span></div><div class="content"><div class="row m-0 n-delete-section-main pl-4 pt-4 pr-4"><div class="col-sm-6 pl-0"><input type="button" value="Delete One Day" class="n-delete-set-left n-btn red-btn"></div><div class="col-sm-6 pr-0"><input type="button" value="Delete Permanently" class="n-delete-set-right n-btn red-btn de-select"></div><div class="n-delete-set-left-cont"><div class="col-sm-12 p-0 pt-3" style="font-size: 18px;text-align: left;">Are you sure want to delete it for one day?</div><span id="deleteremarks_book_day" style="color:red; display:none;"></span><div class="col-sm-12 p-0"><textarea name="remark" placeholder="Please enter remarks." id="delete_remark_day"></textarea></div><div class="col-sm-6 pl-0"><input type="button" value="Yes" data-bookID="' + _booking_id + '" class="delete_yes_book_day n-btn green-btn"/></div></div><div class="n-delete-set-right-cont"><div class="col-sm-12 p-0 pt-3" style="font-size: 18px;text-align: left;">Are you sure want to delete it for permanently?</div><span id="deleteremarks_book_day" style="color:red; display:none;"></span><div class="col-sm-12 p-0"><textarea name="remark" placeholder="Please enter remarks." id="delete_remark_perm"></textarea></div><div class="col-sm-6 pl-0"><input type="button" value="Yes" data-bookID="' + _booking_id +'" class="delete_yes_book n-btn green-btn"/></div></div></div></div></div>',
            topRatio : 0.2,
    }); 
	$(".n-delete-set-right").click(function(){
	 	$('.n-delete-set-right').removeClass('de-select');
	 	$('.n-delete-set-left').addClass('de-select');
		$('.n-delete-set-right-cont').show(500);
		$('.n-delete-set-left-cont').hide(500);
    });
	$(".n-delete-set-left").click(function(){
	 	$('.n-delete-set-left').removeClass('de-select');
	 	$('.n-delete-set-right').addClass('de-select');
		$('.n-delete-set-left-cont').show(500);
		$('.n-delete-set-right-cont').hide(500);
    });
});
$('body').on('click', '.delete_yes_book_day', function() {
    var remarks = $.trim($('#delete_remark_day').val());
   if(remarks == "")
   {
	   $("#delete_remark_day").focus();
        $('#deleteremarks_book_day').css('display','block');
   } else {
        $('#deleteremarks_book_day').css('display','none');
        $.fancybox.close();
        var _booking_id=$(this).attr('data-bookID');
	$.post(_page_url, { action: 'delete-booking-one-day', booking_id: _booking_id, remarks : remarks }, function(response) {
		_bpop_open = false;
		refresh_grid();
		var _alert_html = '';
		if(response == 'success')
		{
			$('#alert-title').html('Success !');
			$('#alert-message').html('Booking has been deleted successfully.');
			fancybox_show('alert-popup');
			_refresh_page = false
		}
		else
		{
                        if(response == 'locked')
                        {
							$('#alert-title').html('Error !');
							$('#alert-message').html('This booking is locked by another user.');
							fancybox_show('alert-popup');
                                _refresh_page = true;
                        }
						else if (response == 'ongoing') {
							$('#alert-title').html('Error !');
							$('#alert-message').html('Please complete ongoing services and try again.');
							fancybox_show('alert-popup');
							_refresh_page = false;
						}
						else if(response == 'odoorefresh')
                        {
							$('#alert-title').html('Error !');
							$('#alert-message').html('Delete failed. Booking already invoiced.');
							fancybox_show('alert-popup');
                                _refresh_page = true;
                        }
                        else
                        {
							$('#alert-title').html('Error !');
							$('#alert-message').html('An unexpected error. Please try again.');
							fancybox_show('alert-popup');
                                _refresh_page = true;
                        }
		}
	});
    }
});
$('#tb-slide-right').click(function() {
	var position = $('.time_line .time_slider').position();
	var max_left = time_grid_width - 3415 + 3 + 5;
	var left_pos = position.left - 200 + 5;
	left_pos = left_pos < max_left ? max_left : left_pos;
	$('.time_line .time_slider').animate({left: left_pos}, 200);
	$('.grids').animate({left: left_pos }, 200);
});
$('#tb-slide-left').click(function() {
	var position = $('.time_line .time_slider').position();
	var left_pos = position.left + 200 + 5;
	left_pos = left_pos > 0 ? 0 : left_pos;
	$('.time_line .time_slider').animate({left: left_pos }, 200);
	$('.grids').animate({left: left_pos }, 200);
});
var fixed_service_rate = false;
$('body').on('change', '#b-service-type-id', function() {
    var service_id = $("#b-service-type-id").val();
    var option_type = $('option:selected', this).attr('data-type');
    var price_type = $('option:selected', this).attr('data-pricetype');
    var servicecostval = $('option:selected', this).attr('data-cost');
    $('#servicetypeval').val(option_type);
    $('#service_pricetype').val(price_type);
    $('#servicecostval').val(servicecostval);
    if(option_type == "C")
    {
		fixed_service_rate = true;
		$('#b-cleaning-materials').prop('checked', false).attr('disabled', true);
        $('#service-panel').slideDown();
        $.post(_page_url, { action: 'get-related-services', service_id: service_id }, function(response) 
        {
            $('#service-panel .inner').html(response);
        });
    } else {
		fixed_service_rate = false;
		$('#b-cleaning-materials').attr('disabled', false);
        $('#squarefeetval').val('');  
        $("#sqrbtnshow").hide();
        calculate_cost();
    }
});
$('body').on('change', '#b-from-time', function() {
	refresh_to_time();
	$('#b-time-slot').text('');
});
function refresh_to_time() {
	$("#b-to-time").val('');
	var _selected_index = $("#b-from-time")[0].selectedIndex;
	var _time_to_options = '<option></option>';
	var _i = 0;
        var _last_index;
	$('#b-from-time option').each(function(index, option) {
		if(index > _selected_index)
		{
			_time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
                        _last_index = index;
			_i++;
		}
	});
	if(_i == 0)
	{
		_time_to_options += '<option value="">No Time</option>';
	}
	$('#b-to-time').html(_time_to_options);
}
$('body').on('change', '#b-to-time', function () {
	$('#b-rate_per_hr').trigger('input');
	return;
	if ($("#b-from-time").val() != '' && $("#b-to-time").val() != '') {
		_start_time = parseAMDate($("#b-from-time option:selected").text().toUpperCase());
		_end_time = parseAMDate($("#b-to-time option:selected").text().toUpperCase());
		if (_end_time < _start_time) {
			_end_time = parseAMDate(_end, 1);
		}
		var difference = _end_time - _start_time;
		var hours = Math.floor(difference / 36e5),
			minutes = Math.floor(difference % 36e5 / 60000);
		if (parseInt(hours) >= 0) {
			if (minutes == 0) {
				minutes = "00";
			}
			var weekdayscount = $('#total_week_days').val();
			var customer_pre_book_hrs = $('#total_prebook_hrs').val();
			$('#b-hrs_per_week').val(parseInt(customer_pre_book_hrs) + parseInt(hours * weekdayscount));
		}
		$('#b-time-slot').text('(' + $("#b-from-time option:selected").text() + ' to ' + $("#b-to-time option:selected").text() + ')');
		var _booking_id = $('#booking-id').val();
		if (_booking_id != '') {
			chk_booking_chg(_booking_id);
		}
	} else {
		$('#b-time-slot').text('');
	}
	calculate_cost();
});
$('body').on('keyup', '#b-rate_per_hr', function() 
{
	return;
	calculate_cost();
	var _booking_id = $('#booking-id').val();
	if (_booking_id != '')
	{
		chk_booking_chg(_booking_id);
	}
});
  function parseAMDate(input, next_day) {
        var dateReg = /(\d{1,2}):(\d{2})\s*(AM|PM)/;
        var hour, minute, result = dateReg.exec(input);
        if (result) {
            hour = +result[1];
            minute = +result[2];
            if (result[3] === 'PM' && hour !== 12) {
                hour += 12;
            }
        }
        if (!next_day) {
            return new Date(1970, 01, 01, hour, minute).getTime();
        }else{
            return new Date(1970, 01, 02, hour, minute).getTime();
        }
    }

//=====================================================================
/**====================================================================
 * Function to set values on Customer dropdown selection
 ======================================================================*/
$("body").on("change", "#b-customer-id", function () {
	var _customer_id = $(this).val();
	$("#customer-picked-address").html("");
	get_no_of_address(_customer_id);
	get_pending_amount(_customer_id);
	//Applying spectial discount based on total weekly book hrs starts
	if (_customer_id > 0) {
		$.ajax({
			type: "POST",
			url: _base_url + "booking/customer_weekly_booking_hrs",
			data: { customer_id: _customer_id },
			cache: false,
			success: function (response) {
				$("#total_prebook_hrs").val(response); // setting pre book hours for calculation
				$("#total_week_days").val($(".w_day:checked").length);
				_start_time = parseAMDate($("#b-from-time option:selected").text().toUpperCase());
				_end_time = parseAMDate($("#b-to-time option:selected").text().toUpperCase());
				if (_end_time < _start_time) {
					_end_time = parseAMDate(_end, 1);
				}
				var difference = _end_time - _start_time;
				var hours = Math.floor(difference / 36e5),
				minutes = Math.floor((difference % 36e5) / 60000);
				var customer_pre_book_hrs = $("#total_prebook_hrs").val();
				$("#b-hrs_per_week").val(parseInt(customer_pre_book_hrs) + parseInt(hours));
				$.ajax({
					type: "POST",
					url: _base_url + "settings/check_hourly_price_new",
					data: { customer_id: _customer_id },
					cache: false,
					success: function (response) {
						if (response == "") {
							response = "";
						}
						if (response != null) {
							$("#b-rate_per_hr").val(response);
							$("#discount_rate_perhr").val(response);
							$("#b-rate_per_hr_cust").val(response);
							calculate_cost();
						}
						setAmount();
					},
				});
			},
		});
	} else {
		$("#total_prebook_hrs").val("0");
	}
});
//=====================================================================
/**====================================================================
 * 
 ======================================================================*/
$('body').on('keyup', '#squarefeetval', function() 
{  
    calculate_cost();
});
$('body').on('keyup', '#discount_rate_perhr', function() 
{  
	return;
	var _booking_id = $('#booking-id').val();
	if(_booking_id != '')
	{
		chk_booking_chg(_booking_id);
	}
    calculate_cost();
});
function calculate_cost(amount='')
{
	if($("#b-from-time").val() != '' && $("#b-to-time").val() != '')
	{
		_start_time = parseAMDate($("#b-from-time option:selected").text().toUpperCase());
        _end_time   = parseAMDate($("#b-to-time option:selected").text().toUpperCase());
        if (_end_time < _start_time) {
            _end_time = parseAMDate(_end, 1);
        }
        var difference = _end_time - _start_time;
        var hours = (difference / 36e5),
          minutes = (difference % 36e5 / 60000);
        if (parseInt(hours) >= 0) {
            if (minutes == 0) {
                minutes = "00";
			}
		} 
		var num_week_checked=1;
		var service_pricetype = $("#service_pricetype").val();
		var service_type = $("#servicetypeval").val();
		var service_id = $("#b-service-type-id").val();
		var servicecost = $("#servicecostval").val();
		if(service_type == 'H')
		{
			var price_per_hr=$("#b-rate_per_hr").val();
			if ($('#discount_rate_perhr').val() == "" || $('#discount_rate_perhr').val() == 0){
				$("#discount_rate_perhr").val(price_per_hr);
			}
			var discountrateperhr = $("#discount_rate_perhr").val() || $('#b-rate_per_hr').val()|| $("#b-rate_per_hr_cust").val();
			if(price_per_hr != "")
			{
			} else {
				if(service_pricetype == 'SP')
				{
					$("#b-rate_per_hr").val(servicecost);
					var price_per_hr = servicecost;
				} else {
					$("#b-rate_per_hr").val($("#b-rate_per_hr_cust").val());
					var price_per_hr = $("#b-rate_per_hr_cust").val();
				}
			}
			if($("#repeat-days").is(":visible"))
			{
				num_week_checked=$('input[name="w_day[]"]:checked').length;
				num_week_checked=num_week_checked >0 ? num_week_checked: 1;
			}
			num_week_checked=1;
			$('#cleaning-materials-charge').val(_serviceMaterialCost.toFixed(2));
			$('#supervisor-charge').val(_supervisorCost.toFixed(2));
			if(discountrateperhr != "")
			{
				if($('#supervisor_selected').is(':checked')==true)
				{
					var supercost = (hours*60*num_week_checked);
				} else {
					var supercost = 0;
				}
				
				
				if($('#b-cleaning-materials').is(':checked')==true)
				{
					var discountcost = (hours*discountrateperhr*num_week_checked)+(hours*10*num_week_checked)+supercost;
				} else {
					var discountcost = (hours * discountrateperhr*num_week_checked)+supercost;
				}
			}
			if(amount!="")
			{
				cost=amount;
			}
			var total_costs = (parseFloat(cost));
			var total_costs = total_costs.toFixed(2);
			if(discountrateperhr != "")
			{
				var discounttotal_costs = (parseFloat(discountcost));
				var discounttotal_costs = discounttotal_costs.toFixed(2);
				$("#tot_amout").val(discounttotal_costs);
				/*************************************** */
				$('#service_vat_amount').val(((service_vat_percentage / 100) * discounttotal_costs).toFixed(2));
				// $('#service_taxed_total').val(Number($("#tot_amout").val()) + Number($('#service_vat_amount').val())+_serviceMaterialCost+_supervisorCost);
				$('#service_taxed_total').val(Number($("#tot_amout").val()) + Number($('#service_vat_amount').val()));
				/*************************************** */
				var discountedamt = (total_costs - discounttotal_costs);
				$('#b-discount').val(discountedamt);
			} else {
				$("#tot_amout").val(total_costs);
				$('#b-discount').val(0);
			}
			$("#serviceamountcost").val(total_costs);
		} else {
			if($("#flatrateval").val() == 'N')
			{
				var sqftcost = $('#servicecostval').val();
				var nu_of_fts = $('#squarefeetval').val();
				var cost = sqftcost * nu_of_fts;
				var total_costs = (parseFloat(cost));
				var total_costs = total_costs.toFixed(2);
				$("#tot_amout").val(total_costs);
				$("#serviceamountcost").val(total_costs);
				$("#sqftcount").val(nu_of_fts);
			}
			else
			{
				var price_per_hr=$("#b-rate_per_hr_cust").val();
				if($("#repeat-days").is(":visible"))
				{
					num_week_checked=$('input[name="w_day[]"]:checked').length;
					num_week_checked=num_week_checked >0 ? num_week_checked: 1;
				}
				if($('#b-cleaning-materials').is(':checked')==true)
				{
					var cost = (hours*price_per_hr*num_week_checked)+(hours*10*num_week_checked);
				} else {
					var cost = hours * price_per_hr*num_week_checked;
				}
				if(amount!="")
				{
					cost=amount;
				}
				var total_costs = (parseFloat(cost));
				var total_costs = total_costs.toFixed(2);
				$("#tot_amout").val(total_costs);
				$("#serviceamountcost").val(total_costs);
			}
		}
	}
	_saved_booking_rate_per_hour = $('#b-rate_per_hr').val();
}
function get_pending_amount(customer_id)
{
$.post(_page_url, { action: 'get-customer-pending-amount', customer_id: customer_id }, function(response) {
    var _response = $.parseJSON(response);
    if(_response[0].balance!=0)
        {
    if(_response[0].signed=="Cr")
        {
            var color="#081775";
        }
        else
            {
              var color="#980407";   
            }
   $("#b-pending-amount").html(_response[0].balance + _response[0].signed).css("color",color);
        }
        else
            {
           $("#b-pending-amount").html('0.00');     
            }
});    
}
function get_no_of_address(customer_id)
{
   $.post(_page_url, { action: 'get-no-of-customer-address', customer_id: customer_id }, function(response) {
			if(response == 'refresh')
			{
				$.fancybox.open({
					autoCenter : true,
					fitToView : false,
					scrolling : false,
					openEffect : 'fade',
					openSpeed : 100,
					helpers : {
						overlay : {
							css : {
								'background' : 'rgba(0, 0, 0, 0.3)'
							},
							closeClick: false
						}
					},
					padding : 0,
					closeBtn : false,
					content:  '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>'
				});
				_refresh_page = true;
			}
			else
			{
                                //customer details on right side
                                $.post(_page_url, { action: 'get-details-customer', customer_id: customer_id }, function(response) {
                                    var _respval = $.parseJSON(response);
                                    if(_respval.payment_type == "D")
                                    {
                                        var paytype = "Daily";
                                    } else if(_respval.payment_type == "W")
                                    {
                                        var paytype = "Weekly";
                                    } else if(_respval.payment_type == "M")
                                    {
                                        var paytype = "Monthly";
                                    } else {
                                        var paytype = "";
                                    }
                                    if(_respval.customer_booktype == '0')
                                    {
                                        var cus_booktype = "Non Regular";
                                    } else if(_respval.customer_booktype == '1')
                                    {
                                        var cus_booktype = "Regular";
                                    } else {
                                        var cus_booktype = "";
                                    }
                                    $(".payment_mode_customer").show();
                                    $('#b-customer-ids-cell').text(_respval.customer_name);
                                    $('#b-customer-mobile-cell').text(_respval.mobile_number_1);
                                    $('#b-customer-email-cell').text(_respval.email_address);
                                    $('#b-customer-paytype-cell').text(paytype);
                                    $('#b-customer-booktype-cell').text(cus_booktype);
                                    $('#b-customer-notes-cell').text(_respval.customer_notes);
                                    $('#b-customer-paymode-cell').val(_respval.payment_mode);
                                    $('#customer_flag_val').val(_respval.is_flag);
                                    $('#customer_flag_reason').val(_respval.flag_reason);
									if(_respval.is_flag == "Y")
									{ 
										if(_respval.flag_reason != "")
										{
											var flagreason = " ("+_respval.flag_reason+")"; 
										} else {
											var flagreason = "";
										}
										$('#b-error').text('Customer is flagged.'+flagreason);
									} else {
										$('#b-error').text('');
									}
                                    $(".fancybox-skin").css("width","100%").css("margin","0 auto");
                                    $(".fancybox-outer").css("width","100%").css("margin","0 auto");
                                    $(".fancybox-inner").css("width","100%").css("margin","0 auto");
                                    $(".fancybox-wrap").css("width","80%").css("margin","0 auto").css("left","10%");
                                    $("#customer-add-popup").hide();
                                    $("#customer-detail-popup").show();
                                    $("#popup-booking").removeClass("col-md-12 col-sm-12");
									$("#popup-booking").addClass("col-md-8 col-sm-7");
                                });
                                //Ends
				var _resp = $.parseJSON(response);				
				var _address_html = '<div class="table">';
                                if(_resp.no_of_address == 1)
                                {
                                    $.each(_resp.address, function(key, val) {
                                        var _address_id = val.customer_address_id;
                                        $('#customer-address-id').val(_address_id);
                                        var _addzone = '<strong>' + val.zone_name + ' - ' + val.area_name + '</strong>';
                                        var _address = val.customer_address;
                                        $('#customer-address-panel').slideUp(function() {
                                                $('#b-customer-area-cell').text(val.area_name);
                                                $('#b-customer-zone-cell').text(val.zone_name);
                                                $('#b-customer-address-cell').text(_address);
                                                $('#b-customer-apartment-no').text(val.building);
                                                $('#customer-address-panel .inner').html('Loading<span class="dots_loader"></span>');
                                                var _booking_id = $('#booking-id').val();
                                                if(_booking_id != '')
                                                {
                                                        chk_booking_chg(_booking_id);
                                                }
                                        });
                                    });
                                }
                                else
                                {
                                    open_address_panel(customer_id)
                                }
			}
		}); 
}
function open_address_panel(customer_id)
{
	$('#customer-address-id').val('');
	$('#customer-address-panel').hide();
	$('#customer-address-panel .inner').html('Loading<span class="dots_loader"></span>');
	if($.isNumeric(customer_id) && customer_id > 0)
	{	
		$('#customer-address-panel').slideDown();
		$.post(_page_url, { action: 'get-customer-address', customer_id: customer_id }, function(response) {
			if(response == 'refresh')
			{
				$.fancybox.open({
					autoCenter : true,
					fitToView : false,
					scrolling : false,
					openEffect : 'fade',
					openSpeed : 100,
					helpers : {
						overlay : {
							css : {
								'background' : 'rgba(0, 0, 0, 0.3)'
							},
							closeClick: false
						}
					},
					padding : 0,
					closeBtn : false,
					content:  '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>'
				});
				_refresh_page = true;
			}
			else
			{
				var _resp = $.parseJSON(response);				
				var _address_html = '<div class="table" style="border-top: 0px;">';                                
                                $.each(_resp, function(key, val) {
                                    _address_html += '<div class="row n-pick-maid-c-main m-0"><div class="col-sm-8 n-pick-maid-left pl-0"><span id="caddzone-' + val.customer_address_id + '"><strong>' + val.zone_name + ' ' + val.area_name + '</strong></span><br /><span id="cadd-' + val.customer_address_id + '">' + val.customer_address + '</span></div><div class="col-sm-4 n-pick-maid-right pr-0"><input type="button" value="Pick" id="cadddress-' + val.customer_address_id + '" class="pick_customer_address"  /></div></div>';
                                });				
                                _address_html += '</div>';
                                $('#customer-address-panel .inner').html(_address_html);
			}
		});
	}
}
$('body').on('click', '.pick_customer_address', function() {
	var _address_id = $(this).attr('id').replace('cadddress-', '');
	/*************************************** */
	// code snippet added by Samnad. S on 23-03-2023
	// for updating popup righside address details on pick a single address from multiple list
	$.get(_base_url + 'customer/get-address-by-id/' + _address_id, function (data) {
		$('#b-customer-area-cell').html(data.area_name);
		$('#b-customer-zone-cell').html(data.zone_name);
		$('#b-customer-address-cell').html(data.customer_address);
		$('#b-customer-apartment-no').html(data.building);
	});
	/*************************************** */
	$('#customer-address-id').val(_address_id);
	var _addzone = $('#caddzone-' + _address_id).html();
	var _address = $('#cadd-' + _address_id).html();
	$('#customer-address-panel').slideUp(function() {
		$('#customer-picked-address').html('<div class="address"><strong>' + _addzone + '</strong> - ' + _address + '</div><div class="action"><span id="chg-cust-address">Change</span></div><div class="clear"></div>');
		$('#customer-picked-address').show();
		$('#customer-address-panel .inner').html('Loading<span class="dots_loader"></span>');
		var _booking_id = $('#booking-id').val();
		if(_booking_id != '')
		{
			chk_booking_chg(_booking_id);
		}
	});
});
$('body').on('click', '#lock-booking', function() {
	var _booking_id = $('#booking-id').val();
	if(_booking_id != '')
	{
		chk_booking_chg(_booking_id);
	}
});
$('body').on('click','#notification-sms-booking', function() {
    var sms_notifications = $('#notification-sms-booking').is(':checked') ? 'Y' : 'N';
    var _booking_id = $('#booking-id').val();
    if(_booking_id != '')
    {
            chk_booking_chg(_booking_id);
    }
});
$('body').on('click', '.popup-main-box .notification-sms-booking', function() {
    var sms_val = $('.popup-main-box input[name=notification_smss_booking]:checked').val();
    if(sms_val == 'N')
    {
        $('.popup-main-box .sms_calender').css('display','none');
    } else if(sms_val == 'L'){
        $('.popup-main-box .sms_calender').css('display','block');
    }
});
$('body').on('click', '.popup-main-box #notification-email-booking', function() {
    var _booking_id = $('#booking-id').val();
    if(_booking_id != '')
    {
            chk_booking_chg(_booking_id);
    }
});
$('body').on('click', '#b-cleaning-materials', function() {
	return;
	var _booking_id = $('#booking-id').val();
	if(_booking_id != '')
	{
		chk_booking_chg(_booking_id);
	}
	calculate_cost();
});
$('body').on('keyup', '#booking-note', function() {
	var _booking_id = $('#booking-id').val();
	if(_booking_id != '')
	{
		chk_booking_chg(_booking_id);
	}
});
$('body').on('keyup', '#tot_amout', function() {
	var _booking_id = $('#booking-id').val();
	if(_booking_id != '')
	{
		chk_booking_chg(_booking_id);
	}
});
$('body').on('keyup', '#b-discount', function() {
	var _booking_id = $('#booking-id').val();
	if(_booking_id != '')
	{
		chk_booking_chg(_booking_id);
	}
});
$('body').on('keyup', '#b-customer-paymode-cell', function() {
    var _booking_id = $('#booking-id').val();
    if(_booking_id != '')
    {
        chk_booking_chg(_booking_id);
    }
});
$('body').on('click', '#request-approval', function() {
	$(this).attr('disabled', true);
	var _alert_html = '';
	var _booking_id=$(this).attr('data-id');
	var service_date=$(this).attr('data-servicedate');
	var requesttype=$(this).attr('data-reqtype');
	var reason = $("#replacereason").val();
	if(reason == "")
	{
		$("#replacereason").css('border','1px solid red');
		return false;
	} else {
		$("#replacereason").css('border','1px solid #cccccc');
	}
	$.post(_page_url, { action: 'add-replace-request', booking_id: _booking_id, service_date: service_date, reason: reason, reqtype: requesttype}, function(response) 
	{
		if(response=='success')
		{
			_alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Request sent successfully.</div><div class="bottom"><input type="button" value="OK" class="pop_close" /></div></div>';
			_refresh_page = false;
			$("#request-approval").removeAttr('disabled');
			refresh_grid();
		}
		else
		{
			_alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">Something went wrong. Try again...</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
		   _refresh_page = false;
		   $("#request-approval").removeAttr('disabled');
		}
		if(_alert_html  != '')
		{
			$.fancybox.open({
				autoCenter : true,
				fitToView : false,
				scrolling : false,
				openEffect : 'fade',
				openSpeed : 100,
				helpers : {
					overlay : {
						css : {
							'background' : 'rgba(0, 0, 0, 0.3)'
						},
						closeClick: false
					}
				},
				padding : 0,
				closeBtn : false,
				content: _alert_html
			});
		}
		refresh_grid();
	});
});
$('body').on('change', 'input[name="repeat_end"]', function() {
	if($('#repeat-end-never').is(':checked'))
	{
		$('#repeat-end-date').val('');
	}
	var _booking_id = $('#booking-id').val();
	if(_booking_id != '')
	{
		chk_booking_chg(_booking_id);
	}
});
$('body').on('change', '#repeat-end-date', function() {
	var _booking_id = $('#booking-id').val();
	if(_booking_id != '')
	{
		chk_booking_chg(_booking_id);
	}
});
$('body').on('keyup', '#b-customer-reference-cell', function() {
	var _booking_id = $('#booking-id').val();
	if(_booking_id != '')
	{
		chk_booking_chg(_booking_id);
	}
});
$('body').on('change', '#tot_amout', function () {
});
function chk_booking_chg(booking_id)
{
	if($.isNumeric(booking_id) && booking_id > 0)
	{
		var _all_bookings = $.parseJSON($('#all-bookings').val());
		var _booking_locked = $('#lock-booking').is(':checked') ? 1 : 0;
		var _cleaning_material = $('#b-cleaning-materials').is(':checked') ? 'Y' : 'N';
		var _supervisor = $('#supervisor_selected').is(':checked') ? 'Y' : 'N';
		var _pending_amount = $.trim($('#b-pending-amount').val());
		var _discount = $.trim($('#b-discount').val());
		var _total_amt = $.trim($('#tot_amout').val());
		var _booking_note = $.trim($('#booking-note').val());
		var _email_notifications = $('#notification-email-booking').is(':checked') ? 'Y' : 'N';
		var sms_notifications = $('#notification-sms-booking').is(':checked') ? 'Y' : 'N';
		if(_all_bookings[booking_id].booking_type != 'OD')
		{
			var _repeat_end = $('#repeat-end-never').is(':checked') ? 0 : 1;
			if(_repeat_end == 1)
			{
				var _repeat_end_date = $('#repeat-end-date').val();
			}
		}
		if(_all_bookings[booking_id].customer_address_id != $('#customer-address-id').val() || _all_bookings[booking_id].time_from_stamp != $('#b-from-time').val() || _all_bookings[booking_id].is_locked != _booking_locked || _all_bookings[booking_id].cleaning_type != _cleaning_material || _all_bookings[booking_id].supervisor_type != _supervisor || _all_bookings[booking_id].time_to_stamp != $('#b-to-time').val() || _all_bookings[booking_id].pending_amount != _pending_amount || _all_bookings[booking_id].discount != _discount || _all_bookings[booking_id].booking_note != _booking_note || (typeof _repeat_end != 'undefined' && _all_bookings[booking_id].service_end != _repeat_end) || (typeof _repeat_end_date != 'undefined' && _all_bookings[booking_id].service_end_date != _repeat_end_date) || _email_notifications == 'Y' || sms_notifications == 'Y')
		{
			if($('#update-booking').length == 0)
			{
				$('.n-popup-btn-main').prepend('<input type="button" class="save-but purple-btn" id="update-booking" value="Update" />');
			}
			if($('#copy-booking').length == 0)//else
			{
				$('.n-popup-btn-main').prepend('<input type="button" class="copy-but" id="copy-booking" value="Copy" />');//Updated by Geethu
			}
		}
		else
		{
			$('#update-booking').remove();
			//Updated by Geethu
			if($('#copy-booking').length == 0)
			{
				$('.n-popup-btn-main').prepend('<input type="button" class="copy-but" id="copy-booking" value="Copy" />');
			}
			//End
		}
	}
	$('#tot_amout').trigger('change');
}
$('body').on('click', '#chg-cust-address', function() {
	var _customer_id = $('#b-customer-id').val();
	$('#customer-picked-address').hide();
	open_address_panel(_customer_id);
});
$('body').on('click', '#transfer-driver', function() {
	var _booking_id = $('#booking-id').val();
	var serv_date = $('#servicesdate').val();
	var hiddentabletid = $('#hiddentabletid').val();
	open_drivers_panel(_booking_id, serv_date, hiddentabletid);
});
function open_drivers_panel(_booking_id, serv_date, hiddentabletid)
{
	if($.isNumeric(_booking_id) && _booking_id > 0)
	{	
		$('#drivers-panel').slideDown();
		$.post(_page_url, { action: 'get-all-drivers', hiddentabletid: hiddentabletid }, function(response) {
			if(response == 'refresh')
			{
				$.fancybox.open({
					autoCenter : true,
					fitToView : false,
					scrolling : false,
					openEffect : 'fade',
					openSpeed : 100,
					helpers : {
						overlay : {
							css : {
								'background' : 'rgba(0, 0, 0, 0.3)'
							},
							closeClick: false
						}
					},
					padding : 0,
					closeBtn : false,
					content:  '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">No drivers found.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>'
				});
				_refresh_page = true;
			}
			else
			{            
				var _resp = $.parseJSON(response);
                var _maid_html = '<div class="table">';
				_maid_html += '<div class="row"><div class="cell1">' + _resp.html + '</div><div class="cell2"><input type="button" value="Transfer &raquo;" id="mdriver_' + hiddentabletid + '_' + _booking_id + '_' + serv_date + '" class="transfer_driver"  /></div></div>';
				_maid_html += '</div>';
				$('#drivers-panel .inner').html(_maid_html);                
			}
		});
	}
}
$('body').on('click', '.transfer_driver', function() 
{
	$.fancybox.close();
	$('.mm-loader').show();
	var _booking_details = $(this).attr('id').replace('mdriver_','').split('_');
	var _current_tablet_id = $.trim(_booking_details[0]);
	var _booking_id = $.trim(_booking_details[1]);  
	var _service_date = $.trim(_booking_details[2]);
	var _to_tablet_id = $('#transferdriverselect').val();
	_refresh_page = false;
	$.post( _page_url, { action: 'transfer-driver', booking_id: _booking_id, cur_tablet_id: _current_tablet_id, service_date: _service_date, to_tablet_id: _to_tablet_id}, function(response) {
		_bpop_open = false;
		var _alert_html = '';
		if(response == 'error')
		{
			$('#alert-title').html('Error !');
			$('#alert-message').html('An unexpected error. Please try again.');
			fancybox_show('alert-popup');
			_refresh_page = true;
		} else if(response == 'locked')
		{
			$('#alert-title').html('Error !');
			$('#alert-message').html('This booking is locked by another user.');
			fancybox_show('alert-popup');
				_refresh_page = false;
		} else {
			if(response == 'success')
			{	
				$('#alert-title').html('Success !');
				$('#alert-message').html('Booking transferred successfully.');
				fancybox_show('alert-popup');
				_refresh_page = false
			}
			else
			{
				$('#alert-title').html('Error !');
				$('#alert-message').html('An unexpected error. Please try again.');
				fancybox_show('alert-popup');
				_refresh_page = true
			}
		}
		refresh_grid();
		$('.mm-loader').hide();
	});
});
//Edited by geethu
$('body').on('click', '#copy-booking', function() {
	var _booking_id = $('#booking-id').val();
	var serv_date = $('#servicesdate').val();
	open_maid_panel(_booking_id, serv_date);
});
function open_maid_panel(_booking_id, serv_date)
{
	if($.isNumeric(_booking_id) && _booking_id > 0)
	{	
		$('#maids-panel').slideDown();
		$('input[name="same_zone"]').attr('onclick', 'open_maid_panel(' + _booking_id + ')');
		$('#maids-panel .inner').html('<div class="inner">Loading<span class="dots_loader"></span></div>');        
		$.post(_page_url, { action: 'get-free-maids', booking_id: _booking_id, same_zone : $('input[name="same_zone"]:checked').val(), service_date: serv_date }, function(response) {
			if(response == 'refresh')
			{
				$.fancybox.open({
					autoCenter : true,
					fitToView : false,
					scrolling : false,
					openEffect : 'fade',
					openSpeed : 100,
					helpers : {
						overlay : {
							css : {
								'background' : 'rgba(0, 0, 0, 0.3)'
							},
							closeClick: false
						}
					},
					padding : 0,
					closeBtn : false,
					content:  '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>'
				});
				_refresh_page = true;
			}
			else
			{
				var _resp = $.parseJSON(response);
                                if(typeof _resp.status && _resp.status == 'error')
                                {
                                        $('#maids-panel .inner').css("overflow","hidden");
                                        $('#maids-panel .inner').html('<div id="c-error">' + _resp.message + '</div>');
                                } 
                                else
                                {
                                    var _maid_html = '<div class="table">';
                                    var i = 0;
                                    $.each(_resp, function(key, val) {
                                            _maid_html += '<div class="row n-pick-maid-c-main m-0"><div class="col-sm-8 n-pick-maid-left pl-0"><span id="mcopyadd-' + val.maid_id + '"><strong>' + val.maid_name + '</strong></span><br /><span id="mcopy-' + val.maid_id + '">' + val.maid_nationality + '</span></div><div class="col-sm-4 n-pick-maid-right pr-0"><input type="button" value="Book" id="mcopy_' + val.maid_id + '_' + _booking_id + '_' + val.service_start_date + '" class="copy_maid"  /></div></div>';
                                    });				
                                    _maid_html += '</div>';
                                    $('#maids-panel .inner').html(_maid_html);
                                }
			}
		});
	}
}
$('body').on('click', '#drivers-panel .close', function() {
	$('#drivers-panel').slideUp( function() {  } );
});
$('body').on('click', '#maids-panel .close', function() {
	$('#maids-panel').slideUp( function() { $('#customer-copied-maid').show(); } );
});
$('body').on('click', '#service-panel .close', function() {
    $('#service-panel').slideUp( function() { $('#customer-copied-maid').show(); } );
});
$('body').on('click', '.copy_service', function() {
    var category_id = $(this).attr('data-catid');
    var subcategory_id = $(this).attr('data-subcatid');
    var furnish_id = $(this).attr('data-furnishid');
    var scrub_id = $(this).attr('data-scrubid');
    var name = $(this).attr('data-name');
    var tot_amount = $(this).attr('data-cost');
    var flat_rate = $(this).attr('data-flat');
    $('#flatrateval').val(flat_rate);
    $('#categoryidval').val(category_id);
    $('#subcategoryidval').val(subcategory_id);
    $('#furnishid').val(furnish_id);
    $('#scrubpolishid').val(scrub_id);
    $('#valname').val(name);
    $('#servicecostval').val(tot_amount);
	$('#b-rate_per_hr').val(tot_amount);
	$('#service_taxed_total').val(tot_amount)
    if(flat_rate == 'Y')
    {
        $('#squarefeetval').val('');
        $("#sqrbtnshow").hide();
        var total_costs = (parseFloat(tot_amount));
        var total_costs = total_costs.toFixed(2);
        $("#tot_amout").val(total_costs);
        $("#serviceamountcost").val(total_costs);
    } else {
        $("#sqrbtnshow").show();
    }
    var _booking_id = $('#booking-id').val();
    if (_booking_id != '')
    {
            chk_booking_chg(_booking_id);
    }
    $('#service-panel').slideUp();
	$('#b-rate_per_hr').trigger("input");
});
$('body').on('click', '.copy_maid', function() {
        var _booking_details = $(this).attr('id').replace('mcopy_','').split('_');
        var _maid_id = $.trim(_booking_details[0]);
        var _booking_id = $.trim(_booking_details[1]);  
	_refresh_page = false;
	$('#save-booking').attr('id', 'copying-booking');
	$('#saving-booking').val('Please wait...');
	$.post( _page_url, { action: 'copy-maid', booking_id: _booking_id, maid_id: _maid_id}, function(response) {
		_bpop_open = false;
		var _alert_html = '';
		if(response == 'refresh')
		{
			$('#alert-title').html('Error !');
			$('#alert-message').html('An unexpected error. Please try again.');
			fancybox_show('alert-popup');
			_refresh_page = true;
		}
                else if(response == 'locked')
                {
			$('#alert-title').html('Error !');
			$('#alert-message').html('This booking is locked by another user.');
			fancybox_show('alert-popup');
                        _refresh_page = false;
                }
		else
		{
			var _resp = $.parseJSON(response);
			if(typeof _resp.status && _resp.status == 'success')
			{	
				$('#saving-booking').val('Done');
				$('#alert-title').html('Success !');
				$('#alert-message').html('Booking has been done successfully.');
				fancybox_show('alert-popup');
				_refresh_page = false
			}
			else if(typeof _resp.status && _resp.status == 'error')
			{
				$('#b-error').text(_resp.message);
				$('#saving-booking').attr('id', 'save-booking');
				$('#save-booking').val('Save');
			}
			else
			{
				$('#alert-title').html('Error !');
				$('#alert-message').html('An unexpected error. Please try again.');
				fancybox_show('alert-popup');
				_refresh_page = true
			}
		}
		$('#maids-panel').slideUp(function() {
                        if(_booking_id != '')
                        {
                                chk_booking_chg(_booking_id);
                        }
                });
		refresh_grid();
	});
});
//End
$('body').on('click', '#customer-address-panel .close', function() {
	$('#customer-address-panel').slideUp( function() { $('#customer-picked-address').show(); } );
});
var _refresh_page = false;
$('body').on('click', '#save-booking', function() {
	$('.field-error').text('');
	var _service_date = $('#service-date').val();
	var _maid_id = $.trim($('#maid-id').val());
	var _customer_id = $.trim($('#b-customer-id').val());
	var _customer_address_id = $.trim($('#customer-address-id').val());
	var _service_type_id = $.trim($('#b-service-type-id').val());
	var _from_time = $.trim($('#b-from-time').val());
	var _to_time = $.trim($('#b-to-time').val());
	var _lock_booking = $('#lock-booking').is(':checked') ? 1 : 0;
	var _booking_type = $.trim($('#b-booking-type').val());
	var _hrly_amount = $.trim($('#b-rate_per_hr').val());
	var _pending_amount = $.trim($('#b-pending_amount').val());
	var _discount = $.trim($('#b-discount').val());
	var _tot_amt = $.trim($('#tot_amout').val());
	var _just_mop_new_ref = $.trim($('#b-customer-reference-cell').val());
    var payment_mode = $.trim($('#b-customer-paymode-cell').val());    
	var _note = $.trim($('#booking-note').val());
	var _cleaning_material = $('#b-cleaning-materials').is(':checked') ? 'Y' : 'N';
	var _cleaning_material_fee = $('#cleaning-materials-charge').val();
	var _supervisor = $('#supervisor_selected').is(':checked') ? 'Y' : 'N';
	var _supervisor_fee = $('#supervisor-charge').val();
	var _email_notifications = $('#notification-email-booking').is(':checked') ? 'Y' : 'N';
	var _sms_notifications = $('#notification-sms-booking').is(':checked') ? 'Y' : 'N';
	var _flagval = $.trim($('#customer_flag_val').val());
	var _flagreason = $.trim($('#customer_flag_reason').val());
	//service-values
	var _service_flatrateval = $.trim($('#flatrateval').val());
	var _service_categoryidval = $.trim($('#categoryidval').val());
	var _service_subcategoryidval = $.trim($('#subcategoryidval').val());
	var _service_furnishid = $.trim($('#furnishid').val());
	var _service_scrubpolishid = $.trim($('#scrubpolishid').val());
	var _service_valname = $.trim($('#valname').val());
	var _service_servicecostval = $.trim($('#servicecostval').val());
	var _service_servicetypeval = $.trim($('#servicetypeval').val());
	var _service_sqftcount = $.trim($('#sqftcount').val());
	//Ends
	//Discount values
	var discountratehr = $('#discount_rate_perhr').val();
	var servicetotamt = $('#serviceamountcost').val();
	//Discount ends
	var tabletidval = $('#driver_select_id').val();
	$('#b-error').text('');
	if($.isNumeric(_customer_id) == false)
	{
		$('#b-customer-id').parent().append('<label class="field-error text-danger">Select a customer !</label>');
		return false;
	}
	if(_flagval == 'Y')
	{
		if(_flagreason != "")
		{
			var flagreason = " ("+_flagreason+")"; 
		} else {
			var flagreason = "";
		}
		$('#b-error').text('Customer is flagged.'+flagreason);
		return false;
	}
	if($.isNumeric(_customer_address_id) == false)
	{
		$('#b-error').text('Pick customer address');
		open_address_panel(_customer_id);
		return false;
	}
	if($.isNumeric(_service_type_id) == false)
	{
		$('#b-error').text('Select service type');
		return false;
	}
	if(_from_time == '' || _to_time == '')
	{
		$('#b-error').text('Select booking time');
		return false;
	}
	if(_booking_type == '')
	{
		$('#b-booking-type').parent().append('<label class="field-error text-danger">Select repeat type !</label>');
		return false;
	}
        if(_tot_amt == '')
	{
		$('#b-error').text('Enter total amount');
		return false;
	}
	var _repeat_days = [];
	var _repeat_end = '';
	var _repeat_end_date = '';
	if(_booking_type == 'WE' || _booking_type == 'BW')
	{	
		_repeat_days = $('input[id^="repeat-on-"]:checked').map(function() {
			return this.value;
		  }).get();
		  if(_repeat_days.length == 0)
		  {
			  $('#repeat-days').append('<label class="field-error text-danger">Select repeat days !</label>');
			  return false;
		  }
		  _repeat_end = $('input[name="repeat_end"]:checked').val();
		  if(_repeat_end == 'ondate')
		  {
			  _repeat_end_date = $('#repeat-end-date').val();
			  if(_repeat_end_date == '')
			  {
				  $('#repeat-ends').append('<label class="field-error text-danger">Select service end date !</label>');
				return false;
			  }
		  }
	}
	if($.isNumeric(tabletidval) == false)
	{
		$('#driver_select_id').parent().append('<label class="field-error text-danger">Select driver !</label>');
		return false;
	}
	_refresh_page = false;
	$('#save-booking').attr('id', 'saving-booking');
	$('#saving-booking').val('Please wait...');

	//------------------------------------------------------
	//--- Add Tools only Cleaning Materials if is Disabled
	var _cleaning_supplies_array = [];
	if($('#b-cleaning-materials').is(':checked')==false){
		$('.w_tool:checked').each(function() {
		 _cleaning_supplies_array.push($(this).val());
		});
	}else{
		const planCleanType = $('.w_tooltype:checked').val();		
		 _cleaning_supplies_array.push(planCleanType);
	}
	console.log(_cleaning_supplies_array);
	//------------------------------------------------------
	$.post( _page_url, {
	action: 'book-maid',
	customer_id: _customer_id,
	customer_address_id: _customer_address_id,
	maid_id: _maid_id,
	service_type_id: _service_type_id,
	time_from: _from_time,
	time_to: _to_time,
	booking_type: _booking_type,
	repeat_days: _repeat_days,
	repeat_end: _repeat_end,
	repeat_end_date: _repeat_end_date,
	is_locked: _lock_booking,
	pending_amount: _pending_amount,
	booking_note: _note,
	discount : _discount,
	tot_amt : _tot_amt,
	cleaning_material : _cleaning_material,
	cleaning_material_fee: _cleaning_material_fee,
	cleaning_supplies : _cleaning_supplies_array,
	supervisor : _supervisor,
	supervisor_fee: _supervisor_fee,
	email_notifications : _email_notifications,
	sms_notifications : _sms_notifications,
	payment_mode:payment_mode,
	just_mop_new_ref:_just_mop_new_ref,
	hrly_amount:_hrly_amount,
	service_flatrateval:_service_flatrateval,
	service_categoryidval:_service_categoryidval,
	service_subcategoryidval:_service_subcategoryidval,
	service_furnishid:_service_furnishid,
	service_scrubpolishid:_service_scrubpolishid,
	service_valname:_service_valname,
	service_servicecostval:_service_servicecostval,
	service_servicetypeval:_service_servicetypeval,
	service_sqftcount:_service_sqftcount,
	discountratehr:discountratehr,
	servicetotamt:servicetotamt,
	service_vat_amount: $('#service_vat_amount').val(),
	service_taxed_total: $('#service_taxed_total').val(),
	tabletidval:tabletidval}, function(response) {
		_bpop_open = false;
		refresh_grid();
		var _alert_html = '';
		if(response == 'refresh')
		{
			$('#alert-title').html('Error !');
			$('#alert-message').html('An unexpected error. Please try again.');
			fancybox_show('alert-popup');
			_refresh_page = true;
		}
		else
		{
			var _resp = $.parseJSON(response);
			if(typeof _resp.status && _resp.status == 'success')
			{	
				$('#saving-booking').val('Done');
				$('#alert-title').html('Success !');
				$('#alert-message').html('Booking has been done successfully.');
				fancybox_show('alert-popup');
				_refresh_page = false
			}
			else if(typeof _resp.status && _resp.status == 'error')
			{
				$('#b-error').text(_resp.message);
				$('#saving-booking').attr('id', 'save-booking');
				$('#save-booking').val('Save');
			}
			else
			{
				$('#alert-title').html('Error !');
				$('#alert-message').html('An unexpected error. Please try again.');
				fancybox_show('alert-popup');
				_refresh_page = true
			}
		}
	});
});
$('body').on('click', '#updatedrivertobooking', function() {
	$('#updatedrivertobooking').val('Please wait...');
	var _booking_id = $('#booking-id').val();
	var _servicedate = $('#servicesdate').val();
	var _driverval = $.trim($('#driver_select_id_update').val());
	$('#b-error').text('');
	if(_driverval == '')
	{
		$('#b-error').text('Select Driver...');
		$('#updatedrivertobooking').val('Update Driver');
		return false;
	}
	$('.mm-loader').show();
	$.post( _page_url, { action: 'update-driver-to-booking', booking_id: _booking_id, tablet_id: _driverval, servicedate:_servicedate}, function(response) {
		var _alert_html = '';
		$('.mm-loader').hide();
		if(response == 'locked')
		{
			$('#updatedrivertobooking').val('Update Driver');
			$('#b-error').text('Booking Locked.');
			return false;
		} else if(response == 'error')
		{
			$('#updatedrivertobooking').val('Update Driver');
			$('#b-error').text('Something went wrong. Try again.');
			return false;
		}
		else
		{
			var _resp = $.parseJSON(response);
			if(typeof _resp.status && _resp.status == 'success')
			{	
				$('#updatedrivertobooking').val('Update Driver');
				$('#b-error').text('');
				$('#updatedriversec').hide();
				$('#hiddentabletid').val(_resp.tablet_id);
				$('.normaldriversec').show();
				$('#driver_select_id option[value="' + _resp.tablet_id + '"]').prop('selected', true);
				$('#driver_select_id').trigger('change');
				$("#driver_select_id").prop("disabled", true);
				refresh_grid();
				return false;
			} else {
				$('#updatedrivertobooking').val('Update Driver');
				$('#b-error').text('Something went wrong. Try again.');
				return false;
			}
		}
	});
});
$('body').on('click', '#update-permanent, #update-one-day', function() {
	var _update_type = $(this).attr('id').replace('update-', '');
	var _all_bookings = $.parseJSON($('#all-bookings').val());
	var _booking_id = $('#booking-id').val();
	if($.isNumeric(_booking_id) == false || typeof _all_bookings[_booking_id] == 'undefined')
	{
		window.location = _page_url;
	}
	var _customer_address_id = $.trim($('#customer-address-id').val());
	var _from_time = $.trim($('#b-from-time').val());
	var _to_time = $.trim($('#b-to-time').val());
	var _pending_amount = $.trim($('#b-pending-amount').val());
	var _discount = $.trim($('#b-discount').val());
	var _priceperhr = $.trim($('#b-rate_per_hr').val());
	var _discountpriceperhr = $.trim($('#discount_rate_perhr').val());
	var _netamtcost = $.trim($('#serviceamountcost').val());
	var _total_amt = $.trim($('#tot_amout').val());
	var _lock_booking = $('#lock-booking').is(':checked') ? 1 : 0;
	var _cleaning_material = $('#b-cleaning-materials').is(':checked') ? 'Y' : 'N';
	var _cleaning_material_fee = $('#cleaning-materials-charge').val();
	var _supervisor = $('#supervisor_selected').is(':checked') ? 'Y' : 'N';
	var _supervisor_fee = $('#supervisor-charge').val();
	var _note = $.trim($('#booking-note').val());
	var _payment_mode = $.trim($('#b-customer-paymode-cell').val());
	var _ref_no = $.trim($('#b-customer-reference-cell').val());
	var _email_notifications = $('#notification-email-booking').is(':checked') ? 'Y' : 'N';
	var _sms_notifications = $('#notification-sms-booking').is(':checked') ? 'Y' : 'N';
	var _dateservice = $.trim($('#servicesdate').val());
	if(_from_time == '' || _to_time == '')
	{
		$('#b-error').text('Select booking time');
		return false;
	}
	var _repeat_end = '';
	var _repeat_end_date = '';
	if(_all_bookings[_booking_id].booking_type != 'OD')
	{
		 _repeat_end = $('input[name="repeat_end"]:checked').val();
		  if(_repeat_end == 'ondate')
		  {
			  _repeat_end_date = $('#repeat-end-date').val();
			  if(_repeat_end_date == '')
			  {
				  $('#b-error').text('Enter an end date');
				return false;
			  }
		  }
	}
	_refresh_page = false;
	$('#update-cancel').hide();
	var _update_label = $('#update-permanent').val();
	$('#update-permanent').attr('id', 'updating-permanent');
	$('#updating-permanent').val('Please wait...');
	//------------------------------------------------------
	//--- Add Tools only Cleaning Materials if is Disabled
	var _cleaning_supplies_array = [];
	if($('#b-cleaning-materials').is(':checked')==false){
		$('.w_tool:checked').each(function() {
		 _cleaning_supplies_array.push($(this).val());
		});
	}else{
		const planCleanType = $('.w_tooltype:checked').val();		
		 _cleaning_supplies_array.push(planCleanType);
	}
	console.log(_cleaning_supplies_array);
	//------------------------------------------------------
	$.post( _page_url, { 
		action: 'update-booking', 
		booking_id: _booking_id, 
		update_type: _update_type, 
		customer_address_id: _customer_address_id, 
		time_from: _from_time, 
		time_to: _to_time, 
		repeat_end: _repeat_end, 
		repeat_end_date: _repeat_end_date, 
		pending_amount: _pending_amount, 
		discount : _discount, 
		total_amt: $('#service_taxed_total').val(), 
		booking_note: _note, 
		is_locked : _lock_booking, 
		cleaning_material : _cleaning_material,
		cleaning_material_fee: _cleaning_material_fee,
		cleaning_supplies : _cleaning_supplies_array,
		supervisor : _supervisor,
		supervisor_fee: _supervisor_fee,
		email_notifications : _email_notifications, 
		sms_notifications : _sms_notifications,
		payment_mode:_payment_mode,
		reference_no:_ref_no,
		priceperhr:_priceperhr,
		discountpriceperhr:_discountpriceperhr,
		netamtcost:_netamtcost,
		net_service_cost: $('#tot_amout').val(),
		dateservice: _dateservice,
		vat_charge: $('#service_vat_amount').val()
	}, function(response) {
		_bpop_open = false;
		refresh_grid();
		var _alert_html = '';
		if(response == 'refresh')
		{
			$('#alert-title').html('Error !');
			$('#alert-message').html('An unexpected error. Please try again.');
			fancybox_show('alert-popup');
			_refresh_page = true;
		}
                else if(response == 'locked')
                {
			$('#alert-title').html('Error !');
			$('#alert-message').html('This booking is locked by another user.');
			fancybox_show('alert-popup');
                        _refresh_page = false; 
                } else if(response == 'odoorefresh')
                {
			$('#alert-title').html('Error !');
			$('#alert-message').html('Update failed. Booking already invoiced.');
			fancybox_show('alert-popup');
                        _refresh_page = false; 
                }
		else
		{
			var _resp = $.parseJSON(response);
			if(typeof _resp.status && _resp.status == 'success')
			{	
				$('#saving-booking').val('Done');
				$('#alert-title').html('Success !');
				$('#alert-message').html('Booking has been updated successfully.');
				fancybox_show('alert-popup');
				_refresh_page = false
			}
			else if(typeof _resp.status && _resp.status == 'error')
			{
				$('#b-error').text(_resp.message);
				$('#update-cancel').show();
				$('#updating-permanent').attr('id', 'update-permanent');
				$('#update-permanent').val(_update_label);
			}
			else
			{
				$('#alert-title').html('Error !');
				$('#alert-message').html('An unexpected error. Please try again.');
				fancybox_show('alert-popup');
				_refresh_page = true
			}
		}
	});
});
$('body').on('click', '.pop_close', function() {
	_bpop_open = false;
	parent.$.fancybox.close();
	$(".ui-selected").removeClass("ui-selected");
	if(_refresh_page)
	{
		window.location = _page_url;
	}
});
function refresh_grid()
{
	if(_bpop_open != true)
	{
		$('.time_grid').addClass('loading');
		$.post( _page_url, { action: 'refresh-grid' }, function(response) {
			$('.time_grid').removeClass('loading');
			response = $.parseJSON(response);
			$('#schedule-grid-rows').html(response.grid);
			$('#schedule-report').html(response.report);
			setTimeout('refresh_grid()', 300000);
			if(_bpop_open != true)
			{
				apply_selectable();
			}
		});
	}
	else
	{
		setTimeout('refresh_grid()', 300000);
	}
}
apply_selectable();
$('#schedule').show();
$('#print-schedule-report').click(function() {
	var _print_ontents = $('#schedule-report').html();
	var _original_contents = $('body').html();
	$('body').html(_print_ontents);
	$('#schedule-report').show();
	window.print();
	$('body').html(_original_contents);
        window.location.reload();
});
var _width = parseInt($('#schedule .time_line').width());
$(window).scroll(function() {
    var scroll = $(window).scrollTop();
});
if ($(".booking-position").length > 0)
{
   var stickyTop = $('.booking-position').offset().top;
   $(window).on('scroll', function () {
       if ($(window).scrollTop() >= stickyTop) {
           $('.scroll-top-fix').addClass('box-fixed-top');
           $('.book-mid-det-lt-box').addClass('book-mid-det-top-padi');
       } else {
           $('.scroll-top-fix').removeClass('box-fixed-top');
           $('.book-mid-det-lt-box').removeClass('book-mid-det-top-padi');
       }
   });
}
function add_customer()
{
    var typed_cont = $(".select2-search__field").val();
    $('#customer-popup #customer_name').val(typed_cont);
    $("#b-customer-id").select2("close");
    $(".fancybox-skin").css("width","100%").css("margin","0 auto");
    $(".fancybox-outer").css("width","100%").css("margin","0 auto");
    $(".fancybox-inner").css("width","100%").css("margin","0 auto");
    $(".fancybox-wrap").css("width","80%").css("margin","0 auto").css("left","10%");
    $("#customer-detail-popup").hide();
    $("#customer-add-popup").show();
    $("#b-customer-id").select2("val", "");
    $('#customer-picked-address').html('');
    $("#popup-booking").removeClass("col-md-12 col-sm-12");
    $("#popup-booking").addClass("col-md-8 col-sm-7");
}
    function loadLocationField(id)
    {
        if(document.getElementById(id).value!=""){
        var input = document.getElementById(id);
        var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(id).value = val;
          }
        }
        });
    }
    }
    function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    var d = new Date(start_time);
    var t=today.getTime()-d.getTime();
    var time=convertMS(t);
    document.getElementById('progress_time').innerHTML =time.h+":"+time.m+":"+time.s;
    var t = setTimeout(startTime, 500);
}
function convertMS(ms,format=true) {
  var d, h, m, s;
  s = Math.floor(ms / 1000);
  m = Math.floor(s / 60);
  s = s % 60;
  h = Math.floor(m / 60);
  m = m % 60;
  d = Math.floor(h / 24);
  h = h % 24;
  if(format){
  if(h<10) {h = "0" + h  };
  if(m<10) {m = "0" + m  };
  if(s<10) {s = "0" + s  };
  }
  return {d: d,h:h, m: m, s: s };
};
function formatAMPM(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
}
$(function () {
	let dateString = new Date();
	let day = dateString.getDay();
	$("#repeat-days").find("input[type=checkbox][value=" + 3 +"]").prop("checked", true);
});
function copyBookingMessage(self) {
	let booking_id = $(self).attr('data-id');
	let _all_bookings = $.parseJSON($('#all-bookings').val());
	let booking = _all_bookings[booking_id];
	let message = "Confirmed Booking" 
	message += "\nHi " + booking.customer_nick_name;
	message += "\nLocation: " +booking.apartment_no+ ", " 
				+booking.customer_zone+ ", "+booking.customer_area +", "
				+booking.customer_address;
	message += "\nService Date : " + $('#servicesdate').val();
	message += "\nService Type : " + booking.service_type_names;
	message += "\nCleaning Materials : " + booking.cleaning_material;
	message += "\nMaid Name : " + booking.maid_name;
	message += "\nTime : " + booking.time_from + " to " + booking.time_to + " (" + booking.working_hours +" hrs)";
	message += "\nTotal Amount : " + booking.total_amount;
	let booking_note = (booking.booking_note === '') ? 'NIL' :booking.booking_note;
	let latlong = (booking.latitude != '' && booking.longitude != '') ? 'https://www.google.com/maps?q=loc:'+booking.latitude+','+booking.longitude : '';
	message += "\nBooking notes : " + booking_note;
	if(latlong != '')
	{
		message += "\n\n" + latlong;
	}
	navigator.clipboard.writeText(message);
	alert("Booking Details Copied !");
};
$('body').on('input', '#service_taxed_total', function () {
	check_and_show_update_btn();
	let service_discount_total = parseFloat($('#b-discount').val() || 0);
	let service_vat_amount = (this.value * service_vat_percentage) / (100 + service_vat_percentage);
	$('#service_vat_amount').val(service_vat_amount.toFixed(2));
	let service_amount_after_discount = this.value - service_vat_amount;
	$('#tot_amout').val(service_amount_after_discount.toFixed(2));
	let service_material_cost = $('#cleaning-materials-charge').val();
	let supervisor_cost = $('#supervisor-charge').val();
	let service_amount_without_material = service_amount_after_discount - service_material_cost - supervisor_cost + service_discount_total;
	/************************************************ */
	let _start_time = parseAMDate($("#b-from-time option:selected").text().toUpperCase());
	let _end_time = parseAMDate($("#b-to-time option:selected").text().toUpperCase());
	if (_end_time < _start_time) {
		_end_time = parseAMDate(_end, 1);
	}
	let difference = _end_time - _start_time;
	let hours = (difference / 36e5),
		minutes = (difference % 36e5 / 60000);
	if (parseInt(hours) >= 0) {
		if (minutes == 0) {
			minutes = "00";
		}
	}
	if (fixed_service_rate == true) hours = 1;
	/************************************************ */
	let service_rate_per_hr = service_amount_without_material / hours;
	$('#b-rate_per_hr').val(service_rate_per_hr.toFixed(2));
	let service_amount_before_discount = service_rate_per_hr * hours;
	$('#serviceamountcost').val(service_amount_before_discount.toFixed(2));
	let service_discount_per_hour = service_discount_total / hours;
	let discount_rate_perhr = service_rate_per_hr - service_discount_per_hour;
	$('#discount_rate_perhr').val(discount_rate_perhr.toFixed(2));
});
$('body').on('input', '#discount_rate_perhr', function () {
	check_and_show_update_btn();
	let input =  this.value || $('#b-rate_per_hr').val();
	_saved_booking_discount_rate_per_hour = input;
	let service_rate_per_hr = $('#b-rate_per_hr').val();
	let service_material_cost = parseFloat($('#cleaning-materials-charge').val());
	let supervisor_cost = parseFloat($('#supervisor-charge').val());
	/************************************************ */
	let _start_time = parseAMDate($("#b-from-time option:selected").text().toUpperCase());
	let _end_time = parseAMDate($("#b-to-time option:selected").text().toUpperCase());
	if (_end_time < _start_time) {
		_end_time = parseAMDate(_end, 1);
	}
	let difference = _end_time - _start_time;
	let hours = (difference / 36e5),
		minutes = (difference % 36e5 / 60000);
	if (parseInt(hours) >= 0) {
		if (minutes == 0) {
			minutes = "00";
		}
	}
	if (fixed_service_rate == true) hours = 1;
	/************************************************ */
	let service_amount_before_discount = service_rate_per_hr * hours;
	$('#serviceamountcost').val(service_amount_before_discount.toFixed(2));
	let service_amount_after_discount = input * hours;
	let service_amount_discount = service_amount_before_discount - service_amount_after_discount;
	$('#b-discount').val(service_amount_discount.toFixed(2));
	let service_amount_without_vat = service_amount_after_discount + service_material_cost + supervisor_cost;
	$('#tot_amout').val(service_amount_without_vat.toFixed(2));
	let service_vat_amount = (service_vat_percentage / 100) * service_amount_without_vat;
	$('#service_vat_amount').val(service_vat_amount.toFixed(2));
	// let service_taxed_total = service_amount_without_vat + service_vat_amount + _serviceMaterialCost;
	let service_taxed_total = service_amount_without_vat + service_vat_amount;
	$('#service_taxed_total').val(service_taxed_total.toFixed(2));
});
$('body').on('change', '#free-service', function () {
	check_and_show_update_btn();
	/************************************************ */
	let _start_time = parseAMDate($("#b-from-time option:selected").text().toUpperCase());
	let _end_time = parseAMDate($("#b-to-time option:selected").text().toUpperCase());
	if (_end_time < _start_time) {
		_end_time = parseAMDate(_end, 1);
	}
	let difference = _end_time - _start_time;
	let hours = (difference / 36e5),
		minutes = (difference % 36e5 / 60000);
	if (parseInt(hours) >= 0) {
		if (minutes == 0) {
			minutes = "00";
		}
	}
	if (fixed_service_rate == true) hours = 1;
	/************************************************ */
	if ($('#free-service').is(':checked') == true) {
		$('#b-rate_per_hr').attr('readonly', true);
		$('#discount_rate_perhr').attr('readonly', true);
		$('#service_taxed_total').attr('readonly', true);
		$('#b-discount').attr('readonly', true);
		$('#cleaning-materials-charge').val(0);
		$('#supervisor-charge').val(0);
		$('#b-discount').val(0);
		$('#b-rate_per_hr').val(0);
		$('#service_taxed_total').val(0).trigger("input");
	} else {
		$('#b-rate_per_hr').attr('readonly', false);
		$('#discount_rate_perhr').attr('readonly', false);
		$('#service_taxed_total').attr('readonly', false);
		$('#b-discount').attr('readonly', false);
		$('#discount_rate_perhr').val('');
		let service_material_cost = ($('#b-cleaning-materials').is(':checked') == true) ? (_service_material_cost_per_hr * hours) : 0;
		let supervisor_cost = ($('#supervisor_selected').is(':checked') == true) ? (_service_supervisor_cost_per_hr * hours) : 0;
		$('#cleaning-materials-charge').val(service_material_cost.toFixed(2));
		$('#supervisor-charge').val(supervisor_cost.toFixed(2));
		$('#discount_rate_perhr').val(_saved_booking_discount_rate_per_hour > 0 ? _saved_booking_discount_rate_per_hour : _saved_booking_rate_per_hour);
		$('#b-rate_per_hr').val(_saved_booking_rate_per_hour).trigger("input");
	}
});
$('body').on('input', '#b-rate_per_hr', function () {
	check_and_show_update_btn();
	let input = this.value || 0;
	_saved_booking_rate_per_hour = input;
	/************************************************ */
	let _start_time = parseAMDate($("#b-from-time option:selected").text().toUpperCase());
	let _end_time = parseAMDate($("#b-to-time option:selected").text().toUpperCase());
	if (_end_time < _start_time) {
		_end_time = parseAMDate(_end, 1);
	}
	let difference = _end_time - _start_time;
	let hours = (difference / 36e5),
		minutes = (difference % 36e5 / 60000);
	if (parseInt(hours) >= 0) {
		if (minutes == 0) {
			minutes = "00";
		}
	}
	if (fixed_service_rate == true) hours = 1;
	/************************************************ */
	let service_discount_rate_per_hr = $('#discount_rate_perhr').val() || input;
	let service_amount_before_discount = input * hours;
	$('#serviceamountcost').val(service_amount_before_discount.toFixed(2));
	let service_amount_after_discount = service_discount_rate_per_hr * hours;
	let service_amount_discount = service_amount_before_discount - service_amount_after_discount;
	$('#b-discount').val(service_amount_discount.toFixed(2));
	let service_material_cost = ($('#b-cleaning-materials').is(':checked') == true) ? (_service_material_cost_per_hr * hours) : 0;
	let supervisor_cost = ($('#supervisor_selected').is(':checked') == true) ? (_service_supervisor_cost_per_hr * hours) : 0;
	$('#cleaning-materials-charge').val(service_material_cost.toFixed(2));
	$('#supervisor-charge').val(supervisor_cost.toFixed(2));
	let service_amount_without_vat = service_amount_after_discount + service_material_cost + supervisor_cost;
	$('#tot_amout').val(service_amount_without_vat.toFixed(2));
	let service_vat_amount = (service_vat_percentage / 100) * service_amount_without_vat;
	$('#service_vat_amount').val(service_vat_amount.toFixed(2));
	// let service_taxed_total = service_amount_without_vat + service_vat_amount+_serviceMaterialCost+_supervisorCost;
	let service_taxed_total = service_amount_without_vat + service_vat_amount;
	$('#service_taxed_total').val(service_taxed_total.toFixed(2));
});
$('body').on('input', '#b-discount', function () {
	check_and_show_update_btn();
	/************************************************ */
	let _start_time = parseAMDate($("#b-from-time option:selected").text().toUpperCase());
	let _end_time = parseAMDate($("#b-to-time option:selected").text().toUpperCase());
	if (_end_time < _start_time) {
		_end_time = parseAMDate(_end, 1);
	}
	let difference = _end_time - _start_time;
	let hours = (difference / 36e5),
		minutes = (difference % 36e5 / 60000);
	if (parseInt(hours) >= 0) {
		if (minutes == 0) {
			minutes = "00";
		}
	}
	if (fixed_service_rate == true) hours = 1;
	/************************************************ */
	let service_rate_per_hr = $('#b-rate_per_hr').val();
	let service_discount_per_hour = this.value / hours;
	let service_discount_rate_per_hr = service_rate_per_hr - service_discount_per_hour;
	$('#discount_rate_perhr').val(service_discount_rate_per_hr);
	$('#service_taxed_total').trigger("input");
});
function show_maids_on_move_panel() {
	var serv_date = $('#servicesdate').val();
	$('#maids-move-panel .inner').html('<div class="inner">Loading free maids<span class="dots_loader"></span></div>');
	$.post(_base_url +"maid/get_available_maids_for_move", { booking_id: _selected_booking_id, same_zone: $('input[name="move_to_same_zone"]:checked').val(), service_date: serv_date }, function (response) {
		console.log(response.message);
		var _maid_html = '<div class="table">';
		$.each(response.data, function (key, val) {
			_maid_html += '<div class="row n-pick-maid-c-main m-0"><div class="col-sm-10 n-pick-maid-left pl-0"><span id="mcopyadd-' + val.maid_id + '"><strong>' + val.maid_name + '</strong></span><br /><span id="mcopy-' + val.maid_id + '">' + val.maid_nationality + '</span></div><div class="col-sm-2 n-pick-maid-right pr-0"><button type="button" data-id="' + val.maid_id + '" data-booking="' + _selected_booking_id + '" id="move_' + val.maid_id + '_' + _selected_booking_id + '" class="n-btn btn-block move_maid">Move</button></div></div>';
		});
		_maid_html += '</div>';
		$('#maids-move-panel .inner').html(_maid_html);
	});
}
$('body').on('click', '#move-booking-btn', function () {
	$('input[name="move_to_same_zone_or_not"]').prop("disabled", false);
	$('#move-booking-modal .title').html("Move Booking #" + _selected_booking_id);
	fancybox_show('move-booking-modal', { 'width': 650, 'height': 300 });
	show_maids_on_move_panel();
});
$('body').on('click', 'input[name="move_to_same_zone_or_not"]', function () {
	show_maids_on_move_panel();
});
$('body').on('click', '.move_maid', function () {
	var _booking_id = $(this).data("booking");
	_refresh_page = false;
	$('input[name="move_to_same_zone_or_not"],.move_maid').prop("disabled", true);
	$(this).html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
	$.post(_page_url, { action: 'move-maid', booking_id: _booking_id, maid_id: $(this).data("id") }, function (response) {
		_bpop_open = false;
		if (response == 'refresh') {
			$('#alert-title').html('Error !');
			$('#alert-message').html('An unexpected error. Please try again.');
			fancybox_show('alert-popup');
			_refresh_page = true;
		}
		else if (response == 'ongoing') {
			$('#alert-title').html('Error !');
			$('#alert-message').html('Please complete ongoing services and try again.');
			fancybox_show('alert-popup');
			_refresh_page = false;
		}
		else if (response == 'locked') {
			$('#alert-title').html('Error !');
			$('#alert-message').html('This booking is locked by another user.');
			fancybox_show('alert-popup');
			_refresh_page = false;
		}
		else {
			var _resp = $.parseJSON(response);
			if (typeof _resp.status && _resp.status == 'success') {
				$('#saving-booking').val('Done');
				$('#alert-title').html('Success !');
				$('#alert-message').html('Booking has been moved successfully.');
				fancybox_show('alert-popup');
				_refresh_page = false
			}
			else if (typeof _resp.status && _resp.status == 'error') {
				$('#b-error').text(_resp.message);
				$('#saving-booking').attr('id', 'save-booking');
				$('#save-booking').val('Save');
			}
			else {
				$('#alert-title').html('Error !');
				$('#alert-message').html('An unexpected error. Please try again.');
				fancybox_show('alert-popup');
				_refresh_page = true
			}
		}
		refresh_grid();
	});
});
$('body').on('click', '#delete-cancelbtn', function() {
        var request_id = $(this).attr("data-id");
        $.fancybox.open({
			autoCenter : true,
			fitToView : false,
			scrolling : false,
			openEffect : 'none',
			openSpeed : 1,
			helpers : {
				overlay : {
					css : {
						'background' : 'rgba(0, 0, 0, 0.3)'
					},
					closeClick: true
				}
			},
			padding : 0,
			closeBtn : true,
			content : _alert_html = '<div id="alert-popup"><div class="head">Delete<span class="alert-popup-close pop_close"></span></div><div class="content padd20" style="padding:20px 0 5px !important;">Are you sure want to continue?</div><div class="bottom"><input type="button" value="Yes" data-bookRequestID="'+request_id+'" class="deleterequest_book" style="background:#b2d157;border:1px solid" />&nbsp;&nbsp;<input type="button" value="No" class="assign_no pop_close" /></div></div>',
			topRatio : 0.2,
		});
});
$('body').on('click', '.deleterequest_book', function() {
	$.fancybox.close();
	var _req_id=$(this).attr('data-bookRequestID');
	$.post(_page_url, { action: 'delete-booking-replace-request', request_id: _req_id }, function(response)
	{
		_bpop_open = false;
		refresh_grid();
		var _alert_html = '';
		if(response == 'success')
		{
			_alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Request has been deleted successfully.</div><div class="bottom"><input type="button" value="OK" class="pop_close" /></div></div>';
			_refresh_page = false
			refresh_grid();
		}
		else
		{
			_alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
			_refresh_page = true;
			refresh_grid();
		}
		if(_alert_html  != '')
		{
			$.fancybox.open({
				autoCenter : true,
				fitToView : false,
				scrolling : false,
				openEffect : 'fade',
				openSpeed : 100,
				helpers : {
						overlay : {
								css : {
										'background' : 'rgba(0, 0, 0, 0.3)'
								},
								closeClick: false
						}
				},
				padding : 0,
				closeBtn : false,
				content: _alert_html
			});
		}
		refresh_grid();
	});
});
function check_and_show_update_btn(){
	if (_new_booking == false && $('#update-booking').length == 0) {
		$('.n-popup-btn-main').prepend('<input type="button" class="save-but purple-btn" id="update-booking" value="Update" />');
	}
}
/**====================================================================
 * Function get Cleaning supplies for a particular booking
 ======================================================================*/
getBookingCleaningSupplies = function(booking_id){
	$.ajax({
		type: "GET",
		url: _base_url + 'cleaning_supplies/get_cleaning_supplies_by_booking/' + booking_id,
		cache: false,
		success: function (response) {
		  // Parse the JSON response to an array of supplies
		  var supplies = response;
	
		  // Log the supplies array to verify if data is correct
	
		  // Loop through the supplies and set the corresponding checkboxes as checked
		  $.each(supplies, function(index, supply) {
			var supplyId = supply.id;
			var checkboxId = "tools-" + supply.name.replace(/\s+/g, '-').toLowerCase();
			console.log(checkboxId);
			// Check the checkbox by setting the "checked" property to true
			$("#" + checkboxId).prop("checked", true);

			if($('#b-cleaning-materials').is(':checked')==true){
				$("#cleanPlanType").show();
				$("#customtools").hide();
			}else{
				$("#cleanPlanType").hide();
				$("#customtools").show();
			}

		  });
		},
		error: function(xhr, status, error) {
		  console.log("AJAX Error:", error);
		}
	  });
}
//=====================================================================
/**====================================================================
 * Function to calculate hours from Start time and End Time
 ======================================================================*/
calculateHours = function(){
	let _start_time = parseAMDate($("#b-from-time option:selected").text().toUpperCase());
	let _end_time = parseAMDate($("#b-to-time option:selected").text().toUpperCase());
	if (_end_time < _start_time) {
		_end_time = parseAMDate(_end, 1);
	}
	let difference = _end_time - _start_time;
	hours = (difference / 36e5);
	minutes = (difference % 36e5 / 60000);
	if (parseInt(hours) >= 0) {
		if (minutes == 0) {
			minutes = "00";
		}
	}
	if (fixed_service_rate == true) hours = 1;
	return hours;
}
//=====================================================================
/**====================================================================
 * Function to set amount based on each rate and hour
 ======================================================================*/
 setAmount = function(){
	//--- Calculate hours 
	let hours = calculateHours();
	let service_rate_per_hr = $('#b-rate_per_hr').val();
	let service_amount_before_discount = service_rate_per_hr * hours;
	$('#serviceamountcost').val(service_amount_before_discount.toFixed(2));
	setServiceDiscount();
 }
//=====================================================================
/**====================================================================
 * Function to set service  discount based on rate and hour and 
 * serviceamountcost
 ======================================================================*/
 setServiceDiscount = function(){
	let discount_amount = $('#discount_rate_perhr').val();
	console.log(discount_amount);
	let hours = calculateHours();
	service_amount_after_discount = hours*discount_amount;
	let service_amount_before_discount = $('#serviceamountcost').val();	
	let service_amount_discount = service_amount_before_discount - service_amount_after_discount;
	$('#b-discount').val(service_amount_discount.toFixed(2));
 }
//=====================================================================
/**====================================================================
 * Function to set material cost based on each dependencies 
 * 1. Cleaning Materials - Disabled - Tools Rent
 ======================================================================*/
setMaterialCost = function(){
	//--- Calculate hours 
	let hours = calculateHours();
	// hours = (hours<1)?1:hours;
	let newMaterialCost = 0;
	//--- Add Custom Tools only Cleaning Materials if is Disabled Or else select a plan
	if($('#b-cleaning-materials').is(':checked')==false){
		newMaterialCost += _totalToolCost;
	}else{
		let cleaningMaterialCost = _planTypeMaterialCost * hours;
		newMaterialCost += cleaningMaterialCost;
	}
	
	//--- Check for free service
	_serviceMaterialCost = ($('#free-service').is(':checked') == true) ? 0 : newMaterialCost;
	//--- Set to field
	$('#cleaning-materials-charge').val(_serviceMaterialCost.toFixed(2));
}
//=====================================================================
/**====================================================================
 * Get service amount without Vat
 ======================================================================*/
getServiceAmountWithoutVat = function(){
	//--- Calculate hours 
	let hours = calculateHours();
	let service_rate_per_hr = $('#b-rate_per_hr').val();
	let service_discount_rate_per_hr = $('#discount_rate_perhr').val() || service_rate_per_hr;
	let service_amount_after_discount = service_discount_rate_per_hr * hours;
	let service_amount_without_vat = service_amount_after_discount + _serviceMaterialCost + _supervisorCost;
	return service_amount_without_vat;
}
//=====================================================================
/**====================================================================
 * Adjust Total fields after changes
 ======================================================================*/
adjustTotals = function(){
	let serviceAmountWithoutVat = getServiceAmountWithoutVat();
	$('#tot_amout').val(serviceAmountWithoutVat.toFixed(2));
	let service_vat_amount = (service_vat_percentage / 100) * serviceAmountWithoutVat;
	$('#service_vat_amount').val(service_vat_amount.toFixed(2));
	// let service_taxed_total = serviceAmountWithoutVat + service_vat_amount + _serviceMaterialCost;
	let service_taxed_total = serviceAmountWithoutVat + service_vat_amount;
	$('#service_taxed_total').val(service_taxed_total.toFixed(2));
}
//=====================================================================
/**====================================================================
 * Add tools only when cleaning materials toggle is not selected
 ======================================================================*/
handleToolsCheckboxClick = function() {
	check_and_show_update_btn();
	/*- Cleaning Materials - Disabled (1)
	--- Each checkbox is checked, Add material cost by toolcost(Configured)*/
	// Loop through all the checked checkboxes
	_totalToolCost = 0;
    $('.w_tool:checked').each(function() {
		// Add the data-amount of the current checkbox to the totalAmount variable
		_totalToolCost += parseInt($(this).attr('data-amount'));
	});
	setMaterialCost();
	setSupervisorCost();
	adjustTotals();
}
//=====================================================================
/**====================================================================
 * Add PlanType only when cleaning materials toggle is selected
 ======================================================================*/
handleCleanPlanTypeCheckboxClick = function(){
	check_and_show_update_btn();
	let selectedRadioButton = $('.w_tooltype:checked');
	_planTypeMaterialCost = parseInt(selectedRadioButton.attr('data-amount'));
	setMaterialCost();
	setSupervisorCost();
	adjustTotals();
}
//=====================================================================
/**====================================================================
 * Cleaning materials Toggle functions
 ======================================================================*/
 handleCleaningMaterialsCheckboxClick = function(){
	check_and_show_update_btn();
	//--- Calculate hours 
	let hours = calculateHours();
	if($('#b-cleaning-materials').is(':checked')==true){
		$("#cleanPlanType").show();
		$("#customtools").hide();
	}else{
		$("#cleanPlanType").hide();
		$("#customtools").show();
	}
	if(_planTypeMaterialCost==0){
		_planTypeMaterialCost = $("#tools-regular-supplies").attr('data-amount');
	}
	setMaterialCost();
	setSupervisorCost();
	adjustTotals();
 }
 /**====================================================================
 * Supervisor Toggle functions
 ======================================================================*/
 handleSupervisorCheckboxClick = function(){
	 check_and_show_update_btn();
	//--- Calculate hours 
	let hours = calculateHours();
	if(_planTypeSUpervisorCost==0){
		_planTypeSUpervisorCost = 60;
	}
	setSupervisorCost();
	adjustTotals();
 }
 setSupervisorCost = function(){
	//--- Calculate hours 
	let hours = calculateHours();
	// hours = (hours<1)?1:hours;
	let newSupervisorCost = 0;
	//--- Add Custom Tools only Cleaning Materials if is Disabled Or else select a plan
	if($('#supervisor_selected').is(':checked')==false){
		newSupervisorCost += 0;
	}else{
		let supervisorCost = _planTypeSUpervisorCost * hours;
		newSupervisorCost += supervisorCost;
	}
	
	//--- Check for free service
	_supervisorCost = ($('#free-service').is(':checked') == true) ? 0 : newSupervisorCost;
	//--- Set to field
	$('#supervisor-charge').val(_supervisorCost.toFixed(2));
}

//============== On Clicks ==============================================
//--- Tools On Click
$('body').on('click', '.w_tool', handleToolsCheckboxClick);
//--- CleanPlanType On Click
$('body').on('change', '.w_tooltype', handleCleanPlanTypeCheckboxClick);
//--- Cleaning Materials On Click
$('body').on('click', '#b-cleaning-materials', handleCleaningMaterialsCheckboxClick);
//--- Supervisor On Click
$('body').on('click', '#supervisor_selected', handleSupervisorCheckboxClick);
//=======================================================================