(function (a) {
    a(document).ready(function (b) {
        if (a('#schedule-report').length > 0) {
            a("table#schedule-report").dataTable({
                sPaginationType: "full_numbers", "bSort": true, "iDisplayLength": 100, "scrollY": true,
                "scrollX": true,
                footerCallback: function (row, data, start, end, display) {
                    var api = this.api();
                    var intVal = function (i) {
                        return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
                    };
                    var hourVal = function (i) {
                        return typeof i === 'string' ? i.replace(':', '.') * 1 : typeof i === 'number' ? i : 0;
                    };
                    totalAmount = api
                        .column(6)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);
                    pageTotalAmount = api
                        .column(6, { page: 'current' })
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);
                    totalHours = api
                        .column(5)
                        .data()
                        .reduce(function (a, b) {
                            return hourVal(a) + hourVal(b);
                        }, 0);
                    pageTotalHours = api
                        .column(5, { page: 'current' })
                        .data()
                        .reduce(function (a, b) {
                            return hourVal(a) + hourVal(b);
                        }, 0);
                    $(api.column(5).footer()).html('<strong>' + pageTotalHours.toFixed(2) + ' <hr>' + totalHours.toFixed(2) + '</strong>');
                    $(api.column(6).footer()).html('<strong>' + pageTotalAmount.toFixed(2) + ' <hr>' + totalAmount.toFixed(2) + '</strong>');
                },
            });
            a("table#da-ex-datatable-default").dataTable({ "iDisplayLength": 100 });
        }
    });

})(jQuery);
$('#scheduleReportExcelBtn').click(function () {
    $("#scheduleReportExcelBtn").html('<i class="fa fa-spinner fa-spin"></i>');
    var fromdate = $('#servicefromdate').val();
    var todate = $('#servicetodate').val();
    var servicezoneid = $('#servicezoneid').val();
    $.ajax({
        type: "POST",
        url: _base_url + 'customerexcel/schedule_report_excellatest',
        data: { fromdate: fromdate, todate: todate, servicezoneid: servicezoneid },
        cache: false,
        success: function (response) {
            $("#scheduleReportExcelBtn").html('<i class="fa fa-file-excel-o"></i>');
            window.location = response;
        }
    });
});
$('#OneDayDate,#search_date_to').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
});