function clamp_range(range) {
    if (range.e.r >= (1 << 20)) range.e.r = (1 << 20) - 1;
    if (range.e.c >= (1 << 14)) range.e.c = (1 << 14) - 1;
    return range;
}

var crefregex = /(^|[^._A-Z0-9])([$]?)([A-Z]{1,2}|[A-W][A-Z]{2}|X[A-E][A-Z]|XF[A-D])([$]?)([1-9]\d{0,5}|10[0-3]\d{4}|104[0-7]\d{3}|1048[0-4]\d{2}|10485[0-6]\d|104857[0-6])(?![_.\(A-Za-z0-9])/g;

function delete_cols(ws, start_col, ncols) {
    if (!ws) throw new Error("operation expects a worksheet");
    var dense = Array.isArray(ws);
    if (!ncols) ncols = 1;
    if (!start_col) start_col = 0;

    /* extract original range */
    var range = XLSX.utils.decode_range(ws["!ref"]);
    var R = 0, C = 0;

    var formula_cb = function ($0, $1, $2, $3, $4, $5) {
        var _R = XLSX.utils.decode_row($5), _C = XLSX.utils.decode_col($3);
        if (_C >= start_col) {
            _C -= ncols;
            if (_C < start_col) return "#REF!";
        }
        return $1 + ($2 == "$" ? $2 + $3 : XLSX.utils.encode_col(_C)) + ($4 == "$" ? $4 + $5 : XLSX.utils.encode_row(_R));
    };

    var addr, naddr;
    for (C = start_col + ncols; C <= range.e.c; ++C) {
        for (R = range.s.r; R <= range.e.r; ++R) {
            addr = XLSX.utils.encode_cell({ r: R, c: C });
            naddr = XLSX.utils.encode_cell({ r: R, c: C - ncols });
            if (!ws[addr]) { delete ws[naddr]; continue; }
            if (ws[addr].f) ws[addr].f = ws[addr].f.replace(crefregex, formula_cb);
            ws[naddr] = ws[addr];
        }
    }
    for (C = range.e.c; C > range.e.c - ncols; --C) {
        for (R = range.s.r; R <= range.e.r; ++R) {
            addr = XLSX.utils.encode_cell({ r: R, c: C });
            delete ws[addr];
        }
    }
    for (C = 0; C < start_col; ++C) {
        for (R = range.s.r; R <= range.e.r; ++R) {
            addr = XLSX.utils.encode_cell({ r: R, c: C });
            if (ws[addr] && ws[addr].f) ws[addr].f = ws[addr].f.replace(crefregex, formula_cb);
        }
    }

    /* write new range */
    range.e.c -= ncols;
    if (range.e.c < range.s.c) range.e.c = range.s.c;
    ws["!ref"] = XLSX.utils.encode_range(clamp_range(range));

    /* merge cells */
    if (ws["!merges"]) ws["!merges"].forEach(function (merge, idx) {
        var mergerange;
        switch (typeof merge) {
            case 'string': mergerange = XLSX.utils.decode_range(merge); break;
            case 'object': mergerange = merge; break;
            default: throw new Error("Unexpected merge ref " + merge);
        }
        if (mergerange.s.c >= start_col) {
            mergerange.s.c = Math.max(mergerange.s.c - ncols, start_col);
            if (mergerange.e.c < start_col + ncols) { delete ws["!merges"][idx]; return; }
            mergerange.e.c -= ncols;
            if (mergerange.e.c < mergerange.s.c) { delete ws["!merges"][idx]; return; }
        } else if (mergerange.e.c >= start_col) mergerange.e.c = Math.max(mergerange.e.c - ncols, start_col);
        clamp_range(mergerange);
        ws["!merges"][idx] = mergerange;
    });
    if (ws["!merges"]) ws["!merges"] = ws["!merges"].filter(function (x) { return !!x; });

    /* cols */
    if (ws["!cols"]) ws["!cols"].splice(start_col, ncols);
}

$('[data-action="excel-export"]').click(function () {
    var fileName = "Delayed Report - " + report_date;
    var fileType = "xlsx";
    var table = document.getElementById("excel-table");
    var wb = XLSX.utils.table_to_book(table, { sheet: "Report" });
    const ws = wb.Sheets['Report'];
    var wscols = [
        { wch: 15 },
        { wch: 25 },
        { wch: 30 },
        { wch: 25 },
        { wch: 20 },
        { wch: 30 },
        { wch: 25 },
        { wch: 20 }
    ];
    ws['!cols'] = wscols;
    delete_cols(ws, 9, 1);
    return XLSX.writeFile(wb, null || fileName + "." + (fileType || "xlsx"));
});
setTimeout(function () {
    $('.mm-loader').show();
    window.location.reload(1);
}, 60000);
if ($("table#statement-content-table").length) {
    //Customer Statement table
    var statementtable = $("table#statement-content-table").dataTable({
        sPaginationType: "full_numbers",
        "bSort": true,
        "bInfo": false,
        "bLengthChange": false,
        "bFilter": false,
        //"lengthMenu": [[50, 100, 150, 200, -1], [50, 100, 150, 200, "All"]],
        //            'columnDefs': [
        //                {
        //                   'targets': 0,
        //                   'checkboxes': {
        //                      'selectRow': true
        //                   }
        //                }
        //            ],
        //            'select': {
        //               'style': 'multi'
        //            },
        //'order': [[1, 'asc']],
        "iDisplayLength": 100
    });
    //Ends
}
$('#cstatemnet-date-from').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
});
function send_delay_sms(mob, time) {
    $.fancybox.open({
        autoCenter: true,
        fitToView: false,
        scrolling: false,
        openEffect: 'none',
        openSpeed: 1,
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.3)'
                },
                closeClick: true
            }
        },
        padding: 0,
        closeBtn: true,
        content: _alert_html = '<div id="alert-popup"><div class="head">Send SMS<span class="alert-popup-close pop_close"></span></div><div class="content padd20" style="padding:0px !important;"></div><div class="content" style="padding:20px 20px 20px 20px;"><span id="sendsms" style="color:red; display:none; padding-bottom: 10px;">Send SMS.</span><textarea style="width:100%;height:100px;" name="sms_msg" placeholder="Enter message" id="sms_msg">We are expecting a small delay for dropping the cleaner.The cleaner will reach your place at approximately ' + time + '. For further assistance please call 8009876. Thank You</textarea></div><div class="bottom"><input type="button" value="Submit" data-mobid="' + mob + '" class="report_sms" style="background:#b2d157;border:1px solid" /><p id="msg_success" style="text-align:center;"></p></div></div>',
        topRatio: 0.2,
    });
}