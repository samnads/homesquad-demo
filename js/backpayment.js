$('#customer-id').select2({
    placeholder: "Select customer",
    language: {
        searching: function () {
            return "Searching...";
        },
        noResults: function () {
            return "No customer found !";
        }
    },
    ajax: {
        url: _base_url + 'dropdown/customer',
        dataType: 'json',
        data: function (params) {
            var query = {
                search: params.term
            }
            return query;
        },
        processResults: function (data) {
            return {
                results: data.data
            };
        }
    }
});