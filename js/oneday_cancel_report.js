$('#OneDayDate').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
});
(function (a) {
    a(document).ready(function (b) {
        if (a('#da-ex-datatable-numberpaging').length > 0) {
            a("table#da-ex-datatable-numberpaging").dataTable({
                sPaginationType: "full_numbers", "bSort": false, "iDisplayLength": 100, "scrollY": true,
                "scrollX": true
            });
            a("table#da-ex-datatable-default").dataTable({ "iDisplayLength": 100 });
        }
    });
})(jQuery);