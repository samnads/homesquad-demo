$("#customers_vh_rep_new_inv").select2({
    escapeMarkup: function (markup) {
        return markup;
    },
    ajax: {
        url: _base_url + "customer/report_srch_usr_news",
        type: "post",
        dataType: 'json',
        delay: 150,
        data: function (params) {
            return {
                searchTerm: params.term,
            };
        },
        processResults: function (response) {
            if (response.id === "") {

            } else {
                return { results: response };
            }
        },
        cache: true
    },
});
$("#invoiceaddform").validate({
    focusInvalid: false,
    rules: {
        invoiceissuedate: "required",
        invoiceduedate: "required",
        customers_vh_rep_new_inv: "required",
        invoicenetamount: "required"
    },
    messages: {
        invoiceissuedate: "Please enter invoice issue date",
        invoiceduedate: "Please enter invoice due date",
        customers_vh_rep_new_inv: "Please select a customer",
        invoicenetamount: "Enter net amount"
    }
});
$('#invoiceissuedate').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
});
$('#invoiceissuedate,#invoiceduedate').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
});