
$(function(){
    
   $('#per-page').change(function(){
      //window.location = _base_url + 'customers/' + $('#per-page').val() + '/' + $('#all-customers').val(); 
      window.location = _base_url + 'customers/' + $('#per-page').val();
   });
//   $('#all-customers').change(function(){
//      window.location = _base_url + 'customers/' + $('#per-page').val() + '/' + $('#all-customers').val(); 
//   });
    $('#all-maids').change(function(){
      //window.location = _base_url + 'customers/' + $('#per-page').val() + '/' + $('#all-customers').val(); 
      window.location = _base_url + 'maids/' + $('#all-maids').val();
   });
   $('#keyword-search').keyup(function(e){
       
       var _keyword = $('#keyword-search').val();
       if(e.keyCode == 13)
       {
            $.ajax({
               type : "POST",
               url : _base_url + 'customer/search' ,
               data : {search_keyword : _keyword},
               dataType : 'text',
               cache : false,
               success : function(response)
               {
                   $('#customer-list tbody').html(response);
                   $('.widget-content').next('p').remove();
               }

            });
        }
   });
   
   
   
var bookId = $('#booking_ID').val();
if(bookId > 0 ){
    $(".booking-tab a")[0].click();
    $('#btn-search-maid').click();
}


   $('#cust_from_date,#cust_to_date,#payment_type,#all-customers,#sort_custmer,#sort_source').on('change', function (e) {
       if($('#checkdate:checked').val() == 1)
       {
           var checkval = $('#checkdate:checked').val();
       } else {
           var checkval = 0;
       }
        var dataString = "";
        if ($('#from_date').val() != "")
            dataString+= "from_date=" + $('#cust_from_date').val() + "&";
        if ($('#to_date').val() != "")
            dataString+= "to_date=" + $('#cust_to_date').val() + "&";
        if($('#payment_type').val() != "")
            dataString+= "payment_type=" + $('#payment_type').val() + "&";
        if($('#all-customers').val() != "")
            dataString+= "all_customers=" + $('#all-customers').val() + "&";
        if($('#sort_custmer').val() != "")
            dataString+= "sort_custmer=" + $('#sort_custmer').val() + "&";
        if($('#sort_source').val() != "")
            dataString+= "sort_source=" + $('#sort_source').val() + "&";
        if($('#checkdate').val() != "")
            dataString+= "checkdate=" + checkval + "&";
        if (dataString != "") {
            //alert(dataString);
            $("#LoadingImage").show();
            $.ajax({
                type: "POST",
                url: _base_url + 'customer/search_by_date',
                data: dataString,
                dataType: 'text',
                cache: false,
                success: function (response)
                {
                    if (response) {
                        if(response == "no")
                        {
                            $("#LoadingImage").hide();
                            alert("Please Remove any one of the date you entered and Try again.");
                        } else {
                            //alert(response);
                        $("#LoadingImage").hide();
                        $('#customer-list tbody').html(response);
                        $('.widget-content').next('p').remove();
                    }
                    }
                }

            });
        }
    });
    
    $('#doj').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        //startDate: new Date()
    });
});

$(function () {
    $('#passport_expiry, #visa_expiry, #labour_expiry, #emirates_expiry, #zone_date,#cust_from_date,#cust_to_date, #schedule_date, #vehicle_date, #payment_date, #b-date-from, #b-date-to,#OneDayDate,#ActFromDate,#ActToDate').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        //startDate: new Date()
    });
     //$("#booking_date").val(moment().format('DD/MM/YYYY'));
//     var nowFrom = moment().format('h:00 a');
//     $("#book-from-time").val(nowFrom);
 $('.end_datepicker').datepicker({
				format: 'dd/mm/yyyy',
				autoclose: true,
				startDate: new Date()
			});

//Setting from time as 8 Am default - customer booking module starts

//$("#book-from-time option").each(function() {
//    if($(this).text() == "8:00 am") {
//       $("#book-from-time").val($(this).val());
//    }                        
//});

//Setting from time as 8 Am default - customer booking module ends

//Setting to time as 12 Pm default - customer booking module starts

//$("#book-to-time option").each(function() {
//    if($(this).text() == "12:00 pm") {
//       $("#book-to-time").val($(this).val());
//    }                        
//});

//Setting to time as 12 Pm default - customer booking module ends


    $("#booking_date").datepicker({
        format: 'dd/mm/yyyy',
       autoclose: true,
      startDate: new Date() ,
  });
});
if($('.sel2').length > 0) $('.sel2').select2();

(function (a) {
    a(document).ready(function (b) {
        if(a('#da-ex-datatable-numberpaging').length > 0)
        {
        a("table#da-ex-datatable-numberpaging").dataTable({sPaginationType: "full_numbers", "bSort": false, "iDisplayLength": 100});
        a("table#da-ex-datatable-default").dataTable({"iDisplayLength": 100});
    }
        
    });
    
})(jQuery);





$(document).ready(function () {


    var max_fields = 10; //maximum input boxes allowed
    var wrapper = $(".input_fields_wrap"); //Fields wrapper
    var add_button = $(".add_field_button"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function (e) { //on add input button click
        var areas = $('#area').html();
        //alert(areas);

        e.preventDefault();
        if (x < max_fields) { //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div><div class="control-group"><label class="control-label" for="basicinput">Area</label><div class="controls"><select name="area[]" id="area" class="span3">' + areas + '</select></div></div><div class="control-group"><label class="control-label" for="firstname">Address</label><div class="controls"><textarea class="span3" rows="5" id="address" name="address[]"></textarea><input type="hidden" name="address_id[]" value="" /></div></div><a style="float:right ; margin-right:10px; cursor:pointer;" href="#" class="remove_field">Remove</a></div>'); //add input box
        }
    });
    $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
        e.preventDefault();
        $(this).parent('div').remove();
        x--;
    });
	


});

$(document).ready(function() {
    $('input[type=radio][name=company]').change(function() {
        if (this.value == 'Y') {
            $('#company_source').css('display','block');
        }
        else if (this.value == 'N') {
            $('#company_source').css('display','none');
        }
    });
});
//$(document).on('click', '#btn-book-maid', function(){
$(document).on('click', '.book_maid_list', function(){
  
var id_maid_data = $(this).attr('id');
var id_maid = id_maid_data.split('maid_');
//alert(id_maid[1]);
var _maid_name = $.trim($('#name_'+id_maid[1]).html());


$.fancybox.open({
		autoCenter : true,
		fitToView : false,
		scrolling : false,
		openEffect : 'none',
		openSpeed : 1,
		helpers : {
			overlay : {
				css : {
					'background' : 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: true
			}
		},
		padding : 0,
		closeBtn : true,
		content : _alert_html = '<div id="alert-popup"><div class="head">Book<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Are you sure want to continue?</div><div class="bottom"><input type="button" value="Yes" class="pop_close book_yes" style="background:#b2d157;border:1px solid" id="'+id_maid[1]+'"  />&nbsp;&nbsp;<input type="button" value="No" class="book_no pop_close"  /></div></div>',
		topRatio : 0.2,
		
	});                              
                                        
                                        
                                        
}); 

$(document).on('click', '.book_yes', function(){
 $.fancybox.close();
$("#LoadingImage").show();											  
var _maid_id = $(this).attr('id');

var _booking_ID = $.trim($('#booking_ID').val()); 
var _action='book-maid';
if(_booking_ID!="")
{
 var _action='assign-maid';   
}
var _service_date = $('#booking_date').val();
var _customer_id = $.trim($('#cust_id').val());
var _customer_address_id = $.trim($('#cust_add_id').val());
var _from_time = $.trim($('#book-from-time').val());
var _to_time = $.trim($('#book-to-time').val());
var _booking_type = $.trim($('#book-booking-type').val());   

var _repeat_end = '';
var _repeat_end_date = '';
	if(_booking_type == 'WE')
	{	
		
		  
		 		  
		  _repeat_end = $('input[name="repeat_end"]:checked').val();
		  if(_repeat_end == 'ondate')
		  {
			  _repeat_end_date = $('#repeat-end-date').val();
			 
		  }
	  }

$.post( _page_url, { action: _action, booking_id:_booking_ID, customer_id: _customer_id, customer_address_id: _customer_address_id, maid_id: _maid_id, service_date:_service_date, time_from: _from_time, time_to: _to_time, booking_type: _booking_type, repeat_end: _repeat_end, repeat_end_date: _repeat_end_date}, function(response) {
		_bpop_open = false;
		//refresh_grid();
		var _alert_html = '';
			
		if(response == 'refresh')
		{
              $("#LoadingImage").hide();  
			_alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="alert-popup-close cancel_btn pop_close" style="float:none;"  /></div></div>';
			_refresh_page = true;
		}
		else
		{
              
//alert('ESTUS-'+_resp.status)
			var _resp = $.parseJSON(response);
			if(typeof _resp.status && _resp.status == 'success')
			{	
				$("#LoadingImage").hide();
				_alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Booking has been done successfully.</div><div class="bottom"><input type="button" value="OK" class="pop_close alert-popup-close" style="float:none;" /></div></div>';
				_refresh_page = false
			}
			else if(typeof _resp.status && _resp.status == 'error')
			{
				 $("#LoadingImage").hide();
				_alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close"></span></div><div class="content padd20">'+ _resp.message+'</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close alert-popup-close" style="float:none;"  /></div></div>';
				_refresh_page = true
			}
			else
			{
				 $("#LoadingImage").hide();
				_alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close alert-popup-close" style="float:none;"  /></div></div>';
				_refresh_page = true
			}
		}
//alert('--alertHtml'+_alert_html);		
		if(_alert_html  != '')
		{
			$.fancybox.open({
				autoCenter : true,
				fitToView : false,
				scrolling : false,
				openEffect : 'fade',
				openSpeed : 100,
				helpers : {
					overlay : {
						css : {
							'background' : 'rgba(0, 0, 0, 0.3)'
						},
						closeClick: false
					}
				},
				padding : 0,
				closeBtn : false,
				content: _alert_html
			});
		}
	});	  
    
    
});

$(document).on('click', '.book_no,.alert-popup-close', function(){
											  
$.fancybox.close();
											  
});
    


/*image uploader*/

$(function () {
    var btnUpload = $('#me');
    var mestatus = $('#mestatus');
    var files = $('#files');
    var call_method = $('#call_method').val();
    var img_fold = $('#img_fold').val();
    var fileObj = $('input[type="file"]');
    var up_archive = new AjaxUpload(btnUpload, {
        action: _base_url + call_method,
        name: 'uploadfile',
        onSubmit: function (file, ext) {
            if (!(ext && /^(jpg|png|jpeg)$/.test(ext))) {
                alert('Only JPG,PNG files are allowed');
                mestatus.text('Only PNG files are allowed');
                return false;
            }

        },
        onChange: function (file, ext) {
            if (up_archive._input.files[0].size >= 1048576) { //1MB
                alert('Selected file is bigger than 1MB.');
                return false;
            }
        },
        onComplete: function (file, response) {
            //On completion clear the status
            mestatus.text('');
            //On completion clear the status
            $('#me span').html('<img src="' + _base_url + 'images/ajax-loader.gif" height="16" width="16">');
            //Add uploaded file to list
            if (response == "error") {
                $('<li style="list-style: none;margin-left: 0px;"></li>').appendTo('#me span').text(file).addClass('error');
            } else {
                $('#me span').html('');
                $('#img_name_resp').val(response);
                $('<li style="list-style: none;margin-left: 0px;"></li>').appendTo('#me span').html('<img height="135" width="95" src="' + _base_url + '' + img_fold + '/' + response + '"/><br />').addClass('success');
            }
        }
    });

});

function add_zone()
{
    $('#add_zone').show();
    $('#edit_zone').hide();
}
function hideadd_zone()
{
    $('#add_zone').hide();
}
function add_area()
{
    $('#add_area').show();
    $('#edit_area').hide();
}
function hideadd_area()
{
    $('#add_area').hide();
}
function add_flat()
{
    $('#add_flat').show();
    $('#edit_flat').hide();
}
function hideadd_flat()
{
    $('#add_flat').hide();
}
function add_services()
{
    $('#add_services').show();
    $('#edit_services').hide();
}
function hideadd_services()
{
    $('#add_services').hide();
}
function add_tablet()
{
    $('#add_tablet').show();
    $('#edit_tablet').hide();
}
function hideadd_tablet()
{
    $('#add_tablet').hide();
}
function hideedit_zone()
{
    $('#edit_zone').hide();
}
function edit_zone(zone_id)
{
    //alert(zone_id);
    $.ajax({
        type: "POST",
        url: _base_url + "settings/edit_zone",
        data: {zone_id: zone_id},
        dataType: "text",
        cache: false,
        success: function (result) {
            //alert(result);
            var obj = jQuery.parseJSON(result);
            $.each($.parseJSON(result), function (edit, value) {
                $('#edit_zoneid').val(value.zone_id)
                $('#edit_zonename').val(value.zone_name)
                $('#edit_drivername').val(value.driver_name)
                $('input[value="' + value.spare_zone + '"]').prop('checked', true);

            });
            $('#add_zone').hide();
            $('#edit_zone').show();

        }
    });
}
function delete_zone(zone_id)
{
    if (confirm('Are you sure you want to delete this Zone?'))
    {
        $.ajax({
            type: "POST",
            url: _base_url + "settings/remove_zone",
            data: {zone_id: zone_id},
            dataType: "text",
            cache: false,
            success: function (result) {
                if (result == 1)
                {
                    alert("Cannot Delete! This Zones has Areas");
                }
                else
                {
                    alert("Zone Deleted Successfully");
                }
                window.location.assign(_base_url + 'settings/');
            }
        });

    }
}
function hideedit_area()
{
    $('#edit_area').hide();
}
function edit_area(area_id)
{
    //alert(zone_id);
    $.ajax({
        type: "POST",
        url: _base_url + "settings/edit_area",
        data: {area_id: area_id},
        dataType: "text",
        cache: false,
        success: function (result) {
            //alert(result);
            var obj = jQuery.parseJSON(result);
            $.each($.parseJSON(result), function (edit, value) {
                //alert(value.zone_id);
                $('#edit_areaid').val(value.area_id)
                $('#edit_areaname').val(value.area_name)
                $('#edit_zone_id option[value="' + value.zone_id + '"]').prop('selected', true);

            });
            $('#add_area').hide();
            $('#edit_area').show();


        }
    });
}
function delete_area(area_id)
{
    if (confirm('Are you sure you want to delete this Area'))
    {
        $.ajax({
            type: "POST",
            url: _base_url + "settings/remove_area",
            data: {area_id: area_id},
            dataType: "text",
            cache: false,
            success: function (result) {
                window.location.assign(_base_url + 'settings/area');
            }
        });
    }
}
function hideedit_flat()
{
    $('#edit_flat').hide();
}
function edit_flat(flat_id)
{
    //alert(flat_id);
    $.ajax({
        type: "POST",
        url: _base_url + "settings/edit_flat",
        data: {flat_id: flat_id},
        dataType: "text",
        cache: false,
        success: function (result) {
            //alert(result);
            var obj = jQuery.parseJSON(result);
            $.each($.parseJSON(result), function (edit, value) {
                $('#edit_flatid').val(value.flat_id);
                $('#edit_flatname').val(value.flat_name);
                $('#edit_tablet_imei').val(value.tablet_imei);

            });
            $('#add_flat').hide();
            $('#edit_flat').show();


        }
    });
}
function delete_flat(flat_id)
{
    if (confirm('Are you sure you want to delete this Flat'))
    {
        $.ajax({
            type: "POST",
            url: _base_url + "settings/remove_flat",
            data: {flat_id: flat_id},
            dataType: "text",
            cache: false,
            success: function (result) {
                window.location.assign(_base_url + 'settings/flats');
            }
        });
    }
}
function hideedit_services()
{
    $('#edit_services').hide();
}
function edit_services(service_id)
{
    $.ajax({
        type: "POST",
        url: _base_url + "settings/edit_services",
        data: {service_id: service_id},
        dataType: "text",
        cache: false,
        success: function (result) {
            //alert(result);
            var obj = jQuery.parseJSON(result);
            $.each($.parseJSON(result), function (edit, value) {
                $('#edit_service_type_id').val(value.service_type_id);
                $('#edit_service_type_name').val(value.service_type_name);
                $('#edit_service_rate').val(value.service_rate);

            });

            $('#add_services').hide();
            $('#edit_services').show();

        }
    });
}
function delete_services(service_id)
{
    if (confirm('Are you sure you want to delete this Services'))
    {
        $.ajax({
            type: "POST",
            url: _base_url + "settings/remove_services",
            data: {service_id: service_id},
            dataType: "text",
            cache: false,
            success: function (result) {
                window.location.assign(_base_url + 'settings/services');
            }
        });
    }
}
function hideedit_tablet()
{
    $('#edit_tablet').hide();
}
function edit_tablet(tablet_id)
{
    $.ajax({
        type: "POST",
        url: _base_url + "settings/edit_tablet",
        data: {tablet_id: tablet_id},
        dataType: "text",
        cache: false,
        success: function (result) {
            //alert(result);
            var obj = jQuery.parseJSON(result);
            $.each($.parseJSON(result), function (edit, value) {
                //alert(value.zone_id);
                $('#edit_tabletid').val(value.tablet_id)
                $('#edit_access_code').html(value.access_code)
                $('#edit_imei').val(value.imei)
                $('#edit_zone_id option[value="' + value.zone_id + '"]').prop('selected', true);

            });
            $('#add_tablet').hide();
            $('#edit_tablet').show();
        }
    });
}
function tablet_status(tablet_id, status)
{
    if (status === 1)
    {
        if (confirm('Are you sure you want to Diasble this Tablet'))
        {
            $.ajax({
                type: "POST",
                url: _base_url + "settings/tablet_status",
                data: {tablet_id: tablet_id, status: status},
                dataType: "text",
                cache: false,
                success: function (result) {
                    window.location.assign(_base_url + 'tablets');
                }
            });
        }
    }
    else if (status === 0)
    {
        if (confirm('Are you sure you want to Activate this Tablet'))
        {
            $.ajax({
                type: "POST",
                url: _base_url + "settings/tablet_status",
                data: {tablet_id: tablet_id, status: status},
                dataType: "text",
                cache: false,
                success: function (result) {
                    window.location.assign(_base_url + 'tablets');
                }
            });
        }
    }

}
function delete_maid(maid_id)
{
    if (confirm('Are you sure you want to delete this Maid'))
    {
        $.ajax({
            type: "POST",
            url: _base_url + "maid/remove_maid",
            data: {maid_id: maid_id},
            dataType: "text",
            cache: false,
            success: function (result) {
                window.location.assign(_base_url + 'maid/maid_list');
            }
        });
    }
}
function view_maid(maid_id)
{
    var gender;
    var p_date;
    var p_date_str;
    var v_date;
    var v_date_str;
    var l_date;
    var l_date_str;
    var e_date;
    var e_date_str;

    $.ajax({
        type: "POST",
        url: _base_url + "maid/view_maid",
        data: {maid_id: maid_id},
        dataType: "text",
        cache: false,
        success: function (result) {
            //alert(result);
            var obj = jQuery.parseJSON(result);
            $.each($.parseJSON(result), function (view, value) {

                if (value.maid_visa_expiry_date == "0000-00-00")
                {
                    p_date_str = "";
                }
                else {
                    p_date = new Date(value.maid_passport_expiry_date);
                    p_date_str = moment(p_date).format("DD/MM/YYYY");
                }
                if (value.maid_visa_expiry_date == "0000-00-00")
                {
                    v_date_str = "";
                }
                else {
                    v_date = new Date(value.maid_visa_expiry_date);
                    v_date_str = moment(v_date).format("DD/MM/YYYY");
                }
                if (value.maid_labour_card_expiry_date == "0000-00-00")
                {
                    l_date_str = "";
                }
                else {
                    l_date = new Date(value.maid_labour_card_expiry_date);
                    l_date_str = moment(l_date).format("DD/MM/YYYY");
                }

                if (value.maid_emirates_expiry_date == "0000-00-00")
                {
                    e_date_str = "";
                }
                else {
                    e_date = new Date(value.maid_emirates_expiry_date);
                    e_date_str = moment(e_date).format("DD/MM/YYYY");
                }

                if (value.maid_gender == "F")
                {
                    gender = "Female";
                }
                else
                {
                    gender = "Male";
                }
                if (value.maid_photo_file == "")
                {
                    photo = _base_url + "img/no_image.jpg";
                }
                else
                {
                    photo = _base_url + "maidimg/" + value.maid_photo_file;
                }



                $('#maid_photo').attr('src', photo);
                $('#maid_name').html(value.maid_name);
                $('#maid_gender').html(gender);
                $('#maid_nationality').html(value.maid_nationality);
                $('#maid_present_address').html(value.maid_present_address);
                $('#maid_permanent_address').html(value.maid_permanent_address);
                $('#maid_mobile_1').html(value.maid_mobile_1);
                $('#maid_mobile_2').html(value.maid_mobile_2);
                $('#flat_name').html(value.flat_name);
                $('#maid_passport_number').html(value.maid_passport_number);
                $('#maid_passport_expiry_date').html(p_date_str);
                $('#maid_visa_number').html(value.maid_visa_number);
                $('#maid_visa_expiry_date').html(v_date_str);
                $('#maid_labour_card_number').html(value.maid_labour_card_number);
                $('#maid_labour_card_expiry_date').html(l_date_str);
                $('#maid_emirates_id').html(value.maid_emirates_id);
                $('#maid_emirates_expiry_date').html(e_date_str);
                $('#maid_notes').html(value.maid_notes);

            });
        }
    });
    $('#myModal').modal({backdrop: true, keyboard: true, show: true, width: "1000"});
}
function view_customer(customer_id)
{
    var j = 0;
    var customer_type;
    var payment_type;
    $.ajax({
        type: "POST",
        url: _base_url + "customer/view_customer",
        data: {customer_id: customer_id},
        dataType: "text",
        cache: false,
        success: function (result) {
            //alert(result);                
            var obj = jQuery.parseJSON(result);
            $.each($.parseJSON(result), function (view, value) {
                if (value.customer_photo_file == "")
                {
                    photo = _base_url + "img/no_image.jpg";
                }
                else
                {
                    photo = _base_url + "customer_img/" + value.customer_photo_file;
                }
                if (value.customer_type == "HO")
                {
                    customer_type = "Home";
                }
                else if (value.customer_type == "OF")
                {
                    customer_type = "Office";
                }
                else if (value.customer_type == "WH")
                {
                    customer_type = "Warehouse";
                }
                if (value.payment_type == "D")
                {
                    payment_type = "Daily Paying";
                }
                else if (value.payment_type == "W")
                {
                    payment_type = "Weekly Paying";
                }
                else if (value.payment_type == "M")
                {
                    payment_type = "Monthly Paying";
                }
                $('#customer_photo').attr('src', photo);
                $('#customer_name').html(value.customer_name);
                $('#customer_nick_name').html(value.customer_nick_name);
                $('#mobile_number_1').html(value.mobile_number_1);
                $('#mobile_number_2').html(value.mobile_number_2);
                $('#mobile_number_3').html(value.mobile_number_3);
                $('#phone_number').html(value.phone_number);
                $('#fax_number').html(value.fax_number);
                $('#email_address').html(value.email_address);
                $('#website_url').html(value.website_url);
                $('#customer_type').html(customer_type);
                $('#contact_person').html(value.contact_person);
                $('#payment_type').html(payment_type);
                $('#payment_mode').html(value.payment_mode);
                $('#price_hourly').html(value.price_hourly);
                $('#price_extra').html(value.price_extra);
                $('#price_weekend').html(value.price_weekend);
                $('#customer_notes').html(value.customer_notes);

                $("#cust_add").html("");
                $.each(value.address, function (see, val) {
                    //alert(val.customer_address);
                    $('#cust_add').append('<span><b>Address</b> &nbsp;' + (++j) + '&nbsp;:&nbsp;' + val.customer_address + '</span><br><span>Area (' + val.area_name + ')</span><br>');

//                        alert(val.customer_address_id);
//                        alert(val.customer_address);
//                        alert(val.area_name);
                });
            });
        }
    });
    $('#myModal').modal({backdrop: true, keyboard: true, show: true, width: "1000"});
}
function cust_address(cust_id)
{
    //alert(cust_id);
    $.ajax({
        type: "POST",
        url: _base_url + "customer/view_customer_address",
        data: {cust_id: cust_id},
        dataType: "text",
        cache: false,
        success: function (result) {
            //alert(result);                
            var obj = jQuery.parseJSON(result);
            //alert(obj[0].customer_address);
            $.each(obj, function (view, value) {
                $('#cust_name').html(value.customer_name);
                $('#customer_add').html(value.customer_address);
            });
        }
    });
    $('#cust_address').modal({backdrop: true, keyboard: true, show: true, width: "1000"});
}
function remove_address(address_id, customer_id)
{
    if (confirm('Are you sure you want to delete this Address'))
    {
        $.ajax({
            type: "POST",
            url: _base_url + "customer/remove_customer_address",
            data: {address_id: address_id},
            dataType: "text",
            cache: false,
            success: function (result) {
                window.location.assign(_base_url + 'customer/edit/' + customer_id);
            }
        });
    }
}
function delete_customer($this, customer_id, customer_status)
{
    var _lblstatus = customer_status == 1 ? 'disable' : 'enable';
    if (confirm('Are you sure you want to ' + _lblstatus + ' this customer'))
    {
        $.ajax({
            type: "POST",
            url: _base_url + "customer/remove_customer",
            data: {customer_id: customer_id, customer_status : customer_status},
            dataType: "text",
            cache: false,
            success: function (result) {
                //window.location.assign(_base_url + 'customers');
                if(result == 1)
                {
                    $($this).attr('class', 'btn btn-success btn-small');
                    $($this).html('<i class="btn-icon-only icon-ok"></i>');
                }
                else
                {
                    if(result == 'exist_bookings') // Edited by Geethu
                    {
                        alert('Warning! Can\'t deactivate this customer, have some active bookings.');
                        result = 0;
                    }
                    else
                    {
                        $($this).attr('class', 'btn btn-danger btn-small');
                        $($this).html('<i class="btn-icon-only icon-remove"> </i>');
                    }
                    
                }
                $($this).attr('onclick', 'delete_customer(this, ' + customer_id +', ' + result +')');
            }
        });
    }
}
function change_status(maid_id,status)
{
    if(status === 1)
    {
        if (confirm('Are you sure you want to Diasble this Maid'))
        {
            $.ajax({
                type: "POST",
                url: _base_url + "maid/change_status",
                data: {maid_id: maid_id,status:status},
                dataType: "text",
                cache: false,
                success: function (result) {
                    window.location.assign(_base_url + 'maids');
                }
            });
        }
    }
    else if(status === 0)
    {
        if (confirm('Are you sure you want to Activate this Maid'))
        {
            $.ajax({
                type: "POST",
                url: _base_url + "maid/change_status",
                data: {maid_id: maid_id,status:status},
                dataType: "text",
                cache: false,
                success: function (result) {
                    window.location.assign(_base_url + 'maids');
                }
            });
        }
    }
    
}
function validate_maid()
{
    if ($('#maid_name').val() == '')
    {
        alert('Please Enter Maid Name');
        return false;
    }
    if($('#nationality').val() == '')
    {
        alert('Please Enter Nationality');
        return false;
    }
    if($('#present_address').val() == '')   
    {
        alert('Please Enter Present Address');
        return false;
    }
    if($('#permanent_address').val() == '')
    {
        alert('Please Enter Permanent Address');
        return false;
    }
    if(($('#mobile1').val() == '') || (!$.isNumeric($('#mobile1').val())))
    {
        alert('Please Enter a Valid Mobile Number');
        return false;
    }
    
    if(!($('.services').is(':checked')))
    {
        alert('Please Select Services');
        return false;
    }
    return true;
}
function validate_customer()
{
    if ($('#customer_name').val() === '')
    {
        alert('Please Enter Customer Name');
        return false;
    }
    if($('#customer_nick').val() === '')
    {
        alert('Please Enter Customer Nick Name');
        return false;
    }
    if($('#area').val() === '')
    {
        alert('Please Enter Area');
        return false;
    }
    if($('#address').val() === '')
    {
        alert('Please Enter Address');
        return false;
    }
    if($('#contact_person').val() === '')
    {
        alert('Please Enter Contact Person');
        return false;
    }
    if(($('#mobile_number1').val() === '') || (!$.isNumeric($('#mobile_number1').val())))
    {
        alert('Please Enter a Valid Mobile Number');
        return false;
    }
//    if($('#user_name').val() === '')
//    {
//        alert('Please Enter User Name');
//        return false;
//    }
    if($('#email').val() !== '')
    {
        //var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        var pattern = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
        if (!pattern.test($('#email').val())){                
                
            alert('Please Enter a Valid Email Address');
            return false;
        } 
        
    }
//    if($('#password1').val() === '')
//    {
//        alert('Please Enter Password');
//        return false;
//    }
    if(($('#hourly').val() === '') || (!$.isNumeric($('#hourly').val())))
    {
        alert('Please Enter a Valid Hourly Price');
        return false;
    }
    if(($('#extra').val() === '') || (!$.isNumeric($('#extra').val())))
    {
        alert('Please Enter a Valid Extra Price');
        return false;
    }
    if(($('#weekend').val() === '') || (!$.isNumeric($('#weekend').val())))
    {
        alert('Please Enter a Valid Weekend Price');
        return false;
    }
    
    return true;
}
$("#printButton").click(function ()
{
    var divContents = $("#divToPrint").html();
    var date = $("#zone_date").val();
    var zone = $("#zone_name").val();
    var day = $("#day").val();
   
    
    var printWindow = window.open('', '', 'height=400,width=800');
    printWindow.document.write('<html><head><title></title>');  
    printWindow.document.write('</head><body >');
    printWindow.document.write('<h1 align="center">Zone Report</h1>');
    printWindow.document.write('<br />');
    printWindow.document.write('Date &nbsp;&nbsp;&nbsp; : &nbsp;');
    printWindow.document.write(date);
    printWindow.document.write('&nbsp;('+day+')');
    printWindow.document.write('&nbsp;&nbsp;&nbsp;&nbsp;');
    printWindow.document.write('<span style="margin-left:300px;">');
    printWindow.document.write('Zone &nbsp;&nbsp;&nbsp; : &nbsp;');
    printWindow.document.write(zone);
    printWindow.document.write('</span>');
    printWindow.document.write('<br /><br />');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    printWindow.document.close();
    printWindow.print();

});


$("#printBtn").click(function ()
{
    var divContents = $("#divPrint").html();
    var date = $("#vehicle_date").val();
    var zone = $("#zone_name").val();
    var day = $("#day").val();
  
    var printWindow = window.open('', '', 'height=400,width=800');
    printWindow.document.write('<html><head><title></title>');  
    printWindow.document.write('</head><body >');
    printWindow.document.write('<h1 align="center">Vehicle Report</h1>');
    printWindow.document.write('<br />');
    printWindow.document.write('Date &nbsp;&nbsp;&nbsp; : &nbsp;');
    printWindow.document.write(date);
    printWindow.document.write('&nbsp;('+day+')');
    printWindow.document.write('&nbsp;&nbsp;&nbsp;&nbsp;');
    printWindow.document.write('<span style="margin-left:300px;">');
    printWindow.document.write('Zone &nbsp;&nbsp;&nbsp; : &nbsp;');
    printWindow.document.write(zone);
    printWindow.document.write('</span>');
    printWindow.document.write('<br /><br />');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    printWindow.document.close();
    printWindow.print();

});


$("#printButn").click(function ()
{
    var divContents = $("#divForPrint").html();
    var date = $("#payment_date").val();
    var zone = $("#zone_name").val();
    var day = $("#day").val();
  
    var printWindow = window.open('', '', 'height=400,width=800');
    printWindow.document.write('<html><head><title></title>');  
    printWindow.document.write('</head><body >');
    printWindow.document.write('<h1 align="center">Payment Report</h1>');
    printWindow.document.write('<br />');
    printWindow.document.write('Date &nbsp;&nbsp;&nbsp; : &nbsp;');
    printWindow.document.write(date);
    printWindow.document.write('&nbsp;('+day+')');
    printWindow.document.write('&nbsp;&nbsp;&nbsp;&nbsp;');
    printWindow.document.write('<span style="margin-left:300px;">');
    printWindow.document.write('Zone &nbsp;&nbsp;&nbsp; : &nbsp;');
    printWindow.document.write(zone);
    printWindow.document.write('</span>');
    printWindow.document.write('<br /><br />');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    printWindow.document.close();
    printWindow.print();

});



$('#btn-search-booking').click(function(){
    $.post(_page_url, {action : 'search-booking', date_from : $('#b-date-from').val(), date_to : $('#b-date-to').val()}, function(response)
    {
        $('#pause-booking table > tbody').html(response);
    })
    
});


//Edited by Aparna

function deleteOnedayCancel($this, id) {
    if (confirm("This booking will no longer be in canceled list.Are you sre to delete this?")) {
        $.ajax({
            url: _base_url + 'reports/delete_onedaycancel',
            type: 'POST',
            data: {id: id},
            success: function (result) {
                if (result == "success")
                    window.location.assign(_base_url + 'reports/one_day_cancel');
                else
                    alert(result);
            }
        });
    }
}
$("#EmpWorkPrint").click(function ()
{
    var divContents = $("#employeeWorkPrint").html();
    var maid = $('#maid option:selected').text();
    var month = $('#month').val();
    var year = $('#year').val();

    var printWindow = window.open('', '', 'height=400,width=800');
    printWindow.document.write('<html><head><title></title>');
    printWindow.document.write('</head><body >');
    printWindow.document.write('<h1 align="center">Employee Work Report</h1>');
    printWindow.document.write('<br />');
    printWindow.document.write('&nbsp;&nbsp;<b>Employee</b>&nbsp;: &nbsp;');
    printWindow.document.write(maid);
    printWindow.document.write('&nbsp;&nbsp;<b>Month</b>&nbsp;: &nbsp;');
    printWindow.document.write(month);
    printWindow.document.write('&nbsp;&nbsp;<b>Year</b>&nbsp;&nbsp;: &nbsp;');
    printWindow.document.write(year);
    printWindow.document.write('<br /><br />');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    printWindow.document.close();
    printWindow.print();

});
$("#OneDayPrint").click(function ()
{
    var divContents = $("#OneDayReportPrint").html();
    var date = $('#OneDayDate').val();

    var printWindow = window.open('', '', 'height=400,width=800');
    printWindow.document.write('<html><head><title></title>');
    printWindow.document.write('</head><body >');
    printWindow.document.write('<h1 align="center">One Day Cancel Report</h1>');
    printWindow.document.write('<br />');
    printWindow.document.write('&nbsp;&nbsp;<b>Date</b>&nbsp;: &nbsp;');
    printWindow.document.write(date);
    printWindow.document.write('<br /><br />');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    printWindow.document.close();
    printWindow.print();

});
$("#ActivityPrint").click(function ()
{
    var divContents = $("#ActivityReportPrint").html();
    var from_date = $('#ActFromDate').val();
    var to_date = $('#ActToDate').val();

    var printWindow = window.open('', '', 'height=400,width=800');
    printWindow.document.write('<html><head><title></title>');
    printWindow.document.write('</head><body >');
    printWindow.document.write('<h1 align="center">Activity Summary Report</h1>');
    printWindow.document.write('<br />');
    printWindow.document.write('&nbsp;&nbsp;<b>From</b>&nbsp;: &nbsp;');
    printWindow.document.write(from_date);
    printWindow.document.write('&nbsp;&nbsp;<b>To</b>&nbsp;: &nbsp;');
    printWindow.document.write(to_date);
    printWindow.document.write('<br /><br />');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    printWindow.document.close();
    printWindow.print();

});

function pause_booking(booking_id, service_date, booking_type)
{
    if(confirm('Are you sure want to pause this booking?'))
    {
        $.post(_page_url, {action : 'pause-booking', booking_id : booking_id, service_date : service_date, booking_type : booking_type}, function(response){

            if(response == 'error')
            {
                alert('Unexpected error!');
            }
            else if(response == 'locked')
            {
                alert('Booking Locked by another user!');
            }
            else
            {
                $('#btn-search-booking').trigger('click');
            }

        });
    }
}

function start_booking(booking_id, service_date, booking_type)
{
    if(confirm('Are you sure want to start this booking?'))
    {
        $.post(_page_url, {action : 'start-booking', booking_id : booking_id, service_date : service_date, booking_type : booking_type}, function(response){

            if(response == 'error')
            {
                alert('Unexpected error!');
            }
            else if(response == 'locked')
            {
                alert('Booking Locked by another user!');
            }
            else
            {
                $('#btn-search-booking').trigger('click');
            }

        });
    }
}

$('#customer-id').change(function(){
    $.post(_base_url + 'settings/add_backpayment', {action : 'get-balance-amount', customer_id : $('#customer-id').val()}, function(response)
    {
        var _json = $.parseJSON(response);
        if(_json.status == 'success')
        {
            $('#requested-amount').val(_json.balance);
            $('#req-balance-amount').show();
        }
        else
        {
            alert(_json.message);
        }
    })
    
});



$('body').on('click', '#save-booking', function() {
//    alert('sdf');
	var _service_date = $('#service-date').val();
	var _maid_id = $.trim($('#maid-id').val());
	var _customer_id = $.trim($('#b-customer-id').val());
	var _customer_address_id = $.trim($('#customer-address-id').val());
	var _service_type_id = $.trim($('#b-service-type-id').val());
	var _from_time = $.trim($('#b-from-time').val());
	var _to_time = $.trim($('#b-to-time').val());
        var _lock_booking = $('#lock-booking').is(':checked') ? 1 : 0;
	var _booking_type = $.trim($('#b-booking-type').val());
	var _pending_amount = $.trim($('#b-pending_amount').val());
        var _discount = $.trim($('#b-discount').val());
	var _note = $.trim($('#booking-note').val());
	
	$('#b-error').text('');
	
	if($.isNumeric(_customer_id) == false)
	{
		$('#b-error').text('Select customer');
		return false;
	}
	
	if($.isNumeric(_customer_address_id) == false)
	{
		$('#b-error').text('Pick customer address');
		open_address_panel(_customer_id);
		return false;
	}
	
	if($.isNumeric(_service_type_id) == false)
	{
		$('#b-error').text('Select service type');
		return false;
	}
	
	if(_from_time == '' || _to_time == '')
	{
		$('#b-error').text('Select booking time');
		return false;
	}
	
	if(_booking_type == '')
	{
		$('#b-error').text('Select repeat type');
		return false;
	}
	
	var _repeat_days = [];
	var _repeat_end = '';
	var _repeat_end_date = '';
	if(_booking_type == 'WE' || _booking_type == 'BW')
	{	
		_repeat_days = $('input[id^="repeat-on-"]:checked').map(function() {
			return this.value;
		  }).get();
		  
		  if(_repeat_days.length == 0)
		  {
			  $('#b-error').text('Select repeat days');
			  return false;
		  }
		  
		  _repeat_end = $('input[name="repeat_end"]:checked').val();
		  if(_repeat_end == 'ondate')
		  {
			  _repeat_end_date = $('#repeat-end-date').val();
			  if(_repeat_end_date == '')
			  {
				  $('#b-error').text('Enter an end date');
				return false;
			  }
		  }
	  }
	  
	_refresh_page = false;
	$('#save-booking').attr('id', 'saving-booking');
	$('#saving-booking').val('Please wait...');

	$.post( _page_url, { action: 'book-maid', customer_id: _customer_id, customer_address_id: _customer_address_id, maid_id: _maid_id, service_type_id: _service_type_id, time_from: _from_time, time_to: _to_time, booking_type: _booking_type, repeat_days: _repeat_days, repeat_end: _repeat_end, repeat_end_date: _repeat_end_date, is_locked: _lock_booking, pending_amount: _pending_amount, booking_note: _note, discount : _discount }, function(response) {
		_bpop_open = false;
		//refresh_grid();
		var _alert_html = '';
			
		if(response == 'refresh')
		{
                
			_alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
			_refresh_page = true;
		}
		else
		{
              
//alert('ESTUS-'+_resp.status)
			var _resp = $.parseJSON(response);
			if(typeof _resp.status && _resp.status == 'success')
			{	
				$('#saving-booking').val('Done');
				_alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Booking has been done successfully.</div><div class="bottom"><input type="button" value="OK" class="pop_close" /></div></div>';
				_refresh_page = false
			}
			else if(typeof _resp.status && _resp.status == 'error')
			{
				$('#b-error').text(_resp.message);
				$('#saving-booking').attr('id', 'save-booking');
				$('#save-booking').val('Save');
			}
			else
			{
				_alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
				_refresh_page = true
			}
		}
//alert('--alertHtml'+_alert_html);		
		if(_alert_html  != '')
		{
			$.fancybox.open({
				autoCenter : true,
				fitToView : false,
				scrolling : false,
				openEffect : 'fade',
				openSpeed : 100,
				helpers : {
					overlay : {
						css : {
							'background' : 'rgba(0, 0, 0, 0.3)'
						},
						closeClick: false
					}
				},
				padding : 0,
				closeBtn : false,
				content: _alert_html
			});
		}
	});
});





$('body').on('click', '#copy-booking', function() {
	var _booking_id = $('#booking-id').val();
	//$('#customer-picked-address').hide();
	open_maid_panel(_booking_id);
});
function open_maid_panel(_booking_id)
{
	//$('#customer-address-id').val('');
	//$('#customer-address-panel').hide();
	//$('#customer-address-panel .inner').html('Loading<span class="dots_loader"></span>');
	
	if($.isNumeric(_booking_id) && _booking_id > 0)
	{	
		$('#maids-panel').slideDown();
		$('input[name="same_zone"]').attr('onclick', 'open_maid_panel(' + _booking_id + ')');
                
		$.post(_page_url, { action: 'get-free-maids', booking_id: _booking_id, same_zone : $('input[name="same_zone"]:checked').val() }, function(response) {
			if(response == 'refresh')
			{
				$.fancybox.open({
					autoCenter : true,
					fitToView : false,
					scrolling : false,
					openEffect : 'fade',
					openSpeed : 100,
					helpers : {
						overlay : {
							css : {
								'background' : 'rgba(0, 0, 0, 0.3)'
							},
							closeClick: false
						}
					},
					padding : 0,
					closeBtn : false,
					content:  '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>'
				});
				
				_refresh_page = true;
			}
			else
			{
                            
				var _resp = $.parseJSON(response);
                                if(typeof _resp.status && _resp.status == 'error')
                                {
                                        //$('#b-error').text(_resp.message);
                                        //$('#saving-booking').attr('id', 'save-booking');
                                        //$('#save-booking').val('Save');
                                        $('#maids-panel .inner').css("overflow","hidden");
                                        $('#maids-panel .inner').html('<div id="c-error">' + _resp.message + '</div>');
                                        
                                } 
                                else
                                {
                                    var _maid_html = '<div class="table">';

                                    var i = 0;
                                    $.each(_resp, function(key, val) {
                                        //if(i == 0)
                                        //{
                                            _maid_html += '<div class="row"><div class="cell1"><span id="mcopyadd-' + val.maid_id + '"><strong>' + val.maid_name + '</strong></span><br /><span id="mcopy-' + val.maid_id + '">' + val.maid_nationality + '</span></div><div class="cell2"><input type="button" value="Book &raquo;" id="mcopy_' + val.maid_id + '_' + _booking_id + '_' + val.service_start_date + '" class="copy_maid"  /></div></div>';
                                            //++i;
                                        //}
                                        //else if(i == 1)
                                        //{
                                            //_maid_html += '<div class="cell1"><span id="mcopyadd-' + val.maid_id + '"><strong>' + val.maid_name + '</strong></span><br /><span id="mcopy-' + val.maid_id + '">' + val.maid_nationality + '</span></div><div class="cell2"><input type="button" value="Book &raquo;" id="mcopy_' + val.maid_id + '_' + _booking_id + '_' + val.service_start_date + '" class="copy_maid"  /></div></div>';
                                            //i = 0;                                        
                                        //}

                                    });				

                                    _maid_html += '</div>';

                                    $('#maids-panel .inner').html(_maid_html);
                                }
			}
		});
	}
}




$('body').on('click', '#delete-booking', function() {
	var _booking_id = $('#booking-id').val();
	var _all_bookings = $.parseJSON($('#all-bookings').val());
	var _booking_type = _all_bookings[_booking_id].booking_type;
	
	if(_booking_type == 'OD')
	{
		$(this).parent().before('<div id="booking-action-confirm-panel"><input type="button" id="delete-permanent" value="Delete Booking" /><input type="button" id="delete-cancel" value="Cancel"></div>');
	}
	else
	{
		$(this).parent().before('<div id="booking-action-confirm-panel"><input type="button" id="delete-one-day" value="Delete One Day" /><input type="button" id="delete-permanent" value="Delete Permanently" /><input type="button" id="delete-cancel" value="Cancel"></div>');
	}
	
	$('#booking-action-confirm-panel').slideDown();
});

$('body').on('change', '#b-booking-type', function() {
	var _booking_type = $(this).val();
	if(_booking_type == 'WE' || _booking_type == 'BW')
	{
		$('#repeat-days').css('display', 'table-row');
		$('#repeat-ends').css('display', 'table-row');
	}
	else
	{
		$('#repeat-days').hide();
		$('#repeat-ends').hide();
	}
});

$('body').on('click', '#btn-search-maid , .location', function() {
	
   
    
    
    var book_date=$("#booking_date").val();
	var from_time=$("#book-from-time").val();
	var to_time=$("#book-to-time").val();
	var book_type=$("#book-booking-type").val();

        var custId=$("#cust_id").val();
        var zone=$("#cust_zone_id").val();
	var area=$("#cust_area_id").val();
	var province=$("#cust_prov_id").val();
        
        
    var location_filter_type = $(this).val();
    if(location_filter_type == 'Search') {
        location_filter_type = 'area';
    }
//        alert(location_filter_type);
    var loc_type , loc_val ;
        loc_type = location_filter_type;
    if(location_filter_type == 'zone') {
        loc_val = zone;
    }
    if(location_filter_type == 'area') {
        loc_val = area;
    }
    if(location_filter_type == 'province') {
        loc_val = province;
    }
    if((location_filter_type == 'all') || (location_filter_type == 'free')) {
        loc_val = 0;
    }
    
    
	if(book_date=="")
	{
	 alert("Please select the date field");
     return false;	 
		
	}
	if(from_time=="")
	{
	 alert("Please select the time from field");
     return false;	 
		
	}
	if(to_time=="")
	{
	 alert("Please select the time to field ");
     return false;	 
		
	}
    if(book_type=="")
	{
	 alert("Please select the Repeat field");
     return false;	 
		
	}
      var _repeat_end = '';
     var _repeat_end_date = '';  
        if(book_type == 'WE')
	{	
		
		  _repeat_end = $('input[name="repeat_end"]:checked').val();
		  if(_repeat_end == 'ondate')
		  {
			  _repeat_end_date = $('#repeat-end-date').val();
			  if(_repeat_end_date == '')
			  {
				  alert('Enter an end date');
				return false;
			  }
		  }
	  }
        $("#LoadingImage").show();
        
        $('#maid_search').hide();
        $.ajax({
            type    : "POST",
            url     : _base_url + "customer/maids_booking",
            data    : {book_date    : book_date,
                        from_time   :from_time,
                        to_time     :to_time,
                        book_type   :book_type,
                        location_type   :loc_type,
                        location_value: loc_val,
                        area_id     :area,
                        zone_id     :zone,
                        province_id :province,
                        customer_id :custId},
            dataType: "text",
            cache   : false,
            success : function (result) {
                 $('#maid_search').show();
                $('#maid_search').html(result);
                $('#maid_search').addClass('widget-content');
                $("#LoadingImage").hide();
                $("input[name=location_filter][value=" + location_filter_type + "]").attr('checked', 'checked');
            }
        });
	
});






$('body').on('change', '#book-booking-type', function() {
	var _booking_type = $(this).val();
	if(_booking_type == 'WE' || _booking_type == 'BW')
	{
              
//		$('#repeat-days-search').css('display', 'table-row');
		$('#repeat-ends-search').css('display', 'table-row');
                $('#repeat-end-never').attr('checked',  true);
	}
	else
	{
//		$('#repeat-days-search').hide();
		$('#repeat-ends-search').hide();
	}
});
$('body').on('change', '#cust_address_area', function() {
	var areaId = $(this).val();
	var cust_add_id=$(this).find(':selected').attr('data-id');
        $('#cust_area_id').val(areaId);
        $('#cust_add_id').val(cust_add_id);
           $.ajax({
            type    : "POST",
            url     : _base_url + "customer/get_zone_province",
            data    : {area_id : areaId},
            dataType: "text",
            cache   : false,
            success : function (result) {
                var _resp = $.parseJSON(result);
                $('#cust_zone_id').val(_resp.zone_id);
                $('#cust_prov_id').val(_resp.province_id);
		
                
            }
        });
	
});

$('body').on('change', 'input[name="repeat_end"]', function() {
	if($(this).val() == 'ondate')
	{
		$('#repeat-end-date').removeAttr('disabled'); 
	}
	else
	{
		$('#repeat-end-date').attr('disabled', 'disabled'); 
	}
});

$('body').on('click', '#delete-cancel', function() {
	//$('#booking-action-confirm-panel').slideUp(function() { $('#booking-action-confirm-panel').remove(); });
	$.fancybox.close();
});

$('body').on('click', '#delete-one-day', function() {
	var _booking_id = $('#boooking-id').val();
	//alert(_booking_id);
	$.post(_page_url, { action: 'delete-booking-one-day', booking_id: _booking_id }, function(response) {
		alert(response);
		_bpop_open = false;
		//refresh_grid();
		var _alert_html = '';
		if(response == 'success')
		{
			_alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Booking has been deleted successfully.</div><div class="bottom"><input type="button" value="OK" class="pop_close" /></div></div>';
			_refresh_page = false
		}
		else
		{
                        if(response == 'locked')
                        {
                                _alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">This booking is locked by another user.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
                                _refresh_page = true;
                        }
                        else
                        {
                                _alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
                                _refresh_page = true;
                        }
			
		}
		
		if(_alert_html  != '')
		{
			$.fancybox.open({
				autoCenter : true,
				fitToView : false,
				scrolling : false,
				openEffect : 'fade',
				openSpeed : 100,
				helpers : {
					overlay : {
						css : {
							'background' : 'rgba(0, 0, 0, 0.3)'
						},
						closeClick: false
					}
				},
				padding : 0,
				closeBtn : false,
				content: _alert_html
			});
		}
	});
});

$('body').on('click', '#delete-permanent', function() {
	var _booking_id = $('#boooking-id').val();
	$.post(_page_url, { action: 'delete-booking-permanent', booking_id: _booking_id }, function(response) {
		_bpop_open = false;
		refresh_grid();
		var _alert_html = '';
		if(response == 'success')
		{
			_alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Booking has been deleted successfully.</div><div class="bottom"><input type="button" value="OK" class="pop_close" /></div></div>';
			_refresh_page = false;
			window.location.reload();
		}
		else
		{
                        if(response == 'locked')
                        {
                                _alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">This booking is locked by another user.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
                                _refresh_page = false;
                        }
                        else
                        {
                                _alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
                                _refresh_page = true;
                        }
		}
		
		if(_alert_html  != '')
		{
			$.fancybox.open({
				autoCenter : true,
				fitToView : false,
				scrolling : false,
				openEffect : 'fade',
				openSpeed : 100,
				helpers : {
					overlay : {
						css : {
							'background' : 'rgba(0, 0, 0, 0.3)'
						},
						closeClick: false
					}
				},
				padding : 0,
				closeBtn : false,
				content: _alert_html
			});
		}
	});
});

function deletebookingservice(bookingid,bookingtype)
{
	//var bookingid = $(this).attr('rel');
	//alert(bookingid);
	//var bookingtype = $(this).attr('rel');
	//alert(bookingtype);
	if(bookingtype == 'OD')
	{
		$.fancybox.open({
			autoCenter : true,
			fitToView : false,
			scrolling : false,
			openEffect : 'fade',
			openSpeed : 100,
			helpers : {
				overlay : {
					css : {
						'background' : 'rgba(0, 0, 0, 0.3)'
					},
					closeClick: false
				}
			},
			padding : 0,
			closeBtn : false,
			content:  '<div id="booking-action-confirm-panel1"><p style="padding: 20px; text-align: center; font-size:16px;">Do you want to delete this booking?</p><input type="hidden" id="boooking-id" value="'+bookingid+'" /><input type="button" id="delete-permanent" value="Delete Booking" /><input type="button" id="delete-cancel" value="Cancel"></div>'
		});
		
		
		
		// $('#myModal').modal('show');
		// $('#myModal').on('shown.bs.modal', function() {
			// $('#myModal').find('.modal-body').append('<div id="booking-action-confirm-panel"><input type="button" id="update-permanent" value="Modify this Booking" /><input type="button" id="update-cancel" value="Cancel"></div>');
		// });
		//$(this).parent().before('<div id="booking-action-confirm-panel"><input type="button" id="update-permanent" value="Modify this Booking" /><input type="button" id="update-cancel" value="Cancel"></div>');
	}
	else
	{
		$.fancybox.open({
			autoCenter : true,
			fitToView : false,
			autoDimensions : true,
			scrolling : false,
			openEffect : 'fade',
			openSpeed : 100,
			helpers : {
				overlay : {
					css : {
						'background' : 'rgba(0, 0, 0, 0.3)'
					},
					closeClick: false
				}
			},
			padding : 0,
			closeBtn : false,
			content:  '<div id="booking-action-confirm-panel1"><p style="padding: 20px; text-align: center; font-size:16px;">Do you want to delete this booking?</p><input type="hidden" id="boooking-id" value="'+bookingid+'" /><input type="button" id="delete-permanent" value="Delete Permanently" /><input type="button" id="delete-cancel" value="Cancel"></div>'
		});
		//$('#booking-action-confirm-panel').slideDown();
		
		
		// $('#myModal').modal('show');
		// $('#myModal').on('shown.bs.modal', function() {
			// $('#myModal').find('.modal-body').append('<div id="booking-action-confirm-panel"><input type="button" id="update-one-day" value="Change One Day" /><input type="button" id="update-permanent" value="Change Permanently" /><input type="button" id="update-cancel" value="Cancel"></div>');
		// });
		//$(this).parent().before('');
	}
	//$("#myModal .modal-body").html()
	//$('#booking-action-confirm-panel').append('#myModal .modal-body');
	//$("#myModal").modal(); 
	///console.log(bookingtype);
}

function refresh_grid()
{
	if(_bpop_open != true)
	{
		$.post( _page_url, { action: 'refresh-grid' }, function(response) {
			response = $.parseJSON(response);
			$('#schedule-grid-rows').html(response.grid);
			$('#schedule-report').html(response.report);
			setTimeout('refresh_grid()', 5000);
			if(_bpop_open != true)
			{
				apply_selectable();
			}
		});
	}
	else
	{
		setTimeout('refresh_grid()', 5000);
	}
}

