$(document).ready(function () {
    service_wise_revenue_table = $('#service_wise_revenue_table').DataTable({
        'bFilter': false,
        'bLengthChange': true,
        'pageLength': 50,
        'processing': true,
        'serverSide': true,
        'bSort': false,
        'serverMethod': 'GET',
        'ajax': {
            'url': _base_url + 'laravel/data/report/service_wise_revenue_report',
            'data': function (data) {
                data.filter_from_date = $('#filter_from_date').val() ? moment($('#filter_from_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD') : undefined;
                data.filter_to_date = $('#filter_to_date').val() ? moment($('#filter_to_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD') : undefined;
                data.filter_service_type_id = $('#filter_service_type_id').find(":selected").val() || undefined;
            },
            "complete": function (json, type) { }
        },
        "lengthMenu": [10, 25, 50, 75, 100],
        'columns': [
            { data: 'slno' },
            { data: 'service_type_name' },
            {
                data: 'cp_total_paid_amount', render: function (data, type) {
                    return data
                        .toLocaleString("en-US", {
                            minimumFractionDigits: 2
                        })
                }
            },
        ],
        'columnDefs': [
            {
                targets: [2],
                "createdCell": function (td, cellData, rowData, row, col) {
                }
            },
        ],
        footerCallback: function (row, data, start, end, display) {
            var api = this.api();
            // Update footer
            $(api.column(2).footer()).html('<span class="text-info">' + api.ajax.json().total.cp_total_paid_amount
                .toLocaleString("en-US", {
                    minimumFractionDigits: 2
                }) +
                '</span>');
        },
    });
});
$('#filter_from_date, #filter_to_date').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    endDate: new Date()
}).on('changeDate', function (ev) {
});
function filter_table() {
    service_wise_revenue_table.draw();
}