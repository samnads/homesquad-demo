<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MaidsController;
use App\Http\Controllers\BookingsController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\QuickBookController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('maid')->group(function () {
    Route::get('check_free_maids_for_booking', [MaidsController::class, 'check_free_maids_for_booking']);
});
Route::prefix('booking')->group(function () {
    Route::post('assign_maids', [BookingsController::class, 'assign_maids']);
});
Route::prefix('data/report')->group(function () {
    Route::get('service_wise_revenue_report', [ReportController::class, 'service_wise_revenue_report_data']);
	Route::get('customer_outstanding_report', [ReportController::class, 'customer_outstanding_report']);
});
Route::get('quickbook/refresh-token', [QuickBookController::class, 'refresh_token']);
