<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Settings;
use App\Classes\Client;

class QuickBookController extends Controller
{
    public function refresh_token()
    {
		// require('/var/www/booking/dhk/application/libraries/Client.php');
        $settings = Settings::Find(1);
		$client_id = $settings->quickBookClientId;
		$client_secret = $settings->quickBookClientSecret;
		
		if($client_id != "" && $client_secret != "")
		{
			$tokenEndPointUrl = 'https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer';
			$grant_type= 'refresh_token';
			$certFilePath = '/var/www/booking/dhk/application/libraries/Certificate/cacert.pem';
			$refresh_token = $settings->refresh_token;
			if($refresh_token != "")
			{
				$client = new Client($client_id, $client_secret, $certFilePath);
				$result = $client->refreshAccessToken($tokenEndPointUrl, $grant_type, $refresh_token);
				if(isset($result['error']))
				{
					$response = array();
					$response['status'] = "error";
					$response['message'] = "Token not generated.";
					echo json_encode($response);
					exit();
				} else {
					$settings->access_token = $result['access_token'];
					$settings->refresh_token = $result['refresh_token'];
					$settings->quick_refresh_count = ($settings->quick_refresh_count + 1);
					$settings->save();
					
					$response = array();
					$response['status'] = "success";
					$response['message'] = "Token generated successfully.";
					echo json_encode($response);
					exit();
				}
			} else {
				$response = array();
				$response['status'] = "error";
				$response['message'] = "Token not available.";
				echo json_encode($response);
				exit();
			}
		} else {
			$response = array();
			$response['status'] = "error";
			$response['message'] = "Client Keys not available.";
			echo json_encode($response);
			exit();
		}
		exit();
        // try {
            // if (sizeof($bookings) != sizeof($request->selected_maids)) {
                // throw new \ErrorException('Mismatch in number of maids !');
            // }
            // DB::beginTransaction();
            // foreach ($bookings as $key => $booking_row) {
                // $booking = Bookings::find($booking_row->booking_id);
                // $booking->maid_id = $request->selected_maids[$key];
                // $booking->booking_status = 1;
                // $booking->save();
            // }
            // DB::commit();
            // return Response::json(array('success' => true, 'message' => 'Maids Assigned Successfully !'), 200, array(), JSON_PRETTY_PRINT);
        // } catch (\Exception $e) {
            // DB::rollback();
            // return Response::json(array('success' => false, 'message' => $e->getMessage()), 200, array(), JSON_PRETTY_PRINT);
        // }
    }
}
