<?php

namespace App\Http\Controllers;

use App\Models\Bookings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class BookingsController extends Controller
{
    public function assign_maids(Request $request)
    {
        $bookings = Bookings::where(['booking_common_id' => $request->booking_id])->get();
        try {
            if (sizeof($bookings) != sizeof($request->selected_maids)) {
                throw new \ErrorException('Mismatch in number of maids !');
            }
            DB::beginTransaction();
            foreach ($bookings as $key => $booking_row) {
                $booking = Bookings::find($booking_row->booking_id);
                $booking->maid_id = $request->selected_maids[$key];
                $booking->booking_status = 1;
                $booking->save();
            }
            DB::commit();
            return Response::json(array('success' => true, 'message' => 'Maids Assigned Successfully !'), 200, array(), JSON_PRETTY_PRINT);
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('success' => false, 'message' => $e->getMessage()), 200, array(), JSON_PRETTY_PRINT);
        }
    }
}
