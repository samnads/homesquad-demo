<?php

namespace App\Http\Controllers;

use App\Models\Bookings;
use App\Models\Customers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class MaidsController extends Controller
{
    public function check_free_maids_for_booking(Request $request)
    {
        /************************************************************* */
        $booking = Bookings::find($request->booking_id);
        //$booking->service_week_day = Carbon::parse($request->service_start_date)->format('w');
        //$booking->service_start_date = $request->service_start_date;
        //$booking->service_actual_end_date = $request->service_start_date;
        //$booking->booking_type = $request->booking_type;
        //$booking->service_end = $request->booking_type == "OD" ? "1" : "0";
        $booking->time_from = "00:00:00";
        $booking->time_to = "23:59:00";
        $day_bookings = get_busy_bookings($booking, []);
        /************************************************************* */
        $booking = Bookings::find($request->booking_id);
        //$booking->service_week_day = Carbon::parse($request->service_start_date)->format('w');
        //$booking->service_start_date = $request->service_start_date;
        //$booking->service_actual_end_date = $request->service_start_date;
        //$booking->booking_type = $request->booking_type;
        //$booking->service_end = $request->booking_type == "OD" ? "1" : "0";
        $booking->time_from = $request->time_from;
        $booking->time_to = $request->time_to;
        /************************************************************* */
        $maids = getActiveMaids();
        $maid_ids = array_column($maids->toArray(), 'maid_id');
        $busy_bookings = get_busy_bookings($booking, @$except_booking_ids ?: []);
        // trick for BW skipping
        if ($request->booking_type == "BW") {
            // trick to exclude not overlapped bw bookings
            foreach ($busy_bookings->toArray() as $key => $busy_booking) {
                if ($busy_booking->booking_type == "BW" && $busy_booking->service_start_date_week_difference & 1) { // if odd number
                    // week diff odd number means it will not overlap other bw bookings, so remove it from timed bookings
                    unset($busy_bookings[$key]);
                }
            }
        }
        /************************************************************* */
        $busy_maids = array_unique(array_column($busy_bookings->toArray(), 'maid_id'));
        $free_maids = array_diff($maid_ids, $busy_maids);
        /************************************************************* */
        $response['available_maids'] = DB::table('maids as m')
            ->select(
                'm.maid_id',
                'm.maid_name',
                'm.maid_photo_file as maid_img_url',
                'm.maid_notes as maid_details',
            )
            ->whereIn('m.maid_id', $free_maids)
            ->whereNotIn('m.maid_id', @$except_maid_ids ?: [])
            ->orderBy('m.maid_id', 'DESC')
            ->get();
        foreach ($response['available_maids'] as $key => $available_maid) {
            $bookings = array_values(array_filter($day_bookings->toArray(), function ($booking) use ($available_maid) {return ($booking->maid_id == $available_maid->maid_id);}));
            $dup_booking_ids = [];
            $bookings_unique = [];
            foreach ($bookings as $key2 => $line) {
                if (!in_array($line->booking_id, $dup_booking_ids)) {
                    $dup_booking_ids[] = $line->booking_id;
                    $bookings_unique[$key2] = $line;
                }
            }
            $response['available_maids'][$key]->bookings = array_values($bookings_unique);
        }
        $response['input'] = $request->all();
        /************************************************************* */
        $response['booking_grouped'] = DB::table('bookings as b')
            ->select(
                DB::raw('MIN(b.booking_id) as booking_id'),
                'b.booking_common_id',
                DB::raw('MAX(b.customer_id) as customer_id'),
                DB::raw('SUM(b.total_amount) as total_amount'),
                DB::raw('ROUND(SUM(b.discount),2) as discount'),
                DB::raw('SUM(b.pay_by_cash_charge) as pay_by_cash_charge'),
                DB::raw('(SUM(b.total_amount)*3)/100 as pay_by_card_charge'),
            )
            ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
            ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
            ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
            ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
            ->where(function ($query) use ($request) {
                $query->where('b.booking_common_id', '=', $request->booking_id)
                    ->orWhere('b.booking_id', '=', $request->booking_id);
            })
            ->groupBy('b.booking_common_id')
            ->first();
        /************************************************************* */
        $response['bookings'] = DB::table('bookings as b')
            ->select('b.*', )
            ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
            ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
            ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
            ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
            ->where(function ($query) use ($request) {
                $query->where('b.booking_common_id', '=', $request->booking_id)
                    ->orWhere('b.booking_id', '=', $request->booking_id);
            })
            ->get();
        /************************************************************* */
        $response['customer'] = Customers::select('customers.*')
            ->where('customers.customer_id', '=', $response['booking_grouped']->customer_id)
            ->first();
        /************************************************************* */
        return Response::json($response, 200, array(), JSON_PRETTY_PRINT);
    }
}
