<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use Illuminate\Support\Str;

class ReportController extends Controller
{
    public function service_wise_revenue_report_data(Request $request)
    {
        $filter_from_date = $request->filter_from_date ?: null;
        $filter_to_date = $request->filter_to_date ?: null;
        $filter_service_type_id = $request->filter_service_type_id ?: null;
        $data['draw'] = $request->draw ?: 0;
        $revenue_by_service = DB::table('day_services as ds')
            ->select(
                DB::raw('SUM(ds.total_fee) as total_fee'),
                DB::raw('SUM(cp.paid_amount) as total_paid_amount'),
                'st.service_type_id',
            )
            ->leftJoin('customer_payments as cp', 'ds.day_service_id', 'cp.day_service_id')
            ->leftJoin('bookings as b', 'b.booking_id', 'ds.booking_id')
            ->leftJoin('service_types as st', 'st.service_type_id', 'b.service_type_id')
            ->groupBy('st.service_type_id');
        if (isset($filter_from_date)) {
            $revenue_by_service->whereDate('cp.paid_datetime', '>=', $filter_from_date);
        }
        if (isset($filter_to_date)) {
            $revenue_by_service->whereDate('cp.paid_datetime', '<=', $filter_to_date);
        }
        $data['data'] = DB::table('service_types as st')
            ->select(
                DB::raw('st.service_type_name as service_type_name'),
                DB::raw('ROUND(IFNULL(rbs.total_fee,0),2) as ds_total_fee'),
                DB::raw('ROUND(IFNULL(rbs.total_paid_amount,0),2) as cp_total_paid_amount'),
            )
            ->where('st.service_type_status', '=', 1)
            ->leftJoinSub($revenue_by_service, 'rbs', function ($join) {
                $join->on('st.service_type_id', '=', 'rbs.service_type_id');
            })
            ->orderBy('cp_total_paid_amount', 'DESC');
        if (isset($filter_service_type_id)) {
            $data['data']->where('st.service_type_id', '=', $filter_service_type_id);
        }
        /**************************************** */
        $total['cp_total_paid_amount'] = number_format($data['data']->sum('total_paid_amount'), 2);
        $total['ds_total_fee'] = number_format($data['data']->sum('total_fee'), 2);
        /**************************************** */
        $data['data'] = $data['data']->get();
        foreach ($data['data'] as $key => $value) {
            $data['data'][$key]->slno = $key + 1;
        }
        $data['total'] = $total;
        $data['recordsTotal'] = $data['data']->count();
        $data['recordsFiltered'] = $data['data']->count();
        return Response::json($data, 200, [], JSON_PRETTY_PRINT);
    }
	public function customer_outstanding_report(Request $request)
    {
        $response['draw'] = $request['draw'];
        $start = $request['start'];
        $rowperpage = $request['length'];
        $sort_columns = ['name' => 'c.customer_name', 'mobile' => 'c.mobile_number_1', 'area' => '', 'balance' => 'c.balance', 'last_invoice_date' => 'c.last_invoice_date', 'type_of_client' => 'c.payment_type'];
        /*********************************************************** */
        $invoices = DB::table('invoice as i')
            ->select(
                DB::raw('i.customer_id'),
                DB::raw('SUM(i.invoice_net_amount) as invoice_net_amount'),
                DB::raw('MAX(i.invoice_date) as last_invoice_date'),
            )
            ->whereNotIn('i.invoice_status', [0, 2])
            ->where('i.showStatus', 1)
            ->groupBy('i.customer_id');
        $response['recordsTotal'] = $invoices->get()->count();
        //return Response::json($invoices->get(), 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        /*********************************************************** */
        $customer_payments = DB::table('customer_payments as cp')
            ->select(
                DB::raw('cp.customer_id'),
                DB::raw('SUM(cp.paid_amount) as paid_amount'),
            )
            ->leftJoinSub(
                $invoices,
                'ci',
                function ($join) {
                    $join->on('cp.customer_id', '=', 'ci.customer_id');
                }
            )
            ->where('cp.deleted_at', null)
            ->where('cp.show_status', 1)
            ->groupBy('cp.customer_id');
        //return Response::json($customer_payments->get(), 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        /*********************************************************** */
        $report_query = DB::table('customers as c')
            ->select(
                // DB::raw('DISTINCT(c.customer_id) AS customer_id'),
				'c.customer_id',
                'c.customer_name',
                'a.area_name',
                'ca.customer_address_id',
                'c.mobile_number_1',
                DB::raw("CASE WHEN c.payment_type = 'D' THEN 'Daily' ELSE (CASE WHEN c.payment_type = 'M' THEN 'Monthly' ELSE (CASE WHEN c.payment_type = 'MA' THEN 'Monthly Advance' ELSE 'Unknown' END) END) END as payment_type"),
                'ci.last_invoice_date',
                'ci.invoice_net_amount',
                'cp.paid_amount',
                DB::raw('(ci.invoice_net_amount - cp.paid_amount) as balance'),
            )
            ->leftjoin('customer_addresses as ca', function ($join) {
                $join->on('c.customer_id', '=', 'ca.customer_id');
                $join->where('ca.default_address', '=', 1);
            })
            ->leftjoin('areas as a', 'ca.area_id', '=', 'a.area_id')
            ->leftJoinSub(
                $invoices,
                'ci',
                function ($join) {
                    $join->on('c.customer_id', '=', 'ci.customer_id');
                }
            )
            ->JoinSub(
                $customer_payments,
                'cp',
                function ($join) {
                    $join->on('c.customer_id', '=', 'cp.customer_id');
                }
            )
            ->whereRaw('(ci.invoice_net_amount - cp.paid_amount) > 0');
        if ($request['filter_customer_id']) {
            $report_query->where('c.customer_id', $request['filter_customer_id']);
        }
        if (@$request['filter_customer_active'] > -1) {
            $report_query->where('c.customer_status', $request['filter_customer_active']);
        }
        if ($request->filter_date_from) {
            $report_query->whereRaw('date(c.customer_added_datetime) >= "' . $request->filter_date_from . '"');
        }
        if ($request->filter_date_to) {
            $report_query->whereRaw('date(c.customer_added_datetime) <= "' . $request->filter_date_to . '"');
        }
        $after_filter = $report_query;
        $report_query->groupBy('c.customer_id');
        $report_query->orderBy('balance', 'desc');
        $report_query_result = $report_query->get()->each(function ($row, $index) {
            $row->slno = $index + 1;
            $row->invoice_net_amount = number_format($row->invoice_net_amount, 2, ".", "");
            $row->paid_amount = number_format($row->paid_amount, 2, ".", "");
            $row->balance = number_format($row->balance, 2, ".", "");
            $row->action = '<a class="btn btn-small btn-info" onclick="viewDetailsModal();listCustOutInvoicesTable('.$row->customer_id.');" title="View Details"><i class="btn-icon-only fa fa-eye"> </i></a>';
        });
        $recordsFiltered = $report_query_result->count();
        $response['recordsFiltered'] = $recordsFiltered;
        $response['totalBalance'] = number_format($after_filter->get()->sum('balance'), 2);
        //return Response::json($report, 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        /*********************************************************** */
        $response['rawQuery'] = Str::replaceArray('?', $report_query->getBindings(), $report_query->toSql());
        $response['aaData'] = $report_query_result;
        return Response::json($response, 200, [], JSON_PRETTY_PRINT);
    }
}
