<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Maid extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        if(!user_permission(user_authenticate(), 3))
        {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $this->load->model('customers_model');
        $this->load->model('maids_model');
        $this->load->model('service_types_model');
        $this->load->model('bookings_model');
        $this->load->model('tablets_model');
        $this->load->model('zones_model');
        $this->load->model('settings_model');
        $this->load->library('upload');
        //$this->load->helper('odoo_helper');
    }

    public function index() {
        
        $data['message'] = "No Message";
        
        if ($this->input->post('maid_sub')) {
            $maid_name = $this->input->post('maid_name');
            $gender = $this->input->post('gender');
            $nationality = $this->input->post('nationality');
            $image = $this->input->post('img_name_resp');
            $present_address = $this->input->post('present_address');
            $permanent_address = $this->input->post('permanent_address');
            $mobile_number1 = $this->input->post('mobile1');
            $mobile_number2 = $this->input->post('mobile2');
            $flat = $this->input->post('flat');
            $services = $this->input->post('services');
            $notes = $this->input->post('notes');
            $passport_number = $this->input->post('passport_number');
            $passport_exp = $this->input->post('passport_expiry');
            if ($passport_exp != '') {
                list($day, $month, $year) = explode("/", $passport_exp);
                $passport_expiry = "$year-$month-$day";
            } else {
                $passport_expiry = "";
            }
            $visa_number = $this->input->post('visa_number');
            $visa_exp = $this->input->post('visa_expiry');
            if ($visa_exp != '') {
                list($day, $month, $year) = explode("/", $visa_exp);
                $visa_expiry = "$year-$month-$day";
            } else {
                $visa_expiry = "";
            }
            $labour_number = $this->input->post('labour_number');
            $labour_exp = $this->input->post('labour_expiry');
            if ($labour_exp != '') {
                list($day, $month, $year) = explode("/", $labour_exp);
                $labour_expiry = "$year-$month-$day";
            } else {
                $labour_expiry = "";
            }
            $emirates_id = $this->input->post('emirates_id');
            $emirates_exp = $this->input->post('emirates_expiry');
            if ($emirates_exp != '') {
                list($day, $month, $year) = explode("/", $emirates_exp);
                $emirates_expiry = "$year-$month-$day";
            } else {
                $emirates_expiry = "";
            }
            $added = date('Y-m-d h:i:s');
            //echo $added;exit;
            $image_file = "";
            $vimage_file = "";
            $limage_file = "";
            $eimage_file = "";

            $data = array(
                'maid_name' => $maid_name,
                'maid_gender' => $gender,
                'maid_nationality' => $nationality,
                'maid_present_address' => $present_address,
                'maid_permanent_address' => $permanent_address,
                'maid_mobile_1' => $mobile_number1,
                'maid_mobile_2' => $mobile_number2,
                'flat_id' => $flat,
                'maid_photo_file' => $image,
                'maid_passport_number' => $passport_number,
                'maid_passport_expiry_date' => $passport_expiry,
                'maid_visa_number' => $visa_number,
                'maid_visa_expiry_date' => $visa_expiry,
                'maid_labour_card_number' => $labour_number,
                'maid_labour_card_expiry_date' => $labour_expiry,
                'maid_emirates_id' => $emirates_id,
                'maid_emirates_expiry_date' => $emirates_expiry,
                'maid_notes' => $notes,
                'maid_status' => 1,
                'maid_added_datetime' => $added,
            );
            $result = $this->maids_model->add_maids($data);
            $id = $result;

            if ($_FILES['attach_passport'] != '') {
                //$config['image_library'] = 'gd2';
                $original_path = './maid_passport';
                $extension = end(explode(".", $_FILES['attach_passport']['name']));
                $uniqueid = date('Ymdhis');
//              $filename = $uniqueid . "." . $extension;
                $filename = "mpassport_" . $id . "." . $extension;
                $config = array(
                    'allowed_types' => 'jpg|jpeg|gif|png|doc|docx|pdf',
                    'upload_path' => $original_path,
                    'file_name' => $filename
                );
                $this->upload->initialize($config);

                if (!$this->upload->do_upload("attach_passport")) {
                    //echo $this->upload->display_errors();     
                } else {
                    $img_data = array('upload_data' => $this->upload->data("attach_passport"));
                    $image_file = $img_data['upload_data']['file_name'];
                    //echo $image_file;exit;
                }
            }
            
            if ($_FILES['attach_visa'] != '') {
                //$config['image_library'] = 'gd2';
                $original_path = './maid_visa';

                $extension = end(explode(".", $_FILES['attach_visa']['name']));
                $uniqueid = date('Ymdhis');
                $filename = "mvisa_" . $id . "." . $extension;
                $config = array(
                    'allowed_types' => 'jpg|jpeg|gif|png|doc|docx|pdf',
                    'upload_path' => $original_path,
                    'file_name' => $filename
                );
                $this->upload->initialize($config);
                if (!$this->upload->do_upload("attach_visa")) {
                    //echo $this->upload->display_errors();     
                } else {
                    $img_dat = array('upload_data' => $this->upload->data("attach_visa"));
                    $vimage_file = $img_dat['upload_data']['file_name'];
                    //echo $vimage_file;exit;
                }
            }
            if ($_FILES['attach_labour'] != '') {

                //$config['image_library'] = 'gd2';
                $original_path = './maid_labour';

                $extension = end(explode(".", $_FILES['attach_labour']['name']));
                $uniqueid = date('Ymdhis');
                $filename = "mlabour_" . $id . "." . $extension;

                $config = array(
                    'allowed_types' => 'jpg|jpeg|gif|png|doc|docx|pdf',
                    'upload_path' => $original_path,
                    'file_name' => $filename
                );
                $this->upload->initialize($config);
                if (!$this->upload->do_upload("attach_labour")) {
                    //echo $this->upload->display_errors();     
                } else {
                    //$this->load->library('upload', $config);
                    $img_da = array('upload_data' => $this->upload->data("attach_labour"));
                    $limage_file = $img_da['upload_data']['file_name'];
                    //echo $limage_file;exit;
                }
            }
            if ($_FILES['attach_emirates'] != '') {
                //$config['image_library'] = 'gd2';
                $original_path = './maid_emirates';

                $extension = end(explode(".", $_FILES['attach_emirates']['name']));
                $uniqueid = date('Ymdhis');
                $filename = "memirates_" . $id . "." . $extension;

                $config = array(
                    'allowed_types' => 'jpg|jpeg|gif|png|doc|docx|pdf',
                    'upload_path' => $original_path,
                    'file_name' => $filename
                );
                $this->upload->initialize($config);
                if (!$this->upload->do_upload("attach_emirates")) {
                    //echo $this->upload->display_errors();     
                } else {
                    //$this->load->library('upload', $config);
                    $img_d = array('upload_data' => $this->upload->data("attach_emirates"));
                    $eimage_file = $img_d['upload_data']['file_name'];
                    //echo $eimage_file;
                }
            }

            $datas = array(
                'maid_passport_file' => $image_file,
                'maid_visa_file' => $vimage_file,
                'maid_labour_card_file' => $limage_file,
                'maid_emirates_file' => $eimage_file,
            );

            $this->maids_model->update_attachments($datas, $id);

            for ($i = 0; $i < count($services); $i++) {
                $dat = array(
                    'maid_id' => $id,
                    'service_type_id' => $services[$i],
                );
                $this->maids_model->update_services($dat, $id);
            }
            $data['message'] = "success";
        }
        //$data = array();
        $data['flats'] = $this->settings_model->get_flats();
        $data['services'] = $this->settings_model->get_services();
        $layout_data['content_body'] = $this->load->view('maid', $data, TRUE);
        $layout_data['page_title'] = 'maids';
        $layout_data['meta_description'] = 'maids';
        $layout_data['css_files'] = array('datepicker.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js','jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }

    function maidimgupload() {
        $uploaddir = './maidimg/';
        $extension = end(explode(".", $_FILES['uploadfile']['name']));
        $uniqueid = date('Ymdhis');
        $file = $uploaddir . $uniqueid . "." . $extension;
        if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $file)) {
            echo $uniqueid . "." . $extension;
        } else {
            echo "error";
        }
    }

    function maid_list() {
        $data = array();
        $data['maids'] = $this->maids_model->get_all_maids();
        //print_r($data['maids']);exit;
        $layout_data['content_body'] = $this->load->view('maid_list', $data, TRUE);
        $layout_data['page_title'] = 'maids';
        $layout_data['meta_description'] = 'maids';
        $layout_data['css_files'] = array('demo.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js','moment.min.js','jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }

//    function edit_maid()
//    {
//        $maid_id = $this->input->post('maid_id');
//        $data = $this->maids_model->get_maid_details($maid_id);
//        echo json_encode($data);
//    }
    function remove_maid() {
        $maid_id = $this->input->post('maid_id');
        $this->maids_model->delete_maid($maid_id);
        $this->maids_model->delete_maid_services($maid_id);
    }

    function view_maid() {
        $maid_id = $this->input->post('maid_id');
        $data = $this->maids_model->get_maid_details($maid_id);
        echo json_encode($data);
    }

    function edit_maid($maid_id) {
        $data['message'] = "No Message";
        if ($this->input->post('maid_edit')) {
            $maid_id = $this->input->post('maid_id');
            $maid_name = $this->input->post('maid_name');
            $gender = $this->input->post('gender');
            $nationality = $this->input->post('nationality');
            $present_address = $this->input->post('present_address');
            $permanent_address = $this->input->post('permanent_address');
            $mobile_number1 = $this->input->post('mobile1');
            $mobile_number2 = $this->input->post('mobile2');
            $flat = $this->input->post('flat');
            $services = $this->input->post('services');
            $notes = $this->input->post('notes');
            $passport_number = $this->input->post('passport_number');
            $passport_exp = $this->input->post('passport_expiry');
            $old_image = $this->input->post('old_image');
            $img = $this->input->post('img_name_resp');
            if ($img == "") {
                $image = $old_image;
            } else {
                $image = $img;
            }
            if ($passport_exp != '') {
                list($day, $month, $year) = explode("/", $passport_exp);
                $passport_expiry = "$year-$month-$day";
            } else {
                $passport_expiry = "";
            }
            $visa_number = $this->input->post('visa_number');
            $visa_exp = $this->input->post('visa_expiry');
            if ($visa_exp != '') {
                list($day, $month, $year) = explode("/", $visa_exp);
                $visa_expiry = "$year-$month-$day";
            } else {
                $visa_expiry = "";
            }
            $labour_number = $this->input->post('labour_number');
            $labour_exp = $this->input->post('labour_expiry');
            if ($labour_exp != '') {
                list($day, $month, $year) = explode("/", $labour_exp);
                $labour_expiry = "$year-$month-$day";
            } else {
                $labour_expiry = "";
            }
            $emirates_id = $this->input->post('emirates_id');
            $emirates_exp = $this->input->post('emirates_expiry');
            if ($emirates_exp != '') {
                list($day, $month, $year) = explode("/", $emirates_exp);
                $emirates_expiry = "$year-$month-$day";
            } else {
                $emirates_expiry = "";
            }
            $added = date('Y-m-d h:i:s');
            //echo $added;exit;
            
            $image_file = $this->input->post('old_attach_passport');
            $vimage_file = $this->input->post('old_attach_visa');
            $limage_file = $this->input->post('old_attach_labour');
            $eimage_file = $this->input->post('old_attach_emirates');

            $data = array(
                'maid_name' => $maid_name,
                'maid_gender' => $gender,
                'maid_nationality' => $nationality,
                'maid_present_address' => $present_address,
                'maid_permanent_address' => $permanent_address,
                'maid_mobile_1' => $mobile_number1,
                'maid_mobile_2' => $mobile_number2,
                'flat_id' => $flat,
                'maid_photo_file' => $image,
                'maid_passport_number' => $passport_number,
                'maid_passport_expiry_date' => $passport_expiry,
                'maid_visa_number' => $visa_number,
                'maid_visa_expiry_date' => $visa_expiry,
                'maid_labour_card_number' => $labour_number,
                'maid_labour_card_expiry_date' => $labour_expiry,
                'maid_emirates_id' => $emirates_id,
                'maid_emirates_expiry_date' => $emirates_expiry,
                'maid_notes' => $notes,
                'maid_status' => 1,
                'maid_added_datetime' => $added,
            );
            $this->maids_model->update_maids($data, $maid_id);
            //$id = $maid_id;

            if ($_FILES['attach_passport'] != '') {
                //$config['image_library'] = 'gd2';
                $original_path = './maid_passport';
                $extension = end(explode(".", $_FILES['attach_passport']['name']));
                $uniqueid = date('Ymdhis');
//              $filename = $uniqueid . "." . $extension;
                $filename = "mpassport_" . $maid_id . "." . $extension;
                $config = array(
                    'allowed_types' => 'jpg|jpeg|gif|png|doc|docx|pdf',
                    'upload_path' => $original_path,
                    'file_name' => $filename
                );
                $this->upload->initialize($config);

                if (!$this->upload->do_upload("attach_passport")) {
                    //echo $this->upload->display_errors();     
                } else {
                    $img_data = array('upload_data' => $this->upload->data("attach_passport"));
                    $image_file = $img_data['upload_data']['file_name'];
                    //echo $image_file;exit;
                }
            }
            if ($_FILES['attach_visa'] != '') {
                //$config['image_library'] = 'gd2';
                $original_path = './maid_visa';

                $extension = end(explode(".", $_FILES['attach_visa']['name']));
                $uniqueid = date('Ymdhis');
                $filename = "mvisa_" . $maid_id . "." . $extension;
                $config = array(
                    'allowed_types' => 'jpg|jpeg|gif|png|doc|docx|pdf',
                    'upload_path' => $original_path,
                    'file_name' => $filename
                );
                $this->upload->initialize($config);
                if (!$this->upload->do_upload("attach_visa")) {
                    //echo $this->upload->display_errors();     
                } else {
                    $img_dat = array('upload_data' => $this->upload->data("attach_visa"));
                    $vimage_file = $img_dat['upload_data']['file_name'];
                    //echo $vimage_file;exit;
                }
            }
            if ($_FILES['attach_labour'] != '') {

                //$config['image_library'] = 'gd2';
                $original_path = './maid_labour';

                $extension = end(explode(".", $_FILES['attach_labour']['name']));
                $uniqueid = date('Ymdhis');
                $filename = "mlabour_" . $maid_id . "." . $extension;

                $config = array(
                    'allowed_types' => 'jpg|jpeg|gif|png|doc|docx|pdf',
                    'upload_path' => $original_path,
                    'file_name' => $filename
                );
                $this->upload->initialize($config);
                if (!$this->upload->do_upload("attach_labour")) {
                    //echo $this->upload->display_errors();     
                } else {
                    //$this->load->library('upload', $config);
                    $img_da = array('upload_data' => $this->upload->data("attach_labour"));
                    $limage_file = $img_da['upload_data']['file_name'];
                    //echo $limage_file;exit;
                }
            }
            if ($_FILES['attach_emirates'] != '') {
                //$config['image_library'] = 'gd2';
                $original_path = './maid_emirates';

                $extension = end(explode(".", $_FILES['attach_emirates']['name']));
                $uniqueid = date('Ymdhis');
                $filename = "memirates_" . $maid_id . "." . $extension;

                $config = array(
                    'allowed_types' => 'jpg|jpeg|gif|png|doc|docx|pdf',
                    'upload_path' => $original_path,
                    'file_name' => $filename
                );
                $this->upload->initialize($config);
                if (!$this->upload->do_upload("attach_emirates")) {
                    //echo $this->upload->display_errors();     
                } else {
                    //$this->load->library('upload', $config);
                    $img_d = array('upload_data' => $this->upload->data("attach_emirates"));
                    $eimage_file = $img_d['upload_data']['file_name'];
                    //echo $eimage_file;
                }
            }

            $datas = array(
                'maid_passport_file' => $image_file,
                'maid_visa_file' => $vimage_file,
                'maid_labour_card_file' => $limage_file,
                'maid_emirates_file' => $eimage_file,
            );

            $this->maids_model->update_attachments($datas, $maid_id);
            $this->maids_model->delete_maid_services($maid_id);
            for ($i = 0; $i < count($services); $i++) {
                $dat = array(
                    'maid_id' => $maid_id,
                    'service_type_id' => $services[$i],
                );
                $this->maids_model->update_services($dat, $maid_id);
            }
            $data['message'] = "success";
        }
        //$data = array();
        $data['maid_details'] = $this->maids_model->get_maid_details($maid_id);
        $maid_services = $this->maids_model->get_maid_services($maid_id);
        
        $service_maid = array();
        foreach ($maid_services as $mservices){
            array_push($service_maid, $mservices->service_type_id);
        }
        $data['service_maid'] = $service_maid;
        
        //print_r($data['maid_details']);exit;
        $data['flats'] = $this->settings_model->get_flats();
        $data['services'] = $this->settings_model->get_services();
        $layout_data['content_body'] = $this->load->view('edit_maid', $data, TRUE);
        $layout_data['page_title'] = 'maids';
        $layout_data['meta_description'] = 'maids';
        $layout_data['css_files'] = array('datepicker.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js','jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }

    function editmaidimgupload() {
        $delmainfile = "./maidimg/" . $this->input->post('old_image');
        @unlink($delmainfile);
        $uploaddir = './maidimg/';
        $extension = end(explode(".", $_FILES['uploadfile']['name']));
        $uniqueid = date('Ymdhis');
        $file = $uploaddir . $uniqueid . "." . $extension;
        if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $file)) {
            echo $uniqueid . "." . $extension;
        } else {
            echo "error";
        }
    }
    function change_status()
    {
        $maid_id = $this->input->post('maid_id');
        $status = $this->input->post('status');
        if($status == 1)
        {
            $data = array(
                'maid_status' => 0
            );
            $this->maids_model->disable_status($maid_id,$data);
        }
        else
        {
            $data = array(
                'maid_status' => 1
            );
            $this->maids_model->activate_status($maid_id,$data);
        }
    }
    function maid_view($maid_id) {
        $data['maid_details'] = $this->maids_model->get_maid_details($maid_id);
        $data['maid_services'] = $this->maids_model->maid_services($maid_id);
        
        //print_r($data);exit;
        
        $layout_data['content_body'] = $this->load->view('view_maid', $data, TRUE);
        $layout_data['page_title'] = 'maids';
        $layout_data['meta_description'] = 'maids';
        $layout_data['css_files'] = array('jquery.lighter.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js','jquery.lighter.js','jquery.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    public function odoo_search($key, $value, $model, $user_id)
    {
        
        $domain_filter = array ( 
                        new xmlrpcval(
                            array(new xmlrpcval($key , "string"), 
                                  new xmlrpcval('=',"string"), 
                                  new xmlrpcval($value,"string")
                                  ),"array"             
                            ),
                        ); 
        
        $field_list = array(
                            new xmlrpcval("id", "int"),                                
                        ); 
        
        $odoo_id = search($user_id, $model,$domain_filter, $field_list);
        
        return $odoo_id;
        
    }
    public function export_to_odoo()
    {
            $user_id = odoo_login();   
            
            //Maids
            $maids = $this->maids_model->get_maids(FALSE);
            $i = 0;
            //echo sizeof($maids);exit;
            foreach ($maids as $maid)
            {
                $nationality = 0;
                switch ($maid->maid_nationality)
                {
                    case 'NEPALI':
                    case 'Nepal':
                        $nationality = 168;
                        break;
                    case 'Sri Lanka':
                    case 'SRI LANKAN':  
                    case 'SRILANKAN':
                        $nationality = 130;
                        break;
                    case 'Philippines':
                    case 'PHILIPINES':
                    case 'FILIPINO':
                    case 'P':
                    case 'PH':
                        $nationality = 178;
                        break;
                    case 'INDIA': 
                        $nationality = 105;
                        break;
                }
                    
                    
                    $maid_services = $this->maids_model->get_maid_services_by_maid_id($maid->maid_id);
                    
                    $service = array();
                    foreach ($maid_services as $m_service)
                    {
                        array_push($service, new xmlrpcval($m_service->odoo_service_id, "int"));
                    }
                    
                    $m2m = new xmlrpcval(
                            array(
                                new xmlrpcval(
                                    array(
                                        new xmlrpcval(6,"int"),
                                        new xmlrpcval(0,"int"),
                                        new xmlrpcval(
                                            $service,"array")
                                    ), "array")
                            ),"array");
                    $odoo_id = 0;
                    /*
                    if($maid->odoo_maid_id)
                    {
                            $field = 'id';
                            $value = $maid->odoo_maid_id;     
                            $odoo_id = $this->odoo_search($field, $value, "hr.employee",$user_id);
                            
                    }
                    if(!$odoo_id)
                    {                            
                            $field = 'name';
                            $value = $maid->maid_name;
                            $odoo_id = $this->odoo_search($field, $value, "hr.employee",$user_id);
                            
                    }*/
                                
                   
                    
                    /*if($odoo_id && $odoo_id > 0 && $maid->odoo_synch_status == 0)
                    {
                        
                        $fields = array(
                            'name' => new xmlrpcval($maid->maid_name, "string"),
                            'mob_1' => new xmlrpcval($maid->maid_mobile_1, "string"),
                            'mob_2' => new xmlrpcval($maid->maid_mobile_2, "string"),
                            'flat' => new xmlrpcval($maid->odoo_flat_id, "string"),
                            'gender' => new xmlrpcval($maid->maid_gender == 'M' ? 'male' : 'female', "string"),
                            'country_id' => new xmlrpcval($nationality, "int"),
                            'active' => new xmlrpcval($maid->maid_status == 1 ? TRUE : FALSE, "boolean"),
                            'services' => $m2m,
                            );
                        odoo_operation($user_id, "write", "hr.employee", $fields, $odoo_id);
                        $odoo_maid_id = $odoo_id;
                    }
                    else*/ if( $maid->odoo_synch_status == 0)
                    {
                        $fields = array(
                            'name' => new xmlrpcval($maid->maid_name, "string"),
                            'mob_1' => new xmlrpcval($maid->maid_mobile_1, "string"),
                            'mob_2' => new xmlrpcval($maid->maid_mobile_2, "string"),
                            'flat' => new xmlrpcval($maid->odoo_flat_id, "string"),
                            'gender' => new xmlrpcval($maid->maid_gender == 'M' ? 'male' : 'female', "string"),
                            'country_id' => new xmlrpcval($nationality, "int"),
                            'active' => new xmlrpcval($maid->maid_status == 1 ? TRUE : FALSE, "boolean"),
                            'services' => $m2m,
                            );
                        
                        $odoo_maid_id = odoo_operation($user_id, "create", "hr.employee", $fields, NULL);  
                        
                    }
                    if($odoo_maid_id > 0)
                        $response = $this->maids_model->update_maids(array('odoo_maid_id' => $odoo_maid_id, 'odoo_synch_status' => 1), $maid->maid_id);
                    
                    echo ++$i, '.', $odoo_maid_id, '-', $maid->maid_name;
            }
                   
    }
    /*public function export_to_odoo()
    {
            $user_id = odoo_login();        
            //Maids
            $maids = $this->maids_model->get_maids();
            
            foreach ($maids as $maid)
            {
                $nationality = 0;
                switch ($maid->maid_nationality)
                {
                    case 'NEPALI':
                    case 'Nepal':
                        $nationality = 168;
                        break;
                    case 'Sri Lanka':
                    case 'SRI LANKAN':  
                    case 'SRILANKAN':
                        $nationality = 130;
                        break;
                    case 'Philippines':
                    case 'PHILIPINES':
                    case 'FILIPINO':
                    case 'P':
                    case 'PH':
                        $nationality = 178;
                        break;
                    case 'INDIA': 
                        $nationality = 105;
                        break;
                }
                
                    $maid_services = $this->maids_model->get_maid_services_by_maid_id($maid->maid_id);
                    $service = array();
                    foreach ($maid_services as $m_service)
                    {
                        array_push($service, new xmlrpcval($m_service->odoo_service_id, "int"));
                    }
                    $m2m = new xmlrpcval(
                            array(
                                new xmlrpcval(
                                    array(
                                        new xmlrpcval(6,"int"),
                                        new xmlrpcval(0,"int"),
                                        new xmlrpcval(
                                            $service,"array")
                                    ), "array")
                            ),"array");
                    
                    $fields = array(
                        'name' => new xmlrpcval($maid->maid_name, "string"),
                        'mob_1' => new xmlrpcval($maid->maid_mobile_1, "string"),
                        'mob_2' => new xmlrpcval($maid->maid_mobile_2, "string"),
                        'flat' => new xmlrpcval($maid->odoo_flat_id, "string"),
                        'gender' => new xmlrpcval($maid->maid_gender == 'M' ? 'male' : 'female', "string"),
                        'country_id' => new xmlrpcval($nationality, "int"),
                        'services' => $m2m,
                        );
                    $response = odoo_operation($user_id, "create", "hr.employee", $fields, NULL);  
                    
                    $this->maids_model->update_maids(array('odoo_maid_id' => $response), $maid->maid_id);
                    //echo '<pre>';print_r($response);exit();
            }
                   
    }
    
    
    function schedule() 
    {
            $start_date = date('Y-m-d', strtotime('-1 month'));
            $end_date = date('Y-m-d');
            
            $i = 0;
            $schedule = array();
            $s_date = $start_date;
            while (strtotime($end_date) >= strtotime($s_date))
            {
                $schedule[$i] = new stdClass();
                $schedule[$i]->date = $s_date;
                
                ++$i;
                $s_date = date("Y-m-d", strtotime("+1 day", strtotime($s_date)));
            }           
           
            
            $data = array();
            $data['maids'] = $this->maids_model->get_maids();
            $data['schedule'] = $schedule;

            $layout_data['content_body'] = $this->load->view('maid_schedule', $data, TRUE);
            $layout_data['page_title'] = 'Maid Schedule';
            $layout_data['meta_description'] = 'maid schedule';
            $layout_data['css_files'] = array('demo.css');
            $layout_data['external_js_files'] = array();
            $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js','moment.min.js','jquery.dataTables.min.js');
            $this->load->view('layouts/default', $layout_data);
            
    }
    */
    public function schedule()
    {
        
                if(!user_permission(user_authenticate(), 4))
                {
                    show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
                }
		if($this->uri->segment(3) && strlen(trim($this->uri->segment(3) > 0)))
		{
			$s_date = explode('-', trim($this->uri->segment(2)));
			if(count($s_date) == 3 && checkdate($s_date[1], $s_date[0], $s_date[2]) && $s_date[2] >= 2014 && $s_date[2] <= date('Y') + 2)
			{
				$schedule_date = $s_date[0] . '-' . $s_date[1] . '-' . $s_date[2];
			}				
		}
		else
		{
			$schedule_date = date('d-M-Y');
                        //$schedule_date = date('d-M-Y', strtotime('-1 day'));
		}
		
		if(!isset($schedule_date))
		{
			redirect('booking');
			exit();
		}
		
		$week_day_names = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
		
		$service_date = date('Y-m-d', strtotime($schedule_date));
		$service_week_day = date('w', strtotime($schedule_date));
		
		$maids = $this->maids_model->get_maids(FALSE);
		$zones = $this->zones_model->get_all_zones();
                
               
                if($this->input->post('search_maid_schedule'))
                {
                        $s_date = explode("/", $this->input->post('start_date'));
                        $e_date = explode("/", $this->input->post('end_date'));
                        $maid_id = trim($this->input->post('maid_id'));

                        $start_date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];                    
                        $end_date = $e_date[2] . '-' . $e_date[1] . '-' . $e_date[0];
                }
                else
                {
                        
//                        $start_date = date('Y-m-d', strtotime('-1 week'));
//                        $end_date = date('Y-m-d');
//                        $maid_id = 0;
                    if(trim($this->input->post('start_date')) && trim($this->input->post('end_date')))
                    {
                        $s_date = explode("/", $this->input->post('start_date'));
                        $e_date = explode("/", $this->input->post('end_date'));
                        $start_date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];                    
                        $end_date = $e_date[2] . '-' . $e_date[1] . '-' . $e_date[0];
                        $maid_id = trim($this->input->post('maid_id'));
                    }
                    else
                    {
                        $start_date = date('Y-m-d', strtotime('-1 week'));
                        $end_date = date('Y-m-d');
                        $maid_id = 0;
                    }
                
                }
                
                $s_date = $start_date;
                $all_bookings = array();
                $maid_bookings = array();
                $i = 0;
                while (strtotime($end_date) >= strtotime($s_date))
                {
                    $bookings = $this->bookings_model->get_maid_schedule_by_date($maid_id, $s_date);

                    
                    foreach($bookings as $booking)
                    {
                            $maid_bookings[$s_date][strtotime($booking->time_from)]['booking_id'] = $booking->booking_id;
                            $maid_bookings[$s_date][strtotime($booking->time_from)]['customer'] = $booking->customer_nick_name;
                            $maid_bookings[$s_date][strtotime($booking->time_from)]['zone'] = $booking->zone_name;
                            $maid_bookings[$s_date][strtotime($booking->time_from)]['start_time'] = strtotime($booking->time_from);
                            $maid_bookings[$s_date][strtotime($booking->time_from)]['end_time'] = strtotime($booking->time_to);
                            $maid_bookings[$s_date][strtotime($booking->time_from)]['type'] = $booking->booking_type;
                            $maid_bookings[$s_date][strtotime($booking->time_from)]['user'] = $booking->user_fullname ? $booking->user_fullname : 'N/A';

                            $booked_slots[$s_date]['time'][strtotime($booking->time_from)] = strtotime($booking->time_to);

                            $all_bookings[$booking->booking_id] = new stdClass();
                            $all_bookings[$booking->booking_id]->customer_id = $booking->customer_id;
                            $all_bookings[$booking->booking_id]->customer_nick_name = $booking->customer_nick_name;
                            $all_bookings[$booking->booking_id]->customer_address_id = $booking->customer_address_id;
                            $all_bookings[$booking->booking_id]->customer_zone = $booking->zone_name;
                            $all_bookings[$booking->booking_id]->customer_area = $booking->area_name;
                            $all_bookings[$booking->booking_id]->customer_address = $booking->customer_address;
                            $all_bookings[$booking->booking_id]->maid_id = $booking->maid_id;
                            $all_bookings[$booking->booking_id]->maid_name = $booking->maid_name;
                            $all_bookings[$booking->booking_id]->service_type_id = $booking->service_type_id;
                            $all_bookings[$booking->booking_id]->service_week_day = $week_day_names[$booking->service_week_day];
                            $all_bookings[$booking->booking_id]->time_from = date('g:i a', strtotime($booking->time_from));
                            $all_bookings[$booking->booking_id]->time_to = date('g:i a', strtotime($booking->time_to));
                            $all_bookings[$booking->booking_id]->time_from_stamp = strtotime($booking->time_from);
                            $all_bookings[$booking->booking_id]->time_to_stamp =  strtotime($booking->time_to);
                            $all_bookings[$booking->booking_id]->service_end = $booking->service_end;
                            $all_bookings[$booking->booking_id]->service_end_date = date('d/m/Y', strtotime($booking->service_end_date));
                            $all_bookings[$booking->booking_id]->service_actual_end_date = date('d/m/Y', strtotime($booking->service_actual_end_date));
                            $all_bookings[$booking->booking_id]->booking_note = $booking->booking_note;
                            $all_bookings[$booking->booking_id]->booking_type = $booking->booking_type;
                            $all_bookings[$booking->booking_id]->pending_amount = $booking->pending_amount;
                            $all_bookings[$booking->booking_id]->is_locked = $booking->is_locked;
                            $all_bookings[$booking->booking_id]->discount = $booking->discount ? $booking->discount : 0;

                            $i++;
                    }
                    $s_date = date("Y-m-d", strtotime("+1 day", strtotime($s_date)));
                }     
                
                
                
                $maid_schedule = array();
		$s_date = $start_date;
                $i = 0;
                while (strtotime($end_date) >= strtotime($s_date))
                {
                    $maid_schedule[$i] = new stdClass();
                    $maid_schedule[$i]->date = $s_date;

                    ++$i;
                    $s_date = date("Y-m-d", strtotime("+1 day", strtotime($s_date)));
                }
		
		//print_r($all_bookings); exit();

		$times = array();
		$current_hour_index = 0;
		$time = '12:00 am';  
		$time_stamp = strtotime($time);
                
		for ($i = 0; $i < 24; $i++)
		{
			if(!isset($times['t-' . $i]))
			{
				$times['t-' . $i] = new stdClass();
			}
			
			$times['t-' . $i]->stamp = $time_stamp;
			$times['t-' . $i]->display = $time;
			
			if(date('H') == $i && $service_date == date('Y-m-d'))
			{
				$current_hour_index = 't-' . ($i - 1);
			}
			
			$time_stamp = strtotime('+60mins', strtotime($time));
			$time = date('g:i a', $time_stamp);
		}
                
                
		
		if($this->input->is_ajax_request())
		{
			if($this->input->post('action') && $this->input->post('action') == 'book-maid')
			{
				if($this->input->post('service_date') && $this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0 && $this->input->post('customer_address_id') && is_numeric($this->input->post('customer_address_id')) && $this->input->post('customer_address_id') > 0 && $this->input->post('maid_id') && is_numeric($this->input->post('maid_id')) && $this->input->post('maid_id') > 0 && $this->input->post('service_type_id') && is_numeric($this->input->post('service_type_id')) && $this->input->post('service_type_id') > 0 && $this->input->post('time_from') && is_numeric($this->input->post('time_from')) && $this->input->post('time_from') > 0 && $this->input->post('time_to') && is_numeric($this->input->post('time_to')) && $this->input->post('time_to') > 0 && $this->input->post('time_from') < $this->input->post('time_to') && $this->input->post('booking_type') && ($this->input->post('booking_type') == 'OD' || $this->input->post('booking_type') == 'WE' || $this->input->post('booking_type') == 'BW'))
				{
                                        
                                        $service_date = trim($this->input->post('service_date'));
                                        
					if(strtotime($service_date) < strtotime( date('d-M-Y')))
					{
						echo 'refresh';
						exit();
					}
					
					$customer_id = trim($this->input->post('customer_id'));
					$customer_address_id = trim($this->input->post('customer_address_id'));
					$maid_id = trim($this->input->post('maid_id'));
					$service_type_id = trim($this->input->post('service_type_id'));
					$time_from =  date('H:i', trim($this->input->post('time_from')));
					$time_to = date('H:i', trim($this->input->post('time_to')));
					$booking_type = trim($this->input->post('booking_type'));
                                        $is_locked = $this->input->post('is_locked') ? 1 : 0;
					$repeat_days = array($service_week_day);
					$repeat_end = 'ondate';
					$service_end_date = $service_date;
					$pending_amount = $this->input->post('pending_amount');
                                        
                                        $discount = trim($this->input->post('discount'));
					$booking_note = $this->input->post('booking_note') ? trim($this->input->post('booking_note')) : '';
					
					if(preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_from) == FALSE || preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_to) == FALSE)
					{
						echo 'refresh';
						exit();
					}
					
					if($booking_type != 'OD')
					{
						if($this->input->post('repeat_days') && is_array($this->input->post('repeat_days')) && count($this->input->post('repeat_days')) > 0 &&  count($this->input->post('repeat_days')) <= 7 && $this->input->post('repeat_end') && ($this->input->post('repeat_end') == 'never' || $this->input->post('repeat_end') == 'ondate'))
						{
							$repeat_days = $this->input->post('repeat_days');
							$repeat_end = $this->input->post('repeat_end');
							
							if($repeat_end == 'ondate')
							{
								if($this->input->post('repeat_end_date'))
								{
									$repeat_end_date = $this->input->post('repeat_end_date');
									
									$repeat_end_date_split = explode('/', $repeat_end_date);
									if(count($repeat_end_date_split) == 3 && checkdate($repeat_end_date_split[1] , $repeat_end_date_split[0], $repeat_end_date_split[2]))
									{
										$s_date = new DateTime($service_date);
										$e_date = new DateTime($repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0]);
										//$diff = $s_date->diff($e_date);
										
                                                                                $diff = abs(strtotime($repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0]) - strtotime($service_date));

                                                                                $years = floor($diff / (365*60*60*24));
                                                                                $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                                                                                $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
                                                                                
										if($days < 7)//if($diff->days < 7)
										{
											echo 'refresh';
											exit();
										}
										
										$service_end_date = $repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0];
									}
									else 
									{
										echo 'refresh';
										exit();
									}
								}
								else
								{
									echo 'refresh';
									exit();
								}
							}
						}
						else 
						{
							echo 'refresh';
							exit();
						}
					}
					
					$time_from_stamp = trim($this->input->post('time_from'));
					$time_to_stamp = trim($this->input->post('time_to'));
					
					if($booking_type == 'OD')
					{
						if(isset($booked_slots[$maid_id]['time']))
						{
							foreach($booked_slots[$maid_id]['time'] as $f_time => $t_time)
							{
								if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
								{
									$return = array();
									$return['status'] = 'error';
									$return['message'] = 'The selected time slot is not available for this maid';

									echo json_encode($return);
									exit();
								}
							}
						}
					}
					
					$today_week_day = date('w', strtotime($service_date));
					
					if($booking_type == 'WE')
					{
						foreach($repeat_days as $repeat_day)
						{
							if($repeat_day < $today_week_day)
							{
								$day_diff = (6 - $today_week_day + $repeat_day + 1);
							}
							else
							{
								$day_diff = $repeat_day - $today_week_day;
							}

							$service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));

							$bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($maid_id, $repeat_day);
							foreach($bookings_on_day as $booking_on_day)
							{
								$s_date_stamp = strtotime($service_start_date);
								$e_date_stamp = strtotime($service_end_date);
								$bs_date_stamp = strtotime($booking_on_day->service_start_date);
								$be_date_stamp = strtotime($booking_on_day->service_actual_end_date);
								
								//if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
								if(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
								{
									$f_time = strtotime($booking_on_day->time_from);
									$t_time = strtotime($booking_on_day->time_to);

									if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
									{
										$return = array();
										$return['status'] = 'error';
										$return['message'] = 'The selected time slot is not available for this maid on ' . $week_day_names[$repeat_day] . 's';

										echo json_encode($return);
										exit();
									}
								}
							}
						}
					}
				}
				else
				{
					echo 'refresh';
					exit();
				}
				
				$todays_new_booking = array();
				foreach($repeat_days as $repeat_day)
				{
					if($repeat_day < $today_week_day)
					{
						$day_diff = (6 - $today_week_day + $repeat_day + 1);
					}
					else
					{
						$day_diff = $repeat_day - $today_week_day;
					}
					
					$service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));
					$service_end_date = $repeat_end == 'never' ? $service_start_date : $service_end_date;

					$booking_fields = array();
					$booking_fields['customer_id'] = $customer_id;
					$booking_fields['customer_address_id'] = $customer_address_id;
					$booking_fields['maid_id'] = $maid_id;
					$booking_fields['service_type_id'] = $service_type_id;
					$booking_fields['service_start_date'] = $service_start_date;
					$booking_fields['service_week_day'] = $repeat_day;
					$booking_fields['time_from'] = $time_from;
					$booking_fields['time_to'] = $time_to;
					$booking_fields['booking_type'] = $booking_type;
					$booking_fields['service_end'] = $repeat_end == 'ondate' ? 1 : 0;
					$booking_fields['service_end_date'] = $service_end_date;					
					$booking_fields['booking_note'] = $booking_note;
                                        $booking_fields['is_locked'] = $is_locked;
					$booking_fields['pending_amount'] = $pending_amount;
                                        
                                        					
					$booking_fields['discount'] = $discount;
					$booking_fields['booking_category'] = 'C';
					$booking_fields['booked_by'] = user_authenticate();
					$booking_fields['booking_status'] = 1;
					$booking_fields['booked_datetime'] = date('Y-m-d H:i:s');
					
					$booking_id = $this->bookings_model->add_booking($booking_fields);
					
					if($booking_id)
					{
						$booking_done = TRUE;
                                                
						//$this->appointment_bill($booking_id);
                                                
						if($service_start_date == date('Y-m-d'))
						{
							$todays_new_booking[] = $booking_id;
						}
					}
				}
				
				if(isset($booking_done))
				{
					if(count($todays_new_booking) > 0 && isset($customer_address_id))
					{
						$c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
						if(isset($c_address->zone_id))
						{
							$tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
							
							//android_push(array($tablet->google_reg_id), array('action' => 'new-booking', 'booking-ids' => $todays_new_booking, 'message' => 'New Booking'));
						}
					}
					
					$return = array();
					$return['status'] = 'success';
					$return['maid_id'] = $maid_id;
					$return['customer_id'] = $customer_id;
					$return['time_from'] = $time_from;
					$return['time_to'] = $time_to;
					//$user_logged_in_session = $CI->session->userdata('user_logged_in');
                                        //$return['user'] = $user_logged_in_session['user_fullname'];
                                        
					echo json_encode($return);
					exit();
				}
			}
			// Edited byt Geethu
			if($this->input->post('action') && $this->input->post('action') == 'copy-maid')
			{
				if($this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0 && $this->input->post('maid_id') && is_numeric($this->input->post('maid_id')) && $this->input->post('maid_id') > 0 )
				{
					if(strtotime($schedule_date) < strtotime( date('d-M-Y')))
					{
						echo 'refresh';
						exit();
					}
                                        $booking_id = trim($this->input->post('booking_id'));				
                                        $booking = $this->bookings_model->get_booking_by_id($booking_id);
					
//                                        if($booking->is_locked == 1 && $booking->booked_by != user_authenticate())
//                                        {
//                                            echo 'locked';
//                                            exit();
//                                        }
					$customer_id = html_escape($booking->customer_id);
					$customer_address_id = html_escape($booking->customer_address_id);
					$maid_id = trim($this->input->post('maid_id'));
					$service_type_id = html_escape($booking->service_type_id);
					$time_from =  date('H:i', strtotime(html_escape($booking->time_from)));
					$time_to = date('H:i', strtotime(html_escape($booking->time_to)));
					$booking_type = html_escape($booking->booking_type);
					$repeat_days = array(html_escape($booking->service_week_day));
					$repeat_end = html_escape($booking->service_end) == 0 ? 'never' : 'ondate';
					$service_end_date = html_escape($booking->service_actual_end_date);
                                        $service_start_date = html_escape($booking->service_start_date);
					$pending_amount = html_escape($booking->pending_amount);
                                        
                                        $discount = html_escape($booking->discount);
                                        $is_locked = html_escape($booking->is_locked) ? 1 : 0;
					$booking_note = html_escape($booking->booking_note) ? html_escape($booking->booking_note) : '';
					
					if(preg_match("/([01]?[0-9]|2[0-3]):[0][0]/", $time_from) == FALSE || preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_to) == FALSE)
					{
						echo 'refresh';
						exit();
					}
					
					if($booking_type != 'OD')
					{
						if($repeat_days && is_array($repeat_days) && count($repeat_days) > 0 &&  count($repeat_days) <= 7 && $repeat_end && ($repeat_end == 'never' || $repeat_end == 'ondate'))
						{
							//$repeat_days = $this->input->post('repeat_days');
							//$repeat_end = $this->input->post('repeat_end');
							
							if($repeat_end == 'ondate')
							{
								if($this->input->post('repeat_end_date'))
								{
									$repeat_end_date = $this->input->post('repeat_end_date');
									
									$repeat_end_date_split = explode('/', $repeat_end_date);
									if(count($repeat_end_date_split) == 3 && checkdate($repeat_end_date_split[1] , $repeat_end_date_split[0], $repeat_end_date_split[2]))
									{
										$s_date = new DateTime($service_date);
										$e_date = new DateTime($repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0]);
										//$diff = $s_date->diff($e_date);
                                                                                
                                                                                $diff = abs(strtotime($repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0]) - strtotime($service_date));

                                                                                $years = floor($diff / (365*60*60*24));
                                                                                $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                                                                                $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
										
                                                                                if($days < 7)//if($diff->days < 7)
										{
											echo 'refresh';
											exit();
										}
										
										$service_end_date = $repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0];
									}
									else 
									{
										echo 'refresh';
										exit();
									}
								}
								else
								{
									echo 'refresh';
									exit();
								}
							}
						}
						else 
						{
							echo 'refresh';
							exit();
						}
					}
					
					$time_from_stamp = (html_escape($booking->time_from));
					$time_to_stamp = (html_escape($booking->time_to));
					
					if($booking_type == 'OD')
					{
						if(isset($booked_slots[$maid_id]['time']))
						{
							foreach($booked_slots[$maid_id]['time'] as $f_time => $t_time)
							{
								if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
								{
									$return = array();
									$return['status'] = 'error';
									$return['message'] = 'The selected time slot is not available for this maid';

									echo json_encode($return);
									exit();
								}
							}
						}
					}
					
					$today_week_day = date('w', strtotime($service_date));
					
					if($booking_type == 'WE')
					{
						foreach($repeat_days as $repeat_day)
						{
							if($repeat_day < $today_week_day)
							{
								$day_diff = (6 - $today_week_day + $repeat_day + 1);
							}
							else
							{
								$day_diff = $repeat_day - $today_week_day;
							}

							$service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));

							$bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($maid_id, $repeat_day);
							foreach($bookings_on_day as $booking_on_day)
							{
								$s_date_stamp = strtotime($service_start_date);
								$e_date_stamp = strtotime($service_end_date);
								$bs_date_stamp = strtotime($booking_on_day->service_start_date);
								$be_date_stamp = strtotime($booking_on_day->service_actual_end_date);
								
								//if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
								if(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
								{
									$f_time = strtotime($booking_on_day->time_from);
									$t_time = strtotime($booking_on_day->time_to);

									if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
									{
										$return = array();
										$return['status'] = 'error';
										$return['message'] = 'The selected time slot is not available for this maid on ' . $week_day_names[$repeat_day] . 's';

										echo json_encode($return);
										exit();
									}
								}
							}
						}
					}
				}
				else
				{
					echo 'refresh';
					exit();
				}
				
				$todays_new_booking = array();
				foreach($repeat_days as $repeat_day)
				{
					if($repeat_day < $today_week_day)
					{
						$day_diff = (6 - $today_week_day + $repeat_day + 1);
					}
					else
					{
						$day_diff = $repeat_day - $today_week_day;
					}
					
					$service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));
					$service_end_date = $repeat_end == 'never' ? $service_start_date : $service_end_date;

					$booking_fields = array();
					$booking_fields['customer_id'] = $customer_id;
					$booking_fields['customer_address_id'] = $customer_address_id;
					$booking_fields['maid_id'] = $maid_id;
					$booking_fields['service_type_id'] = $service_type_id;
					$booking_fields['service_start_date'] = $service_start_date;
					$booking_fields['service_week_day'] = $repeat_day;
					$booking_fields['time_from'] = $time_from;
					$booking_fields['time_to'] = $time_to;
					$booking_fields['booking_type'] = $booking_type;
					$booking_fields['service_end'] = $repeat_end == 'ondate' ? 1 : 0;
					$booking_fields['service_end_date'] = $service_end_date;					
					$booking_fields['booking_note'] = $booking_note;					
					$booking_fields['pending_amount'] = $pending_amount;                                        
					$booking_fields['discount'] = $discount;
					$booking_fields['booked_by'] = user_authenticate();
					$booking_fields['booking_status'] = 1;
					$booking_fields['booking_category'] = 'C';
					$booking_fields['booked_datetime'] = date('Y-m-d H:i:s');
					
					$booking_id = $this->bookings_model->add_booking($booking_fields);
					
					if($booking_id)
					{
						$booking_done = TRUE;
						
                                                //$this->appointment_bill($booking_id);
                                                
						if($service_start_date == date('Y-m-d'))
						{
							$todays_new_booking[] = $booking_id;
						}
					}
				}
				
				if(isset($booking_done))
				{
					if(count($todays_new_booking) > 0 && isset($customer_address_id))
					{
						$c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
						if(isset($c_address->zone_id))
						{
							$tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
							
							//android_push(array($tablet->google_reg_id), array('action' => 'new-booking', 'booking-ids' => $todays_new_booking, 'message' => 'New Booking'));
						}
					}
					
					$return = array();
					$return['status'] = 'success';
					$return['maid_id'] = $maid_id;
					$return['customer_id'] = $customer_id;
					$return['time_from'] = $time_from;
					$return['time_to'] = $time_to;
					
					echo json_encode($return);
					exit();
				}
			}
                        // End
			if($this->input->post('action') && $this->input->post('action') == 'update-booking')
			{
				if($this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0 && $this->input->post('time_from') && is_numeric($this->input->post('time_from')) && $this->input->post('time_from') > 0 && $this->input->post('time_to') && is_numeric($this->input->post('time_to')) && $this->input->post('time_to') > 0 && $this->input->post('time_from') < $this->input->post('time_to') && $this->input->post('update_type') && ($this->input->post('update_type') == 'permanent' || $this->input->post('update_type') == 'one-day'))
				{
                                        $service_date = $this->input->post('service_date');
					$update_type = trim($this->input->post('update_type'));
					$booking_id =trim($this->input->post('booking_id'));
					$time_from =  date('H:i', trim($this->input->post('time_from')));
					$time_to = date('H:i', trim($this->input->post('time_to')));
                                        $is_locked = $this->input->post('is_locked') ? 1 : 0;
					$pending_amount = $this->input->post('pending_amount');
                                        $discount = $this->input->post('discount');
					$booking_note = $this->input->post('booking_note') ? trim($this->input->post('booking_note')) : '';
					
					if(preg_match("/([01]?[0-9]|2[0-3]):[0][0]/", $time_from) == FALSE || preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_to) == FALSE)
					{
						echo 'refresh';
						exit();
					}
					
					$booking = $this->bookings_model->get_booking_by_id($booking_id);
                                        
                                        if($booking->is_locked == 1 && $booking->booked_by != user_authenticate())
                                        {
                                            echo 'locked';
                                            exit();
                                        }
					if(!isset($booking->booking_id) || ($booking->service_end == 1 && strtotime($booking->service_actual_end_date) < strtotime(date('Y-m-d'))))
					{
						echo 'refresh';
						exit();
					}
					
					if(is_numeric(trim($this->input->post('customer_address_id'))) && trim($this->input->post('customer_address_id')) > 0 && $booking->customer_address_id != trim($this->input->post('customer_address_id')))
					{
						$customer_address_id = trim($this->input->post('customer_address_id'));
					}
					else
					{
						$customer_address_id = $booking->customer_address_id;
					}
					
					$time_from_stamp = trim($this->input->post('time_from'));
					$time_to_stamp = trim($this->input->post('time_to'));
			
					if($booking->booking_type == 'OD')
					{
						if(isset($booked_slots[$booking->maid_id]['time']))
						{
							foreach($booked_slots[$booking->maid_id]['time'] as $f_time => $t_time)
							{
								if(strtotime($booking->time_from) != $f_time && strtotime($booking->time_to) != $t_time)
								{
									if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
									{
										$return = array();
										$return['status'] = 'error';
										$return['message'] = 'The selected time slot is not available for this maid';

										echo json_encode($return);
										exit();
									}
								}
							}
						}
						
						$update_fields = array();
						$update_fields['customer_address_id'] = $customer_address_id;
						$update_fields['time_from'] = $time_from;
						$update_fields['time_to'] = $time_to;
						$update_fields['pending_amount'] = $pending_amount;
                                                $update_fields['discount'] = $discount;
                                                $update_fields['is_locked'] = $is_locked;
						$update_fields['booking_note'] = $booking_note;
					
						$this->bookings_model->update_booking($booking_id, $update_fields);
						
						if($booking->service_start_date == date('Y-m-d'))
						{
							$c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
							if(isset($c_address->zone_id))
							{
								$tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);

								//android_push(array($tablet->google_reg_id), array('action' => 'update-booking', 'booking-ids' => array($booking_id), 'message' => 'Booking Updated'));
							}
						}
						
						$return = array();
						$return['status'] = 'success';
						$return['maid_id'] = $booking->maid_id;
						$return['customer_id'] = $booking->customer_id;
						$return['time_from'] = $time_from;
						$return['time_to'] = $time_to;

						echo json_encode($return);
						exit();
					}
					else
					{
						if($this->input->post('repeat_end') && (trim($this->input->post('repeat_end')) == 'never' ||  trim($this->input->post('repeat_end')) == 'ondate'))
						{
							$repeat_end = $this->input->post('repeat_end');
							
							if($repeat_end == 'ondate')
							{
								if($this->input->post('repeat_end_date'))
								{
									$repeat_end_date = $this->input->post('repeat_end_date');
									
									$repeat_end_date_split = explode('/', $repeat_end_date);
									if(count($repeat_end_date_split) == 3 && checkdate($repeat_end_date_split[1] , $repeat_end_date_split[0], $repeat_end_date_split[2]))
									{
										$s_date = new DateTime($service_date);
										$e_date = new DateTime($repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0]);
										//$diff = $s_date->diff($e_date);
										
                                                                                $diff = abs(strtotime($repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0]) - strtotime($service_date));

                                                                                $years = floor($diff / (365*60*60*24));
                                                                                $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                                                                                $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
										if($days < 0)//if($diff->days < 0)
										{
											echo 'refresh';
											exit();
										}
										
										$service_end_date = $repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0];
									}
									else 
									{
										echo 'refresh';
										exit();
									}
								}
								else
								{
									echo 'refresh';
									exit();
								}
							}
							else
							{
								$service_end_date = $service_date;
							}
														
							$today_week_day = date('w', strtotime($service_date));
							$service_start_date = $service_date;
					
							if($booking->booking_type == 'WE')
							{
								$bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($booking->maid_id, $today_week_day);
								foreach($bookings_on_day as $booking_on_day)
								{
									if($booking_on_day->booking_id != $booking->booking_id)
									{
										$s_date_stamp = strtotime($service_start_date);
										$e_date_stamp = strtotime($service_end_date);
										$bs_date_stamp = strtotime($booking_on_day->service_start_date);
										$be_date_stamp = strtotime($booking_on_day->service_actual_end_date);

										//if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
										if(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
										{
											$f_time = strtotime($booking_on_day->time_from);
											$t_time = strtotime($booking_on_day->time_to);

											if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
											{
												$return = array();
												$return['status'] = 'error';
												$return['message'] = 'The selected time slot is not available for this maid on ' . $week_day_names[$today_week_day] . 's';

												echo json_encode($return);
												exit();
											}
										}
									}
								}
								
								if($update_type == 'permanent')
								{
									$service_end_date = $repeat_end == 'never' ? $service_start_date : $service_end_date;
									
									// End current booking and add new booking if address id or time changes
									if($booking->customer_address_id != $customer_address_id || $booking->time_from != $time_from || $booking->time_to != $time_to)
									{
										// End current booking
										$this->bookings_model->update_booking($booking_id, array('service_actual_end_date' => date('Y-m-d', strtotime($service_start_date . ' - 1 day')), 'service_end' => 1));

										$booking_fields = array();
										$booking_fields['customer_id'] = $booking->customer_id;
										$booking_fields['customer_address_id'] = $customer_address_id;
										$booking_fields['maid_id'] = $booking->maid_id;
										$booking_fields['service_type_id'] = $booking->service_type_id;
										$booking_fields['service_start_date'] = $service_start_date;
										$booking_fields['service_week_day'] = $today_week_day;
										$booking_fields['time_from'] = $time_from;
										$booking_fields['time_to'] = $time_to;
										$booking_fields['booking_type'] = $booking->booking_type;
										$booking_fields['service_end'] = $repeat_end == 'ondate' ? 1 : 0;
										$booking_fields['service_end_date'] = $service_end_date;				
										$booking_fields['booking_note'] = $booking_note;
                                                                                $booking_fields['is_locked'] = $is_locked;
										$booking_fields['pending_amount'] = $pending_amount;
										$booking_fields['booked_by'] = user_authenticate();
										$booking_fields['booking_status'] = 1;
										$booking_fields['booking_category'] = 'C';
										$booking_fields['booked_datetime'] = date('Y-m-d H:i:s');

										$booking_id = $this->bookings_model->add_booking($booking_fields);

										if($booking_id)
										{
											if($service_start_date == date('Y-m-d'))
											{
												$c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
												if(isset($c_address->zone_id))
												{
													$tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);

													//android_push(array($tablet->google_reg_id), array('action' => 'new-booking', 'booking-ids' => array($booking_id), 'message' => 'New Booking'));
												}
											}
					
											$return = array();
											$return['status'] = 'success';
											$return['maid_id'] = $booking->maid_id;
											$return['customer_id'] = $booking->customer_id;
											$return['time_from'] = $time_from;
											$return['time_to'] = $time_to;

											echo json_encode($return);
											exit();
										}
									}
									else
									{
										$update_fields = array();
										$update_fields['service_end'] = $repeat_end == 'ondate' ? 1 : 0;
										$update_fields['service_end_date'] = $service_end_date;
										$update_fields['service_actual_end_date'] = $service_end_date;
										$update_fields['pending_amount'] = $pending_amount;
                                                                                $update_fields['is_locked'] = $is_locked;
										$update_fields['booking_note'] = $booking_note;

										$this->bookings_model->update_booking($booking_id, $update_fields);
										
										if($booking->service_start_date == date('Y-m-d'))
										{
											$c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
											if(isset($c_address->zone_id))
											{
												$tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);

												//android_push(array($tablet->google_reg_id), array('action' => 'update-booking', 'booking-ids' => array($booking_id), 'message' => 'Booking Updated'));
											}
										}

										$return = array();
										$return['status'] = 'success';
										$return['maid_id'] = $booking->maid_id;
										$return['customer_id'] = $booking->customer_id;
										$return['time_from'] = $time_from;
										$return['time_to'] = $time_to;

										echo json_encode($return);
										exit();
									}
								}
								else if($update_type == 'one-day')
								{
									/* Delete booking one day */
									$delete_b_fields = array();
									$delete_b_fields['booking_id'] = $booking->booking_id;
									$delete_b_fields['service_date'] = $service_date;

									$this->bookings_model->add_booking_delete($delete_b_fields);
									
									/* Add one day booking */
									$booking_fields = array();
									$booking_fields['customer_id'] = $booking->customer_id;
									$booking_fields['customer_address_id'] = $customer_address_id;
									$booking_fields['maid_id'] = $booking->maid_id;
									$booking_fields['service_type_id'] = $booking->service_type_id;
									$booking_fields['service_start_date'] = $service_date;
									$booking_fields['service_week_day'] = $today_week_day;
									$booking_fields['time_from'] = $time_from;
									$booking_fields['time_to'] = $time_to;
									$booking_fields['booking_type'] = 'OD';
									$booking_fields['service_end'] = 1;
									$booking_fields['service_end_date'] = $service_date;				
									$booking_fields['booking_note'] = $booking_note;
                                                                        $booking_fields['is_locked'] = $is_locked;
									$booking_fields['pending_amount'] = $pending_amount;
									$booking_fields['booked_by'] = user_authenticate();
									$booking_fields['booking_status'] = 1;
									$booking_fields['booking_category'] = 'C';
									$booking_fields['booked_datetime'] = date('Y-m-d H:i:s');

									$booking_id = $this->bookings_model->add_booking($booking_fields);

									if($booking_id)
									{
										if($service_date == date('Y-m-d'))
										{
											$c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
											if(isset($c_address->zone_id))
											{
												$tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);

												//android_push(array($tablet->google_reg_id), array('action' => 'new-booking', 'booking-ids' => array($booking_id), 'message' => 'New Booking'));
											}
										}
											
										$return = array();
										$return['status'] = 'success';
										$return['maid_id'] = $booking->maid_id;
										$return['customer_id'] = $booking->customer_id;
										$return['time_from'] = $time_from;
										$return['time_to'] = $time_to;

										echo json_encode($return);
										exit();
									}
								}
							}
						}
						else
						{
							echo 'refresh';
							exit();
						}
					}
										
					echo 'refresh';
					exit();
				}
			}
			
			if($this->input->post('action') && $this->input->post('action') == 'refresh-grid')
			{
                                
                                
                                $data = array();
				$data['maids'] = $maids;
				$data['maid_bookings'] = $maid_bookings;
				$data['all_bookings'] = $all_bookings;
				$data['times'] = $times;
				$data['booking_allowed'] = 1; // Enable past bookings by Geethu
                                //$data['booking_allowed'] = strtotime($schedule_date) >= strtotime( date('d-M-Y')) ? 1 : 0;
                                $schedule_day = date('d F Y, l', strtotime($schedule_date));
                                $data['schedule_day'] = $schedule_day;
                                $data['maid_schedule'] = $maid_schedule;
				$data['current_hour_index'] = $current_hour_index;
				$schedule_grid = $this->load->view('partials/maid_schedule_grid', $data, TRUE);
				$schedule_report = $this->load->view('partials/maid_schedule_report', $data, TRUE);
				
				echo json_encode(array('grid' => $schedule_grid, 'report' => $schedule_report));
				exit();
			}
			
			if($this->input->post('action') && $this->input->post('action') == 'get-customer-address' && $this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0)
			{
				$customer_id = trim($this->input->post('customer_id'));
				
				$customer_addresses = $this->customers_model->get_customer_addresses($customer_id);
                                
                                
				echo json_encode($customer_addresses); 
				exit();
			}
                        if($this->input->post('action') && $this->input->post('action') == 'get-no-of-customer-address' && $this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0)
			{
				$customer_id = trim($this->input->post('customer_id'));
				
				$customer_addresses = $this->customers_model->get_customer_addresses($customer_id);
                                
                                
				echo json_encode(array("address" => $customer_addresses, "no_of_address" => count($customer_addresses))); 
				exit();
			}
			// Edited byt Geethu
			if($this->input->post('action') && $this->input->post('action') == 'get-free-maids' && $this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0)
			{
				$booking_id = trim($this->input->post('booking_id'));
				$booking = $this->bookings_model->get_booking_by_id($booking_id);
                                $same_zone = $this->input->post('same_zone');
                                
                                $not_free_maids = $this->bookings_model->get_not_free_maids_by_booking_id($booking_id, $booking->service_week_day, /*$booking->service_start_date . ' ' .*/$booking->time_from, /*$booking->service_start_date . ' ' .*/ $booking->time_to, $booking->service_start_date);
                                $all_maids = $this->maids_model->get_maids();
                                
                                $nf_maids = array();
                                $all_maid_list = array();
                                foreach ($not_free_maids as $nmaid){
                                    array_push($nf_maids, $nmaid->maid_id);
                                }
                                foreach ($all_maids as $maid){
                                    array_push($all_maid_list, $maid->maid_id);
                                }
                                $free_maids = array_diff($all_maid_list, $nf_maids);                                          
                                
                                 
                                if(!empty($free_maids))
                                {
                                    //$f_maids = array_diff($free_maids, $not_free_maids);
                                    $free_maid_dtls = array();
                                    $free_maid_ids = array();
                                    foreach ($free_maids as $f_maid){
                                        $maid = $this->maids_model->get_maid_by_id($f_maid);
                                        array_push($free_maid_dtls, $maid);
                                        array_push($free_maid_ids, $f_maid);
                                    }
                                    if($same_zone == 1)
                                    {
                                        $zone = $this->customers_model->get_customer_zone_by_address_id($booking->customer_address_id);
                                        $same_zone_maids = $this->bookings_model->get_same_zone_maids($zone->zone_id, $free_maid_ids, $booking->service_week_day);
                                        $free_maid_dtls = array();
                                        foreach ($same_zone_maids as $same_zone_maid){
                                            $maid = $this->maids_model->get_maid_by_id($same_zone_maid->maid_id);
                                            array_push($free_maid_dtls, $maid);                                            
                                        }
                                        if(empty($free_maid_dtls))
                                        {                                           
                                            $return = array();
                                            $return['status'] = 'error';
                                            $return['message'] = 'There are no same zone maids available';

                                            echo json_encode($return);
                                            exit();
                                            
                                        }
                                    }
                                    echo json_encode($free_maid_dtls);
                                    exit();
                                }
                                else
                                {
                                    $return = array();
                                    $return['status'] = 'error';
                                    $return['message'] = 'There are no maids available for the selected time slot';

                                    echo json_encode($return);
                                    exit();
                                }
			}
                        // End 
			if($this->input->post('action') && $this->input->post('action') == 'delete-booking-permanent' && $this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0)
			{
				
                                $schedule_date = $this->input->post('service_date');
                                
                                $booking_id = trim($this->input->post('booking_id'));
				
				$d_booking = $this->bookings_model->get_booking_by_id($booking_id);
                                
				if($d_booking->is_locked == 1 && $d_booking->booked_by != user_authenticate())
                                {
                                    echo 'locked';
                                    exit();
                                }
				if(isset($d_booking->booking_id))
				{
					if($d_booking->booking_type == 'OD')
					{
						$this->bookings_model->update_booking($booking_id, array('booking_status' => 2));
					}
					else
					{
						$this->bookings_model->update_booking($booking_id, array('booking_status' => 2, 'service_actual_end_date' => date('Y-m-d', strtotime($schedule_date . ' - 1 day')), 'service_end' => 1));
					}

					if(($d_booking->booking_type == 'WE' && $d_booking->service_start_date == date('Y-m-d')) || ($d_booking->service_week_day == date('w') && strtotime(date('Y-m-d')) >= strtotime($d_booking->service_start_date) && strtotime(date('Y-m-d')) < strtotime($schedule_date)))
					{
						$c_address = $this->customers_model->get_customer_address_by_id($d_booking->customer_address_id);
						if(isset($c_address->zone_id))
						{
							$tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);

							//android_push(array($tablet->google_reg_id), array('action' => 'delete-booking', 'booking-ids' => array($booking_id), 'message' => 'Booking Deleted'));
						}
					}
				
					echo 'success';
					exit();
				}
				
				echo 'refresh';
				exit();
			}
			
			if($this->input->post('action') && $this->input->post('action') == 'delete-booking-one-day' && $this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0)
			{
                            
                                $service_date = $this->input->post('service_date');
                                
				$booking_id = trim($this->input->post('booking_id'));
				
				$d_booking = $this->bookings_model->get_booking_by_id($booking_id);
				
                                if($d_booking->is_locked == 1 && $d_booking->booked_by != user_authenticate())
                                {
                                    echo 'locked';
                                    exit();
                                }
				if(isset($d_booking->booking_id))
				{
					if($d_booking->booking_type != 'OD')
					{
						$delete_b_fields = array();
						$delete_b_fields['booking_id'] = $booking_id;
						$delete_b_fields['service_date'] = $service_date;
                                                $delete_b_fields['deleted_by'] = user_authenticate();
                                                
						$this->bookings_model->add_booking_delete($delete_b_fields);
					}
				}
				
				//if($service_date == date('Y-m-d'))
				{
					$c_address = $this->customers_model->get_customer_address_by_id($d_booking->customer_address_id);
					if(isset($c_address->zone_id))
					{
						$tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);

						//android_push(array($tablet->google_reg_id), array('action' => 'delete-booking', 'booking-ids' => array($booking_id), 'message' => 'Booking Deleted'));
					}
				}
				
				echo 'success';
				exit();
			}
                        
                        /*Edited by Betsy Bernard */
        
                        if($this->input->post('action') && $this->input->post('action') == 'maid-leave' && $this->input->post('maid_id') && is_numeric($this->input->post('maid_id')) && $this->input->post('maid_id') > 0)
                        {
                            $maid_id = trim($this->input->post('maid_id'));

                            if(!empty($all_bookings))
                            {
                                echo "exists";
                                exit();
                            }
                            else
                            {
                                $leave = $this->maids_model->check_maid_leave_by_date($maid_id, $start_date, $end_date);
                                if($leave == 0)
                                {
                                    echo "Leave";
                                    exit();
                                }
                                else if($leave == 1)
                                {
                                    $begin = new DateTime($start_date);
                                    $end   = new DateTime($end_date);

                                    for($i = $begin; $begin <= $end; $i->modify('+1 day')){
                                        //echo $i->format("Y-m-d");
                                        $maid_fields = array();
                                        $maid_fields['maid_id'] = $maid_id;
                                        $maid_fields['leave_date'] = $i->format("Y-m-d");
                                        $maid_fields['leave_status'] = 1;  

                                        $this->maids_model->add_maid_leave($maid_fields); 

                                    }
                                    echo "success";
                                    exit();
                                } 
                            }

                        }
			
			echo 'refresh';
			exit();
		}
		
		$schedule_day = date('l d / M / Y', strtotime($schedule_date));
		$next_day = date('d-m-Y', strtotime($schedule_date . ' + 1 day'));
		$prev_day = date('d-m-Y', strtotime($schedule_date . ' - 1 day'));
		$day_number = date('w', strtotime($schedule_date));
		
		$customers = $this->customers_model->get_customers();
                
		$service_types = $this->service_types_model->get_service_types();

		$all_maids = array();
		foreach($maids as $maid)
		{
			$all_maids['m-' . $maid->maid_id] = $maid->maid_name;
		}
		
                
		$data = array(); 
                $data['start_date'] = date("d/m/Y", strtotime($start_date));
                $data['end_date'] = date("d/m/Y", strtotime($end_date));
                $data['maid_id'] = $maid_id;
		$data['schedule_day'] = $schedule_day;
		$data['schedule_day_c'] = date('d-m-Y', strtotime($schedule_date));
		$data['repeate_end_start_c'] = date('Y, m, d', strtotime($schedule_date . ' + 7 day'));
		$data['next_day'] = $next_day;
		$data['prev_day'] = $prev_day;
		$data['day_number'] = $day_number;
		$data['times'] = $times;                           
		$data['maid_bookings'] = $maid_bookings;
		$data['maids'] = $maids;
                $data['zones'] = $zones;
		$data['all_maids'] = json_encode($all_maids);
		$data['customers'] = $customers;
		$data['service_types'] = $service_types;
		$data['booking_allowed'] = strtotime($schedule_date) >= strtotime( date('d-M-Y')) ? 1 : 0;
		$data['current_hour_index'] = $current_hour_index;
                $data['maid_schedule'] = $maid_schedule;
		$data['schedule_grid'] = $this->load->view('partials/maid_schedule_grid', $data, TRUE);
		
		$layout_data = array();
		$layout_data['content_body'] = $this->load->view('maid_schedule', $data, TRUE);		
		$layout_data['page_title'] = 'Schedule';
		$layout_data['meta_description'] = 'Schedule';
		$layout_data['css_files'] = array('jquery.fancybox.css', 'bootstrap.css', 'datepicker.css');
		$layout_data['external_js_files'] = array();
		$layout_data['js_files'] = array('jquery.fancybox.pack.js', 'bootstrap-datepicker.js', 'maid.js');
		
		$this->load->view('layouts/default', $layout_data);
	}
}
