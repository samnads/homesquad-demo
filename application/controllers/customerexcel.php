<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat; // Import the NumberFormat class
use DateTime;
class Customerexcel extends CI_Controller
{
    /**======================================================
     * 
     ========================================================*/
    public function __construct()
    {
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        if (!user_permission(user_authenticate(), 6)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $this->load->model('customers_model');
        $this->load->model('reports_model');
        $this->load->model('zones_model');
        $this->load->model('bookings_model');
        $this->load->model('maids_model');
    }
    /**======================================================
     * 
     ========================================================*/
    public function index()
    {
    }
    /**======================================================
     * 
     ========================================================*/
    public function customerexportexcel_new()
    {
        $fromdate = $this->input->post('fromdate');
        $todate = $this->input->post('todate');
        $status = $this->input->post('status');
        $payment_type = $this->input->post('payment_type');
        $source = $this->input->post('source');
        $sort_cust_type = $this->input->post('sort_cust_type');
        $search = $this->input->post('search');
        $data['customers'] = $this->customers_model->get_all_newcustomers_excel($fromdate, $todate, $status, $payment_type, $source, $sort_cust_type, $search);
        $this->load->view('customer_excel_view', $data);
    }
    /**======================================================
     * 
     ========================================================*/
    public function customerexportexcel()
    {
        $fromdate = $this->input->post('fromdate');
        $todate = $this->input->post('todate');
        $status = $this->input->post('status');
        $payment_type = $this->input->post('payment_type');
        $source = $this->input->post('source');
        $sort_cust_type = $this->input->post('sort_cust_type');
        $search = $this->input->post('search');
        $customers = $this->customers_model->get_all_newcustomers_excel($fromdate, $todate, $status, $payment_type, $source, $sort_cust_type, $search);
        $fileName = 'customers.xlsx';
        $styleArray = array(
            'font' => array(
                'bold' => true,
            )
        );
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Sl.No');
        $sheet->setCellValue('B1', 'Customer Name');
        $sheet->setCellValue('C1', 'Contact No');
        $sheet->setCellValue('D1', 'Email-Id');
        $sheet->setCellValue('E1', 'Area');
        $sheet->setCellValue('F1', 'Address');
        $sheet->setCellValue('G1', 'Source');
        $sheet->setCellValue('H1', 'Last Job');
        $sheet->setCellValue('I1', 'Paytype');
        $sheet->setCellValue('J1', 'Price Per Hr');
        $sheet->setCellValue('K1', 'Added Date time');
        $sheet->setCellValue('L1', 'Status');
        $sheet->getStyle("A1:K1")->applyFromArray($styleArray);
        $sheet->getColumnDimension("A")->setAutoSize(true);
        $sheet->getColumnDimension("B")->setAutoSize(true);
        $sheet->getColumnDimension("C")->setAutoSize(true);
        $sheet->getColumnDimension("D")->setAutoSize(true);
        $sheet->getColumnDimension("E")->setAutoSize(true);
        $sheet->getColumnDimension("F")->setAutoSize(true);
        $sheet->getColumnDimension("G")->setAutoSize(true);
        $sheet->getColumnDimension("H")->setAutoSize(true);
        $sheet->getColumnDimension("I")->setAutoSize(true);
        $sheet->getColumnDimension("J")->setAutoSize(true);
        $sheet->getColumnDimension("K")->setAutoSize(true);
        $sheet->getColumnDimension("L")->setAutoSize(true);
        $rows = 2;
        $i = 1;
        foreach ($customers as $customer) {
            if ($customer->building != "") {
                $apartmnt_no = 'Apartment No:' . $customer->building . ', ' . $customer->unit_no . ', ' . $customer->street . ',';
            } else {
                $apartmnt_no = "";
            }
            if ($customer->payment_type == "D") {
                $paytype = "Daily";
            } else if ($customer->payment_type == "W") {
                $paytype = "Weekly";
            } else if ($customer->payment_type == "M") {
                $paytype = "Monthly";
            } else {
                $paytype = "";
            }
            if ($customer->customer_status == 1) {
                $stats = 'Active';
            } else {
                $stats = 'Inactive';
            }
            $sheet->setCellValue('A' . $rows, $i);
            $sheet->setCellValue('B' . $rows, $customer->customer_name);
            $sheet->setCellValue('C' . $rows, $customer->mobile_number_1);
            // Format the cell as a number with a specific format
            $sheet->getStyle('C' . $rows)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
            $sheet->setCellValue('D' . $rows, $customer->email_address);
            $sheet->setCellValue('E' . $rows, $customer->area_name);
            $sheet->setCellValue('F' . $rows, $apartmnt_no . $customer->customer_address);
            $sheet->setCellValue('G' . $rows, $customer->customer_source);
            $sheet->setCellValue('H' . $rows, $customer->last_date);
            $sheet->setCellValue('I' . $rows, $paytype);
            $sheet->setCellValue('J' . $rows, $customer->price_hourly);
            // $sheet->setCellValue('K' . $rows, $customer->customer_added_datetime);
            
            $customerAddedDatetime = $customer->customer_added_datetime;
            $datetime = new DateTime($customerAddedDatetime);
            $formattedDatetime = $datetime->format('d-m-Y h:i:s A');
            $sheet->setCellValue('K' . $rows, $formattedDatetime);
            $sheet->setCellValue('L' . $rows, $stats);
            $rows++;
            $i++;
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save("upload/" . $fileName);
        header("Content-Type: application/vnd.ms-excel");
        echo base_url() . "upload/" . $fileName;
        exit();
    }
    /**======================================================
     * Function to download
     ========================================================*/
    public function customeroutstandingexportexcel()
    {
        $fromdate = $this->input->post('fromdate');
        $todate = $this->input->post('todate');
        $status = $this->input->post('status');
        $custid = $this->input->post('custid');
        $customers = $this->customers_model->get_all_newcustomers_outstanding_excel($fromdate, $todate, $status, $custid);
        $fileName = 'Customer Outstanding '.date('Y-m-d').'.xlsx';
        $styleArray = array(
            'font' => array(
                'bold' => true,
            )
        );
        $paymentTypeMap = [
            'D'     => 'Daily',
            'M'     => 'Monthly',
            'MA'    => 'Monthly Advance'
         ];
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Sl.No');
        $sheet->setCellValue('B1', 'Customer Name');
        $sheet->setCellValue('C1', 'Contact No');
        $sheet->setCellValue('D1', 'Area');
        $sheet->setCellValue('E1', 'Balance');
        $sheet->setCellValue('F1', 'Last Invoice Date');
        $sheet->setCellValue('G1', 'Type Of Client');
        $sheet->getStyle("A1:G1")->applyFromArray($styleArray);
        $sheet->getColumnDimension("A")->setAutoSize(true);
        $sheet->getColumnDimension("B")->setAutoSize(true);
        $sheet->getColumnDimension("C")->setAutoSize(true);
        $sheet->getColumnDimension("D")->setAutoSize(true);
        $sheet->getColumnDimension("E")->setAutoSize(true);
        $rows = 2;
        $i = 1;
        $formatCode = '0.00'; // Custom format for two decimal points
        foreach ($customers as $customer) {
            if ($customer->building != "") {
                $apartmnt_no = 'Apartment No:' . $customer->building . ', ' . $customer->unit_no . ', ' . $customer->street . ',';
            } else {
                $apartmnt_no = "";
            }
            $sheet->setCellValue('A' . $rows, $i);
            $sheet->setCellValue('B' . $rows, $customer->customer_name);
            $sheet->setCellValue('C' . $rows, $customer->mobile_number_1);
            // Format the cell as a number with a specific format
            $sheet->getStyle('C' . $rows)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
            $sheet->setCellValue('D' . $rows, $customer->area_name);
            $sheet->setCellValue('E' . $rows, $customer->balance);
            // Format the cell as a number with two decimal points
            $sheet->getStyle('E' . $rows)->getNumberFormat()->setFormatCode($formatCode);

            $rawDate = $customer->last_invoice_date;
            $formattedDate = date('d-m-Y', strtotime($rawDate));
            $sheet->setCellValue('F' . $rows, $formattedDate);
            // Format the cell as a date with the desired format (dd-mm-yyyy)
            $sheet->getStyle('F' . $rows)->getNumberFormat()->setFormatCode('dd-mm-yyyy');
            $sheet->setCellValue('G' . $rows, $paymentTypeMap[$customer->payment_type]);
            $rows++;
            $i++;
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save("temp/" . $fileName);
        header("Content-Type: application/vnd.ms-excel");
        echo base_url("temp/" . $fileName);
        exit();
    }
    /**======================================================
     * 
     ========================================================*/
    public function requestexportexcel()
    {
        $fromdate = $this->input->post('fromdate');
        $todate = $this->input->post('todate');
        $status = $this->input->post('status');
        $search = $this->input->post('search');
        $requests = $this->users_model->get_all_newrequests_excel($fromdate, $todate, $status, $search);
        $fileName = 'requests.xlsx';
        $styleArray = array(
            'font' => array(
                'bold' => true,
            )
        );
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Sl.No');
        $sheet->setCellValue('B1', 'Customer Name');
        $sheet->setCellValue('C1', 'Service Date');
        $sheet->setCellValue('D1', 'Shift');
        $sheet->setCellValue('E1', 'Maid');
        $sheet->setCellValue('F1', 'Reason');
        $sheet->setCellValue('G1', 'Requested By');
        $sheet->setCellValue('H1', 'Approved By');
        $sheet->setCellValue('I1', 'Status');
        $sheet->getStyle("A1:I1")->applyFromArray($styleArray);
        $sheet->getColumnDimension("A")->setAutoSize(true);
        $sheet->getColumnDimension("B")->setAutoSize(true);
        $sheet->getColumnDimension("C")->setAutoSize(true);
        $sheet->getColumnDimension("D")->setAutoSize(true);
        $sheet->getColumnDimension("E")->setAutoSize(true);
        $sheet->getColumnDimension("F")->setAutoSize(true);
        $sheet->getColumnDimension("G")->setAutoSize(true);
        $sheet->getColumnDimension("H")->setAutoSize(true);
        $sheet->getColumnDimension("I")->setAutoSize(true);
        $rows = 2;
        $i = 1;
        foreach ($requests as $val) {
            if ($val->request_status == 0) {
                $status = "Pending";
            } else if ($val->request_status == 1) {
                $status = "Approved";
            } else if ($val->request_status == 2) {
                $status = "Rejected";
            } else if ($val->request_status == 3) {
                $status = "Cancelled";
            }
            $sheet->setCellValue('A' . $rows, $i);
            $sheet->setCellValue('B' . $rows, $val->customer_name);
            $sheet->setCellValue('C' . $rows, ($val->service_date) ? date("d/m/Y", strtotime($val->service_date)) : "");
            $sheet->setCellValue('D' . $rows, $val->time_from . '-' . $val->time_to);
            $sheet->setCellValue('E' . $rows, $val->maid_name);
            $sheet->setCellValue('F' . $rows, $val->request_reason);
            $sheet->setCellValue('G' . $rows, $val->user_fullname);
            $sheet->setCellValue('H' . $rows, $val->approvedBy);
            $sheet->setCellValue('I' . $rows, $status);
            $rows++;
            $i++;
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save("upload/" . $fileName);
        header("Content-Type: application/vnd.ms-excel");
        echo base_url() . "upload/" . $fileName;
        exit();
    }
    /**======================================================
     * 
     ========================================================*/
    public function schedule_report_excellatest()
    {
        $service_date = $this->input->post('fromdate');
        $service_date_to = $this->input->post('todate');
        $zone_id = $this->input->post('servicezoneid');
        $schedule_report = array();
        while (strtotime($service_date) <= strtotime($service_date_to)) {
            $per_report = $this->reports_model->get_schedule_report_latest($service_date, $zone_id);
            array_push($schedule_report, $per_report);
            $service_date = date('Y-m-d', strtotime("+1 day", strtotime($service_date)));
        }
        $fileName = 'Schedule Report _ ' . $service_date . ' - ' . $service_date_to . '.xlsx';
        $styleArray = array(
            'font' => array(
                'bold' => true,
            )
        );
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Sl.No');
        $sheet->setCellValue('B1', 'Date of Service');
        $sheet->setCellValue('C1', 'Crews Name');
        $sheet->setCellValue('D1', 'Address');
        $sheet->setCellValue('E1', 'Booking Time');
        $sheet->setCellValue('F1', 'Hours');
        $sheet->setCellValue('G1', 'Customer Name');
        $sheet->setCellValue('H1', 'Cleaning Supplies');
        $sheet->setCellValue('I1', 'Discount');
        $sheet->setCellValue('J1', 'Service Amount');
        $sheet->setCellValue('K1', 'Previous Outstanding');
        $sheet->setCellValue('L1', 'Notes');
        $sheet->setCellValue('M1', 'Payment Mode');
        $sheet->getStyle("A1:M1")->applyFromArray($styleArray);
        $sheet->getColumnDimension("A")->setAutoSize(true);
        $sheet->getColumnDimension("B")->setAutoSize(true);
        $sheet->getColumnDimension("C")->setAutoSize(true);
        $sheet->getColumnDimension("D")->setAutoSize(true);
        $sheet->getColumnDimension("E")->setAutoSize(true);
        $sheet->getColumnDimension("F")->setAutoSize(true);
        $sheet->getColumnDimension("G")->setAutoSize(true);
        $sheet->getColumnDimension("H")->setAutoSize(true);
        $sheet->getColumnDimension("I")->setAutoSize(true);
        $sheet->getColumnDimension("J")->setAutoSize(true);
        $sheet->getColumnDimension("K")->setAutoSize(true);
        $sheet->getColumnDimension("L")->setAutoSize(true);
        $sheet->getColumnDimension("M")->setAutoSize(true);
        $rows = 2;
        $i = 1;
        $totamount = 0;
        $total_hours = 0;
        foreach ($schedule_report as $rep) {
            foreach ($rep as $report) {
                $totamount += $report->total_amount;
                $addrr = "";
                if ($report->building != "") {
                    $addrr .= $report->building . ', ';
                }
                if ($report->customer_address != "") {
                    $addrr .= $report->customer_address . ', ';
                }
                if ($report->area_name != "") {
                    $addrr .= $report->area_name;
                }
                $hours = (strtotime($report->time_to) - strtotime($report->time_from)) / 3600;
                $total_hours += $hours;
                $hrs = strtotime($report->time_to) - strtotime($report->time_from);
                $rep_hrs = gmdate("H:i", $hrs);
                $material = ($report->cleaning_material == 'Y') ? 'Yes' : 'No';
                $sheet->setCellValue('A' . $rows, $i);
                $reportDate = $report->sch_date; // Assuming $report->sch_date is your date value
                $dateTime = DateTime::createFromFormat('d/m/Y', $reportDate);
                if ($dateTime) {
                    $formattedDate = $dateTime->format('d-m-Y');
                } else {
                    $formattedDate = $reportDate;
                }
                $sheet->setCellValue('B' . $rows, $formattedDate);
                // Format the cell as a date with the desired format (dd-mm-yyyy)
                $sheet->getStyle('B' . $rows)->getNumberFormat()->setFormatCode('dd-mm-yyyy');
                $sheet->setCellValue('C' . $rows, $report->allmaidss);
                $sheet->setCellValue('D' . $rows, $addrr);
                $sheet->setCellValue('E' . $rows, $report->booking_time_from . " - " . $report->booking_time_to);
                $sheet->setCellValue('F' . $rows, $rep_hrs);
                $sheet->setCellValue('G' . $rows, $report->customer_name);
                $sheet->setCellValue('H' . $rows, $material);
                $sheet->setCellValue('I' . $rows, $report->discount);
                $sheet->setCellValue('J' . $rows, $report->total_amount);
                $sheet->setCellValue('K' . $rows, $report->balance);
                $sheet->setCellValue('L' . $rows, $report->booking_note);
                $sheet->setCellValue('M' . $rows, $report->pay_by);
                $rows++;
                $i++;
            }
            // $rows = count($rep);
            // $row2 = $rows + 2;
            // $sheet->setCellValue('A' . $row2, '');
            // $sheet->setCellValue('B' . $row2, '');
            // $sheet->setCellValue('C' . $row2, '');
            // $sheet->setCellValue('D' . $row2, '');
            // $sheet->setCellValue('E' . $row2, 'Total');
            // $sheet->setCellValue('F' . $row2, $total_hours);
            // $sheet->setCellValue('G' . $row2, '');
            // $sheet->setCellValue('H' . $row2, '');
            // $sheet->setCellValue('I' . $row2, $totamount);
            // $sheet->setCellValue('J' . $row2, '');
            // $sheet->setCellValue('K' . $row2, '');
            // $sheet->setCellValue('L' . $row2, '');
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save("upload/" . $fileName);
        header("Content-Type: application/vnd.ms-excel");
        echo base_url() . "upload/" . $fileName;
        exit();
    }
	/**======================================================
     * 
     ========================================================*/
	public function maid_performance_report_excel()
    {
        $fromdate = $this->input->post('fromdate');
        $todate = $this->input->post('todate');
        $zoneid = $this->input->post('zoneid');
		
		$performance_report = array();

        if ($this->input->post('fromdate')) {
            $f_s_date = DateTime::createFromFormat('d/m/Y', $this->input->post('fromdate'));
            $st_date = $f_s_date->format('Y-m-d');
            $schedule_date_from = $this->input->post('fromdate');
        } else {
            $st_date = date('Y-m-d');
            $schedule_date_from = date('d/m/Y');

        }
		if ($this->input->post('todate')) {
            $t_s_date = DateTime::createFromFormat('d/m/Y', $this->input->post('todate'));
            $fr_date = $t_s_date->format('Y-m-d');
            $schedule_date_to = $this->input->post('todate');
        } else {
            $fr_date = date('Y-m-d');
            $schedule_date_to = date('d/m/Y');
        }
		if ($this->input->post('zoneid')) {
            $zone_id = $this->input->post('zoneid');
            $zone_names = $this->zones_model->get_zone_by_id($zone_id);
        } else {
            $zone_id = "";
            $zone_names = "";
        }
		$getmaids = $this->maids_model->get_full_maids();
		$per_report = array();

        while (strtotime($st_date) <= strtotime($fr_date)) {

            $newDate = date("d/m/Y", strtotime($st_date));
			// $getmaids = $this->maids_model->get_full_maids_new($st_date);
            foreach ($getmaids as $getmaid) {
                $per_report = $this->bookings_model->get_maid_performance($getmaid->maid_id, $st_date, $zone_id);
                foreach ($per_report as $key => $per_repor) {
                    $per_report[$key]->date = $newDate;
                }
                array_push($performance_report, $per_report);
            }

            $st_date = date('Y-m-d', strtotime("+1 day", strtotime($st_date)));

        }
		$fileName = 'Booking Report '.$st_date.' - '.$fr_date.'.xlsx';
        $styleArray = array(
            'font' => array(
                'bold' => true,
            )
        );
		$spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Sl.No');
        $sheet->setCellValue('B1', 'Cleaner');
        $sheet->setCellValue('C1', 'Client Name');
        $sheet->setCellValue('D1', 'Date');
        $sheet->setCellValue('E1', 'Phone');
        $sheet->setCellValue('F1', 'Booking Type');
        $sheet->setCellValue('G1', 'Material');
        $sheet->setCellValue('H1', 'Source');
        $sheet->setCellValue('I1', 'Timings');
        $sheet->setCellValue('J1', 'Driver');
        $sheet->setCellValue('K1', 'Payment');
        $sheet->setCellValue('L1', 'Total');
        $sheet->setCellValue('M1', 'Hours');
        $sheet->getStyle("A1:M1")->applyFromArray($styleArray);
        $sheet->getColumnDimension("A")->setAutoSize(true);
        $sheet->getColumnDimension("B")->setAutoSize(true);
        $sheet->getColumnDimension("C")->setAutoSize(true);
        $sheet->getColumnDimension("D")->setAutoSize(true);
        $sheet->getColumnDimension("E")->setAutoSize(true);
        $sheet->getColumnDimension("F")->setAutoSize(true);
        $sheet->getColumnDimension("G")->setAutoSize(true);
        $sheet->getColumnDimension("H")->setAutoSize(true);
        $sheet->getColumnDimension("I")->setAutoSize(true);
        $sheet->getColumnDimension("J")->setAutoSize(true);
        $sheet->getColumnDimension("K")->setAutoSize(true);
        $sheet->getColumnDimension("L")->setAutoSize(true);
        $sheet->getColumnDimension("M")->setAutoSize(true);
        $rows = 2;
        $sln = 1;
        $formatCode = '0.00';
		foreach($performance_report as $performance)
		{
			foreach ($performance as $key)
			{
				$booingdate = $key->date;
				$booingdate = str_replace('/', '-', $booingdate);
				$booingdate = date('Y-m-d', strtotime($booingdate));
				$maid_disable_date=date('Y-m-d', strtotime($key->maid_disabled_datetime));
				// if($key->maid_status=='0' && ($booingdate>$maid_disable_date))
				// { 
					// continue;
				// }
				$cursdate = date('Y-m-d');
				if($key->maid_status=='0' && ($booingdate >= $cursdate))
				{ 
					continue;
				}
				
				$sheet->setCellValue(
					'A' . $rows, $sln++
				);
				$maid_names = explode(',', $key->maid_names);
				$chk = 1;
				foreach ($maid_names as $maid_name) {
					$prev_value = $sheet->getCell('B' . $rows)->getValue();
					if(count($maid_names)==$chk)
					{
						$sheet->setCellValue('B' . $rows, $prev_value.$maid_name);
					} else {
						$sheet->setCellValue('B' . $rows, $prev_value.$maid_name.PHP_EOL);
						
					}
					$chk++;
				}
				$sheet->getStyle('B' . $rows)->getAlignment()->setWrapText(true);
				
				$customers = explode(',', $key->customer_names);
				$chk1 = 1;
				foreach ($customers as $customer) {
					$prev_value1 = $sheet->getCell('C' . $rows)->getValue();
					if(count($customers)==$chk1)
					{
						$sheet->setCellValue('C' . $rows, $prev_value1.$customer);
					} else {
						$sheet->setCellValue('C' . $rows, $prev_value1.$customer.PHP_EOL);
						
					}
					$chk1++;
				}
				$sheet->getStyle('C' . $rows)->getAlignment()->setWrapText(true);
				
				$customers = explode(',', $key->customer_names);
				$chk2 = 1;
				foreach ($customers as $customer) {
					$prev_value2 = $sheet->getCell('D' . $rows)->getValue();
					if(count($customers)==$chk2)
					{
						$sheet->setCellValue('D' . $rows, $prev_value2.$key->service_start_date);
					} else {
						$sheet->setCellValue('D' . $rows, $prev_value2.$key->service_start_date.PHP_EOL);
						
					}
					$chk2++;
				}
				$sheet->getStyle('D' . $rows)->getAlignment()->setWrapText(true);
				
				$mobile_numbers = explode(',', $key->mobile_number);
				$chk3 = 1;
				foreach ($mobile_numbers as $mobile_number) {
					$prev_value3 = $sheet->getCell('E' . $rows)->getValue();
					if(count($mobile_numbers)==$chk3)
					{
						$sheet->setCellValue('E' . $rows, ' '.$prev_value3.$mobile_number);
					} else {
						$sheet->setCellValue('E' . $rows, ' '.$prev_value3.$mobile_number.PHP_EOL);
					}
					$chk3++;
				}
				$sheet->getStyle('E' . $rows)->getAlignment()->setWrapText(true);
				
				$booking_typ = explode(',', $key->booking_type);
				$chk4 = 1;
				foreach ($booking_typ as $b_type) {
					if($b_type == "OD")
					{
						$booktype = "One Time";
					}else if($b_type == "WE")
					{
						$booktype = "Weekly";
					}else if($b_type == "BW")
					{
						$booktype = "Bi-Weekly";
					}
					else {
						$booktype = "";
					}
					$prev_value4 = $sheet->getCell('F' . $rows)->getValue();
					if(count($booking_typ)==$chk4)
					{
						$sheet->setCellValue('F' . $rows, $prev_value4.$booktype);
					} else {
						$sheet->setCellValue('F' . $rows, $prev_value4.$booktype.PHP_EOL);
					}
					$chk4++;
				}
				$sheet->getStyle('F' . $rows)->getAlignment()->setWrapText(true);
				
				$materials = explode(',', $key->cleaning_materials);
				$chk5 = 1;
				foreach ($materials as $material) 
				{
					if($material == "Y")
					{
						$mat = "Yes";
					} else {
						$mat = "No";
					}
					$prev_value5 = $sheet->getCell('G' . $rows)->getValue();
					if(count($materials)==$chk5)
					{
						$sheet->setCellValue('G' . $rows, $prev_value5.$mat);
					} else {
						$sheet->setCellValue('G' . $rows, $prev_value5.$mat.PHP_EOL);
					}
					$chk5++;
				}
				$sheet->getStyle('G' . $rows)->getAlignment()->setWrapText(true);
				
				$sources = explode(',', $key->sources);
				$chk6 = 1;
				foreach ($sources as $source) {
					$prev_value6 = $sheet->getCell('H' . $rows)->getValue();
					if(count($sources)==$chk6)
					{
						$sheet->setCellValue('H' . $rows, $prev_value6.$source);
					} else {
						$sheet->setCellValue('H' . $rows, $prev_value6.$source.PHP_EOL);
					}
					$chk6++;
				}
				$sheet->getStyle('H' . $rows)->getAlignment()->setWrapText(true);
				
				$shifts = explode(',', $key->time_from);
				$shifts2 = explode(',', $key->time_to);
				$chk7 = 1;
				$i = 0;
				foreach ($shifts as $shift) {
					$prev_value7 = $sheet->getCell('I' . $rows)->getValue();
					$shiftneww = $shift . ' - ' . $shifts2[$i];
					if(count($shifts)==$chk7)
					{
						$sheet->setCellValue('I' . $rows, $prev_value7.$shiftneww);
					} else {
						$sheet->setCellValue('I' . $rows, $prev_value7.$shiftneww.PHP_EOL);
					}
					$chk7++;
					$i++;
				}
				$sheet->getStyle('I' . $rows)->getAlignment()->setWrapText(true);
				
				$drivers = explode(',', $key->drivers);
				$chk8 = 1;
				foreach ($drivers as $driver) {
					$prev_value8 = $sheet->getCell('J' . $rows)->getValue();
					if(count($drivers)==$chk8)
					{
						$sheet->setCellValue('J' . $rows, $prev_value8.$driver);
					} else {
						$sheet->setCellValue('J' . $rows, $prev_value8.$driver.PHP_EOL);
					}
					$chk8++;
				}
				$sheet->getStyle('J' . $rows)->getAlignment()->setWrapText(true);
				
				$totamt = explode(',', $key->totamt);
				$chk9 = 1;
				$total=0;
				$total=array_sum($totamt);
				foreach ($totamt as $totam) {
					$prev_value9 = $sheet->getCell('K' . $rows)->getValue();
					if(count($totamt)==$chk9)
					{
						$sheet->setCellValue('K' . $rows, ' '.$prev_value9.$totam);
					} else {
						$sheet->setCellValue('K' . $rows, ' '.$prev_value9.$totam.PHP_EOL);
					}
					$chk9++;
				}
				$sheet->getStyle('K' . $rows)->getAlignment()->setWrapText(true);
				
				$totamts = explode(',', $key->totamt);
				$totals=0;
				$totals=array_sum($totamts);
				$sheet->setCellValue('L' . $rows, $totals);
				
				$shiftss = explode(',', $key->new_time_from);
				$shiftss2 = explode(',', $key->new_time_to);
				$i = 0;
				$shours = 0;
				foreach ($shiftss as $shiftt) {
					$t_shrt = $shiftss2[$i];
					$cur_shrt = $shiftt;
					$hours = ((strtotime($t_shrt) - strtotime($cur_shrt)) / 3600);
					$shours += $hours;
					$i++;
				}
				$sheet->setCellValue('M' . $rows, $shours);	
				
				$rows++;
				// $i++;
			}
        }
		// echo '<pre>';
		// print_r($performance_report);
		// exit();
		$writer = new Xlsx($spreadsheet);
        $writer->save("upload/" . $fileName);
        header("Content-Type: application/vnd.ms-excel");
        echo base_url() . "upload/" . $fileName;
        exit();
        
    }
	
	function invoice_report_excel()
    {
		error_reporting(E_ALL);
ini_set('display_errors', '1');
        $filter_from_date = $this->input->post('filter_from_date') ?: date('01/m/Y');
        $filter_to_date = $this->input->post('filter_to_date') ?: date('t/m/Y');
        /************************************************************** */
        $invoices = $this->db->select('i.*,c.customer_name')
            ->from('invoice as i')
            ->join('customers as c','i.customer_id = c.customer_id','left')
            ->where('i.invoice_date >=', DateTime::createFromFormat('d/m/Y', $filter_from_date)->format('Y-m-d'))
            ->where('i.invoice_date <=', DateTime::createFromFormat('d/m/Y', $filter_to_date)->format('Y-m-d'))
            // ->where('i.invoice_status', 3)
			->where('i.invoice_status !=', 2)
			->where('i.invoice_status !=', 0)
            ->order_by('i.invoice_date', 'ASC')->get()->result();
        //echo'<pre>';print_r($data['invoices']);echo'<pre>';die();
        /************************************************************** */
        $fileName = 'Invoice Report '.$filter_from_date.' - '.$filter_to_date.'.xlsx';
		$styleArray = array(
            'font' => array(
                'bold' => true,
            )
        );
		$spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Invoice Date');
        $sheet->setCellValue('B1', 'Invoice No.');
        $sheet->setCellValue('C1', 'Customer Name');
        $sheet->setCellValue('D1', 'Amount (AED)');
        $sheet->setCellValue('E1', 'Status');
        $sheet->getStyle("A1:E1")->applyFromArray($styleArray);
        $sheet->getColumnDimension("A")->setAutoSize(true);
        $sheet->getColumnDimension("B")->setAutoSize(true);
        $sheet->getColumnDimension("C")->setAutoSize(true);
        $sheet->getColumnDimension("D")->setAutoSize(true);
        $sheet->getColumnDimension("E")->setAutoSize(true);
        $rows = 2;
        $i = 1;
        $formatCode = '0.00'; // Custom format for two decimal points
		
        foreach ($invoices as $invoice) {
            $rawDate = $invoice->invoice_date;
            $formattedDate = date('d-m-Y', strtotime($rawDate));
            $sheet->setCellValue('A' . $rows, $formattedDate);
            // Format the cell as a date with the desired format (dd-mm-yyyy)
            $sheet->getStyle('A' . $rows)->getNumberFormat()->setFormatCode('dd-mm-yyyy');
            // $sheet->setCellValue('A' . $rows, DateTime::createFromFormat('Y-m-d', $invoice['invoice_date'])->format('d-m-Y');
            $sheet->setCellValue('B' . $rows, $invoice->invoice_num);
            $sheet->setCellValue('C' . $rows, $invoice->customer_name);
            $sheet->setCellValue('D' . $rows, $invoice->invoice_net_amount);
			$sheet->getStyle('D' . $rows)->getNumberFormat()->setFormatCode($formatCode);
			$status = $invoice->invoice_status == 3 ? 'Paid' : 'Open';
            $sheet->setCellValue('E' . $rows, $status);
            $rows++;
            $i++;
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save("upload/" . $fileName);
        header("Content-Type: application/vnd.ms-excel");
        echo base_url() . "upload/" . $fileName;
        exit();
    }
}
