<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Driverappapi extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('driverappapimodel');
		$this->load->model('api_model');
        $this->load->model('tablets_model');
        $this->load->model('bookings_model');
        $this->load->model('areas_model');
        $this->load->model('maids_model');
        $this->load->model('customers_model');
        $this->load->model('service_types_model');


        $this->load->helper('google_api_helper');
    }

    public function index() {
        $response               = array();
        $response['status']     = 'error';
        $response['error_code'] = '101';
        $response['message']    = 'Invalid request';

        echo json_encode($response);
        exit();
    }
	
	public function getmaids() {
        if ($this->input->get('imei') && strlen(trim($this->input->get('imei'))) > 0) {
            $imei = trim($this->input->get('imei'));

            $tablet = $this->tablets_model->get_tablet_by_imei($imei);

            if (isset($tablet->tablet_id) && $tablet->tablet_status == 1) {
                $this->load->model('maids_model');

                $maids = $this->maids_model->get_maids();

                $response = array();
                $response['status'] = 'success';
                $response['maids'] = array();
                $i = 0;
                foreach ($maids as $maid) {
                    $response['maids'][$i]['id'] = $maid->maid_id;
                    $response['maids'][$i]['name'] = $maid->maid_name;
//					$response['maids'][$i]['photo'] = strlen(trim($maid->maid_photo_file)) > 0 ? base_url() . 'assets/maid/photos/75_75_' . $maid->maid_photo_file : '';
                    $response['maids'][$i]['photo'] = strlen(trim($maid->maid_photo_file)) > 0 ? base_url() . 'maidimg/' . $maid->maid_photo_file : '';
                    $i++;
                }

                echo json_encode($response);
                exit();
            } else if (isset($tablet->tablet_id) && $tablet->tablet_status == 0) {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '103';
                $response['message'] = 'Inactive tablet';

                echo json_encode($response);
                exit();
            } else {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '102';
                $response['message'] = 'Invalid IMEI';

                echo json_encode($response);
                exit();
            }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
    /*
     * Author : Jiby
     * Purpose : Driver app
     */
    public function registertablet() {
        if ($this->input->get('imei') && $this->input->get('regid') && strlen(trim($this->input->get('imei'))) > 0 && strlen(trim($this->input->get('regid'))) > 0) {
            $imei   = trim($this->input->get('imei'));
            $regid  = trim($this->input->get('regid'));

            $tablet = $this->tablets_model->get_tablet_by_imei($imei);

            if (isset($tablet->tablet_id) && $tablet->tablet_status == 1) {
                $update_fields = array();
                $update_fields['google_reg_id'] = $regid;

                $this->tablets_model->update_tablet($tablet->tablet_id, $update_fields);

                $response = array();
                $response['status'] = 'success';

                echo json_encode($response);
                exit();
            } else if (isset($tablet->tablet_id) && $tablet->tablet_status == 0) {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '103';
                $response['message']    = 'Inactive tablet';

                echo json_encode($response);
                exit();
            } else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '102';
                $response['message']    = 'Invalid IMEI';

                echo json_encode($response);
                exit();
            }
        } else {
            $response                   = array();
            $response['status']         = 'error';
            $response['error_code']     = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }


    /*
     * Author : Jiby
     * Purpose : Driver app
     */
    public function settings() {
        $this->load->model('zones_model');

        if ($this->input->get('imei') && strlen(trim($this->input->get('imei'))) > 0) {
            $imei = trim($this->input->get('imei'));

            $tablet = $this->tablets_model->get_tablet_by_imei($imei);

            if (isset($tablet->tablet_id) && $tablet->tablet_status == 1) {
                $my_zone_id = $tablet->zone_id;

                $zones = $this->zones_model->get_zones(TRUE, FALSE);

                $response = array();
                $response['status'] = 'success';
                $response['zones'] = $zones;
                $response['my_zone'] = $my_zone_id;
                $response['my_tablet'] = $tablet->tablet_id;

                echo json_encode($response);
                exit();
            } else if (isset($tablet->tablet_id) && $tablet->tablet_status == 0) {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '103';
                $response['message'] = 'Inactive tablet';

                echo json_encode($response);
                exit();
            } else {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '102';
                $response['message'] = 'Invalid IMEI';

                echo json_encode($response);
                exit();
            }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
/*
 * Author : Jiby
 * Purpose : Driver app
 */
    
/*
 * Author : Jiby
 * Purpose : Driver app
 */
    public function getschedule() {
        if ($this->input->get('imei') && $this->input->get('day') && strlen(trim($this->input->get('imei'))) > 0 && ($this->input->get('day') == 1 || $this->input->get('day') == 2)) {
            $imei = trim($this->input->get('imei'));

            $tablet = $this->tablets_model->get_tablet_by_imei($imei);



            if (isset($tablet->tablet_id) && $tablet->tablet_status == 1) {
                $this->load->model('bookings_model');
                $this->load->model('maids_model');
                $this->load->model('day_services_model');
                if ($this->input->get('day') == 1) {
                    $service_date = date('Y-m-d');
                } else {
                    $service_date = date('Y-m-d', strtotime(date('d-M-Y') . ' + 1 day'));
                }

                $bookings = $this->bookings_model->get_schedule_by_date_for_tab($service_date);
//echo 'SD<pre>';print_r($bookings);exit;
                $booking_transfers = $this->bookings_model->get_booking_transfers_by_date($service_date);
                $transferred_booking_zones = array();
                foreach ($booking_transfers as $b_transfer) {
                    $transferred_booking_zones[$b_transfer->booking_id] = $b_transfer->zone_id;
                }

//		echo 'test<pre>';print_r($bookings);exit;						
                $schedule = array();
                $i = 0;
                foreach ($bookings as $booking) {
                    if (($booking->zone_id == $tablet->zone_id && !isset($transferred_booking_zones[$booking->booking_id])) || (isset($transferred_booking_zones[$booking->booking_id]) && $transferred_booking_zones[$booking->booking_id] == $tablet->zone_id)) {
                        $payment_type = 'Daily Paying';
                        if ($booking->payment_type == 'D') {
                            $payment_type = 'Daily Paying';
                        } else if ($booking->payment_type == 'W') {
                            $payment_type = 'Weekly Paying';
                        } else if ($booking->payment_type == 'M') {
                            $payment_type = 'Monthly Paying';
                        }

                        $maid_attandence = $this->maids_model->get_maid_attandence_by_date($booking->maid_id, $service_date);
                        if (isset($maid_attandence->attandence_id)) {
                            $m_attandence = $maid_attandence->attandence_status;
                        } else {
                            $m_attandence = "0";
                        }

                        $day_service = $this->day_services_model->get_day_service_by_booking_id($service_date, $booking->booking_id);

                        if (isset($day_service->day_service_id)) {
                            $service_status = $day_service->service_status;
                            $totalamount = $day_service->total_fee;
                            //$discount = $booking->discount;
                            //$totamount = $totalamount - $discount;
                            //removed showing from booking total only this line $totamount = $totalamount;
                            if($booking->payment_mode == "Credit Card")
                            {
                                $totamount = 'CC';
                            } else {
                                $totamount = $booking->total_amount;
                            }
                        } else {
                            //calculating service fees
//                            if ($booking->cleaning_materials == 1) {
//                                $getdefault_fee = $this->bookings_model->get_default_fee();
//                                $materialfee = $getdefault_fee->cleaning_material_fee;
//                            } else {
//                                $materialfee = 0;
//                            }

//                            if ($booking->sofa_cleaning == 1) {
//                                $getdefault_fee = $this->bookings_model->get_default_fee();
//                                $sofafee = $getdefault_fee->sofa_cleaning;
//                            } else {
//                                $sofafee = 0;
//                            }

                            $normal_hours = 0;
                            $extra_hours = 0;
                            $weekend_hours = 0;

                            $normal_from = strtotime('06:00:00');
                            $normal_to = strtotime('21:00:00');

                            $shift_from = strtotime($booking->time_from . ':00');
                            $shift_to = strtotime($booking->time_to . ':00');

                            //if(date('w') > 0 && date('w') < 5)
                            if (date('w') != 5) {
                                if ($shift_from < $normal_from) {
                                    if ($shift_to <= $normal_from) {
                                        $extra_hours = ($shift_to - $shift_from) / 3600;
                                    }

                                    if ($shift_to > $normal_from && $shift_to <= $normal_to) {
                                        $extra_hours = ($normal_from - $shift_from) / 3600;
                                        $normal_hours = ($shift_to - $normal_from) / 3600;
                                    }

                                    if ($shift_to > $normal_to) {
                                        $extra_hours = ($normal_from - $shift_from) / 3600;
                                        $extra_hours += ($shift_to - $normal_to) / 3600;
                                        $normal_hours = ($normal_to - $normal_from) / 3600;
                                    }
                                }

                                if ($shift_from >= $normal_from && $shift_from < $normal_to) {
                                    if ($shift_to <= $normal_to) {
                                        $normal_hours = ($shift_to - $shift_from) / 3600;
                                    }

                                    if ($shift_to > $normal_to) {
                                        $normal_hours = ($normal_to - $shift_from) / 3600;
                                        $extra_hours = ($shift_to - $normal_to) / 3600;
                                    }
                                }

                                if ($shift_from > $normal_to) {
                                    $extra_hours = ($shift_to - $shift_from) / 3600;
                                }
                            } else {
                                $weekend_hours = ($shift_to - $shift_from) / 3600;
                            }

                            $service_description = array();

                            $service_description['normal'] = new stdClass();
                            $service_description['normal']->hours = $normal_hours;
                            $service_description['normal']->fees = $normal_hours * $booking->price_hourly;

                            $service_description['extra'] = new stdClass();
                            $service_description['extra']->hours = $extra_hours;
                            $service_description['extra']->fees = $extra_hours * $booking->price_extra;

                            $service_description['weekend'] = new stdClass();
                            $service_description['weekend']->hours = $weekend_hours;
                            $service_description['weekend']->fees = $weekend_hours * $booking->price_weekend;

                            /*
                             * Location charge
                             */
                            $location_charge = 0;

                            $customer_address = $this->customers_model->get_customer_address_by_id($booking->customer_address_id);
                            $cus_detail = $this->customers_model->get_customer_by_id($booking->customer_id);
                            if ($customer_address) {
                                $area = $this->areas_model->get_area_by_id($customer_address->area_id);
                                if ($area) {
                                    $location_charge = $area->area_charge;
                                }
                            }
                            //added by vishnu
                            //$total_fee = $service_description['normal']->fees + $service_description['extra']->fees + $service_description['weekend']->fees+ $location_charge - $booking->discount;
// for total amount hide                           if ($booking->sofa_cleaning == 1) {
//                                $total_fee = (($sofafee * $booking->no_of_seats) - $booking->discount);
//                            } else {
//                                $total_fee = $service_description['normal']->fees + $service_description['extra']->fees + $service_description['weekend']->fees + $location_charge + $materialfee - $booking->discount;
//                            }
                            //servicefee ends
                            $service_status = 0;
                            $totalamount = 0;
                            $discount = 0;
                            //hided show booking total $totamount = $total_fee;
                            if($booking->payment_mode == "Credit Card")
                            {
                                $totamount = 'CC';
                            } else {
                                $totamount = $booking->total_amount;
                            }
                        }
						
						if($booking->customer_address == "")
						{
							$a_address = $booking->building.', '.$booking->unit_no.''.$booking->street;
							$a_address = htmlspecialchars($a_address, ENT_QUOTES, 'UTF-8');
							$a_address = preg_replace('/[^A-Za-z0-9\-\(\) ]/', '', $a_address);
						} else {
							if($booking->building != "")
							{
								$t_address = "Apt No: ".$booking->building.", ".$booking->customer_address;
							} else {
								$t_address = $booking->customer_address;
							}
							$a_address = htmlspecialchars($t_address, ENT_QUOTES, 'UTF-8');
							$a_address = preg_replace('/[^A-Za-z0-9\-\(\) ]/', '', $a_address);
						}
						
						$check_buk_exist = $this->bookings_model->get_booking_exist($booking->booking_id,$service_date);
						if(!empty($check_buk_exist))
						{
							$mop = $check_buk_exist->mop;
							$ref = $check_buk_exist->just_mop_ref;
						} else {
							$mop = $booking->payment_mode;
							$ref = "";
						}
						
						if($mop == "Credit Card")
						{
							$totamount = 'CC';
						} else {
							$totamount = $booking->total_amount;
						}
						
						if($booking->driver != "")
						{
							$driver = " (".$booking->driver.")";
						} else {
							$driver = "";
						}

                        $schedule[$i]['booking_id']     = $booking->booking_id;
                        $schedule[$i]['service_status'] = $service_status; // 0 - Not Started , 1 - Started,  2 - Finished, 3 - Not Done
                        $schedule[$i]['customer_id']    = $booking->customer_id;
                        $schedule[$i]['customer_code']  = $this->config->item('customer_code_prepend') . $booking->customer_id;
                        $schedule[$i]['customer_name']  = htmlspecialchars($booking->customer_name, ENT_QUOTES, 'UTF-8');
                        $schedule[$i]['customer_address']   = $a_address;
                        $schedule[$i]['customer_longitude'] = $booking->longitude;
                        $schedule[$i]['customer_latitude']  = $booking->latitude;
						$schedule[$i]['customer_mobile'] = $booking->customer_source;
                        // if ($booking->mobile_number_2 == '') {
                            // $schedule[$i]['customer_mobile'] = $booking->mobile_number_1;
                        // } else {
                            // $schedule[$i]['customer_mobile'] = $booking->mobile_number_1 . ',' . $booking->mobile_number_2;
                        // }

                        $schedule[$i]['customer_type']  = $payment_type;
                        $schedule[$i]['maid_id']        = $booking->maid_id;
                        $schedule[$i]['maid_name']      = $booking->maid_name;
                        $schedule[$i]['maid_country']   = $booking->maid_nationality;
                        $schedule[$i]['maid_attandence']= $m_attandence; // 0 - Not Present, 1 - Maid In Vehicle, 2 - Maid Out of Vehicle
                        $schedule[$i]['shift_start']    = date('H:i', strtotime($booking->time_from));
                        $schedule[$i]['shift_end']      = date('H:i', strtotime($booking->time_to));
                        $schedule[$i]['key_status']     = $booking->key_given == 'Y' ? "1" : "0";
                        $schedule[$i]['area']           = $booking->area_name.$driver;
						$noteneww = htmlspecialchars($booking->booking_note, ENT_QUOTES, 'UTF-8');
                        $schedule[$i]['booking_note']   = preg_replace('/[^A-Za-z0-9\-\(\) ]/', '', $noteneww);
                        $schedule[$i]['service_fee']    = $totamount;
						$schedule[$i]['new_ref']    = $ref;
						$schedule[$i]['mop']    = $mop;
                        if ($booking->cleaning_material == "Y") {
                            $schedule[$i]['cleaning_material'] = "yes";
                        } else {
                            $schedule[$i]['cleaning_material'] = "no";
                        }
                        $i++;
                    }
                }

                echo json_encode(array('status' => 'success', 'schedule' => $schedule));
                exit();
            } else if (isset($tablet->tablet_id) && $tablet->tablet_status == 0) {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '103';
                $response['message'] = 'Inactive tablet';

                echo json_encode($response);
                exit();
            } else {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '102';
                $response['message'] = 'Invalid IMEI';

                echo json_encode($response);
                exit();
            }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
	
	public function getschedulesbymaid() 
	{
		if ($this->input->get('maid_id') && $this->input->get('booking_id') && $this->input->get('day') && strlen(trim($this->input->get('maid_id'))) > 0 && strlen(trim($this->input->get('booking_id'))) > 0 && ($this->input->get('day') == 1 || $this->input->get('day') == 2)) 
		{
            $maid_id = trim($this->input->get('maid_id'));
            $booking_id = trim($this->input->get('booking_id'));
			if ($this->input->get('day') == 1) 
			{
				$service_date = date('Y-m-d');
			} else {
				$service_date = date('Y-m-d', strtotime(date('d-M-Y') . ' + 1 day'));
			}
			
			$bookings = $this->driverappapimodel->get_schedule_by_date_for_tab_by_maid_id($service_date,$maid_id);
			$schedule = array();
			$i = 0;
			foreach ($bookings as $booking) 
			{
				if($booking->customer_address == "")
				{
					$a_address = $booking->building.', '.$booking->unit_no.''.$booking->street;
				} else {
					if($booking->building != "")
					{
						$t_address = "Apt No: ".$booking->building.", ".$booking->customer_address;
					} else {
						$t_address = $booking->customer_address;
					}
					$a_address = htmlspecialchars($t_address, ENT_QUOTES, 'UTF-8');
				}
				
				// $booking_transfers = $this->driverappapimodel->get_booking_transfers_by_date_booking($service_date,$booking->booking_id);
				
				// if(!empty($booking_transfers))
				// {
					// if($booking_transfers->zone_name != "")
					// {
						// $zonename = $booking_transfers->zone_name;
					// } else {
						// $zonename = $booking->zone_name;
					// }
				// } else {
					// $zonename = $booking->zone_name;
				// }
				$zonename = $booking->zone_name;
				
				if($booking->driver_name != "")
				{
					$driver = " (".$booking->driver_name.")";
				} else {
					$driver = "";
				}
				
				if($booking->booking_id != $booking_id)
				{
					$day_service = $this->driverappapimodel->get_day_service_by_booking_id_new($service_date, $booking->booking_id);
					
					if(!empty($day_service))
					{
						$pick_zone = $day_service->zone_name.' ('.$day_service->driver_name.')';
					} else {
						$pick_zone = "";
					}
					$schedule[$i]['booking_id']     = $booking->booking_id;
					$schedule[$i]['customer_address']   = $a_address;
					$schedule[$i]['shift']    = date('H:i', strtotime($booking->time_from)) .' - '. date('H:i', strtotime($booking->time_to));
					$schedule[$i]['zone']           = $zonename;
					// $schedule[$i]['pick_zone']           = $pick_zone;
					$schedule[$i]['pick_zone']           = $zonename;
					$i++;
				}
			}
			
			echo json_encode(array('status' => 'success', 'schedule' => $schedule));
			exit();
			
		} else {
			echo json_encode(array('status' => 'success', 'schedule' => array()));
			exit();
		}
    }
/*
 * Author : Jiby
 * Purpose : Driver app
 */
    public function maidattendance() {
        if ($this->input->get('imei') && $this->input->get('maid_id') && $this->input->get('status') && is_numeric($this->input->get('maid_id')) && $this->input->get('maid_id') > 0 && is_numeric($this->input->get('status'))) {
            $imei = trim($this->input->get('imei'));
            $maid_id = trim($this->input->get('maid_id'));
            $status = trim($this->input->get('status'));

            $tablet = $this->tablets_model->get_tablet_by_imei($imei);

            if (isset($tablet->tablet_id) && $tablet->tablet_status == 1) {
                $this->load->model('maids_model');

                switch ($status) {
                    case 1: // maid-in
                        $chk_attandence = $this->maids_model->get_maid_attandence_by_date($maid_id, date('Y-m-d'));

                        if (!isset($chk_attandence->attandence_id) || $chk_attandence->attandence_status != 1) {
                            $maid_in_fields = array();
                            $maid_in_fields['maid_id']      = $maid_id;
                            $maid_in_fields['zone_id']      = $tablet->zone_id;
                            $maid_in_fields['tablet_id']    = $tablet->tablet_id;
                            $maid_in_fields['date']         = date('Y-m-d');
                            $maid_in_fields['maid_in_time'] = date('H:i:s');
                            $maid_in_fields['attandence_status'] = 1;

                            $maid_in = $this->maids_model->add_maid_attandence($maid_in_fields);

                            if ($maid_in) {
                                $response = array();
                                $response['status'] = 'success';
                                $response['maid_id'] = $maid_id;
                                $response['attandence_status'] = 1;
                            } else {
                                $response = array();
                                $response['status'] = 'error';
                                $response['error_code'] = '107';
                                $response['message'] = 'Unexpected error';
                            }
                        } else {
                            $response = array();
                            $response['status'] = 'error';
                            $response['error_code'] = '105';
                            $response['message'] = 'Maid already in';
                        }

                        break;

                    case 2: // maid-out
                        $chk_attandence = $this->maids_model->get_maid_attandence_by_date($maid_id, date('Y-m-d'));

                        if (isset($chk_attandence->attandence_id) && $chk_attandence->attandence_status == 1 && $chk_attandence->tablet_id == $tablet->tablet_id) {
                            $maid_out_fields = array();
                            $maid_out_fields['maid_out_time'] = date('H:i:s');
                            $maid_out_fields['attandence_status'] = 2;

                            $maid_out = $this->maids_model->update_maid_attandence($chk_attandence->attandence_id, $maid_out_fields);
                            if ($maid_out) {
                                $response = array();
                                $response['status']     = 'success';
                                $response['maid_id']    = $maid_id;
                                $response['attandence_status'] = 2;
                            } else {
                                $response = array();
                                $response['status'] = 'error';
                                $response['error_code'] = '107';
                                $response['message'] = 'Unexpected error';
                            }
                        } else {
                            $response = array();
                            $response['status'] = 'error';
                            $response['error_code'] = '105';
                            $response['message'] = 'Maid not in';
                        }

                        break;

                    default:
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '104';
                        $response['message'] = 'Invalid status';
                }

                echo json_encode($response);
                exit();
            } else if (isset($tablet->tablet_id) && $tablet->tablet_status == 0) {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '103';
                $response['message'] = 'Inactive tablet';

                echo json_encode($response);
                exit();
            } else {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '102';
                $response['message'] = 'Invalid IMEI';

                echo json_encode($response);
                exit();
            }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
/*
 * Author : Jiby
 * Purpose : Driver app
 */
    public function updateservicestatus() {
        if ($this->input->get('imei') && $this->input->get('booking_id') && $this->input->get('status') && is_numeric($this->input->get('booking_id')) && $this->input->get('booking_id') > 0 && is_numeric($this->input->get('status'))) {
            $imei = trim($this->input->get('imei'));
            $booking_id = trim($this->input->get('booking_id'));
            $status = trim($this->input->get('status'));
            $service_date = date('Y-m-d');

            $tablet = $this->tablets_model->get_tablet_by_imei($imei);

            if (isset($tablet->tablet_id) && $tablet->tablet_status == 1) {
                $this->load->model('bookings_model');
                $this->load->model('customers_model');
                $this->load->model('day_services_model');

                $chk_day_service = $this->day_services_model->get_day_service_by_booking_id($service_date, $booking_id);

                switch ($status) {
                    case 1: // service started
                        if (isset($chk_day_service->day_service_id)) {
                            $response = array();
                            $response['status'] = 'error';
                            $response['error_code'] = '106';
                            $response['message'] = $chk_day_service->service_status == 1 ? 'Service already started' : 'Service already finished';
                            echo json_encode($response);
                            exit();
                        } else {
                            $booking = $this->bookings_model->get_booking_by_id($booking_id);
                            if ($booking->cleaning_material == "Y") {
                                //$getdefault_fee = $this->bookings_model->get_default_fee();
                                //$materialfee = $getdefault_fee->cleaning_material_fee;
								$materialfee =0;
                            } else {
                                $materialfee = 0;
                            }

                            // if ($booking->sofa_cleaning == 1) {
                                // $getdefault_fee = $this->bookings_model->get_default_fee();
                                // $sofafee = $getdefault_fee->sofa_cleaning;
                            // } else {
                                // $sofafee = 0;
                            // }
							$sofafee = 0;

                            $normal_hours = 0;
                            $extra_hours = 0;
                            $weekend_hours = 0;

                            $normal_from = strtotime('06:00:00');
                            $normal_to = strtotime('21:00:00');

                            $shift_from = strtotime($booking->time_from . ':00');
                            $shift_to = strtotime($booking->time_to . ':00');

                            //if(date('w') > 0 && date('w') < 5)
                            if (date('w') != 5) {
                                if ($shift_from < $normal_from) {
                                    if ($shift_to <= $normal_from) {
                                        $extra_hours = ($shift_to - $shift_from) / 3600;
                                    }

                                    if ($shift_to > $normal_from && $shift_to <= $normal_to) {
                                        $extra_hours = ($normal_from - $shift_from) / 3600;
                                        $normal_hours = ($shift_to - $normal_from) / 3600;
                                    }

                                    if ($shift_to > $normal_to) {
                                        $extra_hours = ($normal_from - $shift_from) / 3600;
                                        $extra_hours += ($shift_to - $normal_to) / 3600;
                                        $normal_hours = ($normal_to - $normal_from) / 3600;
                                    }
                                }

                                if ($shift_from >= $normal_from && $shift_from < $normal_to) {
                                    if ($shift_to <= $normal_to) {
                                        $normal_hours = ($shift_to - $shift_from) / 3600;
                                    }

                                    if ($shift_to > $normal_to) {
                                        $normal_hours = ($normal_to - $shift_from) / 3600;
                                        $extra_hours = ($shift_to - $normal_to) / 3600;
                                    }
                                }

                                if ($shift_from > $normal_to) {
                                    $extra_hours = ($shift_to - $shift_from) / 3600;
                                }
                            } else {
                                $weekend_hours = ($shift_to - $shift_from) / 3600;
                            }

                            $service_description = array();

                            $service_description['normal'] = new stdClass();
                            $service_description['normal']->hours = $normal_hours;
                            $service_description['normal']->fees = $normal_hours * $booking->price_hourly;

                            $service_description['extra'] = new stdClass();
                            $service_description['extra']->hours = $extra_hours;
                            $service_description['extra']->fees = $extra_hours * $booking->price_extra;

                            $service_description['weekend'] = new stdClass();
                            $service_description['weekend']->hours = $weekend_hours;
                            $service_description['weekend']->fees = $weekend_hours * $booking->price_weekend;

                            /*
                             * Location charge
                             */
                            $location_charge = 0;

                            $customer_address = $this->customers_model->get_customer_address_by_id($booking->customer_address_id);
                            $cus_detail = $this->customers_model->get_customer_by_id($booking->customer_id);
                            if ($customer_address) {
                                $area = $this->areas_model->get_area_by_id($customer_address->area_id);
                                if ($area) {
                                    $location_charge = $area->area_charge;
                                }
                            }
                            //added by vishnu
                            //$total_fee = $service_description['normal']->fees + $service_description['extra']->fees + $service_description['weekend']->fees+ $location_charge - $booking->discount;
                            // if ($booking->sofa_cleaning == 1) {
                                // $total_fee = (($sofafee * $booking->no_of_seats) - $booking->discount);
                            // } else {
                                // $total_fee = $service_description['normal']->fees + $service_description['extra']->fees + $service_description['weekend']->fees + $location_charge + $materialfee - $booking->discount;
                            // }
							$bal = "";
							$sign = "";
                            if ($cus_detail->signed == "Cr") {
                                $bal = ($cus_detail->balance - $total_fee);
                                $a = substr($bal, 0, 1);
                                if ($a == '-') {
                                    $sign = "Dr";
                                    $bal = abs($bal);
                                } else if ($a == '0') {
                                    $sign = "Dr";
                                } else {
                                    $sign = "Cr";
                                }
                            } else if ($cus_detail->signed == "Dr") {
                                $bal = ($cus_detail->balance + $total_fee);
                                $a = substr($bal, 0, 1);
                                if ($a == '-') {
                                    $sign = "Cr";
                                    $bal = abs($bal);
                                } else if ($a == '0') {
                                    $sign = "Dr";
                                } else {
                                    $sign = "Dr";
                                }
                            }
                            $bal_fields = array();
                            $bal_fields['balance'] = $bal;
                            $bal_fields['signed'] = $sign;
//                                                        $bal_update = $this->customers_model->update_customer($booking->customer_id,$bal_fields);
                            $bal_update = $this->customers_model->update_customers($bal_fields, $booking->customer_id);
                            
							if($booking->customer_address == "")
							{
								$booking->customer_address = 'Building - '.$booking->building.', '.$booking->unit_no.''.$booking->street;
							} else {
								if($booking->building != "")
								{
									$addressss = "Apt No: ".$booking->building.", ".$booking->customer_address;
								} else {
									$addressss = $booking->customer_address;
								}
								$booking->customer_address = $addressss;
							}
							
							
							$ds_fields = array();
                            $ds_fields['booking_id'] = $booking_id;
                            $ds_fields['customer_id'] = $booking->customer_id;
                            $ds_fields['customer_name'] = $booking->customer_name;
                            $ds_fields['customer_address'] = htmlspecialchars($booking->customer_address, ENT_QUOTES, 'UTF-8');
                            $ds_fields['customer_payment_type'] = $booking->payment_type;
                            $ds_fields['maid_id'] = $booking->maid_id;
                            $ds_fields['service_description'] = serialize($service_description);
                            //$ds_fields['total_fee'] = $total_fee;
                            $ds_fields['total_fee'] = $booking->total_amount;
                            $ds_fields['service_date'] = $service_date;
                            $ds_fields['start_time'] = date('H:i:s');
                            $ds_fields['service_status'] = 1;
                            $ds_fields['service_added_by'] = 'T';
                            $ds_fields['service_added_by_id'] = $tablet->tablet_id;

                            $day_service = $this->day_services_model->add_day_service($ds_fields);

                            if ($day_service) {
								//maid transit
								$update_driver = array();
								$update_driver['driver_name'] = $tablet->driver_name;
								$update_driver_details = $this->maids_model->update_maids($update_driver, $booking->maid_id);
								//ends
								
								
                                $response = array();
                                $response['status'] = 'success';
                                $response['booking_id'] = $booking_id;
                                $response['service_status'] = 1;

                                echo json_encode($response);
                                exit();
                            } else {
                                $response = array();
                                $response['status'] = 'error';
                                $response['error_code'] = '105';
                                $response['message'] = 'Unexpected error';

                                echo json_encode($response);
                                exit();
                            }
                        }
//            echo 'TET<pre>';print_r($booking);exit;
                        break;

                    case 2: // service finished						
                        if ($this->input->get('payment') !== FALSE && ($this->input->get('payment') == 0 || $this->input->get('payment') == 1)) {
                            $payment_status = $this->input->get('payment');
                        } else {
                            $zone_id = trim($this->input->get('zone_id'));
                            $response = array();
                            $response['status'] = 'error';
                            $response['error_code'] = '101';
                            $response['message'] = 'Invalid request. Invalid payment status.';

                            echo json_encode($response);
                            exit();
                        }

                        if (isset($chk_day_service->day_service_id) && isset($payment_status)) {
                            if ($chk_day_service->service_status == 1) {
                                if ($payment_status == 1) {
                                    if ($this->input->get('amount') && is_numeric($this->input->get('amount')) && $this->input->get('amount') > 0 || $this->input->get('amount') == 0) {
                                        $amount = $this->input->get('amount');
                                        if ($this->input->get('outstanding_amount') && is_numeric($this->input->get('outstanding_amount')) && $this->input->get('outstanding_amount') > 0) {
                                            $outstanding_amount = $this->input->get('outstanding_amount');
                                        } else {
                                            $outstanding_amount = 0;
                                        }

                                        if ($this->input->get('receipt_no') && is_numeric($this->input->get('receipt_no')) && $this->input->get('receipt_no') > 0) {
                                            $receipt_no = $this->input->get('receipt_no');
                                        } else {
                                            $receipt_no = "";
                                        }

                               

                                        $payment_method = 0;
                                        if ($this->input->get('method') && is_numeric($this->input->get('method')) && $this->input->get('method') > 0) {
                                            $payment_method = 1;
                                        }

                                        $totlamt = ($amount + $outstanding_amount);
                                        $cus_detail = $this->customers_model->get_customer_by_id($chk_day_service->customer_id);
                                        $bal = "";
										$sign = "";
										if ($cus_detail->signed == "Cr") {
                                            $bal = ($cus_detail->balance + $totlamt);
                                            $a = substr($bal, 0, 1);
                                            if ($a == '-') {
                                                $sign = "Dr";
                                                $bal = abs($bal);
                                            } else if ($a == '0') {
                                                $sign = "Dr";
                                            } else {
                                                $sign = "Cr";
                                            }
                                        } else if ($cus_detail->signed == "Dr") {
                                            $bal = ($cus_detail->balance - $totlamt);
                                            $a = substr($bal, 0, 1);
                                            if ($a == '-') {
                                                $sign = "Cr";
                                                $bal = abs($bal);
                                            } else if ($a == '0') {
                                                $sign = "Dr";
                                            } else {
                                                $sign = "Dr";
                                            }
                                        }
                                        $bal_fields = array();
                                        $bal_fields['balance'] = $bal;
                                        $bal_fields['signed'] = $sign;
//                                                                                $bal_update = $this->customers_model->update_customer($chk_day_service->customer_id,$bal_fields);
                                        $bal_update = $this->customers_model->update_customers($bal_fields, $chk_day_service->customer_id);

                                        //exit();

                                        $payment_fields = array();
                                        $payment_fields['customer_id'] = $chk_day_service->customer_id;
                                        $payment_fields['day_service_id'] = $chk_day_service->day_service_id; //offline
                                        $payment_fields['paid_amount'] = $amount;
                                        $payment_fields['outstanding_amt'] = $outstanding_amount;
                                        $payment_fields['receipt_no'] = $receipt_no;
                                        $payment_fields['paid_at'] = 'T';
                                        $payment_fields['paid_at_id'] = $tablet->tablet_id;
                                        $payment_fields['payment_method'] = $payment_method;
                                        $payment_fields['paid_datetime'] = date('Y-m-d H:i:s');

                                        $add_payment = $this->customers_model->add_customer_payment($payment_fields);

                                        //sms
//                                        $booking = $this->bookings_model->get_booking_by_id($booking_id);
////                                                    echo 'Testing hereee EEE<pre>';print_r($booking);exit;
//                                        $customermobile = html_escape($booking->mobile_number_1);
//                                        $mobile = substr(trim(preg_replace('/[^0-9]*/', '', $customermobile)), -8);
//                                        $msg = "";
//                                        $msg .= "Dear Customer,";
//                                        $msg .= "\r\n";
//                                        $msg .= "Thank you for the payment of AED" . $totlamt . " towards the cleaning service done on " . $service_date;
//                                        $msg .= "\r\n";
//                                        $msg .= 'Book online:www.mymaids.com';
//
////                                                                                $sms_url = 'http://messaging.qmobile.me/httpapi/?uid=allam&pwd=allambulk&src=ALALLAM&dst=974'.urlencode($mobile).'&msg='.urlencode($msg);
//
//                                        $ch = curl_init();
//                                        curl_setopt($ch, CURLOPT_URL, $sms_url);
//                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//                                        $res = curl_exec($ch);
//                                        curl_close($ch);
                                        //sms ends
                                    } else {
                                        $response = array();
                                        $response['status'] = 'error';
                                        $response['error_code'] = '101';
                                        $response['message'] = 'Invalid request. Invalid amount.';

                                        echo json_encode($response);
                                        exit();
                                    }
                                } else if ($chk_day_service->payment_type == 'D') {
                                    // Daily paying custmer, not paid. Send notification to manager
                                    $this->push_notification(3);
                                }

                                if ($payment_status == 0) {
                                    //sms
//                                    $booking = $this->bookings_model->get_booking_by_id($booking_id);
//                                    $customermobile = html_escape($booking->mobile_number_1);
//                                    $mobile = substr(trim(preg_replace('/[^0-9]*/', '', $customermobile)), -8);
//                                    $msg = "";
//                                    $msg .= "Dear Customer,";
//                                    $msg .= "\r\n";
//                                    $msg .= "You have an outstanding payment of QR" . $chk_day_service->total_fee . " for the cleaning service done on " . $service_date . ".If you pay in advance please ignore this message";
//                                    $msg .= "\r\n";
//                                    $msg .= 'Book online:www.mymaid.com';
//
////                                                                    $sms_url = 'http://messaging.qmobile.me/httpapi/?uid=allam&pwd=allambulk&src=ALALLAM&dst=974'.urlencode($mobile).'&msg='.urlencode($msg);
//
//                                    $ch = curl_init();
//                                    curl_setopt($ch, CURLOPT_URL, $sms_url);
//                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//                                    $res = curl_exec($ch);
//                                    curl_close($ch);
                                    //sms ends
                                }
								
								//maid transit
								$update_driver = array();
								$update_driver['driver_name'] = $tablet->driver_name;
								$update_driver_details = $this->maids_model->update_maids($update_driver, $booking->maid_id);
								//ends
								
								
								$ds_fields = array();
                                $ds_fields['end_time'] = date('H:i:s');
                                $ds_fields['service_status'] = 2;
                                $ds_fields['payment_status'] = isset($add_payment) && $add_payment ? 1 : 0;

                                $this->day_services_model->update_day_service($chk_day_service->day_service_id, $ds_fields);
								if($chk_day_service->is_company == 'N' && $chk_day_service->rating_mail == 'Y')
								{
									$this->send_service_status_email($chk_day_service->booking_id,$chk_day_service->day_service_id,$service_date);
                                }
								$response = array();
                                $response['status'] = 'success';
                                $response['booking_id'] = $booking_id;
                                $response['service_status'] = 2;

                                echo json_encode($response);
                                exit();
                            } else {
                                $response = array();
                                $response['status'] = 'error';
                                $response['error_code'] = '106';
                                $response['message'] = 'Service already finished';

                                echo json_encode($response);
                                exit();
                            }
                        } else {
                            $response = array();
                            $response['status'] = 'error';
                            $response['error_code'] = '106';
                            $response['message'] = 'Service not started';

                            echo json_encode($response);
                            exit();
                        }
                        break;

                    case 3: // service not done
                        if (isset($chk_day_service->day_service_id)) {
                            if ($chk_day_service->service_status == 1) {
                                $ds_fields = array();
                                $ds_fields['end_time'] = date('H:i:s');
                                $ds_fields['service_status'] = 3;

                                $this->day_services_model->update_day_service($chk_day_service->day_service_id, $ds_fields);
                                $this->push_notification(2);
								
								//maid transit
								$update_driver = array();
								$update_driver['driver_name'] = $tablet->driver_name;
								$update_driver_details = $this->maids_model->update_maids($update_driver, $booking->maid_id);
								//ends
								
                                $response = array();
                                $response['status'] = 'success';
                                $response['booking_id'] = $booking_id;
                                $response['service_status'] = 3;

                                echo json_encode($response);
                                exit();
                            } else {
                                $response = array();
                                $response['status'] = 'error';
                                $response['error_code'] = '106';
                                $response['message'] = 'Service already finished';

                                echo json_encode($response);
                                exit();
                            }
                        } else {
                            $response = array();
                            $response['status'] = 'error';
                            $response['error_code'] = '106';
                            $response['message'] = 'Service not started';

                            echo json_encode($response);
                            exit();
                        }
                        break;

                    default:
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '104';
                        $response['message'] = 'Invalid status';
                        break;
                }

                if (isset($response['status']) && $response['status'] = 'success') {
                    if ($this->input('lat') && $this->input('long') && strlen(trim($this->input('lat'))) && strlen(trim($this->input('long')))) {
                        $booking = $this->bookings_model->get_booking_by_id($booking_id);

                        $update_customer_fields = array();
                        $update_customer_fields['latitude'] = trim($this->input('lat'));
                        $update_customer_fields['longitude'] = trim($this->input('long'));

                        $this->customers_model->update_customer($booking->customer_id, $update_customer_fields);
                    }
                }

                echo json_encode($response);
                exit();
            } else if (isset($tablet->tablet_id) && $tablet->tablet_status == 0) {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '103';
                $response['message'] = 'Inactive tablet';

                echo json_encode($response);
                exit();
            } else {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '102';
                $response['message'] = 'Invalid IMEI';

                echo json_encode($response);
                exit();
            }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
/*
 * Author : Jiby
 * Purpose : Driver app
 */
    public function transfermaid() {

        $this->load->model('zones_model');
        $this->load->model('bookings_model');
        $this->load->model('maids_model');

        if ($this->input->get('imei') && strlen(trim($this->input->get('imei'))) > 0 && $this->input->get('maid_id') && is_numeric($this->input->get('maid_id')) && $this->input->get('maid_id') > 0 && $this->input->get('zone_id') && is_numeric($this->input->get('zone_id')) && $this->input->get('zone_id') > 0 && $this->input->get('day') && ($this->input->get('day') == 1 || $this->input->get('day') == 2)) {
            $imei = trim($this->input->get('imei'));
            $maid_id = trim($this->input->get('maid_id'));
            $zone_id = trim($this->input->get('zone_id'));

            if ($this->input->get('day') == 1) {
                $service_date = date('Y-m-d');
            } else {
                $service_date = date('Y-m-d', strtotime(date('d-M-Y') . ' + 1 day'));
            }

            $tablet = $this->tablets_model->get_tablet_by_imei($imei);

            if (isset($tablet->tablet_id) && $tablet->tablet_status == 1) {
                $zone = $this->zones_model->get_zone_by_id($zone_id);

                if (isset($zone->zone_id) && $zone->zone_status == 1 && $zone->zone_id != $tablet->zone_id) {
                    $booking_transfers = $this->bookings_model->get_booking_transfers_by_date($service_date);
                    $yes_transferred_booking_zones = array();
                    $no_transferred_booking_zones = array();
                    foreach ($booking_transfers as $b_transfer) {
                        if ($b_transfer->zone_id == $tablet->zone_id) {
                            $chk_booking = $this->bookings_model->get_booking_by_id($b_transfer->booking_id);

                            if (isset($chk_booking->maid_id) && $chk_booking->maid_id == $maid_id) {
                                $yes_transferred_booking_zones[$b_transfer->booking_transfer_id] = $b_transfer->booking_id;
                            }
                        } else {
                            $no_transferred_booking_zones[$b_transfer->booking_id] = $b_transfer->zone_id;
                        }
                    }

                    $bookings = $this->bookings_model->get_maid_bookings_on_zone_by_date($maid_id, $tablet->zone_id, $service_date);

                    $transferred_bookings = array();
                    foreach ($bookings as $booking) {
                        if (!isset($no_transferred_booking_zones[$booking->booking_id])) {
                            $b_transfer_fields = array();
                            $b_transfer_fields['booking_id'] = $booking->booking_id;
                            $b_transfer_fields['zone_id'] = $zone->zone_id;
                            $b_transfer_fields['service_date'] = $service_date;
                            $b_transfer_fields['transferred_time'] = date('H:i:s');
                            $b_transfer_fields['transferred_by'] = 'T';
                            $b_transfer_fields['transferred_by_id'] = $tablet->tablet_id;

                            $this->bookings_model->add_booking_transfer($b_transfer_fields);

                            //maid transit
							$update_driver = array();
							$update_driver['driver_name'] = $zone->driver_name;
							$update_driver_details = $this->maids_model->update_maids($update_driver, $booking->maid_id);
							//ends
							
							$transferred_bookings[] = $booking->booking_id;
                            $tzone_tablet = $this->tablets_model->get_tablet_by_zone($zone->zone_id);

                            if (isset($tzone_tablet->google_reg_id)) {
                   
                                $booking_detail = $this->bookings_model->get_booking_by_id($booking->booking_id);
                                $msg = 'Maid Transfered - ' . $booking_detail->maid_name . ', ' . $booking_detail->time_from . ' - ' . $booking_detail->time_to . ', ' . $booking_detail->area_name;
                                

                                $title = "Maid Transfered";
                                $payload = array();
                                $payload['isfeedback'] = false;

                                $res = array();
                                $res['title'] = $title;
                                $res['is_background'] = false;
                                $res['body'] = $msg;
                                $res['image'] = "";
                                $res['payload'] = $payload;
                                $res['timestamp'] = date('Y-m-d G:i:s');
                                $regId =$tzone_tablet->google_reg_id;

                        $fields = array(
                                        'to' => $regId,
                                        'notification' => $res,
                                        'data' => $res,
                                        );

                                android_customer_app_push($fields);
                            }
                        }
                    }

                    foreach ($yes_transferred_booking_zones as $booking_transfer_id => $booking_id) {
                        $b_transfer_fields = array();
                        $b_transfer_fields['booking_id'] = $booking_id;
                        $b_transfer_fields['zone_id'] = $zone->zone_id;
                        $b_transfer_fields['service_date'] = $service_date;
                        $b_transfer_fields['transferred_time'] = date('H:i:s');
                        $b_transfer_fields['transferred_by'] = 'T';
                        $b_transfer_fields['transferred_by_id'] = $tablet->tablet_id;

                        $this->bookings_model->add_booking_transfer($b_transfer_fields);
						
						$chk_booking_new = $this->bookings_model->get_booking_by_id($booking_id);
						//maid transit
						$update_driver = array();
						$update_driver['driver_name'] = $zone->driver_name;
						$update_driver_details = $this->maids_model->update_maids($update_driver, $chk_booking_new->maid_id);
						//ends
                        $this->bookings_model->delete_booking_transfer($booking_transfer_id);

                        $transferred_bookings[] = $booking_id;
                        $tzone_tablet = $this->tablets_model->get_tablet_by_zone($zone->zone_id);

                        if (isset($tzone_tablet->google_reg_id)) {
                            $booking_detail = $this->bookings_model->get_booking_by_id($booking_id);
                            $msg = 'Maid Transfered - ' . $booking_detail->maid_name . ', ' . $booking_detail->time_from . ' - ' . $booking_detail->time_to . ', ' . $booking_detail->area_name;

                                $title = "Maid Transfered";
                                $payload = array();
                                $payload['isfeedback'] = false;

                                $res = array();
                                $res['title'] = $title;
                                $res['is_background'] = false;
                                $res['body'] = $msg;
                                $res['image'] = "";
                                $res['payload'] = $payload;
                                $res['timestamp'] = date('Y-m-d G:i:s');
                                $regId =$tzone_tablet->google_reg_id;

                        $fields = array(
                                        'to' => $regId,
                                        'notification' => $res,
                                        'data' => $res,
                                        );

                                android_customer_app_push($fields);
                        }
                    }

                    /* Check if maid already in */
                    $chk_attandence = $this->maids_model->get_maid_attandence_by_date($maid_id, $service_date);
                    if (isset($chk_attandence->attandence_id) && $chk_attandence->attandence_status == 1) {
                        $maid_out_fields = array();
                        $maid_out_fields['maid_out_time'] = date('H:i:s');
                        $maid_out_fields['attandence_status'] = 2;

                        $maid_out = $this->maids_model->update_maid_attandence($chk_attandence->attandence_id, $maid_out_fields);
                    }

                    if (count($transferred_bookings) > 0) {
                        $response = array();
                        $response['status'] = 'success';
                        $response['maid_id'] = $maid_id;
                        $response['service_date'] = $service_date;
                        $response['bookings'] = $transferred_bookings;
                    } else {
                        $response = array();
                        $response['status'] = 'success';
                        $response['maid_id'] = $maid_id;
                        $response['service_date'] = $service_date;
                        $response['bookings'] = $transferred_bookings;
                    }



                    echo json_encode($response);
                    exit();
                } else if (isset($zone->zone_id) && $zone->zone_status == 0 && $zone->zone_id != $tablet->zone_id) {
                    $response = array();
                    $response['status'] = 'error';
                    $response['error_code'] = '104';
                    $response['message'] = 'Zone not active';

                    echo json_encode($response);
                    exit();
                } else {
                    $response = array();
                    $response['status'] = 'error';
                    $response['error_code'] = '105';
                    $response['message'] = 'Invalid zone id';

                    echo json_encode($response);
                    exit();
                }
            } else if (isset($tablet->tablet_id) && $tablet->tablet_status == 0) {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '103';
                $response['message'] = 'Inactive tablet';

                echo json_encode($response);
                exit();
            } else {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '102';
                $response['message'] = 'Invalid IMEI';

                echo json_encode($response);
                exit();
            }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }

    /*
     * Author : Jiby
     * Purpose : Manager app    
     */
    public function getdayservices() {

        $this->load->model('maids_model');
        $this->load->model('day_services_model');

        if ($this->input->get('date') && strlen(trim($this->input->get('date'))) > 0 && $this->input->get('status') && is_numeric($this->input->get('status')) && ($this->input->get('status') == 1 || $this->input->get('status') == 2 || $this->input->get('status') == 3)) {
            $service_date = trim($this->input->get('date'));
            $service_status = trim($this->input->get('status'));

            $day_services = $this->day_services_model->get_day_services_by_status($service_date, $service_status);

            $services = array();
            $i = 0;
            foreach ($day_services as $service) {
                $payment_type = 'Daily Paying';
                if ($service->payment_type == 'D') {
                    $payment_type = 'Daily Paying';
                } else if ($service->payment_type == 'W') {
                    $payment_type = 'Weekly Paying';
                } else if ($service->payment_type == 'M') {
                    $payment_type = 'Monthly Paying';
                }

                $maid_attandence = $this->maids_model->get_maid_attandence_by_date($service->maid_id, $service_date);
//        echo 'asd<pre>';print_r($maid_attandence);exit;
                if (isset($maid_attandence->attandence_id)) {
                    $m_attandence = $maid_attandence->attandence_status;
                } else {
                    $m_attandence = "0";
                }

                $services[$i]['booking_id'] = $service->booking_id;
                $services[$i]['service_status'] = $service->service_status; // 0 - Not Started , 1 - Started,  2 - Finished, 3 - Not Done
                $services[$i]['zone_id'] = $service->zone_id;
                $services[$i]['zone_name'] = $service->zone_name;
                $services[$i]['area_id'] = $service->area_id;
                $services[$i]['area'] = $service->area_name;
                $services[$i]['customer_id'] = $service->customer_id;
                $services[$i]['customer_code'] = $this->config->item('customer_code_prepend') . $service->customer_id;
                $services[$i]['customer_name'] = $service->customer_name;
                $services[$i]['customer_address'] = $service->customer_address;
                $services[$i]['customer_mobile'] = $service->mobile_number_1;
                $services[$i]['customer_type'] = $payment_type;
                $services[$i]['maid_id'] = $service->maid_id;
                $services[$i]['maid_name'] = $service->maid_name;
                $services[$i]['maid_mobile'] = $service->maid_mobile_1;
                $services[$i]['maid_photo'] = $service->maid_photo_file;
                $services[$i]['maid_country'] = $service->maid_nationality;
                $services[$i]['maid_attandence'] = $m_attandence; // 0 - Not Present, 1 - Maid In Vehicle, 2 - Maid Out of Vehicle
                $services[$i]['shift_start'] = date('H:i', strtotime($service->time_from));
                $services[$i]['shift_end'] = date('H:i', strtotime($service->time_to));
                $services[$i]['key_status'] = $service->key_given == 'Y' ? "1" : "0";
                $services[$i]['booking_note'] = $service->booking_note;

                $i++;
            }

            echo json_encode(array('status' => 'success', 'services' => $services));
            exit();
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }

    /*
     * Author : Jiby
     * Purpose : manager app
     */
    public function getdaypayments() {
        $this->load->model('day_services_model');

        if ($this->input->get('date') && strlen(trim($this->input->get('date'))) > 0 && $this->input->get('payment') !== FALSE && is_numeric($this->input->get('payment')) && ($this->input->get('payment') == 0 || $this->input->get('payment') == 1)) {
            $service_date = trim($this->input->get('date'));
            $payment_status = trim($this->input->get('payment'));

            $day_services = $this->day_services_model->get_day_services_by_payment_status($service_date, $payment_status);
            $services = array();
            $i = 0;
            foreach ($day_services as $service) {
                $payment_type = 'Daily Paying';
                if ($service->payment_type == 'D') {
                    $payment_type = 'Daily Paying';
                } else if ($service->payment_type == 'W') {
                    $payment_type = 'Weekly Paying';
                } else if ($service->payment_type == 'M') {
                    $payment_type = 'Monthly Paying';
                }

                if ($service->payment_type == 'D') {
                    $services[$i]['booking_id'] = $service->booking_id;
                    $services[$i]['service_status'] = $service->service_status; // 0 - Not Started , 1 - Started,  2 - Finished, 3 - Not Done
                    $services[$i]['zone_id'] = $service->zone_id;
                    $services[$i]['zone_name'] = $service->zone_name;
                    $services[$i]['area_id'] = $service->area_id;
                    $services[$i]['area'] = $service->area_name;
                    $services[$i]['customer_id'] = $service->customer_id;
                    $services[$i]['customer_code'] = $this->config->item('customer_code_prepend') . $service->customer_id;
                    $services[$i]['customer_name'] = $service->customer_name;
                    $services[$i]['customer_address'] = $service->customer_address;
                    $services[$i]['customer_mobile'] = $service->mobile_number_1;
                    $services[$i]['customer_type'] = $payment_type;
                    $services[$i]['maid_id'] = $service->maid_id;
                    $services[$i]['maid_name'] = $service->maid_name;
                    $services[$i]['maid_mobile'] = $service->maid_mobile_1;
                    $services[$i]['maid_photo'] = $service->maid_photo_file;
                    $services[$i]['maid_country'] = $service->maid_nationality;
                    $services[$i]['shift_start'] = date('H:i', strtotime($service->time_from));
                    $services[$i]['shift_end'] = date('H:i', strtotime($service->time_to));
                    $services[$i]['key_status'] = $service->key_given == 'Y' ? "1" : "0";
                    $services[$i]['booking_note'] = $service->booking_note;

                    $i++;
                }
            }

            echo json_encode(array('status' => 'success', 'services' => $services));
            exit();
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }


      /*
     * Author : Azinova
     * Purpose : Forgot password api
     */
    public function forgotpassword() {
        if ($this->input->get('email') && strlen(trim($this->input->get('email'))) > 0) {
            $email = trim($this->input->get('email'));

            if ($email) {
                $this->load->model('customers_model');
                $customer = $this->customers_model->get_customers_by_field_value('email_address', $email);

                if (!empty($customer)) {
                    foreach ($customer as $customer_id) {
                        $customer_id = $customer_id;
                    }
                    $customer_details = $this->customers_model->get_customer_by_id($customer_id);
                    $this->sendmail($customer_details);

                    echo json_encode(array('status' => 'success' ,'response_code'=>'200','message' =>'We have send an email to  '.$email.' account . Please check the account for password. '));
                    exit();
                } else {

                    $response = array();
                    $response['status'] = 'error';
                    $response['error_code'] = '104';
                    $response['message'] = 'Invalid email address!';

                    echo json_encode($response);
                    exit();
                }
            } else {

                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '103';
                $response['message'] = 'Parameter missing!';

                echo json_encode($response);
                exit();
            }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }

     
      /*
     * Author : Azinova
     * Purpose : Send emails from api
     */

    function sendmail($recepient_dtls) {
        
        $this->load->library('email');
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';

        $this->email->initialize($config);

        $this->email->from('shani@azinova.info', 'MyMaid');
        $this->email->to($recepient_dtls->email_address);
        $this->email->bcc('jiby.james@azinova.info');


        $this->email->subject('Password Recovery');

        $message = $recepient_dtls->customer_name . ',';
        $message .= '<br /><br /> We received a request to change your password on Mymaid.';
        $message .= '<br /><br /> Your new password is <b>' . $recepient_dtls->customer_password . '</b>';
        $message .= '<br /><br /> Thanks,';
        $message .= '<br /> Mymaid Team';
        $this->email->message($message);

        //$this->email->send();

        //echo $this->email->print_debugger();
    }

    /*
     * Author : Jiby
     * Purpose : Manager app
     */
    function getbookings() {
        $this->load->model('bookings_model');
        $booking_dtls = $this->bookings_model->get_new_bookings();
        $responseArr['booking'] = array();


        if (!empty($booking_dtls)) {
            foreach ($booking_dtls as $row) {
                $data                   = array();
                $responseArr['shifts']  = array();
                $data['booking_id']     = $row->booking_id;
                $data['service_start_date'] = $row->service_start_date;
                $shift                      = $row->shift;
                $service_week_day       = $row->service_week_day;
                $data['booking_type']   = $row->booking_type;
                $data['service_type_name']  = $row->service_type_name;
                $data['customer_name']  = $row->customer_name;
                $data['zone_name'] = $row->zone_name;
                $data['area_name'] = $row->area_name;
                $data['customer_address'] = $row->customer_address;

                $shift = explode(",", $shift);
                $service_week_day = explode(",", $service_week_day);

                $i = 0;
                $week_day_names = array('7' => 'Sunday', '1' => 'Monday', '2' => 'Tuesday', '3' => 'Wednesday', '4' => 'Thursday', '5' => 'Friday', '6' => 'Saturday');

                foreach ($shift as $sft) {
                    $responseArr['shifts'][$week_day_names[trim($service_week_day[$i])]] = $sft;
                    ++$i;
                }

                $data['shifts'] = $responseArr['shifts'];
                array_push($responseArr['booking'], $data);
            }
            echo json_encode(array('status' => 'success', 'bookings' => $responseArr['booking']));
            exit();
        } else {
            echo json_encode(array('status' => 'success', 'message' => 'No new bookings'));
            exit();
        }
    }
   /*
     * Author : Jiby
     * Purpose : Manager app
     */
    function deletebooking() {
        $this->load->model('bookings_model');
        if ($this->input->get('bookingId') && strlen(trim($this->input->get('bookingId'))) > 0) {
            $booking_ids = explode(",", $this->input->get('bookingId'));
            $delete = $this->bookings_model->delete_booking($booking_ids);
            if ($delete > 0) {
                echo json_encode(array('status' => 'success'));
                exit();
            } else {
                echo json_encode(array('status' => 'failure', 'message' => "Can't delete"));
                exit();
            }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }

    function push_notification($msgtype = 1) {

        //$deviceToken = 'd5df0f248a2bf7bd8a2b9b2f646dfbc9a573c7922259ae9fd5ae6dc7aab8634d';

        $device_tokens = $this->tablets_model->get_device_tokens();
        if (!empty($device_tokens)) {
            foreach ($device_tokens as $token) {
                //$deviceToken = 'd5df0f248a2bf7bd8a2b9b2f646dfbc9a573c7922259ae9fd5ae6dc7aab8634d';
                $deviceToken = $token->device_token;
                //$output = file_get_contents('http://shanu.info/mm/push.php?device_token='.$deviceToken.'&msgtype='.$msgtype);
                $output = file_get_contents('http://demo.azinova.info/php/push/push.php?device_token=' . $deviceToken . '&msgtype=' . $msgtype);
            }
        }


    }
    /*
     * Author : Jiby
     * Purpose ; Manager app
     */
    public function managerlogin() {

        $handle = fopen('php://input', 'r');
        $jsonInput = fgets($handle);

//$jsonInput  = '{"user_name" : "anas", "password" : "12345","device_token":"1212"}';

        $json_arr = json_decode($jsonInput, true);
        $user_name = trim($json_arr['user_name']);
        $password = trim($json_arr['password']);
        $device_token = trim($json_arr['device_token']);
        $chk_user = $this->tablets_model->authenticate($user_name, $password);

        if (!empty($chk_user)) {

            $login_fields = array('manager_id' => $chk_user->manager_id, 'device_token' => $device_token, 'status' => 1);

            $this->tablets_model->add_login($login_fields);

            echo json_encode(array('status' => 'success', 'user_id' => $chk_user->manager_id));
            exit();
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '103';
            $response['message'] = 'Invalid user!';

            echo json_encode($response);
            exit();
        }
    }

    public function send_notification($registatoin_ids = 'APA91bGpkJNQpQ_X6J6luaVYrPsuQgPm_gElT01TCJc5P2WogZMy8_RasvSifh5-zG1wIhAJL3UOVLkIVolvf8Sat4gHW3ppq7A864PjRUDSU78oZkw36ThWF8yjFC0rpnfRguY0jwYHMaCx7fozO50gq3J0PwFBVw', $message = 'Maid transfered!') {
        // include config
        // include_once './config.php';
        //"AIzaSyDbzRVY4vAr6Dd15jYDy7m68AqDySrTcfI"

        $message = array("price" => $message);
        $GOOGLE_API_KEY = 'AIzaSyDnqU28Lybyw5_uxP5ZPyOHr7BLUAKDWTs';
        // Set POST variables
        $url = 'https://android.googleapis.com/gcm/send';

        $fields = array(
            'registration_ids' => array($registatoin_ids),
            'data' => $message,
        );

        $headers = array(
            'Authorization: key=' . $GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);
        /* if ($result === FALSE) {
          die('Curl failed: ' . curl_error($ch));
          } */

        // Close connection
        curl_close($ch);
        //print_r($result);
    }


    /*
     * Author : Jiby
     * Purpose : Driver app
     */
    function maidchangerequest() {
        if ($this->input->get('imei') && $this->input->get('booking_id') && strlen(trim($this->input->get('imei'))) > 0 && strlen(trim($this->input->get('booking_id'))) > 0) {
            $imei = trim($this->input->get('imei'));
            $booking_id = trim($this->input->get('booking_id'));

            $tablet = $this->tablets_model->get_tablet_by_imei($imei);

            if (isset($tablet->tablet_id) && $tablet->tablet_status == 1) {

                $this->load->model('bookings_model');

                $request_fields = array();
                $request_fields['tablet_id'] = $tablet->tablet_id;
                $request_fields['booking_id'] = $booking_id;
                $response = array();
                $response['status'] = 'success';


                $requested_id = $this->bookings_model->add_maid_change_request($request_fields);

                if ($requested_id > 0) {
                    $this->push_notification(4);
                    echo json_encode($response);
                    exit();
                } else {
                    $response = array();
                    $response['status'] = 'error';
                    $response['error_code'] = '104';
                    $response['message'] = 'failure';

                    echo json_encode($response);
                    exit();
                }
            } else if (isset($tablet->tablet_id) && $tablet->tablet_status == 0) {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '103';
                $response['message'] = 'Inactive tablet';

                echo json_encode($response);
                exit();
            } else if (isset($tablet->tablet_id) && $tablet->imei != $imei) {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '102';
                $response['message'] = 'Invalid IMEI';

                echo json_encode($response);
                exit();
            }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
    /*
     * Author : Jiby
     * Purpose : Driver app
     */
    public function tabletlocations() {
        if ($this->input->get('imei') && $this->input->get('latitude') && $this->input->get('longitude') && $this->input->get('speed') && strlen(trim($this->input->get('imei'))) > 0 && strlen(trim($this->input->get('latitude'))) && strlen(trim($this->input->get('longitude'))) && strlen(trim($this->input->get('speed'))) > 0) {
            $imei = trim($this->input->get('imei'));
            $latitude = trim($this->input->get('latitude'));
            $longitude = trim($this->input->get('longitude'));
            $speed = trim($this->input->get('speed'));

            $tablet = $this->tablets_model->get_tablet_by_imei($imei);

            if (isset($tablet->tablet_id) && $tablet->tablet_status == 1) {
                $response = array();
                $location_fields = array();

                $location_fields['tablet_id'] = $tablet->tablet_id;
                $location_fields['latitude'] = $latitude;
                $location_fields['longitude'] = $longitude;
                $location_fields['speed'] = $speed;

                $location_id = $this->tablets_model->add_tablet_locations($location_fields);

                if ($location_id) {
                    $response['status'] = 'success';
                    echo json_encode($response);
                    exit();
                } else {
                    $response['status'] = 'failure';
                    echo json_encode($response);
                    exit();
                }
            } else if (isset($tablet->tablet_id) && $tablet->tablet_status == 0) {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '103';
                $response['message'] = 'Inactive tablet';

                echo json_encode($response);
                exit();
            } else {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '102';
                $response['message'] = 'Invalid IMEI';

                echo json_encode($response);
                exit();
            }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
    /*
     * Author : Jiby
     * Purpose : Driver app
     */
    public function getpayments() {
        $this->load->model('day_services_model');
        if ($this->input->get('imei') && strlen(trim($this->input->get('imei'))) > 0) {
            $imei = trim($this->input->get('imei'));
            $tablet = $this->tablets_model->get_tablet_by_imei($imei);

            if (isset($tablet->tablet_id) && $tablet->tablet_status == 1) {
                //$payments = $this->day_services_model->get_payments($tablet->tablet_id);
                $payments = $this->day_services_model->get_payments_new($tablet->tablet_id);
//echo 'SDSD<pre>';print_r($payments);exit;

                $services = array();
                $i = 0;
                foreach ($payments as $service) {
                    $payment_type = 'Daily Paying';
                    if ($service->payment_type == 'D') {
                        $payment_type = 'Daily Paying';
                    } else if ($service->payment_type == 'W') {
                        $payment_type = 'Weekly Paying';
                    } else if ($service->payment_type == 'M') {
                        $payment_type = 'Monthly Paying';
                    }

                    if ($service->payment_type == 'D') {
                        //$services[$i]['booking_id'] = $service->booking_id;
                        //$services[$i]['service_status'] = $service->service_status; // 0 - Not Started , 1 - Started,  2 - Finished, 3 - Not Done
//                                                $services[$i]['zone_id'] = $service->zone_id;
//                                                $services[$i]['zone_name'] = $service->zone_name;
//                                                $services[$i]['area_id'] = $service->area_id;
//                                                $services[$i]['area'] = $service->area_name;
                        $services[$i]['customer_id'] = $service->customer_id;
                        $services[$i]['customer_code'] = $this->config->item('customer_code_prepend') . $service->customer_id;
                        $services[$i]['customer_name'] = $service->customer_name;
                        $services[$i]['customer_address'] = $service->customer_address;
                        $services[$i]['customer_mobile'] = $service->mobile_number_1;
                        $services[$i]['customer_type'] = $payment_type;
                        $services[$i]['paid_amount'] = $service->paid_amount;
                        $services[$i]['payment_status'] = $service->payment_status;
                        //$services[$i]['maid_id'] = $service->maid_id;
                        //$services[$i]['maid_name'] = $service->maid_name;                                                
                        //$services[$i]['maid_country'] = $service->maid_nationality;
                        //$services[$i]['shift_start'] = date('H:i', strtotime($service->time_from));
                        //$services[$i]['shift_end'] = date('H:i', strtotime($service->time_to));


                        $i++;
                    }
                }

                echo json_encode(array('status' => 'success', 'payments' => $services));
                exit();
            } else if (isset($tablet->tablet_id) && $tablet->tablet_status == 0) {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '103';
                $response['message'] = 'Inactive tablet';

                echo json_encode($response);
                exit();
            } else {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '102';
                $response['message'] = 'Invalid IMEI';

                echo json_encode($response);
                exit();
            }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }


    /*
     * Author : Jiby
     * Purpose : Driver app
     */
    public function getmaidattendacereport() {

        if ($this->input->get('imei') && strlen(trim($this->input->get('imei'))) > 0) {
            $imei = trim($this->input->get('imei'));
            $tablet = $this->tablets_model->get_tablet_by_imei($imei);

            if (isset($tablet->tablet_id) && $tablet->tablet_status == 1) {

                $maid_attendance = $this->tablets_model->maid_attendance_by_tablet($tablet->tablet_id);

                $m_attendance = array();
                $i = 0;
                foreach ($maid_attendance as $attendance) {

                    $m_attendance[$i]['maid_id'] = $attendance->maid_id;
                    $m_attendance[$i]['maid_name'] = $attendance->maid_name;
                    $m_attendance[$i]['tablet_id'] = $attendance->tablet_id;
                    $m_attendance[$i]['zone_name'] = $attendance->zone_name;
                    $m_attendance[$i]['attandence_status'] = $attendance->attandence_status;

                    $i++;
                }

                $maids = $this->maids_model->get_maids();

                $all_maids = array();
                $i = 0;
                foreach ($maids as $maid) {

                    $ar = $this->search_maid($maid->maid_id, $m_attendance);

                    if (!empty($ar)) {
                        $all_maids[$i]['maid_id'] = $ar['maid_id'];
                        $all_maids[$i]['maid_name'] = $ar['maid_name'];
                        $all_maids[$i]['tablet_id'] = $ar['tablet_id'];
                        $all_maids[$i]['zone_name'] = $ar['zone_name'];
                        $all_maids[$i]['attandence_status'] = $ar['attandence_status'];
                    } else {
                        $all_maids[$i]['maid_id'] = $maid->maid_id;
                        $all_maids[$i]['maid_name'] = $maid->maid_name;
                        $all_maids[$i]['tablet_id'] = NULL;
                        $all_maids[$i]['zone_name'] = NULL;
                        $all_maids[$i]['attandence_status'] = NULL;
                    }
                    $schedule = $this->bookings_model->get_schedule_by_maid($maid->maid_id, $tablet->zone_id);

                    if (!empty($schedule)) {
                        $all_maids[$i]['scheduled_zone'] = $schedule->zone_name;
                    } else {
                        $all_maids[$i]['scheduled_zone'] = NULL;
                    }

                    $i++;
                }
                //$attendance = $m_attendance + $all_maids;
                echo json_encode(array('status' => 'success', 'attandence' => $all_maids));
                exit();
            } else if (isset($tablet->tablet_id) && $tablet->tablet_status == 0) {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '103';
                $response['message'] = 'Inactive tablet';

                echo json_encode($response);
                exit();
            } else {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '102';
                $response['message'] = 'Invalid IMEI';

                echo json_encode($response);
                exit();
            }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }

      /*
     * Author : Jiby
     * Purpose : Driver app
     */

    function search_maid($maid_id, $array) {
        $data = array();
        foreach ($array as $ar) {
            if ($ar['maid_id'] == $maid_id) {
                $data = $ar;
                break;
            }
        }

        return $data;
    }

    /*
     * Author : Jiby
     * Purpose : Admin app
     */
    public function dailyRepwithSch() {
        if ($this->input->get('date') && strlen(trim($this->input->get('date'))) > 0) {
            $this->load->model('customers_model');
            $this->load->model('reports_model');
            $date = trim($this->input->get('date'));

            $tomo_date = date('Y-m-d', strtotime($date));
            $tomo_date = date('Y-m-d', strtotime('+1 day', strtotime($tomo_date)));


            $response = array();

            $response['status'] = 'success';
            $response["activity keys"] = array("Total Booking Hours",
                "Total Booking",
                "One Day Booking",
                "Long Term Booking",
                "Total Cancellation",
                "Total Invoice Amount",
                "Day Collection",
                "Total PNR",
                "Pending Received",
                "Total Pending",
                "New Customer",
            );
            $total_bk_hrs = $this->reports_model->get_total_booking_hours(date('Y-m-d', strtotime($date)), 1);
            $total_collection = $this->reports_model->get_day_collection(date('Y-m-d', strtotime($date)));
            $response["activity value"] = array((string) $this->reports_model->get_total_booking_hours(date('Y-m-d', strtotime($date)), 1),
                (string) $this->reports_model->get_total_bookings(date('Y-m-d', strtotime($date)), 1),
                (string) $this->reports_model->get_one_day_bookings(date('Y-m-d', strtotime($date)), 1),
                (string) $this->reports_model->get_week_day_bookings(date('Y-m-d', strtotime($date)), 1),
                (string) $this->reports_model->get_total_cancellation(date('Y-m-d', strtotime($date))),
                (string) $this->reports_model->get_total_invoice_amount(date('Y-m-d', strtotime($date))),
                (string) $this->reports_model->get_day_collection(date('Y-m-d', strtotime($date))),
                (string) abs($total_bk_hrs - $total_collection),
                '0',
                (string) $this->reports_model->get_total_pending_amount(date('Y-m-d', strtotime($date))),
                (string) $this->reports_model->get_new_customer(date('Y-m-d', strtotime($date))),
            );
            $response["tomorrows plan keys"] = array("Total Booking Hours",
                "Total Booking",
                "One Day Booking",
                "Long Term Booking",
            );
            $response["tomorrows plan value"] = array((string) $this->reports_model->get_total_booking_hours(date('Y-m-d', strtotime($tomo_date)), 1),
                (string) $this->reports_model->get_total_bookings(date('Y-m-d', strtotime($tomo_date)), 1),
                (string) $this->reports_model->get_one_day_bookings(date('Y-m-d', strtotime($tomo_date)), 1),
                (string) $this->reports_model->get_week_day_bookings(date('Y-m-d', strtotime($tomo_date)), 1), f
            );

//echo 'SD<pre>';print_r($response);exit;

            echo json_encode($response);
            exit();
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
    /*
     * Author : Jiby
     * Purpose : Admin app
     */
    public function pastDateRep() {
        if ($this->input->get('date') && strlen(trim($this->input->get('date'))) > 0) {


            $this->load->model('reports_model');
            $date = trim($this->input->get('date'));

//    $this->reports_model = new Report();

            $response = array();

            $response['status'] = 'success';
            $response["activity keys"] = array("Total Booking Hours",
                "Total Booking",
                "One Day Booking",
                "Long Term Booking",
                "Total Cancellation",
                "Total Invoice Amount",
                "Day Collection",
                "Total PNR",
                "Pending Received",
                "Total Pending",
                "New Customer",
            );
            $total_bk_hrs = $this->reports_model->get_total_booking_hours(date('Y-m-d', strtotime($date)), 2);
            $total_collection = $this->reports_model->get_day_collection(date('Y-m-d', strtotime($date)));
            $response["activity value"] = array((string) $this->reports_model->get_total_booking_hours(date('Y-m-d', strtotime($date)), 2),
                (string) $this->reports_model->get_total_bookings(date('Y-m-d', strtotime($date)), 2),
                (string) $this->reports_model->get_one_day_bookings(date('Y-m-d', strtotime($date)), 2),
                (string) $this->reports_model->get_week_day_bookings(date('Y-m-d', strtotime($date)), 2),
                (string) $this->reports_model->get_total_cancellation(date('Y-m-d', strtotime($date))),
                (string) $this->reports_model->get_total_invoice_amount(date('Y-m-d', strtotime($date))),
                (string) $this->reports_model->get_day_collection(date('Y-m-d', strtotime($date))),
                (string) abs($total_bk_hrs - $total_collection),
                'NA',
                (string) $this->reports_model->get_total_pending_amount(date('Y-m-d', strtotime($date))),
                (string) $this->reports_model->get_new_customer(date('Y-m-d', strtotime($date))),
            );


            echo json_encode($response);
            exit();
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
    /*
     * Author : Jiby
     * Purpose : Admin app
     */
    public function futureSch() {
        if ($this->input->get('date') && strlen(trim($this->input->get('date'))) > 0) {

            $this->load->model('reports_model');
            $date = trim($this->input->get('date'));

//    $this->reports_model = new Report();
            $response = array();

            $response['status'] = 'success';
            $response["plan keys"] = array("Total Booking Hours",
                "Total Booking",
                "One Day Booking",
                "Long Term Booking",
            );

            $response["plan value"] = array((string) $this->reports_model->get_total_booking_hours(date('Y-m-d', strtotime($date)), 2),
                (string) $this->reports_model->get_total_bookings(date('Y-m-d', strtotime($date)), 2),
                (string) $this->reports_model->get_one_day_bookings(date('Y-m-d', strtotime($date)), 2),
                (string) $this->reports_model->get_week_day_bookings(date('Y-m-d', strtotime($date)), 2),
            );

//    echo '<pre>';print_r($response);exit;

            echo json_encode($response);
            exit();
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
    /*
     * Author : Jiby
     * Purpose : Admin app
     */
    public function twodatereport() {
        if ($this->input->get('date1') && strlen(trim($this->input->get('date1'))) > 0 && $this->input->get('date2') && strlen(trim($this->input->get('date2'))) > 0) {

            $this->load->model('reports_model');
            $date = trim($this->input->get('date1'));
            $date2 = trim($this->input->get('date2'));

            $response = array();

            $response['status'] = 'success';
            $response["activity keys"] = array("Total Booking Hours",
                "Total Booking",
                "One Day Booking",
                "Long Term Booking",
                "Total Cancellation",
                "Total Invoice Amount",
                "Day Collection",
                "Total PNR",
                "Pending Received",
                "Total Pending",
                "New Customer",
            );
            $total_bk_hrs = $this->reports_model->get_total_booking_hours(date('Y-m-d', strtotime($date)), 2, date('Y-m-d', strtotime($date2)));
            $total_collection = $this->reports_model->get_day_collection(date('Y-m-d', strtotime($date)), date('Y-m-d', strtotime($date2)));
            $response["activity value"] = array((string) $this->reports_model->get_total_booking_hours(date('Y-m-d', strtotime($date)), 2, date('Y-m-d', strtotime($date2))),
                (string) $this->reports_model->get_total_bookings(date('Y-m-d', strtotime($date)), 2, date('Y-m-d', strtotime($date2))),
                (string) $this->reports_model->get_one_day_bookings(date('Y-m-d', strtotime($date)), 2, date('Y-m-d', strtotime($date2))),
                (string) $this->reports_model->get_week_day_bookings(date('Y-m-d', strtotime($date)), 2, date('Y-m-d', strtotime($date2))),
                (string) $this->reports_model->get_total_cancellation(date('Y-m-d', strtotime($date)), date('Y-m-d', strtotime($date2))),
                (string) $this->reports_model->get_total_invoice_amount(date('Y-m-d', strtotime($date)), date('Y-m-d', strtotime($date2))),
                (string) $this->reports_model->get_day_collection(date('Y-m-d', strtotime($date)), date('Y-m-d', strtotime($date2))),
                (string) abs($total_bk_hrs - $total_collection),
                'NA',
                (string) $this->reports_model->get_total_pending_amount(date('Y-m-d', strtotime($date)), date('Y-m-d', strtotime($date2))),
                (string) $this->reports_model->get_new_customer(date('Y-m-d', strtotime($date)), date('Y-m-d', strtotime($date2))),
            );

//              echo '<pre>'; print_r($total_collection);exit;

            echo json_encode($response);
            exit();
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
    /*
     * Author : Jiby
     * Purpose : Admin app
     */
    public function monthlyreport() {
        if ($this->input->get('year') && strlen(trim($this->input->get('year'))) > 0 && $this->input->get('month') && strlen(trim($this->input->get('month'))) > 0) {

            $this->load->model('reports_model');
            $year = trim($this->input->get('year'));
            $month = trim($this->input->get('month'));

            $date = $year . '-' . $month . '-01';
            $date2 = date('Y-m-t', strtotime($date));

            $response = array();

            $response['status'] = 'success';
            $response["activity keys"] = array("Total Booking Hours",
                "Total Booking",
                "One Day Booking",
                "Long Term Booking",
                "Total Cancellation",
                "Total Invoice Amount",
                "Day Collection",
                "Total PNR",
                "Pending Received",
                "Total Pending",
                "New Customer",
            );
            $total_bk_hrs = $this->reports_model->get_total_booking_hours(date('Y-m-d', strtotime($date)), 2, date('Y-m-d', strtotime($date2)));
            $total_collection = $this->reports_model->get_day_collection(date('Y-m-d', strtotime($date)), date('Y-m-d', strtotime($date2)));
            $response["activity value"] = array((string) $this->reports_model->get_total_booking_hours(date('Y-m-d', strtotime($date)), 2, date('Y-m-d', strtotime($date2))),
                (string) $this->reports_model->get_total_bookings(date('Y-m-d', strtotime($date)), 2, date('Y-m-d', strtotime($date2))),
                (string) $this->reports_model->get_one_day_bookings(date('Y-m-d', strtotime($date)), 2, date('Y-m-d', strtotime($date2))),
                (string) $this->reports_model->get_week_day_bookings(date('Y-m-d', strtotime($date)), 2, date('Y-m-d', strtotime($date2))),
                (string) $this->reports_model->get_total_cancellation(date('Y-m-d', strtotime($date)), date('Y-m-d', strtotime($date2))),
                (string) $this->reports_model->get_total_invoice_amount(date('Y-m-d', strtotime($date)), date('Y-m-d', strtotime($date2))),
                (string) $this->reports_model->get_day_collection(date('Y-m-d', strtotime($date)), date('Y-m-d', strtotime($date2))),
                (string) abs($total_bk_hrs - $total_collection),
                'NA',
                (string) $this->reports_model->get_total_pending_amount(date('Y-m-d', strtotime($date)), date('Y-m-d', strtotime($date2))),
                (string) $this->reports_model->get_new_customer(date('Y-m-d', strtotime($date)), date('Y-m-d', strtotime($date2))),
            );


            echo json_encode($response);
            exit();
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
    /*
     * Author : Jiby
     * Purpose; Flat app
     */
    public function maidsynch() {
        $this->load->model('maids_model');
        $maids = $this->maids_model->get_maids();
        $maidArr = array();
        foreach ($maids as $row) {
            $maidArr[] = array(
                'maid_id' => $row->maid_id,
                'maid_name' => $row->maid_name,
                'rfid' => $row->rfid,
                'photo' => strlen(trim($row->maid_photo_file)) > 0 ? base_url() . 'assets/maid/photos/75_75_' . $row->maid_photo_file : '',
                'nationality' => $row->maid_nationality,
                'address' => $row->maid_present_address
            );
        }
        echo json_encode(array("status" => "success", "result" => $maidArr));
    }

    /*
     * Author : Jiby
     * Purpose; Flat app
     */
    public function flatlist() {

        $this->load->model('tablets_model');
        $flatList = $this->tablets_model->getFlatLIst();

        $respArr['flatList'] = array();

        foreach ($flatList as $flat) {
            array_push($respArr['flatList'], $flat);
        }

        echo json_encode(array("status" => "success", "result" => $respArr['flatList']));
    }
 /*
     * Author : Jiby
     * Purpose; Flat app
     */
    public function tabRegForFlat() {
        if ($this->input->get('regNo') && strlen(trim($this->input->get('regNo'))) > 0 && $this->input->get('imeiNo') && strlen(trim($this->input->get('imeiNo'))) > 0 && $this->input->get('flatId') && strlen(trim($this->input->get('flatId'))) > 0) {


            $regNo = trim($this->input->get('regNo'));
            $flatId = trim($this->input->get('flatId'));
            $imeiNo = trim($this->input->get('imeiNo'));

            $this->load->model('tablets_model');
            $tabReg = $this->tablets_model->regTabletForFlat($regNo, $imeiNo, $flatId);
            if ($tabReg > 0)
                echo json_encode(array("status" => "success", "tabId" => $tabReg));
            else if ($tabReg == -1)
                echo json_encode(array("status" => "Invalid Registration code"));
            else
                echo json_encode(array("status" => "failed"));
        }
        else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Missing parametres';

            echo json_encode($response);
            exit();
        }
    }
/*
     * Author : Jiby
     * Purpose; Flat app
     */
    public function tabLogin() {
        if ($this->input->get('access_code') && strlen(trim($this->input->get('access_code'))) > 0 && $this->input->get('tabId') && strlen(trim($this->input->get('tabId'))) > 0) {


            $access_code = trim($this->input->get('access_code'));
            $tabId = trim($this->input->get('tabId'));

            $this->load->model('tablets_model');
            $tabLogin = $this->tablets_model->loginTablet($access_code, $tabId);
            if ($tabLogin == 1)
                echo json_encode(array("status" => "success"));
            else
                echo json_encode(array("status" => "failed"));
        }
        else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Missing parametres';

            echo json_encode($response);
            exit();
        }
    }
    /*
     * Author : Jiby
     * Purpose; Flat app
     */
    public function scheduleForFlatMaid() {
        if ($this->input->get('flatId') && strlen(trim($this->input->get('flatId'))) > 0 && $this->input->get('tabId') && strlen(trim($this->input->get('tabId'))) > 0) {


            $access_code = trim($this->input->get('access_code'));
            $flatId = trim($this->input->get('flatId'));
            $tabId = trim($this->input->get('tabId'));

            $this->load->model('tablets_model');

            $maids = $this->tablets_model->getMaidForFlatTab($flatId);
            $respArr['maidList'] = array();
            foreach ($maids as $row) {
                array_push($respArr['maidList'], $row);
            }


            $maidList = $respArr['maidList'];




//                $tablet_id = trim(param('tabId'));

            $tablet = $this->tablets_model->get_tablet_by_id($tabId);

            if (isset($tablet->tablet_id) && $tablet->tablet_status == 1) {

                $maids = $this->tablets_model->get_maids();


                $service_date = date('Y-m-d');
                $tomorrow = date('Y-m-d', strtotime(date('d-M-Y') . ' + 1 day'));
                $schedule = array();
                for ($j = 1; $j <= 2; ++$j) {
                    $service_date = $j == 1 ? $service_date : $tomorrow;
                    $service_day = $j == 1 ? 'today' : 'tomorrow';
                    //echo $tablet->tablet_id;
                    $bookings = $this->tablets_model->get_schedule_by_flat($tablet->tablet_id, $tablet->zone_id, $service_date);
                    //print_r($bookings);

                    $booking_transfers = $this->tablets_model->get_booking_transfers_by_date($service_date);
                    $transferred_booking_zones = array();
                    $trans_bookings = array();
                    foreach ($booking_transfers as $b_transfer) {
                        $transferred_booking_zones[$b_transfer->booking_id] = $b_transfer->zone_id;
                        //extra vishnu
                        $trans_bookings[] = $b_transfer->booking_id;
                        //ends
                    }

                    //echo '<pre>';print_r($bookings);exit;
                    $i = 0;
                    foreach ($bookings as $booking) {
                        //if(($booking->zone_id == $tablet->zone_id && !isset($transferred_booking_zones[$booking->booking_id])) || (isset($transferred_booking_zones[$booking->booking_id]) && $transferred_booking_zones[$booking->booking_id] == $tablet->zone_id))
                        //{
                        //extra vishnu
                        if (in_array($booking['activeBookingId'], $trans_bookings)) {
                            $getzonenme = $this->tablets_model->get_zone_name_by_bookingid($booking['activeBookingId'], $service_date);
                            $zonename = $booking['customer_zone'] . ' (' . $getzonenme['zone_name'] . ')';
                        } else {
                            $zonename = $booking['customer_zone'];
                        }
                        //ends

                        $payment_type = 'Daily Paying';
                        if ($booking['payment_type'] == 'D') {
                            $payment_type = 'Daily Paying';
                        } else if ($booking['payment_type'] == 'W') {
                            $payment_type = 'Weekly Paying';
                        } else if ($booking['payment_type'] == 'M') {
                            $payment_type = 'Monthly Paying';
                        }

                        $maid_attandence = $this->tablets_model->get_maid_attandence_by_date($booking['maidId'], $service_date);

                        if (isset($maid_attandence->attandence_id)) {
                            $m_attandence = $maid_attandence->attandence_status;
                        } else {
                            $m_attandence = "0";
                        }

                        $day_service = $this->tablets_model->get_day_service_by_booking_id($service_date, $booking['activeBookingId']);

                        if (isset($day_service->day_service_id)) {
                            $service_status = $day_service->service_status;
                        } else {
                            $service_status = 0;
                        }

                        $schedule[$booking['maidId']][$service_day][] = array('booking_id' => $booking['activeBookingId'],
                            'service_status' => $service_status, // 0 - Not Started , 1 - Started,  2 - Finished, 3 - Not Done
                            'customer_id' => $booking['customerId'],
                            'customer_code' => $booking['customerId'],
                            'customer_name' => $booking['customer_name'],
                            'customer_address' => $booking['customer_adress'],
                            'customer_mobile' => $booking['mobile1'],
                            'customer_type' => $payment_type,
                            'shift_start' => date('H:i', strtotime($booking['time_from'])),
                            'shift_end' => date('H:i', strtotime($booking['time_to'])),
                            'key_status' => $booking['key_given'] == 'Y' ? "1" : "0",
                            'area' => $booking['customer_area'] . ' (' . $booking['customer_zone'] . ')',
                            'booking_note' => $booking['notes'],
                            'maidId' => $booking['maidId'],
                            'maid_name' => $booking['maid_name'],
                            'address' => $booking['customer_adress'],
                            'zone' => $zonename,
                            'shift' => $booking['schedule'],
                        );


                        $i++;
                        //}
                    }
                }
                $response = array();
                $response['status'] = 'success';
                $response['maids'] = array();
                $today = array();
                $tomorrow = array();
                $i = 0;
                foreach ($maidList as $maid) {
                    $response['maids'][$i]['id'] = $maid->id;
                    $response['maids'][$i]['name'] = $maid->maid_name;
                    $response['maids'][$i]['maid_country'] = $maid->maid_nationality;
                    $response['maids'][$i]['photo'] = strlen(trim($maid->maid_photo_file)) > 0 ? base_url() . 'maidimg/' . $maid->maid_photo_file : '';
                    if (!empty($schedule[$maid->id]['today'])) {
                        foreach ($schedule[$maid->id]['today'] as $tdy) {

                            array_push($today, $tdy);
                        }
                    }
                    if (!empty($schedule[$maid->id]['tomorrow'])) {
                        foreach ($schedule[$maid->id]['tomorrow'] as $trw) {

                            array_push($tomorrow, $trw);
                        }
                    }
                    $i++;
                }
                $response['todaySchedule'] = $today;
                $response['tomorrowSchedule'] = $tomorrow;
                echo json_encode($response);
                exit();
            } else if (isset($tablet->tablet_id) && $tablet->tablet_status == 0) {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '103';
                $response['message'] = 'Inactive tablet';

                echo json_encode($response);
                exit();
            } else {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '102';
                $response['message'] = 'Invalid IMEI';

                echo json_encode($response);
                exit();
            }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Missing parametres';

            echo json_encode($response);
            exit();
        }
    }

    /*
     * Author : Vishnu 
     * 
     * Purpose : Customer registration for customer app
     */    
    public function cust_registration() 
    {
        if ($this->config->item('ws') == $this->input->get('ws') ) 
        {
            $handle = fopen('php://input', 'r');
            $jsonInput = fgets($handle);
            //$jsonInput  = '{"name":"Jiby","email":"3tester@gmail.com","phone":"529096658","password":"123456","device_type":"android","device_id":"33313"}'; // normal login from mobile
            $json_arr = json_decode($jsonInput, true);
            if (!empty($json_arr))
            {
                if ($json_arr['name'] && strlen($json_arr['name']) > 0 && $json_arr['email'] && strlen($json_arr['email']) > 0 && $json_arr['device_id'] && strlen($json_arr['device_id']) > 0) 
                {
                    $email_check = $this->api_model->check_email(trim($json_arr['email']));
                    if ($email_check) 
                    {  
                            $response               = array();
                            $response['status']     = 'error';
                            $response['error_code'] = '103';
                            $response['message']    = 'Email already exist, please login with your existing username and password.';
                            echo json_encode($response);
                            exit();
                    }
                    
                    if (empty($json_arr['device_type'])) {
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '103';
                        $response['message'] = 'Type missing!';
                        echo json_encode($response);
                        exit();
                    }
                    $mobile_verification_code               = strtoupper(random_string('numeric', 5));
                        
                    $user_fields                        = array();
                    $user_fields['customer_username']   = trim($json_arr['email']);
                    $user_fields['customer_type']       = 'HO';
                    $user_fields['customer_name']       = trim($json_arr['name']);
                    $user_fields['customer_nick_name']  = trim($json_arr['name']);
                    $user_fields['is_company']          = 'N';

                    $user_fields['email_address']       = trim($json_arr['email']);
                    $user_fields['contact_person']       = trim($json_arr['name']);
                    $user_fields['payment_type']        = 'D';
                    $user_fields['payment_mode']        = 'Cash';
                    $user_fields['key_given']           = 'N';
                    $user_fields['price_hourly']        = '35';
                    $user_fields['price_extra']         = '35';
                    $user_fields['price_weekend']       = '35';
                    $user_fields['customer_source']       = 'Direct Call';
                    $user_fields['customer_status']     = '1';
                    $user_fields['mobile_verification_code']   = $mobile_verification_code;
                    $user_fields['mobile_status']   = '0';
                    $user_fields['push_token']          = trim($json_arr['device_id']);
                    $user_fields['oauth_provider']          = trim($json_arr['device_type']);
                    $user_fields['customer_added_datetime']        = date('Y-m-d h:i:s');
                        
                    if (empty($json_arr['phone']))
                    {
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '103';
                        $response['message'] = 'Phone missing!';
                        echo json_encode($response);
                        exit();
                    }
                        
                    $mobile_check = $this->api_model->check_mobile(trim($json_arr['phone']));
                    if ($mobile_check) 
                    {  
                        $response               = array();
                        $response['status']     = 'error';
                        $response['error_code'] = '103';
                        $response['message']    = 'Mobile already exist, please login with your existing username and password.';
                        echo json_encode($response);
                        exit();	  
                    }
                        
                    if ($json_arr['phone'] && strlen($json_arr['phone']) > 0)
                    {       
                        $user_fields['phone_number']        = trim($json_arr['phone']);
                        $user_fields['mobile_number_1']     = trim($json_arr['phone']);
                        $user_fields['mobile_number_2']     = trim($json_arr['phone']);
                        $user_fields['mobile_number_3']     = trim($json_arr['phone']);
                    }

                    if ($json_arr['password'] && strlen($json_arr['password']) > 0) 
                    { 
                        $user_fields['customer_password']   = trim($json_arr['password']);
                        $register = $this->api_model->add_customers($user_fields);
                        if(isset($register)) 
                        {
                            $this->sent_registration_confirmation_mail($register);
                            $userInfo = $this->api_model->get_customer_by_id($register);
                            if(empty($userInfo))
                            {
                                echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'Customer not found')); exit();   
                            } else {
                                $userdetail_array = array();
                                $userdetail_array['user_id'] = $userInfo->customer_id;
                                $userdetail_array['name'] = $userInfo->customer_name;
                                $userdetail_array['email'] = $userInfo->email_address;
                                $userdetail_array['photo'] = "";
                                $userdetail_array['phone'] = $userInfo->mobile_number_1;
                                $userdetail_array['is_verified'] = false;
                            
                                echo json_encode(array('status' => 'success','response_code'=>'200','message'=>'Account Created Successfully', 'customer_details' => $userdetail_array));
                                exit();
                            }
                        } else {
                            echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'Something went wrong'));
                            exit();
                        }
                    } else {
                        $response               = array();
                        $response['status']     = 'error';
                        $response['error_code'] = '103';
                        $response['message']    = 'Password Parameter missing !';
                        echo json_encode($response);
                        exit();
                    }
                } else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '103';
                    $response['message']    = 'Parameter missing!';
                    echo json_encode($response);
                    exit();
                }
            } else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
            }
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';

            echo json_encode($response);
            exit();
        }
    }
    
    
    /*
     * Author : Vishnu
     * 
     * Purpose : Customer app - customer login
     */
    public function customerLogin() 
    {
        if ($this->config->item('ws') == $this->input->get('ws') ) 
        {
            $handle = fopen('php://input', 'r');
            $jsonInput = fgets($handle); 
            //$jsonInput  = '{"email":"3tester@gmail.com","device_id":"1212332323","device_type":"android","password":"545145451"}'; // normal login from mobile
            $json_arr = json_decode($jsonInput, true);
            
            if (!empty($json_arr))
            { 
                if ( $json_arr['email'] && strlen($json_arr['email']) > 0 && $json_arr['device_id'] && strlen($json_arr['device_id']) > 0) 
                {
                    if (empty($json_arr['device_type'])) 
                    {
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '103';
                        $response['message'] = 'Type missing!';
                        echo json_encode($response);
                        exit();
                    }
                    
                    if (empty($json_arr['email'])) 
                    {
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '103';
                        $response['message'] = 'Username missing!';
                        echo json_encode($response);
                        exit();
                    }
                    
                    if (empty($json_arr['password'])) 
                    {
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '103';
                        $response['message'] = 'password missing!';
                        echo json_encode($response);
                        exit();
                    }
                    
                    $login_check = $this->api_model->customer_login(trim($json_arr['email']) , trim($json_arr['password']));
                    
                    if(isset($login_check->customer_id))
                    {
                        $getuserdetails = $this->api_model->get_all_customer_details($login_check->customer_id);
                        
                        $userdetail_array = array();
                        $userdetail_array['user_id'] = $getuserdetails->customer_id;
                        $userdetail_array['name'] = $getuserdetails->customer_name;
                        $userdetail_array['email'] = $getuserdetails->email_address;
                        //$userdetail_array['photo'] = "";
                        if(!empty($getuserdetails->mobile_number_1))
                        {
                            $userdetail_array['phone'] = $getuserdetails->mobile_number_1;
                        } else {
                            $userdetail_array['phone'] = "";
                        }
                        if(!empty($getuserdetails->area_id))
                        {
                            $userdetail_array['area_id'] = $getuserdetails->area_id;
                        } else {
                            $userdetail_array['area_id'] = "";
                        }
                        if(!empty($getuserdetails->area_name))
                        {
                            $userdetail_array['area_name'] = $getuserdetails->area_name;
                        } else {
                            $userdetail_array['area_name'] = "";
                        }
                        if(!empty($getuserdetails->building))
                        {
                            $userdetail_array['building'] = $getuserdetails->building;
                        } else {
                            $userdetail_array['building'] = "";
                        }
                        if(!empty($getuserdetails->unit_no))
                        {
                            $userdetail_array['unit'] = $getuserdetails->unit_no;
                        } else {
                            $userdetail_array['unit'] = "";
                        }
                        if(!empty($getuserdetails->street))
                        {
                            $userdetail_array['street'] = $getuserdetails->street;
                        } else {
                            $userdetail_array['street'] = "";
                        }
                        if(!empty($getuserdetails->other_area))
                        {
                            $userdetail_array['other_area'] = $getuserdetails->other_area;
                        } else {
                            $userdetail_array['other_area'] = "";
                        }
                        if($getuserdetails->mobile_status == 0)
                        {
                            $userdetail_array['is_verified'] = false;
                        } else {
                            $userdetail_array['is_verified'] = true;
                        }
                        if(!empty($getuserdetails->customer_photo_file))
                        {
                            $getuserdetails->customer_photo_file = base_url(). 'customer_img/'.$getuserdetails->customer_photo_file;
                        } else {
                            $getuserdetails->customer_photo_file = base_url(). 'img/no_image.jpg';
                        }
                       
                        $userdetail_array['photo'] = $getuserdetails->customer_photo_file;
                        
                        $update_device_detail = array();
                        $update_device_detail['push_token']          = trim($json_arr['device_id']);
                        $update_device_detail['oauth_provider']          = trim($json_arr['device_type']);
                        
                        $this->api_model->update_customers($update_device_detail,$login_check->customer_id);
                        
                        echo json_encode(array('status' => 'success','response_code'=>'200','message'=>'Logged in Successfully','customer_details'=>$userdetail_array)); exit();
                    } else {
                        echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'Invalid login')); exit();
                    }
                } else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '103';
                    $response['message']    = 'Parameter missing!';
                    echo json_encode($response);
                    exit();
                }
            } else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
            }
//            print_r($json_arr);
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';

            echo json_encode($response);
            exit();
        }
    }
    
    public function facebook_registration() 
    {
        if ($this->config->item('ws') == $this->input->get('ws') ) 
        {
            $handle = fopen('php://input', 'r');
            $jsonInput = fgets($handle);
            //$jsonInput  = '{"name":"Jiby","email":"3tester@gmail.com","device_type":"android","device_id":"33313"}'; // normal login from mobile
            $json_arr = json_decode($jsonInput, true);
            if (!empty($json_arr))
            {
                if ($json_arr['name'] && strlen($json_arr['name']) > 0 && $json_arr['email'] && strlen($json_arr['email']) > 0 && $json_arr['device_id'] && strlen($json_arr['device_id']) > 0) 
                {
                    $email_check = $this->api_model->check_email(trim($json_arr['email']));
                    if ($email_check) 
                    {  
                            $response               = array();
                            $response['status']     = 'error';
                            $response['error_code'] = '103';
                            $response['message']    = 'Email already exist, please login with your existing username and password.';
                            echo json_encode($response);
                            exit();
                    }
                    
                    if (empty($json_arr['device_type'])) {
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '103';
                        $response['message'] = 'Type missing!';
                        echo json_encode($response);
                        exit();
                    }
                    
                    if (empty($json_arr['auth_token'])) {
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '103';
                        $response['message'] = 'Token missing!';
                        echo json_encode($response);
                        exit();
                    }
                    
                    $mobile_verification_code               = strtoupper(random_string('numeric', 5));
                        
                    $user_fields                        = array();
                    $user_fields['customer_username']   = trim($json_arr['email']);
                    $user_fields['customer_type']       = 'HO';
                    $user_fields['customer_name']       = trim($json_arr['name']);
                    $user_fields['customer_nick_name']  = trim($json_arr['name']);
                    $user_fields['is_company']          = 'N';

                    $user_fields['email_address']       = trim($json_arr['email']);
                    $user_fields['contact_person']       = trim($json_arr['name']);
                    $user_fields['payment_type']        = 'D';
                    $user_fields['payment_mode']        = 'Cash';
                    $user_fields['key_given']           = 'N';
                    $user_fields['price_hourly']        = '35';
                    $user_fields['price_extra']         = '35';
                    $user_fields['price_weekend']       = '35';
                    $user_fields['customer_source']       = 'Direct Call';
                    $user_fields['customer_status']     = '1';
                    $user_fields['mobile_verification_code']   = $mobile_verification_code;
                    $user_fields['mobile_status']   = '0';
                    $user_fields['push_token']          = trim($json_arr['device_id']);
                    $user_fields['oauth_provider']          = trim($json_arr['device_type']);
                    $user_fields['fb_id']          = trim($json_arr['auth_token']);
                    $user_fields['customer_added_datetime']        = date('Y-m-d h:i:s');
                        
                    $register = $this->api_model->add_customers($user_fields);
                    
                    if(isset($register)) 
                    {
                        $this->sent_registration_confirmation_mail($register);
                        $userInfo = $this->api_model->get_customer_by_id($register);
                        if(empty($userInfo))
                        {
                            echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'Customer not found')); exit();   
                        } else {
                            $userdetail_array = array();
                            $userdetail_array['user_id'] = $userInfo->customer_id;
                            $userdetail_array['name'] = $userInfo->customer_name;
                            $userdetail_array['email'] = $userInfo->email_address;
                            $userdetail_array['photo'] = "";
                            //$userdetail_array['phone'] = $userInfo->mobile_number_1;
                            $userdetail_array['is_verified'] = false;

                            echo json_encode(array('status' => 'success','response_code'=>'200','message'=>'Account Created Successfully', 'customer_details' => $userdetail_array));
                            exit();
                        }
                    } else {
                        echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'Something went wrong'));
                        exit();
                    }
                    
                } else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '103';
                    $response['message']    = 'Parameter missing!';
                    echo json_encode($response);
                    exit();
                }
            } else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
            }
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';

            echo json_encode($response);
            exit();
        }
    }
    
    public function facebookLogin() 
    {
        if ($this->config->item('ws') == $this->input->get('ws') ) 
        {
            $handle = fopen('php://input', 'r');
            $jsonInput = fgets($handle); 
            //$jsonInput  = '{"auth_token":"6Uv9mIT8XdTpqP6RnS3fYrS07w82","device_id":"ehBZtL1NKcc:APA91bHez_9X5nlhB-S90lmFF38JmJ7_Tnpr74M7Din6usi3bS4nBwps9kO11RZqqjHplW7i5lCm2ZHp-_xS3R6fdaHU_ICAkxknBoEswKQefXL77Vpz6zLITh1BmqO5viGW8uVnkvQC","device_type":"Android"}'; // normal login from mobile
            $json_arr = json_decode($jsonInput, true);
            
            if (!empty($json_arr))
            { 
                if ( $json_arr['device_id'] && strlen($json_arr['device_id']) > 0) 
                {
                    if (empty($json_arr['device_type'])) 
                    {
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '103';
                        $response['message'] = 'Type missing!';
                        echo json_encode($response);
                        exit();
                    }
                    
                    if (empty($json_arr['auth_token'])) 
                    {
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '103';
                        $response['message'] = 'Token missing!';
                        echo json_encode($response);
                        exit();
                    }
                    
                    //$type       = trim($json_arr['device_type']);
                    $token      = trim($json_arr['auth_token']);
                    $customer   = $this->api_model->get_customer_by_token($token);
                    
                    if(isset($customer->customer_id))
                    {
                        $update_device_detail = array();
                        $update_device_detail['push_token']          = trim($json_arr['device_id']);
                        $update_device_detail['oauth_provider']          = trim($json_arr['device_type']);
                        
                        $this->api_model->update_customers($update_device_detail,$customer->customer_id);
                        
                        $getuserdetails = $this->api_model->get_all_customer_details($customer->customer_id);
                        
                        $userdetail_array = array();
                        $userdetail_array['user_id'] = $getuserdetails->customer_id;
                        $userdetail_array['name'] = $getuserdetails->customer_name;
                        $userdetail_array['email'] = $getuserdetails->email_address;
                        //$userdetail_array['photo'] = "";
                        if(!empty($getuserdetails->mobile_number_1))
                        {
                            $userdetail_array['phone'] = $getuserdetails->mobile_number_1;
                        } else {
                            $userdetail_array['phone'] = "";
                        }
                        if(!empty($getuserdetails->area_id))
                        {
                            $userdetail_array['area_id'] = $getuserdetails->area_id;
                        } else {
                            $userdetail_array['area_id'] = "";
                        }
                        if(!empty($getuserdetails->area_name))
                        {
                            $userdetail_array['area_name'] = $getuserdetails->area_name;
                        } else {
                            $userdetail_array['area_name'] = "";
                        }
                        if(!empty($getuserdetails->building))
                        {
                            $userdetail_array['building'] = $getuserdetails->building;
                        } else {
                            $userdetail_array['building'] = "";
                        }
                        if(!empty($getuserdetails->unit_no))
                        {
                            $userdetail_array['unit'] = $getuserdetails->unit_no;
                        } else {
                            $userdetail_array['unit'] = "";
                        }
                        if(!empty($getuserdetails->street))
                        {
                            $userdetail_array['street'] = $getuserdetails->street;
                        } else {
                            $userdetail_array['street'] = "";
                        }
                        if(!empty($getuserdetails->other_area))
                        {
                            $userdetail_array['other_area'] = $getuserdetails->other_area;
                        } else {
                            $userdetail_array['other_area'] = "";
                        }
                        if($getuserdetails->mobile_status == 0)
                        {
                            $userdetail_array['is_verified'] = false;
                        } else {
                            $userdetail_array['is_verified'] = true;
                        }
                        if(!empty($getuserdetails->customer_photo_file))
                        {
                            $getuserdetails->customer_photo_file = base_url(). 'customer_img/'.$getuserdetails->customer_photo_file;
                        } else {
                            $getuserdetails->customer_photo_file = base_url(). 'img/no_image.jpg';
                        }
                       
                        $userdetail_array['photo'] = $getuserdetails->customer_photo_file;
                        
                        echo json_encode(array('status' => 'success','response_code'=>'200','message'=>'Logged in Successfully','customer_details'=>$userdetail_array)); exit();
                        
                    } else {
                        echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'Invalid login')); exit();
                    }
                    
                } else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '103';
                    $response['message']    = 'Parameter missing!';
                    echo json_encode($response);
                    exit();
                }
            } else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
            }
//            print_r($json_arr);
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';

            echo json_encode($response);
            exit();
        }
    }
    
     /*
     * Author   : Vishnu
     * Date     : 07-11-17
     * Purpose  :Get Area details for customer app
     */
    public function get_all_areas()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            $areas = array();
            $areas = $this->api_model->get_areas();
                
            echo json_encode(array('status' => 'success', 'areas' => $areas));
            exit();
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';
            echo json_encode($response);
            exit();
        }
    }
    
    /*
     * Author : Vishnu
     * 
     * Purpose : User Details from customer app
     */
    public function userDetails()
    {
        if ($this->config->item('ws') == $this->input->get('ws') ) 
        { 
            $handle     = fopen('php://input', 'r');
            $jsonInput  = fgets($handle); 
            //$jsonInput  = '{"user_id":"79"}'; // normal login from mobile
            $json_arr   = json_decode($jsonInput, true);
            
            if (!empty($json_arr))
            { 
                if ( $json_arr['user_id'] && strlen($json_arr['user_id']) > 0 ) 
                {
                    $getuserdetails = $this->api_model->get_all_customer_details($json_arr['user_id']);
                    
                    if(empty($getuserdetails))
                    {
                        echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'Customer not found')); exit();   
                    } else {
                        $userdetail_array = array();
                        $userdetail_array['user_id'] = $getuserdetails->customer_id;
                        $userdetail_array['name'] = $getuserdetails->customer_name;
                        $userdetail_array['email'] = $getuserdetails->email_address;
                        //$userdetail_array['photo'] = "";
                        if(!empty($getuserdetails->mobile_number_1))
                        {
                            $userdetail_array['phone'] = $getuserdetails->mobile_number_1;
                        } else {
                            $userdetail_array['phone'] = "";
                        }
                        if(!empty($getuserdetails->area_id))
                        {
                            $userdetail_array['area_id'] = $getuserdetails->area_id;
                        } else {
                            $userdetail_array['area_id'] = "";
                        }
                        if(!empty($getuserdetails->area_name))
                        {
                            $userdetail_array['area_name'] = $getuserdetails->area_name;
                        } else {
                            $userdetail_array['area_name'] = "";
                        }
                        if(!empty($getuserdetails->building))
                        {
                            $userdetail_array['building'] = $getuserdetails->building;
                        } else {
                            $userdetail_array['building'] = "";
                        }
                        if(!empty($getuserdetails->unit_no))
                        {
                            $userdetail_array['unit'] = $getuserdetails->unit_no;
                        } else {
                            $userdetail_array['unit'] = "";
                        }
                        if(!empty($getuserdetails->street))
                        {
                            $userdetail_array['street'] = $getuserdetails->street;
                        } else {
                            $userdetail_array['street'] = "";
                        }
                        if(!empty($getuserdetails->other_area))
                        {
                            $userdetail_array['other_area'] = $getuserdetails->other_area;
                        } else {
                            $userdetail_array['other_area'] = "";
                        }
                        if($getuserdetails->mobile_status == 0)
                        {
                            $userdetail_array['is_verified'] = false;
                        } else {
                            $userdetail_array['is_verified'] = true;
                        }
                        if(!empty($getuserdetails->customer_photo_file))
                        {
                            $getuserdetails->customer_photo_file = base_url(). 'customer_img/'.$getuserdetails->customer_photo_file;
                        } else {
                            $getuserdetails->customer_photo_file = base_url(). 'img/no_image.jpg';
                        }
                        $userdetail_array['photo'] = $getuserdetails->customer_photo_file;
                        
                        echo json_encode(array('status' => 'success','response_code'=>'200','customer_details'=>$userdetail_array)); exit();  
                    }  
                } else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '103';
                    $response['message']    = 'Parameter missing!';
                    echo json_encode($response);
                    exit();
                }
            } else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
            }
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';
            echo json_encode($response);
            exit();
        }
    }
    
    /*
     * Author : Vishnu
     * Date : 02-11-17
     * Purpose : Edit User Details from customer app
     */

    public function editUserDetails()
    { 
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            $handle     = fopen('php://input', 'r');
            $jsonInput  = fgets($handle); 
            //$jsonInput  = '{"user_id":"57","name":"test","phone":"1212212","photo":"","area_id":"","building":"","unit":"","street":"","other_area_name":""}'; // normal login from mobile
//            $jsonInput  = '{"customer_id":"10","photo":"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANcAAADXCAMAAAC+ozSHAAADAFBMVEX9yaYhISHHZTwREREiIiL1uZZyPS2sWTz////AY0IaGhogICDytZD7nZIcHBwUFBS7YEASEhIeHh75wp62Xj/tq4aGRzIZGRn8yKUWFhadUjh1Pi2PSzS+YUGxWz2ARDD3vZqvWjyzXD2oVzv8xqShUzmZUDaWTjYbGxu4npaVbWH7xKH1upf52cb+3MW4Xz7czspVRjytWTzwr4v+49H8v7ikVTr4wJz9za3ej2jnuqXys5B5QC58Qi/+3sjVq5yLSTPVflbKakHppH/zz7v2vJkYGBj2u5j90bSLbVv+4MzNb0eSTDXGn4TDw8TsqYUuLCvk29f28vGMYVTisZy0emE7NTCvnopXTEZ6RTPirJHjmXT1wJ7PdEzK1+GMf3jmnnmpmYX+5NLx8PBuaWb7oJTKtq/xso7t5uR6STrYhF3Sw73JlXq5X0D8v6GUYEtKPjbskoiERTHNfHXbiWP97+aDVUemhXvapofSelKOdWOwkYi8YDvAq6TCim/+6Nq4g2nglG7urIiSXEf9+/nqsI48PDyShn57QS+omYzztpL8t6/il3GlgWvT0tLtxbLJppm/knfsw6mWi4SIiIjvvp2xo5JmVkyde23DYzzW4Obi6e/RppaBTTy9oJb8sJr7ppincVq1tLShkYBpPCrAqpUxJiJbW1ri4eHxy7e2qJeMV0NLS0t3YVPRd0+1b1abj4iwYUaeZlDRnYD917ympabXhFyIa1n21MK/l337rqWrinX79vRXUEzrt5jBYjzu8vbIwb+WlpbBdG3VmXvYuayjalO4lov+9O32+PnjpoT1m4/chn7htJX9zKvSgHheNih/aluteWPBgWqvaE5BLifqvqrgoIv8uZ5+eHXKkXbt1s1wWUrjzMLfwrOUeWjFu7W3i3nOnYXKno/Wy8f04djPiHjji4JTMSaag3VjUUbJsaLHeluicmPd1dDJ0NbSp4vBl4nsz8Hhl3COUTw9Jx+WZVa3nJT92cGUbF2zf26xiG/hjoP919POh2h3Rji5fJrrAAAi70lEQVR42sWdCXwURfbHOz1MdUhwJjMTZ8jMZCbJZHKzhMRNDCSSgwQhWZIQuSGiILcILPcpp8By/lc5/6CigiCKgAeKuuB9rjeu97Ge67rufa///6uq7q6e6Z5MQ/ewPz/m6Omeqm+/V++9qq4MXLIxFSFfqLu5Sq9FqGOKwX4Z4xragdYHupsuYT1ChQa5DGIF07snQGn5Rk3GGcRK654YCUE0ddJ/iYthJchkC/4rXAsSiQUK+1DH0IvPtQT5At0TqgD4YvvF5pqUaCxQmhvALi5XO0Lh7omXH6HSi8k1dD7K19m16zONgOWBxS4iVxGq1devzIDQ25jFGFjiuQrR+jR9I6RMELK7/xfAuAscXIKuPvUWsDKNgtFwn3iuDpSvGwuUbjh4FF0UrkJIyPqxDDoiKC2IFlwErilTUUBXIBQkpRkF86FJiefahfJ1hgxJOYarYDR1SqK5SpEvTU+AzxZklXU3Ki/qSDRXh75CI4cQHQuRb70Ng7lRYWK5JiG37sH11tMv5jFHNKR0H5qSSC4ooIQunE/+AQ+uiidyXcBFU5hRhVFHIrkKkbsLonSlF+af5DiulqUwg6pFkxLHNXQq0uxib+ppASkWAkr4Bg50IsRSmDGlo6lDE8ZViPzaGfh68k3iCghCqGYh5hqWbziFsdK+0BQu/ebqLQ6gnAAzVy2YC3TSzVKY4ex8HgbjDJmL1UtkjAk5srnCaAbhWugTU1imGQZbkCAuTXNdL42fdDE8ZMKB4D6OCnlZ5DCq9WhKQriWaZkrTQARB8wW0qVgmI9uFLn2+VnxazzWL0kI13x1wUszFQARM10vmasajRW5TtSw4tew9I8wzmCpAWWgxJWO+06/56ETnKgbUQWLHMZH2LIEcBWhkHYZSA2Vjbno9yBEDVELUb55kSMNzTefqx2tVydL5mSZ+BsdbxUIcbLQHMG8yOFH7aZzLUBhdcxgXL1FLrBgC7qdcd2DBPMiRwgtMJtrKMy7NAdXqJYA5YhcEEdqsBuyAZZvauQwmwuCvPbgaqFcmDFNcsOxjGs0cgvmRY58NN1kLgjy2gsY1X4CRC1Ck9cJTqFRPkGUCZEjgBaYy9WO3JqZS/AiwgV2kszmRsuVXLejPMHEmmOquVzqqBEQQJjCi4HSKRdNyqOVXCdlR8w2xRFLTeSCSj46avSWsDBXOh1r6eQoGsVx2o6YZkZEXGIm1yTkV8VCikW4crD/iXhedEMk1xso38TIgTrM5CpCglYs9KNRJzBXNvY/isdqXhYRg3ojR289K1NTzOOagnxaGTnsQzNuxFwC9coAxvPL2Yul5gqdkSMnTUeNOMk8rsLoNV7qhTXgcpSL/i5g3DloYRTXDFQrRY44BgvE91QBFZrH1YECGnVhPhr1BLcctQiysHcGERelsSxy9I7DFT/HpaMi41ys5NVKXdV4JC2E/MWk5qIpLF9fqNfzdHM9Mo1rCfJqBI18tI/DXO5IruobVFxPoKC+UF+mI8fVonazuOaj9AhziQAkQIylfWZiYZ7pBhiEVLI/azqcnmLLi5aZxNWOghqVhhfto2kXSUShmFwL1QZL1wLQk+MEtMAwF3ND9axrDlou2iJMD4SpTejkSxXqvVG5OV0LgM2rDWZmTmcprzZXBRo1lpYTUkDM91L3vFGD66QqN6dpDTVdD5WCyCgXi4bq6YkfvSF1eY54hHIFGZd6hDGDZWolM13VsR+VmsK1DOWrUzKr2sciFKKO2cK4dBgsW2MosRFoMHBw+mpDtbnC6B42dvIpkJ/iAZemwfIiDZajUVbpqo4FtMQMrqEIaUy7alkVuFw0RZD6o/+EJtdCVB1psDQN0+irjlGRGVzTUa3GEpSPrWGMRbSuXU073nIDp6kTyB9psGx1vciqY4OBg9MV5dXm8irXMG6nde2NdJx5JS51lVgRkcNy1D6nb15di6YY4WI1r9pcbnRSWSb5MNFyOoJC93DauhEFI4qONHVU77I6TmNTlVLjXLBuGF0ZgqoheTG9QQwmVYo+NRILMFTXy5G1LGKIMW4N5ZzHVIU7v+GVKVB5I4uKsfvwCOOQjwZEQNbUaGJXNg9LV03JuqyOA2yqssA4VyHyRpmLRUPW41FB4DpBHbFlIRfXE3PYXQpocOVobkw6j4DInV/2KpPcEEXZZOGoFmHtDOqIYWDW1th7kF9pkZzo8BdgqUDNxQ665xvnQki9Ra1CXbOfHBV+cewoGvDViZnZFeUpPDGTMTIutcFUBaUfGeBi67yqZlvQcnWX//xbbjk1x1sxueAMCPbM/QIio4qrLFODK10REI1yTVIUh5lsqr+WU+mWY43cDSQw5Llig92OakLMJGnsR7ZuEuWc7DVFQJxulKuQrV+zVtE+l9boaeSeGIUDfuhFjkk9xNyKLJXNfmQtaBssneXrACo0yMXCBuuEkIeejWWRk2SS+UNsLkCXwdLYiE1jA1hlMMbMAuISo1zzkdoNa9F7rpiRwQ1udpzrQqMpmJiTAyyMML8kL6q5KuTAsb7IKBdar9qHLNSgtYxLHT/gjB5xwPJlmrTIEkNg6q3i8sqH3B0GuUqRW5WUBXSP1dVFt08IwtNcV5qBZDD2rjnM1cXXVFwt6expkUGu6ShPVQx40bOMS2sEvSX8losH5pVo0pT2AUqm9Gguf4AFeoNcrIpiHfCj5YxLS2OPHefigfnCYucZShoLiCwkKrlqy1igbzfGtQQJqigfRC8yLm2wH0p0g/XOZBzs5lHmKK6gkClzlRrjKkIBVS3gG2UFrq6Vy8XR7TJYWrrAnrYQSAYaxRW+Xg70041yqYZXBVqNuQyKgZWllSnAylQGY1zufPkAKjTGNRWphlceGoa5TASjIyxEwAKCUpkR9aG/NkcO9Aa5UFA1vPzoPas1izMTjBgpHMZgUhTxsuDPuILZMtcCg1xulr1Y2DCHi7sB18BMtQBGRSmx0pRc+Ug2YH6RIa4pMhfLmNXIapSL1cBKsFCNEswtlSGMy4u8UuDIM8ZVyrY3CJLQ6iguV/mmKl6puoc2bdQHFmkxr08B5vdGGSxTCCO54vB2GOTKiw4bYXRCybVx0328pu4r1wHWmh8B5leAVQSjqym4pe6AnMDM4WJhIx+HQ+tADquxqh4I6uvqqlZWMT20qJmQNY+PEzUb6ni+BVUrjBSsZpRzvFHlL2TOmrKEcbmhigJBt8rrcOerGso39ohW+ZiHeEy8KTaZa1M9oV+NFEYKVTPz5ddEJecyCFkh8bd0s7hYtYGeJlyNQHVf1SqJRI32p+auvLGBUI37U48ezyrBwj4GVp0fmZwDcE/lqQpqN4crWx5eyIoFd/uh8T261hhMtknTWNic9VXlcFIUmGLAtVSHmMEIlx/5cySuUnO4WLujgKrfovqHGnrEFfFULS4ca+ZiKjVYiwwW8vlZciZcXjRHiiPrjXKp1gBWA1ZzXWsPXfrTpvFaXGAspbGX44kma0ECqxWXvTPlhQDkk35xm8LFwnwQuMrnjm8t18dl5WKoMuK0YUowN6quECtst9Jg6XhwV1wvck03g4stFCF0u7XcOlnkmrEdq+hhrD4a+mDzK5pUP1kcdeLbBw++/fbXd2KNqkWvveauoIQsOROuIMoXIYOFpnCly5MUSF+gVeX/+qBPkh6N3HVLNNS9w/VcePBtBGNNkZzTcM1dK/7sN5fLK3ItnJUqKikJvvQZiTVRoUOHjq5b10bOGHlaSfXKYjhKNXzdupFMw7HJ3j4I7whvSXXwa9E9e4tceQBKHzEZ5cqPrOb9lGstwRq++Ju7vu3Vq9fYS8c/ca6vqHNbtw4YcKoX1keTNly9jpy48xFqtN8vHkk6POvdmyb9sZdCAwYMOLztb3/b9qYgDJsx49Oih/v8hZz4tY/FeuAKIUTzWQ5wGZ+nsLRcS7k2k0Z34h6dOnzuoeZFo/tG6qq+5w7Di4Mv+2h4KlHbzsWLX5eMvPPUZZddRnm2bj3X9ypRAPaGILzZH2vbhjZiMWXoIEuXXuKI2SZzBTHXye19SFc/6kU1tkePAey+g06dGrAVenuuF3T/QwCL0r2DLwMN7nX4HMCcG8B0eH9RTQi4CNhEarDX8mRPJJHEjx0xU/AmgKsPHQATD/cFlwOt/VMV9kMg+V+s/v2ptQYc7nsY+n8kGmv4FZdhneoLZ50Ci52T/Bfe7VPke6O/CNZGh++d0po33RQdxI7Y2ywuFuYJF+nft5cNoP5zH7+o70f77zqy436iHUeObPjwj1dtBav0wgCkfyM/GDRoUB/iiN9QrK1XXNZrwLm+z+y/6667NoBG7N/Wv/8P2e5/7P9fCvY45XotX1zToX+P6iOLxInkugosNBrUd+u85OSzO3Yc+TnV/Ud2bDibvOb9vkCFhfs3fKAVRMflNnoYqM+d+yL58/t37NggXXhk8cfvP7P/uV8Tu18tcolVR4A4TTUKQzrLNso1H0VwhRVcRw+PxroKoEAHRuz45OzPb8IiXHBo3tYBg7F2wsmvW60lWY3WtfjC/oOpel31BZx1dsOOHfffRHVkx8evw6HPnyNkS0Q/RNLfQKeTAZYv5KQJRrmKIrm8wLVa5Hp88IDR74z+4xp64pZrRqxJ/uRqrGt2jLiDboh5/7DE9VlJMYT5nq7fMK7D9NLf3THiyN1nr74J/7djx+Kb8LE1nxc+9+tfP0e5fD5pMb8Ml8VAmQ2AYZO5gozriiuuGLBCOvGFDRuWTfo5xrppxIgRO8SjX5yCk0ZiNxQXgE9jP7wC6zAYi2jDNdOTX5BuyOuf0IOPfb7/uSWiHyJSdcgFTzXdrW8qF9wttM864x+Ui2jF82uS6X2/ZsSIO0AjRlxzhzTpex6fgd1woPQQGn75mF4nNbEFLrzj6nffxVjXfCNduOLUVmKvt8FcSLmVvRrvOTDMtQsFlFx+BJ5oLV+VBHr8R5IeGLzieQC7BgS2gq/UC9fMG4xffQbO/aySE7UzKWknueiKFWvENj65g1yHL90wAdtq3oof0QtBH/zw5moAq1BU+3kmcBUiIYqrGr1IuUbKXJdz3APwbcBdVN+umAe6Vnr19aSk4QOLJa7TcOVH4iuD51Fbt28AMtAdd32x4oorr5Qu/Ba3Mmiy1bp8HwoqJtTuhHAFYX2j3yzc5OWSoLu/vDym2pKSPrbKXBz8+s2VUbr8f6jgJ467XDr6DW7kX5NxOboPjCQqhGpM4QpHcdWiG60QOUCvcHq0C85cmMXWpe4F893C6RCcCdpebgW9h3wh9nQ7xLgMPodlXCE/Lnw/wE2e5vQI5lq/sTYyrt/DlYt1XglaCPYCnUButgCSb5yrNJoLb96AugFajHfb2Xh6xFo5UD7QE2zdpufKXaSNjZRr7Sh5n3qYIm4zyOWP4vKi1dBMG25050+4eLqFmCurpIQd+kyXwV453UbDRj8r0dPME6urBdC3BrjkByosLwsCJDDrxvfoVH746Ti9mwXGWWgtnqzgKsbjZlec6+AcrHsnr9popRqG5kiRnkT9zw1wscI3W8r2wIXw+saLYsswFX4llq1O34vv+WZ4SHEpcMlai6PprMW/50DaizojxffeXr5qlVVUXRC1iDeXDLBSE/bbsPmXj2xms1qhwReLfiOvzdy7+CdKPQJz45HUooBlLa6M4CoGMHoZzKF3RVx4ejFcJkK9/sjAyatWlUtczUt9KCx2Yo5xrg4UwQVDFicwAtbPunbzB10vLbXdu9BqreT6YS6mSuvmPnEWsQZ9NhA3wrD68fzT0oJprQ++TDDGVRTJlUd2fVlBk1dNpi0ujAHX1ufej8Gy1saexZdGcvVstFq3D/rNLE2kPoMeASSqcmhCVAM/zir9YUUYx0Zz9unlCLJwAqOqLJGbXbh98yCFNm8/Kb2S1ZPbKHIxsCz62trt2wcptX27lamkstjKtJKvw7GjlkbEWuEtI1yskEpnXHkSVxbH5bqyrF0qCwooMFcUFxyLd50rF97dyjSNr4Kvt4s7vquFYYa5vJFcJIERiX3tWVzZaNVSY5aLlIWTGZeSzJUV47LK4p7iKVamcXwDfOXu8YXJk5aKNw3v781jXGy/DRFpXoIrdilVXCy/WHmpikvPZSCXgovn++E7OdZNHoq5W35tkAtWfCkXEzwBoyrmdKh4Feaq5C5AlVZF2Ggmnt/4iyAOihU1xrncdG2cyYfOh6v8UqzxF8KlGINj+EWEaxNPwdzPGOICqbhIYgbp2yRVQrDqGi+EqyQ6bFTCg87doeAcMNgSw1xIzfW0FBDjyoWpGhZt+uXlF8ClChsuF88vBWNBdXDCKFeHiqtW4opvhNxyjDWu2fXAL88fqzg6bFiLW3k+R6Bg24z/3Q3lKlPug6WKf8cJFt96+d85A1w0bIByq3iedKPWLZS9b4xrgcSVri44cuP1DHvhfXwz98ADF8Dliqo2wD/q+F+Iqzdu4fhjBv/+K0/kypYSs8xVrCMWjuH5Ku5HVxoLh3X8Stwc4xK8buE9A1xyYhaE6wOKfea6AmLWpZRrEwdLceevgYyrmW/A5qJcVBX+0DMGuKQExvYXs0LKWqkjdTXAxhwOsIyEw3Ke58dn9aRcIYEqlC98YYBrCgpSrkyJKyRzZcUdXaDx9XwDZyxsjAGuehx96yHMe//Qn+jD95+ZYYALEhjlYrEeuPQERCgMKRjcbGNhYxpfB1Yn+3TgSSX7dIAJawxwzUdpmKtMwTVKajA3zvCigui80VB1OI4vv6oT9CHh2mX8821YAhMCyoeWVh0BEfyQgc01Eg5heP3Pnr1nXnhhz28x13zjXOwT57JztLhcceMh1dw6I2FjPF/3O4Dq3HvmLI83fw01zMUSWCAd2FRcWXGG/sB+k6nOf4DlMq65fNWe5Bf27NmT3InLKNh6aNbnOVAuKYHVyFwDuYSpWDm8WjvP7Oncs3dNJw/Nm/B5RCyBBdIYVxC42JQ5Qaq0KpbYXFv27u08k7z3k6WYa0myeZ8rEshkXIhxFSeMq1GRve7jDne+kJy8t/PNdNx8kREuJoQDfU53TS5XwriUw4ufu7FnaWfn2eW7BXO4WKBPB66wmivrIgyvZl7S7oBJXCzQ49zsZ1yyEsXlUmav3hjqvU8gyJvFxQI9KChv4mBcuQniYll5JdS62b2X/uLs99mCqVzTxc2VaUjicree5wDLnTmzmLrXzJnnu2ZTBzWGcPyf+zvPDPn0LcK1yzAXC/QgISg94G2Zez4DrPjW6y4BvfTOO++8BD9dd9uvdGVlFuXBTsOG7Dnzwt49zxGuQnO4hopceUF5U0AzezoQt4e3XqLUj3/8s+suuS4umUsR5XEQBLLOPQf+KpjHxZ7tud1SWhb4Vr0D7FeXROq2U//5z8/g20wdw4u5obDsmDCk88++/cMEIWxOHcWegSG/tEMph2eO6NJvLCbsjV2m9Cg3fKvzU+HYp3n7Opfh4T3FVC4B5YnPHXw5PHPExq6MBcMptm7N1ZG9xuAVjeeG7B2yTDhe+P2ZvdP/6TXt80ULCZcXhcXnX0FhN9/QtSOO/fLLdyjVSy//FOvl2eSXr16lv71Eyd758ssnui4OF+GZyf4tZ/acPS4sG7K385PjLUXmctUiabnXL/Tmp8UpEd/56VNPfTV79ss/ferV2UQvvzr7kq+eeurVr/Avrz4FoLPh158+rXn5QJaUd5cJYQga+6Hl/d9gP/QXmsU1nXD5aqTh5RXKeL48XqQf+2+wzauzX3r0xyCIgi8B4+zrHiW/3PbS7JfhxX8Xx3PDaXg9voXEjbe+P1P48XeCMKfULK5SUtCjOdLwgq9L+blxI+KtNED87MdUj1731UvwC8GSRli8OQqNGvSh8v7OvdNRELiSzeJqR2R45UtbRuFrNl/fzxpnFTFXjBrERITnZyKgFPNj3JGeVklVYC76J7/H939/pvP7fcD1V6NcTJgriCqoG5KwmBdmBiuJNbmcKQLc9igFEqnixkOXPLrqd2djLj84Yufe5L17HgwKFX8wlSsd1YjFYTXdGrKUHxM3hc0Eh5PRsB5lVLFrjp4lkheOg5yMudxgrzcgGJ75PySEPzSPyw1FlLg7yU2zc7UvwPOtzGC687LSWPHMJa3GkwLOfz9YDCHh2GPmcb0B0RCFaNQg7hiGIJLO10tgleeZm69jVGrllsixcHdA3NKLtzZ07FmTDM4C5jKN683uIeSm5iLfj32Hg8hSiB16ljlm3npbRIV468xcPaUhrBKnS4U2/n8B+diM48kmcv3QXfxLxzD+2Lhj/ZOnwiDDYM0iWAnraQw2WXor+ZV4zVrmCvlQO+HqbybXl4KYvILIP+yjx5In0UFW9gsAM3mdI1eJxbj8dJKM/vyYmVy/daMKmrvmv08KYbAaAdvNjxPBKs3B6tkolbuQuULyeiWYazrh2p9sJpdA95F5fXTq0y7NWELe3Xxzq5kriVkMC9qT1pd9fjSfPoubYibX0PVkQ2PYJ660LvCFpCeGgd0QFdkQMyqXEkt4No9yoRrxH2AuLUo2k2sB3URWI2JNoTHk2DOr6RirH0/X6nuasmjIsI5vcwtibkEUqHSSmVyTqkWsDvr7Lj9A9X+//a8hMXiIlUejITCWueZSLOGjbdU0LQNXO+VKNpHrD27ihIA1lL75d589A1GpfR+mpeGeX8lih2GsMWIkPJa8DYVoOGQrNeZxPfaH4zgS4rFFRemmA5YksBidZrpMCIXTpHT8IXARh69OzL9Hv+Z3/3z2r/9Y0B7pmz5oknEBGIn3xQaxoNSVsN7E81k/ccP5Q03lYmRDH1PvjhWUXPU8P84YWM8sCWsprQlLyYIsKd2WJRvh0q+huyBARnAtaqg3BkaxWpt5cXCF55N5epBs45xycbjaOyhWyNviD1YjhLw8b22th0R24WmskkSMer6OcoV9kySuPLocn3iu6VM73vysf//vviuS9BZwWVvH8c1j1GD6sfrV4bgKXBirQ/wnGLEbTk8kF/PB+ap2rgIu6Ne0en4uBiu+ICcEHxwHBgcunCqn0FGcL4RIBZV4rnYYz5pctGuLyLbOC4iEY8hNIVyAVUgLAFQBpfYkA1yGtOU05QKTLeKbG5jF9Fur31y+nhYtfHY1kvJVRw0kL/j54nNNGHLzUafFksdb2SR32vkl6NxGMHQ99kHKdafFYn/85iHY5cENvag08VwHhoAmkB/vHvLkzYeOWkD2Jo9l6Ti2E3cciYuV+osnUmJU9ZPW4++0eDIcFtC6Q18HhTlLiEdAuwcmJIzLEim73ZNi69atm8fC1+XSFXUSPsBk5fjPiPSouMQK+xObx0v7d3Lr7rTAWzoLPB57VHNPJoxrncNms6V4sApsqd0keQ7ydTBKmMmaYeoCXdQz3yLRfa5krMqeXF2exSa9sc3mocLNWg4kjOtdC8CoZf8LX0VuvWyyKh7GS0mxjoiBz2yQnqThC6oIl1oey92JG1/aTdpf4zeRoVLJ9lws4vlF5ZXxdruthCJlpbS46iKeu4m3eLQayXAmLm7crd2k5U6+VexpI9szOA7IGrryxZKqeogX5ewP4IjKeVuGViOOowmM8212jRZtFj/fKHmWq4Q9QW3mISD0jEU1jSfxhbkglYv/2qHRiNNycwK5Jlo0ufLqOZCabPwiIJvbqoZqHA/Rol621UDlSGy+U7uRIQnketLi1OSCrdJKsoEsNGKr1M9t2Bjxqbj38cC7sh+zlVJVmlwey4QEcm2xpGi5iL8hKs4pxlm/lWA00H11VPU8hprWIEX24uioufE1LT+0tyWyjjpg0RrTFuRSlxGVCrQx08bxsuoXVbVK/gchUK1Zdq2w0ZYoP9xyMy6bHFpcD2tXSK6sEivzSFHymKokUDq5nNC089CTW7SC9BCiLrkmQNVHdSCS6clDTmDKKGiKyszOgia7g3Gp2YpdWWpVuoqVTLmRgMOhIY8tciAXWAoKcNXYNvHBuyM6NnGdRdTEu2NxDTkEfbcTOSzOA1FMKbglm6LIScmwkyoxA7gMKWqEHcTN454o4DIs5C6m4BYZ2wTocFOKDasAXnlXzUVPcjTJVZ8zwzlBySQdxoEj1eaRkEjd69ipVSPpxooenBYPbcJBZwsFTtyE7Js2D2N70JHC3MfmsKyboObass7iEU/C7BY4izExWcSbSdujss/SMoJurKgzb5FjbipzCVboKNmoD2bYxNMzLG13R3NNaHM45Zcd0HcsO2FisjU5lP7RJVdlrk4njC4iHxFdnfl7E6aQ7i9jE/tot8jQCjBOxFoHWFR2eIsmi8PT5IiOfQUWcOdUjZLUpt1fPcpVPYXZrJn7HepQ7AQnBRukAKAE1kRdkXEdkm9SCmClOOxO/EWV81OpzVXHb9F6npqra8lG5a8PWzTnDLYm1XHwJ1sBfIFo4pQtdlTJNUQmTnU4UlIyLE5bSop8jPW/m8ejWbs9wqllzdKDpT6pj12znE/1WFSs0NWkDLCDx9Ikdd8uRkVOnAizPjaBpTzdMFyBqv+2ggzNWvtjLS5rcXwsjWeBwzWnKZZudsYrGyclxUkMwV5LdVgOyFwPMgSPPQUs5SRcNjWXU11vxAr0cVd+AUsL3aLpEnYNLg/mAjoPGEzRSecEzBU1rbJDnsPvgJ02OkQkQZMQXtTKOKjJFc8TK9lynHY4ZEpp6qbmTQHfcgKIBzyMHW2yTBS5Dig8LgNcFieQAuBS+ZvDDq9rtWp5RftRcXHXWFoh82G4nVoh16bmstHg7/DYUpS2dFiGUK6jjBZ4bE58x2waXDYwYYFdc4BtVtW8JWxDWKw1m0bd1XyqxenR5koFA3lSUzIiDh8lXHdbmpRZMMlpceLvDlUDOJRAC1oDrE/E7KtSnlhmxR5bDEtVbahUYMfBL1WbC2J0QcRrdsuDwBU5CS4owLFHkyspxQPTL7tWu02WW6QPq8nCTEyVsRavWVSJl5VB9gInKX7UXEnwrQAKYmeE+7QBF3ZDJnyC3QavqblSYXg6Ugsc2o6ImVwDJ7tax49fWVW1skGaELtiLF43ApZeN7Q5SLlg0+DCYdEJt9wZmQAe5JInWJqUNknCNtPmojnNzoJMzBKxvLWqDh5nTVsJdMWai9fay9wQDTXfPQV8KyUWFz5so68xg3GQvBTHUsnPeKKgyQUGg6iYqlVyfMpJYuszc2GBZtEYqzoQZsXKbLMc2snLZrFrcuFDqbTfkQbjkg85lKSp9Ks2FzVYAfipSvaDnKY2NlTVPbQxMhAOjBn+P7XYtIJGKpirSZtL/jnqFW6CJUPJxRzS7lDFQ2IwYM5QO4sTag5dys1yxQz+vwfLqNQE3kOq2xRnF1xRrzm4yDV35mJa+YsYDG6DOjvjpgfpn/Rr65aDKhen99DpcHig7SQ1F7ND5Isp3LuiWVQQaq6kAnhzO8mPHhZrmE9/vgI+dehC9csHBj9ucaqxbKRIbyLmUnOldtOWk2vL0M0FQVIGK8hIiu6Cow0KznkrHrjyvJmufGDFPDZZYkpy2gHGCTNdaLdAY94HJowhzpISgzhDo1CzUTBoDUePpGhPvFn6zO5rwXB6zXTtiuelp4ZRDTptuCGbA7BAqVpc3WJzObVfSGXzFGV6w8pwOGwYzOOMJLPDDEHWmnnXXvv3K7sy0t+vXTFvDbvgQaW56KNYjIWXJFKoF6q5bLG5HLFewVzanohXFArwjYTQqySzaT3LeX4eAEYIcOZp7Ko+SszFqMAxcOgFY2EVJGlxOWNzsciq627YUojsFAw3aLM5nam0VYfTwCYQsexJTXU6bbgVbK1UMrQkL1SvA6TG5vJoH2dLu2pPpGAZgA4uQkT5bFCXXTDXREuKDXhkeRxOGgiJNB0OMmxsLhZmVHMeTWSp6Qx8fzOwxZg8MPO5QE1wRr0VnpjLWNpxz5HRBVfsoce41AYTS6pUuz1FKbv66Zv+J1ApSuHIlCFj2XR2kOn/ASBF85alK+BjAAAAAElFTkSuQmCC"}'; // normal login from mobile
            $json_arr   = json_decode($jsonInput, true);
            
            if (!empty($json_arr))
            {
                if ( $json_arr['user_id'] && strlen($json_arr['user_id']) > 0 )
                {
                    $user_id        = $json_arr['user_id'];
                    $userInfo       = $this->api_model->get_customer_by_id(trim($json_arr['user_id']));
                    if(empty($userInfo))
                    {
                        echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'User not found')); exit();   
                    }
                       
                    $userDetails                     = new stdClass();
                    $userDetails->customer_name      = $userInfo->customer_name;
                    $userDetails->mobile_number_1    = $userInfo->mobile_number_1;
                    if(!empty( $json_arr['name']))
                    {
                       $userDetails->customer_name  =  trim($json_arr['name']);
                    }
                    if(!empty( $json_arr['phone']))
                    {
                        $userDetails->mobile_number_1=  trim($json_arr['phone']);
                    }
                    if(!empty( $json_arr['photo']))
                    {
                        $userDetails->customer_photo_file = !empty($json_arr['photo']) ? $json_arr['photo'] : '';
                        $this->load->library('file_upload');
                        if($userDetails->customer_photo_file != '')
                        {
                            $filename                           = $this->saveImage($userDetails->customer_photo_file);
                            $userDetails->customer_photo_file   = $filename;
                        } 
                    }
                    
                    $userDetails_array                     = new stdClass();
                    $userDetails_array->customer_id  =  $user_id;
                    if(!empty( $json_arr['area_id']))
                    {
                       $userDetails_array->area_id  =  trim($json_arr['area_id']);
                    }
                    if(!empty( $json_arr['building']))
                    {
                       $userDetails_array->building  =  trim($json_arr['building']);
                    }
                    
                    if(!empty( $json_arr['unit']))
                    {
                       $userDetails_array->unit_no  =  trim($json_arr['unit']);
                    }
                    
                    if(!empty( $json_arr['street']))
                    {
                       $userDetails_array->street  =  trim($json_arr['street']);
                    }
                    
                    if(!empty( $json_arr['other_area_name']))
                    {
                       $userDetails_array->other_area  =  trim($json_arr['other_area_name']);
                    }
                    
                    if(count($userDetails) > 0)
                    {
                        $this->api_model->update_customers($userDetails,$user_id);
                        
                        $cust_address_Info       = $this->api_model->get_customer_addresses($userInfo->customer_id);
                        if(empty($cust_address_Info))
                        {
                            $this->api_model->add_customer_address_new($userDetails_array);
                        } else {
                            $this->api_model->update_customer_address_new($userDetails_array,$user_id);
                        }
                        echo json_encode(array('status' => 'success','response_code'=>'200','message'=>'Profile Updated Successfully')); exit();  
                    } else {
                        echo json_encode(array('status' => 'failure','response_code'=>'103','message'=>'Profile Not Updated ')); exit();  
                    }
                } else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '103';
                    $response['message']    = 'User ID missing!';
                    echo json_encode($response);
                    exit();
                }
            } else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
            }
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';
            echo json_encode($response);
            exit();
        }
    }
    
    protected function saveImage($base64img, $is_proof = FALSE)
    {
        $base64img = 'data:image/png;base64,'. $base64img;
        
        $base64img = str_replace('\r\n', '', $base64img);  
        $base64img = str_replace('%2B', '+', $base64img);  
        $base64img = str_replace(' ', '+', $base64img);  
        $extension = str_replace("image/", "", substr($base64img, 5, strpos($base64img, ';')-5));
        $base64img = str_replace(substr($base64img, 0, strpos($base64img, ',')+1), "", $base64img);

        $data = base64_decode($base64img);

        $photo_type = 'user_photo';
        $target_file_name = $photo_type . '_' . time() . '.' . $extension;            
        $tmp_upload_folder = $is_proof ? 'customer_img/' : 'customer_img/';
        $file = $tmp_upload_folder . $target_file_name;
        file_put_contents($file, $data);
        return $target_file_name;

    }
    
    public function extraservices()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            $getextradetails = $this->api_model->get_extra_service_details();
            
            $response = array();
            
            //$response['extra_service_data'] = array();
            $i = 0;
            foreach ($getextradetails as $details) {
				$photourl = strlen(trim($details->image_url)) > 0 ? base_url() . 'images/extra/' . $details->image_url : '';
                $response[$i]['extra_service_id'] = $details->id;
                $response[$i]['image'] = $photourl;
                $response[$i]['name'] = $details->service;
                $response[$i]['rate'] = "AED ".$details->cost." for ".$details->duration."m";
                //$response['maids'][$i]['photo'] = strlen(trim($maid->maid_photo_file)) > 0 ? base_url() . 'maidimg/' . $maid->maid_photo_file : '';
                $i++;
            }

            //echo json_encode($response);
            echo json_encode(array('status'=>'success','extra_service_data'=>$response)); exit();  
            exit();
            
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';
            echo json_encode($response);
            exit();
        } 
    }
    
    public function cleaning_services()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            $getcleaningservices = $this->api_model->getcleaningservices();
            
            $response = array();
            $i = 0;
            foreach ($getcleaningservices as $details) {
                $photourl = strlen(trim($details->photourl)) > 0 ? base_url() . 'images/services/' . $details->photourl : '';
                $response[$i]['service_id'] = $details->service_type_id;
                $response[$i]['details'] = $details->detail;
                $response[$i]['image'] = $photourl;
                $response[$i]['service'] = $details->service_type_name;
                //$response['maids'][$i]['photo'] = strlen(trim($maid->maid_photo_file)) > 0 ? base_url() . 'maidimg/' . $maid->maid_photo_file : '';
                $i++;
            }

            //echo json_encode($response);
            echo json_encode(array('message'=>'success','responce_code'=>'200','service_data'=>$response)); exit();  
            exit();
            
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';
            echo json_encode($response);
            exit();
        } 
    }
    
    public function maintenance_services()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            $getmaintenanceservices = $this->api_model->getmaintenanceservices();
            
            $response = array();
            
            $i = 0;
            foreach ($getmaintenanceservices as $details) {
                $photourl = strlen(trim($details->photourl)) > 0 ? base_url() . 'images/services/' . $details->photourl : '';
                $response[$i]['service_id'] = $details->service_type_id;
                $response[$i]['details'] = $details->detail;
                $response[$i]['image'] = $photourl;
                $response[$i]['service'] = $details->service_type_name;
                //$response['maids'][$i]['photo'] = strlen(trim($maid->maid_photo_file)) > 0 ? base_url() . 'maidimg/' . $maid->maid_photo_file : '';
                $i++;
            }

            //echo json_encode($response);
            echo json_encode(array('message'=>'success','responce_code'=>'200','service_data'=>$response)); exit();  
            exit();
            
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';
            echo json_encode($response);
            exit();
        } 
    }
    
    public function crew_in()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            $response[0] = "At home";
            $response[1]   = "Key is with security";
            $response[2]   = "Key under the mat";
            $response[3]   = "Buzz the intercom";
            $response[4]   = "Door open";
            $response[5]   = "Others";
            echo json_encode(array('status' => 'success', 'crew_detail' => $response));
            exit();
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';
            echo json_encode($response);
            exit();
        } 
    }
    
      /*
     * Author : Vishnu
     * Purpose : Forgot password api
     */
    public function forgot_password()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            if ($this->input->get('email') && strlen(trim($this->input->get('email'))) > 0) 
            {
                $email = trim($this->input->get('email'));

                if ($email) 
                {
                    $customer = $this->api_model->get_customers_by_field_value('email_address', $email);

                    if (!empty($customer)) 
                    {
                        $customer_id = $customer->customer_id;
                        $customer_details = $this->api_model->get_customer_by_id($customer_id);
                        $this->sendforgotmail($customer_details);

                        echo json_encode(array('status' => 'success' ,'response_code'=>'200','message' =>'We have send an email to  '.$email.' account . Please check the account for password. '));
                        exit();
                    } else {

                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '104';
                        $response['message'] = 'Invalid email address!';

                        echo json_encode($response);
                        exit();
                    }
                } else {

                    $response = array();
                    $response['status'] = 'error';
                    $response['error_code'] = '103';
                    $response['message'] = 'Parameter missing!';

                    echo json_encode($response);
                    exit();
                }
            } else {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '101';
                $response['message'] = 'Invalid request';

                echo json_encode($response);
                exit();
            }
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';
            echo json_encode($response);
            exit();
        }
    }
    
    /*
     * author : Vishnu
     * date   : 04/03/18
     */
    public function change_password()
    { 
        if($this->config->item('ws') === $this->input->get('ws'))
        { 
            $handle     = fopen('php://input','r');
            $jsonInput  = fgets($handle); 
            //$jsonInput  = '{"user_id":"1","old_password":"123","new_password":"12345"}'; // normal login from mobile
            $json_arr   = json_decode($jsonInput,true);
            
            if(!empty($json_arr))
            { 
                if($json_arr['user_id'] && is_numeric($json_arr['user_id']) && $json_arr['user_id']> 0 && $json_arr['old_password'] && strlen($json_arr['old_password']) > 0 && $json_arr['new_password'] && strlen($json_arr['new_password']) > 0 )
                { 
                    $customer_id    = trim($json_arr['user_id']);
                    $customer       = $this->api_model->get_customer_by_id($customer_id);
                    if(isset($customer) && isset($customer->customer_id) && $customer->customer_id > 0)
                    { 
                        $old_password = trim($json_arr['old_password']);
                        $new_password = trim($json_arr['new_password']);
                        
                        if($old_password == $customer->customer_password)
                        { 
                            $update_fields              = array();
                            $update_fields['customer_password']  = $new_password;
                            $affected = $this->api_model->update_customers($update_fields, $customer_id);			

                            if($affected > 0)
                            {
                                echo json_encode(array('status' => 'success', 'message' => 'Password Changed Successfully'));
                                exit();
                            } 
                            else 
                            {
                                $response           = array();
                                $response['status'] = 'error';
                                $response['message'] = 'Unexpected error!';
                                $response['error_code'] = '105';

                                echo json_encode($response);
                                exit();
                            }
                        }
                        else
                        {
                            $response           = array();
                            $response['status'] = 'error';
                            $response['message']= 'Invalid old password!';
                            $response['error_code'] = '104';

                            echo json_encode($response);
                            exit();
                        }
                    }
                    else
                    {

                        $response = array();
                        $response['status'] = 'error';
                        $response['message'] = 'Invalid user!';
                        $response['error_code'] = '103';

                        echo json_encode($response);
                        exit();
                    }
                     
                }
                 else 
                {
                    $response           = array();
                    $response['status'] = 'error';
                    $response['message']= 'Parameter missing!';
                    $response['error_code'] = '103';

                    echo json_encode($response);
                    exit();	 
                }
            }
            else
            {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();	
            }
        }
        else
        {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';

            echo json_encode($response);
            exit();	
        }
    }
    
       /*
     * Author   : Vishnu    
     * Date     : 04-03-18
     * Purpose  :Get FAQ details for customer app
     */
    public function getfaq() 
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {

            $faq[0]['question'] = "What if the specific Maid service I require is not listed?";
            $faq[0]['answer']   = "Please contact us with your request and we will endeavor to meet your requirements.";

            $faq[1]['question'] = "Can I cancel my maid service temporary?";
            $faq[1]['answer']   = "Yes. You can cancel your maid service at least two days notice.";
            
            $faq[2]['question'] = "Will I have the same Maid on every visit?";
            $faq[2]['answer']   = "If you are a regular customer ,you will have the same maid on each visit unless you are asking a replacement.";

            $faq[3]['question'] = "What Materials Do I Need To Provide?";
            $faq[3]['answer']   = "Vacuum cleaner , Mop , Floor Cleaner , General Purpose Polish, Multipurpose Cleaning Spray , Bleach , Limescale Remover , Micro Fiber Cloths , Furniture Polish.";
            
            $faq[4]['question'] = "Is my Maid employed by Spectrum Services?";
            $faq[4]['answer']   = "Yes, all of our maids employed by spectrum services.";

            $faq[5]['question'] = "Do I have to sign up to a lengthy contract?";
            $faq[5]['answer']   = "If you want to use, Spectrum Services regular basis, you have to sign a service agreement.";
            
            $faq[6]['question'] = "Is there a minimum number of hours per Maid visit?";
            $faq[6]['answer']   = "Yes. There is a Minimum of 2hr and 4hr, Depends on the areas.";
            
            $faq[7]['question'] = "Can I specify which day(s) my Maid visits?";
            $faq[7]['answer']   = "Yes";
            
            $faq[8]['question'] = "Which areas do you cover?";
            $faq[8]['answer']   = "We currently cover Downtown , Business Bay , Jumeirah 1 , Jumeirah 2 , Jumeirah 3 , Greens and Views , Meadows , Springs , Jumeirah Island , The Lakes , Hattan , JVC , JVT , Jumeirah Park , Arabian Ranches , Sports City , IMPZ , Remeram , Mudon , Mira , Arjan , Marina , JBR , JLT , Al Barsha Heights , Dubai Internet City , Dubai Media City, Emirates Hills, Emirates Living , Knowledge Village , Palm Jumeirah , Silicon Oasis , Sheikh Zayed Road , Discovery Gardens ,The Gardens ,Q Point ,Nad Al Sheba,Studio City ,Al Barari ,Dubai Investment Park (DIP),Al Quoz,Other";
            
            $faq[9]['question'] = "How Do I Pay ?";
            $faq[9]['answer']   = "You have two options: Paying cash to the crew member or paying online with your debit card or credit card. You can choose whichever is convenient to you.";
            
            echo json_encode(array('status' => 'success', 'faq' => $faq));
            exit();
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';

            echo json_encode($response);
            exit();
        }
    }
    
    public function service_time()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            if ($this->input->get('date') && strlen(trim($this->input->get('date'))) > 0) 
            {
                $servicedate = trim($this->input->get('date'));
                $s_hour = $this->input->get('hours');
                if ($servicedate) 
                {
                    $available_times = $this->api_model->get_available_times_new($servicedate);
                    
                    $times = array();
                    $current_hour_index = 0;
                    $time = '07:00 am';  
                    $time_stamp = strtotime($time);
                    $timingarray = array();
                    for ($i = 0; $i < 10; $i++)
                    {
                        //$selected = ($i == 0) ? 'class="selected"' : '';
                        $oneDimensionalArray = array_map('current', $available_times);

                        $time_stamp = strtotime('+60mins', strtotime($time));
                        $timess = date('H:i:s', $time_stamp);
                        $time = date('g:i a', $time_stamp);
                        
                        if( in_array( $timess ,$oneDimensionalArray ) )
                        {

                        } else {
                            if($servicedate == date('Y-m-d'))
                            {
                                $t_shrt = date('H:i:s', strtotime($time)); 
                                $cur_shrt = date('H:i:s');
                                $hours = ((strtotime($t_shrt) - strtotime($cur_shrt))/3600);
                                if($hours >= 2)
                                {
                                    $to_time = date('H', strtotime($timess.'+'.$s_hour.' hour'));
                                    if (((int) $to_time) <= 19 && ((int) $to_time) > 9) {
                                        
                                        $timingarray[] = $time;
                                    }
                                }
                            } else {
                                $to_time = date('H', strtotime($timess.'+'.$s_hour.' hour'));

                                if (((int) $to_time) <= 19 && ((int) $to_time) > 9) {
                                    $timingarray[] = $time;
                                }
                            }
                        }
                    }
                    
                    if (!empty($timingarray)) 
                    {
                        echo json_encode(array('status' => 'success' ,'booking_time_data' =>$timingarray));
                        exit();
                    } else {

                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '104';
                        $response['message'] = 'No shift available!';

                        echo json_encode($response);
                        exit();
                    }
                } else {

                    $response = array();
                    $response['status'] = 'error';
                    $response['error_code'] = '103';
                    $response['message'] = 'Parameter missing!';

                    echo json_encode($response);
                    exit();
                }
            } else {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '101';
                $response['message'] = 'Invalid request';

                echo json_encode($response);
                exit();
            }
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';

            echo json_encode($response);
            exit();
        }
    }
    
    public function price_calculation()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            $handle     = fopen('php://input','r');
            $jsonInput  = fgets($handle); 
            //$jsonInput  = '{"hours_no":"3","maids_no":"1","daycount":"1","cleaning_material":"N","extra_service":["1"],"service_type":"5","coupon_val":"","customer_id":""}'; // normal login from mobile
            //$jsonInput  = '{"hours_no":"3","maids_no":"1","daycount":"1","cleaning_material":"N","extra_service":["1"],"service_type":"5"}'; // normal login from mobile
            //$jsonInput  = '{"hours_no":"2","maids_no":"1","daycount":"1","cleaning_material":"N","service_type":"5"}'; // normal login from mobile
            $json_arr   = json_decode($jsonInput,true);
            
            if(!empty($json_arr))
            { 
                if($json_arr['hours_no'] && is_numeric($json_arr['hours_no']) && $json_arr['hours_no']> 0 && $json_arr['maids_no'] && strlen($json_arr['maids_no']) > 0 && $json_arr['daycount'] && strlen($json_arr['daycount']) > 0 )
                {
                    $no_hours = $json_arr['hours_no'];
                    $maids_no = $json_arr['maids_no'];
                    $cleaning_material = $json_arr['cleaning_material'];
                    $daycount = $json_arr['daycount'];
                    $interior_rate = $this->api_model->get_extraservice_rate(4);
                    $fridge_rate = $this->api_model->get_extraservice_rate(1);
                    $iron_rate = $this->api_model->get_extraservice_rate(2);
                    $oven_rate = $this->api_model->get_extraservice_rate(3);
                    $today_date = date('Y-m-d');
                    $vat_perc = 5;
                    
                    $fridge = 0;
                    $iron = 0;
                    $oven = 0;
                    $interior = 0;
                    if(!empty($json_arr['extra_service']))
                    {
                        $extra_service = $json_arr['extra_service'];
                        if (in_array(1, $extra_service)) {
                            $fridge = $fridge_rate->cost;
                        } else {
                            $fridge = 0;
                        }
                        if (in_array(2, $extra_service)) {
                            $iron = $iron_rate->cost;
                        } else {
                            $iron = 0;
                        }
                        if (in_array(3, $extra_service)) {
                            $oven = $oven_rate->cost;
                        } else {
                            $oven = 0;
                        }
                        if (in_array(4, $extra_service)) {
                            $interior = $interior_rate->cost;
                        } else {
                            $interior = 0;
                        }
                    }
//                    $vat =   $this->api_model->get_vat_rate(1);
//                    $vat_perc = $vat->cost;
                    $vat_percentage = 5;
                    
                    $get_fee_details = $this->api_model->get_fee_details();
                    
                    if($no_hours == 2)
                    {
                        if($cleaning_material == "N")
                        {
                            $per_hour_rate = $get_fee_details[0]->price_n;
                        } else {
                            $per_hour_rate = $get_fee_details[0]->price_c;
                        }
                    } else {
                        if($cleaning_material == "N")
                        {
                            $per_hour_rate = $get_fee_details[1]->price_n;
                        } else {
                            $per_hour_rate = $get_fee_details[1]->price_c;
                        }
                    }
                    
                    $service_rate_totals = ((((($no_hours * $per_hour_rate) * $maids_no)) + $interior + $fridge + $iron + $oven) * $daycount);
        
                    //$total_service_rate = ($service_rate - (($coupon_percentage/100) * $service_rate));

                    //$vat_charge = ($total_service_rate*($vat_percentage/100));
                    $vat_charge = ($service_rate_totals*($vat_percentage/100));

                    //$gross_amount = ($total_service_rate + $vat_charge);
                    $gross_total_amount = ($service_rate_totals + $vat_charge);
                    $discount = 0;
                    $coupons_id = 0;
                    $coupon = $json_arr['coupon_val'];
                    
                    $price_array_inv = array();
                    $price_array_inv['vat_charge'] = $vat_charge;
                    $price_array_inv['service_rate'] = $service_rate_totals;
                    $price_array_inv['discount'] = $discount;
                    $price_array_inv['gross_amount'] = $gross_total_amount;
                    $price_array_inv['hour_rate'] = $per_hour_rate;
                    $price_array_inv['vat_percentage'] = $vat_percentage;
                    $price_array_inv['coupon_id'] = $coupons_id;
                    $price_array_inv['coupon_val'] = $coupon;
                    
                    if($json_arr['coupon_val'] != "")
                    {
                        $coupon = $json_arr['coupon_val'];
                        $get_coupon_id = $this->api_model->get_coupon_id($coupon);
                        $weekday = date('w', strtotime($today_date));
                        
                        if(!empty($json_arr['customer_id']))
                        {
                            $customer_id = $json_arr['customer_id'];
                            $check_coupon_used = $this->api_model->check_coupon_used($customer_id,$get_coupon_id->coupon_id);
                            if(!empty($check_coupon_used))
                            {
                                echo json_encode(array('status' => 'success','response_code'=>'200','promo_status'=>'not','message'=>'Already Used!','price'=>$price_array_inv));
                                exit();
                            }
                        }
                        if(empty($get_coupon_id))
                        {
                            echo json_encode(array('status' => 'success','response_code'=>'200','promo_status'=>'not','message'=>'Coupon Code Invalid!','price'=>$price_array_inv));
                            exit();
                        } else {
                            $coupons_id = $get_coupon_id->coupon_id;
                            if($coupon == "Spec30")
                            {
                                if(!empty($json_arr['customer_id']))
                                {
                                    $customer_id = $json_arr['customer_id'];
                                    $checkbooking = $this->api_model->customer_new_booking($customer_id);
                                    if($checkbooking != 0)
                                    {
                                        echo json_encode(array('status' => 'success','response_code'=>'200','promo_status'=>'not','message'=>'Coupon Code Invalid!','price'=>$price_array_inv));
                                        exit();
                                    }
                                }
                                if($weekday == 4)
                                {
                                    echo json_encode(array('status' => 'success','response_code'=>'200','promo_status'=>'not','message'=>'Coupon Code Invalid!','price'=>$price_array_inv));
                                    exit();
                                } else if($weekday == 1 || $weekday == 2)
                                {
                                    $percentage = 30;
                                    $vat_perc = 5;
                                    $discount = (($percentage/100)*$service_rate_totals);
                                    $getfee = ($service_rate_totals - (($percentage/100)*$service_rate_totals));

                                    $vat_charge = ($getfee*($vat_perc/100));
                                    $gross_total_amount = ($getfee + $vat_charge);
                                    
                                } else {
                                    $percentage = 25;
                                    $vat_perc = 5;
                                    $discount = (($percentage/100)*$service_rate_totals);
                                    $getfee = ($service_rate_totals - (($percentage/100)*$service_rate_totals));

                                    $vat_charge = ($getfee*($vat_perc/100));
                                    $gross_total_amount = ($getfee + $vat_charge);
                                }
                            } else {
                                $percentage = $get_coupon_id->percentage;
                                if($get_coupon_id->offer_type == 'F')
                                {
                                    if($get_coupon_id->discount_type == 0)
                                    {
                                        $v_week_day = $get_coupon_id->valid_week_day;
                                        $weekArray = explode(',', $v_week_day);
                            
                                        if( in_array( $weekday ,$weekArray ) )
                                        {
                                            $discount = (($percentage/100)*$service_rate_totals);

                                            $getfee = ($service_rate_totals - (($percentage/100)*$service_rate_totals));

                                            $vat_charge = ($getfee*($vat_perc/100));
                                            $gross_total_amount = ($getfee + $vat_charge);
                                        } else {
                                            echo json_encode(array('status' => 'success','response_code'=>'200','promo_status'=>'not','message'=>'Coupon Code Invalid!','price'=>$price_array_inv));
                                            exit();
                                        }
                                    } else {
                                        $v_week_day = $get_coupon_id->valid_week_day;
                                        $weekArray = explode(',', $v_week_day);

                                        if( in_array( $weekday ,$weekArray ) )
                                        {
                                            $discount = $percentage;

                                            $getfee = ($service_rate_totals - $discount);

                                            $vat_charge = ($getfee*($vat_perc/100));
                                            $gross_total_amount = ($getfee + $vat_charge);
                                        } else {
                                            echo json_encode(array('status' => 'success','response_code'=>'200','promo_status'=>'not','message'=>'Coupon Code Invalid!','price'=>$price_array_inv));
                                            exit();
                                        }
                                    }
                                } else {
                                    $v_week_day = $get_coupon_id->valid_week_day;
                                    $weekArray = explode(',', $v_week_day);
                                    if( in_array( $weekday ,$weekArray ) )
                                    {
                                        if($cleaning_material == "N")
                                        {
                                            $per_hour_rate = $get_coupon_id->percentage;
                                        } else {
                                            $per_hour_rate = ($get_coupon_id->percentage + 10);
                                        }
                                        $service_rate_f = ((((($no_hours * $per_hour_rate) * $maids_no)) + $interior + $fridge + $iron + $oven) * $daycount);

                                        $discount = ($service_rate_totals - $service_rate_f);
                                        $getfee = ($service_rate_totals - $discount);

                                        $vat_charge = ($getfee*($vat_perc/100));
                                        $gross_total_amount = ($getfee + $vat_charge);
                                    } else {
                                        echo json_encode(array('status' => 'success','response_code'=>'200','promo_status'=>'not','message'=>'Coupon Code Invalid!','price'=>$price_array_inv));
                                        exit();
                                    }
                                }
                            }
                        }
                        $msg = "Coupon added successfully";
                        $promo_stat = "applied";
                    } else {
                        $coupon = "";
                        $msg = "Invalid Coupon";
                        $promo_stat = "not";
                    }
                    
                    $price_array = array();
                    $price_array['vat_charge'] = $vat_charge;
                    $price_array['service_rate'] = $service_rate_totals;
                    $price_array['discount'] = $discount;
                    $price_array['gross_amount'] = $gross_total_amount;
                    $price_array['hour_rate'] = $per_hour_rate;
                    $price_array['vat_percentage'] = $vat_percentage;
                    $price_array['coupon_id'] = $coupons_id;
                    $price_array['coupon_val'] = $coupon;
                    
                    echo json_encode(array('status' => 'success','response_code'=>'200','promo_status'=>$promo_stat,'message'=>$msg,'price'=>$price_array));
                    exit();
                } else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '103';
                    $response['message']    = 'Parameter Missing';
                    echo json_encode($response);
                    exit();
                }
            } else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
            }
            //hours_no,mids_no,service_type,cleaning_material,extra_service:[{extra_service_id}],book_type,date_time:[{date,time}]
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';

            echo json_encode($response);
            exit();
        }
    }
    
    public function maint_price_calculation()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            $handle     = fopen('php://input','r');
            $jsonInput  = fgets($handle); 
            //$jsonInput  = '{"coupon_val":"SpecM15","customer_id":"279"}'; // normal login from mobile
            //$jsonInput  = '{"hours_no":"3","maids_no":"1","daycount":"1","cleaning_material":"N","extra_service":["1"],"service_type":"5"}'; // normal login from mobile
            //$jsonInput  = '{"hours_no":"2","maids_no":"1","daycount":"1","cleaning_material":"N","service_type":"5"}'; // normal login from mobile
            $json_arr   = json_decode($jsonInput,true);
            
            if(!empty($json_arr))
            { 
                if($json_arr['coupon_val'] && strlen($json_arr['coupon_val']) > 0 )
                {
                    $coupon = $json_arr['coupon_val'];
                    $get_coupon_id = $this->api_model->get_coupon_id_maint($coupon);
                    $price_array_inv = array();
                    $price_array_inv['discount'] = 0;
                    $price_array_inv['coupon_id'] = 0;
                    $price_array_inv['coupon_val'] = "";
                    if(empty($get_coupon_id)){
                        echo json_encode(array('status' => 'success','response_code'=>'200','promo_status'=>'not','message'=>'Coupon Code Invalid!','price'=>$price_array_inv));
                        exit();
                    } else {
                        if(!empty($json_arr['customer_id']))
                        {
                            $customer_id = $json_arr['customer_id'];
                            $check_coupon_used = $this->api_model->check_coupon_used($customer_id,$get_coupon_id->coupon_id);
                            if(!empty($check_coupon_used))
                            {
                                echo json_encode(array('status' => 'success','response_code'=>'200','promo_status'=>'not','message'=>'Already Used!','price'=>$price_array_inv));
                                exit();
                            } else {
                                $price_array = array();
                                $price_array['discount'] = $get_coupon_id->percentage;
                                $price_array['coupon_id'] = $get_coupon_id->coupon_id;
                                $price_array['coupon_val'] = $coupon;
                                echo json_encode(array('status' => 'success','response_code'=>'200','promo_status'=>'applied','message'=>'Coupon added successfully','price'=>$price_array));
                                exit();
                            }
                        }
                        $price_array = array();
                        $price_array['discount'] = $get_coupon_id->percentage;
                        $price_array['coupon_id'] = $get_coupon_id->coupon_id;
                        $price_array['coupon_val'] = $coupon;
                        echo json_encode(array('status' => 'success','response_code'=>'200','promo_status'=>'applied','message'=>'Coupon added successfully','price'=>$price_array));
                        exit();
                        
                    }
                } else {
                    $price_array_inv = array();
                    $price_array_inv['discount'] = 0;
                    $price_array_inv['coupon_id'] = 0;
                    $price_array_inv['coupon_val'] = "";
                    echo json_encode(array('status' => 'success','response_code'=>'200','promo_status'=>'not','message'=>'Coupon Code Invalid!','price'=>$price_array_inv));
                    exit();
                }
            } else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
            }
            //hours_no,mids_no,service_type,cleaning_material,extra_service:[{extra_service_id}],book_type,date_time:[{date,time}]
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';

            echo json_encode($response);
            exit();
        }
    }
    
    public function add_booking()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            $handle     = fopen('php://input','r');
            $jsonInput  = fgets($handle); 
//            $jsonInput  = '{"customer_id": "3",
//                            "address_id":"20",
//                            "service_type": 1,
//                            "instructions": "test",
//                            "maid_count": 2,
//                            "hour_count": 4,
//                            "cleaning_material": "N",
//                            "price_per_hour": "35",
//                            "discount": "0",
//                            "coupon_id": "0",
//                            "service_charge":"280",
//                            "vat_charge":"14",
//                            "total_amount":"294",
//                            "crew_in":"ddddddh",
//                            "schedule_list": [
//                                {
//                                    "time": "2:00 pm",
//                                    "date": "2018-02-07"
//                                },
//                                {
//                                    "time": "2:00 pm",
//                                    "date": "2018-02-08"
//                                }
//                            ],
//                            "extra_service_id":[1,2]
//                        }'; // normal login from mobile
            $json_arr   = json_decode($jsonInput,true);
            
            if(!empty($json_arr))
            { 
                if($json_arr['customer_id'] && is_numeric($json_arr['customer_id']) && $json_arr['customer_id']> 0)
                {
                    $customerid = $json_arr['customer_id'];
                    $getaddressbycustomer = $this->api_model->get_address_details($customerid);
                    $address_id = $getaddressbycustomer->customer_address_id;
                    $service_id = $json_arr['service_type'];
                    $instructions = $json_arr['instructions'];
                    $no_of_maids = $json_arr['maid_count'];
                    $hour_count = $json_arr['hour_count'];
                    $cleaning_material = $json_arr['cleaning_material'];
                    $service_cost = $json_arr['service_charge'];
                    $coupon_id = $json_arr['coupon_id'];
                    $discount = $json_arr['discount'];
                    $vat_cost = $json_arr['vat_charge'];
                    $booked_date = date('Y-m-d H:i:s');
                    $crew_in = $json_arr['crew_in'];
                    $reference = "SM".$customerid.mt_rand(1000, 9999);
                    $fridge = 0;
                    $iron = 0;
                    $oven = 0;
                    $interior = 0;
                            
                    if(!empty($json_arr['extra_service_id']))
                    {
                        $extra_service = $json_arr['extra_service_id'];
                        if (in_array(1, $extra_service)) {
                            $fridge = 1;
                        } else {
                            $fridge = 0;
                        }
                        if (in_array(2, $extra_service)) {
                            $iron = 1;
                        } else {
                            $iron = 0;
                        }
                        if (in_array(3, $extra_service)) {
                            $oven = 1;
                        } else {
                            $oven = 0;
                        }
                        if (in_array(4, $extra_service)) {
                            $interior = 1;
                        } else {
                            $interior = 0;
                        }
                    }
                    $schedulelist = $json_arr['schedule_list'];
                    $schedule_count = count($schedulelist);
                    $serv_amt = ($service_cost / ($no_of_maids * $schedule_count));
                    $vat_amt = ($vat_cost / ($no_of_maids * $schedule_count));
                    $total_amt = ($serv_amt + $vat_amt);
                    foreach($schedulelist as $schedule)
                    {
                        $service_start_date = $schedule['date'];
                        $service_week_day   = date('w', strtotime($service_start_date));
                        
                        $time_from          = date("H:i:s", strtotime($schedule['time']));
                        //$no_hrs     = $schedule['hour_count'];
                        $to_time    = date('H:i:s', strtotime($time_from.'+'.$hour_count.' hour'));
                        $book_type  = 'OD';
                        $service_end= '1';
                        $is_locked  = 0;
                        
                        for ($m = 1; $m <= $no_of_maids; $m++) 
                        {
                            $booking_fields                         = array();
                            $booking_fields['reference_id'] = $reference;
                            $booking_fields['customer_id']          = $customerid;
                            $booking_fields['customer_address_id']  = $address_id;
                            $booking_fields['service_type_id']      = $service_id;
                            $booking_fields['service_start_date']   = $service_start_date;
                            $booking_fields['service_week_day']     = $service_week_day;
                            $booking_fields['time_from']            = $time_from;
                            $booking_fields['time_to']              = $to_time;
                            $booking_fields['booking_note']              = $instructions;
                            $booking_fields['booking_type']         = $book_type;
                            $booking_fields['service_end']          = $service_end;
                            $booking_fields['service_end_date']     = $service_start_date;
                            $booking_fields['service_actual_end_date']   = $service_start_date;
                            $booking_fields['price_per_hr']    = $json_arr['price_per_hour'];
                            $booking_fields['service_charge']    = $serv_amt;
                            $booking_fields['vat_charge']    = $vat_amt;
                            $booking_fields['total_amount']    = $total_amt;
                            $booking_fields['is_locked']            = $is_locked;
                            $booking_fields['no_of_maids']          = 1;
                            $booking_fields['no_of_hrs']    = $hour_count;
                            $booking_fields['cleaning_material']    = $cleaning_material;
                            $booking_fields['booked_from']          = 'M';
                            $booking_fields['booking_category']          = 'C';
                            $booking_fields['booking_status']       = 0;
                            $booking_fields['booked_datetime']      = date('Y-m-d H:i:s');
                            $booking_fields['payment_type']         = 0;
                            $booking_fields['interior_window_clean']         = $interior;
                            $booking_fields['fridge_cleaning']         = $fridge;
                            $booking_fields['ironing_services']         = $iron;
                            $booking_fields['oven_cleaning ']         = $oven;
                            $booking_fields['crew_in ']         = $crew_in;
                            $booking_fields['discount ']         = $discount;
                            $booking_fields['coupon_id ']         = $coupon_id;
                            
                            $booking_id = $this->api_model->add_booking($booking_fields);
                            
//                            if($booking_id > 0)
//                            {
//                                
//                            }
                            
                        }
                    }
                    
                    $coupon_fields = array();
                    $coupon_fields['customer_id'] = $customerid;
                    $coupon_fields['coupon_id'] = $coupon_id;
                    $coupon_fields['reference_id'] = $reference;
                    $coupon_fields['discount'] = $discount;
                    if($coupon_id > 0)
                    {
                        $update_coupon = $this->api_model->insert_coupon_for_customer($coupon_fields);
                    }
                    
                    $this->sent_booking_confirmation_mail($reference);
                    $this->sent_booking_admin_confirmation_mail($reference);
                    
                    echo json_encode(array('status' => 'success','response_code'=>'200','booking_id'=>$booking_id,'reference_id'=>$reference,'message' => "Booking added successfully."));
                    exit();
                } else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '103';
                    $response['message']    = 'Parameter Missing';
                    echo json_encode($response);
                    exit();
                }
            } else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
            }
            
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';

            echo json_encode($response);
            exit();
        }
    }
    
    public function contact_us()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            $handle     = fopen('php://input','r');
            $jsonInput  = fgets($handle);
            //$jsonInput  = '{"name":"vishnu","phone":"123456789","email":"vishnu.m@azinova.info","message":"Test message"}';
            $json_arr   = json_decode($jsonInput,true);
            if(!empty($json_arr))
            {
                if($json_arr['name'] && strlen($json_arr['name']) > 0)
                {
                    $name = $json_arr['name'];
                    $phone = $json_arr['phone'];
                    $email = $json_arr['email'];
                    $message = $json_arr['message'];
                    
                    $mail = $this->sent_main_contact_to_admin_mail($name, $phone, $message, $email);
                    //$mail = 0;
                    if($mail > 0)
                    {
                        echo json_encode(array('status' => 'success','response_code'=>'200','message' => "Enquiry submitted successfully."));
                        exit();
                    } else {
                        echo json_encode(array('status' => 'error','response_code'=>'200','message' => "Some thing went wrong try again..."));
                        exit();
                    }
                    
                } else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '103';
                    $response['message']    = 'Parameter Missing';
                    echo json_encode($response);
                    exit();
                }
            } else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
            }
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';

            echo json_encode($response);
            exit();
        } 
    }
    
    public function booking_history()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            $handle     = fopen('php://input','r');
            $jsonInput  = fgets($handle);
            //$jsonInput  = '{"user_id":"279"}';
            $json_arr   = json_decode($jsonInput,true);
            if(!empty($json_arr))
            {
                if($json_arr['user_id'] && strlen($json_arr['user_id']) > 0)
                {
                    $user_id = $json_arr['user_id'];
                    $userInfo = $this->api_model->get_customer_by_id($user_id);
                    if(empty($userInfo))
                    {
                        echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'Customer ID not found')); exit();   
                    }
                    $current_booking = $this->api_model->get_current_booking_mobile($user_id);
//                    echo '<pre>';
//                    print_r($current_booking);
//                    echo '</pre>';
                    //exit();
                    $i=0;
                    $book_cur_array = array();
                    foreach ($current_booking as $c_booking)
                    {
                        $online_pay = $this->api_model->check_online_pay_by_bookid($c_booking->booking_id);
                        if($c_booking->booking_category == 'C')
                        {
                            $date = $c_booking->service_date;
                            if($c_booking->cleaning_material == 'Y')
                            {
                                $clean_mat = "Yes";
                            } else {
                                $clean_mat = "No";
                            }
                            $no_of_maids = $c_booking->no_of_maids;
                            $total_rate = "AED ".number_format($c_booking->total_amount, 2);
                            $schedule = $c_booking->shift;
                            $status = $c_booking->booking_status;
                            $type = 0;
                            $book_cur_array[$i]['date'] = $date;
                            $book_cur_array[$i]['cleaning_mat'] = $clean_mat;
                            $book_cur_array[$i]['maids_count'] = $no_of_maids;
                            $book_cur_array[$i]['rate'] = $total_rate;
                            $book_cur_array[$i]['shift'] = $schedule;
                            $book_cur_array[$i]['status'] = $status;
                            $book_cur_array[$i]['type'] = $type;
                            $book_cur_array[$i]['service_type'] = $c_booking->service_type_name;
                            $book_cur_array[$i]['pay_status'] = 0;
                            if($online_pay->payment_status=='Success')
                            {
                                $book_cur_array[$i]['pay_status'] = '1';
                            }
                        } else if($c_booking->booking_category == 'M'){
                            $date = $c_booking->service_date;
                            $priority = $c_booking->priority_type;
                            $status = $c_booking->booking_status;
                            $time_type = $c_booking->time_type;
                            if($time_type == 'flexible')
                            {
                                $hours = "";
                            } else {
                                $hours = $c_booking->time_val;
                            }
                            $type= 1;
                            $rate = "";
                            $book_cur_array[$i]['date'] = $date;
                            $book_cur_array[$i]['hours'] = $hours;
                            $book_cur_array[$i]['priority'] = $priority;
                            $book_cur_array[$i]['rate'] = $rate;
                            $book_cur_array[$i]['status'] = $status;
                            $book_cur_array[$i]['service_type'] = $c_booking->service_type_name;
                            $book_cur_array[$i]['type'] = $type;
                        }
                        $i++;
                    }
                    
                    $previous_booking = $this->api_model->get_previous_booking_mobile($user_id,array('limit'=>'20'));
//                    echo '<pre>';
//                    print_r($previous_booking);
//                    echo '</pre>';
//                    exit();
                    $j=0;
                    $book_prev_array = array();
                    foreach ($previous_booking as $p_booking)
                    {
                        $online_pay = $this->api_model->check_online_pay_by_bookid($p_booking->booking_id);
                        if($p_booking->booking_category == 'C')
                        {
                            $date = $p_booking->service_date;
                            if($p_booking->cleaning_material == 'Y')
                            {
                                $clean_mat = "Yes";
                            } else {
                                $clean_mat = "No";
                            }
                            $no_of_maids = $p_booking->no_of_maids;
                            $total_rate = "AED ".number_format($p_booking->total_fee, 2);
                            $schedule = $p_booking->shift;
                            $status = $p_booking->booking_status;
                            $type = 0;
                            $book_prev_array[$j]['date'] = $date;
                            $book_prev_array[$j]['cleaning_mat'] = $clean_mat;
                            $book_prev_array[$j]['maids_count'] = $no_of_maids;
                            $book_prev_array[$j]['rate'] = $total_rate;
                            $book_prev_array[$j]['shift'] = $schedule;
                            $book_prev_array[$j]['status'] = $status;
                            $book_prev_array[$j]['type'] = $type;
                            $book_prev_array[$j]['service_type'] = $p_booking->service_type_name;
                            $book_prev_array[$j]['pay_status'] = 0;
                            if($online_pay->payment_status=='Success')
                            {
                                $book_prev_array[$j]['pay_status'] = '1';
                            }
                            if($p_booking->payment_status==1)
                            {
                                $book_prev_array[$j]['pay_status'] = '1';
                            }
                        } else if($p_booking->booking_category == 'M'){
                            $date = $p_booking->service_date;
                            $priority = $p_booking->priority_type;
                            $status = $p_booking->booking_status;
                            $time_type = $p_booking->time_type;
                            if($time_type == 'flexible')
                            {
                                $hours = "";
                            } else {
                                $hours = $p_booking->time_val;
                            }
                            $type= 1;
                            //$hours = "";
                            $rate = "";
                            $book_prev_array[$j]['date'] = $date;
                            $book_prev_array[$j]['hours'] = $hours;
                            $book_prev_array[$j]['priority'] = $priority;
                            $book_prev_array[$j]['rate'] = $rate;
                            $book_prev_array[$j]['status'] = $status;
                            $book_prev_array[$j]['type'] = $type;
                            $book_prev_array[$j]['service_type'] = $p_booking->service_type_name;
                        }
                        $j++;
                    }
                    echo json_encode(array('status' => 'success','response_code'=>'200','booking_data_current'=>$book_cur_array,'booking_data_past'=>$book_prev_array)); exit();
                    
                } else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '103';
                    $response['message']    = 'Parameter Missing';
                    echo json_encode($response);
                    exit();
                }
            } else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
            }
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';

            echo json_encode($response);
            exit();
        } 
    }
    
    public function maintenance_service_time()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            if ($this->input->get('date') && strlen(trim($this->input->get('date'))) > 0) 
            {
                $servicedate = trim($this->input->get('date'));
                //$s_hour = $this->input->get('hours');
                if ($servicedate) 
                {
                    //$available_times = $this->api_model->get_available_times_new($servicedate);
                    $available_times = array();
                    $times = array();
                    $current_hour_index = 0;
                    $time = '07:00 am';  
                    $time_stamp = strtotime($time);
                    $timingarray = array();
                    for ($i = 0; $i < 10; $i++)
                    {
                        //$selected = ($i == 0) ? 'class="selected"' : '';
                        $oneDimensionalArray = array_map('current', $available_times);

                        $time_stamp = strtotime('+60mins', strtotime($time));
                        $timess = date('H:i:s', $time_stamp);
                        $time = date('g:i a', $time_stamp);
                        
                        if( in_array( $timess ,$oneDimensionalArray ) )
                        {

                        } else {
                            if($servicedate == date('Y-m-d'))
                            {
                                $t_shrt = date('H:i:s', strtotime($time)); 
                                $cur_shrt = date('H:i:s');
                                $hours = ((strtotime($t_shrt) - strtotime($cur_shrt))/3600);
                                if($hours >= 2)
                                {
                                    //$to_time = date('H', strtotime($timess.'+'.$s_hour.' hour'));
                                    //if (((int) $to_time) <= 19 && ((int) $to_time) > 9) {
                                        
                                        $timingarray[] = $time;
                                    //}
                                }
                            } else {
                                //$to_time = date('H', strtotime($timess.'+'.$s_hour.' hour'));

                                //if (((int) $to_time) <= 19 && ((int) $to_time) > 9) {
                                    $timingarray[] = $time;
                                //}
                            }
                        }
                    }
                    
                    if (!empty($timingarray)) 
                    {
                        echo json_encode(array('status' => 'success' ,'booking_time_data' =>$timingarray));
                        exit();
                    } else {

                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '104';
                        $response['message'] = 'No shift available!';

                        echo json_encode($response);
                        exit();
                    }
                } else {

                    $response = array();
                    $response['status'] = 'error';
                    $response['error_code'] = '103';
                    $response['message'] = 'Parameter missing!';

                    echo json_encode($response);
                    exit();
                }
            } else {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '101';
                $response['message'] = 'Invalid request';

                echo json_encode($response);
                exit();
            }
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';

            echo json_encode($response);
            exit();
        }
    }
    
    public function maintenance_add_booking()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            $handle     = fopen('php://input','r');
            $jsonInput  = fgets($handle); 
//            $jsonInput  = '{"customer_id": "3",
//                            "service_type": 8,
//                            "priority": "normal",
//                            "date": "2018-03-12",
//                            "instructions": "test booking",
//                            "time_type": "specific",
//                            "time": "10:00 am"
//                        }'; // normal login from mobile
            $json_arr   = json_decode($jsonInput,true);
            
            if(!empty($json_arr))
            { 
                if($json_arr['customer_id'] && is_numeric($json_arr['customer_id']) && $json_arr['customer_id']> 0)
                {
                    $customerid = $json_arr['customer_id'];
                    $getaddressbycustomer = $this->api_model->get_address_details($customerid);
                    $address_id = $getaddressbycustomer->customer_address_id;
                    $reference = "SM".$customerid.mt_rand(1000, 9999);
                    $service_id = $json_arr['service_type'];
                    $service_start_date = $json_arr['date'];
                    $service_week_day = date('w', strtotime($service_start_date));
                    $time_type = $json_arr['time_type'];
                    $time_from = date("H:i:s", strtotime($json_arr['time']));
                    $booking_type = 'OD';
                    $service_end= '1';
                    $is_locked  = 0;
                    $booked_from = 'M';
                    $booking_status = 0;
                    $priority_type = $json_arr['priority'];
                    $instructions = $json_arr['instructions'];
                    $booked_date = date('Y-m-d H:i:s');
                    
                    
                    $booking_fields = array();
                    $booking_fields['reference_id']  = $reference;
                    $booking_fields['customer_id']  = $customerid;
                    $booking_fields['customer_address_id']  = $address_id;
                    $booking_fields['service_type_id']  = $service_id;
                    $booking_fields['service_start_date']   = $service_start_date;
                    $booking_fields['service_week_day']   = $service_week_day;
                    $booking_fields['time_type']   = $time_type;
                    $booking_fields['time_from']   = $time_from;
                    $booking_fields['booking_note']   = $instructions;
                    $booking_fields['booking_type']   = $booking_type;
                    $booking_fields['booking_category']   = 'M';
                    $booking_fields['service_end']   = $service_end;
                    $booking_fields['service_end_date']   = $service_start_date;
                    $booking_fields['service_actual_end_date']   = $service_start_date;
                    $booking_fields['cleaning_material']   = 'N';
                    $booking_fields['is_locked']   = $is_locked;
                    $booking_fields['booked_from']   = $booked_from;
                    $booking_fields['booking_status']   = $booking_status;
                    $booking_fields['booked_datetime']   = $booked_date;
                    $booking_fields['priority_type']   = $priority_type;
                    $booking_fields['coupon_id'] = $json_arr['coupon_id'];
                    $booking_fields['discount'] = $json_arr['discount'];
                
                    $booking_id = $this->api_model->add_booking($booking_fields);
                    
                    if($booking_id > 0)
                    {
                        $coupon_fields = array();
                        $coupon_fields['customer_id'] = $customerid;
                        $coupon_fields['coupon_id'] = $json_arr['coupon_id'];
                        $coupon_fields['reference_id'] = $reference;
                        
                        if($json_arr['coupon_id'] > 0)
                        {
                            $update_coupon = $this->api_model->insert_coupon_for_customer($coupon_fields);
                        }
                        $this->sent_maint_booking_confirmation_mail($booking_id);
                        $this->sent_maint_booking_admin_confirmation_mail($booking_id);
                        echo json_encode(array('status' => 'success','response_code'=>'200','booking_id'=>$booking_id,'reference_id'=>$reference,'message' => "Booking added successfully."));
                        exit();
                    } else {
                        $response               = array();
                        $response['status']     = 'error';
                        $response['error_code'] = '103';
                        $response['message']    = 'Some thing went wrong try again...';
                        echo json_encode($response);
                        exit();
                    }
                } else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '103';
                    $response['message']    = 'Parameter Missing';
                    echo json_encode($response);
                    exit();
                }
            } else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
            }
            
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';

            echo json_encode($response);
            exit();
        }
    }
    

     
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /*
     * Author : Azinova
     * Purpose : Send emails from api
     */

    function sendforgotmail($recepient_dtls) 
    {
        $this->load->library('email');
        $data['content'] = 'Dear '. $recepient_dtls->customer_name . ',<br><br>
                            We received a request to change your password on Spectrum Services.<br>
		            Your new password is <b>' . $recepient_dtls->customer_password . '</b><br><br>
                            Thanks,
                            <br /> Spectrum Cleaning Services';
        $html = $this->load->view('forgot_email_template', $data, True);
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        $this->email->from('info@spectrumservices.ae', 'Spectrum');
        $this->email->to($recepient_dtls->email_address);
        // $this->email->bcc('vishnu.m@azinova.info');
        $this->email->subject('Password Recovery');

        $this->email->message($html);

        $mail=//$this->email->send();
        return $mail;
    }
    
    function sent_registration_confirmation_mail($customer_id) {
        $this->load->library('email');
        $data['customer'] = $this->api_model->get_customer_by_id($customer_id);
        $name = $data['customer']->customer_name;
        $email = $data['customer']->email_address;
        $mobile = $data['customer']->mobile_number_1;
        $data['name'] = $name;
        $data['customer_name'] = $data['customer']->email_address;
        $data['password'] = $data['customer']->customer_password;
//        $data['verification_code'] = base64_encode($data['customer']->customer_id) . "/" . md5($email . $mobile);
        $html = $this->load->view('registration_template_email', $data, True);

        $config['mailtype'] = 'html';
        $config['charset'] = 'iso-8859-1';
        $this->email->initialize($config);

        $this->email->from('web@spectrumservices.ae', 'Spectrum');
        $this->email->to($data['customer']->email_address);
        $this->email->to("vishnu.m@azinova.info");
        $this->email->subject('Spectrum Registration');

        $this->email->message($html);

        //$this->email->send();
    }
    
    function sent_main_contact_to_admin_mail($customer_name, $customer_mobile, $message, $emailid)
    {   
        $this->load->library('email');            

        $c_message = 'Customer : <i>' . $customer_name . '</i>';
        $c_message .= '<br />Mobile : <i>' . $customer_mobile . '</i>';
        $c_message .= '<br />Email : <i>' . $emailid . '</i>';
        $c_message .= '<br />Message : <i>' . $message . '</i>';            
        $b_message = $c_message;
        
        $html = '<html>
                <head>
                </head>
                <body>
                    <div style="margin:0;padding:0;background-color:#f2f2f2;min-height:100%!important;width:100%!important">
                        <center>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;margin:0;padding:0;background-color:#f2f2f2;height:100%!important;width:100%!important">
                                <tbody>
                                  <tr>
                                    <td align="center" valign="top" style="margin:0;padding:20px;border-top:0;height:100%!important;width:100%!important"><table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;border:0">
                                        <tbody>
                                          <tr>
                                            <td align="center" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;background-color:#ffffff;border-top:0;border-bottom:0">
                                                <tbody>
                                                  <tr>
                                                    <td valign="top"></td>
                                                  </tr>
                                                </tbody>
                                              </table></td>
                                          </tr>
                                          <tr>
                                            <td align="center" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;background-color:#ffffff;border-top:0;border-bottom:0">
                                                <tbody>
                                                  <tr>
                                                    <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                                        <tbody>
                                                          <tr>
                                                            <td valign="top" style="padding:0px"><table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
                                                                <tbody>
                                                                  <tr>
                                                                    <td valign="top" style="padding-right:0px;padding-left:0px;padding-top:0;padding-bottom:0"><img align="left" alt="" src="'. base_url().'images/email_banner.jpg" width="600" style="max-width:1144px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;outline:none;text-decoration:none" class="CToWUd a6T" tabindex="0">
                                                                      <div class="a6S" dir="ltr" style="opacity: 0.01; left: 632px; top: 304px;">
                                                                        <div id=":1dx" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" title="Download" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V">
                                                                          <div class="aSK J-J5-Ji aYr"></div>
                                                                        </div>
                                                                      </div></td>
                                                                  </tr>
                                                                </tbody>
                                                              </table></td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                      <table width="600" cellspacing="0" cellpadding="0" border="0" align="left" style="border-collapse:collapse">
                               <tbody>
                               <tr>

                               <td valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left">

                               <br>
                                Dear Admin,<br>
                                <br>New Enquiry Received
                                <br>
                                <br> ' . $b_message . '<br>

                                <br>
                                    Thanks & Regards<br/>
                                    Spectrum Services<br />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                      <tbody>
                        <tr>
                          <td valign="top"><table align="left" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse">
                              <tbody>
                                <tr>
                                  <td valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left"><div style="text-align:center"><em style="color:#606060;font-family:helvetica;font-size:11px;line-height:15.6199998855591px;text-align:center">Copyright &copy; 2017 Spectrum Services.&nbsp;All rights reserved.</em><br style="color:#606060;font-family:Helvetica;font-size:11px;text-align:center;line-height:15.6199998855591px">
                                      <span style="color:#606060;font-family:helvetica;font-size:11px;line-height:15.6199998855591px;text-align:center">You are receiving this email because you signed up to Spectrum Services</span></div>
                                    </td>
                                </tr>
                              </tbody>
                            </table></td>
                        </tr>
                      </tbody>
                    </table>
                                                      </td>
                                                  </tr>
                                                </tbody>
                                              </table></td>
                                          </tr>

                                        </tbody>
                                      </table></td>
                                  </tr>
                                </tbody>
                              </table>
                        </center>
                    </div>
                </body>
                </html>'; 
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        $this->email->from('web@spectrumservices.ae', 'Spectrum');
        $this->email->to('vishnu.m@azinova.info');
        $this->email->subject('New Enquiry Recieved');

        $this->email->message($html);

        $mail=//$this->email->send();
        return $mail;
            //echo $this->email->print_debugger();
    }
    
    public function sent_booking_confirmation_mail($reference_id)
    {
        $this->load->library('email');
        $data['bookingdetails'] = $this->api_model->get_bookingdetails_by_refid($reference_id);
        $coupon_fee = $this->api_model->get_coupon_fee_refid($reference_id);
        if(!empty($coupon_fee))
        {
            $data['coupon_fee'] = $coupon_fee->discount;
        } else {
            $data['coupon_fee'] = 0;
        } 
//        echo '<pre>';
//        print_r($data);
//        echo '</pre>';
//        exit();
        
        $name = $data['bookingdetails'][0]->customer_name;
        $email = $data['bookingdetails'][0]->email_address;
        $mobile = $data['bookingdetails'][0]->mobile_number_1;
        $data['name'] = $name;
        $data['customer_name'] = $data['bookingdetails'][0]->email_address;
        $data['password'] = $data['bookingdetails'][0]->customer_password;
//        $data['verification_code'] = base64_encode($data['customer']->customer_id) . "/" . md5($email . $mobile);
        $html = $this->load->view('booking_confirmation_template_email', $data, True);

        $config['mailtype'] = 'html';
        $config['charset'] = 'iso-8859-1';
        $this->email->initialize($config);

        $this->email->from('booking@spectrumservices.ae', 'Spectrum');
        $this->email->to($email);
        //$this->email->cc("vishnu.m@azinova.info");
        $this->email->subject('Cleaning Booking Confirmation');

        $this->email->message($html);

        //$this->email->send();
    }
    
    public function sent_booking_admin_confirmation_mail($ref_id)
    {
        $this->load->library('email');
        $data['bookingdetails'] = $this->api_model->get_bookingdetails_by_refid($ref_id);
        $coupon_fee = $this->api_model->get_coupon_fee_refid($ref_id);
        if(!empty($coupon_fee))
        {
            $data['coupon_fee'] = $coupon_fee->discount;
        } else {
            $data['coupon_fee'] = 0;
        }
        $name = $data['bookingdetails'][0]->customer_name;
        $email = $data['bookingdetails'][0]->email_address;
        $mobile = $data['bookingdetails'][0]->mobile_number_1;
        $data['name'] = $name;
        $data['customer_name'] = $data['bookingdetails'][0]->email_address;
        $data['password'] = $data['bookingdetails'][0]->customer_password;
//        $data['verification_code'] = base64_encode($data['customer']->customer_id) . "/" . md5($email . $mobile);
        $html = $this->load->view('booking_admin_template_email', $data, True);

        $config['mailtype'] = 'html';
        $config['charset'] = 'iso-8859-1';
        $this->email->initialize($config);

        $this->email->from('web@spectrumservices.ae', 'Spectrum');
        //$this->email->to('booking@spectrumservices.ae');
        //$this->email->cc('pooja.nair@azinova.info,instantbooking@gmail.com');
        
        //$this->email->cc('pooja.nair@azinova.info');
        //$this->email->cc('instantbooking@gmail.com');
        
            $this->email->to("vishnu.m@azinova.info");
         $this->email->subject('Cleaning Booking Confirmation');

        $this->email->message($html);

        //$this->email->send();
    }
    
    public function sent_maint_booking_confirmation_mail($booking_id)
    {
        $this->load->library('email');
        $data['bookingdetails'] = $this->api_model->get_bookingdetails_by_id($booking_id);
        if($data['bookingdetails']->coupon_id > 0)
        {
        $data['percentage'] = $this->api_model->get_coupon_percentage($data['bookingdetails']->coupon_id);
        }
        $name = $data['bookingdetails']->customer_name;
        $email = $data['bookingdetails']->email_address;
        $mobile = $data['bookingdetails']->mobile_number_1;
        $data['name'] = $name;
        $data['customer_name'] = $data['bookingdetails']->email_address;
        $data['password'] = $data['bookingdetails']->customer_password;
//        $data['verification_code'] = base64_encode($data['customer']->customer_id) . "/" . md5($email . $mobile);
        $html = $this->load->view('booking_maint_confirmation_template_email', $data, True);

        $config['mailtype'] = 'html';
        $config['charset'] = 'iso-8859-1';
        $this->email->initialize($config);

        $this->email->from('maintenance@spectrumservices.ae', 'Spectrum');
        $this->email->to($email);
        //$this->email->cc("vishnu.m@azinova.info");
        $this->email->subject('Maintenance Booking Confirmation');

        $this->email->message($html);

        //$this->email->send();
    }
    
    public function sent_maint_booking_admin_confirmation_mail($booking_id)
    {
        $this->load->library('email');
        $data['bookingdetails'] = $this->api_model->get_bookingdetails_by_id($booking_id);
        if($data['bookingdetails']->coupon_id > 0)
        {
        $data['percentage'] = $this->spectrum_model->get_coupon_percentage($data['bookingdetails']->coupon_id);
        }
        $name = $data['bookingdetails']->customer_name;
        $email = $data['bookingdetails']->email_address;
        $mobile = $data['bookingdetails']->mobile_number_1;
        $data['name'] = $name;
        $data['customer_name'] = $data['bookingdetails']->email_address;
        $data['password'] = $data['bookingdetails']->customer_password;
//        $data['verification_code'] = base64_encode($data['customer']->customer_id) . "/" . md5($email . $mobile);
        $html = $this->load->view('booking_maint_admin_template_email', $data, True);

        $config['mailtype'] = 'html';
        $config['charset'] = 'iso-8859-1';
        $this->email->initialize($config);

        $this->email->from('web@spectrumservices.ae', 'Spectrum');
        //$this->email->to('maintenance@spectrumservices.ae');
        //$this->email->cc('pooja.nair@azinova.info,instantbooking@gmail.com');
        //$this->email->cc('');
        //$this->email->cc("");
        $this->email->to('vishnu.m@azinova.info');
        $this->email->subject('Maintenence Booking Confirmation');

        $this->email->message($html);

        //$this->email->send();
    }
    
    
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
     
     
     
     
    
    /*
     * Author   : Jiby
     * Date     : 02-11-17
     * Purpose  : List customer address for customer app
     */
    public function list_customer_address(){
        if ($this->config->item('ws') == $this->input->get('ws') ){
            if ($this->config->item('ws') == $this->input->get('ws') ){
                $handle     = fopen('php://input', 'r');
                $jsonInput  = fgets($handle); 
//                $jsonInput  = '{"customer_id":"3"}';
              
                $json_arr   = json_decode($jsonInput, true);
            
            if (!empty($json_arr)) {
                 if ( $json_arr['customer_id'] && strlen($json_arr['customer_id']) > 0 ){
                    $cust_id        = trim($json_arr['customer_id']);
                    $userInfo       = $this->customers_model->get_customer_by_id($cust_id);
                       if(empty($userInfo)){
                         echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'User ID not found')); exit();   
                       }
                       $cust_address_Info       = $this->customers_model->get_customer_addresses($userInfo->customer_id);
                       $customer_add = array();
                       $i = 0;
                     foreach($cust_address_Info as $address){
                         $customer_add[$i]['address']   = $address->customer_address;
                         $customer_add[$i]['area_name'] = $address->area_name;
                         $customer_add[$i]['zone_id']   = $address->zone_id;
                         $customer_add[$i]['latitude']  = $address->latitude;
                         $customer_add[$i]['longitude'] = $address->longitude;
                         $i++;
                     }
                       echo json_encode(array('status' => 'success','response_code'=>'200','addressList'=>$customer_add)); exit();  
                 }
                   else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '103';
                    $response['message']    = 'Customer ID missing!';
                    echo json_encode($response);
                    exit();
                }
            }
             else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
            }
            }
            else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';
            echo json_encode($response);
            exit();
        }
        }
    }
    
    /*
     * Author   : Jiby
     * Date     : 03-11-17
     * Purpose  : Add customer address for customer app
     */
    public function add_customer_address(){ 
     
              if ($this->config->item('ws') == $this->input->get('ws') ){
                $handle     = fopen('php://input', 'r');
                $jsonInput  = fgets($handle); 
//                $jsonInput  = '{"customer_id":"38","area_id":"2","customer_address":"tsdd","latitude":"81.5555","longitude":"87.4555"}';
              
                $json_arr   = json_decode($jsonInput, true);
                
                if (!empty($json_arr)){
                     if ( $json_arr['customer_id'] && strlen($json_arr['customer_id']) > 0 &&  $json_arr['area_id'] && strlen($json_arr['area_id']) > 0 &&  $json_arr['customer_address'] && strlen($json_arr['customer_address']) > 0 &&  $json_arr['latitude'] && strlen($json_arr['latitude']) > 0 &&  $json_arr['longitude'] && strlen($json_arr['longitude']) > 0 ){
                          $cust_id        = trim($json_arr['customer_id']);
                        $userInfo       = $this->customers_model->get_customer_by_id($cust_id);
                       if(empty($userInfo)){
                         echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'Customer ID not found')); exit();   
                       }
                       
                       $custDetails                     = new stdClass();
                       $custDetails->customer_id        = $userInfo->customer_id;
                       $custDetails->area_id            = trim($json_arr['area_id']);
                       $custDetails->customer_address   = trim($json_arr['customer_address']);
                       $custDetails->latitude           = trim($json_arr['latitude']);
                       $custDetails->longitude          = trim($json_arr['longitude']);
                       $custAddId       = $this->customers_model->update_customer_address($custDetails);
                       if(isset($custAddId)){
                            echo json_encode(array('status' => 'success','response_code'=>'200','message'=>'Address added successfully')); exit();   
                       }else {
                            echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'Something went wrong')); exit();   
                       }
                     }   
                     else {
                        $response               = array();
                        $response['status']     = 'error';
                        $response['error_code'] = '103';
                        $response['message']    = 'Parametres missing!';
                        echo json_encode($response);
                        exit();
                    }
                }
                 else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '101';
                    $response['message']    = 'Invalid request';
                    echo json_encode($response);
                    exit();
                }
              }
               else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '104';
                    $response['message']    = 'Invalid url';
                    echo json_encode($response);
                    exit();
                }
          
    }
    
    
    
    
    
    
    
    
    
    
      /*
     * Author   : Jiby
     * Date     : 07-11-17
     * Purpose  : Get dummy Offers for customer app
     */
   
    
        public function get_offer()
    {
       if ($this->config->item('ws') == $this->input->get('ws') )
        {
            $offer_results=$this->service_types_model->get_offers();
            $i=0;
            if(!empty($offer_results)){
                foreach($offer_results as $results)
                {
                    $offer[$i]['type'] = @$results->type;
                    $offer[$i]['title'] = @$results->title;
                    $offer[$i]['subtitle'] = @$results->subtitle;
                    $offer[$i]['imageurl'] = base_url().'assets/offer/'.@$results->imageurl;
                    $offer[$i]['featured'] = @$results->featured;
                    $i++;
                }
                echo json_encode(array('status' => 'success', 'offers' => $offer));
             }
             else
             {
               echo json_encode(array('status' => 'error', 'message' =>"No offers found"));   
             }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '104';
            $response['message'] = 'Invalid url';

            echo json_encode($response);
            exit();
        }
             
        
    }
    
       protected function saveImageFromUrl($photoUrl)
    {
        define('DIRECTORY', $this->config->item('root_directory'));
        $photo_type         = 'cus_sm_photo';
        $target_file_name   = $photo_type . '_' . time() . '.jpg' ;      
        $tmp_upload_folder  = 'customer_img/';
        $file_name      = DIRECTORY .$tmp_upload_folder.$target_file_name;
        $photo_content  = file_get_contents($photoUrl);
        file_put_contents($file_name, $photo_content);

        return $target_file_name;

    } 
    
       /*
     * Author   : Jiby
     * Date     : 08-11-17
     * Purpose  : Upload photo from url for customer app
     */
   
    
        public function update_photo_from_url(){
// echo '<pre>';print_r($_SERVER);exit;
            if($this->config->item('ws') === $this->input->get('ws')){

                $handle     = fopen('php://input','r');
                $jsonInput  = fgets($handle); 
//                $jsonInput  = '{"customer_id":"1","photo_url":"https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/17796786_407427796309621_2138313569488245364_n.jpg?oh=85040eb23ae5163a989f43ed3ae85115&oe=5A684861"}'; // normal login from mobile
                $json_arr   = json_decode($jsonInput,true);
                if(!empty($json_arr)){
                    if($json_arr['customer_id'] && is_numeric($json_arr['customer_id']) && !empty($json_arr['photo_url']) ){
                        
                        $cust_id        = trim($json_arr['customer_id']);
                        $photoUrl       = trim($json_arr['photo_url']);
                        $cust_photo_url = $this->saveImageFromUrl($photoUrl);
                      
                        $userDetails                     = new stdClass();
                        $userDetails->customer_photo_file = $cust_photo_url;
                        $affected = $this->customers_model->update_customers($userDetails,$cust_id);
                        
                        if($affected > 0)
                        {
                            echo json_encode(array('status' => 'success', 'message' => 'Profile image updated Successfully'));
                            exit();
                        } 
                        else 
                        {
                            $response           = array();
                            $response['status'] = 'error';
                            $response['message']    = 'Unexpected error!';
                            $response['error_code'] = '105';
                            echo json_encode($response);
                            exit();
                        }
                    }
                    else 
                    {
                        $response           = array();
                        $response['status'] = 'error';
                        $response['message']= 'Parameter missing!';
                        $response['error_code'] = '103';

                        echo json_encode($response);
                        exit();	 
                    }
                }
                else
                {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '101';
                    $response['message']    = 'Invalid request';
                    echo json_encode($response);
                    exit();	
                }

            }

            else
            {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '104';
                $response['message']    = 'Invalid url';

                echo json_encode($response);
                exit();	
            }
        }
		
		public function getCustomerDetails()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            if ($this->input->get('mobile') && strlen(trim($this->input->get('mobile'))) > 0) 
            {
                $mobile = trim($this->input->get('mobile'));

                if ($mobile != "") 
                {
                    $custDet = $this->api_model->getCustomerMobSearch($mobile);

                    if(!empty($custDet))
                    {
                        echo json_encode(array("status"=>"success","result"=> $custDet)); exit();
                    } else {

                        echo json_encode(array("status"=>"not found","result"=> 'Mobile number not found')); exit();
                    }
                } else {

                    echo json_encode(array("status"=>"not found","result"=> 'Mobile number not found')); exit();
                }
            } else {
                echo json_encode(array("status"=>"not found","result"=> 'Invalid Url')); exit();
            }
        } else {
            echo json_encode(array("status"=>"not found","result"=> 'Invalid Url')); exit();
        }
    }
	
	public function get_driver_notifications() 
	{
        if ($this->input->get('imei') && strlen(trim($this->input->get('imei'))) > 0) 
		{
            $imei = trim($this->input->get('imei'));

            $tablet = $this->driverappapimodel->get_tablet_by_imei($imei);

            if (isset($tablet->tablet_id) && $tablet->tablet_status == 1) {
                

                $notifications = $this->driverappapimodel->get_driver_notifications($tablet->tablet_id);

                $response = array();
                $response['status'] = 'success';
                $response['notifications'] = $notifications;
                $response['my_tablet'] = $tablet->tablet_id;
                $response['notification_count'] = count($notifications);

                echo json_encode($response);
                exit();
            } else if (isset($tablet->tablet_id) && $tablet->tablet_status == 0) {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '103';
                $response['message'] = 'Inactive tablet';

                echo json_encode($response);
                exit();
            } else {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '102';
                $response['message'] = 'Invalid IMEI';

                echo json_encode($response);
                exit();
            }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
	public function send_service_status_email($booking_id,$day_service_id,$servicedate)
	{
		$this->load->library('email');
        $data['booking'] =$this->bookings_model->get_booking_by_id($booking_id);
		$data['service_id'] =$day_service_id;
		$data['booking_id'] =$booking_id;
		$data['servicedate'] =$servicedate;
        $html = $this->load->view('service_template_email', $data, True);
		$config = array(
			'protocol'  => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'booking.elitemaids@gmail.com',
			'smtp_pass' => 'Elitemaids@2020#',
			'mailtype'  => 'html',
			'charset'   => 'utf-8'
		);
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");
		$this->email->from('booking.elitemaids@gmail.com', 'ELITEMAIDS');
		$this->email->to($data['booking']->email_address);
		$this->email->subject('Elitemaids Service Notification');
		$this->email->message($html);
		//$this->email->send();
	}
    
    
}

