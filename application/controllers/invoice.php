<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Invoice extends CI_Controller {
	
    public function __construct()
    {
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        /*
        if(!is_user_loggedin())
        {
                if($this->input->is_ajax_request())
                {
                        echo 'refresh';
                        exit();
                }
                else
                {
                        redirect('logout');
                }
        }
        */
        //     if(!user_permission(user_authenticate(), 5))
        //        {
        //            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        //         }
        $this->load->model('customers_model');
        $this->load->model('maids_model');
        $this->load->model('invoice_model');
        $this->load->model('zones_model');
        $this->load->model('bookings_model');
        $this->load->model('day_services_model');
        $this->load->helper('google_api_helper');
        $this->load->model('tablets_model');
        $this->load->model('service_types_model');
        $this->load->model('settings_model');
        //$this->load->helper('odoo_helper');
    }
    
    public function index()
    {
        if($this->uri->segment(2) && strlen(trim($this->uri->segment(2) > 0)))
        {
            $schedule_date = $this->uri->segment(2);
        } else {
            redirect('activity/jobs');
            exit();
        }
        if($this->uri->segment(3) && strlen(trim($this->uri->segment(3) > 0)))
        {
            $booking_id = $this->uri->segment(3);		
        }
        else
        {
            redirect('activity/jobs');
            exit();
        }
        
        if($this->uri->segment(4) && strlen(trim($this->uri->segment(4) > 0)))
        {
            $customer_id = $this->uri->segment(4);		
        } else {
            redirect('activity/jobs');
            exit();
        }
        
        $get_total_booking_by_customer = $this->bookings_model->get_total_booking_by_customer($schedule_date,$customer_id);
        
        foreach ($get_total_booking_by_customer as $booking)
        { 
            $chk_day_service = $this->day_services_model->get_day_service_by_booking_id($schedule_date, $booking->booking_id);
            
            if($chk_day_service->service_status != 2)
            {
                redirect('activity/job_view/'.$schedule_date.'/'.$booking_id);
                exit();
            } else {
                //echo 'hai';
                $checkinvoiceexist = $this->invoice_model->checkinvoiceexistbydayserviceid($chk_day_service->day_service_id);
                if(count($checkinvoiceexist) > 0)
                {
                    echo 'hai';
                    //redirect('activity/job_view/'.$schedule_date.'/'.$booking_id);
                    redirect('invoice/view_invoice/'.$checkinvoiceexist->invoice_num);
                    exit();
                }
            }
        }
        
        $all_details = array();
        $i = 0;
        foreach ($get_total_booking_by_customer as $bookings)
        {
            $jobdetail = $this->day_services_model->get_activity_by_date_plan_job_invoice($schedule_date,$bookings->booking_id);
            
            $all_details[$bookings->booking_id] = new stdClass();
            $all_details[$bookings->booking_id]->booking_id = $jobdetail->booking_id;
            $all_details[$bookings->booking_id]->day_service_id = $jobdetail->day_service_id;
            $all_details[$bookings->booking_id]->customer_id = $jobdetail->customer_id;
            $all_details[$bookings->booking_id]->customer_name = $jobdetail->customer_name;
            $all_details[$bookings->booking_id]->customer_address = $jobdetail->customer_address;
            $all_details[$bookings->booking_id]->latitude = $jobdetail->latitude;
            $all_details[$bookings->booking_id]->longitude = $jobdetail->longitude;
            $all_details[$bookings->booking_id]->maid_name = $jobdetail->maid_name;
            $all_details[$bookings->booking_id]->mobile_number_1 = $jobdetail->mobile_number_1;
            $all_details[$bookings->booking_id]->email_address = $jobdetail->email_address;
            $all_details[$bookings->booking_id]->user_fullname = $jobdetail->user_fullname;
            $all_details[$bookings->booking_id]->customer_address_id = $jobdetail->customer_address_id;
            $all_details[$bookings->booking_id]->maid_id = $jobdetail->maid_id;
            $all_details[$bookings->booking_id]->booking_note = $jobdetail->booking_note;
            $all_details[$bookings->booking_id]->time_from = $jobdetail->time_from;
            $all_details[$bookings->booking_id]->time_to = $jobdetail->time_to;
            $all_details[$bookings->booking_id]->start_time = $jobdetail->start_time;
            $all_details[$bookings->booking_id]->end_time = $jobdetail->end_time;
            $all_details[$bookings->booking_id]->booking_type = $jobdetail->booking_type;
            $all_details[$bookings->booking_id]->booking_status = $jobdetail->booking_status;
            $all_details[$bookings->booking_id]->total_fee = $jobdetail->total_fee;
            $all_details[$bookings->booking_id]->material_fee = $jobdetail->material_fee;
            $all_details[$bookings->booking_id]->starting_time = $jobdetail->starting_time;
            $all_details[$bookings->booking_id]->ending_time = $jobdetail->ending_time;
            $all_details[$bookings->booking_id]->collected_amount = $jobdetail->collected_amount;
            $all_details[$bookings->booking_id]->service_status = $jobdetail->service_status;
            $all_details[$bookings->booking_id]->payment_status = $jobdetail->payment_status;
            $all_details[$bookings->booking_id]->ps_no = $jobdetail->ps_no;
            $all_details[$bookings->booking_id]->service_date = $jobdetail->service_date;
            
            $i++;
        }
//        echo '<pre>';
//        print_r($all_details);
//        echo '</pre>';
//        exit();
        
        //$jobdetail = $this->day_services_model->get_activity_by_date_plan_job_view($schedule_date,$booking_id);
        
        $data = array();
        $data['jobdetail'] = $all_details;
        $data['jobdate'] = date('d F Y', strtotime($schedule_date));
        $data['scheduledates'] = $schedule_date;
        $data['bookings_id'] = $booking_id;
        $layout_data['invoice_active'] = '1';
        $layout_data = array();
        $layout_data['content_body'] = $this->load->view('add_invoice', $data, TRUE);		
        $layout_data['page_title'] = 'Invoice';
        $layout_data['meta_description'] = 'Invoice';
        $layout_data['css_files'] = array('jquery.fancybox.css','demo.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.fancybox.pack.js','base.js', 'bootstrap-datepicker.js','jquery.validate.min.js','mymaids.js');

        $this->load->view('layouts/default_dashboard', $layout_data);
        
        
    }

    /**
     * Function to retreive outstanding invoices of a particular cutomer
     */
    function get_customer_outstanding_invoices_datatable() 
	{
        $customerId = $this->input->post('customer_id');
        $recordsTotal = $this->invoice_model->count_customer_outstanding($customerId);
        $draw = $this->input->post('draw');
        $start = $this->input->post('start');
        $rowperpage = $this->input->post('length');

        $outstandingInvoices = $this->invoice_model->get_customer_outstanding_invoices($customerId);
        $data = array();
        $i = 1;
        foreach ($outstandingInvoices as $outstandingInvoice) {

            $data[] = array(
                'slno' => '<center>'.($i + $start).'<center>',
                'invoicenum' => $outstandingInvoice->invoice_num,
                'invoicedate' => $outstandingInvoice->invoice_date,
                'duedate' => $outstandingInvoice->invoice_due_date,
                'total' => $outstandingInvoice->invoice_net_amount,
                'paidamt' => $outstandingInvoice->received_amount,
                'dueamt' => $outstandingInvoice->balance_amount
            );
            $i++;
        }

        $lastCall = isset($outstandingInvoices[0])?$outstandingInvoices[0]:"";
        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $recordsTotal,
            "lastcall" => $lastCall,
            "iTotalDisplayRecords" => count($outstandingInvoices),
            "aaData" => $data
        );
        echo json_encode($response);
    }
	
	function list_customer_invoices() 
	{
		$data = array();
        $date_from = NULL;
        $date_to = NULL;
		$customer_id = NULL;
		$inv_status = NULL;
		$data['search_date_from'] = NULL;
		$data['search_date_to'] = NULL;
		
		if($this->input->post())
		{
			if($this->input->post('customers_vh_rep') > 0)
			{
				$customer_id = $this->input->post('customers_vh_rep');
			} else {
				$customer_id = NULL;
			}
			
			if($this->input->post('inv_status') != "")
			{
				$inv_status = $this->input->post('inv_status');
			} else {
				$inv_status = NULL;
			}
			
			if($this->input->post('vehicle_date') != "")
			{
				$from_date = $this->input->post('vehicle_date');
				$date_from = date('Y-m-d', strtotime(str_replace("/", "-", $from_date)));
				$data['search_date_from'] = date('d/m/Y', strtotime($date_from));
			} else {
				$date_from = NULL;
				$data['search_date_from'] = NULL;
			}
			
			if($this->input->post('vehicle_date_to') != "")
			{
				$to_date = $this->input->post('vehicle_date_to');
				$date_to = date('Y-m-d', strtotime(str_replace("/", "-", $to_date)));
				$data['search_date_to'] = date('d/m/Y', strtotime($date_to));
			} else {
				$date_to = NULL;
				$data['search_date_to'] = NULL;
			}
		}
        
		$data['customer_id'] = $customer_id;
		$data['inv_status'] = $inv_status;
        $data['invoice_report']=$this->invoice_model->get_customer_invoices($date_from,$date_to,$customer_id,$inv_status);
        $data['customerlist'] = $this->invoice_model->get_customer_list();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('customer_invoice_list', $data, TRUE);
        $layout_data['page_title'] = 'Customer Invoices';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }

    function add_monthly_invoice() 
    {
        $settings = $this->settings_model->get_settings();
        $data=array();
        if($this->input->post())
        {
            $customer_id    = $this->input->post('customer');
            $month          = $this->input->post('invoice_month');
            $month          = sprintf("%02d", $month);
            $service_date   = date("Y")."-".$month."-"."01";
            $service_end_date   = date("Y-m-t", strtotime($service_date));
            $product_name   = $this->input->post('product_name');
            $description    = $this->input->post('description');
            $quantity       = $this->input->post('quantity');
            $amount      = $this->input->post('unit_cost');
            $issue_date     = $this->input->post('issue_date');
            $due_date       = $this->input->post('due_date');
            $linearray=array();

            

            $cust_details=$this->customers_model->get_customers_byid($customer_id);

            $added_date_time = date('Y-m-d H:i:s');
            
            
            //$invoice_id = $this->day_services_model->add_main_invoice($main_inv_array);
            $line_items = $this->invoice_model->get_monthly_bookings_for_invoice($customer_id,$service_date,$service_end_date);
            $line_item_count=count($line_items);
            

            //echo $line_item_count;
            //exit();




            if($line_item_count>0)
            {
                foreach ($line_items as $key => $line_item) 
                {
                    $ds_id=$line_item->day_service_id;
                    $total_time = ((strtotime($line_item->end_time) - strtotime($line_item->start_time))/ 3600);
                    $total_fee_l = floatval($line_item->total_fee);
                    $serviceamount_l = $line_item->serviceamount;
                    $vat_fee_l = $line_item->vatamount;
                    if(!is_numeric($vat_fee_l)){
                        $vat_fee_l=0.05*$serviceamount_l;
                        $vat_fee_l=sprintf("%0.2f",$vat_fee_l);
                    }
                    $unit_price = $line_item->price_hourly;
                    $discount=$line_item->discount;
                    $discount=is_numeric($discount)?$discount:'0';

                    $linearray[$key]['day_service_id'] = $ds_id;
                    $linearray[$key]['booking_id'] = $line_item->booking_id;
                    $linearray[$key]['day_service_reference'] = $line_item->day_service_reference;
                    $linearray[$key]['maid_id'] = $line_item->maid_id;
                    $linearray[$key]['maid_name'] = $line_item->maid_name;
                    $linearray[$key]['service_from_time'] = $line_item->start_time;
                    $linearray[$key]['service_to_time'] = $line_item->end_time;
                    $linearray[$key]['line_bill_address'] = $line_item->customer_address;
                    $linearray[$key]['description'] = "Service from ".$line_item->time_from." to ".$line_item->time_to." For ".$line_item->customer_address;
                    $linearray[$key]['service_hrs'] = $total_time;
                    $linearray[$key]['inv_unit_price'] = (float)$unit_price;
                    $linearray[$key]['line_discount'] = (float)$discount;
                    //$linearray[$key]['line_amount'] = (float)$serviceamount_l; 
                    //$linearray[$key]['line_vat_amount'] = (float)$vat_fee_l;
                    $linearray[$key]['line_net_amount'] = $total_fee_l;
                    $linearray[$key]['line_vat_amount'] = ($total_fee_l * $settings->service_vat_percentage)/(100+$settings->service_vat_percentage);
                    $linearray[$key]['line_amount'] = $total_fee_l - $linearray[$key]['line_vat_amount'];
                    array_push($dayservice_id_array,$ds_id);
                    $i++;
                }


                $main_inv_array = array();
                $main_inv_array['customer_id'] = $line_items[0]->customer_id;
                $main_inv_array['customer_name'] = $line_items[0]->customer_name;
                $main_inv_array['bill_address'] = $line_items[0]->customer_address;
                $main_inv_array['added'] = $added_date_time;
                $main_inv_array['service_date'] = $issue_date;
                $main_inv_array['invoice_date'] = $issue_date;
                $main_inv_array['invoice_due_date'] = $due_date;
                $main_inv_array['invoice_type'] = 2;
                $main_inv_array['invoice_month'] = $month; 
                $main_inv_array['invoice_added_by'] = user_authenticate();
                    
                $invoice_id = $this->day_services_model->add_main_invoice($main_inv_array);

                if($invoice_id > 0)
                    {
                        $tot_bill_amount = 0;
                        $total_vat_amt = 0;
                        $total_net_amt = 0;
                        $dayids = array();
                        foreach($linearray as $line_val)
                        {
                            $tot_bill_amount += $line_val['line_amount'];
                            $total_vat_amt += $line_val['line_vat_amount'];
                            $total_net_amt += $line_val['line_net_amount'];
                            $main_inv_line = array();
                            $main_inv_line['invoice_id'] = $invoice_id;
                            $main_inv_line['day_service_id'] = $line_val['day_service_id'];
                            $main_inv_line['day_service_reference'] = $line_val['day_service_reference'];
                            $main_inv_line['maid_id'] = $line_val['maid_id'];
                            $main_inv_line['maid_name'] = $line_val['maid_name'];
                            $main_inv_line['service_from_time'] = $line_val['service_from_time'];
                            $main_inv_line['service_to_time'] = $line_val['service_to_time'];
                            $main_inv_line['line_bill_address'] = $line_val['line_bill_address'];
                            $main_inv_line['description'] = $line_val['description'];
                            $main_inv_line['service_hrs'] = $line_val['service_hrs'];
                            $main_inv_line['inv_unit_price'] = $line_val['inv_unit_price'];
                            $main_inv_line['line_discount'] = $line_val['line_discount'];
                            $main_inv_line['line_amount'] = $line_val['line_amount'];
                            $main_inv_line['line_vat_amount'] = $line_val['line_vat_amount'];
                            $main_inv_line['line_net_amount'] = $line_val['line_net_amount'];
                            
                            $invoice_line_id = $this->day_services_model->add_line_invoice($main_inv_line);
                            
                            if($invoice_line_id > 0)
                            {
                                $ds_fields = array();
                                $ds_fields['serv_invoice_id'] = $invoice_id;
                                $ds_fields['invoice_status'] = 1;
                                
                                $this->day_services_model->update_day_service($line_val['day_service_id'], $ds_fields);
                                array_push($dayids,$line_val['booking_id']);
                            }
                            
                        }
                        
                        $update_main_inv = array();
                        $update_main_inv['invoice_num'] = "INV-".date('Y')."-".sprintf('%04s', $invoice_id);
                        //$update_main_inv['billed_amount'] = $tot_bill_amount;
                        $update_main_inv['invoice_total_amount'] = $tot_bill_amount;
                        $update_main_inv['invoice_tax_amount'] = $total_vat_amt;
                        $update_main_inv['invoice_net_amount'] = $total_net_amt;
                        $update_main_inv['balance_amount'] = $total_net_amt;
                        
                        $updateinv = $this->day_services_model->update_service_invoice($invoice_id, $update_main_inv);
                        if($updateinv)
                        {
                            $return = array();
                            $data['message'] = 'success';
                            $return['dayids'] = $dayids;

                            
                        } 
                        else 
                        {
                            $return = array();
                            $data['message'] = 'error';
                            $return['dayids'] = $dayids;                            
                        }
                    }






                

                //$data['message'] = "success";
            }

        }
        $data['customerlist'] = $this->customers_model->customers_by_payment_types(array('M','W','MA'));
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('add_monthly_invoice', $data, TRUE);
        $layout_data['page_title'] = 'Add Monthly Invoices';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['accounting_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    function add_monthly_invoice_advanced() 
    {
        $settings = $this->settings_model->get_settings();
        if($this->input->post()){
            if($this->input->post('add_ma_invoice')){
                // save invoice
                // Code by : Samnad. S
                /*********************************** */
                // this will find future service dates for all bookings for the user
                // based on that it will add line items for invoice
                //
                /*********************************** */
                $customer_id = $this->input->post('customer');
                $for_year_month = $this->input->post('invoice_month');
                $issue_date     = $this->input->post('invoice_issue_date');
                $due_date = $this->input->post('invoice_due_date');
                $month_start_date = date('Y-m-d',strtotime($for_year_month.'first day of this month'));
                $month_end_date = date('Y-m-d', strtotime($for_year_month . 'last day of this month'));
                $bookings = $this->invoice_model->get_bookings_by_customer_and_between_dates($customer_id,$month_start_date,$month_end_date); // get all booking by customer on the month
                //$days = getDayServiceDates(1,'08-05-2023','08-05-2023','bw');
                //echo('<pre>');print_r($bookings);echo('<pre>');die();
                $service_data = [];
                foreach ($bookings as $key => $booking) {
                    $booking_type = strtoupper($booking->booking_type);
                    /********************************************** */
                    if ($booking->service_end == 0) {
                        $end_date = $month_end_date;
                    } else {
                        if ($booking->service_actual_end_date >= $month_end_date) {
                            $end_date = $month_end_date;
                        } else {
                            $end_date = $booking->service_actual_end_date;
                        }
                    }
                    /********************************************** */
                    if ($booking->service_start_date < $month_start_date) {
                        $start_date = $month_start_date;
                        $day = date('l', strtotime($booking->service_start_date));
                        $start_date = new \DateTime($start_date);
                        $start_date = $start_date->modify('first '.$day.' of this month')->format('Y-m-d');
                        /********************************************** */
                        if($booking_type == 'BW'){
                            // ufffff.. :/
                            $date1 = new DateTime($booking->service_start_date);
                            $date2 = new DateTime($start_date);
                            $differenceInWeeks = $date1->diff($date2)->days / 7;
                            if ( $differenceInWeeks & 1 ) {
                                // if odd number
                                $start_date = date('Y-m-d', strtotime('+1 week', strtotime($start_date)));
                            }
                        }
                        /********************************************** */
                    } else {
                        $start_date = $booking->service_start_date;
                    }
                    $service_data[$booking_type][$booking->booking_id]['service_dates'] = getFutureDayServiceDates($booking->service_week_day, $start_date, $end_date, $booking_type);
                    $service_data[$booking_type][$booking->booking_id]['booking'] = $booking;
                }
                //echo('<pre>');print_r($service_data);echo('<pre>');die();
                if (count($service_data) === 0) {
                    die('No Booking Found !');
                }
                $customer = $this->invoice_model->get_customer_data_for_invoice($customer_id);
                try{
                    $this->db->trans_begin();
                    $insert_main_id = $this->invoice_model->insert_monthly_advanced_invoice_main(array('customer_id' => $customer_id, 'for_year_month' => $for_year_month, 'issue_date' => $issue_date, 'due_date' => $due_date, 'invoice_added_by' => user_authenticate(), 'customer_name' => $customer->customer_name, 'billing_address' => $customer->customer_address));
                    $update_main_data = array();
                    foreach($service_data as $booking_type => $alldetails){
                        //echo('<pre>');print_r($booking_type);echo('<pre>');
                        foreach($alldetails as $booking_id => $details){
                            //echo('<pre>');print_r($booking_id);echo('<pre>');
                            foreach($details['service_dates'] as $key => $service_date){
                                /***** check in delete and add */
                                if(!$this->invoice_model->get_booking_service_delete_by_date($booking_id,$service_date))
                                {
                                    $total_amount = $details['booking']->total_amount;
                                    $tax = number_format(($total_amount*$settings->service_vat_percentage)/(100+$settings->service_vat_percentage),2);
                                    $service_charge = $total_amount - $tax;
                                    //print_r($service_charge);
                                    //echo '<br>';
                                    // if not a deleted schedule
                                    $update_main_data['net_amount'] += $service_charge;
                                    $update_main_data['vat_charge'] += $tax;
                                    $update_main_data['total_amount'] += $total_amount;
                                    $insert_line_id = $this->invoice_model->insert_monthly_advanced_invoice_line(array('invoice_id' => $insert_main_id, 'booking_id' => $booking_id, 'service_date' => $service_date,'price_per_hr' => $details['booking']->price_per_hr,'discount_price_per_hr' => $details['booking']->discount_price_per_hr,'net_service_cost' => $details['booking']->net_service_cost,'discount' => $details['booking']->discount,'service_charge' => $service_charge,'cleaning_material_fee' => $details['booking']->cleaning_material_fee,'vat_charge' => $tax,'total_amount' => $total_amount));
                                }
                            }
                        }
                    }
                    $update_main_data['reference'] = $this->db->escape('INV-MA-'.date('Y-m-',strtotime($for_year_month)).sprintf("%06d", $insert_main_id));
                    $update_main_data['updated_at'] = $this->db->escape(null);
                    $update_main = $this->invoice_model->update_monthly_advanced_invoice_main(array('id' => $insert_main_id),$update_main_data);
                    $data['message'] = 'success';
                    $this->db->trans_commit();
                    redirect('invoice/view/monthly-advanced/'.$insert_main_id);
                }
                catch (Exception $e) {
                    $this->db->trans_rollback();
                }
                //echo('<pre>');print_r($service_data);echo('<pre>');die();
            }
            else{
                die('No Action Found !');
            }
        }
        $data['customerlist'] = $this->customers_model->customers_by_payment_types(array('MA','W','M'));
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $data['service_data'] = $service_data;
        $layout_data['content_body'] = $this->load->view('invoice/add_monthly_invoice_advanced', $data, TRUE);
        $layout_data['page_title'] = 'Add Advanced Monthly Invoice';
        $layout_data['meta_description'] = 'Add Advanced Monthly Invoice';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['accounting_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js','invoice.js');
        $this->load->view('layouts/default', $layout_data);
    }
    function list_customer_monthly_invoices() 
    {
        $data = array();
        $date_from = NULL;
        $date_to = NULL;
        $customer_id = NULL;
        $inv_status = NULL;
        $data['search_date_from'] = NULL;
        $data['search_date_to'] = NULL;
        
        if($this->input->post())
        {
            if($this->input->post('customers_vh_rep') > 0)
            {
                $customer_id = $this->input->post('customers_vh_rep');
            } else {
                $customer_id = NULL;
            }
            
            if($this->input->post('inv_status') != "")
            {
                $inv_status = $this->input->post('inv_status');
            } else {
                $inv_status = NULL;
            }
            
            if($this->input->post('vehicle_date') != "")
            {
                $from_date = $this->input->post('vehicle_date');
                $date_from = date('Y-m-d', strtotime(str_replace("/", "-", $from_date)));
                $data['search_date_from'] = date('d/m/Y', strtotime($date_from));
            } else {
                $date_from = NULL;
                $data['search_date_from'] = NULL;
            }
            
            if($this->input->post('vehicle_date_to') != "")
            {
                $to_date = $this->input->post('vehicle_date_to');
                $date_to = date('Y-m-d', strtotime(str_replace("/", "-", $to_date)));
                $data['search_date_to'] = date('d/m/Y', strtotime($date_to));
            } else {
                $date_to = NULL;
                $data['search_date_to'] = NULL;
            }
        }
        $data['customer_id'] = $customer_id;
        $data['inv_status'] = $inv_status;
        
        $data['invoice_report']=$this->invoice_model->get_customer_monthly_invoices($date_from,$date_to,$customer_id,$inv_status);
        $data['customerlist'] = $this->invoice_model->get_customer_list();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('customer_monthly_invoice_list', $data, TRUE);
        $layout_data['page_title'] = 'Customer Monthly Invoices';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    function list_customer_monthly_invoices_advanced() 
    {
        $data = array();
        $date_from = NULL;
        $date_to = NULL;
        $customer_id = NULL;
        $inv_status = NULL;
        $data['search_date_from'] = NULL;
        $data['search_date_to'] = NULL;
        
        if($this->input->post())
        {
            if($this->input->post('customer') > 0)
            {
                $customer_id = $this->input->post('customer');
            } else {
                $customer_id = NULL;
            }
            
            if($this->input->post('inv_status') != "")
            {
                $inv_status = $this->input->post('inv_status');
            } else {
                $inv_status = NULL;
            }
            
            if($this->input->post('vehicle_date') != "")
            {
                $from_date = $this->input->post('vehicle_date');
                $date_from = date('Y-m-d', strtotime(str_replace("/", "-", $from_date)));
                $data['search_date_from'] = date('d/m/Y', strtotime($date_from));
            } else {
                $date_from = NULL;
                $data['search_date_from'] = NULL;
            }
            
            if($this->input->post('vehicle_date_to') != "")
            {
                $to_date = $this->input->post('vehicle_date_to');
                $date_to = date('Y-m-d', strtotime(str_replace("/", "-", $to_date)));
                $data['search_date_to'] = date('d/m/Y', strtotime($date_to));
            } else {
                $date_to = NULL;
                $data['search_date_to'] = NULL;
            }
        }
        $data['customer_id'] = $customer_id;
        $data['inv_status'] = $inv_status;
        
        $data['invoice_report']=$this->invoice_model->get_customer_monthly_advanced_invoices($date_from,$date_to,$customer_id,$inv_status);
        $data['customerlist'] = $this->customers_model->customers_by_payment_types(array('W','D','M'));
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('invoice/customer_monthly_invoice_list_advanced', $data, TRUE);
        $layout_data['page_title'] = 'Customer Monthly Invoices';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    public function generate_invoice_monthly_advanced($invoice_id)
    {
        $this->load->library('pdf');
        $invoice_detail = $this->invoice_model->get_invoice_detailbyid($invoice_id);
        $data = array();
        $data['invoice_detail'] = $invoice_detail;
        $data['invoice'] = $this->invoice_model->get_monthly_advanced_invoice_main_row(array('id'=>$invoice_id));
        if(!$data['invoice']){
            //redirect('invoice/list/monthly-advanced?invoice-not-found');
        }
        $data['invoice_lines'] = $this->invoice_model->get_monthly_advanced_invoice_lines(array('invoice_id'=>$invoice_id));
        //echo '<pre>';print_r($invoiceid);die;
        $data['settings'] = $this->settings_model->get_settings();
        $html_content = $this->load->view('invoice/pdf_invoice_monthly_advanced', $data, TRUE);
        $this->pdf->loadHtml($html_content);
        $this->pdf->set_paper("a4", "portrait");
        //$font = $this->pdf->getFontMetrics()->get_font("helvetica", "bold");
        //$this->pdf->getCanvas()->page_text(72, 18, "Header: {PAGE_NUM} of {PAGE_COUNT}",$font, 6);
        $this->pdf->render();
        $this->pdf->stream($data['invoice']->reference.".pdf", array("Attachment"=>0));
        //file_put_contents(base_url().''.$invoiceid.".pdf", $this->pdf->output());
        exit();
    }
    
    public function add_invoice()
    {
        $data_table = $this->input->post(NULL, TRUE);
        
        $d_array = $this->input->post('day_service_id');
        $dayservice_var = implode(",",$d_array);
        
        $b_array = $this->input->post('booking_id');
        $booking_var = implode(",",$b_array);
        
        $issuedate = $this->input->post('invoiceissuedate');
        $s_date = explode("/", $issuedate);
        $jobinvoiceissuedate = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];

        $duedate = $this->input->post('invoiceduedate');
        $s_date1 = explode("/", $duedate);
        $jobinvoiceduedate = $s_date1[2] . '-' . $s_date1[1] . '-' . $s_date1[0];
        
        $data = array(
            'invoice_num' => $this->input->post('invoice_num'),
            'customer_id' => $this->input->post('customer_id'),
            'day_service_id' => $dayservice_var,
            'invoice_status' => $this->input->post('invoice_status'),
            'added' => date('Y-m-d H:i:s'),
            'booking_id' => $booking_var,
            'service_date' => $this->input->post('service_date'),
            'invoice_date' => $jobinvoiceissuedate,
            'invoice_due_date' => $jobinvoiceduedate,
            'billed_amount' => $this->input->post('billed_amount'),
            'received_amount' => $this->input->post('received_amount'),
            'balance_amount' => $this->input->post('balance_amount'),
            'invoice_notes' => $this->input->post('invoice_note')
        );
        
        $invoices_id = $this->invoice_model->add_invoice($data);
        if(!empty($invoices_id))
        {
            redirect('invoice/view_invoice/'.$this->input->post('invoice_num'));
            exit();
        } else {
            redirect('activity/jobs');
            exit();
        }
    }
    
    public function view_invoice($invoice_id)
	{
		$invoice_detail = $this->invoice_model->get_invoice_detailbyid($invoice_id);
		$data = array();
		$data['invoice_detail'] = $invoice_detail;
        $data['settings'] = $this->settings_model->get_settings();
		$layout_data = array();
        $layout_data['content_body'] = $this->load->view('view_invoice_new', $data, TRUE);		
        $layout_data['page_title'] = 'Invoice View';
        $layout_data['meta_description'] = 'Invoice View';
        $layout_data['css_files'] = array('jquery.fancybox.css','demo.css','toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.fancybox.pack.js','base.js', 'bootstrap-datepicker.js','jquery.validate.min.js','toastr.min.js','mymaids.js','ajaxupload.3.5.js');

        $this->load->view('layouts/default_dashboard', $layout_data);
	}


    public function view_invoice_monthly($invoice_id)
    {
        $invoice_detail = $this->invoice_model->get_invoice_detailbyid($invoice_id);
        $data = array();
        $data['invoice_detail'] = $invoice_detail;
        $data['settings'] = $this->settings_model->get_settings();
        $layout_data = array();
        $layout_data['content_body'] = $this->load->view('view_invoice_monthly', $data, TRUE);      
        $layout_data['page_title'] = 'Invoice View';
        $layout_data['meta_description'] = 'Invoice View';
        $layout_data['css_files'] = array('jquery.fancybox.css','demo.css','toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.fancybox.pack.js','base.js', 'bootstrap-datepicker.js','jquery.validate.min.js','toastr.min.js','mymaids.js','ajaxupload.3.5.js');

        $this->load->view('layouts/default_dashboard', $layout_data);
    }

    public function update_quo_addr()
    {
        // ini_set('display_errors', 1);
        // ini_set('display_startup_errors', 1);
        // error_reporting(E_ALL);
        $address = $this->input->post('address');
        $quotationid = $this->input->post('quotationid');
        $data=array();
        $data['quo_bill_address']=$address;
        $result=$this->invoice_model->update_quotation($quotationid,$data);
        exit();
    }

    public function update_quotation_amt()
    {
        // ini_set('display_errors', 1);
        // ini_set('display_startup_errors', 1);
        // error_reporting(E_ALL);
        $quotation_amount = $this->input->post('quotation_amount');
        $quotation_amount=round($quotation_amount,2);
        $tax_amount=0.05*$quotation_amount;
        $tax_amount=round($tax_amount,2);
        $total_amount=$quotation_amount+$tax_amount;
        $quotation_id = $this->input->post('quotation_id');
        $data=array();
        $data['quo_net_amount']=$quotation_amount;
        $data['quo_tax_amount']=$tax_amount;
        $data['quo_total_amount']=$total_amount;
        $result=$this->invoice_model->update_quotation($quotation_id,$data);
        print_r(json_encode($data));
        exit();
    }

    public function generatequotation($quotation_id)
    {
        //ini_set('display_errors', 1);
        //ini_set('display_startup_errors', 1);
        //error_reporting(E_ALL);

        $this->load->library('pdf');
        $quotation_detail = $this->invoice_model->get_quotation_detailbyid($quotation_id);
        $quotation_line_items = $this->invoice_model->get_quotation_line_items_byqid($quotation_id);
        //$invoice_detail = $this->invoice_model->get_invoice_detailbyid($invoiceid);
        $data = array();
        $data['quotation_detail'] = $quotation_detail;
        $data['quotation_line_items'] = $quotation_line_items;
        $html_content = $this->load->view('quotationpdf', $data, TRUE);
        $this->pdf->loadHtml($html_content);
        $this->pdf->set_paper("a4", "portrait");
        $this->pdf->render();
        
        $this->pdf->stream("".$quotation_id.".pdf", array("Attachment"=>0));
        //file_put_contents(base_url().''.$invoiceid.".pdf", $this->pdf->output());
        exit();
    }

    public function update_line_desc()
    {
        // ini_set('display_errors', 1);
        // ini_set('display_startup_errors', 1);
        // error_reporting(E_ALL);
        $description = $this->input->post('line_description');
        $lineid = $this->input->post('lineid');
        $data=array();
        $data['description']=$description;
        $result=$this->invoice_model->update_invoicelineitems($lineid,$data);
        exit();
    }

    public function update_line_amt()
    {
        // Update by Samnad. S
        $settings = $this->settings_model->get_settings();
        $invoice_id = $this->input->post('invoice_id');
        $invstat = $this->input->post('invstat');
        $line_items = $this->invoice_model->get_invoice_line_items_by_invoice_id($this->input->post('invoice_id'));
        $data_invoice['invoice_total_amount'] = 0;
        $data_invoice['invoice_tax_amount'] = 0;
        $data_invoice['invoice_net_amount'] = 0;
        foreach($line_items as $row){
            if($row->invoice_line_id == $this->input->post('lineid')){
                $data['line_amount'] = number_format($this->input->post('line_amount'),2);
                $data['line_vat_amount']= number_format(($this->input->post('line_amount')/100) * $settings->service_vat_percentage,2);
                $data['line_net_amount']= number_format($data['line_amount']+$data['line_vat_amount'],2);
                $result=$this->invoice_model->update_invoicelineitems($this->input->post('lineid'),$data);
                $data_invoice['invoice_total_amount'] += $data['line_amount'];
                $data_invoice['invoice_tax_amount'] += $data['line_vat_amount'];
                $data_invoice['invoice_net_amount'] += $data['line_net_amount'];
            }
            else{
                $data_invoice['invoice_total_amount'] += $row->line_amount;
                $data_invoice['invoice_tax_amount'] += $row->line_vat_amount;
                $data_invoice['invoice_net_amount'] += $row->line_net_amount;
            }
        }
        $this->invoice_model->update_invoicepay_detail($invoice_id, $data_invoice);
        //$line_amount = $this->input->post('line_amount');
        //$line_old_net_amount = $this->input->post('line_old_net_amount');
        //$line_vat_amount=($line_amount/100) * $settings->service_vat_percentage;
        //$line_vat_amount=sprintf('%.2f', $line_vat_amount);;
        //$line_net_amount=$line_amount + $line_vat_amount;
        //$line_old_amount = $this->input->post('line_old_amount');
        //$line_old_vat_amount=($line_old_amount/100) * $settings->service_vat_percentage;
        //$line_old_vat_amount=sprintf('%.2f', $line_old_vat_amount);
        //$line_old_net_amount=$line_old_vat_amount + $line_old_amount;
        //$data=array();
        //$data['line_amount']=$line_amount;
        //$data['line_vat_amount']=$line_vat_amount;
        //$data['line_net_amount']=$line_net_amount;
        //$result=$this->invoice_model->update_invoicelineitems($line_id,$data);
        //$invoice_detail = $this->invoice_model->get_invoice_detailbyid($invoice_id);
        //$invoice_total_amount=$invoice_detail[0]->invoice_total_amount;;
        //$invoice_tax_amount=$invoice_detail[0]->invoice_tax_amount;
        //$invoice_net_amount=$invoice_detail[0]->invoice_net_amount;
        //$invoice_new_total_amount=$invoice_total_amount - ($line_old_amount - $line_amount);
        //$invoice_new_tax_amount=$invoice_tax_amount - ($line_old_vat_amount - $line_vat_amount);
        //$invoice_new_net_amount=$invoice_net_amount - ($line_old_net_amount - $line_net_amount);
        //$data_invoice=array();
        //$data_invoice['invoice_total_amount']=$invoice_new_total_amount;
        //$data_invoice['invoice_tax_amount']=$invoice_new_tax_amount;
        //$data_invoice['invoice_net_amount']=$invoice_new_net_amount;
        $data['invoice_total_amount'] = number_format($data_invoice['invoice_total_amount'],2);
        $data['invoice_tax_amount'] = number_format($data_invoice['invoice_tax_amount'],2);
        $data['invoice_net_amount'] = number_format($data_invoice['invoice_net_amount'],2);
        //$data['line_items'] = $line_items;


        $update_main_inv = array();
        $update_main_inv['invoice_total_amount'] = $data_invoice['invoice_total_amount'];
        $update_main_inv['invoice_tax_amount'] = $data_invoice['invoice_tax_amount'];
        $update_main_inv['invoice_net_amount'] = $data_invoice['invoice_net_amount'];
        $update_main_inv['balance_amount'] = $data_invoice['invoice_net_amount'];
        $updateinv = $this->day_services_model->update_service_invoice($invoice_id, $update_main_inv);


        if($invstat=='1'){
            $custid = $this->input->post('custid');
            $line_old_net_amount = $this->input->post('line_old_net_amount');
            $get_customer_detail = $this->invoice_model->get_customer_detail($custid);
            $inv_amt = $get_customer_detail->total_invoice_amount;
            $bal_amount = $get_customer_detail->balance;
            $new_inv_amt=$inv_amt-$line_old_net_amount+$line_net_amount;
            $new_bal_amount = $bal_amount-$line_old_net_amount+$line_net_amount;

            $custdata=array();
            $custdata['total_invoice_amount']=$new_inv_amt;
            $custdata['balance']=$new_bal_amount;
            $custdata['last_invoice_date']=date('Y-m-d');
            $this->invoice_model->update_customer_detail($custid,$custdata);
            $txt="$bal_amount-$line_old_net_amount+$line_net_amount";
            $data['txt']=$txt;
            $data['dtl']=$get_customer_detail;

        }
        die(json_encode($data));
    }
	
	public function validate_invoice()
	{
		$invoice_id = $this->input->post('invoice_id');
		$invoice_amt = $this->input->post('invoice_amt');
		$cust_id = $this->input->post('cust_id');
		$get_customer_detail = $this->invoice_model->get_customer_detail($cust_id);
		if(!empty($get_customer_detail))
		{
			$inv_amt = $get_customer_detail->total_invoice_amount;
			$bal_amount = $get_customer_detail->balance;
			$total_invoice = ($inv_amt + $invoice_amt);
			$total_balance = ($bal_amount + $invoice_amt);
			$update_invoice = $this->invoice_model->update_invoice_detail($invoice_id,$cust_id,$total_invoice,$total_balance);
			if($update_invoice)
			{
				echo "success";
				exit();
			} else {
				echo "error";
				exit();
			}
		} else {
			echo "error";
			exit();
		}
	}
	
	public function generateinvoice($invoiceid)
	{
        // ini_set('display_errors', 1);
        // ini_set('display_startup_errors', 1);
        // error_reporting(E_ALL);
		$this->load->library('pdf');
		$invoice_detail = $this->invoice_model->get_invoice_detailbyid($invoiceid);
        //var_dump($invoice_detail);die();
		$data = array();
        $data['settings'] = $this->settings_model->get_settings();
		$data['invoice_detail'] = $invoice_detail;
		$html_content = $this->load->view('invoicepdf', $data, TRUE);
		$this->pdf->loadHtml($html_content);
		$this->pdf->set_paper("a4", "portrait");
		$this->pdf->render();
        
		$this->pdf->stream("".$invoiceid.".pdf", array("Attachment"=>0));
        //file_put_contents(base_url().''.$invoiceid.".pdf", $this->pdf->output());
		exit();
	}
    public function generateinvoice_monthly($invoiceid)
    {
        // ini_set('display_errors', 1);
        // ini_set('display_startup_errors', 1);
        // error_reporting(E_ALL);
        $this->load->library('pdf');
        $invoice_detail = $this->invoice_model->get_invoice_detailbyid($invoiceid);
        $data = array();
        $data['invoice_detail'] = $invoice_detail;
        // echo '<pre>';print_r($data);die;
        $data['settings'] = $this->settings_model->get_settings();
        $html_content = $this->load->view('invoicepdf_monthly', $data, TRUE);
        $this->pdf->loadHtml($html_content);
        $this->pdf->set_paper("a4", "portrait");
        $this->pdf->render();
        
        $this->pdf->stream("".$invoiceid.".pdf", array("Attachment"=>0));
        //file_put_contents(base_url().''.$invoiceid.".pdf", $this->pdf->output());
        exit();
    }

    public function send_quotation_email()
    {
        //ini_set('display_errors', 1);
        //ini_set('display_startup_errors', 1);
        //error_reporting(E_ALL);
        $quoid=$this->input->post('quoid');
        $quotationcontent=$this->input->post('quotationcontent');
        $quotationemail=$this->input->post('quotationemail');
        $this->load->library('pdf');
        $quotation_detail = $this->invoice_model->get_quotation_detailbyid($quoid);
        $quotation_line_items = $this->invoice_model->get_quotation_line_items_byqid($quoid);
        $data = array();
        $data['quotation_detail'] = $quotation_detail;
        $data['quotation_line_items'] = $quotation_line_items;
        $html_content = $this->load->view('quotationpdf', $data, TRUE);
        $this->pdf->loadHtml($html_content);
        $this->pdf->set_paper("a4", "portrait");
        $this->pdf->render();
        
        //$customer_email=$invoice_detail[0]->email_address;
        
        file_put_contents('/var/www/html/emaid-demo/invoices/'.$quoid.".pdf", $this->pdf->output());
        
        $this->load->library('email');
        $config = array(
                    'protocol'  => 'smtp',
					'smtp_host' => 'ssl://smtp.googlemail.com',
					'smtp_port' => 465,
					'smtp_user' => 'emaidmaps@gmail.com',
					'smtp_pass' => 'viltazemmwrmrybw',
					'mailtype'  => 'html',
					'charset'   => 'utf-8'
              );

        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");
        $this->email->from('emaidmaps@gmail.com', 'Company Name');
        $this->email->to($quotationemail);
        $this->email->subject('Company Name Quotation Email');
        //$this->email->message("Sir ,<br/>&nbsp;&nbsp;&nbsp;&nbsp;Kindly check the attached invoice.");
        $this->email->message($quotationcontent);
        $this->email->attach('/var/www/html/emaid-demo/invoices/invoices/'.$quoid.".pdf");
        if($this->email->send())
        {
            echo "success";
        }
        else
        {
            if(strlen($quotationemail)<2)
            {echo "email_error";}
            else
            {echo "error";} 
            //echo $this->email->print_debugger();           
        }
        unlink('/var/www/html/emaid-demo/invoices/invoices/'.$quoid.".pdf");
        //$this->email->print_debugger();
        exit();
    }


    public function send_invoice_email()
    {
        //ini_set('display_errors', 1);
        //ini_set('display_startup_errors', 1);
        //error_reporting(E_ALL);
        $invoiceid=$this->input->post('invoiceid');
        $invoicecontent=$this->input->post('invoicecontent');
        $customer_email=$this->input->post('invoiceemail');
        $this->load->library('pdf');
        $invoice_detail = $this->invoice_model->get_invoice_detailbyid($invoiceid);
        $data = array();
        $data['invoice_detail'] = $invoice_detail;
        $html_content = $this->load->view('invoicepdf', $data, TRUE);
        $this->pdf->loadHtml($html_content);
        $this->pdf->set_paper("a4", "portrait");
        $this->pdf->render();
        
        //$customer_email=$invoice_detail[0]->email_address;
        
        file_put_contents($this->config->item('root_directory').'invoices/'.$invoiceid.".pdf", $this->pdf->output());
        
        $this->load->library('email');
        $config = array(
				'protocol'  => 'smtp',
				'smtp_host' => $this->config->item('smtp_host'),
				'smtp_port' => $this->config->item('smtp_port'),
				'smtp_user' => $this->config->item('smtp_user'),
				'smtp_pass' => $this->config->item('smtp_pass'),
				'mailtype'  => 'html',
				'crlf' => "\r\n",
				'newline' => "\r\n"
			);

        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");
        $this->email->from($this->config->item('mail_admin'), $this->config->item('company_name'));
        $this->email->to($customer_email);
        $this->email->subject($this->config->item('site_name').' Invoice Email');
        //$this->email->message("Sir ,<br/>&nbsp;&nbsp;&nbsp;&nbsp;Kindly check the attached invoice.");
        $this->email->message($invoicecontent);
        $this->email->attach($this->config->item('root_directory').'invoices/'.$invoiceid.".pdf");
        if($this->email->send())
        {
            echo "success";
        }
        else
        {
            $errors = $this->email->print_debugger();
            if(strlen($customer_email)<2)
            {echo "email_error";}
            else
            {echo $errors;}            
        }
        unlink($this->config->item('root_directory').'invoices/'.$invoiceid.".pdf");
        //$this->email->print_debugger();
        exit();
    }


    public function send_invoice_email_monthly()
    {
        //ini_set('display_errors', 1);
        //ini_set('display_startup_errors', 1);
        //error_reporting(E_ALL);
        $invoiceid=$this->input->post('invoiceid');
        $invoicecontent=$this->input->post('invoicecontent');
        $customer_email=$this->input->post('invoiceemail');
        $this->load->library('pdf');
        $invoice_detail = $this->invoice_model->get_invoice_detailbyid($invoiceid);
        $data = array();
        $data['invoice_detail'] = $invoice_detail;
        $html_content = $this->load->view('invoicepdf_monthly', $data, TRUE);
        $this->pdf->loadHtml($html_content);
        $this->pdf->set_paper("a4", "portrait");
        $this->pdf->render();
        
        //$customer_email=$invoice_detail[0]->email_address;
        
        file_put_contents($this->config->item('root_directory').'invoices/'.$invoiceid.".pdf", $this->pdf->output());
        
        $this->load->library('email');
        $config = array(
				'protocol'  => 'smtp',
				'smtp_host' => $this->config->item('smtp_host'),
				'smtp_port' => $this->config->item('smtp_port'),
				'smtp_user' => $this->config->item('smtp_user'),
				'smtp_pass' => $this->config->item('smtp_pass'),
				'mailtype'  => 'html',
				'crlf' => "\r\n",
				'newline' => "\r\n"
			);

        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");
        $this->email->from($this->config->item('mail_admin'), $this->config->item('company_name'));
        $this->email->to($customer_email);
        $this->email->subject($this->config->item('site_name').' Invoice Email');
        //$this->email->message("Sir ,<br/>&nbsp;&nbsp;&nbsp;&nbsp;Kindly check the attached invoice.");
        $this->email->message($invoicecontent);
        $this->email->attach($this->config->item('root_directory').'invoices/'.$invoiceid.".pdf");
        if($this->email->send())
        {
            echo "success";
        }
        else
        {
            if(strlen($customer_email)<2)
            {echo "email_error";}
            else
            {echo "error";}            
        }
        unlink($this->config->item('root_directory').'invoices/'.$invoiceid.".pdf");
        //$this->email->print_debugger();
        exit();
    }

    public function update_invoice_issue_date()
    {
        $invoiceid=$this->input->post('invoice_id');
        $new_issue_date=$this->input->post('new_issue_date');
        list($day, $month, $year) = explode("/", $new_issue_date);
        $new_issue_date = "$year-$month-$day";
        $data=array();
        $data['invoice_date']=$new_issue_date;
        $invoice_detail = $this->invoice_model->update_invoicepay_detail($invoiceid,$data);
        if($invoice_detail >= 0){echo "success";}else{echo "fail";}
        exit();
    }

    public function update_invoice_due_date()
    {
        $invoiceid=$this->input->post('invoice_id');
        $new_due_date=$this->input->post('new_due_date');
        list($day, $month, $year) = explode("/", $new_due_date);
        $new_due_date = "$year-$month-$day";
        $data=array();
        $data['invoice_due_date']=$new_due_date;
        $invoice_detail = $this->invoice_model->update_invoicepay_detail($invoiceid,$data);
        if($invoice_detail >= 0){echo "success";}else{echo "fail";}
        exit();
    }

    public function generateinvoice_test($invoiceid)
    {
        
        $this->load->library('pdf');
        $invoice_detail = $this->invoice_model->get_invoice_detailbyid($invoiceid);
        $data = array();
        $data['invoice_detail'] = $invoice_detail;
        $html_content = $this->load->view('invoicepdf_test', $data, TRUE);
        $this->pdf->loadHtml($html_content);
        $this->pdf->set_paper("a4", "portrait");
        $this->pdf->render();        
        $this->pdf->stream("".$invoiceid.".pdf", array("Attachment"=>0));
        
        exit();
    }


    public function generateinvoice_test2($invoiceid)
    {
        
        $this->load->library('pdf');
        $invoice_detail = $this->invoice_model->get_invoice_detailbyid($invoiceid);
        $data = array();
        $data['invoice_detail'] = $invoice_detail;
        $html_content = $this->load->view('invoicepdf_test2', $data, TRUE);
        $this->pdf->loadHtml($html_content);
        $this->pdf->set_paper("a4", "portrait");
        $this->pdf->render();        
        $this->pdf->stream("".$invoiceid.".pdf", array("Attachment"=>0));
        
        exit();
    }
	
    public function cancel_invoice_new()
	{
        // Update by Samnad. S
        $invoice_id = $this->input->post('invoice_id');
        $invoice = $this->invoice_model->get_invoice_row($invoice_id);
        $customer = $this->customers_model->get_customer($invoice->customer_id);
        $this->db->trans_begin();
        if($invoice->invoice_status == 0){ // non validated invoice
            // update invoice table
            $invoice_update_data = array();
            $invoice_update_data['invoice_status'] = 2;
            $update_invoice = $this->invoice_model->update_invoice_row($invoice_id,$invoice_update_data);
            // update day services
            $dayservice_update_data = array();
            $dayservice_update_data['invoice_status'] = 0;
            $dayservice_update = $this->day_services_model->update_day_services_where(array('serv_invoice_id'=>$invoice_id),$dayservice_update_data);
            $this->db->trans_commit();
            die("success");
		}
        else if($invoice->invoice_status == 1){ // validated invoice
            // update invoice table
            $invoice_update_data = array();
            $invoice_update_data['invoice_status'] = 2;
            $update_invoice = $this->invoice_model->update_invoice_row($invoice_id,$invoice_update_data);
            // update day services
            $dayservice_update_data = array();
            $dayservice_update_data['invoice_status'] = 0;
            $dayservice_update = $this->day_services_model->update_day_services_where(array('serv_invoice_id'=>$invoice_id),$dayservice_update_data);
            // update customer table
            $customer_update_data = array();
            $customer_update_data['total_invoice_amount'] = $customer->total_invoice_amount - $invoice->invoice_net_amount;
            $total_paid_amount = $customer->total_paid_amount;
            if($invoice->invoice_paid_status == 1){ // paid invoice
                $customer_update_data['total_paid_amount'] = $customer->total_paid_amount - $invoice->invoice_net_amount;
                $total_paid_amount = $customer_update_data['total_paid_amount'];
            }
            $customer_update_data['balance'] = $customer_update_data['total_invoice_amount'] - $total_paid_amount;
            $customer_update = $this->customers_model->update_customer_row($customer->customer_id,$customer_update_data);
            $this->db->trans_commit();
            die("success");
        }
        else{
            $this->db->trans_rollback();
            die("error");
        }

    }
	public function cancel_invoice_old()
	{
        $this->db->trans_begin();
		$invoice_id = $this->input->post('invoice_id');
		$invoice_amt = $this->input->post('invoice_amt');
		$cust_id = $this->input->post('cust_id');
		$invoice_status = $this->input->post('inv_status');
		
		if($invoice_status == 0)
		{
			$update_invoice = $this->invoice_model->update_invoice_status($invoice_id);
			if($update_invoice)
			{
                $this->db->trans_commit();
				echo "success";
				exit();
			} else {
                $this->db->trans_rollback();
				echo "error";
				exit();
			}
		} else if($invoice_status == 1)
		{
			$get_customer_detail = $this->invoice_model->get_customer_detail($cust_id);
            $invoice = $this->invoice_model->get_invoice_row($invoice_id);
			if(!empty($get_customer_detail))
			{
				$inv_amt = $get_customer_detail->total_invoice_amount;
				$last_inv_date = $get_customer_detail->last_invoice_date;
				$last_date = date('Y-m-d', strtotime('-1 day', strtotime($last_inv_date)));
				$bal_amount = $get_customer_detail->balance;
				$total_invoice = ($inv_amt - $invoice->invoice_net_amount);
				$total_balance = ($bal_amount - $invoice->invoice_net_amount);
				$update_invoice = $this->invoice_model->update_invoice_status_detail($invoice_id,$cust_id,$total_invoice,$total_balance,$last_date);
				if($update_invoice >= 0)
				{
                    $this->db->trans_commit();
					echo "success";
					exit();
				} else {
                    $this->db->trans_rollback();
					echo "error";
					exit();
				}
			} else {
                $this->db->trans_rollback();
				echo "error";
				exit();
			}
		} else {
            $this->db->trans_rollback();
			echo "error";
			exit();
		}
	}
	
	public function create_invoice()
	{
		$data = array();
        $data['settings'] = $this->settings_model->get_settings();
		$layout_data = array();
        $layout_data['content_body'] = $this->load->view('create_invoice_new', $data, TRUE);		
        $layout_data['page_title'] = 'Create Invoice';
        $layout_data['meta_description'] = 'Create Invoice';
        $layout_data['css_files'] = array('datepicker.css','jquery.fancybox.css','demo.css','toastr.min.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('bootstrap-datepicker.js','jquery.validate.min.js','create_invoice.js');

        $this->load->view('layouts/default_dashboard', $layout_data);
	}
	
	public function add_new_invoice()
	{
        $settings = $this->settings_model->get_settings();
        $this->db->trans_begin();
		$issuedate = $this->input->post('invoiceissuedate');
        $s_date = explode("/", $issuedate);
        $jobinvoiceissuedate = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        $duedate = $this->input->post('invoiceduedate');
        $s_date1 = explode("/", $duedate);
        $jobinvoiceduedate = $s_date1[2] . '-' . $s_date1[1] . '-' . $s_date1[0];
		$customer_id = $this->input->post('customers_vh_rep_new_inv');
		$get_customer_address = $this->invoice_model->get_customer_invoice_address($customer_id);
		$address = $get_customer_address->customer_address;
		$description = $this->input->post('invoicedescription');
		$amount = $this->input->post('invoiceamount');
		$vatamount = $this->input->post('invoicevatamount');
		$netamount = $this->input->post('invoicenetamount');
		$added_date_time = date('Y-m-d H:i:s');
		$invoice_array = array();
		$invoice_array['customer_id'] = $customer_id;
		$invoice_array['customer_name'] = $get_customer_address->customer_name;
		$invoice_array['bill_address'] = $get_customer_address->customer_address;
		$invoice_array['added'] = $added_date_time;
		$invoice_array['service_date'] = $jobinvoiceissuedate;
		$invoice_array['invoice_date'] = $jobinvoiceissuedate;
		$invoice_array['invoice_due_date'] = $jobinvoiceduedate;
		$invoice_array['invoice_added_by'] = user_authenticate();
		$invoice_array['invoice_status'] = 1;	
        //$invoice_array['invoice_total_amount'] = $amount;
		//$invoice_array['invoice_tax_amount'] = $vatamount;
		$invoice_array['invoice_net_amount'] = $netamount;
        $invoice_array['invoice_tax_amount'] = ($netamount * $settings->service_vat_percentage) / (100 + $settings->service_vat_percentage);
        $invoice_array['invoice_total_amount'] = $netamount - $invoice_array['invoice_tax_amount'];
        $invoice_array['balance_amount'] = $netamount;
		$invoice_array['invoice_notes'] = $description;
		$invoice_id = $this->day_services_model->add_main_invoice($invoice_array);
		if($invoice_id > 0)
		{
			$main_inv_line = array();
			$main_inv_line['invoice_id'] = $invoice_id;
			$main_inv_line['day_service_id'] = null;
			$main_inv_line['maid_id'] = null;
			$main_inv_line['line_bill_address'] = $get_customer_address->customer_address;
			$main_inv_line['description'] = $description;
			//$main_inv_line['line_amount'] = $amount;
			//$main_inv_line['line_vat_amount'] = $vatamount;
			$main_inv_line['line_net_amount'] = $netamount;
            $main_inv_line['line_vat_amount'] = ($netamount * $settings->service_vat_percentage) / (100 + $settings->service_vat_percentage);
            $main_inv_line['line_amount'] = $netamount - $main_inv_line['line_vat_amount'];
			$invoice_line_id = $this->day_services_model->add_line_invoice($main_inv_line);
			$update_main_inv = array();
			$update_main_inv['invoice_num'] = "INV-".date('Y')."-".sprintf('%04s', $invoice_id);
			$updateinv = $this->day_services_model->update_service_invoice($invoice_id, $update_main_inv);
            $customer =  $this->customers_model->get_customer($customer_id);
            $this->customers_model->update_customers(array('last_invoice_date'=>date('Y-m-d'),'total_invoice_amount'=>$customer->total_invoice_amount+$netamount,'balance'=>$customer->balance+$netamount), $customer_id);
			if($updateinv)
			{
                $this->db->trans_commit();
				redirect('invoice/view_invoice/'.$invoice_id);
				exit();
			}
		}
        else{
            $this->db->trans_rollback();
        }
	}
	
	
	
    
    public function add_quotation()
    {
        $schedule_date = date('d-M-Y');
        $day_number = date('w', strtotime($schedule_date));
        $times = array();
        $current_hour_index = 0;
        $time = '12:00 am';  
        $time_stamp = strtotime($time);

        for ($i = 0; $i < 24; $i++)
        {
                if(!isset($times['t-' . $i]))
                {
                        $times['t-' . $i] = new stdClass();
                }

                $times['t-' . $i]->stamp = $time_stamp;
                $times['t-' . $i]->display = $time;

                
                if(date('H') == $i)
                {
                        $current_hour_index = 't-' . ($i - 1);
                }

                $time_stamp = strtotime('+60mins', strtotime($time));
                $time = date('g:i a', $time_stamp);
        }
        if($this->input->is_ajax_request())
        {
            if($this->input->post('action') && $this->input->post('action') == 'get-no-of-customer-address' && $this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0)
            {
                $customer_id = trim($this->input->post('customer_id'));
                $customer_addresses = $this->customers_model->get_customer_addresses($customer_id);
                echo json_encode(array("address" => $customer_addresses, "no_of_address" => count($customer_addresses))); 
                //echo 'refresh';
                exit();
            }
            
            if($this->input->post('action') && $this->input->post('action') == 'get-details-customer' && $this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0)
            {
                $customer_id = trim($this->input->post('customer_id'));
                $customer_details = $this->customers_model->get_customer_by_id($customer_id);
                echo json_encode($customer_details); 
                exit();
            }
            
            if($this->input->post('action') && $this->input->post('action') == 'get-customer-address' && $this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0)
            {
                $customer_id = trim($this->input->post('customer_id'));

                $customer_addresses = $this->customers_model->get_customer_addresses($customer_id);


                echo json_encode($customer_addresses); 
                exit();
            }

            if($this->input->post('action') && $this->input->post('action') == 'add-quotation' && $this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0)
            {
                $customer_id = trim($this->input->post('customer_id'));
                $customer_address_id = trim($this->input->post('customer_address_id'));
                $service_type_id = trim($this->input->post('service_type_id'));
                $time_from =  date('H:i', trim($this->input->post('time_from')));
                $time_to = date('H:i', trim($this->input->post('time_to')));
                $quo_date = trim($this->input->post('quo_date'));
                $cleaning_material = $this->input->post('cleaning_material');
                $total_amount = 0;
                $mail = $this->input->post('mail');
                $phone = $this->input->post('phone');
                $service_data = $this->input->post('service_data');
                
                
                foreach ($service_data as $sdt) {
                    $total_amount+=$sdt['serv_price'];
                }
                
                $get_customer_detail = $this->customers_model->get_customer_details($customer_id);
                $get_customer_addr_detail = $this->customers_model->get_customer_address_by_id($customer_address_id);
                $addressss='';
                if($get_customer_addr_detail->customer_address == "")
                {
                    $addressss = 'Building - '.$get_customer_addr_detail->building.', '.$get_customer_addr_detail->unit_no.''.$get_customer_addr_detail->street;
                } 
                else 
                {
                    if($get_customer_addr_detail->building != "")
                    {
                        $addressss = "Apt No: ".$get_customer_addr_detail->building.", ".$get_customer_addr_detail->customer_address;
                    } else {
                        $addressss = $get_customer_addr_detail->customer_address;
                    }
                    
                }
                //echo 'custname='.$get_customer_detail[0]['customer_name'];
                //print_r($get_customer_detail);exit();
                $net_amount=round(($total_amount/1.05),2);
                $tax_amount=$total_amount - $net_amount;
                $data=array();
                $data['quo_customer_id']=$customer_id;
                $data['quo_customer_addr_id']=$customer_address_id;
                $data['quo_customer_name']=$get_customer_detail[0]['customer_name'];
                $data['quo_bill_address']=$addressss;
                $data['quo_status']=1;
                $data['quo_added_datetime']=date("Y-m-d H::i:s");
                //$data['quo_date']=date("Y-m-d");
                $data['quo_date']=$quo_date;
                $data['quo_total_discount']=0;
                $data['quo_total_amount']=$total_amount;
                $data['quo_time_from']=$time_from;
                $data['quo_time_to']=$time_to;
                $data['quo_tax_amount']=$tax_amount;
                $data['quo_net_amount']=$net_amount;
                $data['quo_added_by']=user_authenticate();
                $data['quo_qb_id']='0';
                $data['quo_qb_sync_stat']='0';
                $data['quo_servicetype_id']=$service_data[0]['serv_id'];

                $add_quotation = $this->invoice_model->add_quotation($data);
                if($add_quotation)
                {
                    foreach ($service_data as $sdt) 
                    {

                        $line_array=array();
                        $line_array['qli_service_type_id']=$sdt['serv_id'];
                        $line_array['qli_description']=$sdt['serv_description'];
                        $line_array['qli_quotation_id']=$add_quotation;
                        $line_array['qli_rate']=$sdt['serv_rate'];
                        $line_array['qli_quantity']=$sdt['serv_qty'];
                        $line_array['qli_price']=$sdt['serv_price'];

                        $add_quotation_line_item = $this->invoice_model->add_quotation_line_item($line_array);
                    }
                	
					$idlength = strlen((string)$add_quotation);
					$ref=($idlength>=4)?$add_quotation:sprintf('%04u', $add_quotation);
                	$update_fields=array();$update_fields['quo_reference']='QTN'.$ref;
                	$this->invoice_model->update_quotation($add_quotation,$update_fields);
                }
                $response = array();
                if($add_quotation){$response['status'] = 'success';}
                else{$response['status'] = 'error';}

                echo json_encode($response); 
                exit();
            }
            
            
        }
        
        $maids_leave_on_date = $this->maids_model->get_maids_leave_by_date(date('Y-m-d'));
        $leave_maid_ids = array();
        foreach ($maids_leave_on_date as $leave)
                {
                    array_push($leave_maid_ids, $leave->maid_id);
                }
                
        
        
        $maids = $this->maids_model->get_maids();
        $service_types = $this->service_types_model->get_service_types();
        $data = array();
        $data['maids'] = $maids;
        $data['service_types'] = $service_types;
        $data['times'] = $times;
        $data['day_number'] = $day_number;
        $data['leave_maid_ids'] = $leave_maid_ids;
        $layout_data = array();
        $layout_data['content_body'] = $this->load->view('add_quotation', $data, TRUE);       
        $layout_data['page_title'] = 'New Job';
        $layout_data['meta_description'] = 'New Job';
        $layout_data['css_files'] = array('jquery.fancybox.css', 'datepicker.css', 'bootstrap-datetimepicker.min.css');
        $layout_data['jobs_active'] = '1';
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.fancybox.pack.js', 'bootstrap-datetimepicker.min.js', 'bootstrap-datepicker.js', 'quotation.js', 'moment.min.js','bootstrap.js');

        $this->load->view('layouts/default_job', $layout_data);
    }

    function list_customer_quotations() 
    {
        $data = array();
        $date_from = NULL;
        $date_to = NULL;
        $customer_id = NULL;
        $data['search_date_from'] = NULL;
        $data['search_date_to'] = NULL;

        if($this->input->post())
        {
            if($this->input->post('customers_vh_rep') > 0)
            {
                $customer_id = $this->input->post('customers_vh_rep');
            } else {
                $customer_id = NULL;
            }            
            
            
            if($this->input->post('vehicle_date') != "")
            {
                $from_date = $this->input->post('vehicle_date');
                $date_from = date('Y-m-d', strtotime(str_replace("/", "-", $from_date)));
                $data['search_date_from'] = date('d/m/Y', strtotime($date_from));
            } else {
                $date_from = NULL;
                $data['search_date_from'] = NULL;
            }
            
            if($this->input->post('vehicle_date_to') != "")
            {
                $to_date = $this->input->post('vehicle_date_to');
                $date_to = date('Y-m-d', strtotime(str_replace("/", "-", $to_date)));
                $data['search_date_to'] = date('d/m/Y', strtotime($date_to));
            } else {
                $date_to = NULL;
                $data['search_date_to'] = NULL;
            }
        }
        $data['customer_id'] = $customer_id;
        $data['inv_status'] = $inv_status;
        $quotations=$this->invoice_model->get_customer_quotations($date_from,$date_to,$customer_id);
        $data['quotations']=$quotations;
        $data['customerlist'] = $this->invoice_model->get_customer_list();
        $layout_data['content_body'] = $this->load->view('customer_quotation_list', $data, TRUE);
        $layout_data['page_title'] = 'Customer Quotations';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }

    public function view_quotation($quotation_id)
	{
		$quotation_detail = $this->invoice_model->get_quotation_detailbyid($quotation_id);
        $quotation_line_items = $this->invoice_model->get_quotation_line_items_byqid($quotation_id);
		$data = array();
		$data['quotation_detail'] = $quotation_detail;
        $data['quotation_line_items'] = $quotation_line_items;
		
		$layout_data = array();
        $layout_data['content_body'] = $this->load->view('view_quotation_new', $data, TRUE);		
        $layout_data['page_title'] = 'Quotation View';
        $layout_data['meta_description'] = 'Quotation View';
        $layout_data['css_files'] = array('jquery.fancybox.css','demo.css','toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.fancybox.pack.js','base.js', 'bootstrap-datepicker.js','jquery.validate.min.js','toastr.min.js','mymaids.js','ajaxupload.3.5.js');

        $this->load->view('layouts/default_dashboard', $layout_data);
	}
    public function view_invoice_monthly_advanced($invoice_id){
        $invoice_detail = $this->invoice_model->get_invoice_detailbyid($invoice_id);
        $data = array();
        $data['invoice_detail'] = $invoice_detail;
        $data['settings'] = $this->settings_model->get_settings();
        $data['invoice'] = $this->invoice_model->get_monthly_advanced_invoice_main_row(array('id'=>$invoice_id));
        if(!$data['invoice']){
            redirect('invoice/list/monthly-advanced?invoice-not-found');
        }
        $data['invoice_lines'] = $this->invoice_model->get_monthly_advanced_invoice_lines(array('invoice_id'=>$invoice_id));
        //print_r($data['invoice_lines']);die();
        $layout_data = array();
        $layout_data['content_body'] = $this->load->view('invoice/view_invoice_monthly_advanced', $data, TRUE);      
        $layout_data['page_title'] = 'Invoice View';
        $layout_data['meta_description'] = 'Invoice View';
        $layout_data['css_files'] = array('jquery.fancybox.css','demo.css','toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.fancybox.pack.js','base.js', 'bootstrap-datepicker.js','jquery.validate.min.js','toastr.min.js','mymaids.js','ajaxupload.3.5.js');

        $this->load->view('layouts/default_dashboard', $layout_data);
    }
    function monthly_advanced_invoice_update(){
        if($this->input->is_ajax_request())
        {
            if($this->input->post('action') == 'validate-invoice'){
                $data['invoice'] = $this->invoice_model->get_monthly_advanced_invoice_main_row(array('id'=>$this->input->post('invoice_id')));
                if(!$data['invoice']){
                    die(json_encode(array('status'=>false,'message'=>'Invoice not found !','type'=>'error')));
                }
                else if($data['invoice']->validation_status == 1){
                    die(json_encode(array('status'=>false,'message'=>'Invoice already validated !','type'=>'warning')));
                }
                else if($data['invoice']->status == 0){
                    die(json_encode(array('status'=>false,'message'=>'Cancelled invoice can\'t be validated !','type'=>'warning')));
                }
                else{
                    if($this->invoice_model->update_monthly_advanced_invoice_main(array('id'=>$this->input->post('invoice_id')),array('validation_status'=>1,'validated_at'=>'NOW()'))){
                        die(json_encode(array('status'=>true,'message'=>'Invoice validated successfully !','type'=>'success')));
                    }
                    else{
                        die(json_encode(array('status'=>false,'message'=>'Validation task failed !','type'=>'error')));
                    }
                }
            }
            else if($this->input->post('action') == 'cancel-invoice'){
                $data['invoice'] = $this->invoice_model->get_monthly_advanced_invoice_main_row(array('id'=>$this->input->post('invoice_id')));
                if(!$data['invoice']){
                    die(json_encode(array('status'=>false,'message'=>'Invoice not found !','type'=>'error')));
                }
                else if($data['invoice']->status == 0){
                    die(json_encode(array('status'=>false,'message'=>'Invoice already cancelled !','type'=>'warning')));
                }
                else if($data['invoice']->invoice_paid_status == 1){
                    die(json_encode(array('status'=>false,'message'=>'Paid invoice can\'t be cancelled ! !','type'=>'warning')));
                }
                else{
                    if($this->invoice_model->update_monthly_advanced_invoice_main(array('id'=>$this->input->post('invoice_id')),array('validation_status'=>0,'validated_at'=>$this->db->escape(null),'status'=>0))){
                        die(json_encode(array('status'=>true,'message'=>'Invoice cancelled successfully !','type'=>'success')));
                    }
                    else{
                        die(json_encode(array('status'=>false,'message'=>'Cancellation task failed !','type'=>'error')));
                    }
                }
            }
            else{
                die(json_encode(array('status'=>false,'message'=>'Update action not specified !','type'=>'error')));
            }
        }
    }
	
	public function nullify_invoice()
	{
		$invoice_id = $this->input->post('invoice_id');
		$update_invoice = $this->invoice_model->update_invoice_nullify($invoice_id);
		if($update_invoice)
		{
			echo "success";
			exit();
		} else {
			echo "error";
			exit();
		}
	}
}