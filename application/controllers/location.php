<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Location extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('bookings_model');
        $this->load->model('driver_model');
        $this->load->model('settings_model');
    }
    public function index()
    {
        redirect('dashboard');
    }
    public function driver()
    {
        //$current_date = date('Y-m-d');
        if ($this->input->post('service_date')) {
            $s_date = $this->input->post('service_date');
            $s_date = explode("/", $s_date);
            $date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        } else {
            $date = date('Y-m-d');
            //$service_date = '2015-06-17';
        }
        $tablet_locations = $this->driver_model->get_tablets_location_by_date($date);
        //header('Content-Type: application/json');echo json_encode($tablet_locations,JSON_PRETTY_PRINT);die();
        $data = array();
        $data['settings'] = $this->settings_model->get_settings();
        $data['formatted_date'] = $date;
        $data['service_date'] = date('d/m/Y', strtotime($date));
        $data['dataarray'] = $this->driver_model->get_tablets_location_by_date($date);
        //$job_json = json_encode($dataarray);
        $layout_data = array();
        $layout_data['content_body'] = $this->load->view('driver_locations', $data, TRUE);
        $layout_data['page_title'] = 'Driver Locations';
        $layout_data['meta_description'] = 'Maps';
        $layout_data['css_files'] = array('demo.css');
        $layout_data['maps_active'] = '1';
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array();
        $this->load->view('layouts/default', $layout_data);
    }
    public function booking()
    {
        //$current_date = date('Y-m-d');
        if ($this->input->post('service_date')) {
            $s_date = $this->input->post('service_date');
            $s_date = explode("/", $s_date);
            $date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        } else {
            $date = date('Y-m-d');
            //$service_date = '2015-06-17';
        }
        $get_today_job_details = $this->bookings_model->get_today_job_details($date);
        $data = array();
        $data['settings'] = $this->settings_model->get_settings();
        $data['formatted_date'] = $date;
        $data['service_date'] = date('d/m/Y', strtotime($date));
        $data['dataarray'] = $get_today_job_details; //\array_map('array_values', $get_today_job_details);
        //$job_json = json_encode($dataarray);
        $layout_data = array();
        $layout_data['content_body'] = $this->load->view('booking_locations', $data, TRUE);
        $layout_data['page_title'] = 'Booking Locations';
        $layout_data['meta_description'] = 'Maps';
        $layout_data['css_files'] = array('demo.css');
        $layout_data['maps_active'] = '1';
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('bootstrap-datepicker.js');
        $this->load->view('layouts/default', $layout_data);
    }
}