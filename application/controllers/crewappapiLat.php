<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class CrewappapiLat extends CI_Controller {
	public function __construct() {
        parent::__construct();

        $this->load->model('crew_appapi_lat_model');
        $this->load->helper('google_api_helper');
		$this->load->model('day_services_model');
        $this->load->model('settings_model');
        $this->load->model('invoice_model');
        $this->load->helper('invoice_helper');
    }
	
	public function index() {
        $response               = array();
        $response['status']     = 'error';
        $response['error_code'] = '101';
        $response['message']    = 'Invalid request';

        echo json_encode($response);
        exit();
    }
	
	public function appLogin() 
    {
        $username           = $this->input->post('username');
        $userpassword       = $this->input->post('userpassword');
        $datee              = $this->input->post('date');
        $maid_driver_stat   = $this->input->post('maid_driver_status');
        $device_token       = $this->input->post('device_regid');
        if (is_numeric($maid_driver_stat) && strlen($datee)>0 && strlen($username)>0 && strlen($userpassword)>0) 
        {
            if($maid_driver_stat==0)
            {
                $this->maidLoginNew($username,$userpassword,$device_token);
            }
            else
            {
                $this->tabLoginNew($username,$userpassword,$device_token);
            }
        }
        else
        {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '1012';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
	
	public function maidLoginNew($username,$password,$device_token)
	{
        if ($username && strlen(trim($username)) > 0 && $password && strlen(trim($password)) > 0) 
        {
            
            $maidLogin = $this->crew_appapi_lat_model->get_maid_login($username, $password);
            if ($maidLogin)
            {
                $userdetails=array();
                $userdetails['id']    = (int)$maidLogin->maid_id;
                $userdetails['name']  = (string)$maidLogin->maid_name;
                $userdetails['phone'] = $maidLogin->maid_mobile_1;
                $userdetails['email'] = '';
                $userdetails['image'] = '';
                $maid_image           =  $maid_details->maid_photo_file;
	            if(strlen($maid_image)>0)
	            {
	                
	                if(file_exists(FCPATH.'maidimg/'.$maid_image))
	                {
	                    $userdetails['image'] = base_url().'maidimg/'.$maid_image;    
	                }                    
                    else
                    {
                        $maid_image = '';
                    }
	            }
                //$token=(string)substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
                $token=bin2hex(random_bytes(20));
                $userdetails['driver_status']=0;
                $userdetails['token']=$token;
                if(strlen($device_token)>0)
                {
                    $userdetails['maid_device_token']=$device_token;
					$this->crew_appapi_lat_model->update_maid($maidLogin->maid_id,array("maid_login_token" => $token,"maid_device_token" => $device_token));
                } else {
					$this->crew_appapi_lat_model->update_maid($maidLogin->maid_id,array("maid_login_token" => $token));
                }
				echo json_encode(array("status" => "success","message" => "Loged in Successfully","userdetails"=>$userdetails));
                // $response = array();
                //     $response["status"] = "failed";
                //     $response["message"] = "Invalid data";
                //     echo json_encode($response);
                    exit();
            }
            else
                {echo json_encode(array("status" => "failed","message" => "Log-in error!"));}
        }
        else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Missing parametres';

            echo json_encode($response);
            exit();
        }
    }
	
	public function tabLoginNew($username,$password,$device_token)
	{
        if ($username && strlen(trim($username)) > 0 && $password && strlen(trim($password)) > 0)
		{
            $tabLogin = $this->crew_appapi_lat_model->loginTabletNew($username, $password);
            if ($tabLogin)
			{
				$userdetails=array();
				$userdetails['id']=(int)$tabLogin->tablet_id;
				$userdetails['name']=(string)$tabLogin->tablet_driver_name;
				$userdetails['zone_name']=(string)$tabLogin->zone_name;
				$userdetails['phone']='';
				$userdetails['email']='';
				$userdetails['image']='';
				// $token=(string)substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
				$token=bin2hex(random_bytes(20));
				$userdetails['driver_status']=1;
				$userdetails['token']=$token;
				if(strlen($device_token)>0)
				{
					$userdetails['google_reg_id']=$device_token;
					$uparray = array();
					$uparray['tablet_login_token'] = $token;
					$uparray['google_reg_id'] = $device_token;
				} else {
					$uparray = array();
					$uparray['tablet_login_token'] = $token;
				}
                    
				$this->crew_appapi_lat_model->update_tablet($tabLogin->tablet_id,$uparray);
				echo json_encode(array("status" => "success","message" => "Loged in Successfully","userdetails"=>$userdetails));
				exit();
			} else {
				echo json_encode(array("status" => "failed","message" => "Log-in error!"));
			}
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Missing parametres';

            echo json_encode($response);
            exit();
        }
    }
	
	public function getbookings_schedule()
    {
		$id=$this->input->post('id');
        $datee=$this->input->post('date');
        $maid_driver_stat=$this->input->post('maid_driver_stat');
        $token=$this->input->post('token');
		
		// $id=1;
        // $datee="2021-09-14";
        // $maid_driver_stat=1;
        // $token="11fdeaa2e3dfa30794b02439259dd4f1bc6b7443";

        if (strlen($id)>0 && is_numeric($maid_driver_stat) && strlen($datee)>0 ) 
        {
			if($maid_driver_stat=='0')
			{
				$token_check = $this->crew_appapi_lat_model->token_maid_check($id, $token);
				if(isset($token_check->maid_id))
				{
					$this->getschedulesbymaid_new($id,$datee);
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			} else {
				$token_tab_check = $this->crew_appapi_lat_model->token_tablet_check($id, $token);
				if(isset($token_tab_check->tablet_id))
				{
					$this->getschedule_new($id,$datee);
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			}
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid requestff';

            echo json_encode($response);
            exit();
        }
    }
	
	public function getschedulesbymaid_new($maid_id,$service_date) 
    {
        if (is_numeric($maid_id) && $service_date ) 
        {
            $booking_id = 0;
            $schedule  = $this->get_maid_schedules($service_date,$maid_id);

            $schedule1 = $this->get_maid_schedules(date('Y-m-d', strtotime($service_date. ' + 1 day')),$maid_id);

            $schedule2 = $this->get_maid_schedules(date('Y-m-d', strtotime($service_date. ' + 2 days')),$maid_id);
			
			echo json_encode(array('status' => 'success','message' => 'Retrieved maid schedules successfully', 'schedule' => array_merge($schedule,$schedule1,$schedule2)));
            exit();
            
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '1015';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
	
	public function get_maid_schedules($service_date,$maid_id)
    {
        $bookings = $this->crew_appapi_lat_model->get_schedule_by_date_for_tab_by_maid_id($service_date,$maid_id);
        $bookingsarray = (array) $bookings;
        $bookings_time_from=array_column($bookingsarray, 'time_from');
        $schedule = array();
        $i = 0;
        foreach ($bookings as $booking) 
        {
            $service_status=0;
            if($booking->customer_address == "")
            {
                $a_address = $booking->building.', '.$booking->unit_no.''.$booking->street;
            } 
            else 
            {
                if($booking->building != "")
                {
                    $t_address = "Apt No: ".$booking->building.", ".$booking->customer_address;
                } else {
                    $t_address = $booking->customer_address;
                }
                $a_address = htmlspecialchars($t_address, ENT_QUOTES, 'UTF-8');
            }
            
            

            $zonename = $booking->zone_name;
            
            if($booking->driver_name != "")
            {
                $driver = " (".$booking->driver_name.")";
            } else {
                $driver = "";
            }
            $payment_status=0;$paid_amount=0;
            if($booking->booking_id != $booking_id)
            {
                $day_service = $this->crew_appapi_lat_model->get_day_service_by_booking_id($service_date, $booking->booking_id);
                if(!empty($day_service))
                {
                    $pick_zone      = $day_service->zone_name.' ('.$day_service->driver_name.')';
                    $payment_status = $day_service->payment_status;
                    $paid_amount    = $day_service->paid_amount;
                    $paid_amount    = is_numeric($paid_amount)?$paid_amount:'0';
					$starttime = date('h:i:s a', strtotime($day_service->start_time));
					$endtime = date('h:i:s a', strtotime($day_service->end_time));
                } else {
                    $pick_zone = "";
                    $starttime = "";
                    $endtime = "";
                }

                if($day_service)
                {
                    $service_status=$day_service->service_status;
                }

                $payment_type = 'Daily Paying';
                if ($booking->payment_type == 'D') 
                {
                    $payment_type = 'Daily Paying';
                } 
                else if ($booking->payment_type == 'W') 
                {
                    $payment_type = 'Weekly Paying';
                } else if ($booking->payment_type == 'M') 
                {
                    $payment_type = 'Monthly Paying';
                }

                $maid_attandence = $this->crew_appapi_lat_model->get_maid_attandence_by_date($booking->maid_id, $service_date);
                if (isset($maid_attandence->attandence_id)) 
                {
                    $m_attandence = $maid_attandence->attandence_status;
                } 
                else 
                {
                    $m_attandence = "0";
                }


                $check_buk_exist = $this->crew_appapi_lat_model->get_booking_exist($booking->booking_id,$service_date);
                if(!empty($check_buk_exist))
                {
                    $mop = $check_buk_exist->mop;
                    $ref = $check_buk_exist->just_mop_ref;
                } else {
                    $mop = $booking->payment_mode;
                    $ref = "";
                }
                
                if($mop == "Credit Card")
                {
                    $totamount = 'CC';
                } else {
                    $totamount = $booking->total_amount;
                }
				
				if($booking->pay_by != "")
				{
					$mop = $booking->pay_by;
				} else {
					$mop = $booking->payment_mode;
				}

				$extraserviceary = array();
				if($booking->interior_window_clean == 1)
				{
					array_push($extraserviceary,'Window Cleaning');
				}
				if($booking->fridge_cleaning == 1)
				{
					array_push($extraserviceary,'Fridge Cleaning');
				}
				if($booking->ironing_services == 1)
				{
					array_push($extraserviceary,'Ironing');
				}
				if($booking->oven_cleaning == 1)
				{
					array_push($extraserviceary,'Oven Cleaning');
				}
				
				if(!empty($extraserviceary))
				{
					$ext_services = implode(', ', $extraserviceary);
				} else {
					$ext_services = '';
				}

                $schedule[$i]['id']                = $booking->booking_id;
                $schedule[$i]['schedule_date']     = date('d-m-Y',strtotime($service_date));
                $schedule[$i]['schedule_timeing']['timeing']     = date('H:i', strtotime($booking->time_from));
                $refid=$booking->reference_id;$no_of_maids=1;
                if(strlen($refid)>0)
                {
                    $no_of_maids=1;
                }

                $no_of_maids=count(array_keys($bookings_time_from, $booking->time_from));
                
                $schedule[$i]['schedule_timeing']['No.of.maids'] = $no_of_maids;



                $schedule[$i]['Schedule']['booking_id']         = $booking->booking_id;
                $schedule[$i]['Schedule']['istransfer']         = $this->crew_appapi_lat_model->istransferred($booking->booking_id);
                $schedule[$i]['Schedule']['booking_type']       = $booking->booking_type;
                $schedule[$i]['Schedule']['vaccum_cleaner']     = $booking->vaccum_cleaning!=NULL?1:0;
                $schedule[$i]['Schedule']['service_status']     = (int)$service_status; 
                $schedule[$i]['Schedule']['customer_id']        = $booking->customer_id;
                $schedule[$i]['Schedule']['customer_code']      = $this->config->item('customer_code_prepend') . $booking->customer_id;
                $schedule[$i]['Schedule']['customer_name']      = htmlspecialchars($booking->customer_name, ENT_QUOTES, 'UTF-8');
                $schedule[$i]['Schedule']['customer_address']   = $a_address;
                $schedule[$i]['Schedule']['customer_longitude'] = $booking->longitude;
                $schedule[$i]['Schedule']['customer_latitude']  = $booking->latitude;
                $schedule[$i]['Schedule']['customer_mobile']    = $booking->customer_source;
                $schedule[$i]['Schedule']['customer_type']      = $payment_type;
                $schedule[$i]['Schedule']['shift_start']        = date('H:i', strtotime($booking->time_from));
                $schedule[$i]['Schedule']['shift_end']          = date('H:i', strtotime($booking->time_to));
                $schedule[$i]['Schedule']['key_status']         = $booking->key_given == 'Y' ? "1" : "0";
                $schedule[$i]['Schedule']['area']               = $booking->area_name;
                $schedule[$i]['Schedule']['zone']               = $booking->zone_name;
                $schedule[$i]['Schedule']['booking_note']       = htmlspecialchars($booking->booking_note,ENT_QUOTES,'UTF-8');
                $schedule[$i]['Schedule']['service_fee']        = $totamount;
                $schedule[$i]['Schedule']['new_ref']            = $ref;
                $schedule[$i]['Schedule']['payMode']                = $mop;
                $schedule[$i]['Schedule']['mop']                = $this->mode_of_paymet($booking->payment_method);
                if ($booking->cleaning_material == "Y") {
                    $schedule[$i]['Schedule']['cleaning_material'] = "yes";
					$schedule[$i]['Schedule']['tools']                 = 'Normal Cleaning Set';
                } else {
                    $schedule[$i]['Schedule']['cleaning_material'] = "no";
					$schedule[$i]['Schedule']['tools']                 = '';
                }
				if($booking->customer_notes != "")
				{
					$schedule[$i]['Schedule']['customer_notes']           = $booking->customer_notes;
				} else {
					$schedule[$i]['Schedule']['customer_notes']           = "";
				}
                $schedule[$i]['Schedule']['servicetype']           = $booking->service_type_name;
                $schedule[$i]['Schedule']['extra_service']         = $ext_services;
                $schedule[$i]['Schedule']['outstanding_balance']   = (string) round($booking->balance,2);
					// $schedule[$i]['Schedule']['payment_status']        = (int)$payment_status;
					$schedule[$i]['Schedule']['payment_status']        = (int)$booking->payment_status;
					$schedule[$i]['Schedule']['paid_amount']           = (string)  round($paid_amount,2);
					$schedule[$i]['Schedule']['balance']               = (string)($paid_amount > $booking->total_amount)?'0':round(($booking->total_amount - $paid_amount),2);
					$schedule[$i]['Schedule']['total']                 = (string) round($booking->total_amount,2);
                $schedule[$i]['Schedule']['starttime']                 = $starttime;
                $schedule[$i]['Schedule']['endtime']                 = $endtime;


                //$schedule[$i]['Schedule']['related_bookings']   = array();
                $schedule[$i]['maid_details']   = array();
				$schedule[$i]['maid_details'][0]['booking_id']        = $booking->booking_id;
                $schedule[$i]['maid_details'][0]['service_fee']       = $totamount;
                $schedule[$i]['maid_details'][0]['total']             = (string) round($booking->total_amount,2);
                $schedule[$i]['maid_details'][0]['maid_id']           = $booking->maid_id;
                $schedule[$i]['maid_details'][0]['customer_id']       = $booking->customer_id;
                $schedule[$i]['maid_details'][0]['maid_name']         = $booking->maid_name;
                $schedule[$i]['maid_details'][0]['maid_country']      = $booking->maid_nationality;
                $schedule[$i]['maid_details'][0]['maid_attandence']   = $m_attandence;

                $maid_image           =  (strlen($booking->maid_photo_file)>0)?$booking->maid_photo_file:'';
                if(strlen($maid_image)>0)
                {
                    
                    if(file_exists(FCPATH.'maidimg/'.$maid_image))
                    {
                        $maid_image = base_url().'maidimg/'.$maid_image;    
                    }
                    else
                    {
                        $maid_image = '';
                    }
                }

                $schedule[$i]['maid_details'][0]['maid_image']   = $maid_image;


                if(strlen($booking->reference_id)>0)
                { 
                    $related_bookings=$this->crew_appapi_lat_model->get_related_bookings($refid,$booking->booking_id,$service_date);
                    if($related_bookings)
                    {
                        $j=0;
                        foreach ($related_bookings as $rel_buks) 
						{
                            $schedule[$i]['maid_details'][$j+1]['maid_id']           = $rel_buks->maid_id;
                            $schedule[$i]['maid_details'][$j+1]['customer_id']       = $rel_buks->customer_id;
                            $schedule[$i]['maid_details'][$j+1]['maid_name']         = $rel_buks->maid_name;
                            $schedule[$i]['maid_details'][$j+1]['maid_country']      = $rel_buks->maid_nationality;
                            $maid_image           =  (strlen($rel_buks->maid_photo_file)>0)?$rel_buks->maid_photo_file:'';
                            if(strlen($maid_image)>0)
                            {
                                
                                if(file_exists(FCPATH.'maidimg/'.$maid_image))
                                {
                                    $maid_image = base_url().'maidimg/'.$maid_image;    
                                }
                                else
                                {
                                    $maid_image = '';
                                }
                            }

                            $schedule[$i]['maid_details'][$j+1]['maid_image']   = $maid_image;

                            $maid_attandence = $this->crew_appapi_lat_model->get_maid_attandence_by_date($rel_buks->maid_id, $service_date);
                            if (isset($maid_attandence->attandence_id)) 
                            {
                                $m_attandence = $maid_attandence->attandence_status;
                            } 
                            else 
                            {
                                $m_attandence = "0";
                            }

                            $schedule[$i]['maid_details'][$j+1]['maid_attandence']   = $m_attandence;

                                

                            $j++;
                        }
                    }
                }                   


                
                $i++;
            }
        }
		
		//pradeesh changes 15-09-21
        if(!$schedule){
        $schedule[0]['id']='';
        $schedule[0]['schedule_date']=date('d-m-Y',strtotime($service_date));
        $schedule[0]['schedule_timeing']=(object) [
            "timeing"=> "",
            "No.of.maids"=> 0
        ];
        $schedule[0]['Schedule']=(object) [
            "booking_id"=> "",
            "service_status"=> 0,
            "istransfer"=> 0,
            "vaccum_cleaner" => 0,
            "booking_type" => "",
            "customer_id"=> "",
            "customer_code"=> "",
            "customer_name"=> "",
            "customer_address"=> "",
            "customer_longitude"=> "",
            "customer_latitude"=> "",
            "customer_mobile"=> "",
            "customer_type"=> "",
            "shift_start"=> "",
            "shift_end"=> "",
            "key_status"=> "",
            "area"=> "",
            "zone"=> "",
            "booking_note"=> "",
            "service_fee"=> "",
            "new_ref"=> "",
            "payMode"=> "",
            "mop"=> "",
            "cleaning_material"=> "",
            "tools"=> "",
            "customer_notes"=> "",
            "servicetype"=> "e",
            "extra_service"=> "",
            "outstanding_balance"=> "",
            "payment_status"=> 0,
            "paid_amount"=> 0.0,
            "balance"=> 00,
            "total"=> "",
            "starttime"=> "",
            "endtime"=> ""
        ];
        $schedule[0]['maid_details']=[
        ];
        }
        //pradeesh changes 15-09-21

        return $schedule;
    }
	
	
    function mode_of_paymet($a){
        $b="";
        if($a == 0) $b= "Cash";
        elseif($a == 1) $b= "Card";
        elseif($a == 2) $b= "Cheque";
        elseif($a == 3) $b= "Online";
        return $b;
    }
    public function getschedule_new($tabid,$service_date)
	{
        if (strlen($tabid) > 0 && $service_date)
		{
            $tablet = $this->crew_appapi_lat_model->get_tablet_by_tabletid($tabid);
            if (isset($tablet->tablet_id) && $tablet->tablet_status == 1)
			{
                $schedule  = $this->get_driver_schedules($service_date,$tablet);
                $schedule1 = $this->get_driver_schedules(date('Y-m-d', strtotime($service_date. ' + 1 day')),$tablet);
				$schedule2 = $this->get_driver_schedules(date('Y-m-d', strtotime($service_date. ' + 2 days')),$tablet);
				
				echo json_encode(array('status' => 'success','message' => 'Retrieved driver schedule successfully!', 'schedule' => $this->driver_maid_merge(array_merge($schedule,$schedule1,$schedule2))));
                exit();
            } else if (isset($tablet->tablet_id) && $tablet->tablet_status == 0) {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '103';
                $response['message'] = 'Inactive tablet';

                echo json_encode($response);
                exit();
            } else {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '102';
                $response['message'] = 'Invalid IMEI';

                echo json_encode($response);
                exit();
            }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
	
	// merge code 
    public function driver_maid_merge($arr){
        // return $arr;
        $ar=[]; $a=[];
        foreach($arr as $k => $v){
            if(!is_object($v['Schedule']))
            {
            $aa=[ 'index' => $k, 'c_id' => $v['Schedule']['customer_id'], 'id' => $v['id'], 'date' => $v['schedule_date'], 'from' => $v['Schedule']['shift_start'], 'to' => $v['Schedule']['shift_end']];
            $b=$this->in_array_repeat($aa,$a);
            if(!$b){ $a[]=$aa; $stop=0;}
            else{
                $arr[$b['index']]['maid_details'][]=$v['maid_details'][0];
                $arr[$b['index']]['Schedule']['service_fee']=floatval($arr[$b['index']]['Schedule']['service_fee'])+floatval($v['Schedule']['service_fee']);
                $arr[$b['index']]['Schedule']['total']=floatval($arr[$b['index']]['Schedule']['total'])+floatval($v['Schedule']['total']);
                $arr[$b['index']]['Schedule']['paid_amount']=floatval($arr[$b['index']]['Schedule']['paid_amount'])+floatval($v['Schedule']['paid_amount']);
                $arr[$b['index']]['Schedule']['balance']=floatval($arr[$b['index']]['Schedule']['balance'])+floatval($v['Schedule']['balance']);
                $arr[$b['index']]['Schedule']['service_fee']=(string) $arr[$b['index']]['Schedule']['service_fee'];
                $arr[$b['index']]['Schedule']['total'] = (string) $arr[$b['index']]['Schedule']['total'];
                $arr[$b['index']]['Schedule']['paid_amount'] = (string) $arr[$b['index']]['Schedule']['paid_amount'];
                $arr[$b['index']]['Schedule']['balance'] = (string) $arr[$b['index']]['Schedule']['balance'];
                if($v['Schedule']['service_status']==0)
                $stop=1;
                if($stop)
                $arr[$b['index']]['Schedule']['service_status'] = 0;
                unset($arr[$k]);
            }
         }
        }
        foreach($arr as $d){ $ar[]=$d; }
        return $ar;
    }
    function in_array_repeat($aa,$a){
        foreach($a as $k=>$v){ if($v['c_id']==$aa['c_id'] && $v['from']==$aa['from'] && $v['to']==$aa['to'] && $v['date']==$aa['date']) return $v; }
        return false;
    }
// merge code
	
	public function get_driver_schedules($service_date,$tablet) 
    {
        $bookings = $this->crew_appapi_lat_model->get_schedule_by_date_for_tab($service_date);

        //$booking_transfers = $this->crew_appapi_lat_model->get_booking_transfers_by_date($service_date);
        $booking_transfers = $this->crew_appapi_lat_model->get_booking_transfers_by_date_new($service_date);
        $transferred_booking_zones = array();
        foreach ($booking_transfers as $b_transfer) 
        {
            $transferred_booking_zones[$b_transfer->booking_id] = $b_transfer->transfering_to_tablet;
        }
                
        $schedule = array();
        $i = 0;
        $service_status =0;$booking_refs=array();
        $bookingsarray = (array) $bookings;

        $bookings_time_from=array();
        foreach ($bookings as $booking) {
			if($booking->tabletid > 0)
			{
				if (($booking->tabletid == $tablet->tablet_id && !isset($transferred_booking_zones[$booking->booking_id])) || (isset($transferred_booking_zones[$booking->booking_id]) && $transferred_booking_zones[$booking->booking_id] == $tablet->tablet_id)){
				//if (($booking->zone_id == $tablet->zone_id && !isset($transferred_booking_zones[$booking->booking_id])) || (isset($transferred_booking_zones[$booking->booking_id]) && $transferred_booking_zones[$booking->booking_id] == $tablet->zone_id)) {
						$bookings_time_from[]=$booking->time_from;
				}
			} else {
				if (($booking->zone_id == $tablet->zone_id && !isset($transferred_booking_zones[$booking->booking_id])) || (isset($transferred_booking_zones[$booking->booking_id]) && $transferred_booking_zones[$booking->booking_id] == $tablet->zone_id)) {
						$bookings_time_from[]=$booking->time_from;
				}
			}
        }

        foreach ($bookings as $booking) {
			if($booking->tabletid > 0)
			{
				if (($booking->tabletid == $tablet->tablet_id && !isset($transferred_booking_zones[$booking->booking_id])) || (isset($transferred_booking_zones[$booking->booking_id]) && $transferred_booking_zones[$booking->booking_id] == $tablet->tablet_id))
				{
					$payment_type = 'Daily Paying';
					if ($booking->payment_type == 'D') 
					{
						$payment_type = 'Daily Paying';
					} 
					else if ($booking->payment_type == 'W') 
					{
						$payment_type = 'Weekly Paying';
					} else if ($booking->payment_type == 'M') 
					{
						$payment_type = 'Monthly Paying';
					}

					$maid_attandence = $this->crew_appapi_lat_model->get_maid_attandence_by_date($booking->maid_id, $service_date);
					if (isset($maid_attandence->attandence_id) && ($tablet->tablet_id == $maid_attandence->tablet_id)) 
					{
						$m_attandence = $maid_attandence->attandence_status;
					} 
					else 
					{
						$m_attandence = "0";
					}

					$refid=$booking->reference_id;$no_of_maids=1;

					if (in_array($refid, $booking_refs)) // to eliminate duplicate refid bookings
					{
						continue;
					}

					$payment_status=0;$paid_amount=0;
					$day_service = $this->crew_appapi_lat_model->get_day_service_by_booking_id($service_date, $booking->booking_id);

					if (isset($day_service->day_service_id)) 
					{
						$service_status = $day_service->service_status;
						$totalamount    = $day_service->total_fee;
						$payment_status = $day_service->payment_status;
						$paid_amount    = $day_service->paid_amount;
						$paid_amount    = is_numeric($paid_amount)?$paid_amount:'0';
						$starttime = date('h:i:s a', strtotime($day_service->start_time));
						$endtime = date('h:i:s a', strtotime($day_service->end_time));
						//$discount = $booking->discount;
						//$totamount = $totalamount - $discount;
						//removed showing from booking total only this line $totamount = $totalamount;
						if($booking->payment_mode == "Credit Card")
						{
							$totamount = 'CC';
						} 
						else 
						{
							$totamount = $booking->total_amount;
						}
					} 
					else 
					{

						$normal_hours = 0;
						$extra_hours = 0;
						$weekend_hours = 0;

						$normal_from = strtotime('06:00:00');
						$normal_to = strtotime('21:00:00');

						$shift_from = strtotime($booking->time_from . ':00');
						$shift_to = strtotime($booking->time_to . ':00');

						
						if (date('w') != 5) 
						{
							if ($shift_from < $normal_from) 
							{
								if ($shift_to <= $normal_from) 
								{
									$extra_hours = ($shift_to - $shift_from) / 3600;
								}

								if ($shift_to > $normal_from && $shift_to <= $normal_to) 
								{
									$extra_hours = ($normal_from - $shift_from) / 3600;
									$normal_hours = ($shift_to - $normal_from) / 3600;
								}

								if ($shift_to > $normal_to) 
								{
									$extra_hours = ($normal_from - $shift_from) / 3600;
									$extra_hours += ($shift_to - $normal_to) / 3600;
									$normal_hours = ($normal_to - $normal_from) / 3600;
								}
							}

							if ($shift_from >= $normal_from && $shift_from < $normal_to) 
							{
								if ($shift_to <= $normal_to) 
								{
									$normal_hours = ($shift_to - $shift_from) / 3600;
								}

								if ($shift_to > $normal_to) 
								{
									$normal_hours = ($normal_to - $shift_from) / 3600;
									$extra_hours = ($shift_to - $normal_to) / 3600;
								}
							}

							if ($shift_from > $normal_to) 
							{
								$extra_hours = ($shift_to - $shift_from) / 3600;
							}
						} 
						else 
						{
							$weekend_hours = ($shift_to - $shift_from) / 3600;
						}

						$service_description = array();

						$service_description['normal'] = new stdClass();
						$service_description['normal']->hours = $normal_hours;
						$service_description['normal']->fees = $normal_hours * $booking->price_hourly;

						$service_description['extra'] = new stdClass();
						$service_description['extra']->hours = $extra_hours;
						$service_description['extra']->fees = $extra_hours * $booking->price_extra;

						$service_description['weekend'] = new stdClass();
						$service_description['weekend']->hours = $weekend_hours;
						$service_description['weekend']->fees = $weekend_hours * $booking->price_weekend;

						
						$location_charge = 0;

						$customer_address = $this->crew_appapi_lat_model->get_customer_address_by_id($booking->customer_address_id);
						$cus_detail = $this->crew_appapi_lat_model->get_customer_by_id($booking->customer_id);
						if ($customer_address) 
						{
							$area = $this->crew_appapi_lat_model->get_area_by_id($customer_address->area_id);
							if ($area) 
							{
								$location_charge = $area->area_charge;
							}
						}

						$service_status = 0;
						$totalamount = 0;
						$discount = 0;
						
						if($booking->payment_mode == "Credit Card")
						{
							$totamount = 'CC';
						} 
						else 
						{
							$totamount = $booking->total_amount;
						}
						$starttime = "";
						$endtime = "";
					}
					
					if($booking->customer_address == "")
					{
						$a_address = $booking->building.', '.$booking->unit_no.''.$booking->street;
					} 
					else 
					{
						if($booking->building != "")
						{
							$t_address = "Apt No: ".$booking->building.", ".$booking->customer_address;
						} else {
							$t_address = $booking->customer_address;
						}
						$a_address = htmlspecialchars($t_address, ENT_QUOTES, 'UTF-8');
					}
					
					$check_buk_exist = $this->crew_appapi_lat_model->get_booking_exist($booking->booking_id,$service_date);
					if(!empty($check_buk_exist))
					{
						$mop = $check_buk_exist->mop;
						$ref = $check_buk_exist->just_mop_ref;
					} else {
						$mop = $booking->payment_mode;
						$ref = "";
					}
					
					if($mop == "Credit Card")
					{
						$totamount = 'CC';
					} else {
						$totamount = $booking->total_amount;
					}
					
					if($booking->pay_by != "")
					{
						$mop = $booking->pay_by;
					} else {
						$mop = $booking->payment_mode;
					}
					
					if($booking->driver != "")
					{
						$driver = " (".$booking->driver.")";
					} else {
						$driver = "";
					}

					$extraserviceary = array();
					if($booking->interior_window_clean == 1)
					{
						array_push($extraserviceary,'Window Cleaning');
					}
					if($booking->fridge_cleaning == 1)
					{
						array_push($extraserviceary,'Fridge Cleaning');
					}
					if($booking->ironing_services == 1)
					{
						array_push($extraserviceary,'Ironing');
					}
					if($booking->oven_cleaning == 1)
					{
						array_push($extraserviceary,'Oven Cleaning');
					}
					
					if(!empty($extraserviceary))
					{
						$ext_services = implode(', ', $extraserviceary);
					} else {
						$ext_services = '';
					}


					$schedule[$i]['id']                = $booking->booking_id;
					$schedule[$i]['schedule_date']     = date('d-m-Y',strtotime($service_date));
					//$schedule[$i]['schedule_date']='14-06-2021';
					$schedule[$i]['schedule_timeing']['timeing']     = date('H:i', strtotime($booking->time_from));
					

					if(strlen($refid)>0)
					{
						$booking_refs[]=$refid;
						
						//$no_of_maids=$this->driverappapimodel->get_booking_count_by_refid($refid);
					}

					$no_of_maids=count(array_keys($bookings_time_from, $booking->time_from));
					

					$schedule[$i]['schedule_timeing']['No.of.maids'] = $no_of_maids;



					$schedule[$i]['Schedule']['booking_id']         = $booking->booking_id;
					$schedule[$i]['Schedule']['istransfer']         = $this->crew_appapi_lat_model->istransferred($booking->booking_id);
                    $schedule[$i]['Schedule']['vaccum_cleaner']     = $booking->vaccum_cleaning!=NULL?1:0;
                    $schedule[$i]['Schedule']['booking_type']       = $booking->booking_type;
					$schedule[$i]['Schedule']['service_status']     = (int)$service_status; 
					$schedule[$i]['Schedule']['customer_id']        = $booking->customer_id;
					$schedule[$i]['Schedule']['customer_code']      = $this->config->item('customer_code_prepend') . $booking->customer_id;
					$schedule[$i]['Schedule']['customer_name']      = htmlspecialchars($booking->customer_name, ENT_QUOTES, 'UTF-8');
					$schedule[$i]['Schedule']['customer_address']   = $a_address;
					$schedule[$i]['Schedule']['customer_longitude'] = $booking->longitude;
					$schedule[$i]['Schedule']['customer_latitude']  = $booking->latitude;
					$schedule[$i]['Schedule']['customer_mobile']    = $booking->customer_source;
					$schedule[$i]['Schedule']['customer_type']      = $payment_type;
					$schedule[$i]['Schedule']['shift_start']        = date('H:i', strtotime($booking->time_from));
					$schedule[$i]['Schedule']['shift_end']          = date('H:i', strtotime($booking->time_to));
					$schedule[$i]['Schedule']['key_status']         = $booking->key_given == 'Y' ? "1" : "0";
					$schedule[$i]['Schedule']['area']               = $booking->area_name;
					$schedule[$i]['Schedule']['zone']               = $booking->zone_name;
					$schedule[$i]['Schedule']['booking_note']       = htmlspecialchars($booking->booking_note,ENT_QUOTES,'UTF-8');
					$schedule[$i]['Schedule']['service_fee']        = $totamount;
					$schedule[$i]['Schedule']['new_ref']            = $ref;
					$schedule[$i]['Schedule']['payMode']                = $mop;
                    $schedule[$i]['Schedule']['mop']                = $this->mode_of_paymet($booking->payment_method);
					if ($booking->cleaning_material == "Y") {
						$schedule[$i]['Schedule']['cleaning_material'] = "yes";
						$schedule[$i]['Schedule']['tools']                 = 'Normal Cleaning Set';
					} else {
						$schedule[$i]['Schedule']['cleaning_material'] = "no";
						$schedule[$i]['Schedule']['tools']                 = '';
					}
					if($booking->customer_notes != "")
					{
						$schedule[$i]['Schedule']['customer_notes']           = $booking->customer_notes;
					} else {
						$schedule[$i]['Schedule']['customer_notes']           = "";
					}
					$schedule[$i]['Schedule']['servicetype']           = $booking->service_type_name;
					$schedule[$i]['Schedule']['extra_service']         = $ext_services;
					$schedule[$i]['Schedule']['outstanding_balance']   = (string) round($booking->balance,2);
					// $schedule[$i]['Schedule']['payment_status']        = (int)$payment_status;
					$schedule[$i]['Schedule']['payment_status']        = (int)$booking->payment_status;
					$schedule[$i]['Schedule']['paid_amount']           = (string)  round($paid_amount,2);
					$schedule[$i]['Schedule']['balance']               = (string)($paid_amount > $booking->total_amount)?'0':round(($booking->total_amount - $paid_amount),2);
					$schedule[$i]['Schedule']['total']                 = (string) round($booking->total_amount,2);
					$schedule[$i]['Schedule']['starttime']                 = $starttime;
					$schedule[$i]['Schedule']['endtime']                 = $endtime;

					$schedule[$i]['maid_details']   = array();
					$schedule[$i]['maid_details'][0]['booking_id']        = $booking->booking_id;
                    $schedule[$i]['maid_details'][0]['service_fee']       = $totamount;
                    $schedule[$i]['maid_details'][0]['total']             = (string) round($booking->total_amount,2);
					$schedule[$i]['maid_details'][0]['maid_id']           = $booking->maid_id;
					$schedule[$i]['maid_details'][0]['customer_id']       = $booking->customer_id;
					$schedule[$i]['maid_details'][0]['maid_name']         = $booking->maid_name;
					$schedule[$i]['maid_details'][0]['maid_country']      = $booking->maid_nationality;
					$schedule[$i]['maid_details'][0]['maid_attandence']   = $m_attandence;
					$maid_image           =  (strlen($booking->maid_photo_file)>0)?$booking->maid_photo_file:'';
					if(strlen($maid_image)>0)
					{
						
						if(file_exists(FCPATH.'maidimg/'.$maid_image))
						{
							$maid_image = base_url().'maidimg/'.$maid_image;    
						}
						else
						{
							$maid_image = '';
						}
					}

					$schedule[$i]['maid_details'][0]['maid_image']   = $maid_image;

					if(strlen($booking->reference_id)>0)
					{
						$related_bookings=$this->crew_appapi_lat_model->get_related_bookings($booking->reference_id,$booking->booking_id,$service_date);
						if($related_bookings)
						{
							$j=0;
							foreach ($related_bookings as $rel_buks) {
								// $schedule[$i]['Schedule']['related_bookings'][$j]['maid_id']=$rel_buks->maid_id;
								// $schedule[$i]['Schedule']['related_bookings'][$j]['booking_id']=$rel_buks->booking_id;
								// $schedule[$i]['Schedule']['related_bookings'][$j]['customer_id']=$rel_buks->customer_id;
								// $schedule[$i]['Schedule']['related_bookings'][$j]['reference_id']=$rel_buks->reference_id;
								// $schedule[$i]['Schedule']['related_bookings'][$j]['maid_attendance']=(is_numeric($rel_buks->attandence_status))?$rel_buks->attandence_status:'0';


								$schedule[$i]['maid_details'][$j+1]['maid_id']           = $rel_buks->maid_id;
								$schedule[$i]['maid_details'][$j+1]['customer_id']       = $rel_buks->customer_id;
								$schedule[$i]['maid_details'][$j+1]['maid_name']         = $rel_buks->maid_name;
								$schedule[$i]['maid_details'][$j+1]['maid_country']      = $rel_buks->maid_nationality;

								$maid_image           =  (strlen($rel_buks->maid_photo_file)>0)?$rel_buks->maid_photo_file:'';
								if(strlen($maid_image)>0)
								{
									
									if(file_exists(FCPATH.'maidimg/'.$maid_image))
									{
										$maid_image = base_url().'maidimg/'.$maid_image;    
									}
									else
									{
										$maid_image = '';
									}
								}
								
								$schedule[$i]['maid_details'][$j+1]['maid_image']   = $maid_image;

								$maid_attandence = $this->crew_appapi_lat_model->get_maid_attandence_by_date($rel_buks->maid_id, $service_date);
								if (isset($maid_attandence->attandence_id) && ($tablet->tablet_id == $maid_attandence->tablet_id)) 
								{
									$m_attandence = $maid_attandence->attandence_status;
								} 
								else 
								{
									$m_attandence = "0";
								}

								$schedule[$i]['maid_details'][$j+1]['maid_attandence']   = $m_attandence;

								$j++;
							}
						}
					}

					// $schedule[$i]['maid_details']['maid_id']           = $booking->maid_id;
					// $schedule[$i]['maid_details']['customer_id']       = $booking->customer_id;
					// $schedule[$i]['maid_details']['maid_name']         = $booking->maid_name;
					// $schedule[$i]['maid_details']['maid_country']      = $booking->maid_nationality;
					// $schedule[$i]['maid_details']['maid_attandence']   = $m_attandence;



					$i++;
				}
			} else
			{
				if (($booking->zone_id == $tablet->zone_id && !isset($transferred_booking_zones[$booking->booking_id])) || (isset($transferred_booking_zones[$booking->booking_id]) && $transferred_booking_zones[$booking->booking_id] == $tablet->zone_id))
				{
					$payment_type = 'Daily Paying';
					if ($booking->payment_type == 'D') 
					{
						$payment_type = 'Daily Paying';
					} 
					else if ($booking->payment_type == 'W') 
					{
						$payment_type = 'Weekly Paying';
					} else if ($booking->payment_type == 'M') 
					{
						$payment_type = 'Monthly Paying';
					}

					$maid_attandence = $this->crew_appapi_lat_model->get_maid_attandence_by_date($booking->maid_id, $service_date);
					if (isset($maid_attandence->attandence_id) && ($tablet->tablet_id == $maid_attandence->tablet_id)) 
					{
						$m_attandence = $maid_attandence->attandence_status;
					} 
					else 
					{
						$m_attandence = "0";
					}

					$refid=$booking->reference_id;$no_of_maids=1;

					if (in_array($refid, $booking_refs)) // to eliminate duplicate refid bookings
					{
						continue;
					}

					$payment_status=0;$paid_amount=0;
					$day_service = $this->crew_appapi_lat_model->get_day_service_by_booking_id($service_date, $booking->booking_id);

					if (isset($day_service->day_service_id)) 
					{
						$service_status = $day_service->service_status;
						$totalamount    = $day_service->total_fee;
						$payment_status = $day_service->payment_status;
						$paid_amount    = $day_service->paid_amount;
						$paid_amount    = is_numeric($paid_amount)?$paid_amount:'0';
						$starttime = date('h:i:s a', strtotime($day_service->start_time));
						$endtime = date('h:i:s a', strtotime($day_service->end_time));
						//$discount = $booking->discount;
						//$totamount = $totalamount - $discount;
						//removed showing from booking total only this line $totamount = $totalamount;
						if($booking->payment_mode == "Credit Card")
						{
							$totamount = 'CC';
						} 
						else 
						{
							$totamount = $booking->total_amount;
						}
					} 
					else 
					{

						$normal_hours = 0;
						$extra_hours = 0;
						$weekend_hours = 0;

						$normal_from = strtotime('06:00:00');
						$normal_to = strtotime('21:00:00');

						$shift_from = strtotime($booking->time_from . ':00');
						$shift_to = strtotime($booking->time_to . ':00');

						
						if (date('w') != 5) 
						{
							if ($shift_from < $normal_from) 
							{
								if ($shift_to <= $normal_from) 
								{
									$extra_hours = ($shift_to - $shift_from) / 3600;
								}

								if ($shift_to > $normal_from && $shift_to <= $normal_to) 
								{
									$extra_hours = ($normal_from - $shift_from) / 3600;
									$normal_hours = ($shift_to - $normal_from) / 3600;
								}

								if ($shift_to > $normal_to) 
								{
									$extra_hours = ($normal_from - $shift_from) / 3600;
									$extra_hours += ($shift_to - $normal_to) / 3600;
									$normal_hours = ($normal_to - $normal_from) / 3600;
								}
							}

							if ($shift_from >= $normal_from && $shift_from < $normal_to) 
							{
								if ($shift_to <= $normal_to) 
								{
									$normal_hours = ($shift_to - $shift_from) / 3600;
								}

								if ($shift_to > $normal_to) 
								{
									$normal_hours = ($normal_to - $shift_from) / 3600;
									$extra_hours = ($shift_to - $normal_to) / 3600;
								}
							}

							if ($shift_from > $normal_to) 
							{
								$extra_hours = ($shift_to - $shift_from) / 3600;
							}
						} 
						else 
						{
							$weekend_hours = ($shift_to - $shift_from) / 3600;
						}

						$service_description = array();

						$service_description['normal'] = new stdClass();
						$service_description['normal']->hours = $normal_hours;
						$service_description['normal']->fees = $normal_hours * $booking->price_hourly;

						$service_description['extra'] = new stdClass();
						$service_description['extra']->hours = $extra_hours;
						$service_description['extra']->fees = $extra_hours * $booking->price_extra;

						$service_description['weekend'] = new stdClass();
						$service_description['weekend']->hours = $weekend_hours;
						$service_description['weekend']->fees = $weekend_hours * $booking->price_weekend;

						
						$location_charge = 0;

						$customer_address = $this->crew_appapi_lat_model->get_customer_address_by_id($booking->customer_address_id);
						$cus_detail = $this->crew_appapi_lat_model->get_customer_by_id($booking->customer_id);
						if ($customer_address) 
						{
							$area = $this->crew_appapi_lat_model->get_area_by_id($customer_address->area_id);
							if ($area) 
							{
								$location_charge = $area->area_charge;
							}
						}

						$service_status = 0;
						$totalamount = 0;
						$discount = 0;
						
						if($booking->payment_mode == "Credit Card")
						{
							$totamount = 'CC';
						} 
						else 
						{
							$totamount = $booking->total_amount;
						}
						$starttime = "";
						$endtime = "";
					}
					
					if($booking->customer_address == "")
					{
						$a_address = $booking->building.', '.$booking->unit_no.''.$booking->street;
					} 
					else 
					{
						if($booking->building != "")
						{
							$t_address = "Apt No: ".$booking->building.", ".$booking->customer_address;
						} else {
							$t_address = $booking->customer_address;
						}
						$a_address = htmlspecialchars($t_address, ENT_QUOTES, 'UTF-8');
					}
					
					$check_buk_exist = $this->crew_appapi_lat_model->get_booking_exist($booking->booking_id,$service_date);
					if(!empty($check_buk_exist))
					{
						$mop = $check_buk_exist->mop;
						$ref = $check_buk_exist->just_mop_ref;
					} else {
						$mop = $booking->payment_mode;
						$ref = "";
					}
					
					if($mop == "Credit Card")
					{
						$totamount = 'CC';
					} else {
						$totamount = $booking->total_amount;
					}
					
					if($booking->pay_by != "")
					{
						$mop = $booking->pay_by;
					} else {
						$mop = $booking->payment_mode;
					}
					
					if($booking->driver != "")
					{
						$driver = " (".$booking->driver.")";
					} else {
						$driver = "";
					}

					$extraserviceary = array();
					if($booking->interior_window_clean == 1)
					{
						array_push($extraserviceary,'Window Cleaning');
					}
					if($booking->fridge_cleaning == 1)
					{
						array_push($extraserviceary,'Fridge Cleaning');
					}
					if($booking->ironing_services == 1)
					{
						array_push($extraserviceary,'Ironing');
					}
					if($booking->oven_cleaning == 1)
					{
						array_push($extraserviceary,'Oven Cleaning');
					}
					
					if(!empty($extraserviceary))
					{
						$ext_services = implode(', ', $extraserviceary);
					} else {
						$ext_services = '';
					}


					$schedule[$i]['id']                = $booking->booking_id;
					$schedule[$i]['schedule_date']     = date('d-m-Y',strtotime($service_date));
					//$schedule[$i]['schedule_date']='14-06-2021';
					$schedule[$i]['schedule_timeing']['timeing']     = date('H:i', strtotime($booking->time_from));
					

					if(strlen($refid)>0)
					{
						$booking_refs[]=$refid;
						
						//$no_of_maids=$this->driverappapimodel->get_booking_count_by_refid($refid);
					}

					$no_of_maids=count(array_keys($bookings_time_from, $booking->time_from));
					

					$schedule[$i]['schedule_timeing']['No.of.maids'] = $no_of_maids;



					$schedule[$i]['Schedule']['booking_id']         = $booking->booking_id;
					$schedule[$i]['Schedule']['istransfer']         = $this->crew_appapi_lat_model->istransferred($booking->booking_id);
                    $schedule[$i]['Schedule']['vaccum_cleaner']     = $booking->vaccum_cleaning!=NULL?1:0;
                    $schedule[$i]['Schedule']['booking_type']       = $booking->booking_type;
					$schedule[$i]['Schedule']['service_status']     = (int)$service_status; 
					$schedule[$i]['Schedule']['customer_id']        = $booking->customer_id;
					$schedule[$i]['Schedule']['customer_code']      = $this->config->item('customer_code_prepend') . $booking->customer_id;
					$schedule[$i]['Schedule']['customer_name']      = htmlspecialchars($booking->customer_name, ENT_QUOTES, 'UTF-8');
					$schedule[$i]['Schedule']['customer_address']   = $a_address;
					$schedule[$i]['Schedule']['customer_longitude'] = $booking->longitude;
					$schedule[$i]['Schedule']['customer_latitude']  = $booking->latitude;
					$schedule[$i]['Schedule']['customer_mobile']    = $booking->customer_source;
					$schedule[$i]['Schedule']['customer_type']      = $payment_type;
					$schedule[$i]['Schedule']['shift_start']        = date('H:i', strtotime($booking->time_from));
					$schedule[$i]['Schedule']['shift_end']          = date('H:i', strtotime($booking->time_to));
					$schedule[$i]['Schedule']['key_status']         = $booking->key_given == 'Y' ? "1" : "0";
					$schedule[$i]['Schedule']['area']               = $booking->area_name;
					$schedule[$i]['Schedule']['zone']               = $booking->zone_name;
					$schedule[$i]['Schedule']['booking_note']       = htmlspecialchars($booking->booking_note,ENT_QUOTES,'UTF-8');
					$schedule[$i]['Schedule']['service_fee']        = $totamount;
					$schedule[$i]['Schedule']['new_ref']            = $ref;
					$schedule[$i]['Schedule']['payMode']                = $mop;
                    $schedule[$i]['Schedule']['mop']                = $this->mode_of_paymet($booking->payment_method);
					if ($booking->cleaning_material == "Y") {
						$schedule[$i]['Schedule']['cleaning_material'] = "yes";
						$schedule[$i]['Schedule']['tools']                 = 'Normal Cleaning Set';
					} else {
						$schedule[$i]['Schedule']['cleaning_material'] = "no";
						$schedule[$i]['Schedule']['tools']                 = '';
					}
					if($booking->customer_notes != "")
					{
						$schedule[$i]['Schedule']['customer_notes']           = $booking->customer_notes;
					} else {
						$schedule[$i]['Schedule']['customer_notes']           = "";
					}
					$schedule[$i]['Schedule']['servicetype']           = $booking->service_type_name;
					$schedule[$i]['Schedule']['extra_service']         = $ext_services;
					$schedule[$i]['Schedule']['outstanding_balance']   = (string) round($booking->balance,2);
					// $schedule[$i]['Schedule']['payment_status']        = (int)$payment_status;
					$schedule[$i]['Schedule']['payment_status']        = (int)$booking->payment_status;
					$schedule[$i]['Schedule']['paid_amount']           = (string)  round($paid_amount,2);
					$schedule[$i]['Schedule']['balance']               = (string)($paid_amount > $booking->total_amount)?'0':round(($booking->total_amount - $paid_amount),2);
					$schedule[$i]['Schedule']['total']                 = (string) round($booking->total_amount,2);
					$schedule[$i]['Schedule']['starttime']                 = $starttime;
					$schedule[$i]['Schedule']['endtime']                 = $endtime;

					$schedule[$i]['maid_details']   = array();
                    $schedule[$i]['maid_details'][0]['booking_id']        = $booking->booking_id;
                    $schedule[$i]['maid_details'][0]['service_fee']       = $totamount;
                    $schedule[$i]['maid_details'][0]['total']             = (string) round($booking->total_amount,2);
					$schedule[$i]['maid_details'][0]['maid_id']           = $booking->maid_id;
					$schedule[$i]['maid_details'][0]['customer_id']       = $booking->customer_id;
					$schedule[$i]['maid_details'][0]['maid_name']         = $booking->maid_name;
					$schedule[$i]['maid_details'][0]['maid_country']      = $booking->maid_nationality;
					$schedule[$i]['maid_details'][0]['maid_attandence']   = $m_attandence;
					$maid_image           =  (strlen($booking->maid_photo_file)>0)?$booking->maid_photo_file:'';
					if(strlen($maid_image)>0)
					{
						
						if(file_exists(FCPATH.'maidimg/'.$maid_image))
						{
							$maid_image = base_url().'maidimg/'.$maid_image;    
						}
						else
						{
							$maid_image = '';
						}
					}

					$schedule[$i]['maid_details'][0]['maid_image']   = $maid_image;

					if(strlen($booking->reference_id)>0)
					{
						$related_bookings=$this->crew_appapi_lat_model->get_related_bookings($booking->reference_id,$booking->booking_id,$service_date);
						if($related_bookings)
						{
							$j=0;
							foreach ($related_bookings as $rel_buks) {
								// $schedule[$i]['Schedule']['related_bookings'][$j]['maid_id']=$rel_buks->maid_id;
								// $schedule[$i]['Schedule']['related_bookings'][$j]['booking_id']=$rel_buks->booking_id;
								// $schedule[$i]['Schedule']['related_bookings'][$j]['customer_id']=$rel_buks->customer_id;
								// $schedule[$i]['Schedule']['related_bookings'][$j]['reference_id']=$rel_buks->reference_id;
								// $schedule[$i]['Schedule']['related_bookings'][$j]['maid_attendance']=(is_numeric($rel_buks->attandence_status))?$rel_buks->attandence_status:'0';


								$schedule[$i]['maid_details'][$j+1]['maid_id']           = $rel_buks->maid_id;
								$schedule[$i]['maid_details'][$j+1]['customer_id']       = $rel_buks->customer_id;
								$schedule[$i]['maid_details'][$j+1]['maid_name']         = $rel_buks->maid_name;
								$schedule[$i]['maid_details'][$j+1]['maid_country']      = $rel_buks->maid_nationality;

								$maid_image           =  (strlen($rel_buks->maid_photo_file)>0)?$rel_buks->maid_photo_file:'';
								if(strlen($maid_image)>0)
								{
									
									if(file_exists(FCPATH.'maidimg/'.$maid_image))
									{
										$maid_image = base_url().'maidimg/'.$maid_image;    
									}
									else
									{
										$maid_image = '';
									}
								}
								
								$schedule[$i]['maid_details'][$j+1]['maid_image']   = $maid_image;

								$maid_attandence = $this->crew_appapi_lat_model->get_maid_attandence_by_date($rel_buks->maid_id, $service_date);
								if (isset($maid_attandence->attandence_id) && ($tablet->tablet_id == $maid_attandence->tablet_id)) 
								{
									$m_attandence = $maid_attandence->attandence_status;
								} 
								else 
								{
									$m_attandence = "0";
								}

								$schedule[$i]['maid_details'][$j+1]['maid_attandence']   = $m_attandence;

								$j++;
							}
						}
					}

					// $schedule[$i]['maid_details']['maid_id']           = $booking->maid_id;
					// $schedule[$i]['maid_details']['customer_id']       = $booking->customer_id;
					// $schedule[$i]['maid_details']['maid_name']         = $booking->maid_name;
					// $schedule[$i]['maid_details']['maid_country']      = $booking->maid_nationality;
					// $schedule[$i]['maid_details']['maid_attandence']   = $m_attandence;



					$i++;
				}
			}
        }
		//pradeesh changes 15-09-21
        if(!$schedule){
        $schedule[0]['id']='';
        $schedule[0]['schedule_date']=date('d-m-Y',strtotime($service_date));
        $schedule[0]['schedule_timeing']=(object) [
            "timeing"=> "",
            "No.of.maids"=> 0
        ];
        $schedule[0]['Schedule']=(object) [
            "booking_id"=> "",
            'istransfer' => 0,
            "vaccum_cleaner" => 0,
            'booking_type' => '',
            "service_status"=> 0,
            "customer_id"=> "",
            "customer_code"=> "",
            "customer_name"=> "",
            "customer_address"=> "",
            "customer_longitude"=> "",
            "customer_latitude"=> "",
            "customer_mobile"=> "",
            "customer_type"=> "",
            "shift_start"=> "",
            "shift_end"=> "",
            "key_status"=> "",
            "area"=> "",
            "zone"=> "",
            "booking_note"=> "",
            "service_fee"=> "",
            "new_ref"=> "",
            "payMode"=> "",
            "mop"=> "",
            "cleaning_material"=> "",
            "tools"=> "",
            "customer_notes"=> "",
            "servicetype"=> "e",
            "extra_service"=> "",
            "outstanding_balance"=> "",
            "payment_status"=> 0,
            "paid_amount"=> 0.0,
            "balance"=> 00,
            "total"=> "",
            "starttime"=> "",
            "endtime"=> ""
        ];
        $schedule[0]['maid_details']=[
        //     [
        //     "maid_id"=> "",
        //     "customer_id"=> "",
        //     "maid_name"=> "",
        //     "maid_country"=> "",
        //     "maid_attandence"=> "",
        //     "maid_image"=> ""
        // ]
        ];
        }
        //pradeesh changes 15-09-21
        return $schedule;
    }
	
	public function payment_list() 
    {
        $id=$this->input->post('id');
        $token=$this->input->post('token');
        $datee=$this->input->post('date');
        $maid_driver_stat=$this->input->post('maid_driver_status');

        if (strlen($id)>0 && is_numeric($maid_driver_stat) && strlen($datee)>0 ) 
        {
             if($maid_driver_stat==0)
             {
				$token_check = $this->crew_appapi_lat_model->token_maid_check($id, $token);
				if(isset($token_check->maid_id))
				{
					$this->getschedule_payments_bymaid_new($id,$datee);
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
             }
             else
             {
				$token_tab_check = $this->crew_appapi_lat_model->token_tablet_check($id, $token);
				if(isset($token_tab_check->tablet_id))
				{
					$this->getschedule_payments_new($id,$datee);
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
             }
        }
        else
        {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
	
	public function getschedule_payments_bymaid_new($maid_id,$service_date) 
    {
        if (is_numeric($maid_id) && $service_date ) 
        {
            $booking_id = 0;
            
            $bookings = $this->crew_appapi_lat_model->get_schedule_payments_by_date_for_tab_by_maid_id($service_date,$maid_id);
            $schedule = array();
            $i = 0;$payment_total=0;
            foreach ($bookings as $booking) 
            {
                $service_status=0;
                if($booking->customer_address == "")
                {
                    $a_address = $booking->building.', '.$booking->unit_no.''.$booking->street;
                } 
                else 
                {
                    if($booking->building != "")
                    {
                        $t_address = "Apt No: ".$booking->building.", ".$booking->customer_address;
                    } else {
                        $t_address = $booking->customer_address;
                    }
                    $a_address = htmlspecialchars($t_address, ENT_QUOTES, 'UTF-8');
                }

                $zonename = $booking->zone_name;
                
                if($booking->driver_name != "")
                {
                    $driver = " (".$booking->driver_name.")";
                } else {
                    $driver = "";
                }
                
                if($booking->booking_id != $booking_id)
                {
                    $day_service = $this->crew_appapi_lat_model->get_day_service_by_booking_id($service_date, $booking->booking_id);
                    if(!empty($day_service))
                    {
                        $pick_zone = $day_service->zone_name.' ('.$day_service->driver_name.')';
                    } else {
                        $pick_zone = "";
                    }

                    if($day_service)
                    {
                        $service_status=$day_service->service_status;
                    }

                    $payment_type = 'Daily Paying';
                    if ($booking->payment_type == 'D') 
                    {
                        $payment_type = 'Daily Paying';
                    } 
                    else if ($booking->payment_type == 'W') 
                    {
                        $payment_type = 'Weekly Paying';
                    } else if ($booking->payment_type == 'M') 
                    {
                        $payment_type = 'Monthly Paying';
                    }

                    $maid_attandence = $this->crew_appapi_lat_model->get_maid_attandence_by_date($booking->maid_id, $service_date);
                    if (isset($maid_attandence->attandence_id)) 
                    {
                        $m_attandence = $maid_attandence->attandence_status;
                    } 
                    else 
                    {
                        $m_attandence = "0";
                    }

                    $check_buk_exist = $this->crew_appapi_lat_model->get_booking_exist($booking->booking_id,$service_date);
                    if(!empty($check_buk_exist))
                    {
                        $mop = $check_buk_exist->mop;
                        $ref = $check_buk_exist->just_mop_ref;
                    } else {
                        $mop = $booking->payment_mode;
                        $ref = "";
                    }
                    
                    if($mop == "Credit Card")
                    {
                        $totamount = 'CC';
                    } else {
                        $totamount = $booking->total_amount;
                    }

                    $schedule[$i]['customer_id']        = $booking->customer_id;
                    $schedule[$i]['customer_name']        = $booking->customer_name;
                    $schedule[$i]['booking_id']         = $booking->booking_id;
                    $schedule[$i]['schedule_date']     = date('d-m-Y',strtotime($service_date));
                    $schedule[$i]['service_status']     = $service_status; 
                    $schedule[$i]['customer_mobile']    = $booking->customer_source;
                    $schedule[$i]['customer_type']      = $payment_type;
                    $schedule[$i]['shift_start']        = date('H:i', strtotime($booking->time_from));
                    $schedule[$i]['shift_end']          = date('H:i', strtotime($booking->time_to));
                    $schedule[$i]['key_status']         = $booking->key_given == 'Y' ? "1" : "0";
                    $schedule[$i]['area']               = $booking->area_name.$driver;
                    $schedule[$i]['zone']               = $booking->zone_name;
                    $schedule[$i]['booking_note']       = htmlspecialchars($booking->booking_note,ENT_QUOTES,'UTF-8');
                    $schedule[$i]['service_fee']        = $totamount;
                    $schedule[$i]['new_ref']            = $ref;
                    $schedule[$i]['mop']                = $mop;
                    if ($booking->cleaning_material == "Y") {
                        $schedule[$i]['cleaning_material'] = "yes";
                    } else {
                        $schedule[$i]['cleaning_material'] = "no";
                    }
                    $schedule[$i]['paymentstatus']      = 1;
                    $schedule[$i]['billing']            = $booking->total_amount;
                    $schedule[$i]['collected']          = $booking->paid_amount;
                    $payment_total = $payment_total + $booking->paid_amount;

                    $schedule[$i]['maid_details']       = array();
                    $schedule[$i]['maid_details'][0]['maid_id']           = $booking->maid_id;
					$schedule[$i]['maid_details'][0]['booking_id']        = $booking->booking_id;
                    $schedule[$i]['maid_details'][0]['service_fee']       = $totamount;
                    $schedule[$i]['maid_details'][0]['total']             = (string) round($booking->total_amount,2);
                    $schedule[$i]['maid_details'][0]['customer_id']       = $booking->customer_id;
                    $schedule[$i]['maid_details'][0]['customer_name']     = $booking->customer_name;
                    $schedule[$i]['maid_details'][0]['maid_name']         = $booking->maid_name;
                    $schedule[$i]['maid_details'][0]['maid_country']      = $booking->maid_nationality;
                    $schedule[$i]['maid_details'][0]['maid_attandence']   = $m_attandence;
                    $maid_image  =  (strlen($booking->maid_photo_file)>0)?$booking->maid_photo_file:'';
                        if(strlen($maid_image)>0)
                        {
                            
                            if(file_exists(FCPATH.'maidimg/'.$maid_image))
                            {
                                $maid_image = base_url().'maidimg/'.$maid_image;    
                            }
                            else
                            {
                                $maid_image = '';
                            }
                        }
                    $schedule[$i]['maid_details'][0]['maid_image']   = $maid_image;

                    if(strlen($booking->reference_id)>0)
                    { 
                        $related_bookings=$this->crew_appapi_lat_model->get_related_bookings($booking->reference_id,$booking->booking_id,$service_date);
                        if($related_bookings)
                        {
                            $j=0;
                            foreach ($related_bookings as $rel_buks) {
                                $schedule[$i]['maid_details'][$j+1]['maid_id']        = $rel_buks->maid_id;
                                $schedule[$i]['maid_details'][$j+1]['customer_id']    = $rel_buks->customer_id;
                                $schedule[$i]['maid_details'][$j+1]['customer_name']  = $rel_buks->customer_name;
                                $schedule[$i]['maid_details'][$j+1]['maid_name']      = $rel_buks->maid_name;
                                $schedule[$i]['maid_details'][$j+1]['maid_country']   = $rel_buks->maid_nationality;
                                $schedule[$i]['maid_details'][$j+1]['maid_attendance']= (is_numeric($rel_buks->attandence_status))?$rel_buks->attandence_status:'0';
                                $maid_image  =  (strlen($rel_buks->maid_photo_file)>0)?$rel_buks->maid_photo_file:'';
                                if(strlen($maid_image)>0)
                                {
                                    
                                    if(file_exists(FCPATH.'maidimg/'.$maid_image))
                                    {
                                        $maid_image = base_url().'maidimg/'.$maid_image;    
                                    }
                                    else
                                    {
                                        $maid_image = '';
                                    }
                                }
                                $schedule[$i]['maid_details'][$j+1]['maid_image']   = $maid_image;
                                $j++;
                            }
                        }
                    }
                    $i++;
                }
            }
            
            echo json_encode(array('status' => 'success','message' => 'Payments details retrieved successfully','total' => $payment_total, 'schedule' => $schedule));
            exit();
            
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '1015';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
	
	public function getschedule_payments_new($id,$service_date)
	{
        if ($id &&  strlen($id) > 0 && $service_date)
		{
            $tablet = $this->crew_appapi_lat_model->get_tablet_by_tabletid($id);

            if (isset($tablet->tablet_id) && $tablet->tablet_status == 1)
			{
                $bookings = $this->crew_appapi_lat_model->get_schedule_payments_by_date_for_tab($service_date);

                $booking_transfers = $this->crew_appapi_lat_model->get_booking_transfers_by_date_new($service_date);
                $transferred_booking_zones = array();
                foreach ($booking_transfers as $b_transfer) 
                {
                    //$transferred_booking_zones[$b_transfer->booking_id] = $b_transfer->zone_id;
					$transferred_booking_zones[$b_transfer->booking_id] = $b_transfer->transfering_to_tablet;
                }
        
                $schedule = array();
                $i = 0;
                $service_status =0;$payment_total =0;
                foreach ($bookings as $booking)
				{
                    //if (($booking->zone_id == $tablet->zone_id && !isset($transferred_booking_zones[$booking->booking_id])) || (isset($transferred_booking_zones[$booking->booking_id]) && $transferred_booking_zones[$booking->booking_id] == $tablet->zone_id))
					if (($booking->tabletid == $tablet->tablet_id && !isset($transferred_booking_zones[$booking->booking_id])) || (isset($transferred_booking_zones[$booking->booking_id]) && $transferred_booking_zones[$booking->booking_id] == $tablet->tablet_id))
					{
                        $payment_type = 'Daily Paying';
                        if ($booking->payment_type == 'D') 
                        {
                            $payment_type = 'Daily Paying';
                        } 
                        else if ($booking->payment_type == 'W') 
                        {
                            $payment_type = 'Weekly Paying';
                        } else if ($booking->payment_type == 'M') 
                        {
                            $payment_type = 'Monthly Paying';
                        }

                        $maid_attandence = $this->crew_appapi_lat_model->get_maid_attandence_by_date($booking->maid_id, $service_date);
                        if (isset($maid_attandence->attandence_id)) 
                        {
                            $m_attandence = $maid_attandence->attandence_status;
                        } 
                        else 
                        {
                            $m_attandence = "0";
                        }

                        $day_service = $this->crew_appapi_lat_model->get_day_service_by_booking_id($service_date, $booking->booking_id);

                        if (isset($day_service->day_service_id)) 
                        {
                            $service_status = $day_service->service_status;
                            $totalamount = $day_service->total_fee;
                            //$discount = $booking->discount;
                            //$totamount = $totalamount - $discount;
                            //removed showing from booking total only this line $totamount = $totalamount;
                            if($booking->payment_mode == "Credit Card")
                            {
                                $totamount = 'CC';
                            } 
                            else 
                            {
                                $totamount = $booking->total_amount;
                            }
                        } 
                        else 
                        {
                            $normal_hours = 0;
                            $extra_hours = 0;
                            $weekend_hours = 0;

                            $normal_from = strtotime('06:00:00');
                            $normal_to = strtotime('21:00:00');

                            $shift_from = strtotime($booking->time_from . ':00');
                            $shift_to = strtotime($booking->time_to . ':00');

                            if (date('w') != 5) 
                            {
                                if ($shift_from < $normal_from) 
                                {
                                    if ($shift_to <= $normal_from) 
                                    {
                                        $extra_hours = ($shift_to - $shift_from) / 3600;
                                    }

                                    if ($shift_to > $normal_from && $shift_to <= $normal_to) 
                                    {
                                        $extra_hours = ($normal_from - $shift_from) / 3600;
                                        $normal_hours = ($shift_to - $normal_from) / 3600;
                                    }

                                    if ($shift_to > $normal_to) 
                                    {
                                        $extra_hours = ($normal_from - $shift_from) / 3600;
                                        $extra_hours += ($shift_to - $normal_to) / 3600;
                                        $normal_hours = ($normal_to - $normal_from) / 3600;
                                    }
                                }

                                if ($shift_from >= $normal_from && $shift_from < $normal_to) 
                                {
                                    if ($shift_to <= $normal_to) 
                                    {
                                        $normal_hours = ($shift_to - $shift_from) / 3600;
                                    }

                                    if ($shift_to > $normal_to) 
                                    {
                                        $normal_hours = ($normal_to - $shift_from) / 3600;
                                        $extra_hours = ($shift_to - $normal_to) / 3600;
                                    }
                                }

                                if ($shift_from > $normal_to) 
                                {
                                    $extra_hours = ($shift_to - $shift_from) / 3600;
                                }
                            } 
                            else 
                            {
                                $weekend_hours = ($shift_to - $shift_from) / 3600;
                            }

                            $service_description = array();

                            $service_description['normal'] = new stdClass();
                            $service_description['normal']->hours = $normal_hours;
                            $service_description['normal']->fees = $normal_hours * $booking->price_hourly;

                            $service_description['extra'] = new stdClass();
                            $service_description['extra']->hours = $extra_hours;
                            $service_description['extra']->fees = $extra_hours * $booking->price_extra;

                            $service_description['weekend'] = new stdClass();
                            $service_description['weekend']->hours = $weekend_hours;
                            $service_description['weekend']->fees = $weekend_hours * $booking->price_weekend;

                            
                            $location_charge = 0;

                            $customer_address = $this->crew_appapi_lat_model->get_customer_address_by_id($booking->customer_address_id);
                            $cus_detail = $this->crew_appapi_lat_model->get_customer_by_id($booking->customer_id);
                            if ($customer_address) 
                            {
                                $area = $this->crew_appapi_lat_model->get_area_by_id($customer_address->area_id);
                                if ($area) 
                                {
                                    $location_charge = $area->area_charge;
                                }
                            }

                            $service_status = 0;
                            $totalamount = 0;
                            $discount = 0;
                            
                            if($booking->payment_mode == "Credit Card")
                            {
                                $totamount = 'CC';
                            } 
                            else 
                            {
                                $totamount = $booking->total_amount;
                            }
                        }
                        
                        if($booking->customer_address == "")
                        {
                            $a_address = $booking->building.', '.$booking->unit_no.''.$booking->street;
                        } else {
                            if($booking->building != "")
                            {
                                $t_address = "Apt No: ".$booking->building.", ".$booking->customer_address;
                            } else {
                                $t_address = $booking->customer_address;
                            }
                            $a_address = htmlspecialchars($t_address, ENT_QUOTES, 'UTF-8');
                        }
                        
                        $check_buk_exist = $this->crew_appapi_lat_model->get_booking_exist($booking->booking_id,$service_date);
                        if(!empty($check_buk_exist))
                        {
                            $mop = $check_buk_exist->mop;
                            $ref = $check_buk_exist->just_mop_ref;
                        } else {
                            $mop = $booking->payment_mode;
                            $ref = "";
                        }
                        
                        if($mop == "Credit Card")
                        {
                            $totamount = 'CC';
                        } else {
                            $totamount = $booking->total_amount;
                        }
                        
                        if($booking->driver != "")
                        {
                            $driver = " (".$booking->driver.")";
                        } else {
                            $driver = "";
                        }

                        $schedule[$i]['customer_id']        = $booking->customer_id;
                        $schedule[$i]['customer_name']      = $booking->customer_name;
                        $schedule[$i]['booking_id']         = $booking->booking_id;
                        $schedule[$i]['schedule_date']      = date('d-m-Y',strtotime($service_date));
                        $schedule[$i]['service_status']     = $service_status; 
                        $schedule[$i]['customer_mobile']    = $booking->customer_source;
                        $schedule[$i]['customer_type']      = $payment_type;
                        $schedule[$i]['shift_start']        = date('H:i', strtotime($booking->time_from));
                        $schedule[$i]['shift_end']          = date('H:i', strtotime($booking->time_to));
                        $schedule[$i]['key_status']         = $booking->key_given == 'Y' ? "1" : "0";
                        $schedule[$i]['area']               = $booking->area_name.$driver;
                        $schedule[$i]['zone']               = $booking->zone_name;
                        $schedule[$i]['booking_note']       = htmlspecialchars($booking->booking_note,ENT_QUOTES,'UTF-8');
                        $schedule[$i]['service_fee']        = $totamount;
                        $schedule[$i]['new_ref']            = $ref;
                        $schedule[$i]['mop']                = $mop;
                        if ($booking->cleaning_material == "Y") {
                            $schedule[$i]['cleaning_material'] = "yes";
                        } else {
                            $schedule[$i]['cleaning_material'] = "no";
                        }
                        $schedule[$i]['paymentstatus']      = 1;
                        $schedule[$i]['billing']            = $booking->total_amount;
                        $schedule[$i]['collected']          = $booking->paid_amount;
                        $payment_total = $payment_total + $booking->paid_amount;    

                        $schedule[$i]['maid_details']       = array();
                        $schedule[$i]['maid_details'][0]['maid_id']           = $booking->maid_id;
						$schedule[$i]['maid_details'][0]['booking_id']        = $booking->booking_id;
                        $schedule[$i]['maid_details'][0]['service_fee']       = $totamount;
                        $schedule[$i]['maid_details'][0]['total']             = (string) round($booking->total_amount,2);
                        $schedule[$i]['maid_details'][0]['customer_id']       = $booking->customer_id;
                        $schedule[$i]['maid_details'][0]['customer_name']     = $booking->customer_name;
                        $schedule[$i]['maid_details'][0]['maid_name']         = $booking->maid_name;
                        $schedule[$i]['maid_details'][0]['maid_country']      = $booking->maid_nationality;
                        $schedule[$i]['maid_details'][0]['maid_attandence']   = $m_attandence;
                        $maid_image           =  (strlen($booking->maid_photo_file)>0)?$booking->maid_photo_file:'';
                        if(strlen($maid_image)>0)
                        {
                            
                            if(file_exists(FCPATH.'maidimg/'.$maid_image))
                            {
                                $maid_image = base_url().'maidimg/'.$maid_image;    
                            }
                            else
                            {
                                $maid_image = '';
                            }
                        }
                        $schedule[$i]['maid_details'][0]['maid_image']   = $maid_image;

                        if(strlen($booking->reference_id)>0)
                        { 
                            $related_bookings=$this->crew_appapi_lat_model->get_related_bookings($booking->reference_id,$booking->booking_id,$service_date);
                            if($related_bookings)
                            {
                                $j=0;
                                foreach ($related_bookings as $rel_buks) {
                                    $schedule[$i]['maid_details'][$j+1]['maid_id']        = $rel_buks->maid_id;
                                    $schedule[$i]['maid_details'][$j+1]['customer_id']    = $rel_buks->customer_id;
                                    $schedule[$i]['maid_details'][$j+1]['customer_name']  = $rel_buks->customer_name;
                                    $schedule[$i]['maid_details'][$j+1]['maid_name']      = $rel_buks->maid_name;
                                    $schedule[$i]['maid_details'][$j+1]['maid_country']   = $rel_buks->maid_nationality;
                                    $schedule[$i]['maid_details'][$j+1]['maid_attendance']= (is_numeric($rel_buks->attandence_status))?$rel_buks->attandence_status:'0';
                                    $maid_image           =  (strlen($rel_buks->maid_photo_file)>0)?$rel_buks->maid_photo_file:'';
                                    if(strlen($maid_image)>0)
                                    {
                                        
                                        if(file_exists(FCPATH.'maidimg/'.$maid_image))
                                        {
                                            $maid_image = base_url().'maidimg/'.$maid_image;    
                                        }
                                        else
                                        {
                                            $maid_image = '';
                                        }
                                    }
                                    $schedule[$i]['maid_details'][$j+1]['maid_image']   = $maid_image;
                                    $j++;
                                }
                            }
                        }
                        $i++;
                    }
                }

                echo json_encode(array('status' => 'success','message'=>'Payment details retrieved successfully','total'=>$payment_total, 'schedule' => $schedule));
                exit();
            } else if (isset($tablet->tablet_id) && $tablet->tablet_status == 0) {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '103';
                $response['message'] = 'Inactive tablet';

                echo json_encode($response);
                exit();
            } else {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '102';
                $response['message'] = 'Invalid ID';

                echo json_encode($response);
                exit();
            }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '10177';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
	
	public function profile_edit() 
    {
        $id                 = $this->input->post('id');
        $token              = $this->input->post('token');
        $datee              = $this->input->post('date');
        $maid_driver_stat   = $this->input->post('maid_driver_stat');
        $response = array();

        $booking_count=0;$booking_hours=0;$total_billed=0;$total_collected=0;

        if(is_numeric($maid_driver_stat) && strlen($datee)>0 && strlen($id)>0 && strlen($token)>0) 
        {
            if($maid_driver_stat=='0') // maid profile edit
			{
				$token_check = $this->crew_appapi_lat_model->token_maid_check($id, $token);
				if(isset($token_check->maid_id))
				{
					$maid_data=array();
					$full_name  = $this->input->post('full_name');
					if(strlen($full_name)>0)
					{
						$maid_data['maid_name']=$full_name;
					}

					$phone_no  = $this->input->post('phone_no');
					if(strlen($phone_no)>0)
					{
						$maid_data['maid_mobile_1']=$phone_no;
					}

					$email  = $this->input->post('email');
					if(strlen($email)>0)
					{
						
					}

					if(count($maid_data) > 0)
					{
						$this->crew_appapi_lat_model->update_maids($maid_data,$id);
					}
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.'));
					exit();
				}
             }
             else                     // driver profile edit
             {
                
             }
             $response = array();
             $response['status']     = 'success';
             $response['message']    = 'Details Updated successfully';

             echo json_encode($response);
             exit();

        }        
        else
        {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '1012';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
	
	public function maid_details_list() 
    {
        $id=$this->input->post('id');
        $token=$this->input->post('token');
        $datee=$this->input->post('date');
        $maid_driver_stat=$this->input->post('maid_driver_status');
        $booking_id=$this->input->post('booking_id');
        $response = array();
		
		if($maid_driver_stat=='0')
		{
			$token_check = $this->crew_appapi_lat_model->token_maid_check($id, $token);
			if(isset($token_check->maid_id))
			{
			
			} else {
				echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
			}
		} else {
			$token_tab_check = $this->crew_appapi_lat_model->token_tablet_check($id, $token);
			if(isset($token_tab_check->tablet_id))
			{
			
			} else {
				echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
			}
		}
		
        if(is_numeric($maid_driver_stat) && strlen($datee)>0 && strlen($id)>0 && strlen($booking_id)>0) 
        {
            $booking_details = $this->crew_appapi_lat_model->get_booking_by_id($booking_id);
            $maid_details    = $this->crew_appapi_lat_model->get_maid_details($booking_details->maid_id);

            $maiddtls=array();
            $maiddtls['maid_id']      = $maid_details->maid_id;
            $maiddtls['maid_name']    = $maid_details->maid_name;
            $maid_attandence = $this->crew_appapi_lat_model->get_maid_attandence_by_date($maid_details->maid_id, $datee);
            if (isset($maid_attandence->attandence_id)) 
            {
                $m_attandence = $maid_attandence->attandence_status;
            } 
            else 
            {
                $m_attandence = "0";
            }
            $maiddtls['maid_attendance']  = $m_attandence;
            $maid_image               = $maid_details->maid_photo_file;
            $maiddtls['maid_image']   = ''; 
            if(strlen($maid_image)>0)
            {
                
                if(file_exists(FCPATH.'maidimg/'.$maid_image))
                {
                    $maiddtls['maid_image']   = base_url().'maidimg/'.$maid_image;    
                }                
                else
                {
                    $maid_image = '';
                }
            }
            $maiddtls['booking_id']     = $booking_id;
            $maiddtls['customer_id']    = $booking_details->customer_id;
            $maiddtls['customer_name']  = $booking_details->customer_name;
            $maiddtls['schedule_date']  = $datee;
            $maiddtls['shift_start']    = $booking_details->time_from;
            $maiddtls['shift_end']      = $booking_details->time_to;

            $next_schedules = array();
            $bookings = $this->crew_appapi_lat_model->get_schedule_by_date_for_tab_by_maid_id($datee,$maid_details->maid_id);
            foreach ($bookings as $booking)
			{
                if($booking->booking_id == $booking_id){continue;}
                $service_status=0;
                $day_service = $this->crew_appapi_lat_model->get_day_service_by_booking_id($datee, $booking->booking_id);
                if($day_service)
                {
                    $service_status=$day_service->service_status;
                }
                $schedule  =  array();
                $schedule['service_status']     = $service_status;
                $schedule['booking_id']         = $booking->booking_id;
                $schedule['customer_id']        = $booking->customer_id;
                $schedule['customer_name']      = $booking->customer_name;
                $schedule['schedule_date']      = $datee;
                $schedule['shift_start']        = $booking->time_from;
                $schedule['shift_end']          = $booking->time_to;
                $schedule['customer_address']   = strlen($booking->customer_address)>0 ?$booking->customer_address:'';
                $schedule['zone']               = $booking->zone_name;

                $next_schedules[]          = $schedule;
            }
            $maiddtls['next_schedules']      = $next_schedules; 

            $response['status']            = 'success';
            $response['message']           = 'Maid details retrieved successfully';
            $response['maid_details'][0]   = $maiddtls;

            if(strlen($booking_details->reference_id)>0)
            {
                $related_bookings=$this->crew_appapi_lat_model->get_related_bookings($booking_details->reference_id,$booking_details->booking_id,$datee);

                if($related_bookings)
                {
                    $j=1;
                    foreach ($related_bookings as $rel_buks) 
                    {
                        $booking_details = $this->crew_appapi_lat_model->get_booking_by_id($rel_buks->booking_id);
                        
                        $maid_details    = $this->crew_appapi_lat_model->get_maid_details($booking_details->maid_id);
                        //echo $this->db->last_query();
                        //echo 'maidid='.$booking_details->maid_id;
                        $maiddtls=array();
                        $maiddtls['maid_id']          = $maid_details->maid_id;
                        $maiddtls['maid_name']        = $maid_details->maid_name;
                        $maid_attandence = $this->crew_appapi_lat_model->get_maid_attandence_by_date($maid_details->maid_id, $datee);
                        if (isset($maid_attandence->attandence_id)) 
                        {
                            $m_attandence = $maid_attandence->attandence_status;
                        } 
                        else 
                        {
                            $m_attandence = "0";
                        }
                        $maiddtls['maid_attendance']  = $m_attandence;
                        $maid_image                   = $maid_details->maid_photo_file;
                        $maiddtls['maid_image']       = ''; 
                        if(strlen($maid_image)>0)
                        {
                            
                            if(file_exists(FCPATH.'maidimg/'.$maid_image))
                            {
                                $maiddtls['maid_image']   = base_url().'maidimg/'.$maid_image;    
                            }
                            else
                            {
                                $maid_image = '';
                            }

                        }
                        $maiddtls['booking_id']     = $rel_buks->booking_id;
                        $maiddtls['customer_id']    = $booking_details->customer_id;
                        $maiddtls['customer_name']  = $booking_details->customer_name;
                        $maiddtls['schedule_date']  = $datee;
                        $maiddtls['shift_start']    = $booking_details->time_from;
                        $maiddtls['shift_end']      = $booking_details->time_to;

                        $next_schedules = array();
                        $bookings = $this->crew_appapi_lat_model->get_schedule_by_date_for_tab_by_maid_id($datee,$booking_details->maid_id);
                        foreach ($bookings as $booking) 
                        {
                            if($booking->booking_id == $rel_buks->booking_id){continue;}
                            $service_status=0;
                            $day_service = $this->crew_appapi_lat_model->get_day_service_by_booking_id($datee, $booking->booking_id);
                            if($day_service)
                            {
                                $service_status=$day_service->service_status;
                            }

                            $schedule  =  array();
                            $schedule['service_status']     = $service_status;
                            $schedule['booking_id']         = $booking->booking_id;
                            $schedule['customer_id']        = $booking->customer_id;
                            $schedule['customer_name']      = $booking->customer_name;
                            $schedule['schedule_date']      = $datee;
                            $schedule['shift_start']        = $booking->time_from;
                            $schedule['shift_end']          = $booking->time_to;
                            $schedule['customer_address']   = strlen($booking->customer_address)>0 ?$booking->customer_address:'';
                            $schedule['zone']               = $booking->zone_name;

                            $next_schedules[]          = $schedule;
                        }
                        $maiddtls['next_schedules']      = $next_schedules;

                        $response['maid_details'][$j]   = $maiddtls;
                        $j++;
                    }
                }
            }

            echo json_encode($response);
            exit();

        }
        else
        {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '1012';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
	
	public function maid_in_out()
	{
        if ($this->input->post('id') && is_numeric($this->input->post('maid_driver_stat')) && $this->input->post('maid_id') && $this->input->post('status') && is_numeric($this->input->post('maid_id')) && $this->input->post('maid_id') > 0 && is_numeric($this->input->post('status')) && $this->input->post('date'))
		{
            $id               = $this->input->post('id');
            $maid_driver_stat = $this->input->post('maid_driver_stat');
            $status           = $this->input->post('status');
            $token            = $this->input->post('token');
            $maid_id          = $this->input->post('maid_id');
            $datee            = $this->input->post('date');

            $tablet = '';
            if($maid_driver_stat==0)
			{
				$token_check = $this->crew_appapi_lat_model->token_maid_check($id, $token);
				if(isset($token_check->maid_id))
				{
					$response = array();
					$response['status'] = 'error';
					$response['error_code'] = '103';
					$response['message'] = 'Maid attendance can be marked by driver only!';

					echo json_encode($response);
					exit();
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
             } else {
				$token_tab_check = $this->crew_appapi_lat_model->token_tablet_check($id, $token);
				if(isset($token_tab_check->tablet_id))
				{
					$tablet = $this->crew_appapi_lat_model->get_tablet_by_tabletid($id);
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
             }

            //$tablet = $this->tablets_model->get_tablet_by_imei($imei);
            /* -------------------------------Maid details start------------------------------- */
            $maid_details    = $this->crew_appapi_lat_model->get_maid_details($maid_id);

            $maiddtls=array();
            $maiddtls['maid_id']            = $maid_details->maid_id;
            $maiddtls['maid_name']          = $maid_details->maid_name;
            $maid_image                     = $maid_details->maid_photo_file;
            $maiddtls['maid_image']         = ''; 
            $maiddtls['maid_attandence']    = '0'; 
            $maiddtls['maid_country']       = $maid_details->maid_nationality; 
            
            if(strlen($maid_image)>0)
            {
                
                if(file_exists(FCPATH.'maidimg/'.$maid_image))
                {
                    $maiddtls['maid_image']   = base_url().'maidimg/'.$maid_image;    
                }
                else
                {
                    $maid_image = '';
                }
            }
            /* -------------------------------Maid details end------------------------------- */


            if (isset($tablet->tablet_id) && $tablet->tablet_status == 1)
			{
                switch ($status) {
                    case 1: // maid-in
                        $chk_attandence = $this->crew_appapi_lat_model->get_maid_attandence_by_date($maid_id, $datee);

                        if (!isset($chk_attandence->attandence_id) || $chk_attandence->attandence_status != 1 || ($tablet->tablet_id != $chk_attandence->tablet_id)) {
                            $maid_in_fields = array();
                            $maid_in_fields['maid_id']      = $maid_id;
                            $maid_in_fields['zone_id']      = $tablet->zone_id;
                            $maid_in_fields['tablet_id']    = $tablet->tablet_id;
                            $maid_in_fields['date']         = date('Y-m-d');
                            $maid_in_fields['maid_in_time'] = date('H:i:s');
                            $maid_in_fields['attandence_status'] = 1;

                            $maid_in = $this->crew_appapi_lat_model->add_maid_attandence($maid_in_fields);

                            if ($maid_in) {

                                $maid_attandence = $this->crew_appapi_lat_model->get_maid_attandence_by_date($maid_id, $datee);
                                if (isset($maid_attandence->attandence_id)) 
                                {
                                    $maiddtls['maid_attandence'] = $maid_attandence->attandence_status;
                                }
                                $response = array();
                                $response['status']       = 'success';
                                $response['message']      = 'Maid-In marked successfully!';
                                $response['maid_id']      = $maid_id;
                                $response['maid_details'] = $maiddtls;
                                $response['attandence_status'] = 1;
                            } else {
                                $response = array();
                                $response['status'] = 'error';
                                $response['error_code'] = '107';
                                $response['message'] = 'Unexpected error';
                            }
                        } else {
                            $response = array();
                            $response['status'] = 'error';
                            $response['error_code'] = '105';
                            $response['message'] = 'Maid already in';
                        }

                        break;

                    case 2: // maid-out
                        $chk_attandence = $this->crew_appapi_lat_model->get_maid_attandence_by_date($maid_id, $datee);

                        if (isset($chk_attandence->attandence_id) && $chk_attandence->attandence_status == 1 && $chk_attandence->tablet_id == $tablet->tablet_id) {
                            $maid_out_fields = array();
                            $maid_out_fields['maid_out_time'] = date('H:i:s');
                            $maid_out_fields['attandence_status'] = 2;

                            $maid_out = $this->crew_appapi_lat_model->update_maid_attandence($chk_attandence->attandence_id, $maid_out_fields);
                            if ($maid_out) {
                                $maid_attandence = $this->crew_appapi_lat_model->get_maid_attandence_by_date($maid_id, $datee);
                                if (isset($maid_attandence->attandence_id)) 
                                {
                                    $maiddtls['maid_attandence'] = $maid_attandence->attandence_status;
                                }
                                $response = array();
                                $response['status']       = 'success';
                                $response['message']      = 'Maid-Out marked successfully!';
                                $response['maid_id']      = $maid_id;
                                $response['maid_details'] = $maiddtls;
                                $response['attandence_status'] = 2;
                            } else {
                                $response = array();
                                $response['status'] = 'error';
                                $response['error_code'] = '107';
                                $response['message'] = 'Unexpected error';
                            }
                        } else {
                            $response = array();
                            $response['status'] = 'error';
                            $response['error_code'] = '105';
                            $response['message'] = 'Maid not in';
                        }

                        break;

                    default:
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '104';
                        $response['message'] = 'Invalid status';
                }

                echo json_encode($response);
                exit();
            } else if (isset($tablet->tablet_id) && $tablet->tablet_status == 0) {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '103';
                $response['message'] = 'Inactive tablet';

                echo json_encode($response);
                exit();
            } else {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '102';
                $response['message'] = 'Invalid IMEI';

                echo json_encode($response);
                exit();
            }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
	
	public function get_driver_list() 
    {
        $id=$this->input->post('id');
        $token=$this->input->post('token');
        $datee=$this->input->post('date');
        $maid_driver_stat=$this->input->post('maid_driver_stat');
        $booking_id=$this->input->post('booking_id');
        $response = array();
		
        if(is_numeric($maid_driver_stat) && strlen($datee)>0 && strlen($id)>0 && strlen($booking_id)>0) 
        {
			if($maid_driver_stat=='0')
			{
				$token_check = $this->crew_appapi_lat_model->token_maid_check($id, $token);
				if(isset($token_check->maid_id))
				{
					
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			} else {
				$token_tab_check = $this->crew_appapi_lat_model->token_tablet_check($id, $token);
				if(isset($token_tab_check->tablet_id))
				{
					
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			}

            $all_tablets = $this->crew_appapi_lat_model->get_all_drivertablets();

            $transfer_list=array();
            foreach ($all_tablets as $key => $tablet) 
            {
                if($tablet->tablet_id == $zone_id){continue;}
                
                $driver_dtls=array();
                $driver_dtls['tablet_id']     =  $tablet->tablet_id;
                $driver_dtls['tablet_driver'] =  $tablet->tablet_driver_name;

                $transfer_list[]=$driver_dtls;
            }
            
            $response = array();
            $response['status']           = 'success';
            $response['message']          = 'Drivers retrieved successfully';
            $response['transfer_list']    = $transfer_list;

            echo json_encode($response);
            exit();
        }
        else
        {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '1012';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
	
	public function get_transfer_list() 
    {
        $id=$this->input->post('id');
        $token=$this->input->post('token');
        $datee=$this->input->post('date');
        $maid_driver_stat=$this->input->post('maid_driver_stat');
        $booking_id=$this->input->post('booking_id');
        $response = array();
		
        if(is_numeric($maid_driver_stat) && strlen($datee)>0 && strlen($id)>0 && strlen($booking_id)>0) 
        {
			if($maid_driver_stat=='0')
			{
				$token_check = $this->crew_appapi_lat_model->token_maid_check($id, $token);
				if(isset($token_check->maid_id))
				{
					
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			} else {
				$token_tab_check = $this->crew_appapi_lat_model->token_tablet_check($id, $token);
				if(isset($token_tab_check->tablet_id))
				{
					
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			}
			
            $booking_details = $this->crew_appapi_lat_model->get_booking_by_id($booking_id);
            $zone_id         = $booking_details->zone_id;

            $all_tablets = $this->crew_appapi_lat_model->get_all_tablets();

            $transfer_list=array();
            foreach ($all_tablets as $key => $tablet) 
            {
                if($tablet->zone_id == $zone_id){continue;}
                
                $driver_dtls=array();
                $driver_dtls['tablet_id']     =  $tablet->tablet_id;
                $driver_dtls['tablet_driver'] =  $tablet->driver_name;
                $driver_dtls['zone_id']       =  $tablet->zone_id;
                $driver_dtls['zone_name']     =  $tablet->zone_name;

                $transfer_list[]=$driver_dtls;
            }
            
            $response = array();
            $response['status']           = 'success';
            $response['message']          = 'Drivers retrieved successfully';
            $response['transfer_list']    = $transfer_list;

            echo json_encode($response);
            exit();
        }
        else
        {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '1012';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
	
	public function transfer_maid() 
    {
        $id               = $this->input->post('id');
        // $id               = 1;
        $token            = $this->input->post('token');
        // $token            = "e40b6e5c1f56ad1159861e4cfaae6a70e96c1506";
        $datee            = $this->input->post('date');
        // $datee            = "2021-09-04";
        $maid_driver_stat = $this->input->post('maid_driver_stat');
        // $maid_driver_stat = 1;
        $booking_id       = $this->input->post('booking_id');
        // $booking_id       = 10;
        $zone_id 	   	  = $this->input->post('zone_id');
        // $zone_id 	   	  = 5;
        $transfer_all     = $this->input->post('transfer_all');
        // $transfer_all     = 0;
		$is_driver_based     = $this->input->post('is_driver_based');
		$maid_id_new     = $this->input->post('maid_id');
        $response = array();$bookings_time_from=array();
		
		if(is_numeric($maid_driver_stat) && strlen($datee)>0 && strlen($id)>0 && (is_array($booking_id) || strlen($booking_id)>0) && strlen($zone_id)>0 && strlen($transfer_all)>0)
        {
			if($maid_driver_stat=='0')
			{
				$token_check = $this->crew_appapi_lat_model->token_maid_check($id, $token);
				if(isset($token_check->maid_id))
				{
					
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			} else {
				$token_tab_check = $this->crew_appapi_lat_model->token_tablet_check($id, $token);
				if(isset($token_tab_check->tablet_id))
				{
					
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			}
			
			if(!is_array($booking_id)){
                $a=$this->multiple_transfer_maids($booking_id,$datee,$zone_id,$id,$maid_driver_stat,$transfer_all,$is_driver_based);
                echo json_encode($a);
                exit();
                }else{
                    $bid=[];
                    foreach($booking_id as $b){
                        $ab=$this->multiple_transfer_maids($b,$datee,$zone_id,$id,$maid_driver_stat,$transfer_all,$is_driver_based);
                        if($ab['status']=='success'){
                            $bid[]=$ab['transfered_bookings'][0]['booking_id'];
                        }
                        $a[]=$ab;
                    }
                    echo json_encode(['status' =>$bid?'success':'error','booking_id'=>$bid,'transfer_all'=>$transfer_all,'message'=>$bid?'Booking transferred successfully!':'Booking transfer Failed!','data' =>$a]);die;
                }

        }
        else
        {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '1012';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }

    }
	
	function multiple_transfer_maids($booking_id,$datee,$zone_id,$id,$maid_driver_stat,$transfer_all,$is_driver_based){
        if($is_driver_based == "yes")
        {
            $service_date  		= $datee;
            $tablet = $this->crew_appapi_lat_model->get_tablet_by_tabletid($id);
            if (isset($tablet->tablet_id) && $tablet->tablet_status == 1) 
            {
                $check_booking_transferred = $this->crew_appapi_lat_model->check_booking_transferred_new($booking_id,$service_date);
                if(count($check_booking_transferred) > 0)
                {
                    $transferids = $check_booking_transferred->booking_transfer_tablet_id;
                } else {
                    $transferids = 0;
                }
                $datetimes = date('Y-m-d H:i:s');
                $toid = $zone_id;
                $driver_chng_array = array();
                $driver_chng_array['booking_id'] = $booking_id;
                $driver_chng_array['transfering_to_tablet'] = $toid;
                $driver_chng_array['service_date'] = $service_date;
                $driver_chng_array['transferred_date_time'] = $datetimes;
                $driver_chng_array['transferred_from'] = 'T';
                $driver_chng_array['transferred_from_id'] = $tablet->tablet_id;
                $driver_chng_array['transferred_by'] = $tablet->tablet_id;
                $user_type = $maid_driver_stat=0?'C':'D';
                $activityid = $this->crew_appapi_lat_model->insert_driverchnage($driver_chng_array,$transferids,$user_type);
                if($activityid > 0)
                {
                    $booking_detail = $this->crew_appapi_lat_model->get_booking_by_id($booking_id);
                    $transferred_bookings[] = $booking_id;
                    $transfer_details[]     = $arrayName = array('booking_id' => $booking_detail->booking_id,'customer_id' => $booking_detail->customer_id,'no_of_maids' =>0,'time_from' =>$booking_detail->time_from);
                    $tzone_tablet = $this->crew_appapi_lat_model->get_tablet_by_zone_tabid($toid);
                    //maid transit
                    $update_driver = array();
                    $update_driver['driver_name'] = $tzone_tablet->tablet_driver_name;
                    $update_driver_details = $this->crew_appapi_lat_model->update_maids($update_driver, $maid_id_new);
                    //ends
                    /* Check if maid already in */
                    $chk_attandence = $this->crew_appapi_lat_model->get_maid_attandence_by_date($maid_id_new, $service_date);
                    if (isset($chk_attandence->attandence_id) && $chk_attandence->attandence_status == 1) {
                        $maid_out_fields = array();
                        $maid_out_fields['maid_out_time'] = date('H:i:s');
                        $maid_out_fields['attandence_status'] = 2;

                        $maid_out = $this->crew_appapi_lat_model->update_maid_attandence($chk_attandence->attandence_id, $maid_out_fields);
                    }
                    
                    //$tzone_tablet = $this->tablets_model->get_tablet_by_zone_tabid($toid);
                    if(isset($tzone_tablet->google_reg_id))
                    {
                        $message = 'Booking on '. $service_date.' from '.$booking_detail->newtime_from . 'to ' . $booking_detail->newtime_to . ' of maid '. $booking_detail->maid_name . ' is transfered to '.$tzone_tablet->tablet_driver_name;
                        $push_fields = array();
                        $push_fields['tab_id'] = $tzone_tablet->tablet_id;
                        $push_fields['type'] = 1;
                        $push_fields['message'] = $message;
                        $push_fields['title'] = "Booking Transfered";
                        $push_fields['customer_name'] = $booking_detail->customer_name;
                        $push_fields['maid_name'] = $booking_detail->maid_name;
                        $push_fields['booking_time'] = $booking_detail->newtime_from.' - '.$booking_detail->newtime_to;
                        $push = $this->crew_appapi_lat_model->add_push_notifications($push_fields);
                        
                        $deviceid = $tzone_tablet->google_reg_id;
                        $title = "Booking Transfered";
                        $payload = array();
                        $payload['isfeedback'] = false;
                        if($push > 0)
                        {
                            $payload['pushid'] = $push;
                        } else {
                            $payload['pushid'] = 0;
                        }
                        
                        $res = array();											   
                        $res['title'] = $title;
                        $res['is_background'] = false;
                        $res['body'] = $message;
                        $res['image'] = "";
                        $res['payload'] = $payload;
                        $res['customer'] = $booking_detail->customer_name;
                        $res['maid'] = $booking_detail->maid_name;
                        $res['bookingTime'] = $booking_detail->newtime_from.' - '.$booking_detail->newtime_to;
                        $res['timestamp'] = date('Y-m-d G:i:s');
                        $regId = $deviceid;

                        $fields = array(
                            'to' => $regId,
                            'notification' => $res,
                            'data' => $res,
                        );

                        android_customer_app_push($fields);
                    }
                    $response = array();
                    $response['status']  	          = 'success';
                    $response['message'] 	          = 'Booking transferred successfully';
                    $response['transfered_bookings']  = $transfer_details;
                    $response['transfer_all'] = $transfer_all;
                    // echo json_encode($response);
                            // exit();
                            return $response;
                } else {
                    $response = array();
                    $response['status'] = 'error';
                    $response['error_code'] = '104';
                    $response['message'] = 'Something went wrong. Try again.';

                    // echo json_encode($response);
                            // exit();
                            return $response;
                }
            }
        } else {
        
            $booking_details  	= $this->crew_appapi_lat_model->get_booking_by_id($booking_id);
            $maid_id 	        = $booking_details->maid_id;
            $service_date  		= $datee;
        

            $tablet = $this->crew_appapi_lat_model->get_tablet_by_tabletid($id);
            //$tablet = $this->tablets_model->get_tablet_by_imei($imei);
        
            if (isset($tablet->tablet_id) && $tablet->tablet_status == 1) 
            {
                $zone = $this->crew_appapi_lat_model->get_zone_by_id($zone_id);

                if (isset($zone->zone_id) && $zone->zone_status == 1 && $zone->zone_id != $tablet->zone_id) 
                { 
                    $transferred_bookings = array();$transfer_details = array();

                    if($transfer_all == '1') // transfer all maid bookings in same zone case
                    {
                        /*-----------------------------------All bookings transfer codes starts-----------------------------------*/
                        $booking_transfers = $this->crew_appapi_lat_model->get_booking_transfers_by_date($service_date);
                        $yes_transferred_booking_zones = array();
                        $no_transferred_booking_zones = array();
                        foreach ($booking_transfers as $b_transfer) 
                        {
                            if ($b_transfer->zone_id == $tablet->zone_id) 
                            {
                                $chk_booking = $this->crew_appapi_lat_model->get_booking_by_id($b_transfer->booking_id);

                                if (isset($chk_booking->maid_id) && $chk_booking->maid_id == $maid_id) {
                                    $yes_transferred_booking_zones[$b_transfer->booking_transfer_id] = $b_transfer->booking_id;
                                }
                            } 
                            else 
                            {
                                $no_transferred_booking_zones[$b_transfer->booking_id] = $b_transfer->zone_id;
                            }
                        }

                        $bookings = $this->crew_appapi_lat_model->get_maid_bookings_on_zone_by_date($maid_id, $tablet->zone_id, $service_date);
                        //print_r(count($bookings));
                        
                        foreach ($bookings as $booking) 
                        {
                            if (!isset($no_transferred_booking_zones[$booking->booking_id])) {
                                $b_transfer_fields = array();
                                $b_transfer_fields['booking_id'] = $booking->booking_id;
                                $b_transfer_fields['zone_id'] = $zone->zone_id;
                                $b_transfer_fields['service_date'] = $service_date;
                                $b_transfer_fields['transferred_time'] = date('H:i:s');
                                $b_transfer_fields['transferred_by'] = 'T';
                                $b_transfer_fields['transferred_by_id'] = $tablet->tablet_id;

                                $this->crew_appapi_lat_model->add_booking_transfer($b_transfer_fields);

                                //maid transit
                                $update_driver = array();
                                $update_driver['driver_name'] = $zone->driver_name;
                                $update_driver_details = $this->crew_appapi_lat_model->update_maids($update_driver, $booking->maid_id);
                                //ends
                                
                                $transferred_bookings[] = $booking->booking_id;
                                $transfer_details[]     = $arrayName = array('booking_id' => $booking->booking_id,'customer_id' => $booking->customer_id,'no_of_maids' =>0,'time_from' =>$booking->time_from);
                                $tzone_tablet = $this->crew_appapi_lat_model->get_tablet_by_zone($zone->zone_id);

                                if (isset($tzone_tablet->google_reg_id)) {
                       
                                    $booking_detail = $this->crew_appapi_lat_model->get_booking_by_id($booking->booking_id);
                                    $msg = 'Booking Transfered - ' . $booking_detail->maid_name . ', ' . $booking_detail->newtime_from . ' - ' . $booking_detail->newtime_to . ', ' . $booking_detail->area_name;
                                    $push_fields = array();
                                    $push_fields['tab_id'] = $tzone_tablet->tablet_id;
                                    $push_fields['type'] = 1;
                                    $push_fields['message'] = $msg;
                                    $push_fields['title'] = "Booking Transfered";
                                    $push_fields['customer_name'] = $booking_detail->customer_name;
                                    $push_fields['maid_name'] = $booking_detail->maid_name;
                                    $push_fields['booking_time'] = $booking_detail->newtime_from.' - '.$booking_detail->newtime_to;
                                    $push = $this->crew_appapi_lat_model->add_push_notifications($push_fields);

                                    $title = "Booking Transfered";
                                    $payload = array();
                                    $payload['isfeedback'] = false;
                                    if($push > 0)
                                    {
                                        $payload['pushid'] = $push;
                                    } else {
                                        $payload['pushid'] = 0;
                                    }

                                    $res = array();
                                    $res['title'] = $title;
                                    $res['is_background'] = false;
                                    $res['body'] = $msg;
                                    $res['image'] = "";
                                    $res['payload'] = $payload;
                                    $res['customer'] = $booking_detail->customer_name;
                                    $res['maid'] = $booking_detail->maid_name;
                                    $res['bookingTime'] = $booking_detail->newtime_from.' - '.$booking_detail->newtime_to;
                                    $res['timestamp'] = date('Y-m-d G:i:s');
                                    $regId =$tzone_tablet->google_reg_id;

                                    $fields = array(
                                                    'to' => $regId,
                                                    'notification' => $res,
                                                    'data' => $res,
                                                    );

                                    android_customer_app_push($fields);
                                }
                            }
                        }

                        foreach ($yes_transferred_booking_zones as $booking_transfer_id => $booking_id) 
                        {
                            $b_transfer_fields = array();
                            $b_transfer_fields['booking_id'] = $booking_id;
                            $b_transfer_fields['zone_id'] = $zone->zone_id;
                            $b_transfer_fields['service_date'] = $service_date;
                            $b_transfer_fields['transferred_time'] = date('H:i:s');
                            $b_transfer_fields['transferred_by'] = 'T';
                            $b_transfer_fields['transferred_by_id'] = $tablet->tablet_id;

                            $this->crew_appapi_lat_model->add_booking_transfer($b_transfer_fields);
                            
                            $chk_booking_new = $this->crew_appapi_lat_model->get_booking_by_id($booking_id);
                            //maid transit
                            $update_driver = array();
                            $update_driver['driver_name'] = $zone->driver_name;
                            $update_driver_details = $this->crew_appapi_lat_model->update_maids($update_driver, $chk_booking_new->maid_id);
                            //ends
                            $this->crew_appapi_lat_model->delete_booking_transfer($booking_transfer_id);

                            $transferred_bookings[] = $booking_id;
                            $transfer_details[]     = $arrayName = array('booking_id' => $booking_id,'customer_id' => $chk_booking_new->customer_id,'no_of_maids' =>0,'time_from' =>$chk_booking_new->time_from);
                            $tzone_tablet = $this->crew_appapi_lat_model->get_tablet_by_zone($zone->zone_id);

                            if (isset($tzone_tablet->google_reg_id)) 
                            {
                                $booking_detail = $this->crew_appapi_lat_model->get_booking_by_id($booking_id);
                                $msg = 'Booking Transfered - ' . $booking_detail->maid_name . ', ' . $booking_detail->newtime_from . ' - ' . $booking_detail->newtime_to . ', ' . $booking_detail->area_name;
                                $push_fields = array();
                                $push_fields['tab_id'] = $tzone_tablet->tablet_id;
                                $push_fields['type'] = 1;
                                $push_fields['message'] = $msg;
                                $push_fields['title'] = "Booking Transfered";
                                $push_fields['customer_name'] = $booking_detail->customer_name;
                                $push_fields['maid_name'] = $booking_detail->maid_name;
                                $push_fields['booking_time'] = $booking_detail->newtime_from.' - '.$booking_detail->newtime_to;
                                $push = $this->crew_appapi_lat_model->add_push_notifications($push_fields);
                                
                                $title = "Booking Transfered";
                                $payload = array();
                                $payload['isfeedback'] = false;
                                if($push > 0)
                                {
                                    $payload['pushid'] = $push;
                                } else {
                                    $payload['pushid'] = 0;
                                }

                                $res = array();
                                $res['title'] = $title;
                                $res['is_background'] = false;
                                $res['body'] = $msg;
                                $res['image'] = "";
                                $res['payload'] = $payload;
                                $res['customer'] = $booking_detail->customer_name;
                                $res['maid'] = $booking_detail->maid_name;
                                $res['bookingTime'] = $booking_detail->newtime_from.' - '.$booking_detail->newtime_to;
                                $res['timestamp'] = date('Y-m-d G:i:s');
                                $regId =$tzone_tablet->google_reg_id;

                                $fields = array(
                                                'to' => $regId,
                                                'notification' => $res,
                                                'data' => $res,
                                                );

                                android_customer_app_push($fields);
                            }
                        }

                        /* Check if maid already in */
                        $chk_attandence = $this->crew_appapi_lat_model->get_maid_attandence_by_date($maid_id, $service_date);
                        if (isset($chk_attandence->attandence_id) && $chk_attandence->attandence_status == 1) 
                        {
                            $maid_out_fields = array();
                            $maid_out_fields['maid_out_time'] = date('H:i:s');
                            $maid_out_fields['attandence_status'] = 2;

                            $maid_out = $this->crew_appapi_lat_model->update_maid_attandence($chk_attandence->attandence_id, $maid_out_fields);
                        }
                        /*-----------------------------------All bookings transfer codes ends-----------------------------------*/
                    }
                    else 					// transfer single booking case
                    {	
                        $booking_transfers = $this->crew_appapi_lat_model->get_booking_transfers_by_date_and_booking_id($service_date,$booking_id);

                        
                        $b_transfer_fields = array();
                        $b_transfer_fields['booking_id'] 		= $booking_id;
                        $b_transfer_fields['zone_id'] 			= $zone->zone_id;
                        $b_transfer_fields['service_date'] 		= $service_date;
                        $b_transfer_fields['transferred_time']  = date('H:i:s');
                        $b_transfer_fields['transferred_by'] 	= 'T';
                        $b_transfer_fields['transferred_by_id'] = $tablet->tablet_id;

                        $add_booking=$this->crew_appapi_lat_model->add_booking_transfer($b_transfer_fields);

                        $chk_booking_new = $this->crew_appapi_lat_model->get_booking_by_id($booking_id);

                        $transferred_bookings[]=$add_booking;
                        $transfer_details[]     = $arrayName = array('booking_id' => $booking_id,'customer_id' => $chk_booking_new->customer_id,'no_of_maids' =>0,'time_from' =>$chk_booking_new->time_from);
                        if($add_booking)
                        {
                            foreach ($booking_transfers as $bt) {
                                $this->crew_appapi_lat_model->delete_booking_transfer($bt->booking_transfer_id);
                            }
                        }
                        //maid transit
                        $update_driver = array();
                        $update_driver['driver_name'] = $zone->driver_name;
                        $update_driver_details = $this->crew_appapi_lat_model->update_maids($update_driver, $booking->maid_id);
                        //ends
                        
                        $tzone_tablet = $this->crew_appapi_lat_model->get_tablet_by_zone($zone->zone_id);

                        if (isset($tzone_tablet->google_reg_id)) 
                        {
                            $booking_detail = $this->crew_appapi_lat_model->get_booking_by_id($booking_id);
                            $msg = 'Booking Transfered - ' . $booking_detail->maid_name . ', ' . $booking_detail->newtime_from . ' - ' . $booking_detail->newtime_to . ', ' . $booking_detail->area_name;

                            $push_fields = array();
                            $push_fields['tab_id'] = $tzone_tablet->tablet_id;
                            $push_fields['type'] = 1;
                            $push_fields['message'] = $msg;
                            $push_fields['title'] = "Booking Transfered";
                            $push_fields['customer_name'] = $booking_detail->customer_name;
                            $push_fields['maid_name'] = $booking_detail->maid_name;
                            $push_fields['booking_time'] = $booking_detail->newtime_from.' - '.$booking_detail->newtime_to;
                            $push = $this->crew_appapi_lat_model->add_push_notifications($push_fields);
                            
                            $title = "Booking Transfered";
                            $payload = array();
                            $payload['isfeedback'] = false;
                            if($push > 0)
                            {
                                $payload['pushid'] = $push;
                            } else {
                                $payload['pushid'] = 0;
                            }

                            $res = array();
                            $res['title'] = $title;
                            $res['is_background'] = false;
                            $res['body'] = $msg;
                            $res['image'] = "";
                            $res['payload'] = $payload;
                            $res['customer'] = $booking_detail->customer_name;
                            $res['maid'] = $booking_detail->maid_name;
                            $res['bookingTime'] = $booking_detail->newtime_from.' - '.$booking_detail->newtime_to;
                            $res['timestamp'] = date('Y-m-d G:i:s');
                            $regId =$tzone_tablet->google_reg_id;

                            $fields = array(
                                            'to' => $regId,
                                            'notification' => $res,
                                            'data' => $res,
                                            );

                            android_customer_app_push($fields);
                        }
                        
                    }
                    


                    /*--------------  Section to count number of maids at a given time after transfer process starts  --------------*/
                    if($maid_driver_stat=='0')
                    {
                        $bookings = $this->crew_appapi_lat_model->get_schedule_by_date_for_tab_by_maid_id($service_date,$id);
                        $bookingsarray = (array)$bookings;
                        $bookings_time_from=array_column($bookingsarray, 'time_from');
                    }
                    else
                    {
                        $tablet = $this->crew_appapi_lat_model->get_tablet_by_tabletid($id);
                        $bookings = $this->crew_appapi_lat_model->get_schedule_by_date_for_tab($service_date);

                        $booking_transfers = $this->crew_appapi_lat_model->get_booking_transfers_by_date($service_date);
                        $transferred_booking_zones = array();
                        foreach ($booking_transfers as $b_transfer) 
                        {
                            $transferred_booking_zones[$b_transfer->booking_id] = $b_transfer->zone_id;
                        }


                        foreach ($bookings as $booking) {
                            if (($booking->zone_id == $tablet->zone_id && !isset($transferred_booking_zones[$booking->booking_id])) || (isset($transferred_booking_zones[$booking->booking_id]) && $transferred_booking_zones[$booking->booking_id] == $tablet->zone_id)) {
                                    $bookings_time_from[]=$booking->time_from;
                            }
                        }

                    }

                    foreach ($transfer_details as $key => $dtl) {
                        $booking_id = $dtl['booking_id'];
                        $time_from  = $dtl['time_from'];
                        $transfer_details[$key]['no_of_maids']=count(array_keys($bookings_time_from, $time_from));
                    }
                    /*--------------  Section to count number of maids at a given time after transfer process ends  --------------*/

                    if (count($transferred_bookings) > 0) 
                    {
                        $response = array();
                        $response['status'] 	          = 'success';
                        $response['message'] 	          = 'Booking transferred successfully';
                        $response['transfered_bookings']  = $transfer_details;
                        // $response['maid_id'] 	  = $maid_id;
                        // $response['service_date'] = $service_date;
                        // $response['booking_id']   = $booking_id;
                        $response['transfer_all'] = $transfer_all;
                    } 
                    else 
                    {
                        $response = array();
                        $response['status']  	          = 'success';
                        $response['message'] 	          = 'Booking transferred successfully';
                        $response['transfered_bookings']  = $transfer_details;
                         // $response['maid_id'] 	  = $maid_id;
                        // $response['service_date'] = $service_date;
                        // $response['booking_id']   = $booking_id;
                        $response['transfer_all'] = $transfer_all;
                    }



                    // echo json_encode($response);
                            // exit();
                            return $response;
                } 
                else if (isset($zone->zone_id) && $zone->zone_status == 0 && $zone->zone_id != $tablet->zone_id) 
                {
                    $response = array();
                    $response['status'] = 'error';
                    $response['error_code'] = '104';
                    $response['message'] = 'Zone not active';

                   // echo json_encode($response);
                            // exit();
                            return $response;
                } 
                else 
                {
                    $response = array();
                    $response['status'] = 'error';
                    $response['error_code'] = '105';
                    $response['message'] = 'Invalid zone id';

                    // echo json_encode($response);
                            // exit();
                            return $response;
                }
            }

        }
        
        $response = array();
        $response['status']           = 'success';
        $response['message']          = 'Booking transferred successfully';
        if($transfer_all == '1')
        {
           $response['booking_id']    = ''; 
        }
        else
        {
           $response['booking_id']    = $booking_id; 
        }

        $response['transfer_all']     = $transfer_all; 

        // echo json_encode($response);
                            // exit();
                            return $response;
    }
	
	public function service_start_stop()
	{
        $id               = $this->input->post('id');
        $maid_driver_stat = $this->input->post('maid_driver_stat');
        $token            = $this->input->post('token');
        $booking_id       = $this->input->post('booking_id');
        $service_date     = $this->input->post('date');
        $status           = $this->input->post('status');
        $notes            = $this->input->post('notes');


        if(is_numeric($id) && is_numeric($maid_driver_stat) && strlen($token)>0 && (is_array($booking_id) || is_numeric($booking_id)>0) && strlen($service_date)>0 && is_numeric($status)) 
        {
            $tablet = '';
            if($maid_driver_stat==0)
			{
				$token_check = $this->crew_appapi_lat_model->token_maid_check($id, $token);
				if(isset($token_check->maid_id))
				{
					if(!is_array($booking_id)){
						$bukid = $booking_id;
					} else {
						$bukid = $booking_id[0];
					}
					$booking_details=$this->crew_appapi_lat_model->get_booking_by_id($bukid);
					$zone_id = $booking_details->zone_id;
					$tabletid=$this->crew_appapi_lat_model->get_tablet_by_zone($zone_id);
					$tablet = $this->crew_appapi_lat_model->get_tablet_by_tabletid($tabletid->tablet_id);
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
             }
             else
             {
				$token_tab_check = $this->crew_appapi_lat_model->token_tablet_check($id, $token);
				if(isset($token_tab_check->tablet_id))
				{
					$tablet = $this->crew_appapi_lat_model->get_tablet_by_tabletid($id);
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			}
			if (isset($tablet->tablet_id) && $tablet->tablet_status == 1)
			{
                //$chk_day_service = $this->crew_appapi_lat_model->get_day_service_by_booking_id($service_date, $booking_id);

                switch ($status) {
                    case 1: // service started
                        if(!is_array($booking_id)){
                        echo json_encode($this->multiple_service_start($booking_id,$service_date,$id,$notes));die;
                        }
                        $a=[];
                        // $bb=explode(',',$booking_id);
                        $bb=$booking_id;
                        $sttime=0;
                        $started=1;
                        foreach($bb as $b){
                            $ab=$this->multiple_service_start($b,$service_date,$id,$notes);
                            if($ab['status']=='success' || $ab['error_code']=='106')
                            $sttime=$ab['starttime'];
                            else
                            $started=0;
                            $a[]=$ab;
                        }
                        if(!$started)
                        $sttime=0;
                        echo json_encode(['status' =>$sttime?'success':'error','service_status'=>$sttime?1:0,'starttime'=>$sttime,'message'=>$sttime?'Booking started successfully!':'Booking Already started!','data' =>$a]);die;
                       break;

                    case 2: // service finished    
                    	$amount = $this->input->post('amount');
                        $outstand_amt = $this->input->post('outstanding_amount');
                        $payment_status = $this->input->post('payment');
                         if ($this->input->post('receipt_no') && is_numeric($this->input->post('receipt_no')) && $this->input->post('receipt_no') > 0) {
                            $receipt_no = $this->input->post('receipt_no');
                        } else {
                            $receipt_no = "";
                        }
                        $payment_method = 0;
                        if ($this->input->post('method') && is_numeric($this->input->post('method')) && $this->input->post('method') > 0) {
                            $payment_method = $this->input->post('method');
                        }
                        if ($payment_status == 0 || $payment_status == 1) {
                            $payment_status = $payment_status;
                        } else {
                            $response = array();
                            $response['status'] = 'error';
                            $response['error_code'] = '101';
                            $response['message'] = 'Invalid request. Invalid payment status.';
                            echo json_encode($response);
                            exit();
                        }
                        if(!is_array($booking_id)){
                            echo json_encode($this->multiple_service_stop($booking_id,$service_date,$notes,$tablet,$amount,$outstand_amt,$payment_status,$receipt_no,$payment_method));die;
                        }
                        $a=[];
                        // $bb=explode(',',$booking_id);
                        $bb=$booking_id;
                        $service_status=0;
                        $amtt=0;
                        $outamt=0;
                        $balamt=0;
                        $stopped=1;
                        foreach($bb as $k => $b){
                            $bkng = $this->crew_appapi_lat_model->get_booking_by_id($b);
                            if($amount >= $bkng->total_amount){
                                if($k==count($bb)-1){
                                $amt=$amount;
                                $amount=0;
                            }else{
                                $amount=$amount-$bkng->total_amount;
                                $amt=$bkng->total_amount;
                            }
                        } else {
                            $amt=$amount;
                            $amount=0;
                        }
                        if($amt==0)
                        $payment_status=0;
                        $ab=$this->multiple_service_stop($b,$service_date,$notes,$tablet,$amt,$outstand_amt,$payment_status,$receipt_no,$payment_method);
                        if($ab['status']=='success' || $ab['error_code']=='106'){
                            $sttime=$ab['starttime'];
                            $amtt+=floatval($ab['amount']);
                            $outamt+=$ab['outstanding_amount'];
                            $method=$payment_method;
                            $payment=$payment_status;
                            $balamt+=$ab['balance_amt'];
                            $endtime=$ab['endtime'];
                            $service_status=2;
                        }
                        else
                        $stopped=0;
                        $a[]=$ab;
                    }
                    $res=['status' =>'error','message'=>'Booking already finished!','data' =>$a];
                    if(!$stopped)
                    $service_status=0;
                    $res['service_status']=$service_status;
                        if($service_status){
                        $res['amount']=$amtt;
                        $res['outstanding_amount']=$outamt;
                        $res['method']=$method;
                        $res['payment']=$payment;
                        $res['balance_amt']=$balamt;
                        $res['endtime']=$endtime;
                        $res['status']='success';
                        $res['message']='Booking Finished Successfully';
                        }
						/********************************************************************************************* */
                        // create invoice
                        if (!is_array($booking_id)) {
                            $booking_id1 = $booking_id;
                            $this->db->select('ds.*')
                                ->from('day_services as ds')
                                ->where('ds.booking_id', $booking_id1)
                                ->where('ds.service_date', $service_date)
                                ->limit(1);
                            $day_service = $this->db->get();
                            $day_service = $day_service->row();
                            $customer = $this->crew_appapi_lat_model->get_customer_by_id($day_service->customer_id);
                            if ($customer->payment_type == "D" && $day_service->invoice_status == 0) { // no invoice found
                                // create invoice
                                $chk_booking = $this->crew_appapi_lat_model->get_booking_by_id($booking_id1);
                                create_invoice_on_service_stop($day_service->day_service_id, $chk_booking->total_amount);
                            }
                        } else {
                            foreach ($booking_id as $booking_id1) {
                                $this->db->select('ds.*')
                                    ->from('day_services as ds')
                                    ->where('ds.booking_id', $booking_id1)
                                    ->where('ds.service_date', $service_date)
                                    ->limit(1);
                                $day_service = $this->db->get();
                                $day_service = $day_service->row();
                                $customer = $this->crew_appapi_lat_model->get_customer_by_id($day_service->customer_id);
                                if ($customer->payment_type == "D" && $day_service->invoice_status == 0) { // no invoice found
                                    // create invoice
                                    $chk_booking = $this->crew_appapi_lat_model->get_booking_by_id($booking_id1);
                                    create_invoice_on_service_stop($day_service->day_service_id, $chk_booking->total_amount);
                                }
                            }
                        }
                    /********************************************************************************************* */
                        echo json_encode($res);die;
                        break;

                    case 3: // service not done
                        if(!is_array($booking_id)){
                            echo json_encode($this->multiple_service_cancel($booking_id,$service_date,$notes));die;
                            }
                            $a=[];
                            // $bb=explode(',',$booking_id);
                            $bb=$booking_id;
                            $service_status=0;
                            foreach($bb as $b){
                                $abc=$this->multiple_service_cancel($b,$service_date,$notes);
                                if($abc['status']=='success'){
                                    $service_status=3;
                                }else{
                                    $service_msg=$abc['message'];
                                }
                                $a[]=$abc;
                            }
                            echo json_encode(['status' =>$service_status?'success':'error','service_status' =>$service_status,'message'=>$service_status?'Booking cancelled successfully!':$service_msg,'data' =>$a]);die;
                        break;

                    default:
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '104';
                        $response['message'] = 'Invalid status';
                        break;
                }

                if (isset($response['status']) && $response['status'] = 'success') {
                    if ($this->input('lat') && $this->input('long') && strlen(trim($this->input('lat'))) && strlen(trim($this->input('long')))) {
                        $booking = $this->crew_appapi_lat_model->get_booking_by_id($booking_id);

                        $update_customer_fields = array();
                        $update_customer_fields['latitude'] = trim($this->input('lat'));
                        $update_customer_fields['longitude'] = trim($this->input('long'));

                        $this->crew_appapi_lat_model->update_customers($update_customer_fields,$booking->customer_id);
                    }
                }

                echo json_encode($response);
                exit();
            } else if (isset($tablet->tablet_id) && $tablet->tablet_status == 0) {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '103';
                $response['message'] = 'Inactive tablet';

                echo json_encode($response);
                exit();
            } else {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '102';
                $response['message'] = 'Invalid IMEI';

                echo json_encode($response);
                exit();
            }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid requestd';

            echo json_encode($response);
            exit();
        }
    }
	
	function multiple_service_cancel($booking_id,$service_date,$notes){
        $booking_details=$this->crew_appapi_lat_model->get_booking_by_id($booking_id);
        $zone_id = $booking_details->zone_id;
        $tabletid=$this->crew_appapi_lat_model->get_tablet_by_zone($zone_id);
        $tablet = $this->crew_appapi_lat_model->get_tablet_by_tabletid($tabletid->tablet_id);
        $chk_day_service = $this->crew_appapi_lat_model->get_day_service_by_booking_id($service_date, $booking_id);


        if (isset($chk_day_service->day_service_id)) {
            if ($chk_day_service->service_status == 1) {
                $endingtime = date('H:i:s');
                $ds_fields = array();
                $ds_fields['end_time'] = $endingtime;
                $ds_fields['service_status'] = 3;
                if(strlen($notes)>2)
                {
                    $ds_fields['comments'] = $notes;	
                }
                if(strlen($this->input->post('reason_id'))>0)
                {
                    $ds_fields['reason_id'] = $this->input->post('reason_id');	
                    $ds_fields['cancel_reason'] = $this->input->post('reason_name');	
                }
                $this->crew_appapi_lat_model->update_day_service($chk_day_service->day_service_id, $ds_fields);
                //$this->push_notification(2);
                
                //maid transit
                $update_driver = array();
                $update_driver['driver_name'] = $tablet->driver_name;
                $update_driver_details = $this->crew_appapi_lat_model->update_maids($update_driver, $booking_details->maid_id);
                //ends
                
                $response = array();
                $response['status'] = 'success';
                $response['message'] = 'Booking cancelled successfully!';
                $response['booking_id'] = $booking_id;
                $response['service_status'] = 3;
                $response['payment'] 		= '';
                $response['amount'] 		= 0;
                $response['outstanding_amount'] = 0;
                $response['receipt_no']     = '';
                $response['method']     = '';
                $response['balance_amt'] = 0;
                $response['starttime'] = date('h:i:s a', strtotime($chk_day_service->start_time));
                $response['endtime'] = date('h:i:s a', strtotime($endingtime));
                // echo json_encode($response);
                                // exit();
                                return $response;
            } else {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '106';
                $response['message'] = 'Service already finished';

                // echo json_encode($response);
                                // exit();
                                return $response;
            }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '106';
            $response['message'] = 'Service not started';

            // echo json_encode($response);
                                // exit();
                                return $response;
        }
    }
	
	function multiple_service_stop($booking_id,$service_date,$notes,$tablet,$amount,$outstand_amt,$payment_status,$receipt_no,$payment_method){
        
        $booking_details=$this->crew_appapi_lat_model->get_booking_by_id($booking_id);
        $zone_id = $booking_details->zone_id;
        $tabletid=$this->crew_appapi_lat_model->get_tablet_by_zone($zone_id);
        $tablet = $this->crew_appapi_lat_model->get_tablet_by_tabletid($tabletid->tablet_id);
        $chk_day_service = $this->crew_appapi_lat_model->get_day_service_by_booking_id($service_date, $booking_id);


        $outstanding_amount=$outstand_amt;             
                        // if ($this->input->post('payment') == 0 || $this->input->post('payment') == 1) {
                        //     $payment_status = $this->input->post('payment');
                        // } else {
                        //     $zone_id = trim($this->input->get('zone_id'));
                        //     $response = array();
                        //     $response['status'] = 'error';
                        //     $response['error_code'] = '101';
                        //     $response['message'] = 'Invalid request. Invalid payment status.';

                        //     echo json_encode($response);
                        //     exit();
                        // }

                        if (isset($chk_day_service->day_service_id) && isset($payment_status)) {
                            if ($chk_day_service->service_status == 1) 
                            {
                                if ($payment_status == 1) 
                                {
                                    if ($amount && is_numeric($amount) && $amount > 0 /*|| $this->input->post('amount') == 0*/) 
                                    {
                                        // $amount = $this->input->post('amount');
                                        // if ($this->input->post('outstanding_amount') && is_numeric($this->input->post('outstanding_amount')) && $this->input->post('outstanding_amount') > 0) {
                                        //     $outstanding_amount = $this->input->post('outstanding_amount');
                                        // } else {
                                        //     $outstanding_amount = 0;
                                        // }

                                        // if ($this->input->post('receipt_no') && is_numeric($this->input->post('receipt_no')) && $this->input->post('receipt_no') > 0) {
                                        //     $receipt_no = $this->input->post('receipt_no');
                                        // } else {
                                        //     $receipt_no = "";
                                        // }

                               

                                        // $payment_method = 0;
                                        // if ($this->input->post('method') && is_numeric($this->input->post('method')) && $this->input->post('method') > 0) {
                                        //     $payment_method = $this->input->post('method');
                                        // }

                                        $totlamt = ($amount + $outstanding_amount);
                                        $cus_detail = $this->crew_appapi_lat_model->get_customer_by_id($chk_day_service->customer_id);
                                        $bal = "";
                                        $sign = "";
                                        if ($cus_detail->signed == "Cr") {
                                            $bal = ($cus_detail->balance + $totlamt);
                                            $a = substr($bal, 0, 1);
                                            if ($a == '-') {
                                                $sign = "Dr";
                                                $bal = abs($bal);
                                            } else if ($a == '0') {
                                                $sign = "Dr";
                                            } else {
                                                $sign = "Cr";
                                            }
                                        } else if ($cus_detail->signed == "Dr") {
                                            $bal = ($cus_detail->balance - $totlamt);
                                            $a = substr($bal, 0, 1);
                                            if ($a == '-') {
                                                $sign = "Cr";
                                                $bal = abs($bal);
                                            } else if ($a == '0') {
                                                $sign = "Dr";
                                            } else {
                                                $sign = "Dr";
                                            }
                                        }
                                        $bal_fields = array();
                                        $bal_fields['balance'] = $bal;
                                        $bal_fields['signed'] = $sign;

                                        //$bal_update = $this->customers_model->update_customers($bal_fields, $chk_day_service->customer_id);

                                        //exit();

                                        $payment_fields = array();
                                        $payment_fields['customer_id'] = $chk_day_service->customer_id;
                                        $payment_fields['day_service_id'] = $chk_day_service->day_service_id; //offline
                                        $payment_fields['paid_amount'] = $amount;
                                        $payment_fields['balance_amount'] = $amount;
                                        $payment_fields['outstanding_amt'] = $outstanding_amount;
                                        $payment_fields['receipt_no'] = $receipt_no;
                                        $payment_fields['paid_at'] = 'T';
                                        $payment_fields['paid_at_id'] = $tablet->tablet_id;
                                        $payment_fields['payment_method'] = $payment_method;
                                        $payment_fields['paid_datetime'] = date('Y-m-d H:i:s');

                                        $add_payment = $this->crew_appapi_lat_model->add_customer_payment($payment_fields);

                                        //sms
                                    } 
                                    else 
                                    {
                                        $response = array();
                                        $response['status'] = 'error';
                                        $response['error_code'] = '101';
                                        $response['message'] = 'Invalid request. Invalid amount.';

                                        // echo json_encode($response);
                                        // exit();
                                        return $response;
                                    }
                                } else if ($chk_day_service->payment_type == 'D') {
                                    // Daily paying custmer, not paid. Send notification to manager
                                    //$this->push_notification(3);
                                }

                                if ($payment_status == 0) {
                                    //sms
                                    
                                    //sms ends
                                }
                                
                                //maid transit
                                $update_driver = array();
                                $update_driver['driver_name'] = $tablet->driver_name;
                                $update_driver_details = $this->crew_appapi_lat_model->update_maids($update_driver, $booking_details->maid_id);
                                //ends
                                
                                $endingtime = date('H:i:s');
                                $ds_fields = array();
                                $ds_fields['end_time'] = $endingtime;
                                $ds_fields['service_status'] = 2;
                                $ds_fields['payment_status'] = isset($add_payment) && $add_payment ? 1 : 0;
                                if(strlen($notes)>2)
	                            {
	                            	$ds_fields['comments'] = $notes;	
	                            }

                                $this->crew_appapi_lat_model->update_day_service($chk_day_service->day_service_id, $ds_fields);
								if ($chk_day_service->is_company == 'N' && $chk_day_service->rating_mail == 'Y') {
									//$this->send_service_status_email($chk_day_service->booking_id,$chk_day_service->day_service_id,$service_date);
								}
                                $response = array();
                                $response['status'] 		= 'success';
                                $response['message'] 		= 'Booking finished successfully';
                                $response['booking_id'] 	= $booking_id;
                                $response['service_status'] = 2;
                                $response['payment'] 		= $payment_status;
                                $response['amount'] 		= $amount;
                                $response['outstanding_amount'] = $outstanding_amount;
                                $response['receipt_no']     = $receipt_no;
                                $response['method']         = $payment_method;
                                $response['balance_amt'] = ($chk_day_service->total_fee - $amount);
								$response['starttime'] = date('h:i:s a', strtotime($chk_day_service->start_time));
								$response['endtime'] = date('h:i:s a', strtotime($endingtime));
                                // echo json_encode($response);
                                // exit();
                                return $response;
                            } else {
                                $response = array();
                                $response['status'] = 'error';
                                $response['error_code'] = '106';
                                $response['message'] = 'Service already finished';

                                // echo json_encode($response);
                                // exit();
                                return $response;
                            }
                        } else {
                            $response = array();
                            $response['status'] = 'error';
                            $response['error_code'] = '106';
                            $response['message'] = 'Service not started';

                            // echo json_encode($response);
                            // exit();
                            return $response;
                        }
    }
	
	function multiple_service_start($booking_id,$service_date,$tt,$notes){

        $booking_details=$this->crew_appapi_lat_model->get_booking_by_id($booking_id);
        $zone_id = $booking_details->zone_id;
        $tabletid=$this->crew_appapi_lat_model->get_tablet_by_zone($zone_id);
        $tablet = $this->crew_appapi_lat_model->get_tablet_by_tabletid($tt);
        $chk_day_service = $this->crew_appapi_lat_model->get_day_service_by_booking_id($service_date, $booking_id);
        // echo "<pre>";print_r($tablet);die;

        if (isset($chk_day_service->day_service_id)) {
            if($chk_day_service->service_status == 0)
			{
				$starting_time = date('H:i:s');
				$ds_fields = array();
				$ds_fields['start_time'] = $starting_time;
				$ds_fields['service_status'] = 1;
				$ds_fields['service_added_by'] = 'T';
				$ds_fields['service_added_by_id'] = $tt;
				$ds_fields['service_added_datetime'] = date('Y-m-d H:i:s');

				$day_service_update = $this->crew_appapi_lat_model->update_day_service($chk_day_service->day_service_id, $ds_fields);
				if ($day_service_update) {
					//maid transit
					$update_driver = array();
					$update_driver['driver_name'] = $tablet->driver_name;
					$update_driver_details = $this->crew_appapi_lat_model->update_maids($update_driver, $booking->maid_id);
					//ends
					
					
					$response = array();
					$response['status']     = 'success';
					$response['message']    = 'Booking started successfully!';
					$response['booking_id'] = $booking_id;
					$response['service_status'] = 1;
					$response['payment'] 		= '';
					$response['amount'] 		= 0;
					$response['outstanding_amount'] = 0;
					$response['receipt_no']     = '';
					$response['method']     = '';
					$response['balance_amt'] = 0;
					$response['starttime'] = date('h:i:s a', strtotime($starting_time));
					$response['endtime'] = "";
					// echo json_encode($response);
					// exit();
					return $response;
				} else {
					$response = array();
					$response['status'] = 'error';
					$response['error_code'] = '105';
					$response['message'] = 'Unexpected error';

					// echo json_encode($response);
					// exit();
					return $response;
				}
				
			} else {
				$response = array();
				$response['status'] = 'error';
				$response['error_code'] = '106';
				$response['message'] = $chk_day_service->service_status == 1 ? 'Service already started' : 'Service already finished';
				// echo json_encode($response);
				// exit();
				return $response;
			}
        } else {
            $booking = $this->crew_appapi_lat_model->get_booking_by_id($booking_id);
            if ($booking->cleaning_material == "Y") {
                //$getdefault_fee = $this->bookings_model->get_default_fee();
                //$materialfee = $getdefault_fee->cleaning_material_fee;
                $materialfee =0;
            } else {
                $materialfee = 0;
            }

            
            $sofafee = 0;

            $normal_hours = 0;
            $extra_hours = 0;
            $weekend_hours = 0;

            $normal_from = strtotime('06:00:00');
            $normal_to = strtotime('21:00:00');

            $shift_from = strtotime($booking->time_from . ':00');
            $shift_to = strtotime($booking->time_to . ':00');

            //if(date('w') > 0 && date('w') < 5)
            if (date('w') != 5) {
                if ($shift_from < $normal_from) {
                    if ($shift_to <= $normal_from) {
                        $extra_hours = ($shift_to - $shift_from) / 3600;
                    }

                    if ($shift_to > $normal_from && $shift_to <= $normal_to) {
                        $extra_hours = ($normal_from - $shift_from) / 3600;
                        $normal_hours = ($shift_to - $normal_from) / 3600;
                    }

                    if ($shift_to > $normal_to) {
                        $extra_hours = ($normal_from - $shift_from) / 3600;
                        $extra_hours += ($shift_to - $normal_to) / 3600;
                        $normal_hours = ($normal_to - $normal_from) / 3600;
                    }
                }

                if ($shift_from >= $normal_from && $shift_from < $normal_to) {
                    if ($shift_to <= $normal_to) {
                        $normal_hours = ($shift_to - $shift_from) / 3600;
                    }

                    if ($shift_to > $normal_to) {
                        $normal_hours = ($normal_to - $shift_from) / 3600;
                        $extra_hours = ($shift_to - $normal_to) / 3600;
                    }
                }

                if ($shift_from > $normal_to) {
                    $extra_hours = ($shift_to - $shift_from) / 3600;
                }
            } else {
                $weekend_hours = ($shift_to - $shift_from) / 3600;
            }

            $service_description = array();

            $service_description['normal'] = new stdClass();
            $service_description['normal']->hours = $normal_hours;
            $service_description['normal']->fees = $normal_hours * $booking->price_hourly;

            $service_description['extra'] = new stdClass();
            $service_description['extra']->hours = $extra_hours;
            $service_description['extra']->fees = $extra_hours * $booking->price_extra;

            $service_description['weekend'] = new stdClass();
            $service_description['weekend']->hours = $weekend_hours;
            $service_description['weekend']->fees = $weekend_hours * $booking->price_weekend;

            /*
             * Location charge
             */
            $location_charge = 0;

            $customer_address = $this->crew_appapi_lat_model->get_customer_address_by_id($booking->customer_address_id);
            $cus_detail = $this->crew_appapi_lat_model->get_customer_by_id($booking->customer_id);
            if ($customer_address) {
                $area = $this->crew_appapi_lat_model->get_area_by_id($customer_address->area_id);
                if ($area) {
                    $location_charge = $area->area_charge;
                }
            }
            
            $bal = "";
            $sign = "";
            if ($cus_detail->signed == "Cr") {
                $bal = ($cus_detail->balance - $total_fee);
                $a = substr($bal, 0, 1);
                if ($a == '-') {
                    $sign = "Dr";
                    $bal = abs($bal);
                } else if ($a == '0') {
                    $sign = "Dr";
                } else {
                    $sign = "Cr";
                }
            } else if ($cus_detail->signed == "Dr") {
                $bal = ($cus_detail->balance + $total_fee);
                $a = substr($bal, 0, 1);
                if ($a == '-') {
                    $sign = "Cr";
                    $bal = abs($bal);
                } else if ($a == '0') {
                    $sign = "Dr";
                } else {
                    $sign = "Dr";
                }
            }
            $bal_fields = array();
            $bal_fields['balance'] = $bal;
            $bal_fields['signed'] = $sign;


            //$bal_update = $this->customers_model->update_customers($bal_fields, $booking->customer_id);
            
            if($booking->customer_address == "")
            {
                $booking->customer_address = 'Building - '.$booking->building.', '.$booking->unit_no.''.$booking->street;
            } else {
                if($booking->building != "")
                {
                    $addressss = "Apt No: ".$booking->building.", ".$booking->customer_address;
                } else {
                    $addressss = $booking->customer_address;
                }
                $booking->customer_address = $addressss;
            }
            
            $starting_time = date('H:i:s');
            $ds_fields = array();
            $ds_fields['booking_id'] = $booking_id;
            $ds_fields['customer_id'] = $booking->customer_id;
            $ds_fields['customer_name'] = $booking->customer_name;
            $ds_fields['customer_address'] = htmlspecialchars($booking->customer_address, ENT_QUOTES, 'UTF-8');
            $ds_fields['customer_payment_type'] = $booking->payment_type;
            $ds_fields['maid_id'] = $booking->maid_id;
            $ds_fields['service_description'] = serialize($service_description);
            //$ds_fields['total_fee'] = $total_fee;
            $ds_fields['total_fee'] = $booking->total_amount;
            $ds_fields['service_date'] = $service_date;
            $ds_fields['start_time'] = $starting_time;
            $ds_fields['service_status'] = 1;
            $ds_fields['service_added_by'] = 'T';
            $ds_fields['service_added_by_id'] = $tt;
            if(strlen($notes)>2)
            {
                $ds_fields['comments'] = $notes;	
            }
            

            $day_service = $this->crew_appapi_lat_model->add_day_service($ds_fields);

            if ($day_service) {
                //maid transit
                $update_driver = array();
                $update_driver['driver_name'] = $tablet->driver_name;
                $update_driver_details = $this->crew_appapi_lat_model->update_maids($update_driver, $booking->maid_id);
                //ends
                
                
                $response = array();
                $response['status']     = 'success';
                $response['message']    = 'Booking started successfully!';
                $response['booking_id'] = $booking_id;
                $response['service_status'] = 1;
                $response['payment'] 		= '';
                $response['amount'] 		= 0;
                $response['outstanding_amount'] = 0;
                $response['receipt_no']     = '';
                $response['method']     = '';
                $response['balance_amt'] = 0;
                $response['starttime'] = date('h:i:s a', strtotime($starting_time));
                $response['endtime'] = "";
                // echo json_encode($response);
                // exit();
                return $response;
            } else {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '105';
                $response['message'] = 'Unexpected error';

                // echo json_encode($response);
                // exit();
                return $response;
            }
        }
    }
	
	public function schedule_reports() 
    {
        $id                 = $this->input->post('id');
        $token              = $this->input->post('token');
        $datee              = $this->input->post('date');
        $maid_driver_stat   = $this->input->post('maid_driver_stat');
        $response = array();

        $booking_count=0;$booking_hours=0;$total_billed=0;$total_collected=0;$cashinhand=0;

        if(is_numeric($maid_driver_stat) && strlen($datee)>0 && strlen($id)>0 && strlen($token)>0) 
        {
			if($maid_driver_stat=='0')
			{
				$token_check = $this->crew_appapi_lat_model->token_maid_check($id, $token);
				if(isset($token_check->maid_id))
				{
					
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			} else {
				$token_tab_check = $this->crew_appapi_lat_model->token_tablet_check($id, $token);
				if(isset($token_tab_check->tablet_id))
				{
					
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			}
            if($maid_driver_stat=='0') //maid details
			{
                $bookings = $this->crew_appapi_lat_model->get_schedule_by_date_for_tab_by_maid_id($datee,$id);
                foreach ($bookings as $booking) 
                {
                    $day_service = $this->crew_appapi_lat_model->get_day_service_by_booking_id($datee, $booking->booking_id);
                    if(!empty($day_service))
                    {
                        $paid_amount    = $day_service->paid_amount;
                        if($paid_amount > 0)
                        {
                            $total_collected+=$paid_amount;
                        }
						if($day_service->verified_status == 0)
                        {
							if($paid_amount > 0)
							{
								$cashinhand+=$paid_amount;
							}
                        }
                    }

                    $booking_hours+= (strtotime($booking->time_to) - strtotime($booking->time_from))/3600;
                    $total_billed+=$booking->total_amount;
                    $booking_count++;
                }


                $response = array();
                $response['status']         = 'success';
                $response['message']        = 'Report details retrieved successfully!';
                // $response['report_details'] = array("booking_count" => $booking_count,"booking_hours" => $booking_hours,"total_billed" => $total_billed,"total_collected" => $total_collected,"cash_in_hand" => $cashinhand);
                $response['report_details'] = array(
                    "booking_count" => $booking_count,
                    "booking_hours" => $booking_hours,
                    "total_billed" => (string) round($total_billed,2),
                    "total_collected" => (string) round($total_collected,2),
                    "cash_in_hand" => (string) round($total_collected,2)
                    // "cash_in_hand" => (string) round($cashinhand,2)
                );
                echo json_encode($response);
                exit();

             }
             else                      //driver details
             {
                $tablet = $this->crew_appapi_lat_model->get_tablet_by_tabletid($id);
                if (isset($tablet->tablet_id) && $tablet->tablet_status == 1) 
                {
                    $bookings = $this->crew_appapi_lat_model->get_schedule_by_date_for_tab($datee);

                    //$booking_transfers = $this->crew_appapi_lat_model->get_booking_transfers_by_date($datee);
                    $booking_transfers = $this->crew_appapi_lat_model->get_booking_transfers_by_date_new($datee);
                    $transferred_booking_zones = array();
                    foreach ($booking_transfers as $b_transfer) 
                    {
                        //$transferred_booking_zones[$b_transfer->booking_id] = $b_transfer->zone_id;
						$transferred_booking_zones[$b_transfer->booking_id] = $b_transfer->transfering_to_tablet;
                    }

                    foreach ($bookings as $booking) 
                    {
                        //if (($booking->zone_id == $tablet->zone_id && !isset($transferred_booking_zones[$booking->booking_id])) || (isset($transferred_booking_zones[$booking->booking_id]) && $transferred_booking_zones[$booking->booking_id] == $tablet->zone_id)) 
                        if (($booking->tabletid == $tablet->tablet_id && !isset($transferred_booking_zones[$booking->booking_id])) || (isset($transferred_booking_zones[$booking->booking_id]) && $transferred_booking_zones[$booking->booking_id] == $tablet->tablet_id))
						{
                            $day_service = $this->crew_appapi_lat_model->get_day_service_by_booking_id($datee, $booking->booking_id);
                            if(!empty($day_service))
                            {
                                $paid_amount    = $day_service->paid_amount;
                                if($paid_amount > 0)
                                {
                                    $total_collected+=$paid_amount;
                                }
                            }

                            $booking_hours+= (strtotime($booking->time_to) - strtotime($booking->time_from))/3600;
                            $total_billed+=$booking->total_amount;
                            $booking_count++;
                        }
                    }
                }

                $response = array();
                $response['status']         = 'success';
                $response['message']        = 'Report details retrieved successfully!';
                // $response['report_details'] = array("booking_count" => $booking_count,"booking_hours" => $booking_hours,"total_billed" => $total_billed,"total_collected" => $total_collected,"cash_in_hand" => $total_collected);
                $response['report_details'] = array(
                    "booking_count" => $booking_count,
                    "booking_hours" => $booking_hours,
                    "total_billed" => (string) round($total_billed,2),
                    "total_collected" => (string) round($total_collected,2),
                    "cash_in_hand" => (string) round($total_collected,2)
                    // "cash_in_hand" => (string) round($cashinhand,2)
                );
                echo json_encode($response);
                exit();
             }
        }
        else
        {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '1012';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
	
	public function maid_attendance_list() 
    {
        $id                 = $this->input->post('id');
        $token              = $this->input->post('token');
        $datee              = $this->input->post('date');
        $maid_driver_stat   = $this->input->post('maid_driver_stat');
        $response = array();
        
        if(is_numeric($maid_driver_stat) && strlen($datee)>0 && strlen($id)>0 ) 
        {   
			if($maid_driver_stat=='0')
			{
				$token_check = $this->crew_appapi_lat_model->token_maid_check($id, $token);
				if(isset($token_check->maid_id))
				{
					
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			} else {
				$token_tab_check = $this->crew_appapi_lat_model->token_tablet_check($id, $token);
				if(isset($token_tab_check->tablet_id))
				{
					
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			}
            if (strlen($id)>0 && is_numeric($maid_driver_stat) && strlen($datee)>0 ) 
            {
                $maid_details    = $this->crew_appapi_lat_model->get_all_maids();
                $maiddtls=array();
                foreach ($maid_details as $k => $detail) 
                {
                	$maid_id=$detail['maid_id'];
                	$single_detail 			   		   = array();
		            $single_detail['sl_no']=($k+1);
		            $single_detail['maid_id']  		   = $maid_id;
		            $single_detail['maid_name']  	   = $detail['maid_name'];
		            $single_detail['maid_country']     = $detail['maid_nationality'];

		            $single_detail['maid_attandence']  = 0;

                    $maid_image = $detail['maid_photo_file'];
                    $maid_image           =  (strlen($maid_image)>0)?$maid_image:'';
                    if(strlen($maid_image)>0)
                    {
                        
                        if(file_exists(FCPATH.'maidimg/'.$maid_image))
                        {
                            $maid_image = base_url().'maidimg/'.$maid_image;    
                        }
                        else
                        {
                            $maid_image = '';
                        }
                    }

                    $single_detail['maid_image']  = $maid_image;

		            $maid_attandence 				   = $this->crew_appapi_lat_model->get_maid_attandence_by_date($maid_id, $datee);
                    if (isset($maid_attandence->attandence_id)) 
                    {
                     $single_detail['maid_attandence'] = $maid_attandence->attandence_status;
                    }

		            $maiddtls[]=$single_detail;
		        }
            

	            $response['status']            = 'success';
	            $response['message']           = 'Maid details retrieved successfully';
	            $response['maid_details']      = $maiddtls;

	            echo json_encode($response);
	            exit();
            }
            else
            {
            	$response = array();
	            $response['status'] = 'error';
	            $response['error_code'] = '1012';
	            $response['message'] = 'Invalid request';

	            echo json_encode($response);
	            exit();
            }
            
        }
        else
        {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '1012';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
	
	public function get_notifications()
	{
        $id               = $this->input->post('id');
        $token            = $this->input->post('token');
        $datee            = $this->input->post('date');
        $maid_driver_stat = $this->input->post('maid_driver_stat');

        if ($this->input->post('id') && $this->input->post('token') && $this->input->post('date') && strlen(trim($this->input->post('date'))) > 0 && strlen($this->input->post('maid_driver_stat')) > 0) 
        {
			if($maid_driver_stat=='0')
			{
				$token_check = $this->crew_appapi_lat_model->token_maid_check($id, $token);
				if(isset($token_check->maid_id))
				{
					
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			} else {
				$token_tab_check = $this->crew_appapi_lat_model->token_tablet_check($id, $token);
				if(isset($token_tab_check->tablet_id))
				{
					
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			}
            if($maid_driver_stat=='0') // case of maid
			{
                $today          = $this->crew_appapi_lat_model->get_maid_notifications_by_date($id,date('Y-m-d'));

                $cur_datetime   = date("Y-m-d H:i:s");
                
                $today_schedule = array();
                foreach ($today as $schedule) 
                {
                    $notif_datetime = $schedule->addeddate;
                    $datetime1      = new DateTime($cur_datetime);
                    $datetime2      = new DateTime($notif_datetime);
                    $interval       = $datetime1->diff($datetime2);
                    $hour_diff      = $interval->format("%h");
                    $min_diff       = $interval->format("%i");
                    $sec_diff       = $interval->format("%s");

                    $timediff       = '';
                    if($hour_diff > 0)
                    {
                        if($hour_diff=='1')
                        {
                            $timediff = $hour_diff.' hr';
                        }
                        else
                        {
                            $timediff = $hour_diff.' hrs';
                        }
                    }
                    else
                    {
                        if($min_diff > 0)
                          {
                            $timediff = $min_diff.' mins';
                          }
                        else
                        {
                            $timediff = $sec_diff.' secs';
                        }
                    }
					
                    $single_array            = array();
                    $single_array['pushid'] = $schedule->id;
                    $single_array['title'] = $schedule->title;
                    $single_array['message'] = $schedule->message;
                    $single_array['type']    = $schedule->type;
                    $single_array['customer']    = $schedule->customer_name;
                    $single_array['maid']    = $schedule->maid_name;
                    $single_array['booking_time']    = $schedule->booking_time;
                    $single_array['read_status']    = $schedule->read_status;
                    $single_array['maid_read_status']    = $schedule->maid_read_status;
                    $single_array['date']    = date("Y-m-d",strtotime($notif_datetime));
                    $single_array['time']    = $timediff;
                    $today_schedule[] = $single_array;
                }

                $yesterday          = $this->crew_appapi_lat_model->get_maid_notifications_by_date($id,date('Y-m-d', strtotime('- 1 day')));
                $yesterday_schedule = array();
                foreach ($yesterday as $schedule) 
                {
                    $notif_datetime = $schedule->addeddate;
                    $single_array            = array();
					$single_array['pushid'] = $schedule->id;
					$single_array['title'] = $schedule->title;
                    $single_array['message'] = $schedule->message;
                    $single_array['type']    = $schedule->type;
					$single_array['customer']    = $schedule->customer_name;
                    $single_array['maid']    = $schedule->maid_name;
                    $single_array['booking_time']    = $schedule->booking_time;
					$single_array['read_status']    = $schedule->read_status;
					$single_array['maid_read_status']    = $schedule->maid_read_status;
                    $single_array['date']    = date("Y-m-d",strtotime($notif_datetime));
                    $single_array['time']    = date("g:i a",strtotime($notif_datetime));
                    $yesterday_schedule[] = $single_array;
                }

                $db_yesterday          = $this->crew_appapi_lat_model->get_maid_notifications_by_date($id,date('Y-m-d', strtotime('- 2 day')));
                $db_yesterday_schedule = array();
                foreach ($db_yesterday as $schedule) 
                {
                    $notif_datetime = $schedule->addeddate;
                    $single_array            = array();
					$single_array['pushid'] = $schedule->id;
					$single_array['title'] = $schedule->title;
                    $single_array['message'] = $schedule->message;
                    $single_array['type']    = $schedule->type;
					$single_array['customer']    = $schedule->customer_name;
                    $single_array['maid']    = $schedule->maid_name;
                    $single_array['booking_time']    = $schedule->booking_time;
					$single_array['read_status']    = $schedule->read_status;
					$single_array['maid_read_status']    = $schedule->maid_read_status;
                    $single_array['date']    = date("Y-m-d",strtotime($notif_datetime));
                    $single_array['time']    = date("g:i a",strtotime($notif_datetime));
                    $db_yesterday_schedule[] = $single_array;
                }

                $today_details                      = array();
                //$today_details['Date']              = date("d-m-Y");
                $today_details['Date']              = 'Today';
                $today_details['notification_list'] = $today_schedule;

                $yesterday_details                      = array();
                //$yesterday_details['Date']              = date("d-m-Y", strtotime('- 1 day'));
                $yesterday_details['Date']              = 'Yesterday';
                $yesterday_details['notification_list'] = $yesterday_schedule;

                $db_yesterday_details                      = array();
                //$db_yesterday_details['Date']              = date("d-m-Y", strtotime('- 2 day'));
                $db_yesterday_details['Date']              = date("D d M y", strtotime('- 2 day'));
                $db_yesterday_details['notification_list'] = $db_yesterday_schedule;


                $response = array();
                $response['status']       = 'success';
                $response['message']      = 'Notifications retrieved successfully';
                $response['notification_details']   = array();
                $response['notification_details'][] = $today_details;
                $response['notification_details'][] = $yesterday_details;
                $response['notification_details'][] = $db_yesterday_details;
                //$response['today']        = $today_schedule;
                //$response['yesterday']    = $yesterday_schedule;
                //$response['db_yesterday'] = $db_yesterday_schedule;
                echo json_encode($response);
                exit();


             }
             else   // case of driver
             {
                $today          = $this->crew_appapi_lat_model->get_driver_notifications_by_date($id,date('Y-m-d'));

                $cur_datetime   = date("Y-m-d H:i:s");
                

                $today_schedule = array();
                foreach ($today as $schedule) 
                {
                    
                    $notif_datetime = $schedule->addeddate;
                    $datetime1      = new DateTime($cur_datetime);
                    $datetime2      = new DateTime($notif_datetime);
                    $interval       = $datetime1->diff($datetime2);
                    $hour_diff      = $interval->format("%h");
                    $min_diff       = $interval->format("%i");
                    $sec_diff       = $interval->format("%s");

                    $timediff       = '';
                    if($hour_diff > 0)
                    {
                        if($hour_diff=='1')
                        {
                            $timediff = $hour_diff.' hr';
                        }
                        else
                        {
                            $timediff = $hour_diff.' hrs';
                        }
                        
                    }
                    else
                    {
                        if($min_diff > 0)
                          {
                            $timediff = $min_diff.' mins';
                          }
                        else
                        {
                            $timediff = $sec_diff.' secs';
                        }
                    }

                    $single_array            = array();
					$single_array['pushid'] = $schedule->id;
					$single_array['title'] = $schedule->title;
                    $single_array['message'] = $schedule->message;
                    $single_array['type']    = $schedule->type;
					$single_array['customer']    = $schedule->customer_name;
                    $single_array['maid']    = $schedule->maid_name;
                    $single_array['booking_time']    = $schedule->booking_time;
					$single_array['read_status']    = $schedule->read_status;
					$single_array['maid_read_status']    = $schedule->maid_read_status;
                    $single_array['date']    = date("Y-m-d",strtotime($notif_datetime));
                    $single_array['time']    = $timediff;
                    $today_schedule[] = $single_array;
                }

                $yesterday          = $this->crew_appapi_lat_model->get_driver_notifications_by_date($id,date('Y-m-d', strtotime('- 1 day')));
                $yesterday_schedule = array();
                foreach ($yesterday as $schedule) 
                {
                    $notif_datetime = $schedule->addeddate;
                    $single_array            = array();
					$single_array['pushid'] = $schedule->id;
					$single_array['title'] = $schedule->title;
                    $single_array['message'] = $schedule->message;
                    $single_array['type']    = $schedule->type;
					$single_array['customer']    = $schedule->customer_name;
                    $single_array['maid']    = $schedule->maid_name;
                    $single_array['booking_time']    = $schedule->booking_time;
					$single_array['read_status']    = $schedule->read_status;
					$single_array['maid_read_status']    = $schedule->maid_read_status;
                    $single_array['date']    = date("Y-m-d",strtotime($notif_datetime));
                    $single_array['time']    = date("g:i a",strtotime($notif_datetime));
                    $yesterday_schedule[] = $single_array;
                }

                $db_yesterday          = $this->crew_appapi_lat_model->get_driver_notifications_by_date($id,date('Y-m-d', strtotime('- 2 day')));
                $db_yesterday_schedule = array();
                foreach ($db_yesterday as $schedule) 
                {
                    $notif_datetime = $schedule->addeddate;
                    $single_array            = array();
					$single_array['pushid'] = $schedule->id;
					$single_array['title'] = $schedule->title;
                    $single_array['message'] = $schedule->message;
                    $single_array['type']    = $schedule->type;
					$single_array['customer']    = $schedule->customer_name;
                    $single_array['maid']    = $schedule->maid_name;
                    $single_array['booking_time']    = $schedule->booking_time;
					$single_array['read_status']    = $schedule->read_status;
					$single_array['maid_read_status']    = $schedule->maid_read_status;
                    $single_array['date']    = date("Y-m-d",strtotime($notif_datetime));
                    $single_array['time']    = date("g:i a",strtotime($notif_datetime));
                    $db_yesterday_schedule[] = $single_array;
                }

                $today_details                      = array();
                //$today_details['Date']              = date("d-m-Y");
                $today_details['Date']              = 'Today';
                $today_details['notification_list'] = $today_schedule;

                $yesterday_details                      = array();
                //$yesterday_details['Date']              = date("d-m-Y", strtotime('- 1 day'));
                $yesterday_details['Date']              = 'Yesterday';
                $yesterday_details['notification_list'] = $yesterday_schedule;

                $db_yesterday_details                      = array();
                //$db_yesterday_details['Date']              = date("d-m-Y", strtotime('- 2 day'));
                $db_yesterday_details['Date']              = date("D d M y", strtotime('- 2 day'));
                $db_yesterday_details['notification_list'] = $db_yesterday_schedule;


                $response = array();
                $response['status']       = 'success';
                $response['message']      = 'Notifications retrieved successfully';
                $response['notification_details']   = array();
                $response['notification_details'][] = $today_details;
                $response['notification_details'][] = $yesterday_details;
                $response['notification_details'][] = $db_yesterday_details;
                echo json_encode($response);
                exit();
             }
        }
        else
        {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
	
	public function maid_details() 
    {
        $id                 = $this->input->post('id');
        $token              = $this->input->post('token');
        $datee              = $this->input->post('date');
        $maid_driver_stat   = $this->input->post('maid_driver_stat');
        $maid_id            = $this->input->post('maid_id');
        $booking_id         = $this->input->post('booking_id');
        $response = array();
        
        if(is_numeric($maid_driver_stat) && strlen($datee)>0 && strlen($id)>0 && strlen($maid_id)>0) 
        {
			if($maid_driver_stat=='0')
			{
				$token_check = $this->crew_appapi_lat_model->token_maid_check($id, $token);
				if(isset($token_check->maid_id))
				{
					
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			} else {
				$token_tab_check = $this->crew_appapi_lat_model->token_tablet_check($id, $token);
				if(isset($token_tab_check->tablet_id))
				{
					
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			}
            $maid_details    = $this->crew_appapi_lat_model->get_maid_details($maid_id);

            $maiddtls=array();
            $maiddtls['maid_id']      = $maid_details->maid_id;
            $maiddtls['maid_name']    = $maid_details->maid_name;
            $maid_attandence = $this->crew_appapi_lat_model->get_maid_attandence_by_date($maid_details->maid_id, $datee);
            if (isset($maid_attandence->attandence_id) && ($token_tab_check->tablet_id == $maid_attandence->tablet_id)) 
            {
                $m_attandence = $maid_attandence->attandence_status;
            } 
            else 
            {
                $m_attandence = "0";
            }
            $maiddtls['maid_attendance']  = $m_attandence;
            $maid_image                   = $maid_details->maid_photo_file;
            $maiddtls['maid_image']       = ''; 
            if(strlen($maid_image)>0)
            {
                
                if(file_exists(FCPATH.'maidimg/'.$maid_image))
                {
                    $maiddtls['maid_image']   = base_url().'maidimg/'.$maid_image;    
                }
                else
                {
                    $maid_image = '';
                }

            }
            
            $next_schedules = array();
            $bookings = $this->crew_appapi_lat_model->get_schedule_by_date_for_tab_by_maid_id($datee,$maid_id);
            foreach ($bookings as $booking) {
                if($booking->booking_id == $booking_id){continue;}
                $service_status=0;
                $day_service = $this->crew_appapi_lat_model->get_day_service_by_booking_id($datee, $booking->booking_id);
                if($day_service)
                {
                    $service_status=$day_service->service_status;
                }


                $schedule  =  array();
                $schedule['service_status']     = $service_status;
                $schedule['booking_id']         = $booking->booking_id;
                $schedule['customer_id']        = $booking->customer_id;
                $schedule['customer_name']      = $booking->customer_name;
                $schedule['schedule_date']      = $datee;
                $schedule['shift_start']        = $booking->time_from;
                $schedule['shift_end']          = $booking->time_to;
                $schedule['customer_address']   = strlen($booking->customer_address)>0 ?$booking->customer_address:'';
                $schedule['zone']               = $booking->zone_name;

                $next_schedules[]          = $schedule;
            }
            $maiddtls['next_schedules']      = $next_schedules; 

            $response['status']            = 'success';
            $response['message']           = 'Maid details retrieved successfully';
            $response['maid_details']      = $maiddtls;

            echo json_encode($response);
            exit();
        }
        else
        {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '1012';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
	
	public function update_location()
	{
        set_status_header(404);
	    die('stopped for testing');
	    
        $id               = $this->input->post('id');
        $token            = $this->input->post('token');
        $datee            = $this->input->post('date');
        $maid_driver_stat = $this->input->post('maid_driver_stat');
        $latitude         = $this->input->post('latitude');
        $longitude        = $this->input->post('longitude');

        if ($this->input->post('id') && $this->input->post('token') && $this->input->post('longitude') && $this->input->post('date') && strlen(trim($this->input->post('latitude'))) && strlen(trim($this->input->post('longitude'))) && strlen(trim($this->input->post('date'))) > 0 && strlen($this->input->post('maid_driver_stat')) > 0)
		{
			if($maid_driver_stat=='0')
			{
				$token_check = $this->crew_appapi_lat_model->token_maid_check($id, $token);
				if(isset($token_check->maid_id))
				{
					
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			} else {
				$token_tab_check = $this->crew_appapi_lat_model->token_tablet_check($id, $token);
				if(isset($token_tab_check->tablet_id))
				{
					
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			}
            if($maid_driver_stat=='1') // case of driver
			{
				$tablet = $this->crew_appapi_lat_model->get_tablet_by_tabletid($id);

				if (isset($tablet->tablet_id) && $tablet->tablet_status == 1)
				{
					$response = array();
					$location_fields = array();

					$location_fields['tablet_id'] = $tablet->tablet_id;
					$location_fields['latitude'] = $latitude;
					$location_fields['longitude'] = $longitude;
					$location_fields['speed'] = '0.0';
					$location_fields['added'] = date('Y-m-d H:i:s');
					
					file_put_contents('/var/www/booking/dhk/location.txt', serialize($location_fields).PHP_EOL, FILE_APPEND);

					// $location_id = $this->crew_appapi_lat_model->add_tablet_locations($location_fields);

					// if ($location_id) {
						// $response['status'] = 'success';
						// $response['message'] = 'Tablet location updated successfully!';
						// echo json_encode($response);
						// exit();
					// } else {
						// $response['status'] = 'error';
						// $response['message'] = 'Tablet location updation error!';
						// echo json_encode($response);
						// exit();
					// }
					$response['status'] = 'success';
					$response['message'] = 'Tablet location updated successfully!';
					echo json_encode($response);
					exit();
				} else if (isset($tablet->tablet_id) && $tablet->tablet_status == 0) 
				{
					$response = array();
					$response['status'] = 'error';
					$response['error_code'] = '103';
					$response['message'] = 'Inactive tablet';

					echo json_encode($response);
					exit();
				} else {
					$response = array();
					$response['status'] = 'error';
					$response['error_code'] = '102';
					$response['message'] = 'Invalid IMEI';

					echo json_encode($response);
					exit();
				}
             }
             else           // case of maid
             {
                $maid_details= $this->crew_appapi_lat_model->get_maid_by_id($id);

                if($maid_details)
                {
                    // $maid_fields = array();
                    // $maid_fields['maid_latitude']  = $latitude;
                    // $maid_fields['maid_longitude'] = $longitude;
                    // $maid_update = $this->crew_appapi_lat_model->update_maids($maid_fields,$id);

                    $response = array();
                    $response['status'] = 'success';
                    $response['message'] = 'Maid location updated successfully!';
                    echo json_encode($response);
                    exit();

                }
                else
                {
                    $response = array();
                    $response['status'] = 'error';
                    $response['error_code'] = '103';
                    $response['message'] = 'Invalid Maid ID!';

                    echo json_encode($response);
                    exit();
                }
                
             }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
	
	public function update_location_new()
	{
		error_reporting(E_ALL);
		ini_set('display_errors', '1');
        // $id               = $this->input->post('id');
        // $token            = $this->input->post('token');
        // $datee            = $this->input->post('date');
        // $maid_driver_stat = $this->input->post('maid_driver_stat');
        // $latitude         = $this->input->post('latitude');
        // $longitude        = $this->input->post('longitude');
		
		$id               = 36;
        $token            = "8f83b1eb6dc3b7bf8bcd9d1fae9e463d06706e15";
        $datee            = "2022-09-12";
        $maid_driver_stat = '1';
        $latitude         = "11.347441669999995";
        $longitude        = "75.76930998999997";

        //if ($this->input->post('id') && $this->input->post('token') && $this->input->post('longitude') && $this->input->post('date') && strlen(trim($this->input->post('latitude'))) && strlen(trim($this->input->post('longitude'))) && strlen(trim($this->input->post('date'))) > 0 && strlen($this->input->post('maid_driver_stat')) > 0)
		if ($id && $token && $longitude && $datee && strlen(trim($latitude)) && strlen(trim($longitude)) && strlen(trim($datee)) > 0 && strlen($maid_driver_stat) > 0)
		{
			if($maid_driver_stat=='0')
			{
				$token_check = $this->crew_appapi_lat_model->token_maid_check($id, $token);
				if(isset($token_check->maid_id))
				{
					
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			} else {
				$token_tab_check = $this->crew_appapi_lat_model->token_tablet_check($id, $token);
				if(isset($token_tab_check->tablet_id))
				{
					
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			}
            if($maid_driver_stat=='1') // case of driver
			{
				$tablet = $this->crew_appapi_lat_model->get_tablet_by_tabletid($id);

				if (isset($tablet->tablet_id) && $tablet->tablet_status == 1)
				{
					$response = array();
					$location_fields = array();

					$location_fields['tablet_id'] = $tablet->tablet_id;
					$location_fields['latitude'] = $latitude;
					$location_fields['longitude'] = $longitude;
					$location_fields['speed'] = '0.0';
					$location_fields['added'] = date('Y-m-d H:i:s');
					
					file_put_contents('/var/www/booking/dhk/location.txt', serialize($location_fields).PHP_EOL, FILE_APPEND);
					
					// $handle = fopen("/var/www/html/elitemaids/location.txt", "r");
					// if ($handle) {
						// while (($line = fgets($handle)) !== false) {
							// echo $line;
							// echo '<br>';
						// }

						// fclose($handle);
					// }
					
					// $location_id = $this->crew_appapi_lat_model->add_tablet_locations($location_fields);

					// if ($location_id) {
						// $response['status'] = 'success';
						// $response['message'] = 'Tablet location updated successfully!';
						// echo json_encode($response);
						// exit();
					// } else {
						// $response['status'] = 'error';
						// $response['message'] = 'Tablet location updation error!';
						// echo json_encode($response);
						// exit();
					// }
					$response['status'] = 'success';
					$response['message'] = 'Tablet location updated successfully!';
					echo json_encode($response);
					exit();
				} else if (isset($tablet->tablet_id) && $tablet->tablet_status == 0) 
				{
					$response = array();
					$response['status'] = 'error';
					$response['error_code'] = '103';
					$response['message'] = 'Inactive tablet';

					echo json_encode($response);
					exit();
				} else {
					$response = array();
					$response['status'] = 'error';
					$response['error_code'] = '102';
					$response['message'] = 'Invalid IMEI';

					echo json_encode($response);
					exit();
				}
             }
             else           // case of maid
             {
                $maid_details= $this->crew_appapi_lat_model->get_maid_by_id($id);

                if($maid_details)
                {
                    // $maid_fields = array();
                    // $maid_fields['maid_latitude']  = $latitude;
                    // $maid_fields['maid_longitude'] = $longitude;
                    // $maid_update = $this->crew_appapi_lat_model->update_maids($maid_fields,$id);

                    $response = array();
                    $response['status'] = 'success';
                    $response['message'] = 'Maid location updated successfully!';
                    echo json_encode($response);
                    exit();

                }
                else
                {
                    $response = array();
                    $response['status'] = 'error';
                    $response['error_code'] = '103';
                    $response['message'] = 'Invalid Maid ID!';

                    echo json_encode($response);
                    exit();
                }
                
             }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }

	public function logout()
    {
		$id=$this->input->post('id');
        $maid_driver_stat=$this->input->post('maid_driver_stat');
        $token=$this->input->post('token');

        if (strlen($id)>0 && is_numeric($maid_driver_stat) ) 
        {
			if($maid_driver_stat=='0')
			{
				// $token_check = $this->crew_appapi_lat_model->token_maid_check($id, $token);
				// if(isset($token_check->maid_id))
				// {	
					
				// } else {
					// echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				// }
				$update_array = array();
				$update_array['maid_device_token'] = "";
				$maid_update = $this->crew_appapi_lat_model->update_maids($update_array,$id);
				
				$response = array();
				$response['status'] = 'success';
				$response['message'] = 'Logged out successfully!';
				echo json_encode($response);
				exit();
			} else {
				// $token_tab_check = $this->crew_appapi_lat_model->token_tablet_check($id, $token);
				// if(isset($token_tab_check->tablet_id))
				// {
					
				// } else {
					// echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				// }
				$update_array = array();
				$update_array['google_reg_id'] = "";
				$tablet_update = $this->crew_appapi_lat_model->update_tablet($id,$update_array);
				
				$response = array();
				$response['status'] = 'success';
				$response['message'] = 'Logged out successfully!';
				echo json_encode($response);
				exit();
			}
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
	
	public function send_service_status_email($booking_id,$day_service_id,$servicedate)
	{
        /*********************************************************
         * 
         * For demo url
         * 
         */
        if (strpos($_SERVER['REQUEST_URI'], "homesquad-demo") !== false) {
            return true;
        }
        /*********************************************************/
		$this->load->library('email');
        $data['booking'] =$this->crew_appapi_lat_model->get_booking_by_id($booking_id);
		$data['service_id'] =$day_service_id;
		$data['booking_id'] =$booking_id;
		$data['servicedate'] =$servicedate;
        $html = $this->load->view('service_template_email_new', $data, True);
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://mail.dubaihousekeeping.com',
            'smtp_user' => 'hhomesquad@gmail.com',
            'smtp_pass' => 'yjevhrmogynkkudy',
            'smtp_port' => 465,
            'mailtype'  => 'html',
            'crlf' => "\r\n",
            'newline' => "\r\n"
        );
		
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");
		$this->email->from('hhomesquad@gmail.com', 'HomeSquad');
		$this->email->to($data['booking']->email_address);
		$this->email->subject('HomeSquad Service Notification');
		$this->email->message($html);
		$this->email->send();
	}
	
	function push_notification($msgtype = 1) {

        //$deviceToken = 'd5df0f248a2bf7bd8a2b9b2f646dfbc9a573c7922259ae9fd5ae6dc7aab8634d';

        $device_tokens = $this->crew_appapi_lat_model->get_device_tokens();
        if (!empty($device_tokens)) {
            foreach ($device_tokens as $token) {
                //$deviceToken = 'd5df0f248a2bf7bd8a2b9b2f646dfbc9a573c7922259ae9fd5ae6dc7aab8634d';
                $deviceToken = $token->device_token;
                //$output = file_get_contents('http://shanu.info/mm/push.php?device_token='.$deviceToken.'&msgtype='.$msgtype);
                $output = file_get_contents('http://demo.azinova.info/php/push/push.php?device_token=' . $deviceToken . '&msgtype=' . $msgtype);
            }
        }


    }
    function company(){
        $id=$this->input->post('company_code');
        if(!$id){
            echo json_encode(['status'=>'error','message'=>'Parameter missing..']);
            exit;  
        }else{
            echo json_encode(['status'=>'success','message'=>'Url Fetched Successfully','url'=>'https://booking.emaid.info/ktt/crewappapi/','is_testing'=>'yes']);
            exit; 
        }
    }
	
	public function create_invoice()
	{
		$id               = $this->input->post('id');
		// $id               = 1;
        $token            = $this->input->post('token');
        // $token            = 'eb04081231badff17b0e68fdd55690fa79cc2d2b';
        $datee            = $this->input->post('service_date');
        // $datee            = "2021-09-22";
        $maid_driver_stat = $this->input->post('maid_driver_stat');
        // $maid_driver_stat = '1';
        $booking_id = $this->input->post('booking_id');
        // $booking_id = 249;
		
		if (strlen($id)>0 && is_numeric($maid_driver_stat) && ($booking_id > 0) ) 
        {
			if($maid_driver_stat=='0')
			{
				$token_check = $this->crew_appapi_lat_model->token_maid_check($id, $token);
				if(isset($token_check->maid_id))
				{	
					
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			} else {
				$token_tab_check = $this->crew_appapi_lat_model->token_tablet_check($id, $token);
				if(isset($token_tab_check->tablet_id))
				{
					
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			}
			$check_service_exist = $this->crew_appapi_lat_model->get_day_service_by_booking_id($datee,$booking_id);
			if(!empty($check_service_exist))
			{
				if($check_service_exist->service_status == 1)
				{
					$response = array();
					$response['status'] = 'error';
					$response['error_code'] = '101';
					$response['message'] = 'Service Not Finished.';

					echo json_encode($response);
					exit();
				} else if($check_service_exist->service_status == 3)
				{
					$response = array();
					$response['status'] = 'error';
					$response['error_code'] = '101';
					$response['message'] = 'Service Cancelled.';

					echo json_encode($response);
					exit();
				} else {
					if($check_service_exist->invoice_status == 1)
					{
						$invoice_detail = $this->crew_appapi_lat_model->get_invoice_detailbyid($check_service_exist->serv_invoice_id);
						// echo '<pre>';
						// print_r($invoice_detail);
						// exit();
						
						$main_inv_array = array();
						$main_line_array = array();
						$main_inv_array['invoice_id'] = $invoice_detail[0]->invoice_id;
						$main_inv_array['customer_id'] = $invoice_detail[0]->custid;
						$main_inv_array['customer_name'] = $invoice_detail[0]->customer_name;
						$main_inv_array['customer_address'] = $invoice_detail[0]->bill_address;
						$main_inv_array['inv_ref'] = $invoice_detail[0]->invoice_num;
						$main_inv_array['issued_date'] = date('d F - Y',strtotime($invoice_detail[0]->invoice_date));
						$main_inv_array['due_date'] = date('d F - Y',strtotime($invoice_detail[0]->invoice_due_date));
						$main_inv_array['cust_trn'] = $invoice_detail[0]->customer_trn;
						$main_inv_array['total_amount'] = number_format($invoice_detail[0]->invoice_total_amount,2);
						$main_inv_array['vat_amount'] = number_format($invoice_detail[0]->invoice_tax_amount,2);
						$main_inv_array['vat_percent'] = 5;
						$main_inv_array['total_net_amount'] = number_format($invoice_detail[0]->invoice_net_amount,2);
						$main_inv_array['issued_date'] = date('d F - Y',strtotime($invoice_detail[0]->invoice_date));
						$main_inv_array['signature_status'] = $invoice_detail[0]->signature_status;
						$main_inv_array['pdf_url'] = "";
						foreach($invoice_detail as $jobs)
						{
							$tot_hrs = ((strtotime($jobs->service_to_time) - strtotime($jobs->service_from_time))/ 3600);
							$linearray = array();
							$linearray['service_date'] = date('M d,Y', strtotime($jobs->service_date));
							$linearray['service_name'] = $jobs->service_type_name;
							$linearray['description'] = $jobs->description;
							$linearray['hrs'] = $tot_hrs;
							$linearray['maid_id'] = $jobs->maid_id;
							$linearray['maid_name'] = $jobs->maid_name;
							$linearray['from_time'] = date('h:i A',strtotime($jobs->service_from_time));
							$linearray['to_time'] = date('h:i A',strtotime($jobs->service_to_time));
							$linearray['line_amount'] = number_format($jobs->line_amount,2);
							array_push($main_line_array,$linearray);
						}
						$main_inv_array['line_items'] = $main_line_array;
						echo json_encode(['status'=>'success','message'=>'Invoice fetched Successfully','details'=>$main_inv_array]);
						exit;
					} else {
						$service_id = $check_service_exist->day_service_id;
						$service_details = $this->crew_appapi_lat_model->getservicedetails($service_id);
						$dayservice_id_array = array();
						$invoice_array = array();
						if(!empty($service_details))
						{
							$ds_id = $service_details->day_service_id;
							if(!in_array($ds_id, $dayservice_id_array))
							{
								$total_fee = $service_details->total_fee;
								if($service_details->serviceamount > 0)
								{
									$serviceamount = $service_details->serviceamount;
									$vat_fee = $service_details->vatamount;
									if(!is_numeric($vat_fee)){$vat_fee=0.05*$serviceamount;$vat_fee=sprintf("%0.2f",$vat_fee);}
								} else {
									$serviceamount = ($total_fee / 1.05);
									$vat_fee = ($total_fee - $serviceamount);
								}
								
								$total_time = ((strtotime($service_details->end_time) - strtotime($service_details->start_time))/ 3600);
								$unit_price = $service_details->price_hourly;
								
								$invoice_array['customer_id'] = $service_details->customer_id;
								$invoice_array['customer_name'] = $service_details->customer_name;
								$invoice_array['bill_address'] = $service_details->customer_address;
								$invoice_array['service_date'] = $service_details->service_date;
								$invoice_array['billed_amount'] = $total_fee;
								
								$linearray = array();
								$linearray[0]['day_service_id'] = $service_details->day_service_id;
								$linearray[0]['booking_id'] = $service_details->booking_id;
								$linearray[0]['day_service_reference'] = $service_details->day_service_reference;
								$linearray[0]['maid_id'] = $service_details->maid_id;
								$linearray[0]['maid_name'] = $service_details->maid_name;
								$linearray[0]['service_from_time'] = $service_details->start_time;
								$linearray[0]['service_to_time'] = $service_details->end_time;
								$linearray[0]['line_bill_address'] = $service_details->customer_address;
								$linearray[0]['description'] = "Service from ".$service_details->time_from." to ".$service_details->time_to." For ".$service_details->customer_address;
								$linearray[0]['service_hrs'] = $total_time;
								$linearray[0]['inv_unit_price'] = (float)$unit_price;
								$linearray[0]['line_discount'] = 0;
								$linearray[0]['line_amount'] = (float)$serviceamount;
								$linearray[0]['line_vat_amount'] = (float)$vat_fee;
								$linearray[0]['line_net_amount'] = (float)$total_fee;
								
								array_push($dayservice_id_array,$ds_id);
								
								$related_bookings= $this->crew_appapi_lat_model->get_related_bookings_for_invoice($service_details->customer_id,$service_details->service_date,$ds_id);
								
								$i=1;
								foreach ($related_bookings as $relbuks)
								{
									$ds_id=$relbuks->day_service_id;
									$total_time = ((strtotime($relbuks->end_time) - strtotime($relbuks->start_time))/ 3600);
									$total_fee_l = $relbuks->total_fee;
									if($relbuks->serviceamount > 0)
									{
										$serviceamount_l = $relbuks->serviceamount;
										$vat_fee_l = $relbuks->vatamount;
										if(!is_numeric($vat_fee_l)){$vat_fee_l=0.05*$serviceamount_l;$vat_fee_l=sprintf("%0.2f",$vat_fee_l);}
									} else {
										$serviceamount_l = ($total_fee_l / 1.05);
										$vat_fee_l = ($total_fee_l - $serviceamount_l);
									}
									
									//$serviceamount_l = $relbuks->serviceamount;
									//$vat_fee_l = $relbuks->vatamount;
									//if(!is_numeric($vat_fee_l)){$vat_fee_l=0.05*$serviceamount_l;$vat_fee_l=sprintf("%0.2f",$vat_fee_l);}
									$unit_price = $service_details->price_hourly;
										
									$linearray[$i]['day_service_id'] = $ds_id;
									$linearray[$i]['booking_id'] = $relbuks->booking_id;
									$linearray[$i]['day_service_reference'] = $relbuks->day_service_reference;
									$linearray[$i]['maid_id'] = $relbuks->maid_id;
									$linearray[$i]['maid_name'] = $relbuks->maid_name;
									$linearray[$i]['service_from_time'] = $relbuks->start_time;
									$linearray[$i]['service_to_time'] = $relbuks->end_time;
									$linearray[$i]['line_bill_address'] = $relbuks->customer_address;
									$linearray[$i]['description'] = "Service from ".$relbuks->time_from." to ".$relbuks->time_to." For ".$relbuks->customer_address;
									$linearray[$i]['service_hrs'] = $total_time;
									$linearray[$i]['inv_unit_price'] = (float)$unit_price;
									$linearray[$i]['line_discount'] = 0;
									$linearray[$i]['line_amount'] = (float)$serviceamount_l; 
									$linearray[$i]['line_vat_amount'] = (float)$vat_fee_l;
									$linearray[$i]['line_net_amount'] = (float)$total_fee_l;
									   
									array_push($dayservice_id_array,$ds_id);
									$i++;
								}
								
								$invoice_array['line_item'] = $linearray;
								$added_date_time = date('Y-m-d H:i:s');
								
								// echo '<pre>';
								// print_r($invoice_array);
								// exit();
								
								$main_inv_array = array();
								$main_inv_array['customer_id'] = $service_details->customer_id;
								$main_inv_array['customer_name'] = $service_details->customer_name;
								$main_inv_array['bill_address'] = $service_details->customer_address;
								$main_inv_array['added'] = $added_date_time;
								$main_inv_array['service_date'] = $service_details->service_date;
								$main_inv_array['invoice_date'] = $service_details->service_date;
								$main_inv_array['invoice_due_date'] = $service_details->service_date;
								//$main_inv_array['billed_amount'] = $inv_val['billed_amount'];
								$main_inv_array['invoice_added_by'] = $id;
									
								$invoice_id = $this->crew_appapi_lat_model->add_main_invoice($main_inv_array);
								if($invoice_id > 0)
								{
									$tot_bill_amount = 0;
									$total_vat_amt = 0;
									$total_net_amt = 0;
									$dayids = array();
									foreach($linearray as $line_val)
									{
										$tot_bill_amount += $line_val['line_amount'];
										$total_vat_amt += $line_val['line_vat_amount'];
										$total_net_amt += $line_val['line_net_amount'];
										$main_inv_line = array();
										$main_inv_line['invoice_id'] = $invoice_id;
										$main_inv_line['day_service_id'] = $line_val['day_service_id'];
										$main_inv_line['day_service_reference'] = $line_val['day_service_reference'];
										$main_inv_line['maid_id'] = $line_val['maid_id'];
										$main_inv_line['maid_name'] = $line_val['maid_name'];
										$main_inv_line['service_from_time'] = $line_val['service_from_time'];
										$main_inv_line['service_to_time'] = $line_val['service_to_time'];
										$main_inv_line['line_bill_address'] = $line_val['line_bill_address'];
										$main_inv_line['description'] = $line_val['description'];
										$main_inv_line['service_hrs'] = $line_val['service_hrs'];
										$main_inv_line['inv_unit_price'] = $line_val['inv_unit_price'];
										$main_inv_line['line_discount'] = $line_val['line_discount'];
										$main_inv_line['line_amount'] = $line_val['line_amount'];
										$main_inv_line['line_vat_amount'] = $line_val['line_vat_amount'];
										$main_inv_line['line_net_amount'] = $line_val['line_net_amount'];
										
										$invoice_line_id = $this->crew_appapi_lat_model->add_line_invoice($main_inv_line);
										
										if($invoice_line_id > 0)
										{
											$ds_fields = array();
											$ds_fields['serv_invoice_id'] = $invoice_id;
											$ds_fields['invoice_status'] = 1;
											
											$this->crew_appapi_lat_model->update_day_service($line_val['day_service_id'], $ds_fields);
											array_push($dayids,$line_val['booking_id']);
										}
										
									}
									
									$update_main_inv = array();
									$update_main_inv['invoice_num'] = "INV-".date('Y')."-".sprintf('%04s', $invoice_id);
									//$update_main_inv['billed_amount'] = $tot_bill_amount;
									$update_main_inv['invoice_total_amount'] = $tot_bill_amount;
									$update_main_inv['invoice_tax_amount'] = $total_vat_amt;
									$update_main_inv['invoice_net_amount'] = $total_net_amt;
									$update_main_inv['balance_amount'] = $total_net_amt;
									
									$updateinv = $this->crew_appapi_lat_model->update_service_invoice($invoice_id, $update_main_inv);
									if($updateinv)
									{
										$invoice_detail = $this->crew_appapi_lat_model->get_invoice_detailbyid($invoice_id);
										// echo '<pre>';
										// print_r($invoice_detail);
										// exit();
										
										$main_inv_array = array();
										$main_line_array = array();
										$main_inv_array['invoice_id'] = $invoice_detail[0]->invoice_id;
										$main_inv_array['customer_id'] = $invoice_detail[0]->custid;
										$main_inv_array['customer_name'] = $invoice_detail[0]->customer_name;
										$main_inv_array['customer_address'] = $invoice_detail[0]->bill_address;
										$main_inv_array['inv_ref'] = $invoice_detail[0]->invoice_num;
										$main_inv_array['issued_date'] = date('d F - Y',strtotime($invoice_detail[0]->invoice_date));
										$main_inv_array['due_date'] = date('d F - Y',strtotime($invoice_detail[0]->invoice_due_date));
										$main_inv_array['cust_trn'] = $invoice_detail[0]->customer_trn;
										$main_inv_array['total_amount'] = number_format($invoice_detail[0]->invoice_total_amount,2);
										$main_inv_array['vat_amount'] = number_format($invoice_detail[0]->invoice_tax_amount,2);
										$main_inv_array['vat_percent'] = 5;
										$main_inv_array['total_net_amount'] = number_format($invoice_detail[0]->invoice_net_amount,2);
										$main_inv_array['issued_date'] = date('d F - Y',strtotime($invoice_detail[0]->invoice_date));
										$main_inv_array['signature_status'] = $invoice_detail[0]->signature_status;
										$main_inv_array['pdf_url'] = "";
										foreach($invoice_detail as $jobs)
										{
											$tot_hrs = ((strtotime($jobs->service_to_time) - strtotime($jobs->service_from_time))/ 3600);
											$linearray = array();
											$linearray['service_date'] = date('M d,Y', strtotime($jobs->service_date));
											$linearray['service_name'] = $jobs->service_type_name;
											$linearray['description'] = $jobs->description;
											$linearray['hrs'] = $tot_hrs;
											$linearray['maid_id'] = $jobs->maid_id;
											$linearray['maid_name'] = $jobs->maid_name;
											$linearray['from_time'] = date('h:i A',strtotime($jobs->service_from_time));
											$linearray['to_time'] = date('h:i A',strtotime($jobs->service_to_time));
											$linearray['line_amount'] = number_format($jobs->line_amount,2);
											array_push($main_line_array,$linearray);
										}
										$main_inv_array['line_items'] = $main_line_array;
										echo json_encode(['status'=>'success','message'=>'Invoice fetched Successfully','details'=>$main_inv_array]);
										exit;
									} else {
										$response = array();
										$response['status'] = 'error';
										$response['error_code'] = '101';
										$response['message'] = 'Error try again.';

										echo json_encode($response);
										exit();
									}
								} else {
									$response = array();
									$response['status'] = 'error';
									$response['error_code'] = '101';
									$response['message'] = 'Error try again.';

									echo json_encode($response);
									exit();
								}
							}
						}
					}
				}
			} else {
				$response = array();
				$response['status'] = 'error';
				$response['error_code'] = '101';
				$response['message'] = 'Service Not Started.';

				echo json_encode($response);
				exit();
			}
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
	}
	
	public function invoice_pdf_generate()
	{
		$id               = $this->input->post('id');
        $token            = $this->input->post('token');
        $invoice_id       = $this->input->post('invoice_id');
        $maid_driver_stat = $this->input->post('maid_driver_stat');
		
		if (strlen($id)>0 && is_numeric($maid_driver_stat) ) 
        {
			if($maid_driver_stat=='0')
			{
				$token_check = $this->crew_appapi_lat_model->token_maid_check($id, $token);
				if(isset($token_check->maid_id))
				{
					
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			} else {
				$token_tab_check = $this->crew_appapi_lat_model->token_tablet_check($id, $token);
				if(isset($token_tab_check->tablet_id))
				{
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			}
			$invoice_detail = $this->crew_appapi_lat_model->get_invoice_detailbyid($invoice_id);
			if(!empty($invoice_detail))
			{
				if($invoice_detail[0]->pdf_status == 0)
				{
					$this->load->library('pdf');
					$data = array();
					$data['invoice_detail'] = $invoice_detail;
					$html_content = $this->load->view('invoicepdf', $data, TRUE);
					$this->pdf->loadHtml($html_content);
					$this->pdf->set_paper("a4", "portrait");
					$this->pdf->render();
					$pdfname = 'inv-'.rand(100, 999).'-'.$invoice_id;
					file_put_contents('/var/www/booking/dhk/invoices/'.$pdfname.'.pdf', $this->pdf->output());
					$update_main_inv = array();
					$update_main_inv['pdf_status'] = 1;
					$update_main_inv['pdf_link'] = $pdfname.'.pdf';
					$updateinv = $this->crew_appapi_lat_model->update_service_invoice($invoice_id, $update_main_inv);
					
					if($updateinv)
					{
						$response = array();
						$response['status'] = 'success';
						$response['pdfurl'] = base_url().'invoices/'.$pdfname.'.pdf';

						echo json_encode($response);
						exit();
					} else {
						$response = array();
						$response['status'] = 'error';
						$response['error_code'] = '101';
						$response['message'] = 'Error try again..';

						echo json_encode($response);
						exit();
					}
				} else {
					$response = array();
					$response['status'] = 'success';
					$response['pdfurl'] = base_url().'invoices/'.$invoice_detail[0]->pdf_link;

					echo json_encode($response);
					exit();
				}
			}
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
	}
    function invoice_signature_update(){
        $id               = $this->input->post('id');
        $token            = $this->input->post('token');
        $invoice_id       = $this->input->post('invoice_id');
        $maid_driver_stat = $this->input->post('maid_driver_stat');
		
		if (strlen($id)>0 && is_numeric($maid_driver_stat) ) 
        {
			if($maid_driver_stat=='0')
			{
				$token_check = $this->crew_appapi_lat_model->token_maid_check($id, $token);
				if(isset($token_check->maid_id))
				{
					
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			} else {
				$token_tab_check = $this->crew_appapi_lat_model->token_tablet_check($id, $token);
				if(isset($token_tab_check->tablet_id))
				{
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			}
			$invoice_detail = $this->crew_appapi_lat_model->get_invoice_detailbyid($invoice_id);
			if(!empty($invoice_detail))
			{
                $config['file_name']          = 'invoive-'.$invoice_detail[0]->invoice_id.'-'.$invoice_detail[0]->customer_id;
                $config['upload_path']          = './invoice_customer_signature/';
                $config['allowed_types']        = 'gif|jpg|png';
                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload('signature')){
                    echo json_encode(array('status' => 'error','error_code'=>'101','message'=>'Signature Upload Failed.')); exit();
                }
                else {
                    $updata['signature_status']=1;
                    $updata['signature_image']=$this->upload->data()['file_name'];
                    if($this->db->update('invoice',$updata,['invoice_id'=>$invoice_detail[0]->invoice_id])){
						if($invoice_detail[0]->pdf_status == 1)
						{
							$invoice_detail_new = $this->crew_appapi_lat_model->get_invoice_detailbyid($invoice_id);
							unlink('/var/www/booking/dhk/invoices/'.$invoice_detail_new[0]->pdf_link);
							$this->load->library('pdf');
							$data = array();
							$data['invoice_detail'] = $invoice_detail_new;
							$html_content = $this->load->view('invoicepdf', $data, TRUE);
							$this->pdf->loadHtml($html_content);
							$this->pdf->set_paper("a4", "portrait");
							$this->pdf->render();
							$pdfname = 'inv-'.rand(100, 999).'-'.$invoice_id;
							file_put_contents('/var/www/booking/dhk/invoices/'.$pdfname.'.pdf', $this->pdf->output());
							$updatanew['pdf_status'] = 1;
							$updatanew['pdf_link'] = $pdfname.'.pdf';
							$this->db->update('invoice',$updatanew,['invoice_id'=>$invoice_detail_new[0]->invoice_id]);
						}
                        echo json_encode(array('status' => 'success','message'=>'Signature Uploaded Successfully.')); exit();
                    }
                    else{
                        echo json_encode(array('status' => 'error','error_code'=>'101','message'=>'Signature Update Failed.')); exit();
                    }

                }
			}
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
	
	public function list_cancel_reasons()
	{
		$id               = $this->input->post('id');
        $token            = $this->input->post('token');
        $maid_driver_stat = $this->input->post('maid_driver_stat');
		if (strlen($id)>0 && is_numeric($maid_driver_stat) ) 
        {
			if($maid_driver_stat=='0')
			{
				$token_check = $this->crew_appapi_lat_model->token_maid_check($id, $token);
				if(isset($token_check->maid_id))
				{
					
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			} else {
				$token_tab_check = $this->crew_appapi_lat_model->token_tablet_check($id, $token);
				if(isset($token_tab_check->tablet_id))
				{
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			}
			$get_reasons = $this->crew_appapi_lat_model->get_cancel_reasons();
			echo json_encode(array('status' => 'success','message'=>'Reasons fetched successfully.','reasons'=>$get_reasons)); exit();
		} else {
			$response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
		}
	}
	
	public function update_notification_status()
	{
		$id               = $this->input->post('id');
        $token            = $this->input->post('token');
        $maid_driver_stat = $this->input->post('maid_driver_stat');
		$status = $this->input->post('read_status');
		$pushid = $this->input->post('pushid');
		if (strlen($id)>0 && is_numeric($maid_driver_stat) ) 
        {
			if($maid_driver_stat=='0')
			{
				$token_check = $this->crew_appapi_lat_model->token_maid_check($id, $token);
				if(isset($token_check->maid_id))
				{
					
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			} else {
				$token_tab_check = $this->crew_appapi_lat_model->token_tablet_check($id, $token);
				if(isset($token_tab_check->tablet_id))
				{
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			}
			if($maid_driver_stat=='0')
			{
				$updatearray = array();
				$updatearray['maid_read_status'] = $status;
			} else {
				$updatearray = array();
				$updatearray['read_status'] = $status;
			}
			
			$update_status = $this->crew_appapi_lat_model->update_notifications_status($pushid,$updatearray);
			if($update_status)
			{
				echo json_encode(array('status' => 'success','message'=>'Notification updated successfully.')); exit();
			} else {
				echo json_encode(array('status' => 'error','message'=>'Error. Try again.')); exit();
			}
		} else {
			$response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
		}
	}

	
	public function sent_invoice_email()
	{
        /****************************************************************** */
        /**
         * 
         * For demo url
         * 
         */
        if (strpos($_SERVER['REQUEST_URI'], "homesquad-demo") !== false) {
            die(json_encode(array('status' => 'success', 'message' => 'Invoice sent successfully.')));
        }
        /****************************************************************** */
		$id               = $this->input->post('id');
        $token            = $this->input->post('token');
        $maid_driver_stat = $this->input->post('maid_driver_stat');
		$invoice_id = $this->input->post('invoice_id');
		if (strlen($id)>0 && is_numeric($maid_driver_stat) ) 
        {
			if($maid_driver_stat=='0')
			{
				$token_check = $this->crew_appapi_lat_model->token_maid_check($id, $token);
				if(isset($token_check->maid_id))
				{
					
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token does not exist.')); exit();
				}
			} else {
				$token_tab_check = $this->crew_appapi_lat_model->token_tablet_check($id, $token);
				if(isset($token_tab_check->tablet_id))
				{
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token does not exist.')); exit();
				}
			}
			
			$invoice_detail = $this->crew_appapi_lat_model->get_invoice_detailbyid($invoice_id);
			$customer_email=$invoice_detail[0]->email_address;
			$customer_name=$invoice_detail[0]->customer_name;
			$invoice_num=$invoice_detail[0]->invoice_num;
			$service_name=$invoice_detail[0]->service_type_name;
			$service_date=date('d/m/Y',strtotime($invoice_detail[0]->service_date));
			if($customer_email != "")
			{
				$invoicecontent = 'Dear '.$customer_name.',<br/>Thank you for your recent business with us. Please find attached a detailed copy of the invoice '.$invoice_num.' for '.$service_name.' done on '.$service_date.'.<br/><br/>If you have any questions or concerns please contact us on office@dubaihousekeeping.com or +971 54 350 2510';
				$this->load->library('email');
				$config = array(
                    'protocol'  => 'smtp',
					'smtp_host' => 'ssl://smtp.googlemail.com',
					'smtp_port' => 465,
					'smtp_user' => 'hhomesquad@gmail.com',
					'smtp_pass' => 'yjevhrmogynkkudy',
					'mailtype'  => 'html',
					'charset'   => 'utf-8'
              );
				$this->email->initialize($config);
				$this->email->set_mailtype("html");
				$this->email->set_newline("\r\n");
				$this->email->from('hhomesquad@gmail.com', 'HomeSquad');
				$this->email->to($customer_email);
				$this->email->subject('HomeSquad Service Invoice Email');
				$this->email->message($invoicecontent);
				$this->email->attach('/var/www/booking/homesquad-demo/invoices/'.$invoice_detail[0]->pdf_link);
				// echo json_encode(array('status' => 'success','message'=>'Invoice sent successfully.'));
					// exit();
				if($this->email->send())
				{
					echo json_encode(array('status' => 'success','message'=>'Invoice sent successfully.'));
					exit();
				}
			} else {
				echo json_encode(array('status' => 'error','message'=>'Error. Try again.'));
				exit();
			}
		} else {
			$response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
		}
	}
    function testpush(){
        $res = array();
        $res['data']['title'] = 'test';
        $res['data']['is_background'] = false;
        $res['data']['message'] = 'test message';
        $res['data']['image'] = "";
        $res['data']['payload'] = json_decode('{payload={"pushid":248,"isfeedback":false}');
        $res['data']['customer'] = 'testname';
        $res['data']['maid'] = 'testmaid';
        $res['data']['bookingTime'] = '10:00 am - 11:00am';
        $res['data']['timestamp'] = date('Y-m-d G:i:s');
        $res=json_decode('{payload={"pushid":248,"isfeedback":false}, body=New Booking. Maid : Test Maid, Customer : Anju Azinova, Shift : 02:00 PM-03:00 PM, maid=Test Maid, timestamp=2021-10-11 10:33:12, image=, title=New Booking, bookingTime=02:00 PM - 03:00 PM, customer=Anju Azinova, is_background=false}');
        $regId = 'efSWWEGuTUeRfZaxMP0jgT:APA91bGshai5f7mWBnaEy-Z5z9_sGTthcOmbcj0HQ82GeCXp7fNZJK9RSpqN2H-KrN8LottNK6cFnvtuZaqVQjcBsDA27GspCcHG2-8ogi025-sP3X8ZTxWYwUxKgKRtQWTyuIIEX_Qc';
        $fields = array(
            'to' => $regId,
            'data' => $res,
        );
        // echo "<pre>";echo json_encode($fields);die;
        $return = android_customer_push($fields);
        echo "<pre>";print_r($return);die;
    }
    public function add_remove_key()
	{
		$id               = $this->input->post('id');
        $token            = $this->input->post('token');
        $maid_driver_stat = $this->input->post('maid_driver_stat');
		$c_id = $this->input->post('customer_id');
		$add_remove = $this->input->post('add_remove');
		if (strlen($id)>0 && is_numeric($maid_driver_stat) ) 
        {
			if($maid_driver_stat=='0')
			{
				$token_check = $this->crew_appapi_lat_model->token_maid_check($id, $token);
				if(isset($token_check->maid_id))
				{
					
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			} else {
				$token_tab_check = $this->crew_appapi_lat_model->token_tablet_check($id, $token);
				if(isset($token_tab_check->tablet_id))
				{
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			}
			
			// contents starts here

           echo $this->crew_appapi_lat_model->add_remove_key($c_id,$add_remove);die;

			// contents ends here
		} else {
			$response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
		}
	}
	
	public function delete_account()
    {
		// error_reporting(E_ALL);
		// ini_set('display_errors', '1');
		$id=$this->input->post('id');
        $maid_driver_stat=$this->input->post('maid_driver_stat');
        $token=$this->input->post('token');
		
		// $id=36;
        // $maid_driver_stat=1;
        // $token="94c21ab1c47a433a5f5a1c5308c877a249a7397a";

        if (strlen($id)>0 && is_numeric($maid_driver_stat) ) 
        {
			if($maid_driver_stat=='0')
			{
				$token_check = $this->crew_appapi_lat_model->token_maid_check($id, $token);
				if(isset($token_check->maid_id))
				{	
					$update_array = array();
					$update_array['maid_status'] = 0;
					$maid_update = $this->crew_appapi_lat_model->update_maids($update_array,$id);
					
					$response = array();
                    $response['status'] = 'success';
                    $response['message'] = 'Deleted successfully!';
                    echo json_encode($response);
                    exit();
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			} else {
				$token_tab_check = $this->crew_appapi_lat_model->token_tablet_check($id, $token);
				if(isset($token_tab_check->tablet_id))
				{
					$update_array = array();
					$update_array['tablet_status'] = 0;
					$tablet_update = $this->crew_appapi_lat_model->update_tablet($id,$update_array);
					
					$response = array();
                    $response['status'] = 'success';
                    $response['message'] = 'Deleted successfully!';
                    echo json_encode($response);
                    exit();
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			}
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
	
	public function payment_modes()
	{
		$id               = $this->input->post('id');
        $token            = $this->input->post('token');
        $maid_driver_stat = $this->input->post('maid_driver_stat');
		if (strlen($id)>0 && is_numeric($maid_driver_stat) ) 
        {
			if($maid_driver_stat=='0')
			{
				$token_check = $this->crew_appapi_lat_model->token_maid_check($id, $token);
				if(isset($token_check->maid_id))
				{
					
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			} else {
				$token_tab_check = $this->crew_appapi_lat_model->token_tablet_check($id, $token);
				if(isset($token_tab_check->tablet_id))
				{
				} else {
					echo json_encode(array('status' => 'token_error','error_code'=>'103','message'=>'Token doesnot exist.')); exit();
				}
			}
			
			$payarray = array();
			$payarray[0]['key'] = 'Cash';
			$payarray[0]['value'] = '0';
			$payarray[0]['icon'] = base_url().'upload/paymodes/cash.png';
			$payarray[0]['icon-active'] = base_url().'upload/paymodes/cash-active.png';
			
			// $payarray[1]['key'] = 'Card';
			// $payarray[1]['value'] = '1';
			// $payarray[1]['icon'] = base_url().'upload/paymodes/card.png';
			// $payarray[1]['icon-active'] = base_url().'upload/paymodes/card-active.png';
			
			// $payarray[2]['key'] = 'Cheque';
			// $payarray[2]['value'] = '2';
			// $payarray[2]['icon'] = base_url().'upload/paymodes/cheque.png';
			// $payarray[2]['icon-active'] = base_url().'upload/paymodes/cheque-active.png';
			
			// $payarray[3]['key'] = 'Online';
			// $payarray[3]['value'] = '3';
			// $payarray[3]['icon'] = base_url().'upload/paymodes/online.png';
			// $payarray[3]['icon-active'] = base_url().'upload/paymodes/online-active.png';
			
			echo json_encode(array('status' => 'success','message'=>'Details fetched successfully.','data'=>$payarray)); exit();
		} else {
			$response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
		}
	}
}