<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Reports extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        $this->load->model('settings_model');
        $this->load->model('bookings_model');
        $this->load->model('reports_model');
        $this->load->model('maids_model');
        $this->load->model('tablets_model');
        $this->load->model('zones_model');
        $this->load->model('users_model');
        $this->load->model('customers_model');
        ini_set("memory_limit", "256M");
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    function index()
    {
        if (!user_permission(user_authenticate(), 13)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $data['zone_report'] = array();
        $data['search'] = array();
        $date = "";
        $zone = "";
        $zone_name = "All";
        $day = strftime("%A", strtotime(date('Y-m-d')));
        $search = array('search_date' => $date, 'search_zone' => $zone, 'search_zone_name' => $zone_name, 'search_day' => $day);
        $data['search'] = $search;
        if ($this->input->post('zone_report')) {
            $date = $this->input->post('zone_date');
            if ($date != "") {
                list($day, $month, $year) = explode("/", $date);
                $zone_date = "$year-$month-$day";
                $day = strftime("%A", strtotime($zone_date));
            } else {
                $zone_date = "";
                $day = "";
            }
            $zone = $this->input->post('zones');
            if ($zone == "") {
                $zone_name = "All";
            } else {
                $zone_name = $this->reports_model->get_zone_name($zone);
            }
            $zone_result = $this->reports_model->get_zone_reports($zone_date, $zone);
            //echo "<pre>";
            //print_r($zone_result);
            $zone_report = array();
            foreach ($zone_result as $zone_res) {
                $firstTime = $zone_res->time_from;
                $lastTime = $zone_res->time_to;
                $firstTime = strtotime($firstTime);
                $lastTime = strtotime($lastTime);
                $timeDiff = $lastTime - $firstTime;
                $timedif = ($timeDiff / 60) / 60;
                //echo $timedif; 
                $zone_report['booking_id'] = $zone_res->booking_id;
                $zone_report['maid_id'] = $zone_res->maid_id;
                $zone_report['maid_name'] = $zone_res->maid_name;
                $zone_report['customer'] = $zone_res->customer_name;
                $zone_report['customer_paytype'] = $zone_res->payment_type;
                $zone_report['price'] = $zone_res->price_hourly;
                $zone_report['zone'] = $zone_res->zone_name;
                $zone_report['area'] = $zone_res->area_name;
                $zone_report['booking_type'] = $zone_res->booking_type;
                $zone_report['time_from'] = $zone_res->time_from;
                $zone_report['time_to'] = $zone_res->time_to;
                $zone_report['time_diff'] = $timedif;
                $data['zone_report'][] = $zone_report;
            }
            //echo "<pre>";
            //print_r($data['zone_report']);
            $search = array('search_date' => $date, 'search_zone' => $zone, 'search_zone_name' => $zone_name, 'search_day' => $day);
            $data['search'] = $search;
            $data['maids'] = $this->reports_model->get_maids($zone_date, $zone);
        } else {
            $date = date('d/m/Y');
            if ($date != "") {
                list($day, $month, $year) = explode("/", $date);
                $zone_date = "$year-$month-$day";
                $day = strftime("%A", strtotime($zone_date));
            } else {
                $zone_date = "";
                $day = "";
            }
            $zone = $this->input->post('zones');
            if ($zone == "") {
                $zone_name = "All";
            } else {
                $zone_name = $this->reports_model->get_zone_name($zone);
            }
            $zone_result = $this->reports_model->get_zone_reports($zone_date, $zone);
            $zone_report = array();
            foreach ($zone_result as $zone_res) {
                $firstTime = $zone_res->time_from;
                $lastTime = $zone_res->time_to;
                $firstTime = strtotime($firstTime);
                $lastTime = strtotime($lastTime);
                $timeDiff = $lastTime - $firstTime;
                $timedif = ($timeDiff / 60) / 60;
                //echo $timedif; 
                $zone_report['booking_id'] = $zone_res->booking_id;
                $zone_report['maid_id'] = $zone_res->maid_id;
                $zone_report['customer'] = $zone_res->customer_name;
                $zone_report['customer_paytype'] = $zone_res->payment_type;
                $zone_report['price'] = $zone_res->price_hourly;
                $zone_report['zone'] = $zone_res->zone_name;
                $zone_report['area'] = $zone_res->area_name;
                $zone_report['booking_type'] = $zone_res->booking_type;
                $zone_report['time_from'] = $zone_res->time_from;
                $zone_report['time_to'] = $zone_res->time_to;
                $zone_report['time_diff'] = $timedif;
                $data['zone_report'][] = $zone_report;
            }
            $search = array('search_date' => $date, 'search_zone' => $zone, 'search_zone_name' => $zone_name, 'search_day' => $day);
            $data['search'] = $search;
            $data['maids'] = $this->reports_model->get_maids($zone_date, $zone);
        }
        //echo '<pre>';print_r($data);exit;
        //$data['maids'] = $this->reports_model->get_all_maids();
        $data['zones'] = $this->settings_model->get_zones();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('zone_reports', $data, TRUE);
        $layout_data['page_title'] = 'Zone Reports';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1';
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    public function newreport()
    {
        if ($this->input->post('cstatemnet-date-from') != "") {
            $d_from = $this->input->post('cstatemnet-date-from');
            $s_date = explode("/", $d_from);
            $d_from_date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
            $data['search_date_from_statement'] = $this->input->post('cstatemnet-date-from');
            $data['date_from_job'] = $d_from_date;
        } else {
            $date_from_job = date('Y-m-d');
            $data['search_date_from_statement'] = date('d/m/Y', strtotime($date_from_job));
            $data['date_from_job'] = $date_from_job;
        }
        $service_date = $data['date_from_job'];
        if ($this->input->post('zones') != "") {
            $zone_id = $this->input->post('zones');
        } else {
            $zone_id = "";
        }
        $morning_avail = '';
        $eve_avail = '';
        $tot_avail = '';
        $leave_maid = '';
        $inactive_maid = '';
        if ($this->input->post('freemaids') && strlen(trim($this->input->post('freemaids') > 0))) {
            if ($this->input->post('freemaids') == 1) {
                $freemaid_id =     $this->input->post('freemaids');
            } else if ($this->input->post('freemaids') == 2) {
                $morning_avail = $this->input->post('freemaids');
            } else if ($this->input->post('freemaids') == 3) {
                $eve_avail   = $this->input->post('freemaids');
            } else if ($this->input->post('freemaids') == 4) {
                $tot_avail = $this->input->post('freemaids');
            } else if ($this->input->post('freemaids') == 5) {
                $leave_maid = $this->input->post('freemaids');
            } else if ($this->input->post('freemaids') == 6) {
                $inactive_maid = $this->input->post('freemaids');
            }
        } else {
            $freemaid_id = '';
            $morning_avail = '';
            $eve_avail = '';
            $tot_avail = '';
            $leave_maid = '';
            $inactive_maid = '';
        }
        //starts
        $get_leave_maids = $this->maids_model->get_maids_leave_by_date($service_date);
        $leave_maid_ids_new = array();
        foreach ($get_leave_maids as $leave) {
            array_push($leave_maid_ids_new, $leave->maid_id);
        }
        if ($freemaid_id == 1) {
            //$maidss = $this->maids_model->get_maids($team_id);
            $maidss = $this->maids_model->get_maids();
            $maids = array();
            foreach ($maidss as $maid) {
                if (!in_array($maid->maid_id, $leave_maid_ids_new)) {
                    $shifts = $this->bookings_model->get_schedule_by_date_new_count($service_date, $maid->maid_id);
                    if ($shifts == 0) {
                        //$newdata[] = new stdClass();
                        $newdata['maid_id'] = $maid->maid_id;
                        $newdata['odoo_maid_id'] = $maid->odoo_maid_id;
                        $newdata['maid_name'] = $maid->maid_name;
                        $newdata['maid_nationality'] = $maid->maid_nationality;
                        $newdata['maid_mobile_1'] = $maid->maid_mobile_1;
                        $newdata['maid_photo_file'] = $maid->maid_photo_file;
                        $newdata['flat_name'] = $maid->flat_name;
                        $newdata['odoo_flat_id'] = $maid->odoo_flat_id;
                        $newdata['maid_status'] = $maid->maid_status;
                        $newdata['odoo_synch_status'] = $maid->odoo_synch_status;
                        //$newdata['team_name'] = $maid->team_name;
                        array_push($maids, (object) $newdata);
                    }
                }
            }
            $view_id = $freemaid_id;
        } else if ($morning_avail == 2) {
            $view_id = $morning_avail;
            $maidss = $this->maids_model->get_maids();
            $maids  = array();
            $filter_by = 2;
            foreach ($maidss as $maid) {
                if (!in_array($maid->maid_id, $leave_maid_ids_new)) {
                    $get_avilable_before_noon = $this->bookings_model->get_avilable_before_noon($service_date, $maid->maid_id);
                    $count = count($get_avilable_before_noon);
                    $hour_array = array();
                    for ($i = 0; $i < $count; $i++) {
                        if ($i == 0) {
                            $gap = (strtotime($get_avilable_before_noon[$i]->time_from) - strtotime('08:00:00')) / 3600;
                            array_push($hour_array, $gap);
                            if ($count == 1) {
                                $gap2 = (strtotime('13:00:00') - strtotime($get_avilable_before_noon[$i]->time_to)) / 3600;
                                array_push($hour_array, $gap2);
                            }
                        } else if ($i == ($count - 1)) {
                            $gap = (strtotime($get_avilable_before_noon[$i]->time_from) - strtotime($get_avilable_before_noon[$i - 1]->time_to)) / 3600;
                            array_push($hour_array, $gap);
                            $gap2 = (strtotime('13:00:00') - strtotime($get_avilable_before_noon[$i]->time_to)) / 3600;
                            array_push($hour_array, $gap2);
                        } else {
                            $gap = (strtotime($get_avilable_before_noon[$i]->time_from) - strtotime($get_avilable_before_noon[$i - 1]->time_to)) / 3600;
                            array_push($hour_array, $gap);
                        }
                    }
                    foreach ($hour_array as $list) {
                        if ($list >= 3) {
                            $newdata['maid_id'] = $maid->maid_id;
                            $newdata['odoo_maid_id'] = $maid->odoo_maid_id;
                            $newdata['maid_name'] = $maid->maid_name;
                            $newdata['maid_nationality'] = $maid->maid_nationality;
                            $newdata['maid_mobile_1'] = $maid->maid_mobile_1;
                            $newdata['maid_photo_file'] = $maid->maid_photo_file;
                            $newdata['flat_name'] = $maid->flat_name;
                            $newdata['odoo_flat_id'] = $maid->odoo_flat_id;
                            $newdata['maid_status'] = $maid->maid_status;
                            $newdata['odoo_synch_status'] = $maid->odoo_synch_status;
                            $newdata['total_hrs'] = $total_hrs;
                            array_push($maids, (object) $newdata);
                        }
                    }
                    if ($count == 0) {
                        $newdata['maid_id'] = $maid->maid_id;
                        $newdata['odoo_maid_id'] = $maid->odoo_maid_id;
                        $newdata['maid_name'] = $maid->maid_name;
                        $newdata['maid_nationality'] = $maid->maid_nationality;
                        $newdata['maid_mobile_1'] = $maid->maid_mobile_1;
                        $newdata['maid_photo_file'] = $maid->maid_photo_file;
                        $newdata['flat_name'] = $maid->flat_name;
                        $newdata['odoo_flat_id'] = $maid->odoo_flat_id;
                        $newdata['maid_status'] = $maid->maid_status;
                        $newdata['odoo_synch_status'] = $maid->odoo_synch_status;
                        $newdata['total_hrs'] = $total_hrs;
                        array_push($maids, (object) $newdata);
                    }
                }
            }
            //print_r(json_encode($maids));
        } else if ($eve_avail == 3) {
            $view_id = $eve_avail;
            $maidss = $this->maids_model->get_maids();
            $maids  = array();
            $filter_by = 3;
            foreach ($maidss as $maid) {
                if (!in_array($maid->maid_id, $leave_maid_ids_new)) {
                    $get_avilable_after_noon = $this->bookings_model->get_avilable_after_noon($service_date, $maid->maid_id);
                    $count = count($get_avilable_after_noon);
                    $hour_array = array();
                    for ($i = 0; $i < $count; $i++) {
                        if ($i == 0) {
                            $gap = (strtotime($get_avilable_after_noon[$i]->time_from) - strtotime('13:00:00')) / 3600;
                            array_push($hour_array, $gap);
                            if ($count == 1) {
                                $gap2 = (strtotime('19:00:00') - strtotime($get_avilable_after_noon[$i]->time_to)) / 3600;
                                array_push($hour_array, $gap2);
                            }
                        } else if ($i == ($count - 1)) {
                            $gap = (strtotime($get_avilable_after_noon[$i]->time_from) - strtotime($get_avilable_after_noon[$i - 1]->time_to)) / 3600;
                            array_push($hour_array, $gap);
                            $gap2 = (strtotime('19:00:00') - strtotime($get_avilable_after_noon[$i]->time_to)) / 3600;
                            array_push($hour_array, $gap2);
                        } else {
                            $gap = (strtotime($get_avilable_after_noon[$i]->time_from) - strtotime($get_avilable_after_noon[$i - 1]->time_to)) / 3600;
                            array_push($hour_array, $gap);
                        }
                    }
                    foreach ($hour_array as $list) {
                        if ($list >= 3) {
                            $newdata['maid_id'] = $maid->maid_id;
                            $newdata['odoo_maid_id'] = $maid->odoo_maid_id;
                            $newdata['maid_name'] = $maid->maid_name;
                            $newdata['maid_nationality'] = $maid->maid_nationality;
                            $newdata['maid_mobile_1'] = $maid->maid_mobile_1;
                            $newdata['maid_photo_file'] = $maid->maid_photo_file;
                            $newdata['flat_name'] = $maid->flat_name;
                            $newdata['odoo_flat_id'] = $maid->odoo_flat_id;
                            $newdata['maid_status'] = $maid->maid_status;
                            $newdata['odoo_synch_status'] = $maid->odoo_synch_status;
                            $newdata['total_hrs'] = $total_hrs;
                            array_push($maids, (object) $newdata);
                        }
                    }
                    if ($count == 0) {
                        $newdata['maid_id'] = $maid->maid_id;
                        $newdata['odoo_maid_id'] = $maid->odoo_maid_id;
                        $newdata['maid_name'] = $maid->maid_name;
                        $newdata['maid_nationality'] = $maid->maid_nationality;
                        $newdata['maid_mobile_1'] = $maid->maid_mobile_1;
                        $newdata['maid_photo_file'] = $maid->maid_photo_file;
                        $newdata['flat_name'] = $maid->flat_name;
                        $newdata['odoo_flat_id'] = $maid->odoo_flat_id;
                        $newdata['maid_status'] = $maid->maid_status;
                        $newdata['odoo_synch_status'] = $maid->odoo_synch_status;
                        $newdata['total_hrs'] = $total_hrs;
                        array_push($maids, (object) $newdata);
                    }
                }
            }
        } else if ($tot_avail == 4) {
            $view_id = $tot_avail;
            $maidss = $this->maids_model->get_maids();
            $maids  = array();
            $filter_by = 4;
            foreach ($maidss as $maid) {
                if (!in_array($maid->maid_id, $leave_maid_ids_new)) {
                    //$newdata[] = new stdClass();
                    $newdata['maid_id'] = $maid->maid_id;
                    $newdata['odoo_maid_id'] = $maid->odoo_maid_id;
                    $newdata['maid_name'] = $maid->maid_name;
                    $newdata['maid_nationality'] = $maid->maid_nationality;
                    $newdata['maid_mobile_1'] = $maid->maid_mobile_1;
                    $newdata['maid_photo_file'] = $maid->maid_photo_file;
                    $newdata['flat_name'] = $maid->flat_name;
                    $newdata['odoo_flat_id'] = $maid->odoo_flat_id;
                    $newdata['maid_status'] = $maid->maid_status;
                    $newdata['odoo_synch_status'] = $maid->odoo_synch_status;
                    //$newdata['team_name'] = $maid->team_name;
                    array_push($maids, (object) $newdata);
                    //$maids = ;
                }
            }
        } else if ($leave_maid == 5) {
            $view_id = $leave_maid;
            $list_leave_maidss = $this->maids_model->list_maids_leave_by_date($service_date);
            $maids  = array();
            foreach ($list_leave_maidss as $maid) {
                $newdata['maid_id'] = $maid->maid_id;
                $newdata['odoo_maid_id'] = $maid->odoo_maid_id;
                $newdata['maid_name'] = $maid->maid_name;
                $newdata['maid_nationality'] = $maid->maid_nationality;
                $newdata['maid_mobile_1'] = $maid->maid_mobile_1;
                $newdata['maid_photo_file'] = $maid->maid_photo_file;
                $newdata['flat_name'] = $maid->flat_name;
                $newdata['odoo_flat_id'] = $maid->odoo_flat_id;
                $newdata['maid_status'] = $maid->maid_status;
                $newdata['odoo_synch_status'] = $maid->odoo_synch_status;
                array_push($maids, (object) $newdata);
            }
        } else if ($inactive_maid == 6) {
            $view_id = $inactive_maid;
            $inactiveemaidss = $this->maids_model->list_inactivemaids();
            $maids  = array();
            foreach ($inactiveemaidss as $maid) {
                $newdata['maid_id'] = $maid->maid_id;
                $newdata['odoo_maid_id'] = $maid->odoo_maid_id;
                $newdata['maid_name'] = $maid->maid_name;
                $newdata['maid_nationality'] = $maid->maid_nationality;
                $newdata['maid_mobile_1'] = $maid->maid_mobile_1;
                $newdata['maid_photo_file'] = $maid->maid_photo_file;
                $newdata['flat_name'] = $maid->flat_name;
                $newdata['odoo_flat_id'] = $maid->odoo_flat_id;
                $newdata['maid_status'] = $maid->maid_status;
                $newdata['odoo_synch_status'] = $maid->odoo_synch_status;
                array_push($maids, (object) $newdata);
            }
        } else {
            $view_id = 0;
            //$maids = $this->maids_model->get_maids($team_id);
            $maids = $this->maids_model->get_maids();
        }
        //ends
        $data['freemaid_ids'] = $view_id;
        //$data['maids'] = $this->reports_model->get_maids_for_report();
        $data['maids'] = $maids;
        $data['zones'] = $this->settings_model->get_zones();
        $data['zone_id'] = $zone_id;
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('zone_reports_new', $data, TRUE);
        $layout_data['page_title'] = 'Zone Reports';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1';
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default_dashboard', $layout_data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    public function newreporttoExcel()
    {
        $date_from_job = $this->uri->segment(3);
        if ($this->uri->segment(5) != "") {
            $zone_id = $this->uri->segment(5);
        } else {
            $zone_id = "";
        }
        if ($this->uri->segment(4) != "") {
            $freemaidid = $this->uri->segment(4);
        } else {
            $freemaidid = "";
        }
        $service_date = $date_from_job;
        //starts
        $get_leave_maids = $this->maids_model->get_maids_leave_by_date($service_date);
        $leave_maid_ids_new = array();
        foreach ($get_leave_maids as $leave) {
            array_push($leave_maid_ids_new, $leave->maid_id);
        }
        if ($freemaidid == 1) {
            //$maidss = $this->maids_model->get_maids($team_id);
            $maidss = $this->maids_model->get_maids();
            $maids = array();
            foreach ($maidss as $maid) {
                if (!in_array($maid->maid_id, $leave_maid_ids_new)) {
                    $shifts = $this->bookings_model->get_schedule_by_date_new_count($service_date, $maid->maid_id);
                    if ($shifts == 0) {
                        //$newdata[] = new stdClass();
                        $newdata['maid_id'] = $maid->maid_id;
                        $newdata['odoo_maid_id'] = $maid->odoo_maid_id;
                        $newdata['maid_name'] = $maid->maid_name;
                        $newdata['maid_nationality'] = $maid->maid_nationality;
                        $newdata['maid_mobile_1'] = $maid->maid_mobile_1;
                        $newdata['maid_photo_file'] = $maid->maid_photo_file;
                        $newdata['flat_name'] = $maid->flat_name;
                        $newdata['odoo_flat_id'] = $maid->odoo_flat_id;
                        $newdata['maid_status'] = $maid->maid_status;
                        $newdata['odoo_synch_status'] = $maid->odoo_synch_status;
                        //$newdata['team_name'] = $maid->team_name;
                        array_push($maids, (object) $newdata);
                        //$maids = ;
                    }
                }
            }
            $view_id = $freemaidid;
        } else if ($freemaidid == 2) {
            $view_id = $freemaidid;
            $maidss = $this->maids_model->get_maids();
            $maids  = array();
            $filter_by = 2;
            foreach ($maidss as $maid) {
                if (!in_array($maid->maid_id, $leave_maid_ids_new)) {
                    $get_avilable_before_noon = $this->bookings_model->get_avilable_before_noon($service_date, $maid->maid_id);
                    $count = count($get_avilable_before_noon);
                    $hour_array = array();
                    for ($i = 0; $i < $count; $i++) {
                        if ($i == 0) {
                            $gap = (strtotime($get_avilable_before_noon[$i]->time_from) - strtotime('08:00:00')) / 3600;
                            array_push($hour_array, $gap);
                            if ($count == 1) {
                                $gap2 = (strtotime('13:00:00') - strtotime($get_avilable_before_noon[$i]->time_to)) / 3600;
                                array_push($hour_array, $gap2);
                            }
                        } else if ($i == ($count - 1)) {
                            $gap = (strtotime($get_avilable_before_noon[$i]->time_from) - strtotime($get_avilable_before_noon[$i - 1]->time_to)) / 3600;
                            array_push($hour_array, $gap);
                            $gap2 = (strtotime('13:00:00') - strtotime($get_avilable_before_noon[$i]->time_to)) / 3600;
                            array_push($hour_array, $gap2);
                        } else {
                            $gap = (strtotime($get_avilable_before_noon[$i]->time_from) - strtotime($get_avilable_before_noon[$i - 1]->time_to)) / 3600;
                            array_push($hour_array, $gap);
                        }
                    }
                    foreach ($hour_array as $list) {
                        if ($list >= 3) {
                            $newdata['maid_id'] = $maid->maid_id;
                            $newdata['odoo_maid_id'] = $maid->odoo_maid_id;
                            $newdata['maid_name'] = $maid->maid_name;
                            $newdata['maid_nationality'] = $maid->maid_nationality;
                            $newdata['maid_mobile_1'] = $maid->maid_mobile_1;
                            $newdata['maid_photo_file'] = $maid->maid_photo_file;
                            $newdata['flat_name'] = $maid->flat_name;
                            $newdata['odoo_flat_id'] = $maid->odoo_flat_id;
                            $newdata['maid_status'] = $maid->maid_status;
                            $newdata['odoo_synch_status'] = $maid->odoo_synch_status;
                            $newdata['total_hrs'] = $total_hrs;
                            array_push($maids, (object) $newdata);
                        }
                    }
                    if ($count == 0) {
                        $newdata['maid_id'] = $maid->maid_id;
                        $newdata['odoo_maid_id'] = $maid->odoo_maid_id;
                        $newdata['maid_name'] = $maid->maid_name;
                        $newdata['maid_nationality'] = $maid->maid_nationality;
                        $newdata['maid_mobile_1'] = $maid->maid_mobile_1;
                        $newdata['maid_photo_file'] = $maid->maid_photo_file;
                        $newdata['flat_name'] = $maid->flat_name;
                        $newdata['odoo_flat_id'] = $maid->odoo_flat_id;
                        $newdata['maid_status'] = $maid->maid_status;
                        $newdata['odoo_synch_status'] = $maid->odoo_synch_status;
                        $newdata['total_hrs'] = $total_hrs;
                        array_push($maids, (object) $newdata);
                    }
                }
            }
            //print_r(json_encode($maids));
        } else if ($freemaidid == 3) {
            $view_id = $freemaidid;
            $maidss = $this->maids_model->get_maids();
            $maids  = array();
            $filter_by = 3;
            foreach ($maidss as $maid) {
                if (!in_array($maid->maid_id, $leave_maid_ids_new)) {
                    $get_avilable_after_noon = $this->bookings_model->get_avilable_after_noon($service_date, $maid->maid_id);
                    $count = count($get_avilable_after_noon);
                    $hour_array = array();
                    for ($i = 0; $i < $count; $i++) {
                        if ($i == 0) {
                            $gap = (strtotime($get_avilable_after_noon[$i]->time_from) - strtotime('13:00:00')) / 3600;
                            array_push($hour_array, $gap);
                            if ($count == 1) {
                                $gap2 = (strtotime('19:00:00') - strtotime($get_avilable_after_noon[$i]->time_to)) / 3600;
                                array_push($hour_array, $gap2);
                            }
                        } else if ($i == ($count - 1)) {
                            $gap = (strtotime($get_avilable_after_noon[$i]->time_from) - strtotime($get_avilable_after_noon[$i - 1]->time_to)) / 3600;
                            array_push($hour_array, $gap);
                            $gap2 = (strtotime('19:00:00') - strtotime($get_avilable_after_noon[$i]->time_to)) / 3600;
                            array_push($hour_array, $gap2);
                        } else {
                            $gap = (strtotime($get_avilable_after_noon[$i]->time_from) - strtotime($get_avilable_after_noon[$i - 1]->time_to)) / 3600;
                            array_push($hour_array, $gap);
                        }
                    }
                    foreach ($hour_array as $list) {
                        if ($list >= 3) {
                            $newdata['maid_id'] = $maid->maid_id;
                            $newdata['odoo_maid_id'] = $maid->odoo_maid_id;
                            $newdata['maid_name'] = $maid->maid_name;
                            $newdata['maid_nationality'] = $maid->maid_nationality;
                            $newdata['maid_mobile_1'] = $maid->maid_mobile_1;
                            $newdata['maid_photo_file'] = $maid->maid_photo_file;
                            $newdata['flat_name'] = $maid->flat_name;
                            $newdata['odoo_flat_id'] = $maid->odoo_flat_id;
                            $newdata['maid_status'] = $maid->maid_status;
                            $newdata['odoo_synch_status'] = $maid->odoo_synch_status;
                            $newdata['total_hrs'] = $total_hrs;
                            array_push($maids, (object) $newdata);
                        }
                    }
                    if ($count == 0) {
                        $newdata['maid_id'] = $maid->maid_id;
                        $newdata['odoo_maid_id'] = $maid->odoo_maid_id;
                        $newdata['maid_name'] = $maid->maid_name;
                        $newdata['maid_nationality'] = $maid->maid_nationality;
                        $newdata['maid_mobile_1'] = $maid->maid_mobile_1;
                        $newdata['maid_photo_file'] = $maid->maid_photo_file;
                        $newdata['flat_name'] = $maid->flat_name;
                        $newdata['odoo_flat_id'] = $maid->odoo_flat_id;
                        $newdata['maid_status'] = $maid->maid_status;
                        $newdata['odoo_synch_status'] = $maid->odoo_synch_status;
                        $newdata['total_hrs'] = $total_hrs;
                        array_push($maids, (object) $newdata);
                    }
                }
            }
        } else if ($freemaidid == 4) {
            $view_id = $freemaidid;
            $maidss = $this->maids_model->get_maids();
            $maids  = array();
            $filter_by = 4;
            foreach ($maidss as $maid) {
                if (!in_array($maid->maid_id, $leave_maid_ids_new)) {
                    //$newdata[] = new stdClass();
                    $newdata['maid_id'] = $maid->maid_id;
                    $newdata['odoo_maid_id'] = $maid->odoo_maid_id;
                    $newdata['maid_name'] = $maid->maid_name;
                    $newdata['maid_nationality'] = $maid->maid_nationality;
                    $newdata['maid_mobile_1'] = $maid->maid_mobile_1;
                    $newdata['maid_photo_file'] = $maid->maid_photo_file;
                    $newdata['flat_name'] = $maid->flat_name;
                    $newdata['odoo_flat_id'] = $maid->odoo_flat_id;
                    $newdata['maid_status'] = $maid->maid_status;
                    $newdata['odoo_synch_status'] = $maid->odoo_synch_status;
                    //$newdata['team_name'] = $maid->team_name;
                    array_push($maids, (object) $newdata);
                    //$maids = ;
                }
            }
        } else if ($freemaidid == 5) {
            $view_id = $freemaidid;
            $list_leave_maidss = $this->maids_model->list_maids_leave_by_date($service_date);
            $maids  = array();
            foreach ($list_leave_maidss as $maid) {
                $newdata['maid_id'] = $maid->maid_id;
                $newdata['odoo_maid_id'] = $maid->odoo_maid_id;
                $newdata['maid_name'] = $maid->maid_name;
                $newdata['maid_nationality'] = $maid->maid_nationality;
                $newdata['maid_mobile_1'] = $maid->maid_mobile_1;
                $newdata['maid_photo_file'] = $maid->maid_photo_file;
                $newdata['flat_name'] = $maid->flat_name;
                $newdata['odoo_flat_id'] = $maid->odoo_flat_id;
                $newdata['maid_status'] = $maid->maid_status;
                $newdata['odoo_synch_status'] = $maid->odoo_synch_status;
                array_push($maids, (object) $newdata);
            }
        } else if ($freemaidid == 6) {
            $view_id = $freemaidid;
            $inactiveemaidss = $this->maids_model->list_inactivemaids();
            $maids  = array();
            foreach ($inactiveemaidss as $maid) {
                $newdata['maid_id'] = $maid->maid_id;
                $newdata['odoo_maid_id'] = $maid->odoo_maid_id;
                $newdata['maid_name'] = $maid->maid_name;
                $newdata['maid_nationality'] = $maid->maid_nationality;
                $newdata['maid_mobile_1'] = $maid->maid_mobile_1;
                $newdata['maid_photo_file'] = $maid->maid_photo_file;
                $newdata['flat_name'] = $maid->flat_name;
                $newdata['odoo_flat_id'] = $maid->odoo_flat_id;
                $newdata['maid_status'] = $maid->maid_status;
                $newdata['odoo_synch_status'] = $maid->odoo_synch_status;
                array_push($maids, (object) $newdata);
            }
        } else {
            $view_id = 0;
            //$maids = $this->maids_model->get_maids($team_id);
            $maids = $this->maids_model->get_maids();
        }
        //ends
        $data['freemaid_ids'] = $view_id;
        $data['date_from_job'] = $date_from_job;
        $data['zone_id'] = $zone_id;
        //$data['maids'] = $this->reports_model->get_maids_for_report();
        $data['maids'] = $maids;
        //$data['zones'] = $this->settings_model->get_zones();
        $this->load->view('zone_reports_new_excel', $data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    function vehicle_report_test()
    {
        if (!user_permission(user_authenticate(), 14)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $data['vehicle_report'] = array();
        $data['search'] = array();
        $date = "";
        $zone = "";
        $zone_name = "All";
        $day = strftime("%A", strtotime(date('Y-m-d')));
        $search = array('search_date' => $date, 'search_zone' => $zone, 'search_zone_name' => $zone_name, 'search_day' => $day);
        $data['search'] = $search;
        if ($this->input->post('vehicle_report')) {
            $date = $this->input->post('vehicle_date');
            $date_to = $this->input->post('vehicle_date_to');
            $customer_id = $this->input->post('customers_vh_rep');
            if ($date != "") {
                list($day, $month, $year) = explode("/", $date);
                $vehicle_date = "$year-$month-$day";
                $day = strftime("%A", strtotime($vehicle_date));
            } else {
                $vehicle_date = "";
                $day = "";
            }
            if ($date_to != "") {
                list($day, $month, $year) = explode("/", $date_to);
                $vehicle_date_to = "$year-$month-$day";
                $day_to = strftime("%A", strtotime($vehicle_date_to));
            } else {
                $vehicle_date_to = "";
                $day_to = "";
            }
            $zone = $this->input->post('zones');
            if ($zone == "") {
                $zone_name = "All";
            } else {
                $zone_name = $this->reports_model->get_zone_name($zone);
            }
            $loop_start_date = $vehicle_date;
            $loop_end_date = $vehicle_date_to;
            $vehicle_report = array();
            $vehicle_result = array();
            while (strtotime($loop_start_date) <= strtotime($loop_end_date)) {
                $vehicle_result = array_merge($vehicle_result, $this->reports_model->get_vehicle_reports_new($loop_start_date, $zone, $customer_id));
                $loop_start_date = date('Y-m-d', strtotime("+1 day", strtotime($loop_start_date)));
            }
            if (count($vehicle_result) > 0) {
                $i = 0;
                $MaidId = 0;
                $CustId = 0;
                $time_To = 0;
                foreach ($vehicle_result as $vehicle_res) {
                    $check_buk_exist = $this->bookings_model->get_booking_exist($vehicle_res->booking_id, $vehicle_date);
                    if (!empty($check_buk_exist)) {
                        $mop = $check_buk_exist->mop;
                        $ref = $check_buk_exist->just_mop_ref;
                    } else {
                        if ($vehicle_res->pay_by != "") {
                            $mop = $vehicle_res->pay_by;
                            $ref = $vehicle_res->justmop_reference;;
                        } else {
                            $mop = $vehicle_res->payment_mode;
                            $ref = $vehicle_res->justmop_reference;;
                        }
                        //$mop = $vehicle_res->payment_mode;
                        //$ref = $vehicle_res->justmop_reference;
                    }
                    if ($vehicle_res->customer_address == "") {
                        $a_address = 'Building - ' . $vehicle_res->building . ', ' . $vehicle_res->unit_no . '' . $vehicle_res->street;
                    } else {
                        $a_address = $vehicle_res->customer_address;
                    }
                    if ($vehicle_res->maid_id == $MaidId && $vehicle_res->customer_id == $CustId) {
                        if ($time_To == $vehicle_res->time_from) {
                            $j++;
                            $vehicle_report[$i - $j]['time_to'] = $vehicle_res->time_to;
                        } else {
                            $j = 0;
                            $vehicle_report[$i]['maid_id'] = $vehicle_res->maid_id;
                            $vehicle_report[$i]['maid'] = $vehicle_res->maid_name;
                            $vehicle_report[$i]['customer_id'] = $vehicle_res->customer_id;
                            $vehicle_report[$i]['customer'] = $vehicle_res->customer_name;
                            $vehicle_report[$i]['customer_paytype'] = $vehicle_res->payment_type;
                            $vehicle_report[$i]['customer_address'] = $a_address;
                            $vehicle_report[$i]['zone_name'] = $vehicle_res->zone_name;
                            $vehicle_report[$i]['booking_type'] = $vehicle_res->booking_type;
                            $vehicle_report[$i]['time_from'] = $vehicle_res->time_from;
                            $vehicle_report[$i]['time_to'] = $vehicle_res->time_to;
                            $vehicle_report[$i]['cleaning_material'] = $vehicle_res->cleaning_material;
                            $vehicle_report[$i]['booking_note'] = $vehicle_res->booking_note;
                            $vehicle_report[$i]['booked_from'] = $vehicle_res->booked_from;
                            $vehicle_report[$i]['price_per_hr'] = $vehicle_res->price_per_hr;
                            $vehicle_report[$i]['discount'] = $vehicle_res->discount;
                            $vehicle_report[$i]['total_amount'] = $vehicle_res->total_amount;
                            $vehicle_report[$i]['price_hourly'] = $vehicle_res->price_hourly;
                            $vehicle_report[$i]['price_extra'] = $vehicle_res->price_extra;
                            $vehicle_report[$i]['price_weekend'] = $vehicle_res->price_weekend;
                            $vehicle_report[$i]['driver_name'] = $vehicle_res->driver_name;
                            $vehicle_report[$i]['customer_source'] = $vehicle_res->customer_source;
                            $vehicle_report[$i]['service_type_name'] = $vehicle_res->service_type_name;
                            $vehicle_report[$i]['mobile_number'] = $vehicle_res->mobile_number_1;
                            //$vehicle_report[$i]['payment_mode'] = $vehicle_res->payment_mode;
                            $vehicle_report[$i]['payment_mode'] = $mop;
                            $vehicle_report[$i]['justmop_reference'] = $ref;
                        }
                    } else {
                        $j = 0;
                        $vehicle_report[$i]['maid_id'] = $vehicle_res->maid_id;
                        $vehicle_report[$i]['maid'] = $vehicle_res->maid_name;
                        $vehicle_report[$i]['customer_id'] = $vehicle_res->customer_id;
                        $vehicle_report[$i]['customer'] = $vehicle_res->customer_name;
                        $vehicle_report[$i]['customer_paytype'] = $vehicle_res->payment_type;
                        $vehicle_report[$i]['customer_address'] = $a_address;
                        $vehicle_report[$i]['zone_name'] = $vehicle_res->zone_name;
                        $vehicle_report[$i]['booking_type'] = $vehicle_res->booking_type;
                        $vehicle_report[$i]['time_from'] = $vehicle_res->time_from;
                        $vehicle_report[$i]['time_to'] = $vehicle_res->time_to;
                        $vehicle_report[$i]['cleaning_material'] = $vehicle_res->cleaning_material;
                        $vehicle_report[$i]['booking_note'] = $vehicle_res->booking_note;
                        $vehicle_report[$i]['booked_from'] = $vehicle_res->booked_from;
                        $vehicle_report[$i]['price_per_hr'] = $vehicle_res->price_per_hr;
                        $vehicle_report[$i]['discount'] = $vehicle_res->discount;
                        $vehicle_report[$i]['total_amount'] = $vehicle_res->total_amount;
                        $vehicle_report[$i]['price_hourly'] = $vehicle_res->price_hourly;
                        $vehicle_report[$i]['price_extra'] = $vehicle_res->price_extra;
                        $vehicle_report[$i]['price_weekend'] = $vehicle_res->price_weekend;
                        $vehicle_report[$i]['driver_name'] = $vehicle_res->driver_name;
                        $vehicle_report[$i]['customer_source'] = $vehicle_res->customer_source;
                        $vehicle_report[$i]['service_type_name'] = $vehicle_res->service_type_name;
                        $vehicle_report[$i]['mobile_number'] = $vehicle_res->mobile_number_1;
                        //$vehicle_report[$i]['payment_mode'] = $vehicle_res->payment_mode;
                        //$vehicle_report[$i]['justmop_reference'] = $vehicle_res->justmop_reference;
                        $vehicle_report[$i]['payment_mode'] = $mop;
                        $vehicle_report[$i]['justmop_reference'] = $ref;
                    }
                    $time_To = $vehicle_res->time_to;
                    $data['vehicle_report'] = $vehicle_report;
                    $MaidId = $vehicle_res->maid_id;
                    $CustId = $vehicle_res->customer_id;
                    $i++;
                }
            }
            $search = array('search_date' => $date, 'search_date_to' => $date_to, 'search_zone' => $zone, 'customer' => $customer_id, 'search_zone_name' => $zone_name, 'search_day' => $day);
            $data['search'] = $search;
        } else {
            $date = date('d/m/Y', strtotime('-3 day'));
            $date_to = date('d/m/Y');
            $customer_id = '';
            $start_date = date('d/m/Y', strtotime('-3 day'));
            if ($date != "") {
                list($day, $month, $year) = explode("/", $date);
                $vehicle_date = "$year-$month-$day";
                $day = strftime("%A", strtotime($vehicle_date));
            } else {
                $vehicle_date = "";
                $day = "";
            }
            if ($date_to != "") {
                list($day, $month, $year) = explode("/", $date_to);
                $vehicle_date_to = "$year-$month-$day";
                $day_to = strftime("%A", strtotime($vehicle_date_to));
            } else {
                $vehicle_date_to = "";
                $day = "";
            }
            $zone = $this->input->post('zones');
            if ($zone == "") {
                $zone_name = "All";
            } else {
                $zone_name = $this->reports_model->get_zone_name($zone);
            }
            $loop_start_date = $vehicle_date;
            $loop_end_date = $vehicle_date_to;
            $vehicle_report = array();
            $vehicle_result = array();
            while (strtotime($loop_start_date) <= strtotime($loop_end_date)) {
                $vehicle_result = array_merge($vehicle_result, $this->reports_model->get_vehicle_reports_new($loop_start_date, $zone, $customer_id));
                $loop_start_date = date('Y-m-d', strtotime("+1 day", strtotime($loop_start_date)));
            }
            if (count($vehicle_result) > 0) {
                $i = 0;
                $MaidId = 0;
                $CustId = 0;
                $time_To = 0;
                foreach ($vehicle_result as $vehicle_res) {
                    $check_buk_exist = $this->bookings_model->get_booking_exist($vehicle_res->booking_id, $vehicle_date);
                    if (!empty($check_buk_exist)) {
                        $mop = $check_buk_exist->mop;
                        $ref = $check_buk_exist->just_mop_ref;
                    } else {
                        if ($vehicle_res->pay_by != "") {
                            $mop = $vehicle_res->pay_by;
                            $ref = $vehicle_res->justmop_reference;;
                        } else {
                            $mop = $vehicle_res->payment_mode;
                            $ref = $vehicle_res->justmop_reference;;
                        }
                        //$mop = $vehicle_res->payment_mode;
                        //$ref = $vehicle_res->justmop_reference;
                    }
                    if ($vehicle_res->customer_address == "") {
                        $a_address = 'Building - ' . $vehicle_res->building . ', ' . $vehicle_res->unit_no . '' . $vehicle_res->street;
                    } else {
                        $a_address = $vehicle_res->customer_address;
                    }
                    if ($vehicle_res->maid_id == $MaidId && $vehicle_res->customer_id == $CustId) {
                        if ($time_To == $vehicle_res->time_from) {
                            $j++;
                            $vehicle_report[$i - $j]['time_to'] = $vehicle_res->time_to;
                        } else {
                            $j = 0;
                            $vehicle_report[$i]['maid_id'] = $vehicle_res->maid_id;
                            $vehicle_report[$i]['maid'] = $vehicle_res->maid_name;
                            $vehicle_report[$i]['customer_id'] = $vehicle_res->customer_id;
                            $vehicle_report[$i]['customer'] = $vehicle_res->customer_name;
                            $vehicle_report[$i]['customer_paytype'] = $vehicle_res->payment_type;
                            $vehicle_report[$i]['customer_address'] = $a_address;
                            $vehicle_report[$i]['zone_name'] = $vehicle_res->zone_name;
                            $vehicle_report[$i]['booking_type'] = $vehicle_res->booking_type;
                            $vehicle_report[$i]['time_from'] = $vehicle_res->time_from;
                            $vehicle_report[$i]['time_to'] = $vehicle_res->time_to;
                            $vehicle_report[$i]['cleaning_material'] = $vehicle_res->cleaning_material;
                            $vehicle_report[$i]['booking_note'] = $vehicle_res->booking_note;
                            $vehicle_report[$i]['booked_from'] = $vehicle_res->booked_from;
                            $vehicle_report[$i]['price_per_hr'] = $vehicle_res->price_per_hr;
                            $vehicle_report[$i]['discount'] = $vehicle_res->discount;
                            $vehicle_report[$i]['total_amount'] = $vehicle_res->total_amount;
                            $vehicle_report[$i]['price_hourly'] = $vehicle_res->price_hourly;
                            $vehicle_report[$i]['price_extra'] = $vehicle_res->price_extra;
                            $vehicle_report[$i]['price_weekend'] = $vehicle_res->price_weekend;
                            $vehicle_report[$i]['driver_name'] = $vehicle_res->driver_name;
                            $vehicle_report[$i]['customer_source'] = $vehicle_res->customer_source;
                            $vehicle_report[$i]['service_type_name'] = $vehicle_res->service_type_name;
                            $vehicle_report[$i]['mobile_number'] = $vehicle_res->mobile_number_1;
                            //$vehicle_report[$i]['payment_mode'] = $vehicle_res->payment_mode;
                            //$vehicle_report[$i]['justmop_reference'] = $vehicle_res->justmop_reference;
                            $vehicle_report[$i]['payment_mode'] = $mop;
                            $vehicle_report[$i]['justmop_reference'] = $ref;
                        }
                    } else {
                        $j = 0;
                        $vehicle_report[$i]['maid_id'] = $vehicle_res->maid_id;
                        $vehicle_report[$i]['maid'] = $vehicle_res->maid_name;
                        $vehicle_report[$i]['customer_id'] = $vehicle_res->customer_id;
                        $vehicle_report[$i]['customer'] = $vehicle_res->customer_name;
                        $vehicle_report[$i]['customer_paytype'] = $vehicle_res->payment_type;
                        $vehicle_report[$i]['customer_address'] = $a_address;
                        $vehicle_report[$i]['zone_name'] = $vehicle_res->zone_name;
                        $vehicle_report[$i]['booking_type'] = $vehicle_res->booking_type;
                        $vehicle_report[$i]['time_from'] = $vehicle_res->time_from;
                        $vehicle_report[$i]['time_to'] = $vehicle_res->time_to;
                        $vehicle_report[$i]['cleaning_material'] = $vehicle_res->cleaning_material;
                        $vehicle_report[$i]['booking_note'] = $vehicle_res->booking_note;
                        $vehicle_report[$i]['booked_from'] = $vehicle_res->booked_from;
                        $vehicle_report[$i]['price_per_hr'] = $vehicle_res->price_per_hr;
                        $vehicle_report[$i]['discount'] = $vehicle_res->discount;
                        $vehicle_report[$i]['total_amount'] = $vehicle_res->total_amount;
                        $vehicle_report[$i]['price_hourly'] = $vehicle_res->price_hourly;
                        $vehicle_report[$i]['price_extra'] = $vehicle_res->price_extra;
                        $vehicle_report[$i]['price_weekend'] = $vehicle_res->price_weekend;
                        $vehicle_report[$i]['driver_name'] = $vehicle_res->driver_name;
                        $vehicle_report[$i]['customer_source'] = $vehicle_res->customer_source;
                        $vehicle_report[$i]['service_type_name'] = $vehicle_res->service_type_name;
                        $vehicle_report[$i]['mobile_number'] = $vehicle_res->mobile_number_1;
                        //$vehicle_report[$i]['payment_mode'] = $vehicle_res->payment_mode;
                        //$vehicle_report[$i]['justmop_reference'] = $vehicle_res->justmop_reference;
                        $vehicle_report[$i]['payment_mode'] = $mop;
                        $vehicle_report[$i]['justmop_reference'] = $ref;
                    }
                    $time_To = $vehicle_res->time_to;
                    $data['vehicle_report'] = $vehicle_report;
                    $MaidId = $vehicle_res->maid_id;
                    $CustId = $vehicle_res->customer_id;
                    $i++;
                }
            }
            $search = array('search_date' => $date, 'search_date_to' => $date_to, 'customer' => $customer_id, 'search_zone' => $zone, 'search_zone_name' => $zone_name, 'search_day' => $day);
            $data['search'] = $search;
        }
        //$data['customers'] = $this->customers_model->get_customers_for_search();
        $data['zones'] = $this->settings_model->get_zones();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('vehicle_reports', $data, TRUE);
        $layout_data['page_title'] = 'Vehicle Reports';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1';
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    function vehicle_report()
    {
        if (!user_permission(user_authenticate(), 14)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $data['vehicle_report'] = array();
        $data['search'] = array();
        $date = "";
        $zone = "";
        $zone_name = "All";
        $company_name = "";
        $day = strftime("%A", strtotime(date('Y-m-d')));
        $search = array('search_date' => $date, 'search_zone' => $zone, 'search_zone_name' => $zone_name, 'search_day' => $day);
        $data['search'] = $search;
        $current_date = date('Y-m-d');
        $data['filter_maid_id'] =  $this->input->post('filter_maid_id') ?: null;
        if ($this->input->post('vehicle_report')) {
            $date = $this->input->post('vehicle_date');
            $date_to = $this->input->post('vehicle_date_to');
            $customer_id = $this->input->post('customers_vh_rep');
            $company_name = $this->input->post('company_vh_rep');
            if ($date != "") {
                list($day, $month, $year) = explode("/", $date);
                $vehicle_date = "$year-$month-$day";
                $day = strftime("%A", strtotime($vehicle_date));
            } else {
                $vehicle_date = "";
                $day = "";
            }
            if ($date_to != "") {
                list($day, $month, $year) = explode("/", $date_to);
                $vehicle_date_to = "$year-$month-$day";
                $day_to = strftime("%A", strtotime($vehicle_date_to));
            } else {
                $vehicle_date_to = "";
                $day_to = "";
            }
            $zone = $this->input->post('zones');
            if ($zone == "") {
                $zone_name = "All";
            } else {
                $zone_name = $this->reports_model->get_zone_name($zone);
            }
            $loop_start_date = $vehicle_date;
            $loop_end_date = $vehicle_date_to;
            $vehicle_report = array();
            $vehicle_result = array();
            while (strtotime($loop_start_date) <= strtotime($loop_end_date)) {
                if ($loop_start_date < $current_date) {
                    $vehicle_result = array_merge($vehicle_result, $this->reports_model->get_vehicle_reports_new_test($loop_start_date, $zone, $customer_id, $company_name,$data['filter_maid_id']));
                } else {
                    $vehicle_result = array_merge($vehicle_result, $this->reports_model->get_vehicle_reports_new($loop_start_date, $zone, $customer_id, $company_name,$data['filter_maid_id']));
                }
                //$vehicle_result = array_merge($vehicle_result, $this->reports_model->get_vehicle_reports_new($loop_start_date, $zone,$customer_id));
                $loop_start_date = date('Y-m-d', strtotime("+1 day", strtotime($loop_start_date)));
            }
            if (count($vehicle_result) > 0) {
                $i = 0;
                $MaidId = 0;
                $CustId = 0;
                $time_To = 0;
                foreach ($vehicle_result as $vehicle_res) {
                    $check_buk_exist = $this->bookings_model->get_booking_exist($vehicle_res->booking_id, $vehicle_date);
                    if (!empty($check_buk_exist)) {
                        $mop = $check_buk_exist->mop;
                        $ref = $check_buk_exist->just_mop_ref;
                    } else {
                        if ($vehicle_res->pay_by != "") {
                            $mop = $vehicle_res->pay_by;
                            $ref = $vehicle_res->justmop_reference;;
                        } else {
                            $mop = $vehicle_res->payment_mode;
                            $ref = $vehicle_res->justmop_reference;;
                        }
                        //$mop = $vehicle_res->payment_mode;
                        //$ref = $vehicle_res->justmop_reference;
                    }
                    if ($vehicle_res->customer_address == "") {
                        $a_address = 'Building - ' . $vehicle_res->building . ', ' . $vehicle_res->unit_no . '' . $vehicle_res->street;
                    } else {
                        $a_address = $vehicle_res->customer_address;
                    }
                    if ($vehicle_res->maid_id == $MaidId && $vehicle_res->customer_id == $CustId) {
                        if ($time_To == $vehicle_res->time_from) {
                            $j++;
                            $vehicle_report[$i - $j]['time_to'] = $vehicle_res->time_to;
                        } else {
                            $j = 0;
                            $vehicle_report[$i]['maid_id'] = $vehicle_res->maid_id;
                            $vehicle_report[$i]['maid'] = $vehicle_res->maid_name;
                            $vehicle_report[$i]['customer_id'] = $vehicle_res->customer_id;
                            $vehicle_report[$i]['customer'] = $vehicle_res->customer_name;
                            $vehicle_report[$i]['customer_paytype'] = $vehicle_res->payment_type;
                            $vehicle_report[$i]['customer_address'] = $a_address;
                            $vehicle_report[$i]['zone_name'] = $vehicle_res->zone_name;
                            $vehicle_report[$i]['booking_type'] = $vehicle_res->booking_type;
                            $vehicle_report[$i]['time_from'] = $vehicle_res->time_from;
                            $vehicle_report[$i]['time_to'] = $vehicle_res->time_to;
                            $vehicle_report[$i]['cleaning_material'] = $vehicle_res->cleaning_material;
                            $vehicle_report[$i]['booking_note'] = $vehicle_res->booking_note;
                            $vehicle_report[$i]['booked_from'] = $vehicle_res->booked_from;
                            $vehicle_report[$i]['price_per_hr'] = $vehicle_res->price_per_hr;
                            $vehicle_report[$i]['discount'] = $vehicle_res->discount;
                            $vehicle_report[$i]['total_amount'] = $vehicle_res->total_amount;
                            $vehicle_report[$i]['price_hourly'] = $vehicle_res->price_hourly;
                            $vehicle_report[$i]['price_extra'] = $vehicle_res->price_extra;
                            $vehicle_report[$i]['price_weekend'] = $vehicle_res->price_weekend;
                            $vehicle_report[$i]['driver_name'] = $vehicle_res->driver_name;
                            $vehicle_report[$i]['customer_source'] = $vehicle_res->customer_source;
                            $vehicle_report[$i]['service_type_name'] = $vehicle_res->service_type_name;
                            $vehicle_report[$i]['mobile_number'] = $vehicle_res->mobile_number_1;
                            //$vehicle_report[$i]['payment_mode'] = $vehicle_res->payment_mode;
                            $vehicle_report[$i]['payment_mode'] = $mop;
                            $vehicle_report[$i]['justmop_reference'] = $ref;
                            $vehicle_report[$i]['veh_date'] = $vehicle_res->start_date;
                        }
                    } else {
                        $j = 0;
                        $vehicle_report[$i]['maid_id'] = $vehicle_res->maid_id;
                        $vehicle_report[$i]['maid'] = $vehicle_res->maid_name;
                        $vehicle_report[$i]['customer_id'] = $vehicle_res->customer_id;
                        $vehicle_report[$i]['customer'] = $vehicle_res->customer_name;
                        $vehicle_report[$i]['customer_paytype'] = $vehicle_res->payment_type;
                        $vehicle_report[$i]['customer_address'] = $a_address;
                        $vehicle_report[$i]['zone_name'] = $vehicle_res->zone_name;
                        $vehicle_report[$i]['booking_type'] = $vehicle_res->booking_type;
                        $vehicle_report[$i]['time_from'] = $vehicle_res->time_from;
                        $vehicle_report[$i]['time_to'] = $vehicle_res->time_to;
                        $vehicle_report[$i]['cleaning_material'] = $vehicle_res->cleaning_material;
                        $vehicle_report[$i]['booking_note'] = $vehicle_res->booking_note;
                        $vehicle_report[$i]['booked_from'] = $vehicle_res->booked_from;
                        $vehicle_report[$i]['price_per_hr'] = $vehicle_res->price_per_hr;
                        $vehicle_report[$i]['discount'] = $vehicle_res->discount;
                        $vehicle_report[$i]['total_amount'] = $vehicle_res->total_amount;
                        $vehicle_report[$i]['price_hourly'] = $vehicle_res->price_hourly;
                        $vehicle_report[$i]['price_extra'] = $vehicle_res->price_extra;
                        $vehicle_report[$i]['price_weekend'] = $vehicle_res->price_weekend;
                        $vehicle_report[$i]['driver_name'] = $vehicle_res->driver_name;
                        $vehicle_report[$i]['customer_source'] = $vehicle_res->customer_source;
                        $vehicle_report[$i]['service_type_name'] = $vehicle_res->service_type_name;
                        $vehicle_report[$i]['mobile_number'] = $vehicle_res->mobile_number_1;
                        //$vehicle_report[$i]['payment_mode'] = $vehicle_res->payment_mode;
                        //$vehicle_report[$i]['justmop_reference'] = $vehicle_res->justmop_reference;
                        $vehicle_report[$i]['payment_mode'] = $mop;
                        $vehicle_report[$i]['justmop_reference'] = $ref;
                        $vehicle_report[$i]['veh_date'] = $vehicle_res->start_date;
                    }
                    $time_To = $vehicle_res->time_to;
                    $data['vehicle_report'] = $vehicle_report;
                    $MaidId = $vehicle_res->maid_id;
                    $CustId = $vehicle_res->customer_id;
                    $i++;
                }
            }
            $search = array('search_date' => $date, 'search_date_to' => $date_to, 'search_zone' => $zone, 'customer' => $customer_id, 'search_zone_name' => $zone_name, 'search_day' => $day, 'company_name' => $company_name);
            $data['search'] = $search;
        } else {
            $date = date('d/m/Y', strtotime('-3 day'));
            $date_to = date('d/m/Y');
            $customer_id = '';
            $company_name = '';
            $start_date = date('d/m/Y', strtotime('-3 day'));
            if ($date != "") {
                list($day, $month, $year) = explode("/", $date);
                $vehicle_date = "$year-$month-$day";
                $day = strftime("%A", strtotime($vehicle_date));
            } else {
                $vehicle_date = "";
                $day = "";
            }
            if ($date_to != "") {
                list($day, $month, $year) = explode("/", $date_to);
                $vehicle_date_to = "$year-$month-$day";
                $day_to = strftime("%A", strtotime($vehicle_date_to));
            } else {
                $vehicle_date_to = "";
                $day = "";
            }
            $zone = $this->input->post('zones');
            if ($zone == "") {
                $zone_name = "All";
            } else {
                $zone_name = $this->reports_model->get_zone_name($zone);
            }
            $loop_start_date = $vehicle_date;
            $loop_end_date = $vehicle_date_to;
            $vehicle_report = array();
            $vehicle_result = array();
            while (strtotime($loop_start_date) <= strtotime($loop_end_date)) {
                if ($loop_start_date < $current_date) {
                    $vehicle_result = array_merge($vehicle_result, $this->reports_model->get_vehicle_reports_new_test($loop_start_date, $zone, $customer_id, $company_name,$data['filter_maid_id']));
                } else {
                    $vehicle_result = array_merge($vehicle_result, $this->reports_model->get_vehicle_reports_new($loop_start_date, $zone, $customer_id, $company_name,$data['filter_maid_id']));
                }
                $loop_start_date = date('Y-m-d', strtotime("+1 day", strtotime($loop_start_date)));
            }
            if (count($vehicle_result) > 0) {
                $i = 0;
                $MaidId = 0;
                $CustId = 0;
                $time_To = 0;
                foreach ($vehicle_result as $vehicle_res) {
                    $check_buk_exist = $this->bookings_model->get_booking_exist($vehicle_res->booking_id, $vehicle_date);
                    if (!empty($check_buk_exist)) {
                        $mop = $check_buk_exist->mop;
                        $ref = $check_buk_exist->just_mop_ref;
                    } else {
                        if ($vehicle_res->pay_by != "") {
                            $mop = $vehicle_res->pay_by;
                            $ref = $vehicle_res->justmop_reference;;
                        } else {
                            $mop = $vehicle_res->payment_mode;
                            $ref = $vehicle_res->justmop_reference;;
                        }
                        //$mop = $vehicle_res->payment_mode;
                        //$ref = $vehicle_res->justmop_reference;
                    }
                    if ($vehicle_res->customer_address == "") {
                        $a_address = 'Building - ' . $vehicle_res->building . ', ' . $vehicle_res->unit_no . '' . $vehicle_res->street;
                    } else {
                        $a_address = $vehicle_res->customer_address;
                    }
                    if ($vehicle_res->maid_id == $MaidId && $vehicle_res->customer_id == $CustId) {
                        if ($time_To == $vehicle_res->time_from) {
                            $j++;
                            $vehicle_report[$i - $j]['time_to'] = $vehicle_res->time_to;
                        } else {
                            $j = 0;
                            $vehicle_report[$i]['maid_id'] = $vehicle_res->maid_id;
                            $vehicle_report[$i]['maid'] = $vehicle_res->maid_name;
                            $vehicle_report[$i]['customer_id'] = $vehicle_res->customer_id;
                            $vehicle_report[$i]['customer'] = $vehicle_res->customer_name;
                            $vehicle_report[$i]['customer_paytype'] = $vehicle_res->payment_type;
                            $vehicle_report[$i]['customer_address'] = $a_address;
                            $vehicle_report[$i]['zone_name'] = $vehicle_res->zone_name;
                            $vehicle_report[$i]['booking_type'] = $vehicle_res->booking_type;
                            $vehicle_report[$i]['time_from'] = $vehicle_res->time_from;
                            $vehicle_report[$i]['time_to'] = $vehicle_res->time_to;
                            $vehicle_report[$i]['cleaning_material'] = $vehicle_res->cleaning_material;
                            $vehicle_report[$i]['booking_note'] = $vehicle_res->booking_note;
                            $vehicle_report[$i]['booked_from'] = $vehicle_res->booked_from;
                            $vehicle_report[$i]['price_per_hr'] = $vehicle_res->price_per_hr;
                            $vehicle_report[$i]['discount'] = $vehicle_res->discount;
                            $vehicle_report[$i]['total_amount'] = $vehicle_res->total_amount;
                            $vehicle_report[$i]['price_hourly'] = $vehicle_res->price_hourly;
                            $vehicle_report[$i]['price_extra'] = $vehicle_res->price_extra;
                            $vehicle_report[$i]['price_weekend'] = $vehicle_res->price_weekend;
                            $vehicle_report[$i]['driver_name'] = $vehicle_res->driver_name;
                            $vehicle_report[$i]['customer_source'] = $vehicle_res->customer_source;
                            $vehicle_report[$i]['service_type_name'] = $vehicle_res->service_type_name;
                            $vehicle_report[$i]['mobile_number'] = $vehicle_res->mobile_number_1;
                            //$vehicle_report[$i]['payment_mode'] = $vehicle_res->payment_mode;
                            //$vehicle_report[$i]['justmop_reference'] = $vehicle_res->justmop_reference;
                            $vehicle_report[$i]['payment_mode'] = $mop;
                            $vehicle_report[$i]['justmop_reference'] = $ref;

                            $start_date_formatted = date('d-m-Y', strtotime($vehicle_res->start_date));
                            $vehicle_report[$i]['veh_date'] = $start_date_formatted;
                        }
                    } else {
                        $j = 0;
                        $vehicle_report[$i]['maid_id'] = $vehicle_res->maid_id;
                        $vehicle_report[$i]['maid'] = $vehicle_res->maid_name;
                        $vehicle_report[$i]['customer_id'] = $vehicle_res->customer_id;
                        $vehicle_report[$i]['customer'] = $vehicle_res->customer_name;
                        $vehicle_report[$i]['customer_paytype'] = $vehicle_res->payment_type;
                        $vehicle_report[$i]['customer_address'] = $a_address;
                        $vehicle_report[$i]['zone_name'] = $vehicle_res->zone_name;
                        $vehicle_report[$i]['booking_type'] = $vehicle_res->booking_type;
                        $vehicle_report[$i]['time_from'] = $vehicle_res->time_from;
                        $vehicle_report[$i]['time_to'] = $vehicle_res->time_to;
                        $vehicle_report[$i]['cleaning_material'] = $vehicle_res->cleaning_material;
                        $vehicle_report[$i]['booking_note'] = $vehicle_res->booking_note;
                        $vehicle_report[$i]['booked_from'] = $vehicle_res->booked_from;
                        $vehicle_report[$i]['price_per_hr'] = $vehicle_res->price_per_hr;
                        $vehicle_report[$i]['discount'] = $vehicle_res->discount;
                        $vehicle_report[$i]['total_amount'] = $vehicle_res->total_amount;
                        $vehicle_report[$i]['price_hourly'] = $vehicle_res->price_hourly;
                        $vehicle_report[$i]['price_extra'] = $vehicle_res->price_extra;
                        $vehicle_report[$i]['price_weekend'] = $vehicle_res->price_weekend;
                        $vehicle_report[$i]['driver_name'] = $vehicle_res->driver_name;
                        $vehicle_report[$i]['customer_source'] = $vehicle_res->customer_source;
                        $vehicle_report[$i]['service_type_name'] = $vehicle_res->service_type_name;
                        $vehicle_report[$i]['mobile_number'] = $vehicle_res->mobile_number_1;
                        //$vehicle_report[$i]['payment_mode'] = $vehicle_res->payment_mode;
                        //$vehicle_report[$i]['justmop_reference'] = $vehicle_res->justmop_reference;
                        $vehicle_report[$i]['payment_mode'] = $mop;
                        $vehicle_report[$i]['justmop_reference'] = $ref;
                        $start_date_formatted = date('d-m-Y', strtotime($vehicle_res->start_date));
                        $vehicle_report[$i]['veh_date'] = $start_date_formatted;
                    }
                    $time_To = $vehicle_res->time_to;
                    $data['vehicle_report'] = $vehicle_report;
                    $MaidId = $vehicle_res->maid_id;
                    $CustId = $vehicle_res->customer_id;
                    $i++;
                }
            }
            $search = array('search_date' => $date, 'search_date_to' => $date_to, 'customer' => $customer_id, 'search_zone' => $zone, 'search_zone_name' => $zone_name, 'search_day' => $day, 'company_name' => $company_name);
            $data['search'] = $search;
        }
        //$data['customers'] = $this->customers_model->get_customers_for_search();
        $data['zones'] = $this->settings_model->get_zones();
        $data['maids'] = $this->reports_model->get_all_maids();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('vehicle_reports', $data, TRUE);
        $layout_data['page_title'] = 'Vehicle Report';
        $layout_data['meta_description'] = 'Vehicle Report';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1';
        $layout_data['js_files'] = array('base.js','bootstrap-datepicker.js', 'jquery.dataTables.min.js','shim.min.js','xlsx.full.min.js','Blob.js','FileSaver.js','vehicle-report.js');
        $this->load->view('layouts/default', $layout_data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    function report_srch_usr()
    {
        $searchTerm = $this->input->post('searchTerm');
        $search_result = $this->customers_model->report_srch_usr($searchTerm);
        $data = array();
        foreach ($search_result as $user) {
            $data[] = array("id" => $user->customer_id, "text" => $user->customer_name);
        }
        print_r(json_encode($data));
        exit();
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    function vehiclereporttoExcel()
    {
        //   	ini_set('display_errors', 1);
        // ini_set('display_startup_errors', 1);
        // error_reporting(E_ALL);
        if (!user_permission(user_authenticate(), 14)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $vehicle_date = $this->uri->segment(3);
        $vehicle_date_to = $this->uri->segment(5);
        $customer_id = $this->uri->segment(6);
        $company_name = $this->uri->segment(7);
        $zone = $this->uri->segment(4);
        $data['filter_maid_id'] =  $this->input->post('filter_maid_id') ?: null;
        //$vehicle_result = $this->reports_model->get_vehicle_reports($vehicle_date, $zone);
        $vehicle_report = array();
        $current_date = date('Y-m-d');
        $zone = $this->input->post('zones');
        if ($zone == "") {
            $zone_name = "All";
        } else if ($zone == "All") {
            $zone_name = "All";
        } else {
            $zone_name = $this->reports_model->get_zone_name($zone);
        }
        $loop_start_date = $vehicle_date;
        $loop_end_date = $vehicle_date_to;
        $vehicle_result = array();
        while (strtotime($loop_start_date) <= strtotime($loop_end_date)) {
            if ($loop_start_date < $current_date) {
                $vehicle_result = array_merge($vehicle_result, $this->reports_model->get_vehicle_reports_new_test($loop_start_date, $zone, $customer_id, $company_name,$data['filter_maid_id']));
            } else {
                $vehicle_result = array_merge($vehicle_result, $this->reports_model->get_vehicle_reports_new($loop_start_date, $zone, $customer_id, $company_name,$data['filter_maid_id']));
            }
            //$vehicle_result = array_merge($vehicle_result, $this->reports_model->get_vehicle_reports_new($loop_start_date, $zone,$customer_id));
            $loop_start_date = date('Y-m-d', strtotime("+1 day", strtotime($loop_start_date)));
        }
        if (count($vehicle_result) > 0) {
            $i = 0;
            $MaidId = 0;
            $CustId = 0;
            $time_To = 0;
            foreach ($vehicle_result as $vehicle_res) {
                $check_buk_exist = $this->bookings_model->get_booking_exist($vehicle_res->booking_id, $vehicle_date);
                if (!empty($check_buk_exist)) {
                    $mop = $check_buk_exist->mop;
                    $ref = $check_buk_exist->just_mop_ref;
                } else {
                    if ($vehicle_res->pay_by != "") {
                        $mop = $vehicle_res->pay_by;
                        $ref = $vehicle_res->justmop_reference;
                    } else {
                        $mop = $vehicle_res->payment_mode;
                        $ref = $vehicle_res->justmop_reference;
                    }
                    //$mop = $vehicle_res->payment_mode;
                    //$ref = $vehicle_res->justmop_reference;
                }
                if ($vehicle_res->customer_address == "") {
                    $a_address = 'Building - ' . $vehicle_res->building . ', ' . $vehicle_res->unit_no . '' . $vehicle_res->street;
                } else {
                    $a_address = $vehicle_res->customer_address;
                }
                if ($vehicle_res->maid_id == $MaidId && $vehicle_res->customer_id == $CustId) {
                    if ($time_To == $vehicle_res->time_from) {
                        $j++;
                        $vehicle_report[$i - $j]['time_to'] = $vehicle_res->time_to;
                    } else {
                        $j = 0;
                        $vehicle_report[$i]['maid_id'] = $vehicle_res->maid_id;
                        $vehicle_report[$i]['maid'] = $vehicle_res->maid_name;
                        $vehicle_report[$i]['customer_id'] = $vehicle_res->customer_id;
                        $vehicle_report[$i]['customer'] = $vehicle_res->customer_name;
                        $vehicle_report[$i]['customer_paytype'] = $vehicle_res->payment_type;
                        $vehicle_report[$i]['customer_address'] = $a_address;
                        $vehicle_report[$i]['zone_name'] = $vehicle_res->zone_name;
                        $vehicle_report[$i]['booking_type'] = $vehicle_res->booking_type;
                        $vehicle_report[$i]['time_from'] = $vehicle_res->time_from;
                        $vehicle_report[$i]['time_to'] = $vehicle_res->time_to;
                        $vehicle_report[$i]['cleaning_material'] = $vehicle_res->cleaning_material;
                        $vehicle_report[$i]['booking_note'] = $vehicle_res->booking_note;
                        $vehicle_report[$i]['booked_from'] = $vehicle_res->booked_from;
                        $vehicle_report[$i]['price_per_hr'] = $vehicle_res->price_per_hr;
                        $vehicle_report[$i]['discount'] = $vehicle_res->discount;
                        $vehicle_report[$i]['total_amount'] = $vehicle_res->total_amount;
                        $vehicle_report[$i]['price_hourly'] = $vehicle_res->price_hourly;
                        $vehicle_report[$i]['price_extra'] = $vehicle_res->price_extra;
                        $vehicle_report[$i]['price_weekend'] = $vehicle_res->price_weekend;
                        $vehicle_report[$i]['driver_name'] = $vehicle_res->driver_name;
                        $vehicle_report[$i]['customer_source'] = $vehicle_res->customer_source;
                        $vehicle_report[$i]['payment_mode'] = $mop;
                        $vehicle_report[$i]['service_type_name'] = $vehicle_res->service_type_name;
                        $vehicle_report[$i]['justmop_reference'] = $ref;
                        $start_date_formatted = date('d-m-Y', strtotime($vehicle_res->start_date));
                        $vehicle_report[$i]['veh_date'] = $start_date_formatted;
                    }
                } else {
                    $j = 0;
                    $vehicle_report[$i]['maid_id'] = $vehicle_res->maid_id;
                    $vehicle_report[$i]['maid'] = $vehicle_res->maid_name;
                    $vehicle_report[$i]['customer_id'] = $vehicle_res->customer_id;
                    $vehicle_report[$i]['customer'] = $vehicle_res->customer_name;
                    $vehicle_report[$i]['customer_paytype'] = $vehicle_res->payment_type;
                    $vehicle_report[$i]['customer_address'] = $a_address;
                    $vehicle_report[$i]['zone_name'] = $vehicle_res->zone_name;
                    $vehicle_report[$i]['booking_type'] = $vehicle_res->booking_type;
                    $vehicle_report[$i]['time_from'] = $vehicle_res->time_from;
                    $vehicle_report[$i]['time_to'] = $vehicle_res->time_to;
                    $vehicle_report[$i]['cleaning_material'] = $vehicle_res->cleaning_material;
                    $vehicle_report[$i]['booking_note'] = $vehicle_res->booking_note;
                    $vehicle_report[$i]['booked_from'] = $vehicle_res->booked_from;
                    $vehicle_report[$i]['price_per_hr'] = $vehicle_res->price_per_hr;
                    $vehicle_report[$i]['discount'] = $vehicle_res->discount;
                    $vehicle_report[$i]['total_amount'] = $vehicle_res->total_amount;
                    $vehicle_report[$i]['price_hourly'] = $vehicle_res->price_hourly;
                    $vehicle_report[$i]['price_extra'] = $vehicle_res->price_extra;
                    $vehicle_report[$i]['price_weekend'] = $vehicle_res->price_weekend;
                    $vehicle_report[$i]['driver_name'] = $vehicle_res->driver_name;
                    $vehicle_report[$i]['customer_source'] = $vehicle_res->customer_source;
                    $vehicle_report[$i]['payment_mode'] = $mop;
                    $vehicle_report[$i]['service_type_name'] = $vehicle_res->service_type_name;
                    $vehicle_report[$i]['justmop_reference'] = $ref;
                    $start_date_formatted = date('d-m-Y', strtotime($vehicle_res->start_date));
                    $vehicle_report[$i]['veh_date'] = $start_date_formatted;
                }
                $time_To = $vehicle_res->time_to;
                $data['vehicle_report'] = $vehicle_report;
                $MaidId = $vehicle_res->maid_id;
                $CustId = $vehicle_res->customer_id;
                $i++;
            }
        }
        $data['searchdate'] = $this->uri->segment(3);
        $data['searchzone'] = $this->uri->segment(4);
        $this->load->view('vehiclereport_spreadsheetview', $data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    function payment_report()
    {
        if (!user_permission(user_authenticate(), 15)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $data['payment_report'] = array();
        $data['search'] = array();
        $date = "";
        $zone = "";
        $zone_name = "All";
        $day = strftime("%A", strtotime(date('Y-m-d')));
        $search = array('search_date' => $date, 'search_zone' => $zone, 'search_zone_name' => $zone_name, 'search_day' => $day);
        $data['search'] = $search;
        if ($this->input->post('payment_report')) {
            $date = $this->input->post('payment_date');
            if ($date != "") {
                list($day, $month, $year) = explode("/", $date);
                $payment_date = "$year-$month-$day";
                $day = strftime("%A", strtotime($payment_date));
            } else {
                $payment_date = "";
                $day = "";
            }
            $zone = $this->input->post('zones');
            if ($zone == "") {
                $zone_name = "All";
            } else {
                $zone_name = $this->reports_model->get_zone_name($zone);
            }
            $payment_result = $this->reports_model->get_payment_reports($payment_date, $zone);
            //$payment_result = $this->reports_model->get_payment_reports($payment_date);
            //echo "<pre>";
            //print_r($payment_result);
            $payment_report = array();
            foreach ($payment_result as $payment_res) {
                $payment_report['customer'] = $payment_res->customer_name;
                $payment_report['payment_type'] = $payment_res->payment_type;
                $payment_report['paid_amount'] = $payment_res->paid_amount;
                $payment_report['zone_name'] = $payment_res->zone_name;
                $payment_report['paid_datetime'] = date('d/m/Y H:i:s', strtotime($payment_res->paid_datetime));
                $data['payment_report'][] = $payment_report;
            }
            $search = array('search_date' => $date, 'search_zone' => $zone, 'search_zone_name' => $zone_name, 'search_day' => $day);
            $data['search'] = $search;
        } else {
            $date = date('d/m/Y');
            if ($date != "") {
                list($day, $month, $year) = explode("/", $date);
                $payment_date = "$year-$month-$day";
                $day = strftime("%A", strtotime($payment_date));
            } else {
                $payment_date = "";
                $day = "";
            }
            $zone = $this->input->post('zones');
            if ($zone == "") {
                $zone_name = "All";
            } else {
                $zone_name = $this->reports_model->get_zone_name($zone);
            }
            $payment_result = $this->reports_model->get_payment_reports($payment_date, $zone);
            //$payment_result = $this->reports_model->get_payment_reports($payment_date);
            //echo "<pre>"; print_r($payment_result);exit;
            $payment_report = array();
            foreach ($payment_result as $payment_res) {
                $payment_report['customer'] = $payment_res->customer_name;
                $payment_report['payment_type'] = $payment_res->payment_type;
                $payment_report['paid_amount'] = $payment_res->paid_amount;
                $payment_report['zone_name'] = $payment_res->zone_name;
                $payment_report['paid_datetime'] = date('d/m/Y H:i:s', strtotime($payment_res->paid_datetime));
                $data['payment_report'][] = $payment_report;
            }
            $search = array('search_date' => $date, 'search_zone' => $zone, 'search_zone_name' => $zone_name, 'search_day' => $day);
            $data['search'] = $search;
        }
        $data['zones'] = $this->settings_model->get_zones();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('payment_reports', $data, TRUE);
        $layout_data['page_title'] = 'Payment Reports';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        //        $layout_data['reports_active'] = '1';
        $layout_data['accounts_active'] = '1';
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    //Edited By Aparna
    function one_day_cancel()
    {
        if (!user_permission(user_authenticate(), 16)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        if ($this->input->post('ondeday_report')) {
            $data['search_date'] = $this->input->post('search_date');
            list($day, $month, $year) = explode('/', $this->input->post('search_date'));
            $search_date = "$year-$month-$day";
            $data['reports'] = $this->reports_model->get_onedaycancel_report($search_date);
        } else {
            $data['reports'] = $this->reports_model->get_onedaycancel_report();
        }
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('one_day_cancel_report', $data, TRUE);
        $layout_data['page_title'] = 'One Day Cancel Report';
        $layout_data['meta_description'] = 'One Day Cancel Report';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('base.js','bootstrap-datepicker.js', 'jquery.dataTables.min.js','shim.min.js','xlsx.full.min.js','Blob.js','FileSaver.js','oneday_cancel_report.js');
        $this->load->view('layouts/default', $layout_data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    function delete_onedaycancel()
    {
        if ($this->input->post('id')) {
            echo $this->reports_model->delete_onedaycancel($this->input->post('id'));
        }
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    function employee_work()
    {
        if (!user_permission(user_authenticate(), 17)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $reports = array();
        $search_conditions = array();
        if ($this->input->post('activity_report')) {
            if ($this->input->post('maid') != "" && $this->input->post('month') != "" && $this->input->post('year') != "") {
                $results = $this->reports_model->get_employee_work_report($this->input->post('maid'), $this->input->post('month'), $this->input->post('year'));
                $search_conditions = array(
                    'maid_id' => $this->input->post('maid'),
                    'month' => $this->input->post('month'),
                    'year' => $this->input->post('year')
                );
                //                if (!empty($results)) {
                //                    foreach ($results as $result) {
                //                        $reports[$result->service_date][$result->time_from]['time_to'] = $result->time_to;
                //                        $reports[$result->service_date][$result->time_from]['customer'] = $result->customer_name;
                //                        $reports[$result->service_date][$result->time_from]['type'] = $result->booking_type;
                //                        $time_diff = (strtotime($result->time_to) - strtotime($result->time_from)) / 3600;
                //                        if ($time_diff > 1) {
                //                            for ($j = 0; $j < $time_diff; $j++) {
                //                                $start_time = date('H:i:s', strtotime($result->time_from) + 3600);
                //                                $reports[$result->service_date][$start_time]['time_to'] = $result->time_to;
                //                                $reports[$result->service_date][$start_time]['customer'] = $result->customer_name;
                //                                $reports[$result->service_date][$start_time]['type'] = $result->booking_type;
                //                            }
                //                        }
                //                    }
                //                }
                if (!empty($results)) {
                    foreach ($results as $result) {
                        $reports[$result->service_date][$result->time_from]['time_from'] = $result->time_from;
                        $reports[$result->service_date][$result->time_from]['time_to'] = $result->time_to;
                        $reports[$result->service_date][$result->time_from]['customer'] = $result->customer_name;
                        $reports[$result->service_date][$result->time_from]['payment_type'] = $result->payment_type;
                        $reports[$result->service_date][$result->time_from]['type'] = $result->booking_type;
                        $time_diff = (strtotime($result->time_to) - strtotime($result->time_from)) / 3600;
                        $reports[$result->service_date][$result->time_from]['time_diff'] = $time_diff;
                        if ($time_diff > 1) {
                            for ($j = 0; $j < $time_diff; $j++) {
                                $reports[$result->service_date][$result->time_from]['time_from'] = $result->time_from;
                                $reports[$result->service_date][$result->time_from]['time_to'] = $result->time_to;
                                $reports[$result->service_date][$result->time_from]['customer'] = $result->customer_name;
                                $reports[$result->service_date][$result->time_from]['payment_type'] = $result->payment_type;
                                $reports[$result->service_date][$result->time_from]['type'] = $result->booking_type;
                                $reports[$result->service_date][$result->time_from]['time_diff'] = $time_diff;
                            }
                        }
                    }
                }
            }
        }
        $data['reports'] = $reports;
        $data['search_condition'] = $search_conditions;
        $data['maids'] = $this->reports_model->get_all_maids();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('employee_work_report', $data, TRUE);
        $layout_data['page_title'] = 'Employee Work Reports';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1';
        $layout_data['js_files'] = array('base.js','bootstrap-datepicker.js', 'jquery.dataTables.min.js','shim.min.js','xlsx.full.min.js','Blob.js','FileSaver.js','employee-work-report.js');
        $this->load->view('layouts/default', $layout_data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    function employee_work_all()
    {
        if (!user_permission(user_authenticate(), 17)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $reports = array();
        $search_conditions = array();
        if ($this->input->post('activity_report')) {
            if ($this->input->post('month') != "" && $this->input->post('year') != "") {
                $search_conditions = array(
                    'month' => $this->input->post('month'),
                    'year' => $this->input->post('year')
                );
            }
        } else {
            $currentMonth = date('m');
            $currentNewMonth = Date('m', strtotime($currentMonth . " last month"));
            $search_conditions = array(
                'month' => $currentNewMonth,
                'year' => date('Y')
            );
        }
        $reports = $this->reports_model->get_employee_work_all($search_conditions['month'], $search_conditions['year']);
        $source_ref = $this->reports_model->get_customer_refereces();
        $maid_ref = array();
        if (!empty($source_ref)) {
            foreach ($source_ref as $ref) {
                $maid_ref[$ref->customer_source_id] = $ref->reference;
            }
        }
        $data['month'] = $search_conditions['month'];
        $data['year'] = $search_conditions['year'];
        $data['reports'] = $reports;
        $data['search_condition'] = $search_conditions;
        $data['maids'] = $this->reports_model->get_all_maids();
        $data['maid_references'] = $maid_ref;
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('employee_work_report_all', $data, TRUE);
        $layout_data['page_title'] = 'All Employees Work Report';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1';
        $layout_data['js_files'] = array('base.js','bootstrap-datepicker.js', 'jquery.dataTables.min.js','shim.min.js','xlsx.full.min.js','Blob.js','FileSaver.js','employee-work-report-all.js');
        $this->load->view('layouts/default', $layout_data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    function activity_summary()
    {
        if (!user_permission(user_authenticate(), 21)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        if ($this->input->post('activity_report')) {
            $data['from_date'] = $this->input->post('from_date');
            $data['to_date'] = $this->input->post('to_date');
            list($day, $month, $year) = explode('/', $this->input->post('from_date'));
            $from_date = "$year-$month-$day";
            list($day, $month, $year) = explode('/', $this->input->post('to_date'));
            $to_date = "$year-$month-$day";
            //$reports = $this->reports_model->get_activity_summary_report($from_date, $to_date);
        } else {
            $to_date = date('Y-m-d');
            $from_date = date('Y-m-d', strtotime('-30 days', strtotime($to_date)));
            //$reports = $this->reports_model->get_activity_summary_report($from_date, $to_date);
        }
        $datetime1 = new DateTime($from_date);
        $datetime2 = new DateTime($to_date);
        $interval = $datetime1->diff($datetime2);
        $days = $interval->format('%a');
        $service_date = $from_date;
        $final_report = array();
        for ($i = 0; $i <= $days; ++$i) {
            $reports = $this->reports_model->get_activity_summary_report_by_date($service_date);
            if (!empty($reports)) {
                $data_to_check = "";
                $j = 1;
                $od_count = 0;
                $we_count = 0;
                $tot_hrs = 0;
                $od_tot_hrs = 0;
                $we_tot_hrs = 0;
                $ot_tot_hrs = 0;
                foreach ($reports as $report) {
                    if ($report->service_date == $data_to_check) {
                        $final_report[$report->service_date]['date'] = $report->service_date;
                        //$final_report[$report->service_date]['OD'] += $report->booking_type == 'OD' ? 1 : 0;
                        //$final_report[$report->service_date]['WE'] += $report->booking_type == 'WE' ? 1 : 0;
                        //$final_report[$report->service_date]['OD_hrs'] += $report->booking_type == 'OD' ? $report->Total_hours : 0;
                        //$final_report[$report->service_date]['WE_hrs'] += $report->booking_type == 'WE' ? $report->Total_hours : 0;
                        //$final_report[$report->service_date]['hours'] += $report->Total_hours;
                        if ($report->booking_type == 'OD') {
                            $od_count++;
                            $od_tot_hrs += $report->Total_hours;
                        } else if ($report->booking_type == 'WE' || $report->booking_type == 'BW') {
                            $we_count++;
                            $we_tot_hrs += $report->Total_hours;
                        }
                        $tot_hrs += $report->Total_hours;
                        //echo 'ctr='.$j.','.$final_report[$report->service_date]['OD'].'<br>';
                        //$final_report[$report->service_date]['payment'] = $this->reports_model->get_payment_by_date($report->service_date);
                    } else {
                        $activity_report[$report->service_date] = array();
                        $final_report[$report->service_date]['date'] = $report->service_date;
                        //$final_report[$report->service_date]['OD'] = $report->booking_type == 'OD' ? (isset($final_report[$report->service_date]['OD']) ? ($final_report[$report->service_date]['OD'] + 1) : 0) : 0;
                        //$final_report[$report->service_date]['WE'] = $report->booking_type == 'WE' ? (isset($final_report[$report->service_date]['WE']) ? ($final_report[$report->service_date]['WE'] + 1) : 0) : 0;
                        //$final_report[$report->service_date]['OD_hrs'] = $report->booking_type == 'OD' ? (isset($final_report[$report->service_date]['OD_hrs']) ? ($final_report[$report->service_date]['OD_hrs'] + 1) : 0) : 0;
                        //$final_report[$report->service_date]['WE_hrs'] = $report->booking_type == 'WE' ? (isset($final_report[$report->service_date]['WE_hrs']) ? ($final_report[$report->service_date]['WE_hrs'] + 1) : 0) : 0;
                        //$final_report[$report->service_date]['hours'] = isset($final_report[$report->service_date]['hours']) ? ($final_report[$report->service_date]['hours'] + $report->Total_hours ) : $report->Total_hours;
                        if ($report->booking_type == 'OD') {
                            $od_count++;
                            $od_tot_hrs += $report->Total_hours;
                        } else if ($report->booking_type == 'WE' || $report->booking_type == 'BW') {
                            $we_count++;
                            $we_tot_hrs += $report->Total_hours;
                        }
                        $tot_hrs += $report->Total_hours;
                        //echo 'ctr='.$j.','.$final_report[$report->service_date]['OD'].'<br>';
                        //$final_report[$report->service_date]['payment'] = $this->reports_model->get_payment_by_date($report->service_date);
                    }
                    /*if ($report->service_date == $data_to_check) {
                        $final_report[$report->service_date]['OD'] = $report->booking_type == 'OD' ? $report->hour_count : (isset($final_report[$report->service_date]['OD']) ? $final_report[$report->service_date]['OD'] : 0);
                        $final_report[$report->service_date]['WE'] = $report->booking_type == 'WE' ? $report->hour_count : (isset($final_report[$report->service_date]['WE']) ? $final_report[$report->service_date]['WE'] : 0);
                        $final_report[$report->service_date]['OD_hrs'] = $report->booking_type == 'OD' ? $report->Total_hours : (isset($final_report[$report->service_date]['OD_hrs']) ? $final_report[$report->service_date]['OD_hrs'] : 0);
                        $final_report[$report->service_date]['WE_hrs'] = $report->booking_type == 'WE' ? $report->Total_hours : (isset($final_report[$report->service_date]['WE_hrs']) ? $final_report[$report->service_date]['WE_hrs'] : 0);
                        $final_report[$report->service_date]['hours'] += $report->Total_hours;
                        //$final_report[$report->service_date]['payment'] = $this->reports_model->get_payment_by_date($report->service_date);
                    } else {
                        $activity_report[$report->service_date] = array();
                        $final_report[$report->service_date]['date'] = $report->service_date;
                        $final_report[$report->service_date]['OD'] = $report->booking_type == 'OD' ? $report->hour_count : (isset($final_report[$report->service_date]['OD']) ? $final_report[$report->service_date]['OD'] : 0);
                        $final_report[$report->service_date]['WE'] = $report->booking_type == 'WE' ? $report->hour_count : (isset($final_report[$report->service_date]['WE']) ? $final_report[$report->service_date]['WE'] : 0);
                        $final_report[$report->service_date]['OD_hrs'] = $report->booking_type == 'OD' ? $report->Total_hours : (isset($final_report[$report->service_date]['OD_hrs']) ? $final_report[$report->service_date]['OD_hrs'] : 0);
                        $final_report[$report->service_date]['WE_hrs'] = $report->booking_type == 'WE' ? $report->Total_hours : (isset($final_report[$report->service_date]['WE_hrs']) ? $final_report[$report->service_date]['WE_hrs'] : 0);
                        $final_report[$report->service_date]['hours'] = $report->Total_hours;
                        //$final_report[$report->service_date]['payment'] = $this->reports_model->get_payment_by_date($report->service_date);
                    }*/
                    $final_report[$report->service_date]['payment'] = $this->reports_model->get_payment_by_date($report->service_date);
                    $data_to_check = $report->service_date;
                    $j++;
                }
                $final_report[$service_date]['OD'] = $od_count;
                $final_report[$service_date]['WE'] = $we_count;
                $final_report[$service_date]['OD_hrs'] = $od_tot_hrs;
                $final_report[$service_date]['WE_hrs'] = $we_tot_hrs;
                $final_report[$service_date]['hours'] = $tot_hrs;
            } //echo $we_count.'<br/>';
            $service_date = date('Y-m-d', strtotime('+1 day', strtotime($service_date)));
        }
        //print_r(json_encode($final_report));exit();
        $data['reports'] = $final_report;
        $data['from_date'] = date('d/m/Y', strtotime($from_date));
        $data['to_date'] = date('d/m/Y', strtotime($to_date));
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('activity_summary_report', $data, TRUE);
        $layout_data['page_title'] = 'Activity Summary Reports';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1';
        $layout_data['js_files'] = array('base.js','bootstrap-datepicker.js', 'jquery.dataTables.min.js','shim.min.js','xlsx.full.min.js','Blob.js','FileSaver.js','activity-summary-report.js');
        $this->load->view('layouts/default', $layout_data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    function activity_summary_view()
    {
        if (!user_permission(user_authenticate(), 13)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $data['zone_report'] = array();
        $data['search'] = array();
        $date = "";
        $zone = "";
        $zone_name = "All";
        $day = strftime("%A", strtotime(date('Y-m-d')));
        $search = array('search_date' => $date, 'search_zone' => $zone, 'search_zone_name' => $zone_name, 'search_day' => $day);
        $data['search'] = $search;
        if ($this->input->post('zone_report')) {
            $date = $this->input->post('zone_date');
            if ($date != "") {
                list($day, $month, $year) = explode("/", $date);
                $zone_date = "$year-$month-$day";
                $day = strftime("%A", strtotime($zone_date));
            } else {
                $zone_date = "";
                $day = "";
            }
            $zone = $this->input->post('zones');
            if ($zone == "") {
                $zone_name = "All";
            } else {
                $zone_name = $this->reports_model->get_zone_name($zone);
            }
            $zone_result = $this->reports_model->get_activity_zone_report($zone_date, $zone);
            //echo "<pre>";
            //print_r($zone_result);
            $zone_report = array();
            foreach ($zone_result as $zone_res) {
                $firstTime = $zone_res->time_from;
                $lastTime = $zone_res->time_to;
                $firstTime = strtotime($firstTime);
                $lastTime = strtotime($lastTime);
                $timeDiff = $lastTime - $firstTime;
                $timedif = ($timeDiff / 60) / 60;
                //echo $timedif; 
                $zone_report['maid_id'] = $zone_res->maid_id;
                $zone_report['maid_name'] = $zone_res->maid_name;
                $zone_report['customer'] = $zone_res->customer_name;
                $zone_report['zone'] = $zone_res->zone_name;
                $zone_report['area'] = $zone_res->area_name;
                $zone_report['booking_type'] = $zone_res->booking_type;
                $zone_report['time_from'] = $zone_res->time_from;
                $zone_report['time_to'] = $zone_res->time_to;
                $zone_report['time_diff'] = $timedif;
                $data['zone_report'][] = $zone_report;
            }
            //echo "<pre>";
            //print_r($data['zone_report']);
            $search = array('search_date' => $date, 'search_zone' => $zone, 'search_zone_name' => $zone_name, 'search_day' => $day);
            $data['search'] = $search;
            $data['maids'] = $this->reports_model->get_maids_report($zone_date, $zone);
        } else {
            $date = $this->uri->segment(3); //date('d/m/Y');
            if ($date != "") {
                list($year, $month, $day) = explode("-", $date);
                $zone_date = "$year-$month-$day";
                $day = strftime("%A", strtotime($zone_date));
            } else {
                $zone_date = "";
                $day = "";
            }
            $zone = $this->input->post('zones');
            if ($zone == "") {
                $zone_name = "All";
            } else {
                $zone_name = $this->reports_model->get_zone_name($zone);
            }
            $zone_result = $this->reports_model->get_activity_zone_report($zone_date, $zone);
            $zone_report = array();
            foreach ($zone_result as $zone_res) {
                $firstTime = $zone_res->time_from;
                $lastTime = $zone_res->time_to;
                $firstTime = strtotime($firstTime);
                $lastTime = strtotime($lastTime);
                $timeDiff = $lastTime - $firstTime;
                $timedif = ($timeDiff / 60) / 60;
                //echo $timedif; 
                $zone_report['maid_id'] = $zone_res->maid_id;
                $zone_report['customer'] = $zone_res->customer_name;
                $zone_report['zone'] = $zone_res->zone_name;
                $zone_report['area'] = $zone_res->area_name;
                $zone_report['booking_type'] = $zone_res->booking_type;
                $zone_report['time_from'] = $zone_res->time_from;
                $zone_report['time_to'] = $zone_res->time_to;
                $zone_report['time_diff'] = $timedif;
                $data['zone_report'][] = $zone_report;
            }
            $search = array('search_date' => date('d/m/Y', strtotime($date)), 'search_zone' => $zone, 'search_zone_name' => $zone_name, 'search_day' => $day);
            $data['search'] = $search;
            $data['maids'] = $this->reports_model->get_maids_report($zone_date, $zone);
        }
        //$data['maids'] = $this->reports_model->get_all_maids();
        $data['zones'] = $this->settings_model->get_zones();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('activity_summary_view', $data, TRUE);
        $layout_data['page_title'] = 'Activity Summary Report View';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    function maidattendance()
    {
        if (!user_permission(user_authenticate(), 18)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        if ($this->input->post('attendance_date')) {
            $attendance_date = $this->input->post('attendance_date');
            $s_date = explode("/", $attendance_date);
            $date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        } else {
            $date = date('Y-m-d');
            $attendance_date = date('d/m/Y');
        }
        $data['attendance_date'] = $attendance_date;
        $data['attendance_report'] = $this->maids_model->get_maid_attendance_by_date($date);
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('maid_attendance_report', $data, TRUE);
        $layout_data['page_title'] = 'Staff Attendance Report';
        $layout_data['meta_description'] = 'Staff Attendance Report';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1';
        $layout_data['js_files'] = array('base.js', 'ajaxupload.3.5.js','bootstrap-datepicker.js', 'jquery.dataTables.min.js','shim.min.js','xlsx.full.min.js','Blob.js','FileSaver.js','maid-attendance-report.js');
        $this->load->view('layouts/default', $layout_data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    function driveractivity()
    {
        if (!user_permission(user_authenticate(), 20)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        if ($this->input->post('activity_date')) {
            $activity_date = $this->input->post('activity_date');
            $s_date = explode("/", $activity_date);
            $date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        } else {
            $date = date('Y-m-d');
            $activity_date = date('d/m/Y');
        }
        $data['activity_date'] = $activity_date;
        $data['driver_activity_report'] = $this->tablets_model->get_driver_activity_by_date($date);
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('driver_activity_report', $data, TRUE);
        $layout_data['page_title'] = 'Driver Activity Report';
        $layout_data['meta_description'] = 'Driver Activity Report';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1';
        $layout_data['js_files'] = array('base.js', 'ajaxupload.3.5.js', 'mymaids.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    /**==================================================================
	 * Maid attendance by vehicle
     * @author : Geethu
     * @date : 27-08-2015
	 ====================================================================*/
    function vehicleattendance()
    {
        if (!user_permission(user_authenticate(), 21)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        if ($this->input->post('vehicle_report')) {
            $search_date = $this->input->post('search_date');
            list($day, $month, $year) = explode('/', $search_date);
            $date = "$year-$month-$day";
            $reports = $this->maids_model->get_maid_vehicle_report($date);
        } else {
            $search_date = date('d/m/Y');
            $reports = $this->maids_model->get_maid_vehicle_report();
        }
        $zones = $this->zones_model->get_all_zones();
        $maid_attendance = array();
        $i = 0;
        $zone_id = 0;
        foreach ($reports as $rpt) {
            foreach ($zones as $zone) {
                if ($zone_id != $rpt->zone_id) {
                    $zone_id = $rpt->zone_id;
                    $i = 0;
                }
                $maid_attendance[$i][$zone_id]['maid_name'] = $rpt->maid_name;
                $maid_attendance[$i][$zone_id]['attendance_status'] = $rpt->attendance_status;
            }
            ++$i;
        }
        //echo '<pre>';
        //print_r($maid_attendance);
        //exit;
        $data = array();
        $data['maids'] = $this->maids_model->get_maids();
        $data['zones'] = $zones;
        $data['reports'] = $maid_attendance;
        $data['search_date'] = $search_date;
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('maid_vehicle_report', $data, TRUE);
        $layout_data['page_title'] = 'Vehicle Attendance Reports';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1';
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    function useractivity_new()
    {
        if (!user_permission(user_authenticate(), 20)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        if ($this->input->post('activity_date')) {
            $activity_date = $this->input->post('activity_date');
            $s_date = explode("/", $activity_date);
            $date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        } else {
            $date = date('Y-m-d');
            $activity_date = date('d/m/Y');
        }
        if ($this->input->post('activity_date_to')) {
            $activity_date_to = $this->input->post('activity_date_to');
            $s_date = explode("/", $activity_date_to);
            $date_to = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        } else {
            $date_to = NULL;
            $activity_date_to = date('d/m/Y');
        }
        $data['activity_date'] = $activity_date;
        $data['activity_date_to'] = $activity_date_to;
        $data['user_activity_report'] = $this->users_model->get_user_activity_by_date($date, $date_to);
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('user_activity_report', $data, TRUE);
        $layout_data['page_title'] = 'User Activity Report';
        $layout_data['meta_description'] = 'User Activity Report';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1';
        $layout_data['js_files'] = array('base.js', 'ajaxupload.3.5.js', 'mymaids.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    public function useractivity()
    {
        $data = array();
        //$data['active'] = 2;
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('user_activity_report_new', $data, TRUE);
        $layout_data['page_title'] = 'User Activity Report';
        $layout_data['meta_description'] = 'User Activity Report';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1';
        $layout_data['js_files'] = array('bootstrap-datepicker.js', 'jquery.dataTables.min.js', 'useractivity.js');
        $this->load->view('layouts/default', $layout_data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    public function list_ajax_useractivity_list()
    {
        $orders = array();
        $draw = $_POST['draw'];
        $start = $_POST['start'];
        $rowperpage = $_POST['length'];
        $columnIndex = $_POST['order'][0]['column'];
        $columnName = $_POST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_POST['order'][0]['dir'];
        $searchValue = $_POST['search']['value'];
        // Custom search filter
        $regdate = $_POST['regdate'];
        $regdateto = $_POST['regdateto'];
        $keywordsearch = $_POST['keywordsearch'];
        $recordsTotal = $this->users_model->count_all_activity();
        $recordsTotalFilter = $this->users_model->get_all_activitynew($regdate, $regdateto, $keywordsearch);
        $orders  = $this->users_model->get_all_newactivity($columnName, $columnSortOrder, $rowperpage, $start, $draw, $recordsTotal, $recordsTotalFilter, $regdate, $regdateto, $keywordsearch);
        echo json_encode($orders);
        exit();
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    function maidhours()
    {
        if (!user_permission(user_authenticate(), 20)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        if ($this->input->post('from_date')) {
            $activity_date = $this->input->post('from_date');
            $s_date = explode("/", $activity_date);
            $date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        } else {
            $date = date('Y-m-d', strtotime('-7 days'));
            $activity_date = date('d/m/Y', strtotime('-5 days'));
        }
        if ($this->input->post('to_date')) {
            $activity_date_to = $this->input->post('to_date');
            $s_date = explode("/", $activity_date_to);
            $date_to = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        } else {
            $date_to = date('Y-m-d');
            $activity_date_to = date('d/m/Y');
        }
        $data['activity_date'] = $activity_date;
        $data['activity_date_to'] = $activity_date_to;
        $data['maid_hours_report'] = $this->maids_model->get_maid_hours($date, $date_to);
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('maid_hours_report', $data, TRUE);
        $layout_data['page_title'] = 'Maid Hours Report';
        $layout_data['meta_description'] = 'Maid Hours Report';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1';
        $layout_data['js_files'] = array('base.js', 'ajaxupload.3.5.js', 'mymaids.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    function zone_activity_summary()
    {
        if (!user_permission(user_authenticate(), 21)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        if ($this->input->post('activity_report')) {
            $data['from_date'] = $this->input->post('from_date');
            $data['to_date'] = $this->input->post('to_date');
            list($day, $month, $year) = explode('/', $this->input->post('from_date'));
            $from_date = "$year-$month-$day";
            list($day, $month, $year) = explode('/', $this->input->post('to_date'));
            $to_date = "$year-$month-$day";
            $reports = $this->reports_model->get_zone_activity_summary_report($from_date, $to_date);
        } else {
            $to_date = date('Y-m-d');
            $from_date = date('Y-m-d', strtotime('-30 days', strtotime($to_date)));
            $reports = $this->reports_model->get_zone_activity_summary_report($from_date, $to_date);
        }
        if (!empty($reports)) {
            $data_to_check = "";
            $final_report = array();
            foreach ($reports as $report) {
                if ($report->zone_id == $data_to_check) {
                    $final_report[$report->zone_id]['OD'] = $report->booking_type == 'OD' ? $report->hour_count : (isset($final_report[$report->zone_id]['OD']) ? $final_report[$report->zone_id]['OD'] : 0);
                    $final_report[$report->zone_id]['WE'] = $report->booking_type == 'WE' ? $report->hour_count : (isset($final_report[$report->zone_id]['WE']) ? $final_report[$report->zone_id]['WE'] : 0);
                    $final_report[$report->zone_id]['OD_hrs'] = $report->booking_type == 'OD' ? $report->Total_hours : (isset($final_report[$report->zone_id]['OD_hrs']) ? $final_report[$report->zone_id]['OD_hrs'] : 0);
                    $final_report[$report->zone_id]['WE_hrs'] = $report->booking_type == 'WE' ? $report->Total_hours : (isset($final_report[$report->zone_id]['WE_hrs']) ? $final_report[$report->zone_id]['WE_hrs'] : 0);
                    $final_report[$report->zone_id]['hours'] += $report->Total_hours;
                    $final_report[$report->zone_id]['payment'] += $report->Total_payment;
                } else {
                    $activity_report[$report->zone_id] = array();
                    $final_report[$report->zone_id]['zone'] = $report->zone_name;
                    $final_report[$report->zone_id]['OD'] = $report->booking_type == 'OD' ? $report->hour_count : (isset($final_report[$report->zone_id]['OD']) ? $final_report[$report->zone_id]['OD'] : 0);
                    $final_report[$report->zone_id]['WE'] = $report->booking_type == 'WE' ? $report->hour_count : (isset($final_report[$report->zone_id]['WE']) ? $final_report[$report->zone_id]['WE'] : 0);
                    $final_report[$report->zone_id]['OD_hrs'] = $report->booking_type == 'OD' ? $report->Total_hours : (isset($final_report[$report->zone_id]['OD_hrs']) ? $final_report[$report->zone_id]['OD_hrs'] : 0);
                    $final_report[$report->zone_id]['WE_hrs'] = $report->booking_type == 'WE' ? $report->Total_hours : (isset($final_report[$report->zone_id]['WE_hrs']) ? $final_report[$report->zone_id]['WE_hrs'] : 0);
                    $final_report[$report->zone_id]['hours'] = $report->Total_hours;
                    $final_report[$report->zone_id]['payment'] = $report->Total_payment;
                }
                $data_to_check = $report->zone_id;
            }
        }
        $data['reports'] = $final_report;
        $data['from_date'] = date('d/m/Y', strtotime($from_date));
        $data['to_date'] = date('d/m/Y', strtotime($to_date));
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('zone_activity_summary_report', $data, TRUE);
        $layout_data['page_title'] = 'Zone Activity Summary Reports';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1';
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/   
    function booking_cancel()
    {
        if (!user_permission(user_authenticate(), 16)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        if ($this->input->post('ondeday_report')) {
            $data['search_date'] = $this->input->post('search_date');
            list($day, $month, $year) = explode('/', $this->input->post('search_date'));
            $search_date = "$year-$month-$day";
            $data['search_from_date'] = $search_date;
            $data['search_date_to'] = $this->input->post('search_date_to');
            list($day, $month, $year) = explode('/', $this->input->post('search_date_to'));
            $search_date_to = "$year-$month-$day";
            $data['search_to_date'] = $search_date_to;
            //$data['reports'] = $this->reports_model->get_bookingremarks_report($search_date,$search_date_to);
            $data['reports'] = $this->reports_model->get_bookingremarks_report_new($search_date, $search_date_to);
        } else {
            $data['search_from_date'] = date('Y-m-d');
            $data['search_to_date'] = date('Y-m-d');
            // $data['reports'] = $this->reports_model->get_bookingremarks_report();
            $data['reports'] = $this->reports_model->get_bookingremarks_report_new();
        }
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('booking_cancel_report', $data, TRUE);
        $layout_data['page_title'] = 'Booking Cancel Reports';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1';
        $layout_data['js_files'] = array('base.js','bootstrap-datepicker.js', 'jquery.dataTables.min.js','shim.min.js','xlsx.full.min.js','Blob.js','FileSaver.js','booking-cancel-report.js');
        $this->load->view('layouts/default', $layout_data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    function maid_leave_report()
    {
        $param = array();
        $data['leaveresults'] = "";
        if ($this->input->post('search')) {
            $data['maid_id']     =    $this->input->post('maid_id');
            $start_date = str_replace('/', '-', $this->input->post('start_date'));
            $end_date   = str_replace('/', '-', $this->input->post('end_date'));
            $data['startdate']   =    $this->input->post('start_date');
            $data['enddate']     =    $this->input->post('end_date');
            $data['start_date']  =    date('Y-m-d', strtotime($start_date));
            $data['end_date']    =   date('Y-m-d', strtotime($end_date));
            $data['leave_type']  =  $this->input->post('leave_type');
            $data['results'] = $this->maids_model->get_maid_leave_report_new($data);
            $data['leaveresults'] = $this->maids_model->get_maid_leave_reportresults($data);
        } else {
            $data['maid_id']     =    '';
            $start_date = date('Y-m-d');
            $end_date   = date('Y-m-d');
            $data['startdate']   =    date('d/m/Y');
            $data['enddate']     =    date('d/m/Y');
            $data['start_date']  =    $start_date;
            $data['end_date']    =   $end_date;
            $data['leave_type']  =  $this->input->post('leave_type');
            $data['results'] = $this->maids_model->get_maid_leave_report_new($data);
            // $data['results']=$this->maids_model->get_maid_leave_report($data);
            //print_r( $data['results']);die;
            $data['leaveresults'] = $this->maids_model->get_maid_leave_reportresults($data);
        }
        if ($this->input->post('action') == "add_leave") {
            $maid_id     =    $this->input->post('maid_id');
            $start_date = str_replace('/', '-', $this->input->post('start_date'));
            $end_date   = str_replace('/', '-', $this->input->post('end_date'));
            $start_date  =    date('Y-m-d', strtotime($start_date));
            $end_date    =   date('Y-m-d', strtotime($end_date));
            $begin = new DateTime($start_date);
            $end   = new DateTime($end_date);
            $leave_done = array();
            $leave_days = array();
            for ($i = $begin; $begin <= $end; $i->modify('+1 day')) {
                //echo $i->format("Y-m-d");
                $maid_fields = array();
                $maid_fields['maid_id'] = $maid_id;
                $maid_fields['leave_date'] = $i->format("Y-m-d");
                $maid_fields['leave_status'] = 1;
                $maid_fields['added_by'] = user_authenticate();
                $maid_fields['leave_type'] = $this->input->post('leave_type');
                $maid_fields['typeleaves'] = $this->input->post('leave_type_new');
                if (!in_array($i->format("Y-m-d"), $leave_days)) {
                    $leave_id = $this->maids_model->add_maid_leave($maid_fields);
                    array_push($leave_days, $i->format("Y-m-d"));
                    array_push($leave_done, $leave_id);
                }
            }
            if (!empty($leave_done)) {
                echo "success";
                exit();
            }
        }
        $data['maids'] = $this->maids_model->get_maids();
        $data['type'] = array("1" => "Full Day", "2" => "Half Day");
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('maid_leave_report', $data, TRUE);
        $layout_data['page_title'] = 'Staff Leave Reports';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('jquery.fancybox.css', 'datepicker.css', 'demo.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1';
        $layout_data['js_files'] = array('jquery.fancybox.pack.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js', 'maid_leave_report.js', 'leave_report.js');
        $this->load->view('layouts/default', $layout_data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/   
    function remove_leave()
    {
        $leave_id = $this->input->post('leave_id');
        $status = $this->input->post('leave_status');
        $status = $status == 1 ? 0 : 1;
        $this->maids_model->delete_leavereport($leave_id, $status);
        echo $status;
        exit;
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    function online_payment_report()
    {
        //         if(!user_permission(user_authenticate(), 15))
        //        {
        //            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        //        }
        $data['online_payment_reports'] = $this->reports_model->get_online_payments();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('online_payment_reports', $data, TRUE);
        $layout_data['page_title'] = 'Online Payment Reports';
        $layout_data['meta_description'] = 'Online Payment Reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        //        $layout_data['reports_active'] = '1';
        $layout_data['accounts_active'] = '1';
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/    
    public function rate_review()
    {
        $data['rate_report'] = array();
        $data['search'] = array();
        $date = "";
        $day = strftime("%A", strtotime(date('Y-m-d')));
        $search = array('search_date' => $date, 'search_day' => $day);
        $data['search'] = $search;
        if ($this->input->post('rate_report')) {
            //$date = $this->input->post('vehicle_date');
            $from_datee = $this->input->post('search_date_from');
            $to_datee = $this->input->post('search_date_to');
            if ($from_datee != "" && $to_datee != "") {
                list($day, $month, $year) = explode("/", $from_datee);
                $from_date = "$year-$month-$day";
                $from_day = strftime("%A", strtotime($from_date));
                list($day, $month, $year) = explode("/", $to_datee);
                $to_date = "$year-$month-$day";
                $to_day = strftime("%A", strtotime($to_date));
            } else {
                $from_date = "";
                $to_date = "";
                $from_day = "";
                $to_day = "";
            }
            $rate_result = $this->reports_model->get_rate_reports($from_date, $to_date);
            $data['rate_report'] = $rate_result;
            $search = array('search_date_from' => $from_datee, 'search_date_to' => $to_datee, 'search_day_from' => $from_day, 'search_day_to' => $to_day);
            $data['search'] = $search;
        } else {
            $from_datee = date('d/m/Y', strtotime('-7 days'));
            $to_datee = date('d/m/Y');
            if ($from_datee != "" && $to_datee != "") {
                list($day, $month, $year) = explode("/", $from_datee);
                $from_date = "$year-$month-$day";
                $from_day = strftime("%A", strtotime($from_date));
                list($day, $month, $year) = explode("/", $to_datee);
                $to_date = "$year-$month-$day";
                $to_day = strftime("%A", strtotime($to_date));
            } else {
                $from_date = "";
                $to_date = "";
                $from_day = "";
                $to_day = "";
            }
            $rate_result = $this->reports_model->get_rate_reports($from_date, $to_date);
            $data['rate_report'] = $rate_result;
            $search = array('search_date_from' => $from_datee, 'search_date_to' => $to_datee, 'search_day_from' => $from_day, 'search_day_to' => $to_day);
            $data['search'] = $search;
        }
        // echo '<pre>';
        // print_r($data);
        // exit();
        //$data['zones'] = $this->settings_model->get_zones();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('rate_review', $data, TRUE);
        $layout_data['page_title'] = 'Rate & Review';
        $layout_data['meta_description'] = 'Rate Review';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    function call_report()
    {
        if ($this->input->post('search_date') && strlen($this->input->post('search_date')) > 0) {
            $payment_date = trim($this->input->post('search_date'));
        } else {
            $payment_date = date('d/m/Y');
        }
        $date = date('Y-m-d', strtotime(str_replace("/", "-", $payment_date)));
        if ($this->input->post('call_type') && strlen($this->input->post('call_type')) > 0) {
            $call_type = trim($this->input->post('call_type'));
        } else {
            $call_type = 0;
        }
        $reports = $this->reports_model->get_call_report_new($date, $customer_id = NULL, $call_type);
        $data = array();
        $data['reports'] = $reports;
        $data['payment_date'] = $payment_date;
        $data['call_type'] = $call_type;
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('call_report', $data, TRUE);
        $layout_data['page_title'] = 'Call Reports';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1';
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    public function schedule_report_nnn()
    {
        if ($this->input->post('search_date') && strlen($this->input->post('search_date')) > 0) {
            $payment_date = trim($this->input->post('search_date'));
        } else {
            $payment_date = date('d/m/Y');
        }
        $date = date('Y-m-d', strtotime(str_replace("/", "-", $payment_date)));
        $reports = $this->reports_model->get_schedule_report($date);
        // echo '<pre>';
        // print_r($reports);
        // exit();
        $data = array();
        $data['reports'] = $reports;
        $data['payment_date'] = $payment_date;
        $data['servicedate'] = $date;
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('schedule_report', $data, TRUE);
        $layout_data['page_title'] = 'Schedule Reports';
        $layout_data['meta_description'] = 'Schedule reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1';
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    public function schedule_report()
    {
        if ($this->input->post('search_date') && strlen($this->input->post('search_date')) > 0) {
            $payment_date = trim($this->input->post('search_date'));
        } else {
            $payment_date = date('d/m/Y');
        }
        $date = date('Y-m-d', strtotime(str_replace("/", "-", $payment_date)));
        //$date = "2020-07-01";
        $reports = $this->reports_model->get_schedule_report_lat($date);
        //echo $this->db->last_query();
        $data = array();
        $data['reports'] = $reports;
        $data['payment_date'] = $payment_date;
        $data['servicedate'] = $date;
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('schedule_report', $data, TRUE);
        $layout_data['page_title'] = 'Schedule Reports';
        $layout_data['meta_description'] = 'Schedule reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1';
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    public function schedule_report_new()
    {
        $data = array();
        $schedule_report = array();
        if ($this->input->post('search_date') && strlen($this->input->post('search_date')) > 0) {
            $payment_date = trim($this->input->post('search_date'));
        } else {
            $payment_date = date('d/m/Y');
        }
        if ($this->input->post('search_date_to') && strlen($this->input->post('search_date_to')) > 0) {
            $payment_date_to = trim($this->input->post('search_date_to'));
        } else {
            $payment_date_to = date('d/m/Y');
        }
        $date = date('Y-m-d', strtotime(str_replace("/", "-", $payment_date)));
        $date_to = date('Y-m-d', strtotime(str_replace("/", "-", $payment_date_to)));
        if ($this->input->post('zones')) {
            $zone_id = $this->input->post('zones');
        } else {
            $zone_id = "";
        }
        $data['payment_date'] = $payment_date;
        $data['payment_date_to'] = $payment_date_to;
        $data['servicedate'] = $date;
        $data['servicedateto'] = $date_to;
        while (strtotime($date) <= strtotime($date_to)) {
            $per_report = $this->reports_model->get_schedule_report_latest($date, $zone_id);
            array_push($schedule_report, $per_report);
            $date = date('Y-m-d', strtotime("+1 day", strtotime($date)));
        }
        //$reports = $this->reports_model->get_schedule_report_lat($date);
        $data['reports'] = $schedule_report;
        $data['zones'] = $this->settings_model->get_zones();
        $data['zone_id'] = $zone_id;
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        // $layout_data['content_body'] = $this->load->view('schedule_report_new', $data, TRUE);
        $layout_data['content_body'] = $this->load->view('schedule_report_latest', $data, TRUE);
        $layout_data['page_title'] = 'Schedule Reports';
        $layout_data['meta_description'] = 'Schedule reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1';
        $layout_data['js_files'] = array('base.js','bootstrap-datepicker.js', 'jquery.dataTables.min.js','schedule-report.js');
        $this->load->view('layouts/default', $layout_data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    public function schedule_reportnew()
    {
        if ($this->input->post('search_date') && strlen($this->input->post('search_date')) > 0) {
            $payment_date = trim($this->input->post('search_date'));
        } else {
            $payment_date = date('d/m/Y');
        }
        $date = date('Y-m-d', strtotime(str_replace("/", "-", $payment_date)));
        //$date = "2020-07-01";
        $reports = $this->reports_model->get_schedule_report_latnew($date);
        $data = array();
        $data['reports'] = $reports;
        $data['payment_date'] = $payment_date;
        $data['servicedate'] = $date;
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('schedule_report_new', $data, TRUE);
        $layout_data['page_title'] = 'Schedule Reports';
        $layout_data['meta_description'] = 'Schedule reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1';
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    public function schedule_report_excelnew()
    {
        $service_date = $this->uri->segment(3);
        // $reports = $this->reports_model->get_schedule_report($service_date);
        $reports = $this->reports_model->get_schedule_report_latnew($service_date);
        $data = array();
        $data['reports'] = $reports;
        $this->load->view('schedule_report_spreadsheetviewnew', $data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    public function schedule_report_excel()
    {
        $service_date = $this->uri->segment(3);
        // $reports = $this->reports_model->get_schedule_report($service_date);
        $reports = $this->reports_model->get_schedule_report_lat($service_date);
        $data = array();
        $data['reports'] = $reports;
        $this->load->view('schedule_report_spreadsheetview', $data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    public function schedule_report_excel_new()
    {
        $service_date = $this->uri->segment(3);
        $service_date_to = $this->uri->segment(4);
        $zone_id = $this->uri->segment(5);
        //$rpr=$this->reports_model->get_schedule_report_lat($service_date);
        $schedule_report = array();
        while (strtotime($service_date) <= strtotime($service_date_to)) {
            $per_report = $this->reports_model->get_schedule_report_latest($service_date, $zone_id);
            array_push($schedule_report, $per_report);
            $service_date = date('Y-m-d', strtotime("+1 day", strtotime($service_date)));
        }
        $data = array();
        $data['file_name'] = 'Schedule Report _ ' . $service_date . ' - ' . $service_date_to . '.xls';
        $data['reports'] = $schedule_report;
        // $this->load->view('schedule_report_spreadsheetview_2', $data);
        $this->load->view('schedule_report_spreadsheetview_3', $data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    public function booking_cancel_excel()
    {
        $search_date = $this->uri->segment(3);
        $search_date_to = $this->uri->segment(4);
        //$data['reports'] = $this->reports_model->get_bookingremarks_report($search_date,$search_date_to);
        $data['reports'] = $this->reports_model->get_bookingremarks_report_new($search_date, $search_date_to);
        $this->load->view('booking_cancel_excelsheet', $data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/
    public  function complaint_report()
    {
        // echo 'God is Love';
        $data['rate_report'] = array();
        $data['search'] = array();
        $date = "";
        $day = strftime("%A", strtotime(date('Y-m-d')));
        $search = array('search_date' => $date, 'search_day' => $day);
        $data['search'] = $search;
        if ($this->input->post('rate_report')) {
            //$date = $this->input->post('vehicle_date');
            $from_datee = $this->input->post('search_date_from');
            $to_datee = $this->input->post('search_date_to');
            // print_r($from_datee);
            // print_r($to_datee); die;
            if ($from_datee != "" && $to_datee != "") {
                list($day, $month, $year) = explode("/", $from_datee);
                $from_date = "$year-$month-$day";
                $from_day = strftime("%A", strtotime($from_date));
                list($day, $month, $year) = explode("/", $to_datee);
                $to_date = "$year-$month-$day";
                $to_day = strftime("%A", strtotime($to_date));
            } else {
                $from_date = "";
                $to_date = "";
                $from_day = "";
                $to_day = "";
            }
            $rate_result = $this->reports_model->get_complaints_reports($from_date, $to_date);
            $data['rate_report'] = $rate_result;
            $search = array('search_date_from' => $from_datee, 'search_date_to' => $to_datee, 'search_day_from' => $from_day, 'search_day_to' => $to_day);
            $data['search'] = $search;
        } else {
            $from_datee = date('d/m/Y', strtotime('-7 days'));
            $to_datee = date('d/m/Y');
            if ($from_datee != "" && $to_datee != "") {
                list($day, $month, $year) = explode("/", $from_datee);
                $from_date = "$year-$month-$day";
                $from_day = strftime("%A", strtotime($from_date));
                list($day, $month, $year) = explode("/", $to_datee);
                $to_date = "$year-$month-$day";
                $to_day = strftime("%A", strtotime($to_date));
            } else {
                $from_date = "";
                $to_date = "";
                $from_day = "";
                $to_day = "";
            }
            $rate_result = $this->reports_model->get_complaints_reports($from_date, $to_date);
            $data['rate_report'] = $rate_result;
            //print_r($data['rate_report']); die;
            $search = array('search_date_from' => $from_datee, 'search_date_to' => $to_datee, 'search_day_from' => $from_day, 'search_day_to' => $to_day);
            $data['search'] = $search;
        }
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('complaints_report', $data, TRUE);
        $layout_data['page_title'] = 'Complaints Report';
        $layout_data['meta_description'] = 'Complaints Report';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/   
    public function invoicereporttoExcel()
    {
        $from = $this->uri->segment(3) == 'nil' ? null : date('Y-m-d', strtotime(str_replace("/", "-", $this->uri->segment(3))));
        $to = $this->uri->segment(4) == 'nil' ? null : date('Y-m-d', strtotime(str_replace("/", "-", $this->uri->segment(4))));
        $c_id = $this->uri->segment(5) == 'nil' ? null : $this->uri->segment(5);
        $status = $this->uri->segment(6) == 'nil' ? null : $this->uri->segment(6);
        $this->load->model('invoice_model');
        $data['invoice_report'] = $this->invoice_model->get_customer_invoices($from, $to, $c_id, $status);
        $this->load->view('invoicereporttoExcel', $data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/   
    function customerpaymentreporttoExcel()
    {
        $from = $this->uri->segment(3) == 'nil' ? null : date('Y-m-d', strtotime(str_replace("/", "-", $this->uri->segment(3))));
        $to = $this->uri->segment(4) == 'nil' ? null : date('Y-m-d', strtotime(str_replace("/", "-", $this->uri->segment(4))));
        $c_id = $this->uri->segment(5) == 'nil' ? null : $this->uri->segment(5);
        $company = $this->uri->segment(6) == 'nil' ? null : $this->uri->segment(6);
        $type = $this->uri->segment(7) == 'nil' ? null : $this->uri->segment(7);
        $this->load->model('customers_model');
        $data['payment_report'] = $this->customers_model->get_customer_payments($from, $to, $c_id, $company, $type);
        $this->load->view('customerpaymentreporttoExcel', $data);
    }
    /**==================================================================
	 * 
	 ====================================================================*/    
    function customerbookinghours()
    {
        if ($this->input->post('from_date')) {
            $activity_date = $this->input->post('from_date');
            $s_date = explode("/", $activity_date);
            $date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        } else {
            $date = date('Y-m-d', strtotime('-7 days'));
            $activity_date = date('d/m/Y', strtotime('-5 days'));
        }
        if ($this->input->post('to_date')) {
            $activity_date_to = $this->input->post('to_date');
            $s_date = explode("/", $activity_date_to);
            $date_to = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        } else {
            $date_to = date('Y-m-d');
            $activity_date_to = date('d/m/Y');
        }
        $data['activity_date'] = $activity_date;
        $data['activity_date_to'] = $activity_date_to;
        $data['maid_hours_report'] = $this->maids_model->get_custmer_maid_hours($date, $date_to);
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('customer_hours_report', $data, true);
        $layout_data['page_title'] = 'Customer Hours Report';
        $layout_data['meta_description'] = 'Customer Hours Report';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1';
        $layout_data['js_files'] = array('base.js', 'ajaxupload.3.5.js', 'mymaids.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
	function invoice_report()
    {
        // Author : Samnad. S
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        /************************************************************** */
        $data['filter_from_date'] = $this->input->post('filter_from_date') ?: date('01/m/Y');
        $data['filter_to_date'] = $this->input->post('filter_to_date') ?: date('t/m/Y');
        /************************************************************** */
        $data['invoices'] = $this->db->select('i.*,c.customer_name')
            ->from('invoice as i')
            ->join('customers as c','i.customer_id = c.customer_id','left')
            ->where('i.invoice_date >=', DateTime::createFromFormat('d/m/Y', $data['filter_from_date'])->format('Y-m-d'))
            ->where('i.invoice_date <=', DateTime::createFromFormat('d/m/Y', $data['filter_to_date'])->format('Y-m-d'))
            // ->where('i.invoice_status', 3)
			->where('i.invoice_status !=', 2)
			->where('i.invoice_status !=', 0)
            ->order_by('i.invoice_date', 'ASC')->get()->result_array();
        //echo'<pre>';print_r($data['invoices']);echo'<pre>';die();
        /************************************************************** */
        $layout_data['content_body'] = $this->load->view('reports/invoice_report', $data, true);
        $layout_data['page_title'] = 'Invoice Report';
        $layout_data['meta_description'] = 'Invoice Report';
        $layout_data['css_files'] = array('demo.css', 'datepicker.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.dataTables.min.js', 'bootstrap-datepicker.js');
        $this->load->view('layouts/default', $layout_data);
    }
	
    function customer_sales_report()
    {
        // Author : Samnad. S
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        /************************************************************** */
        $data['filter_from_date'] = $this->input->post('filter_from_date') ?: date('01/m/Y');
        $data['filter_to_date'] = $this->input->post('filter_to_date') ?: date('t/m/Y');
        /************************************************************** */
        $data['rows'] = $this->db->select('i.customer_id,c.customer_name,sum(i.invoice_total_amount) as invoice_total_amount,sum(i.invoice_tax_amount) as invoice_tax_amount,sum(i.invoice_net_amount) as invoice_net_amount')
            ->from('invoice as i')
            ->join('customers as c','i.customer_id = c.customer_id','left')
            ->where('i.invoice_date >=', DateTime::createFromFormat('d/m/Y', $data['filter_from_date'])->format('Y-m-d'))
            ->where('i.invoice_date <=', DateTime::createFromFormat('d/m/Y', $data['filter_to_date'])->format('Y-m-d'))
            // ->where('i.invoice_status', 3)
			->where('i.invoice_status !=', 2)
			->where('i.invoice_status !=', 0)
            ->order_by('c.customer_name', 'ASC')
            ->group_by('i.customer_id')
            ->get()->result_array();
        //echo'<pre>';print_r($data['rows']);echo'<pre>';die();
        /************************************************************** */
        $layout_data['content_body'] = $this->load->view('reports/customer_sales_report', $data, true);
        $layout_data['page_title'] = 'Customer Sales Report';
        $layout_data['meta_description'] = 'Customer Sales Report';
        $layout_data['css_files'] = array('demo.css', 'datepicker.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.dataTables.min.js', 'bootstrap-datepicker.js');
        $this->load->view('layouts/default', $layout_data);
    }
	public function schedule_report_service_wise()
    {
        $data = array();
        $schedule_report = array();
        if ($this->input->post('search_date') && strlen($this->input->post('search_date')) > 0) {
            $payment_date = trim($this->input->post('search_date'));
        } else {
            $payment_date = date('d/m/Y');
        }
        if ($this->input->post('search_date_to') && strlen($this->input->post('search_date_to')) > 0) {
            $payment_date_to = trim($this->input->post('search_date_to'));
        } else {
            $payment_date_to = date('d/m/Y');
        }
        $date = date('Y-m-d', strtotime(str_replace("/", "-", $payment_date)));
        $date_to = date('Y-m-d', strtotime(str_replace("/", "-", $payment_date_to)));
        if ($this->input->post('zones')) {
            $zone_id = $this->input->post('zones');
        } else {
            $zone_id = "";
        }
        $data['payment_date'] = $payment_date;
        $data['payment_date_to'] = $payment_date_to;
        $data['servicedate'] = $date;
        $data['servicedateto'] = $date_to;
        $data['filter_service_type_id'] =  $this->input->post('filter_service_type_id') ?: null;
        $data['filter_maid_id'] =  $this->input->post('filter_maid_id') ?: null;
        while (strtotime($date) <= strtotime($date_to)) {
            $per_report = $this->reports_model->schedule_report_service_wise($date, $zone_id,$data['filter_service_type_id'],$data['filter_maid_id']);
            array_push($schedule_report, $per_report);
            $date = date('Y-m-d', strtotime("+1 day", strtotime($date)));
        }
        $rows = [];
        foreach($schedule_report as $key1 => $date_report){
            foreach($date_report as $key2 => $service){
                //echo '<pre>'; print_r($service->total_amount);echo '</pre>';die();
                if($rows[$service->service_type_id]){
                    $rows[$service->service_type_id]['total_amount'] = $rows[$service->service_type_id]['total_amount'] + $service->total_amount;
                }
                else{
                    $rows[$service->service_type_id] = array('service_type_id' => $service->service_type_id,'service_type_name' => $service->service_type_name,'total_amount' => $service->total_amount);
                }
            }
        }
        $data['rows'] = $rows;
        $data['maids'] = $this->reports_model->get_all_maids();
        //echo '<pre>'; print_r($data['maids']);echo '</pre>';die();
        //$reports = $this->reports_model->get_schedule_report_lat($date);
        $data['reports'] = $schedule_report;
        $data['zones'] = $this->settings_model->get_zones();
        $data['zone_id'] = $zone_id;
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $data['service_types'] = $this->settings_model->get_services();
        $layout_data['content_body'] = $this->load->view('reports/schedule_report_service_wise', $data, TRUE);
        $layout_data['page_title'] = 'Schedule Report Service Wise';
        $layout_data['meta_description'] = 'Schedule Report Service Wise';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('bootstrap-datepicker.js', 'jquery.dataTables.min.js','main.js');
        $this->load->view('layouts/default', $layout_data);
    }
	
	public function deletedbooking()
	{
		$date_from = "2023-12-01";
		$date_to = "2023-12-31";
		$deletedbookings = $this->maids_model->get_deleted_bookings($date_from,$date_to);
		
		$deletearray = array();
		foreach($deletedbookings as $val)
		{
			$getdayservices = $this->maids_model->getdayservices($val->booking_id,$val->service_date);
			if(!empty($getdayservices))
			{
				$dataaraay = array();
				$dataaraay['service_status'] = 3;
				$dataaraay['odoo_package_activity_status'] = 1;
				$updatearray = $this->maids_model->updatedayservice($dataaraay,$getdayservices->day_service_id);
				array_push($deletearray,$getdayservices);
			}
		}
		
		echo "<pre>";
		print_r($deletearray);
		exit();
	}
}