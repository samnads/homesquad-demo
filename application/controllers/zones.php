<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Zones extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
            if(!is_user_loggedin())
            {
		redirect('logout');
            }
            if(!user_permission(user_authenticate(), 7))
            {
                show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
            }
            $this->load->model('zones_model');
            //$this->load->helper('odoo_helper');
	
    }
    public function index()
    {
        //$data = array();
        if($this->input->post('zone_sub'))
        {
            $zone_name = $this->input->post('zonename');
            $drivername = $this->input->post('drivername');
            $spare = $this->input->post('spare');

            $data = array(
                'zone_name' => $zone_name,
                'driver_name' => $drivername,
                'spare_zone' => $spare,
                'zone_status' => 1,
            );
            $this->zone_model->add_zones($data);
            //print_r($this->db->error());
        }
        $data['zones'] = $this->zone_model->get_zones();
        $layout_data['content_body'] = $this->load->view('zone_list', $data, TRUE);
	$layout_data['page_title'] = 'Zones';
	$layout_data['meta_description'] = 'Zones';
	$layout_data['css_files'] = array();
	$layout_data['external_js_files'] = array();
	$layout_data['js_files'] = array('mymaids.js');
	$this->load->view('layouts/default', $layout_data);
    }
//    public function add_zone()
//    {
//        $data['zones'] = $this->zone_model->get_zones();
//        $layout_data['content_body'] = $this->load->view('zone_list', $data, TRUE);
//	$layout_data['page_title'] = 'Zones';
//	$layout_data['meta_description'] = 'Zones';
//	$layout_data['css_files'] = array();
//	$layout_data['external_js_files'] = array();
//	$layout_data['js_files'] = array('mymaids.js');
//        
//        
//        $zone_name = $this->input->post('zonename');
//        $drivername = $this->input->post('drivername');
//        $spare = $this->input->post('spare');
//        
//        $data = array(
//            'zone_name' => $zone_name,
//            'driver_name' => $drivername,
//            'spare_zone' => $spare,
//            'zone_status' => 1,
//        );
//        $this->zone_model->add_zones($data);
//        
//	$this->load->view('layouts/default', $layout_data);
//    }
    /*
    public function export_to_odoo()
    {
           $user_id = odoo_login();         
            //Zones
            $zones = $this->zones_model->get_all_zones();
            
            foreach ($zones as $zone)
            {
                    $fields = array(
                        'name' => new xmlrpcval($zone->zone_name, "string"),
                        'driver' => new xmlrpcval($zone->driver_name, "string"),
                        'spare' => new xmlrpcval($zone->spare_zone == 'Y' ? TRUE : FALSE, "boolean"),
                        );
                    $response = odoo_operation($user_id, "create", "res.zone", $fields, NULL); 
                    $this->zones_model->update_zone($zone->zone_id, array('odoo_zone_id' => $response));
                    //echo '<pre>';print_r($response);exit();
            }
                   
    }
    */
    public function odoo_search($key, $value, $model, $user_id)
    {
        
        $domain_filter = array ( 
                        new xmlrpcval(
                            array(new xmlrpcval($key , "string"), 
                                  new xmlrpcval('=',"string"), 
                                  new xmlrpcval($value,"string")
                                  ),"array"             
                            ),
                        ); 
        
        $field_list = array(
                            new xmlrpcval("id", "int"),                                
                        ); 
        
        $odoo_id = search($user_id, $model,$domain_filter, $field_list);
        
        return $odoo_id;
        
    }
    public function export_to_odoo()
    {
           $user_id = odoo_login();         
            //Zones
            $zones = $this->zones_model->get_all_zones();
            
            foreach ($zones as $zone)
            {
                if($flat['odoo_flat_id'])
                    {
                            $field = 'id';
                            $value = $zone->odoo_zone_id;                        
                    }
                    else
                    {                            
                            $field = 'name';
                            $value = $zone->zone_name;
                    }
                                
                   
                    $odoo_id = $this->odoo_search($field, $value, "res.flat",$user_id);
                    
                    if($odoo_id && $odoo_id > 0)
                    {
                         $fields = array(
                                'name' => new xmlrpcval($zone->zone_name, "string"),
                                'driver_name' => new xmlrpcval($zone->driver_name, "string"),
                                'spare' => new xmlrpcval($zone->spare_zone == 'Y' ? TRUE : FALSE, "boolean"),
                                );
                        odoo_operation($user_id, "write", "res.zone", $fields, $odoo_id);
                        $odoo_zone_id = $odoo_id;
                    }
                    else
                    {
                         $fields = array(
                                'name' => new xmlrpcval($zone->zone_name, "string"),
                                'driver_name' => new xmlrpcval($zone->driver_name, "string"),
                                'spare' => new xmlrpcval($zone->spare_zone == 'Y' ? TRUE : FALSE, "boolean"),
                                );
                        $odoo_zone_id = odoo_operation($user_id, "create", "res.zone", $fields, NULL); 
                    }
                   
                    $this->zones_model->update_zone($zone->zone_id, array('odoo_zone_id' => $odoo_zone_id));
                    //echo '<pre>';print_r($response);exit();
            }
                   
    }
}