<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Booking_edit extends CI_Controller {
	
    public function __construct()
    {
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        $this->load->model('edit_booking_model');
        $this->load->model('settings_model');
		$this->load->model('booking_cleaning_supplies_model',"b_cleaning_supplies_m");
        $this->load->model('config_cleaning_supplies_model',"c_cleaning_supplies_m");
    }
	
	public function edit_past_booking()
	{
		$service_data = [];
		$mainservice_array = array();
		$curdatetime = date('Y-m-d H:i:s');
		
		$times = array();
        $current_hour_index = 0;
        $time = '12:00 am';
        $time_stamp = strtotime($time);
        for ($i = 0; $i < 48; $i++) {
            if (!isset($times['t-' . $i])) {
                $times['t-' . $i] = new stdClass();
            }
            $times['t-' . $i]->stamp = $time_stamp;
            $times['t-' . $i]->display = $time;
            if (date('H') == $i)
            {
                $current_hour_index = 't-' . (($i * 2) - 1);
            }
            //For 30 min
            $time_stamp = strtotime('+30mins', strtotime($time));
            $time = date('g:i a', $time_stamp);
        }
		$data['times'] = $times;
		$data['customerId'] = "";
		$data['servicedate'] = "";
		$data['cleaning_supplies'] = $this->c_cleaning_supplies_m->get_all_supplies();
		$data['customerlist'] = $this->edit_booking_model->get_all_customers();
		$data['stafflist'] = $this->edit_booking_model->get_all_staff();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        // $data['service_data'] = $mainservice_array;
        $layout_data['content_body'] = $this->load->view('booking/edit_past_booking', $data, TRUE);
        $layout_data['page_title'] = 'Edit Booking';
        $layout_data['meta_description'] = 'Edit Booking';
        $layout_data['css_files'] = array('jquery.fancybox.css', 'datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['accounting_active'] = '1'; 
        $layout_data['js_files'] = array('jquery.fancybox.pack.js', 'base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js','booking-edit-invoice.js');
        $this->load->view('layouts/default', $layout_data);
	}
	
	public function booking_by_servicedate()
	{
		$orders = array();
        $draw = $_POST['draw'];
        $start = $_POST['start'];
        $rowperpage = $_POST['length'];
		
		$service_date = $this->input->post('service_date');
        $customerid = $this->input->post('customerid');
        $maidid = $this->input->post('maidid');
		$orders = $this->edit_booking_model->get_bookings_by_servicedate($rowperpage, $start, $draw, $service_date, $customerid, $maidid);
		
		echo json_encode($orders);
        exit();
	}
	
	public function service_by_servicedate()
	{
		$orders = array();
        $draw = $_POST['draw'];
        $start = $_POST['start'];
        $rowperpage = $_POST['length'];
		
		$service_date = $this->input->post('service_date');
        $customerid = $this->input->post('customerid');
        $maidid = $this->input->post('maidid');
		$orders = $this->edit_booking_model->get_dailyactivities_by_servicedate($rowperpage, $start, $draw, $service_date, $customerid, $maidid);
		
		echo json_encode($orders);
        exit();
	}
	
	public function listinvoices()
	{
		$orders = array();
        $draw = $_POST['draw'];
        $start = $_POST['start'];
        $rowperpage = $_POST['length'];
		
		$invoiceid = $this->input->post('invoiceid');
        // $serviceid = $this->input->post('serviceid');
		$orders = $this->edit_booking_model->get_invoices_by_serviceid($rowperpage, $start, $draw, $invoiceid);
		
		echo json_encode($orders);
        exit();
	}
	
	public function paymentlist()
	{
		$orders = array();
        $draw = $_POST['draw'];
        $start = $_POST['start'];
        $rowperpage = $_POST['length'];
		
		$invoiceid = $this->input->post('invoiceid');
        // $serviceid = $this->input->post('serviceid');
		$orders = $this->edit_booking_model->get_payments_by_invoiceid($rowperpage, $start, $draw, $invoiceid);
		
		echo json_encode($orders);
        exit();
	}
	
	public function get_booking_detail()
	{
		$booking_id = $this->input->post("booking_id");
		$service_date = $this->input->post("service_date");
		$bookingdetail = $this->edit_booking_model->get_booking_detail($booking_id);
		$bookingdetail->time_from = strtotime($bookingdetail->time_from);
		$bookingdetail->time_to = strtotime($bookingdetail->time_to);
		echo json_encode($bookingdetail);
		exit();
	}
	
	public function update_booking_data()
	{
		if (user_authenticate() != 1) 
		{
			if ($this->input->post('edit_booking_lock') == 1 && $this->input->post('edit_booked_by') != user_authenticate()) {
				echo 'locked';
				exit();
			}
		}
		if ($this->input->post('edit_booking_id') && is_numeric($this->input->post('edit_booking_id')) && $this->input->post('edit_booking_id') > 0 && $this->input->post('time_from') && is_numeric($this->input->post('time_from')) && $this->input->post('time_from') > 0 && $this->input->post('time_to') && is_numeric($this->input->post('time_to')) && $this->input->post('time_to') > 0 && $this->input->post('time_from') < $this->input->post('time_to'))
		{
			$booking_id = $this->input->post('edit_booking_id');
			$time_from = date('H:i', trim($this->input->post('time_from')));
			$time_to = date('H:i', trim($this->input->post('time_to')));
			$cleaning_material = $this->input->post('cleaning_materials');
			$supervisor = $this->input->post('supervisor_selected');
			$isfree = $this->input->post('free_service');
			$priceperhr = $this->input->post('rate_per_hr') ?: 0;
			$discountpriceperhr = $this->input->post('discount_rate_perhr') ?: 0;
			$netamtcost = $this->input->post('serviceamountcost') ?: 0;
			$discount = $this->input->post('discount') ?: 0;
			$cleaning_material_fee = $this->input->post('cleaning_materials_charge') ?: 0;
			$supervisor_fee = $this->input->post('supervisor-charge') ?: 0;
			$total_amt = $this->input->post('tot_amout');
			$vat_amt = $this->input->post('service_vat_amount');
			$service_taxed_total = $this->input->post('service_taxed_total');
			$service_date = $this->input->post('edit_book_date');
			$maid_id = $this->input->post('edit_maid_id');
			$booking_type = $this->input->post('edit_booking_type');
			$oldtime_from = $this->input->post('edit_timefrom');
			$oldtime_to = $this->input->post('edit_timeto');
			$today_week_day = date('w', strtotime($service_date));
			if (preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_from) == false || preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_to) == false) {
				echo 'refresh';
				exit();
			}
			$no_of_hourss = $this->get_time_difference($time_from, $time_to);
			$bookings = $this->edit_booking_model->get_schedule_by_date_new($service_date,$maid_id);
			
			foreach ($bookings as $bookingval) {
				$booked_slots[$bookingval->maid_id]['time'][strtotime($bookingval->time_from)] = strtotime($bookingval->time_to);
			}
			// echo json_encode($booked_slots);
			// exit();
			
			$time_from_stamp = trim($this->input->post('time_from'));
			$time_to_stamp = trim($this->input->post('time_to'));
			
			if (isset($booked_slots[$maid_id]['time'])) {
				foreach ($booked_slots[$maid_id]['time'] as $f_time => $t_time) {
					if (strtotime($oldtime_from) != $f_time && strtotime($oldtime_to) != $t_time) {
						if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
							$return = array();
							$return['status'] = 'error';
							$return['message'] = 'The selected time slot is not available for this maid';
							echo json_encode($return);
							exit();
						}
					}
				}
			}
			$this->db->trans_begin();
			if ($booking_type == 'OD') {
				$update_fields = array();
				$update_fields['time_from'] = $time_from;
				$update_fields['time_to'] = $time_to;
				$update_fields['price_per_hr'] = $priceperhr;
				$update_fields['discount_price_per_hr'] = $discountpriceperhr;
				$update_fields['discount'] = $discount;
				$update_fields['net_service_cost'] = $netamtcost;
				$update_fields['service_charge'] = $total_amt;
				$update_fields['vat_charge'] = $vat_amt;
				$update_fields['total_amount'] = $service_taxed_total;
				$update_fields['no_of_hrs'] = $no_of_hourss;
				$update_fields['cleaning_material'] = $cleaning_material;
				$update_fields['cleaning_material_fee'] = $cleaning_material_fee;
				$update_fields['is_free'] = $isfree;
				if ($supervisor == 'Y') {
					$update_fields['supervisor_selected'] = 'Y';
					$update_fields['supervisor_charge_hourly'] = 60;
				} else {
					$update_fields['supervisor_selected'] = 'N';
					$update_fields['supervisor_charge_hourly'] = 0;
				}
				$update_fields['supervisor_charge_total'] = $supervisor_fee;
				$editupdate = $this->edit_booking_model->update_booking($booking_id, $update_fields, 'Booking_update');
				if($editupdate)
				{
					$input_cleaning_supplies = array_filter(explode(',',$this->input->post('cleaning_supplies')));
					
					if(!empty($input_cleaning_supplies))
					{
						// echo "hai";
						// exit();
						$cleaning_supplies = array();
						foreach($input_cleaning_supplies as $input_cleaning_supply){
							$cleaning_supplies[] = array(
								'booking_id' => $booking_id,
								'cleaning_supply_id' => $input_cleaning_supply
							);
						}
						// print_r($cleaning_supplies);exit();
						$cleaningsupplies = $this->edit_booking_model->insert_batch_booking_cleaning_supply($cleaning_supplies,$booking_id);
					} else {
						$cleaning_supplies = array();
						$cleaningsupplies = $this->edit_booking_model->insert_batch_booking_cleaning_supply($cleaning_supplies,$booking_id);
					}

					$get_dayservice_details = $this->edit_booking_model->check_day_service_booking_new($booking_id, $service_date);
					if(!empty($get_dayservice_details))
					{
						$updatearry = array();
						$updatearry['total_fee'] = $service_taxed_total;
						$this->edit_booking_model->update_day_service_booking($get_dayservice_details->day_service_id, $updatearry);
					}
					
					if ($this->db->trans_status() === FALSE)
					{
						$this->db->trans_rollback();
						$return = array();
						$return['status'] = 'error';
						$return['message'] = 'Update failed. Try again.';
						echo json_encode($return);
						exit();
					}
					else
					{
						$this->db->trans_commit();
						$return = array();
						$return['status'] = 'success';
						$return['message'] = 'Updated Successfully.';
						echo json_encode($return);
						exit();
					}
				} else {
					$this->db->trans_rollback();
					$return = array();
					$return['status'] = 'error';
					$return['message'] = 'Update failed. Try again.';
					echo json_encode($return);
					exit();
				}
			} else {
				$currentbooking = $this->edit_booking_model->get_booking_by_id($booking_id);
				
				$booking_fields = array();
				$booking_fields['customer_id'] = $currentbooking->customer_id;
				$booking_fields['customer_address_id'] = $currentbooking->customer_address_id;
				$booking_fields['maid_id'] = $currentbooking->maid_id;
				$booking_fields['service_type_id'] = $currentbooking->service_type_id;
				$booking_fields['service_start_date'] = $service_date;
				$booking_fields['service_week_day'] = $today_week_day;
				$booking_fields['time_from'] = $time_from;
				$booking_fields['time_to'] = $time_to;
				$booking_fields['booking_note'] = $currentbooking->booking_note;
				$booking_fields['booking_type'] = 'OD';
				$booking_fields['booking_category'] = 'C';
				$booking_fields['service_end'] = 1;
				$booking_fields['service_end_date'] = $service_date;
				$booking_fields['service_actual_end_date'] = $service_date;
				$booking_fields['price_per_hr'] = $priceperhr;
				$booking_fields['discount_price_per_hr'] = $discountpriceperhr;
				$booking_fields['discount'] = $discount;
				$booking_fields['net_service_cost'] = $netamtcost;
				$booking_fields['service_charge'] = $total_amt;
				$booking_fields['vat_charge'] = $vat_amt;
				$booking_fields['total_amount'] = $service_taxed_total;
				$booking_fields['pay_by_cash_charge'] = $currentbooking->pay_by_cash_charge;
				$booking_fields['is_locked'] = $currentbooking->is_locked;
				$booking_fields['no_of_hrs'] = $no_of_hourss;
				$booking_fields['cleaning_material'] = $cleaning_material;
				$booking_fields['cleaning_material_fee'] = $cleaning_material_fee;
				$booking_fields['booked_from'] = $currentbooking->booked_from;
				$booking_fields['booked_by'] = user_authenticate();
				$booking_fields['booking_status'] = 1;
				$booking_fields['booked_datetime'] = date('Y-m-d H:i:s');
				$booking_fields['payment_type'] = $currentbooking->book_payment_type;
				$booking_fields['interior_window_clean'] = $currentbooking->interior_window_clean;
				$booking_fields['fridge_cleaning'] = $currentbooking->fridge_cleaning;
				$booking_fields['ironing_services'] = $currentbooking->ironing_services;
				$booking_fields['oven_cleaning'] = $currentbooking->oven_cleaning;
				$booking_fields['crew_in'] = $currentbooking->crew_in;
				$booking_fields['pay_by'] = $currentbooking->pay_by;
				$booking_fields['tabletid'] = $currentbooking->tabletid;
				if ($supervisor == 'Y') {
					$booking_fields['supervisor_selected'] = 'Y';
					$booking_fields['supervisor_charge_hourly'] = 60;
				} else {
					$booking_fields['supervisor_selected'] = 'N';
					$booking_fields['supervisor_charge_hourly'] = 0;
				}
				$booking_fields['supervisor_charge_total'] = $supervisor_fee;
				$booking_fields['is_free'] = $isfree;
				
				$new_booking_id = $this->edit_booking_model->add_booking($booking_fields);
				if($new_booking_id)
				{
					$delete_b_fields = array();
					$delete_b_fields['booking_id'] = $booking_id;
					$delete_b_fields['service_date'] = $service_date;
					$delete_b_fields['type'] = 2;
					$delete_b_fields['deleted_by'] = user_authenticate();
					$this->edit_booking_model->add_booking_delete($booking_id,$delete_b_fields);
					
					$input_cleaning_supplies = array_filter(explode(',',$this->input->post('cleaning_supplies')));
					
					if(!empty($input_cleaning_supplies))
					{
						$cleaning_supplies = array();
						foreach($input_cleaning_supplies as $input_cleaning_supply){
							$cleaning_supplies[] = array(
								'booking_id' => $new_booking_id,
								'cleaning_supply_id' => $input_cleaning_supply
							);
						}
						// print_r($cleaning_supplies);exit();
						$cleaningsupplies = $this->edit_booking_model->insert_batch_booking_cleaning_supply($cleaning_supplies,$new_booking_id);
					}

					$get_dayservice_details = $this->edit_booking_model->check_day_service_booking_new($booking_id, $service_date);
					if(!empty($get_dayservice_details))
					{
						$updatearry = array();
						$updatearry['booking_id'] = $new_booking_id;
						$updatearry['total_fee'] = $service_taxed_total;
						$this->edit_booking_model->update_day_service_booking($get_dayservice_details->day_service_id, $updatearry);
					}
					
					if ($this->db->trans_status() === FALSE)
					{
						$this->db->trans_rollback();
						$return = array();
						$return['status'] = 'error';
						$return['message'] = 'Update failed. Try again.';
						echo json_encode($return);
						exit();
					}
					else
					{
						$this->db->trans_commit();
						$return = array();
						$return['status'] = 'success';
						$return['message'] = 'Updated Successfully.';
						echo json_encode($return);
						exit();
					}
				} else {
					$this->db->trans_rollback();
					$return = array();
					$return['status'] = 'error';
					$return['message'] = 'Update failed. Try again.';
					echo json_encode($return);
					exit();
				}
			}
		} else {
			$return = array();
			$return['status'] = 'error';
			$return['message'] = 'Parameter Missing';
			echo json_encode($return);
			exit();
		}
	}
	
	public function get_time_difference($time1, $time2)
    {
        $time1 = strtotime("1/1/1980 $time1");
        $time2 = strtotime("1/1/1980 $time2");
        if ($time2 < $time1) {
            $time2 = $time2 + 86400;
        }
        return ($time2 - $time1) / 3600;
    }
	
	public function get_invoice_detail()
	{
		$invoiceid = $this->input->post("invoiceid");
		$invoicedetail = $this->edit_booking_model->get_invoice_detailbyid($invoiceid);
		$data['invoice_detail'] = $invoicedetail;
		$html = $this->load->view('booking/invoice_detail_page', $data, TRUE);
		echo $html;
		exit();
	}
	
	public function update_line_desc()
    {
        $description = $this->input->post('line_description');
        $lineid = $this->input->post('lineid');
        $data=array();
        $data['description']=$description;
        $result=$this->edit_booking_model->update_invoicelineitems($lineid,$data);
        exit();
    }
	
	public function update_line_amt()
    {
        $settings = $this->settings_model->get_settings();
        $balanceamountval = $this->input->post('balanceamountval');
        $invoice_id = $this->input->post('invoice_id');
        $invstat = $this->input->post('invstat');
        $line_items = $this->edit_booking_model->get_invoice_line_items_by_invoice_id($this->input->post('invoice_id'));
        $data_invoice['invoice_total_amount'] = 0;
        $data_invoice['invoice_tax_amount'] = 0;
        $data_invoice['invoice_net_amount'] = 0;
        $data_invoice['balance_amount'] = 0;
		$this->db->trans_begin();
        foreach($line_items as $row){
            if($row->invoice_line_id == $this->input->post('lineid')){
                $data['line_amount'] = number_format($this->input->post('line_amount'),2);
                $data['line_vat_amount']= number_format(($this->input->post('line_amount')/100) * $settings->service_vat_percentage,2);
                $data['line_net_amount']= number_format($data['line_amount']+$data['line_vat_amount'],2);
				$result=$this->edit_booking_model->update_invoicelineitems($this->input->post('lineid'),$data);
                $data_invoice['invoice_total_amount'] += $data['line_amount'];
                $data_invoice['invoice_tax_amount'] += $data['line_vat_amount'];
                $data_invoice['invoice_net_amount'] += $data['line_net_amount'];
            }
            else{
                $data_invoice['invoice_total_amount'] += $row->line_amount;
                $data_invoice['invoice_tax_amount'] += $row->line_vat_amount;
                $data_invoice['invoice_net_amount'] += $row->line_net_amount;
            }
        }
		
        $data['invoice_total_amount'] = number_format($data_invoice['invoice_total_amount'],2);
        $data['invoice_tax_amount'] = number_format($data_invoice['invoice_tax_amount'],2);
        $data['invoice_net_amount'] = number_format($data_invoice['invoice_net_amount'],2);
        
		$custid = $this->input->post('custid');
		$line_old_net_amount = $this->input->post('line_old_net_amount');
		$line_net_amount = $this->input->post('line_new_net_amount');
		$get_customer_detail = $this->edit_booking_model->get_customer_detail($custid);
		$inv_amt = $get_customer_detail->total_invoice_amount;
		$bal_amount = $get_customer_detail->balance;
		$payd_amount = $get_customer_detail->total_paid_amount;
		$new_inv_amt=$inv_amt-$line_old_net_amount+$line_net_amount;
		$new_bal_amount = $bal_amount-$line_old_net_amount+$line_net_amount;

		$txt="$bal_amount-$line_old_net_amount+$line_net_amount";
		$data['txt']=$txt;
		$data['dtl']=$get_customer_detail;
		$balamnt = 0;
		if($data['invoice_net_amount'] < $balanceamountval)
		{
			$balamnt = ($balanceamountval - $data['invoice_net_amount']);
			$updteamt = ($balanceamountval - $data['invoice_net_amount']);
			$getinvoicepayments = $this->edit_booking_model->get_invoicepayments_by_id($invoice_id);
			foreach($getinvoicepayments as $payval)
			{
				if($payval->payment_amount <= $updteamt)
				{
					$deletepayval = $this->edit_booking_model->delete_invoicepayments_by_id($payval->inv_map_id,$payval->payment_amount,$payval->paymentId);
					$updteamt = ($updteamt - $payval->payment_amount);
				} else {
					$updatepayval = $this->edit_booking_model->update_invoicepayments_by_id($payval->inv_map_id,$payval->payment_amount,$payval->paymentId,$updteamt);
					$updteamt = ($updteamt - $payval->payment_amount);
				}
			}
			$data_invoice['balance_amount'] = ($data_invoice['invoice_net_amount'] - ($balanceamountval - $balamnt));
			$data_invoice['received_amount'] = ($balanceamountval - $balamnt);
		} else {
			$data_invoice['balance_amount'] = ($data_invoice['invoice_net_amount'] - $balanceamountval);
			$data_invoice['received_amount'] = ($balanceamountval);
		}
		
		if($invstat != 0)
		{
			if($data_invoice['balance_amount'] == 0)
			{
				$data_invoice['invoice_status'] = 3;
				$data_invoice['invoice_paid_status'] = 1;
			} else if($data_invoice['balance_amount'] == $data_invoice['invoice_net_amount']){
				$data_invoice['invoice_status'] = 1;
				$data_invoice['invoice_paid_status'] = 0;
			} else {
				$data_invoice['invoice_status'] = 1;
				$data_invoice['invoice_paid_status'] = 2;
			}
		}
		$data_invoice['invoice_qb_resync'] = 1;
        $this->edit_booking_model->update_invoicepay_detail($invoice_id, $data_invoice);
		
		$custdata=array();
		$custdata['total_invoice_amount']=$new_inv_amt;
		$custdata['total_paid_amount']= ($payd_amount - $balamnt);
		$custdata['balance']=($custdata['total_invoice_amount'] - $custdata['total_paid_amount']);
		$this->edit_booking_model->update_customer_detail($custid,$custdata);
		
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}

        die(json_encode($data));
    }
	
	public function delete_line_item()
	{
		$settings = $this->settings_model->get_settings();
        $balanceamountval = $this->input->post('balanceamountval');
        $invoice_id = $this->input->post('invoice_id');
        $invstat = $this->input->post('invstat');
        $line_items = $this->edit_booking_model->get_invoice_line_items_by_invoice_id($this->input->post('invoice_id'));
        $data_invoice['invoice_total_amount'] = 0;
        $data_invoice['invoice_tax_amount'] = 0;
        $data_invoice['invoice_net_amount'] = 0;
        $data_invoice['balance_amount'] = 0;
		$this->db->trans_begin();
        foreach($line_items as $row){
            if($row->invoice_line_id == $this->input->post('lineid')){
                // $data['line_amount'] = number_format($this->input->post('line_amount'),2);
                // $data['line_vat_amount']= number_format(($this->input->post('line_amount')/100) * $settings->service_vat_percentage,2);
                // $data['line_net_amount']= number_format($data['line_amount']+$data['line_vat_amount'],2);
				// $result=$this->edit_booking_model->update_invoicelineitems($this->input->post('lineid'),$data);
                // $data_invoice['invoice_total_amount'] += $data['line_amount'];
                // $data_invoice['invoice_tax_amount'] += $data['line_vat_amount'];
                // $data_invoice['invoice_net_amount'] += $data['line_net_amount'];
				$result=$this->edit_booking_model->delete_invoicelineitems($this->input->post('lineid'),$this->input->post('serviceid'));
            }
            else{
                $data_invoice['invoice_total_amount'] += $row->line_amount;
                $data_invoice['invoice_tax_amount'] += $row->line_vat_amount;
                $data_invoice['invoice_net_amount'] += $row->line_net_amount;
            }
        }
		
        
        $data['invoice_total_amount'] = number_format($data_invoice['invoice_total_amount'],2);
        $data['invoice_tax_amount'] = number_format($data_invoice['invoice_tax_amount'],2);
        $data['invoice_net_amount'] = number_format($data_invoice['invoice_net_amount'],2);
        
		$custid = $this->input->post('custid');
		$line_old_net_amount = $this->input->post('line_old_net_amount');
		$get_customer_detail = $this->edit_booking_model->get_customer_detail($custid);
		$inv_amt = $get_customer_detail->total_invoice_amount;
		$bal_amount = $get_customer_detail->balance;
		$payd_amount = $get_customer_detail->total_paid_amount;
		$new_inv_amt=$inv_amt-$line_old_net_amount;
		$new_bal_amount = $bal_amount-$line_old_net_amount;

		$txt="$bal_amount-$line_old_net_amount";
		$data['txt']=$txt;
		$data['dtl']=$get_customer_detail;
		$balamnt = 0;
		if($data['invoice_net_amount'] < $balanceamountval)
		{
			$balamnt = ($balanceamountval - $data['invoice_net_amount']);
			$updteamt = ($balanceamountval - $data['invoice_net_amount']);
			$getinvoicepayments = $this->edit_booking_model->get_invoicepayments_by_id($invoice_id);
			foreach($getinvoicepayments as $payval)
			{
				if($payval->payment_amount <= $updteamt)
				{
					$deletepayval = $this->edit_booking_model->delete_invoicepayments_by_id($payval->inv_map_id,$payval->payment_amount,$payval->paymentId);
					$updteamt = ($updteamt - $payval->payment_amount);
				} else {
					$updatepayval = $this->edit_booking_model->update_invoicepayments_by_id($payval->inv_map_id,$payval->payment_amount,$payval->paymentId,$updteamt);
					$updteamt = ($updteamt - $payval->payment_amount);
				}
			}
			$data_invoice['balance_amount'] = ($data_invoice['invoice_net_amount'] - ($balanceamountval - $balamnt));
			$data_invoice['received_amount'] = ($balanceamountval - $balamnt);
		} else {
			$data_invoice['balance_amount'] = ($data_invoice['invoice_net_amount'] - $balanceamountval);
			$data_invoice['received_amount'] = ($balanceamountval);
		}
		
		if($invstat != 0)
		{
			if($data_invoice['balance_amount'] == 0)
			{
				$data_invoice['invoice_status'] = 3;
				$data_invoice['invoice_paid_status'] = 1;
			} else if($data_invoice['balance_amount'] == $data_invoice['invoice_net_amount']){
				$data_invoice['invoice_status'] = 1;
				$data_invoice['invoice_paid_status'] = 0;
			} else {
				$data_invoice['invoice_status'] = 1;
				$data_invoice['invoice_paid_status'] = 2;
			}
		}
		$data_invoice['invoice_qb_resync'] = 1;
        $this->edit_booking_model->update_invoicepay_detail($invoice_id, $data_invoice);
		
		$custdata=array();
		$custdata['total_invoice_amount']=$new_inv_amt;
		$custdata['total_paid_amount']= ($payd_amount - $balamnt);
		// $custdata['balance']=$new_bal_amount;
		$custdata['balance']=($custdata['total_invoice_amount'] - $custdata['total_paid_amount']);
		$this->edit_booking_model->update_customer_detail($custid,$custdata);
		
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}

        die(json_encode($data));
	}
	
	public function delete_invoice_payments()
	{
		$invoice_id = $this->input->post('invoiceid');
		$inv_map_id = $this->input->post('inv_map_id');
		$paymentid = $this->input->post('paymentid');
		$customerid = $this->input->post('customerid');
		
		$getinvoicedetail = $this->edit_booking_model->get_invoice_detail_by_invoice_id($invoice_id,$inv_map_id);
		
		if(!empty($getinvoicedetail))
		{
			$this->db->trans_begin();
			$data_invoice['balance_amount'] = 0;
			$deletepayval = $this->edit_booking_model->delete_invoicepayments_by_id($inv_map_id,$getinvoicedetail->payment_amount,$paymentid);
			if($deletepayval)
			{
				$data_invoice['received_amount'] = ($getinvoicedetail->received_amount - $getinvoicedetail->payment_amount);
				$data_invoice['balance_amount'] = ($getinvoicedetail->invoice_net_amount - $data_invoice['received_amount']);
				
				if($data_invoice['balance_amount'] == 0)
				{
					$data_invoice['invoice_status'] = 3;
					$data_invoice['invoice_paid_status'] = 1;
				} else if($data_invoice['balance_amount'] == $getinvoicedetail->invoice_net_amount){
					$data_invoice['invoice_status'] = 1;
					$data_invoice['invoice_paid_status'] = 0;
				} else {
					$data_invoice['invoice_status'] = 1;
					$data_invoice['invoice_paid_status'] = 2;
				}
				$this->edit_booking_model->update_invoicepay_detail($invoice_id, $data_invoice);
				
				$custdata=array();
				$custdata['total_paid_amount']= ($getinvoicedetail->total_paid_amount - $getinvoicedetail->payment_amount);
				$custdata['balance']=($getinvoicedetail->total_invoice_amount - $custdata['total_paid_amount']);
				$this->edit_booking_model->update_customer_detail($customerid,$custdata);
			} else {
				$return = array();
				$return['status'] = 'error';
				$return['message'] = 'Something went wrong. Try again';
				echo json_encode($return);
				exit();
			}
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}
			else
			{
				$this->db->trans_commit();
			}
			$return = array();
			$return['status'] = 'success';
			$return['message'] = 'Payment deleted successfully.';
			echo json_encode($return);
			exit();
		} else {
			$return = array();
			$return['status'] = 'error';
			$return['message'] = 'Something went wrong. Try again';
			echo json_encode($return);
			exit();
		}
	}
	
	public function cancel_invoice()
    {
		$invoice_id = $this->input->post('invoiceid');
		$invoicedetail = $this->edit_booking_model->get_invoice_by_invoice_id($invoice_id);
		if(!empty($invoicedetail))
		{
			$this->db->trans_begin();
			if($invoicedetail->invoice_status == 0){
				$invoice_update_data = array();
				$invoice_update_data['invoice_status'] = 2;
				$update_invoice = $this->edit_booking_model->update_invoice_row($invoice_id,$invoice_update_data);
				// update day services
				$dayservice_update_data = array();
				$dayservice_update_data['invoice_status'] = 0;
				$dayservice_update = $this->edit_booking_model->update_day_services_where(array('serv_invoice_id'=>$invoice_id),$dayservice_update_data);
				$this->db->trans_commit();
				
				$return = array();
				$return['status'] = 'success';
				$return['message'] = 'Invoice cancelled successfully.';
				echo json_encode($return);
				exit();
			} else if($invoicedetail->invoice_status == 1 && $invoicedetail->invoice_paid_status == 0){
				$custdata=array();
				$custdata['total_invoice_amount']=($invoicedetail->total_invoice_amount - $invoicedetail->invoice_net_amount);
				$custdata['balance']=($invoicedetail->balance - $invoicedetail->invoice_net_amount);
				$this->edit_booking_model->update_customer_detail($invoicedetail->customer_id,$custdata);
				
				$invoice_update_data = array();
				$invoice_update_data['invoice_status'] = 2;
				$invoice_update_data['invoice_qb_resync'] = 1;
				$update_invoice = $this->edit_booking_model->update_invoice_row($invoice_id,$invoice_update_data);
				// update day services
				$dayservice_update_data = array();
				$dayservice_update_data['invoice_status'] = 0;
				$dayservice_update = $this->edit_booking_model->update_day_services_where(array('serv_invoice_id'=>$invoice_id),$dayservice_update_data);
				$this->db->trans_commit();
				
				$return = array();
				$return['status'] = 'success';
				$return['message'] = 'Invoice cancelled successfully.';
				echo json_encode($return);
				exit();
			} else if($invoicedetail->invoice_status != 0 && $invoicedetail->invoice_paid_status != 0){
				$getinvoicepayments = $this->edit_booking_model->get_invoicepayments_by_id($invoice_id);
				foreach($getinvoicepayments as $payval)
				{
					$deletepayval = $this->edit_booking_model->delete_invoicepayments_by_id($payval->inv_map_id,$payval->payment_amount,$payval->paymentId);
					if($deletepayval)
					{
						
					} else {
						$this->db->trans_rollback();
						$return = array();
						$return['status'] = 'error';
						$return['message'] = 'Something went wrong. Try again';
						echo json_encode($return);
						exit();
					}
				}
				$invoice_update_data = array();
				$invoice_update_data['invoice_status'] = 2;
				$invoice_update_data['invoice_qb_resync'] = 1;
				$invoice_update_data['invoice_paid_status'] = 0;
				$invoice_update_data['received_amount'] = 0;
				$invoice_update_data['balance_amount'] = $invoicedetail->invoice_net_amount;
				$update_invoice = $this->edit_booking_model->update_invoice_row($invoice_id,$invoice_update_data);
				// update day services
				$dayservice_update_data = array();
				$dayservice_update_data['invoice_status'] = 0;
				$dayservice_update = $this->edit_booking_model->update_day_services_where(array('serv_invoice_id'=>$invoice_id),$dayservice_update_data);
				
				$custdata=array();
				$custdata['total_invoice_amount']=($invoicedetail->total_invoice_amount - $invoicedetail->invoice_net_amount);
				$custdata['total_paid_amount']=($invoicedetail->total_paid_amount - $invoicedetail->received_amount);
				$custdata['balance']=($custdata['total_invoice_amount'] - $custdata['total_paid_amount']);
				$this->edit_booking_model->update_customer_detail($invoicedetail->customer_id,$custdata);
				$this->db->trans_commit();
				
				$return = array();
				$return['status'] = 'success';
				$return['message'] = 'Invoice cancelled successfully.';
				echo json_encode($return);
				exit();
			} else {
				$this->db->trans_rollback();
				$return = array();
				$return['status'] = 'error';
				$return['message'] = 'Something went wrong. Try again';
				echo json_encode($return);
				exit();
			}
		} else {
			$return = array();
			$return['status'] = 'error';
			$return['message'] = 'Something went wrong. Try again';
			echo json_encode($return);
			exit();
		}
    }
	
	public function delete_service()
	{
		$serviceid = $this->input->post("serviceid");
		$checkservice = $this->edit_booking_model->get_service_detail($serviceid);
		if(!empty($checkservice))
		{
			if($checkservice->invoice_status == 1)
			{
				$return = array();
				$return['status'] = 'error';
				$return['message'] = 'Please cancel the invoice and try again';
				echo json_encode($return);
				exit();
			} else {
				$dayservice_update_data = array();
				$dayservice_update_data['service_status'] = 3;
				$dayservice_update = $this->edit_booking_model->update_day_services($serviceid,$dayservice_update_data);
				if($dayservice_update)
				{
					$return = array();
					$return['status'] = 'success';
					$return['message'] = 'Service cancelled.';
					echo json_encode($return);
					exit();
				} else {
					$return = array();
					$return['status'] = 'error';
					$return['message'] = 'Something went wrong. Try again.';
					echo json_encode($return);
					exit();
				}
			}
		} else {
			$return = array();
			$return['status'] = 'error';
			$return['message'] = 'Something went wrong. Try again.';
			echo json_encode($return);
			exit();
		}
	}
	
	public function delete_booking()
	{
		$booking_id = $this->input->post("booking_id");
		$service_date = $this->input->post("service_date");
		$bookingdetail = $this->edit_booking_model->get_booking_service_detail($booking_id,$service_date);
		if(!empty($bookingdetail))
		{
			if($bookingdetail->invoice_status == 1)
			{
				$return = array();
				$return['status'] = 'error';
				$return['message'] = 'Please cancel the invoice and try again';
				echo json_encode($return);
				exit();
			} else {
				// $dayservice_update_data = array();
				// $dayservice_update_data['service_status'] = 3;
				// $dayservice_update = $this->edit_booking_model->update_day_services($bookingdetail->day_service_id,$dayservice_update_data);
			}
		}
		$return = array();
		$return['status'] = 'success';
		$return['message'] = 'Success';
		echo json_encode($return);
		exit();
		
	}
	
	public function delete_booking_process()
	{
		$booking_id = $this->input->post("bookingid");
		$service_date = $this->input->post("servicedate");
		$remarks = $this->input->post("remarks");
		$bookingdetail = $this->edit_booking_model->get_booking_by_id($booking_id);
		if(!empty($bookingdetail))
		{
			if (user_authenticate() != 1) {
				if ($bookingdetail->is_locked == 1 && $bookingdetail->booked_by != user_authenticate()) {
					$return = array();
					$return['status'] = 'error';
					$return['message'] = 'Booking Locked. Please try after unlocking.';
					echo json_encode($return);
					exit();
				}
			}
			$this->db->trans_begin();
			if ($bookingdetail->booking_type == 'OD') {
				$this->edit_booking_model->update_booking($booking_id, array('booking_status' => 2), 'Delete');
			} else {
				$this->edit_booking_model->add_activity($booking_id, 'Delete');
			}
			$delete_b_fields = array();
			$delete_b_fields['booking_id'] = $booking_id;
			$delete_b_fields['service_date'] = $service_date;
			$delete_b_fields['type'] = 1;
			$delete_b_fields['deleted_by'] = user_authenticate();
			$this->edit_booking_model->add_booking_delete_new($delete_b_fields);
			$delete_b_fields_new = array();
			$delete_b_fields_new['booking_id'] = $booking_id;
			$delete_b_fields_new['service_date'] = $service_date;
			$delete_b_fields_new['remarks'] = $remarks;
			$delete_b_fields_new['deleted_by'] = user_authenticate();
			$delete_b_fields_new['deleted_date_time'] = date('Y-m-d H:i:s');
			$this->edit_booking_model->add_booking_remarks($delete_b_fields_new);
			
			$bookingservicedetail = $this->edit_booking_model->get_booking_service_detail($booking_id,$service_date);
			if(!empty($bookingservicedetail))
			{
				$dayservice_update_data = array();
				$dayservice_update_data['service_status'] = 3;
				$dayservice_update = $this->edit_booking_model->update_day_services($bookingservicedetail->day_service_id,$dayservice_update_data);
			}
			
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
				$return = array();
				$return['status'] = 'error';
				$return['message'] = 'Something went wrong. Try again';
				echo json_encode($return);
				exit();
			}
			else
			{
				$this->db->trans_commit();
			}
			$return = array();
			$return['status'] = 'success';
			$return['message'] = 'Booking deleted successfully.';
			echo json_encode($return);
			exit();
		} else {
			$return = array();
			$return['status'] = 'error';
			$return['message'] = 'Booking not found.';
			echo json_encode($return);
			exit();
		}
	}
}