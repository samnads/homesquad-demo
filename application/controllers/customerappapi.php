<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Customerappapi extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('customerappapi_model');
		$this->load->helper('string');
        $this->load->model('tablets_model');
    }

    public function index() {
        $response               = array();
        $response['status']     = 'error';
        $response['error_code'] = '101';
        $response['message']    = 'Invalid request';

        echo json_encode($response);
        exit();
    }
	
	public function cust_registration() 
    {
        if ($this->config->item('ws') == $this->input->get('ws') ) 
        {
            $handle = fopen('php://input', 'r');
            $jsonInput = fgets($handle);
            //$jsonInput  = '{"name":"Sarumol","email":"saranya.12@gmail.com","phone":"7561859284","password":"kalidas","device_type":"ios","device_id":"fvlvndnflvdndkvdnfvkdvdfv dvdlkfndlvdnfvldnvld vndvndlvdv"}'; // normal login from mobile
            $json_arr = json_decode($jsonInput, true);
            if (!empty($json_arr))
            {
                if ($json_arr['name'] && strlen($json_arr['name']) > 0 && $json_arr['email'] && strlen($json_arr['email']) > 0 && $json_arr['device_id'] && strlen($json_arr['device_id']) > 0) 
                {
                    $email_check = $this->customerappapi_model->check_email(trim($json_arr['email']));
                    if ($email_check) 
                    {  
                            $response               = array();
                            $response['status']     = 'error';
                            $response['error_code'] = '103';
                            $response['message']    = 'Email already exist, please login with your existing username and password.';
                            echo json_encode($response);
                            exit();
                    }
                    
                    if (empty($json_arr['device_type'])) {
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '103';
                        $response['message'] = 'Type missing!';
                        echo json_encode($response);
                        exit();
                    }
                    $mobile_verification_code               = strtoupper(random_string('numeric', 5));
                        
                    $user_fields                        = array();
                    $user_fields['customer_username']   = trim($json_arr['email']);
                    $user_fields['customer_type']       = 'HO';
                    $user_fields['customer_name']       = trim($json_arr['name']);
                    $user_fields['customer_nick_name']  = trim($json_arr['name']);
                    $user_fields['is_company']          = 'N';

                    $user_fields['email_address']       = trim($json_arr['email']);
                    $user_fields['contact_person']       = trim($json_arr['name']);
                    $user_fields['payment_type']        = 'D';
                    $user_fields['payment_mode']        = 'Cash';
                    $user_fields['key_given']           = 'N';
                    $user_fields['price_hourly']        = '35';
                    $user_fields['price_extra']         = '35';
                    $user_fields['price_weekend']       = '35';
                    $user_fields['customer_source']       = 'Direct Call';
                    $user_fields['customer_status']     = '1';
                    $user_fields['mobile_verification_code']   = $mobile_verification_code;
                    $user_fields['mobile_status']   = '0';
                    $user_fields['push_token']          = trim($json_arr['device_id']);
                    $user_fields['oauth_provider']          = trim($json_arr['device_type']);
					$user_fields['fb_id']          = trim($json_arr['fb_id']);
                    $user_fields['customer_added_datetime']        = date('Y-m-d h:i:s');
                        
                    if (empty($json_arr['phone']))
                    {
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '103';
                        $response['message'] = 'Phone missing!';
                        echo json_encode($response);
                        exit();
                    }
                        
                    $mobile_check = $this->customerappapi_model->check_mobile(trim($json_arr['phone']));
                    if ($mobile_check) 
                    {  
                        $response               = array();
                        $response['status']     = 'error';
                        $response['error_code'] = '103';
                        $response['message']    = 'Mobile already exist, please login with your existing username and password.';
                        echo json_encode($response);
                        exit();	  
                    }
                        
                    if ($json_arr['phone'] && strlen($json_arr['phone']) > 0)
                    {       
                        $user_fields['phone_number']        = trim($json_arr['phone']);
                        $user_fields['mobile_number_1']     = trim($json_arr['phone']);
                        $user_fields['mobile_number_2']     = trim($json_arr['phone']);
                        $user_fields['mobile_number_3']     = trim($json_arr['phone']);
                    }

                    if ($json_arr['password'] && strlen($json_arr['password']) > 0) 
                    { 
                        $user_fields['customer_password']   = trim($json_arr['password']);
                        $register = $this->customerappapi_model->add_customers($user_fields);
                        if(isset($register)) 
                        {
							$this->_send_sms($json_arr['phone'], 'EliteMaids New Registration OTP - '.$mobile_verification_code);
							$this->_send_otp_mail($json_arr['name'],$json_arr['email'],$mobile_verification_code);
                            $this->sent_registration_confirmation_mail($register);
                            $userInfo = $this->customerappapi_model->get_customer_by_id($register);
                            if(empty($userInfo))
                            {
                                echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'Customer not found')); exit();   
                            } else {
                                $userdetail_array = array();
                                $userdetail_array['user_id'] = $userInfo->customer_id;
                                $userdetail_array['name'] = $userInfo->customer_name;
                                $userdetail_array['email'] = $userInfo->email_address;
                                $userdetail_array['photo'] = "";
                                $userdetail_array['phone'] = $userInfo->mobile_number_1;
                                $userdetail_array['is_verified'] = false;
                            
                                echo json_encode(array('status' => 'success','response_code'=>'200','message'=>'Account Created Successfully', 'customer_details' => $userdetail_array));
                                exit();
                            }
                        } else {
                            echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'Something went wrong'));
                            exit();
                        }
                    } else {
                        $response               = array();
                        $response['status']     = 'error';
                        $response['error_code'] = '103';
                        $response['message']    = 'Password Parameter missing !';
                        echo json_encode($response);
                        exit();
                    }
                } else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '103';
                    $response['message']    = 'Parameter missing!';
                    echo json_encode($response);
                    exit();
                }
            } else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
            }
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';

            echo json_encode($response);
            exit();
        }
    }
	
	public function customerLogin() 
    {
        if ($this->config->item('ws') == $this->input->get('ws') ) 
        {
            $handle = fopen('php://input', 'r');
            $jsonInput = fgets($handle); 
            //$jsonInput  = '{"email":"3tester@gmail.com","device_id":"1212332323","device_type":"android","password":"545145451"}'; // normal login from mobile
            $json_arr = json_decode($jsonInput, true);
            
            if (!empty($json_arr))
            { 
                if ( $json_arr['email'] && strlen($json_arr['email']) > 0 && $json_arr['device_id'] && strlen($json_arr['device_id']) > 0) 
                {
                    if (empty($json_arr['device_type'])) 
                    {
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '103';
                        $response['message'] = 'Type missing!';
                        echo json_encode($response);
                        exit();
                    }
                    
                    if (empty($json_arr['email'])) 
                    {
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '103';
                        $response['message'] = 'Username missing!';
                        echo json_encode($response);
                        exit();
                    }
                    
                    if (empty($json_arr['password'])) 
                    {
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '103';
                        $response['message'] = 'password missing!';
                        echo json_encode($response);
                        exit();
                    }
                    $user_exist=$this->customerappapi_model->check_email(trim($json_arr['email']));
                    if($user_exist){
                    $login_check = $this->customerappapi_model->customer_login(trim($json_arr['email']) , trim($json_arr['password']));
                    
                    if(isset($login_check->customer_id))
                    {
                        $getuserdetails = $this->customerappapi_model->get_all_customer_details($login_check->customer_id);
                        
                        $userdetail_array = array();
                        $userdetail_array['user_id'] = $getuserdetails->customer_id;
                        $userdetail_array['name'] = $getuserdetails->customer_name;
                        $userdetail_array['email'] = $getuserdetails->email_address;
                        //$userdetail_array['photo'] = "";
                        if(!empty($getuserdetails->mobile_number_1))
                        {
                            $userdetail_array['phone'] = $getuserdetails->mobile_number_1;
                        } else {
                            $userdetail_array['phone'] = "";
                        }
                        if(!empty($getuserdetails->area_id))
                        {
                            $userdetail_array['area_id'] = $getuserdetails->area_id;
                        } else {
                            $userdetail_array['area_id'] = "";
                        }
                        if(!empty($getuserdetails->area_name))
                        {
                            $userdetail_array['area_name'] = $getuserdetails->area_name;
                        } else {
                            $userdetail_array['area_name'] = "";
                        }
                        if(!empty($getuserdetails->building))
                        {
                            $userdetail_array['building'] = $getuserdetails->building;
                        } else {
                            $userdetail_array['building'] = "";
                        }
                        if(!empty($getuserdetails->unit_no))
                        {
                            $userdetail_array['unit'] = $getuserdetails->unit_no;
                        } else {
                            $userdetail_array['unit'] = "";
                        }
                        if(!empty($getuserdetails->street))
                        {
                            $userdetail_array['street'] = $getuserdetails->street;
                        } else {
                            $userdetail_array['street'] = "";
                        }
                        if(!empty($getuserdetails->other_area))
                        {
                            $userdetail_array['other_area'] = $getuserdetails->other_area;
                        } else {
                            $userdetail_array['other_area'] = "";
                        }
                        if($getuserdetails->mobile_status == 0)
                        {
                            $userdetail_array['is_verified'] = false;
                        } else {
                            $userdetail_array['is_verified'] = true;
                        }
                        if(!empty($getuserdetails->customer_photo_file))
                        {
                            $getuserdetails->customer_photo_file = base_url().'customer_img/'.$getuserdetails->customer_photo_file;
                        } else {
                            $getuserdetails->customer_photo_file = base_url().'img/no_image.jpg';
                        }
                       
                        $userdetail_array['photo'] = $getuserdetails->customer_photo_file;
                        
                        $update_device_detail = array();
                        $update_device_detail['push_token']          = trim($json_arr['device_id']);
                        $update_device_detail['oauth_provider']          = trim($json_arr['device_type']);
                        
                        $this->customerappapi_model->update_customers($update_device_detail,$login_check->customer_id);
                        
                        echo json_encode(array('status' => 'success','response_code'=>'200','message'=>'Logged in Successfully','customer_details'=>$userdetail_array)); exit();
                    } else {
                        echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'Invalid login')); exit();
                    }
                   }
                    else
                  {
                    echo json_encode(array('status' => 'invalid','response_code'=>'103','message'=>'User not found ')); exit();  
                  }
                } else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '103';
                    $response['message']    = 'Parameter missing!';
                    echo json_encode($response);
                    exit();
                }
            } else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
            }
//            print_r($json_arr);
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';

            echo json_encode($response);
            exit();
        }
    }
	
	public function facebook_registration()
	{
		if ($this->config->item('ws') == $this->input->get('ws') ) 
        {
			$handle = fopen('php://input', 'r');
            $jsonInput = fgets($handle); 
            //$jsonInput  = '{"name":"Sincy Thomas","email":"sweetythomasaz@gmail.com","auth_token":"538913763140884","device_id":"dRI2vIKFnr4:APA91bGV-vpSm_7wdckpMrv0jIx2dzyo-E8e58q-auqG8bgikm84APEPlm_VmIFtETKJdQmvhUj2ebPq2xep-VTFlygd4rAEHPWmD18Kcmb-NnqJBfGT81BwNIf2nfmW49kAa2ldirkS","device_type":"Android"}'; // normal login from mobile
            //$jsonInput = '{"name":"Dina Seaman","email":"","auth_token":"114785726079170","device_id":"deYu7AyADt8:APA91bF49NGtyQURA2ahltrI4GMpbsmppSOMIaRn46ll0a2bxdZXCXPnqOy6HmRYSfncWogqj7HW4XI91GinCKle_5v4G3M4SQ3jmMDuGlXfwmoQGzpx2JYYGezTMsXb8Oh2DGuGjs60","device_type":"Android"}';
			$json_arr = json_decode($jsonInput, true);
			if (!empty($json_arr))
            {
				if ( $json_arr['device_id'] && strlen($json_arr['device_id']) > 0) 
                {
					if($json_arr['email'] == "") 
                    {
						$token      = trim($json_arr['auth_token']);
						$customer   = $this->customerappapi_model->get_customer_by_token($token);
						if(isset($customer->customer_id))
						{
							$update_device_detail = array();
							$update_device_detail['push_token']          = trim($json_arr['device_id']);
							$update_device_detail['oauth_provider']          = trim($json_arr['device_type']);
							
							$this->customerappapi_model->update_customers($update_device_detail,$customer->customer_id);
							
							$getuserdetails = $this->customerappapi_model->get_all_customer_details($customer->customer_id);
                        
							$userdetail_array = array();
							$userdetail_array['user_id'] = $getuserdetails->customer_id;
							$userdetail_array['name'] = $getuserdetails->customer_name;
							$userdetail_array['email'] = $getuserdetails->email_address;
							//$userdetail_array['photo'] = "";
							if(!empty($getuserdetails->mobile_number_1))
							{
								$userdetail_array['phone'] = $getuserdetails->mobile_number_1;
							} else {
								$userdetail_array['phone'] = "";
							}
							if(!empty($getuserdetails->area_id))
							{
								$userdetail_array['area_id'] = $getuserdetails->area_id;
							} else {
								$userdetail_array['area_id'] = "";
							}
							if(!empty($getuserdetails->area_name))
							{
								$userdetail_array['area_name'] = $getuserdetails->area_name;
							} else {
								$userdetail_array['area_name'] = "";
							}
							if(!empty($getuserdetails->building))
							{
								$userdetail_array['building'] = $getuserdetails->building;
							} else {
								$userdetail_array['building'] = "";
							}
							if(!empty($getuserdetails->unit_no))
							{
								$userdetail_array['unit'] = $getuserdetails->unit_no;
							} else {
								$userdetail_array['unit'] = "";
							}
							if(!empty($getuserdetails->street))
							{
								$userdetail_array['street'] = $getuserdetails->street;
							} else {
								$userdetail_array['street'] = "";
							}
							if(!empty($getuserdetails->other_area))
							{
								$userdetail_array['other_area'] = $getuserdetails->other_area;
							} else {
								$userdetail_array['other_area'] = "";
							}
							if($getuserdetails->mobile_status == 0)
							{
								$userdetail_array['is_verified'] = false;
							} else {
								$userdetail_array['is_verified'] = true;
							}
							if(!empty($getuserdetails->customer_photo_file))
							{
								$getuserdetails->customer_photo_file = base_url(). 'customer_img/'.$getuserdetails->customer_photo_file;
							} else {
								$getuserdetails->customer_photo_file = base_url(). 'img/no_image.jpg';
							}
                       
							$userdetail_array['photo'] = $getuserdetails->customer_photo_file;
							
							echo json_encode(array('status' => 'success','response_code'=>'200','message'=>'Logged in Successfully','customer_details'=>$userdetail_array)); exit();
                        
						} else {
							$response               = array();
							$response['status']     = 'error';
							$response['error_code'] = '103';
							$response['message']    = 'Invalid login...';
							echo json_encode($response);
							exit();
						}
					} else {
						$token      = trim($json_arr['auth_token']);
						$customer   = $this->customerappapi_model->get_customer_by_token($token);
						if(isset($customer->customer_id))
						{
							$update_device_detail = array();
							$update_device_detail['push_token']          = trim($json_arr['device_id']);
							$update_device_detail['oauth_provider']          = trim($json_arr['device_type']);
							
							$this->customerappapi_model->update_customers($update_device_detail,$customer->customer_id);
							
							$getuserdetails = $this->customerappapi_model->get_all_customer_details($customer->customer_id);
                        
							$userdetail_array = array();
							$userdetail_array['user_id'] = $getuserdetails->customer_id;
							$userdetail_array['name'] = $getuserdetails->customer_name;
							$userdetail_array['email'] = $getuserdetails->email_address;
							//$userdetail_array['photo'] = "";
							if(!empty($getuserdetails->mobile_number_1))
							{
								$userdetail_array['phone'] = $getuserdetails->mobile_number_1;
							} else {
								$userdetail_array['phone'] = "";
							}
							if(!empty($getuserdetails->area_id))
							{
								$userdetail_array['area_id'] = $getuserdetails->area_id;
							} else {
								$userdetail_array['area_id'] = "";
							}
							if(!empty($getuserdetails->area_name))
							{
								$userdetail_array['area_name'] = $getuserdetails->area_name;
							} else {
								$userdetail_array['area_name'] = "";
							}
							if(!empty($getuserdetails->building))
							{
								$userdetail_array['building'] = $getuserdetails->building;
							} else {
								$userdetail_array['building'] = "";
							}
							if(!empty($getuserdetails->unit_no))
							{
								$userdetail_array['unit'] = $getuserdetails->unit_no;
							} else {
								$userdetail_array['unit'] = "";
							}
							if(!empty($getuserdetails->street))
							{
								$userdetail_array['street'] = $getuserdetails->street;
							} else {
								$userdetail_array['street'] = "";
							}
							if(!empty($getuserdetails->other_area))
							{
								$userdetail_array['other_area'] = $getuserdetails->other_area;
							} else {
								$userdetail_array['other_area'] = "";
							}
							if($getuserdetails->mobile_status == 0)
							{
								$userdetail_array['is_verified'] = false;
							} else {
								$userdetail_array['is_verified'] = true;
							}
							if(!empty($getuserdetails->customer_photo_file))
							{
								$getuserdetails->customer_photo_file = base_url(). 'customer_img/'.$getuserdetails->customer_photo_file;
							} else {
								$getuserdetails->customer_photo_file = base_url(). 'img/no_image.jpg';
							}
                       
							$userdetail_array['photo'] = $getuserdetails->customer_photo_file;
							
							echo json_encode(array('status' => 'success','response_code'=>'200','message'=>'Logged in Successfully','customer_details'=>$userdetail_array)); exit();
                        
						} else {
							$email = $json_arr['email'];
							$user_exist=$this->customerappapi_model->check_email(trim($email));
							if($user_exist)
							{
								$response               = array();
								$response['status']     = 'error';
								$response['error_code'] = '103';
								$response['message']    = 'Email id already exists...';
								echo json_encode($response);
								exit();
							} else {
								$mobile_verification_code               = strtoupper(random_string('numeric', 5));
                        
								$user_fields                        = array();
								$user_fields['customer_username']   = trim($json_arr['email']);
								$user_fields['customer_type']       = 'HO';
								$user_fields['customer_name']       = trim($json_arr['name']);
								$user_fields['customer_nick_name']  = trim($json_arr['name']);
								$user_fields['customer_password']       = $mobile_verification_code;
								$user_fields['is_company']          = 'N';

								$user_fields['email_address']       = trim($json_arr['email']);
								$user_fields['contact_person']       = trim($json_arr['name']);
								$user_fields['payment_type']        = 'D';
								$user_fields['payment_mode']        = 'Cash';
								$user_fields['key_given']           = 'N';
								$user_fields['price_hourly']        = '35';
								$user_fields['price_extra']         = '35';
								$user_fields['price_weekend']       = '35';
								$user_fields['customer_source']       = 'Direct Call';
								$user_fields['customer_status']     = '1';
								$user_fields['mobile_verification_code']   = $mobile_verification_code;
								$user_fields['mobile_status']   = '0';
								$user_fields['push_token']          = trim($json_arr['device_id']);
								$user_fields['oauth_provider']          = trim($json_arr['device_type']);
								$user_fields['fb_id']          = trim($json_arr['auth_token']);
								$user_fields['customer_from']          = 'C';
								$user_fields['customer_added_datetime']        = date('Y-m-d h:i:s');
                        
								$register = $this->customerappapi_model->add_customers($user_fields);
                    
								if(isset($register)) 
								{
									//hided need to do
									$this->sent_registration_confirmation_mail($register);
									$getuserdetails = $this->customerappapi_model->get_all_customer_details($register);
								
									$userdetail_array = array();
									$userdetail_array['user_id'] = $getuserdetails->customer_id;
									$userdetail_array['name'] = $getuserdetails->customer_name;
									$userdetail_array['email'] = $getuserdetails->email_address;
									//$userdetail_array['photo'] = "";
									if(!empty($getuserdetails->mobile_number_1))
									{
										$userdetail_array['phone'] = $getuserdetails->mobile_number_1;
									} else {
										$userdetail_array['phone'] = "";
									}
									if(!empty($getuserdetails->area_id))
									{
										$userdetail_array['area_id'] = $getuserdetails->area_id;
									} else {
										$userdetail_array['area_id'] = "";
									}
									if(!empty($getuserdetails->area_name))
									{
										$userdetail_array['area_name'] = $getuserdetails->area_name;
									} else {
										$userdetail_array['area_name'] = "";
									}
									if(!empty($getuserdetails->building))
									{
										$userdetail_array['building'] = $getuserdetails->building;
									} else {
										$userdetail_array['building'] = "";
									}
									if(!empty($getuserdetails->unit_no))
									{
										$userdetail_array['unit'] = $getuserdetails->unit_no;
									} else {
										$userdetail_array['unit'] = "";
									}
									if(!empty($getuserdetails->street))
									{
										$userdetail_array['street'] = $getuserdetails->street;
									} else {
										$userdetail_array['street'] = "";
									}
									if(!empty($getuserdetails->other_area))
									{
										$userdetail_array['other_area'] = $getuserdetails->other_area;
									} else {
										$userdetail_array['other_area'] = "";
									}
									if($getuserdetails->mobile_status == 0)
									{
										$userdetail_array['is_verified'] = false;
									} else {
										$userdetail_array['is_verified'] = true;
									}
									if(!empty($getuserdetails->customer_photo_file))
									{
										$getuserdetails->customer_photo_file = base_url(). 'customer_img/'.$getuserdetails->customer_photo_file;
									} else {
										$getuserdetails->customer_photo_file = base_url(). 'img/no_image.jpg';
									}
							   
									$userdetail_array['photo'] = $getuserdetails->customer_photo_file;
									echo json_encode(array('status' => 'success','response_code'=>'200','message'=>'Logged in Successfully','customer_details'=>$userdetail_array)); exit();
									
								} else {
									echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'Something went wrong'));
									exit();
								}
							}
						}
					}
					
				} else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '103';
                    $response['message']    = 'Parameter missing!';
                    echo json_encode($response);
                    exit();
                } 
				
			} else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
            }
		} else {
			$response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';

            echo json_encode($response);
            exit();
		}
	}
	
	public function facebookLogin()
	{
		if ($this->config->item('ws') == $this->input->get('ws') ) 
        {
			$handle = fopen('php://input', 'r');
            $jsonInput = fgets($handle); 
            //$jsonInput  = '{"name":"Sincy Thomas","email":"sweetythomasaz@gmail.com","auth_token":"538913763140884","device_id":"dRI2vIKFnr4:APA91bGV-vpSm_7wdckpMrv0jIx2dzyo-E8e58q-auqG8bgikm84APEPlm_VmIFtETKJdQmvhUj2ebPq2xep-VTFlygd4rAEHPWmD18Kcmb-NnqJBfGT81BwNIf2nfmW49kAa2ldirkS","device_type":"Android"}'; // normal login from mobile
            //$jsonInput = '{"name":"Dina Seaman","email":"","auth_token":"114785726079170","device_id":"deYu7AyADt8:APA91bF49NGtyQURA2ahltrI4GMpbsmppSOMIaRn46ll0a2bxdZXCXPnqOy6HmRYSfncWogqj7HW4XI91GinCKle_5v4G3M4SQ3jmMDuGlXfwmoQGzpx2JYYGezTMsXb8Oh2DGuGjs60","device_type":"Android"}';
			$json_arr = json_decode($jsonInput, true);
			if (!empty($json_arr))
            {
				if ( $json_arr['device_id'] && strlen($json_arr['device_id']) > 0) 
                {
					if($json_arr['email'] == "") 
                    {
						$token      = trim($json_arr['auth_token']);
						$customer   = $this->customerappapi_model->get_customer_by_token($token);
						if(isset($customer->customer_id))
						{
							$update_device_detail = array();
							$update_device_detail['push_token']          = trim($json_arr['device_id']);
							$update_device_detail['oauth_provider']          = trim($json_arr['device_type']);
							
							$this->customerappapi_model->update_customers($update_device_detail,$customer->customer_id);
							
							$getuserdetails = $this->customerappapi_model->get_all_customer_details($customer->customer_id);
                        
							$userdetail_array = array();
							$userdetail_array['user_id'] = $getuserdetails->customer_id;
							$userdetail_array['name'] = $getuserdetails->customer_name;
							$userdetail_array['email'] = $getuserdetails->email_address;
							//$userdetail_array['photo'] = "";
							if(!empty($getuserdetails->mobile_number_1))
							{
								$userdetail_array['phone'] = $getuserdetails->mobile_number_1;
							} else {
								$userdetail_array['phone'] = "";
							}
							if(!empty($getuserdetails->area_id))
							{
								$userdetail_array['area_id'] = $getuserdetails->area_id;
							} else {
								$userdetail_array['area_id'] = "";
							}
							if(!empty($getuserdetails->area_name))
							{
								$userdetail_array['area_name'] = $getuserdetails->area_name;
							} else {
								$userdetail_array['area_name'] = "";
							}
							if(!empty($getuserdetails->building))
							{
								$userdetail_array['building'] = $getuserdetails->building;
							} else {
								$userdetail_array['building'] = "";
							}
							if(!empty($getuserdetails->unit_no))
							{
								$userdetail_array['unit'] = $getuserdetails->unit_no;
							} else {
								$userdetail_array['unit'] = "";
							}
							if(!empty($getuserdetails->street))
							{
								$userdetail_array['street'] = $getuserdetails->street;
							} else {
								$userdetail_array['street'] = "";
							}
							if(!empty($getuserdetails->other_area))
							{
								$userdetail_array['other_area'] = $getuserdetails->other_area;
							} else {
								$userdetail_array['other_area'] = "";
							}
							if($getuserdetails->mobile_status == 0)
							{
								$userdetail_array['is_verified'] = false;
							} else {
								$userdetail_array['is_verified'] = true;
							}
							if(!empty($getuserdetails->customer_photo_file))
							{
								$getuserdetails->customer_photo_file = base_url(). 'customer_img/'.$getuserdetails->customer_photo_file;
							} else {
								$getuserdetails->customer_photo_file = base_url(). 'img/no_image.jpg';
							}
                       
							$userdetail_array['photo'] = $getuserdetails->customer_photo_file;
							
							echo json_encode(array('status' => 'success','response_code'=>'200','message'=>'Logged in Successfully','customer_details'=>$userdetail_array)); exit();
                        
						} else {
							$response               = array();
							$response['status']     = 'error';
							$response['error_code'] = '103';
							$response['message']    = 'Invalid login...';
							echo json_encode($response);
							exit();
						}
					} else {
						$token      = trim($json_arr['auth_token']);
						$customer   = $this->customerappapi_model->get_customer_by_token($token);
						if(isset($customer->customer_id))
						{
							$update_device_detail = array();
							$update_device_detail['push_token']          = trim($json_arr['device_id']);
							$update_device_detail['oauth_provider']          = trim($json_arr['device_type']);
							
							$this->customerappapi_model->update_customers($update_device_detail,$customer->customer_id);
							
							$getuserdetails = $this->customerappapi_model->get_all_customer_details($customer->customer_id);
                        
							$userdetail_array = array();
							$userdetail_array['user_id'] = $getuserdetails->customer_id;
							$userdetail_array['name'] = $getuserdetails->customer_name;
							$userdetail_array['email'] = $getuserdetails->email_address;
							//$userdetail_array['photo'] = "";
							if(!empty($getuserdetails->mobile_number_1))
							{
								$userdetail_array['phone'] = $getuserdetails->mobile_number_1;
							} else {
								$userdetail_array['phone'] = "";
							}
							if(!empty($getuserdetails->area_id))
							{
								$userdetail_array['area_id'] = $getuserdetails->area_id;
							} else {
								$userdetail_array['area_id'] = "";
							}
							if(!empty($getuserdetails->area_name))
							{
								$userdetail_array['area_name'] = $getuserdetails->area_name;
							} else {
								$userdetail_array['area_name'] = "";
							}
							if(!empty($getuserdetails->building))
							{
								$userdetail_array['building'] = $getuserdetails->building;
							} else {
								$userdetail_array['building'] = "";
							}
							if(!empty($getuserdetails->unit_no))
							{
								$userdetail_array['unit'] = $getuserdetails->unit_no;
							} else {
								$userdetail_array['unit'] = "";
							}
							if(!empty($getuserdetails->street))
							{
								$userdetail_array['street'] = $getuserdetails->street;
							} else {
								$userdetail_array['street'] = "";
							}
							if(!empty($getuserdetails->other_area))
							{
								$userdetail_array['other_area'] = $getuserdetails->other_area;
							} else {
								$userdetail_array['other_area'] = "";
							}
							if($getuserdetails->mobile_status == 0)
							{
								$userdetail_array['is_verified'] = false;
							} else {
								$userdetail_array['is_verified'] = true;
							}
							if(!empty($getuserdetails->customer_photo_file))
							{
								$getuserdetails->customer_photo_file = base_url(). 'customer_img/'.$getuserdetails->customer_photo_file;
							} else {
								$getuserdetails->customer_photo_file = base_url(). 'img/no_image.jpg';
							}
                       
							$userdetail_array['photo'] = $getuserdetails->customer_photo_file;
							
							echo json_encode(array('status' => 'success','response_code'=>'200','message'=>'Logged in Successfully','customer_details'=>$userdetail_array)); exit();
                        
						} else {
							$email = $json_arr['email'];
							$user_exist=$this->customerappapi_model->check_email(trim($email));
							if($user_exist)
							{
								$response               = array();
								$response['status']     = 'error';
								$response['error_code'] = '103';
								$response['message']    = 'Email id already exists...';
								echo json_encode($response);
								exit();
							} else {
								$mobile_verification_code               = strtoupper(random_string('numeric', 5));
                        
								$user_fields                        = array();
								$user_fields['customer_username']   = trim($json_arr['email']);
								$user_fields['customer_type']       = 'HO';
								$user_fields['customer_name']       = trim($json_arr['name']);
								$user_fields['customer_nick_name']  = trim($json_arr['name']);
								$user_fields['customer_password']       = $mobile_verification_code;
								$user_fields['is_company']          = 'N';

								$user_fields['email_address']       = trim($json_arr['email']);
								$user_fields['contact_person']       = trim($json_arr['name']);
								$user_fields['payment_type']        = 'D';
								$user_fields['payment_mode']        = 'Cash';
								$user_fields['key_given']           = 'N';
								$user_fields['price_hourly']        = '35';
								$user_fields['price_extra']         = '35';
								$user_fields['price_weekend']       = '35';
								$user_fields['customer_source']       = 'Direct Call';
								$user_fields['customer_status']     = '1';
								$user_fields['mobile_verification_code']   = $mobile_verification_code;
								$user_fields['mobile_status']   = '0';
								$user_fields['push_token']          = trim($json_arr['device_id']);
								$user_fields['oauth_provider']          = trim($json_arr['device_type']);
								$user_fields['fb_id']          = trim($json_arr['auth_token']);
								$user_fields['customer_from']          = 'C';
								$user_fields['customer_added_datetime']        = date('Y-m-d h:i:s');
                        
								$register = $this->customerappapi_model->add_customers($user_fields);
                    
								if(isset($register)) 
								{
									//hided need to do
									$this->sent_registration_confirmation_mail($register);
									$getuserdetails = $this->customerappapi_model->get_all_customer_details($register);
								
									$userdetail_array = array();
									$userdetail_array['user_id'] = $getuserdetails->customer_id;
									$userdetail_array['name'] = $getuserdetails->customer_name;
									$userdetail_array['email'] = $getuserdetails->email_address;
									//$userdetail_array['photo'] = "";
									if(!empty($getuserdetails->mobile_number_1))
									{
										$userdetail_array['phone'] = $getuserdetails->mobile_number_1;
									} else {
										$userdetail_array['phone'] = "";
									}
									if(!empty($getuserdetails->area_id))
									{
										$userdetail_array['area_id'] = $getuserdetails->area_id;
									} else {
										$userdetail_array['area_id'] = "";
									}
									if(!empty($getuserdetails->area_name))
									{
										$userdetail_array['area_name'] = $getuserdetails->area_name;
									} else {
										$userdetail_array['area_name'] = "";
									}
									if(!empty($getuserdetails->building))
									{
										$userdetail_array['building'] = $getuserdetails->building;
									} else {
										$userdetail_array['building'] = "";
									}
									if(!empty($getuserdetails->unit_no))
									{
										$userdetail_array['unit'] = $getuserdetails->unit_no;
									} else {
										$userdetail_array['unit'] = "";
									}
									if(!empty($getuserdetails->street))
									{
										$userdetail_array['street'] = $getuserdetails->street;
									} else {
										$userdetail_array['street'] = "";
									}
									if(!empty($getuserdetails->other_area))
									{
										$userdetail_array['other_area'] = $getuserdetails->other_area;
									} else {
										$userdetail_array['other_area'] = "";
									}
									if($getuserdetails->mobile_status == 0)
									{
										$userdetail_array['is_verified'] = false;
									} else {
										$userdetail_array['is_verified'] = true;
									}
									if(!empty($getuserdetails->customer_photo_file))
									{
										$getuserdetails->customer_photo_file = base_url(). 'customer_img/'.$getuserdetails->customer_photo_file;
									} else {
										$getuserdetails->customer_photo_file = base_url(). 'img/no_image.jpg';
									}
							   
									$userdetail_array['photo'] = $getuserdetails->customer_photo_file;
									echo json_encode(array('status' => 'success','response_code'=>'200','message'=>'Logged in Successfully','customer_details'=>$userdetail_array)); exit();
									
								} else {
									echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'Something went wrong'));
									exit();
								}
							}
						}
					}
					
				} else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '103';
                    $response['message']    = 'Parameter missing!';
                    echo json_encode($response);
                    exit();
                } 
				
			} else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
            }
		} else {
			$response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';

            echo json_encode($response);
            exit();
		}
	}
	
	public function userDetails()
    {
        if ($this->config->item('ws') == $this->input->get('ws') ) 
        { 
            $handle     = fopen('php://input', 'r');
            $jsonInput  = fgets($handle); 
            //$jsonInput  = '{"user_id":"79"}'; // normal login from mobile
            $json_arr   = json_decode($jsonInput, true);
            
            if (!empty($json_arr))
            { 
                if ( $json_arr['user_id'] && strlen($json_arr['user_id']) > 0 ) 
                {
                    $getuserdetails = $this->customerappapi_model->get_all_customer_details($json_arr['user_id']);
                    
                    if(empty($getuserdetails))
                    {
                        echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'Customer not found')); exit();   
                    } else {
                        $userdetail_array = array();
                        $userdetail_array['user_id'] = $getuserdetails->customer_id;
                        $userdetail_array['name'] = $getuserdetails->customer_name;
                        $userdetail_array['email'] = $getuserdetails->email_address;
                        //$userdetail_array['photo'] = "";
                        if(!empty($getuserdetails->mobile_number_1))
                        {
                            $userdetail_array['phone'] = $getuserdetails->mobile_number_1;
                        } else {
                            $userdetail_array['phone'] = "";
                        }
                        if(!empty($getuserdetails->area_id))
                        {
                            $userdetail_array['area_id'] = $getuserdetails->area_id;
                        } else {
                            $userdetail_array['area_id'] = "";
                        }
                        if(!empty($getuserdetails->area_name))
                        {
                            $userdetail_array['area_name'] = $getuserdetails->area_name;
                        } else {
                            $userdetail_array['area_name'] = "";
                        }
                        if(!empty($getuserdetails->building))
                        {
                            $userdetail_array['building'] = $getuserdetails->building;
                        } else {
                            $userdetail_array['building'] = "";
                        }
                        if(!empty($getuserdetails->unit_no))
                        {
                            $userdetail_array['unit'] = $getuserdetails->unit_no;
                        } else {
                            $userdetail_array['unit'] = "";
                        }
                        if(!empty($getuserdetails->street))
                        {
                            $userdetail_array['street'] = $getuserdetails->street;
                        } else {
                            $userdetail_array['street'] = "";
                        }
                        if(!empty($getuserdetails->other_area))
                        {
                            $userdetail_array['other_area'] = $getuserdetails->other_area;
                        } else {
                            $userdetail_array['other_area'] = "";
                        }
                        if($getuserdetails->mobile_status == 0)
                        {
                            $userdetail_array['is_verified'] = false;
                        } else {
                            $userdetail_array['is_verified'] = true;
                        }
                        if(!empty($getuserdetails->customer_photo_file))
                        {
                            $getuserdetails->customer_photo_file = base_url().'customer_img/'.$getuserdetails->customer_photo_file;
                        } else {
                            $getuserdetails->customer_photo_file = base_url().'img/no_image.jpg';
                        }
                        $userdetail_array['photo'] = $getuserdetails->customer_photo_file;
                        
                        echo json_encode(array('status' => 'success','response_code'=>'200','customer_details'=>$userdetail_array)); exit();  
                    }  
                } else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '103';
                    $response['message']    = 'Parameter missing!';
                    echo json_encode($response);
                    exit();
                }
            } else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
            }
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';
            echo json_encode($response);
            exit();
        }
    }
	
	public function get_all_areas()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            $areas = array();
            $areas = $this->customerappapi_model->get_areas();
                
            echo json_encode(array('status' => 'success', 'areas' => $areas));
            exit();
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';
            echo json_encode($response);
            exit();
        }
    }
	
	public function editUserDetails()
    { 
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            $handle     = fopen('php://input', 'r');
            $jsonInput  = fgets($handle); 
            //$jsonInput  = '{"user_id":"57","name":"test","phone":"1212212","photo":"","area_id":"","building":"","unit":"","street":"","other_area_name":""}'; // normal login from mobile
//            $jsonInput  = '{"customer_id":"10","photo":"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANcAAADXCAMAAAC+ozSHAAADAFBMVEX9yaYhISHHZTwREREiIiL1uZZyPS2sWTz////AY0IaGhogICDytZD7nZIcHBwUFBS7YEASEhIeHh75wp62Xj/tq4aGRzIZGRn8yKUWFhadUjh1Pi2PSzS+YUGxWz2ARDD3vZqvWjyzXD2oVzv8xqShUzmZUDaWTjYbGxu4npaVbWH7xKH1upf52cb+3MW4Xz7czspVRjytWTzwr4v+49H8v7ikVTr4wJz9za3ej2jnuqXys5B5QC58Qi/+3sjVq5yLSTPVflbKakHppH/zz7v2vJkYGBj2u5j90bSLbVv+4MzNb0eSTDXGn4TDw8TsqYUuLCvk29f28vGMYVTisZy0emE7NTCvnopXTEZ6RTPirJHjmXT1wJ7PdEzK1+GMf3jmnnmpmYX+5NLx8PBuaWb7oJTKtq/xso7t5uR6STrYhF3Sw73JlXq5X0D8v6GUYEtKPjbskoiERTHNfHXbiWP97+aDVUemhXvapofSelKOdWOwkYi8YDvAq6TCim/+6Nq4g2nglG7urIiSXEf9+/nqsI48PDyShn57QS+omYzztpL8t6/il3GlgWvT0tLtxbLJppm/knfsw6mWi4SIiIjvvp2xo5JmVkyde23DYzzW4Obi6e/RppaBTTy9oJb8sJr7ppincVq1tLShkYBpPCrAqpUxJiJbW1ri4eHxy7e2qJeMV0NLS0t3YVPRd0+1b1abj4iwYUaeZlDRnYD917ympabXhFyIa1n21MK/l337rqWrinX79vRXUEzrt5jBYjzu8vbIwb+WlpbBdG3VmXvYuayjalO4lov+9O32+PnjpoT1m4/chn7htJX9zKvSgHheNih/aluteWPBgWqvaE5BLifqvqrgoIv8uZ5+eHXKkXbt1s1wWUrjzMLfwrOUeWjFu7W3i3nOnYXKno/Wy8f04djPiHjji4JTMSaag3VjUUbJsaLHeluicmPd1dDJ0NbSp4vBl4nsz8Hhl3COUTw9Jx+WZVa3nJT92cGUbF2zf26xiG/hjoP919POh2h3Rji5fJrrAAAi70lEQVR42sWdCXwURfbHOz1MdUhwJjMTZ8jMZCbJZHKzhMRNDCSSgwQhWZIQuSGiILcILPcpp8By/lc5/6CigiCKgAeKuuB9rjeu97Ge67rufa///6uq7q6e6Z5MQ/ewPz/m6Omeqm+/V++9qq4MXLIxFSFfqLu5Sq9FqGOKwX4Z4xragdYHupsuYT1ChQa5DGIF07snQGn5Rk3GGcRK654YCUE0ddJ/iYthJchkC/4rXAsSiQUK+1DH0IvPtQT5At0TqgD4YvvF5pqUaCxQmhvALi5XO0Lh7omXH6HSi8k1dD7K19m16zONgOWBxS4iVxGq1devzIDQ25jFGFjiuQrR+jR9I6RMELK7/xfAuAscXIKuPvUWsDKNgtFwn3iuDpSvGwuUbjh4FF0UrkJIyPqxDDoiKC2IFlwErilTUUBXIBQkpRkF86FJiefahfJ1hgxJOYarYDR1SqK5SpEvTU+AzxZklXU3Ki/qSDRXh75CI4cQHQuRb70Ng7lRYWK5JiG37sH11tMv5jFHNKR0H5qSSC4ooIQunE/+AQ+uiidyXcBFU5hRhVFHIrkKkbsLonSlF+af5DiulqUwg6pFkxLHNXQq0uxib+ppASkWAkr4Bg50IsRSmDGlo6lDE8ZViPzaGfh68k3iCghCqGYh5hqWbziFsdK+0BQu/ebqLQ6gnAAzVy2YC3TSzVKY4ex8HgbjDJmL1UtkjAk5srnCaAbhWugTU1imGQZbkCAuTXNdL42fdDE8ZMKB4D6OCnlZ5DCq9WhKQriWaZkrTQARB8wW0qVgmI9uFLn2+VnxazzWL0kI13x1wUszFQARM10vmasajRW5TtSw4tew9I8wzmCpAWWgxJWO+06/56ETnKgbUQWLHMZH2LIEcBWhkHYZSA2Vjbno9yBEDVELUb55kSMNzTefqx2tVydL5mSZ+BsdbxUIcbLQHMG8yOFH7aZzLUBhdcxgXL1FLrBgC7qdcd2DBPMiRwgtMJtrKMy7NAdXqJYA5YhcEEdqsBuyAZZvauQwmwuCvPbgaqFcmDFNcsOxjGs0cgvmRY58NN1kLgjy2gsY1X4CRC1Ck9cJTqFRPkGUCZEjgBaYy9WO3JqZS/AiwgV2kszmRsuVXLejPMHEmmOquVzqqBEQQJjCi4HSKRdNyqOVXCdlR8w2xRFLTeSCSj46avSWsDBXOh1r6eQoGsVx2o6YZkZEXGIm1yTkV8VCikW4crD/iXhedEMk1xso38TIgTrM5CpCglYs9KNRJzBXNvY/isdqXhYRg3ojR289K1NTzOOagnxaGTnsQzNuxFwC9coAxvPL2Yul5gqdkSMnTUeNOMk8rsLoNV7qhTXgcpSL/i5g3DloYRTXDFQrRY44BgvE91QBFZrH1YECGnVhPhr1BLcctQiysHcGERelsSxy9I7DFT/HpaMi41ys5NVKXdV4JC2E/MWk5qIpLF9fqNfzdHM9Mo1rCfJqBI18tI/DXO5IruobVFxPoKC+UF+mI8fVonazuOaj9AhziQAkQIylfWZiYZ7pBhiEVLI/azqcnmLLi5aZxNWOghqVhhfto2kXSUShmFwL1QZL1wLQk+MEtMAwF3ND9axrDlou2iJMD4SpTejkSxXqvVG5OV0LgM2rDWZmTmcprzZXBRo1lpYTUkDM91L3vFGD66QqN6dpDTVdD5WCyCgXi4bq6YkfvSF1eY54hHIFGZd6hDGDZWolM13VsR+VmsK1DOWrUzKr2sciFKKO2cK4dBgsW2MosRFoMHBw+mpDtbnC6B42dvIpkJ/iAZemwfIiDZajUVbpqo4FtMQMrqEIaUy7alkVuFw0RZD6o/+EJtdCVB1psDQN0+irjlGRGVzTUa3GEpSPrWGMRbSuXU073nIDp6kTyB9psGx1vciqY4OBg9MV5dXm8irXMG6nde2NdJx5JS51lVgRkcNy1D6nb15di6YY4WI1r9pcbnRSWSb5MNFyOoJC93DauhEFI4qONHVU77I6TmNTlVLjXLBuGF0ZgqoheTG9QQwmVYo+NRILMFTXy5G1LGKIMW4N5ZzHVIU7v+GVKVB5I4uKsfvwCOOQjwZEQNbUaGJXNg9LV03JuqyOA2yqssA4VyHyRpmLRUPW41FB4DpBHbFlIRfXE3PYXQpocOVobkw6j4DInV/2KpPcEEXZZOGoFmHtDOqIYWDW1th7kF9pkZzo8BdgqUDNxQ665xvnQki9Ra1CXbOfHBV+cewoGvDViZnZFeUpPDGTMTIutcFUBaUfGeBi67yqZlvQcnWX//xbbjk1x1sxueAMCPbM/QIio4qrLFODK10REI1yTVIUh5lsqr+WU+mWY43cDSQw5Llig92OakLMJGnsR7ZuEuWc7DVFQJxulKuQrV+zVtE+l9boaeSeGIUDfuhFjkk9xNyKLJXNfmQtaBssneXrACo0yMXCBuuEkIeejWWRk2SS+UNsLkCXwdLYiE1jA1hlMMbMAuISo1zzkdoNa9F7rpiRwQ1udpzrQqMpmJiTAyyMML8kL6q5KuTAsb7IKBdar9qHLNSgtYxLHT/gjB5xwPJlmrTIEkNg6q3i8sqH3B0GuUqRW5WUBXSP1dVFt08IwtNcV5qBZDD2rjnM1cXXVFwt6expkUGu6ShPVQx40bOMS2sEvSX8losH5pVo0pT2AUqm9Gguf4AFeoNcrIpiHfCj5YxLS2OPHefigfnCYucZShoLiCwkKrlqy1igbzfGtQQJqigfRC8yLm2wH0p0g/XOZBzs5lHmKK6gkClzlRrjKkIBVS3gG2UFrq6Vy8XR7TJYWrrAnrYQSAYaxRW+Xg70041yqYZXBVqNuQyKgZWllSnAylQGY1zufPkAKjTGNRWphlceGoa5TASjIyxEwAKCUpkR9aG/NkcO9Aa5UFA1vPzoPas1izMTjBgpHMZgUhTxsuDPuILZMtcCg1xulr1Y2DCHi7sB18BMtQBGRSmx0pRc+Ug2YH6RIa4pMhfLmNXIapSL1cBKsFCNEswtlSGMy4u8UuDIM8ZVyrY3CJLQ6iguV/mmKl6puoc2bdQHFmkxr08B5vdGGSxTCCO54vB2GOTKiw4bYXRCybVx0328pu4r1wHWmh8B5leAVQSjqym4pe6AnMDM4WJhIx+HQ+tADquxqh4I6uvqqlZWMT20qJmQNY+PEzUb6ni+BVUrjBSsZpRzvFHlL2TOmrKEcbmhigJBt8rrcOerGso39ohW+ZiHeEy8KTaZa1M9oV+NFEYKVTPz5ddEJecyCFkh8bd0s7hYtYGeJlyNQHVf1SqJRI32p+auvLGBUI37U48ezyrBwj4GVp0fmZwDcE/lqQpqN4crWx5eyIoFd/uh8T261hhMtknTWNic9VXlcFIUmGLAtVSHmMEIlx/5cySuUnO4WLujgKrfovqHGnrEFfFULS4ca+ZiKjVYiwwW8vlZciZcXjRHiiPrjXKp1gBWA1ZzXWsPXfrTpvFaXGAspbGX44kma0ECqxWXvTPlhQDkk35xm8LFwnwQuMrnjm8t18dl5WKoMuK0YUowN6quECtst9Jg6XhwV1wvck03g4stFCF0u7XcOlnkmrEdq+hhrD4a+mDzK5pUP1kcdeLbBw++/fbXd2KNqkWvveauoIQsOROuIMoXIYOFpnCly5MUSF+gVeX/+qBPkh6N3HVLNNS9w/VcePBtBGNNkZzTcM1dK/7sN5fLK3ItnJUqKikJvvQZiTVRoUOHjq5b10bOGHlaSfXKYjhKNXzdupFMw7HJ3j4I7whvSXXwa9E9e4tceQBKHzEZ5cqPrOb9lGstwRq++Ju7vu3Vq9fYS8c/ca6vqHNbtw4YcKoX1keTNly9jpy48xFqtN8vHkk6POvdmyb9sZdCAwYMOLztb3/b9qYgDJsx49Oih/v8hZz4tY/FeuAKIUTzWQ5wGZ+nsLRcS7k2k0Z34h6dOnzuoeZFo/tG6qq+5w7Di4Mv+2h4KlHbzsWLX5eMvPPUZZddRnm2bj3X9ypRAPaGILzZH2vbhjZiMWXoIEuXXuKI2SZzBTHXye19SFc/6kU1tkePAey+g06dGrAVenuuF3T/QwCL0r2DLwMN7nX4HMCcG8B0eH9RTQi4CNhEarDX8mRPJJHEjx0xU/AmgKsPHQATD/cFlwOt/VMV9kMg+V+s/v2ptQYc7nsY+n8kGmv4FZdhneoLZ50Ci52T/Bfe7VPke6O/CNZGh++d0po33RQdxI7Y2ywuFuYJF+nft5cNoP5zH7+o70f77zqy436iHUeObPjwj1dtBav0wgCkfyM/GDRoUB/iiN9QrK1XXNZrwLm+z+y/6667NoBG7N/Wv/8P2e5/7P9fCvY45XotX1zToX+P6iOLxInkugosNBrUd+u85OSzO3Yc+TnV/Ud2bDibvOb9vkCFhfs3fKAVRMflNnoYqM+d+yL58/t37NggXXhk8cfvP7P/uV8Tu18tcolVR4A4TTUKQzrLNso1H0VwhRVcRw+PxroKoEAHRuz45OzPb8IiXHBo3tYBg7F2wsmvW60lWY3WtfjC/oOpel31BZx1dsOOHfffRHVkx8evw6HPnyNkS0Q/RNLfQKeTAZYv5KQJRrmKIrm8wLVa5Hp88IDR74z+4xp64pZrRqxJ/uRqrGt2jLiDboh5/7DE9VlJMYT5nq7fMK7D9NLf3THiyN1nr74J/7djx+Kb8LE1nxc+9+tfP0e5fD5pMb8Ml8VAmQ2AYZO5gozriiuuGLBCOvGFDRuWTfo5xrppxIgRO8SjX5yCk0ZiNxQXgE9jP7wC6zAYi2jDNdOTX5BuyOuf0IOPfb7/uSWiHyJSdcgFTzXdrW8qF9wttM864x+Ui2jF82uS6X2/ZsSIO0AjRlxzhzTpex6fgd1woPQQGn75mF4nNbEFLrzj6nffxVjXfCNduOLUVmKvt8FcSLmVvRrvOTDMtQsFlFx+BJ5oLV+VBHr8R5IeGLzieQC7BgS2gq/UC9fMG4xffQbO/aySE7UzKWknueiKFWvENj65g1yHL90wAdtq3oof0QtBH/zw5moAq1BU+3kmcBUiIYqrGr1IuUbKXJdz3APwbcBdVN+umAe6Vnr19aSk4QOLJa7TcOVH4iuD51Fbt28AMtAdd32x4oorr5Qu/Ba3Mmiy1bp8HwoqJtTuhHAFYX2j3yzc5OWSoLu/vDym2pKSPrbKXBz8+s2VUbr8f6jgJ467XDr6DW7kX5NxOboPjCQqhGpM4QpHcdWiG60QOUCvcHq0C85cmMXWpe4F893C6RCcCdpebgW9h3wh9nQ7xLgMPodlXCE/Lnw/wE2e5vQI5lq/sTYyrt/DlYt1XglaCPYCnUButgCSb5yrNJoLb96AugFajHfb2Xh6xFo5UD7QE2zdpufKXaSNjZRr7Sh5n3qYIm4zyOWP4vKi1dBMG25050+4eLqFmCurpIQd+kyXwV453UbDRj8r0dPME6urBdC3BrjkByosLwsCJDDrxvfoVH746Ti9mwXGWWgtnqzgKsbjZlec6+AcrHsnr9popRqG5kiRnkT9zw1wscI3W8r2wIXw+saLYsswFX4llq1O34vv+WZ4SHEpcMlai6PprMW/50DaizojxffeXr5qlVVUXRC1iDeXDLBSE/bbsPmXj2xms1qhwReLfiOvzdy7+CdKPQJz45HUooBlLa6M4CoGMHoZzKF3RVx4ejFcJkK9/sjAyatWlUtczUt9KCx2Yo5xrg4UwQVDFicwAtbPunbzB10vLbXdu9BqreT6YS6mSuvmPnEWsQZ9NhA3wrD68fzT0oJprQ++TDDGVRTJlUd2fVlBk1dNpi0ujAHX1ufej8Gy1saexZdGcvVstFq3D/rNLE2kPoMeASSqcmhCVAM/zir9YUUYx0Zz9unlCLJwAqOqLJGbXbh98yCFNm8/Kb2S1ZPbKHIxsCz62trt2wcptX27lamkstjKtJKvw7GjlkbEWuEtI1yskEpnXHkSVxbH5bqyrF0qCwooMFcUFxyLd50rF97dyjSNr4Kvt4s7vquFYYa5vJFcJIERiX3tWVzZaNVSY5aLlIWTGZeSzJUV47LK4p7iKVamcXwDfOXu8YXJk5aKNw3v781jXGy/DRFpXoIrdilVXCy/WHmpikvPZSCXgovn++E7OdZNHoq5W35tkAtWfCkXEzwBoyrmdKh4Feaq5C5AlVZF2Ggmnt/4iyAOihU1xrncdG2cyYfOh6v8UqzxF8KlGINj+EWEaxNPwdzPGOICqbhIYgbp2yRVQrDqGi+EqyQ6bFTCg87doeAcMNgSw1xIzfW0FBDjyoWpGhZt+uXlF8ClChsuF88vBWNBdXDCKFeHiqtW4opvhNxyjDWu2fXAL88fqzg6bFiLW3k+R6Bg24z/3Q3lKlPug6WKf8cJFt96+d85A1w0bIByq3iedKPWLZS9b4xrgcSVri44cuP1DHvhfXwz98ADF8Dliqo2wD/q+F+Iqzdu4fhjBv/+K0/kypYSs8xVrCMWjuH5Ku5HVxoLh3X8Stwc4xK8buE9A1xyYhaE6wOKfea6AmLWpZRrEwdLceevgYyrmW/A5qJcVBX+0DMGuKQExvYXs0LKWqkjdTXAxhwOsIyEw3Ke58dn9aRcIYEqlC98YYBrCgpSrkyJKyRzZcUdXaDx9XwDZyxsjAGuehx96yHMe//Qn+jD95+ZYYALEhjlYrEeuPQERCgMKRjcbGNhYxpfB1Yn+3TgSSX7dIAJawxwzUdpmKtMwTVKajA3zvCigui80VB1OI4vv6oT9CHh2mX8821YAhMCyoeWVh0BEfyQgc01Eg5heP3Pnr1nXnhhz28x13zjXOwT57JztLhcceMh1dw6I2FjPF/3O4Dq3HvmLI83fw01zMUSWCAd2FRcWXGG/sB+k6nOf4DlMq65fNWe5Bf27NmT3InLKNh6aNbnOVAuKYHVyFwDuYSpWDm8WjvP7Oncs3dNJw/Nm/B5RCyBBdIYVxC42JQ5Qaq0KpbYXFv27u08k7z3k6WYa0myeZ8rEshkXIhxFSeMq1GRve7jDne+kJy8t/PNdNx8kREuJoQDfU53TS5XwriUw4ufu7FnaWfn2eW7BXO4WKBPB66wmivrIgyvZl7S7oBJXCzQ49zsZ1yyEsXlUmav3hjqvU8gyJvFxQI9KChv4mBcuQniYll5JdS62b2X/uLs99mCqVzTxc2VaUjicree5wDLnTmzmLrXzJnnu2ZTBzWGcPyf+zvPDPn0LcK1yzAXC/QgISg94G2Zez4DrPjW6y4BvfTOO++8BD9dd9uvdGVlFuXBTsOG7Dnzwt49zxGuQnO4hopceUF5U0AzezoQt4e3XqLUj3/8s+suuS4umUsR5XEQBLLOPQf+KpjHxZ7tud1SWhb4Vr0D7FeXROq2U//5z8/g20wdw4u5obDsmDCk88++/cMEIWxOHcWegSG/tEMph2eO6NJvLCbsjV2m9Cg3fKvzU+HYp3n7Opfh4T3FVC4B5YnPHXw5PHPExq6MBcMptm7N1ZG9xuAVjeeG7B2yTDhe+P2ZvdP/6TXt80ULCZcXhcXnX0FhN9/QtSOO/fLLdyjVSy//FOvl2eSXr16lv71Eyd758ssnui4OF+GZyf4tZ/acPS4sG7K385PjLUXmctUiabnXL/Tmp8UpEd/56VNPfTV79ss/ferV2UQvvzr7kq+eeurVr/Avrz4FoLPh158+rXn5QJaUd5cJYQga+6Hl/d9gP/QXmsU1nXD5aqTh5RXKeL48XqQf+2+wzauzX3r0xyCIgi8B4+zrHiW/3PbS7JfhxX8Xx3PDaXg9voXEjbe+P1P48XeCMKfULK5SUtCjOdLwgq9L+blxI+KtNED87MdUj1731UvwC8GSRli8OQqNGvSh8v7OvdNRELiSzeJqR2R45UtbRuFrNl/fzxpnFTFXjBrERITnZyKgFPNj3JGeVklVYC76J7/H939/pvP7fcD1V6NcTJgriCqoG5KwmBdmBiuJNbmcKQLc9igFEqnixkOXPLrqd2djLj84Yufe5L17HgwKFX8wlSsd1YjFYTXdGrKUHxM3hc0Eh5PRsB5lVLFrjp4lkheOg5yMudxgrzcgGJ75PySEPzSPyw1FlLg7yU2zc7UvwPOtzGC687LSWPHMJa3GkwLOfz9YDCHh2GPmcb0B0RCFaNQg7hiGIJLO10tgleeZm69jVGrllsixcHdA3NKLtzZ07FmTDM4C5jKN683uIeSm5iLfj32Hg8hSiB16ljlm3npbRIV468xcPaUhrBKnS4U2/n8B+diM48kmcv3QXfxLxzD+2Lhj/ZOnwiDDYM0iWAnraQw2WXor+ZV4zVrmCvlQO+HqbybXl4KYvILIP+yjx5In0UFW9gsAM3mdI1eJxbj8dJKM/vyYmVy/daMKmrvmv08KYbAaAdvNjxPBKs3B6tkolbuQuULyeiWYazrh2p9sJpdA95F5fXTq0y7NWELe3Xxzq5kriVkMC9qT1pd9fjSfPoubYibX0PVkQ2PYJ660LvCFpCeGgd0QFdkQMyqXEkt4No9yoRrxH2AuLUo2k2sB3URWI2JNoTHk2DOr6RirH0/X6nuasmjIsI5vcwtibkEUqHSSmVyTqkWsDvr7Lj9A9X+//a8hMXiIlUejITCWueZSLOGjbdU0LQNXO+VKNpHrD27ihIA1lL75d589A1GpfR+mpeGeX8lih2GsMWIkPJa8DYVoOGQrNeZxPfaH4zgS4rFFRemmA5YksBidZrpMCIXTpHT8IXARh69OzL9Hv+Z3/3z2r/9Y0B7pmz5oknEBGIn3xQaxoNSVsN7E81k/ccP5Q03lYmRDH1PvjhWUXPU8P84YWM8sCWsprQlLyYIsKd2WJRvh0q+huyBARnAtaqg3BkaxWpt5cXCF55N5epBs45xycbjaOyhWyNviD1YjhLw8b22th0R24WmskkSMer6OcoV9kySuPLocn3iu6VM73vysf//vviuS9BZwWVvH8c1j1GD6sfrV4bgKXBirQ/wnGLEbTk8kF/PB+ap2rgIu6Ne0en4uBiu+ICcEHxwHBgcunCqn0FGcL4RIBZV4rnYYz5pctGuLyLbOC4iEY8hNIVyAVUgLAFQBpfYkA1yGtOU05QKTLeKbG5jF9Fur31y+nhYtfHY1kvJVRw0kL/j54nNNGHLzUafFksdb2SR32vkl6NxGMHQ99kHKdafFYn/85iHY5cENvag08VwHhoAmkB/vHvLkzYeOWkD2Jo9l6Ti2E3cciYuV+osnUmJU9ZPW4++0eDIcFtC6Q18HhTlLiEdAuwcmJIzLEim73ZNi69atm8fC1+XSFXUSPsBk5fjPiPSouMQK+xObx0v7d3Lr7rTAWzoLPB57VHNPJoxrncNms6V4sApsqd0keQ7ydTBKmMmaYeoCXdQz3yLRfa5krMqeXF2exSa9sc3mocLNWg4kjOtdC8CoZf8LX0VuvWyyKh7GS0mxjoiBz2yQnqThC6oIl1oey92JG1/aTdpf4zeRoVLJ9lws4vlF5ZXxdruthCJlpbS46iKeu4m3eLQayXAmLm7crd2k5U6+VexpI9szOA7IGrryxZKqeogX5ewP4IjKeVuGViOOowmM8212jRZtFj/fKHmWq4Q9QW3mISD0jEU1jSfxhbkglYv/2qHRiNNycwK5Jlo0ufLqOZCabPwiIJvbqoZqHA/Rol621UDlSGy+U7uRIQnketLi1OSCrdJKsoEsNGKr1M9t2Bjxqbj38cC7sh+zlVJVmlwey4QEcm2xpGi5iL8hKs4pxlm/lWA00H11VPU8hprWIEX24uioufE1LT+0tyWyjjpg0RrTFuRSlxGVCrQx08bxsuoXVbVK/gchUK1Zdq2w0ZYoP9xyMy6bHFpcD2tXSK6sEivzSFHymKokUDq5nNC089CTW7SC9BCiLrkmQNVHdSCS6clDTmDKKGiKyszOgia7g3Gp2YpdWWpVuoqVTLmRgMOhIY8tciAXWAoKcNXYNvHBuyM6NnGdRdTEu2NxDTkEfbcTOSzOA1FMKbglm6LIScmwkyoxA7gMKWqEHcTN454o4DIs5C6m4BYZ2wTocFOKDasAXnlXzUVPcjTJVZ8zwzlBySQdxoEj1eaRkEjd69ipVSPpxooenBYPbcJBZwsFTtyE7Js2D2N70JHC3MfmsKyboObass7iEU/C7BY4izExWcSbSdujss/SMoJurKgzb5FjbipzCVboKNmoD2bYxNMzLG13R3NNaHM45Zcd0HcsO2FisjU5lP7RJVdlrk4njC4iHxFdnfl7E6aQ7i9jE/tot8jQCjBOxFoHWFR2eIsmi8PT5IiOfQUWcOdUjZLUpt1fPcpVPYXZrJn7HepQ7AQnBRukAKAE1kRdkXEdkm9SCmClOOxO/EWV81OpzVXHb9F6npqra8lG5a8PWzTnDLYm1XHwJ1sBfIFo4pQtdlTJNUQmTnU4UlIyLE5bSop8jPW/m8ejWbs9wqllzdKDpT6pj12znE/1WFSs0NWkDLCDx9Ikdd8uRkVOnAizPjaBpTzdMFyBqv+2ggzNWvtjLS5rcXwsjWeBwzWnKZZudsYrGyclxUkMwV5LdVgOyFwPMgSPPQUs5SRcNjWXU11vxAr0cVd+AUsL3aLpEnYNLg/mAjoPGEzRSecEzBU1rbJDnsPvgJ02OkQkQZMQXtTKOKjJFc8TK9lynHY4ZEpp6qbmTQHfcgKIBzyMHW2yTBS5Dig8LgNcFieQAuBS+ZvDDq9rtWp5RftRcXHXWFoh82G4nVoh16bmstHg7/DYUpS2dFiGUK6jjBZ4bE58x2waXDYwYYFdc4BtVtW8JWxDWKw1m0bd1XyqxenR5koFA3lSUzIiDh8lXHdbmpRZMMlpceLvDlUDOJRAC1oDrE/E7KtSnlhmxR5bDEtVbahUYMfBL1WbC2J0QcRrdsuDwBU5CS4owLFHkyspxQPTL7tWu02WW6QPq8nCTEyVsRavWVSJl5VB9gInKX7UXEnwrQAKYmeE+7QBF3ZDJnyC3QavqblSYXg6Ugsc2o6ImVwDJ7tax49fWVW1skGaELtiLF43ApZeN7Q5SLlg0+DCYdEJt9wZmQAe5JInWJqUNknCNtPmojnNzoJMzBKxvLWqDh5nTVsJdMWai9fay9wQDTXfPQV8KyUWFz5so68xg3GQvBTHUsnPeKKgyQUGg6iYqlVyfMpJYuszc2GBZtEYqzoQZsXKbLMc2snLZrFrcuFDqbTfkQbjkg85lKSp9Ks2FzVYAfipSvaDnKY2NlTVPbQxMhAOjBn+P7XYtIJGKpirSZtL/jnqFW6CJUPJxRzS7lDFQ2IwYM5QO4sTag5dys1yxQz+vwfLqNQE3kOq2xRnF1xRrzm4yDV35mJa+YsYDG6DOjvjpgfpn/Rr65aDKhen99DpcHig7SQ1F7ND5Isp3LuiWVQQaq6kAnhzO8mPHhZrmE9/vgI+dehC9csHBj9ucaqxbKRIbyLmUnOldtOWk2vL0M0FQVIGK8hIiu6Cow0KznkrHrjyvJmufGDFPDZZYkpy2gHGCTNdaLdAY94HJowhzpISgzhDo1CzUTBoDUePpGhPvFn6zO5rwXB6zXTtiuelp4ZRDTptuCGbA7BAqVpc3WJzObVfSGXzFGV6w8pwOGwYzOOMJLPDDEHWmnnXXvv3K7sy0t+vXTFvDbvgQaW56KNYjIWXJFKoF6q5bLG5HLFewVzanohXFArwjYTQqySzaT3LeX4eAEYIcOZp7Ko+SszFqMAxcOgFY2EVJGlxOWNzsciq627YUojsFAw3aLM5nam0VYfTwCYQsexJTXU6bbgVbK1UMrQkL1SvA6TG5vJoH2dLu2pPpGAZgA4uQkT5bFCXXTDXREuKDXhkeRxOGgiJNB0OMmxsLhZmVHMeTWSp6Qx8fzOwxZg8MPO5QE1wRr0VnpjLWNpxz5HRBVfsoce41AYTS6pUuz1FKbv66Zv+J1ApSuHIlCFj2XR2kOn/ASBF85alK+BjAAAAAElFTkSuQmCC"}'; // normal login from mobile
            $json_arr   = json_decode($jsonInput, true);
            
            if (!empty($json_arr))
            {
                if ( $json_arr['user_id'] && strlen($json_arr['user_id']) > 0 )
                {
                    $user_id        = $json_arr['user_id'];
                    $userInfo       = $this->customerappapi_model->get_customer_by_id(trim($json_arr['user_id']));
                    if(empty($userInfo))
                    {
                        echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'User not found')); exit();   
                    }
                       
                    $userDetails                     = new stdClass();
                    $userDetails->customer_name      = $userInfo->customer_name;
                    $userDetails->mobile_number_1    = $userInfo->mobile_number_1;
                    if(!empty( $json_arr['name']))
                    {
                       $userDetails->customer_name  =  trim($json_arr['name']);
                    }
                    if(!empty( $json_arr['phone']))
                    {
                        $userDetails->mobile_number_1=  trim($json_arr['phone']);
                    }
                    if(!empty( $json_arr['photo']))
                    {
                        $userDetails->customer_photo_file = !empty($json_arr['photo']) ? $json_arr['photo'] : '';
                        $this->load->library('file_upload');
                        if($userDetails->customer_photo_file != '')
                        {
                            $filename                           = $this->saveImage($userDetails->customer_photo_file);
                            $userDetails->customer_photo_file   = $filename;
                        } 
                    }
                    
                    $userDetails_array                     = new stdClass();
                    $userDetails_array->customer_id  =  $user_id;
                    if(!empty( $json_arr['area_id']))
                    {
                       $userDetails_array->area_id  =  trim($json_arr['area_id']);
                    }
                    if(!empty( $json_arr['building']))
                    {
                       $userDetails_array->building  =  trim($json_arr['building']);
                    }
                    
                    if(!empty( $json_arr['unit']))
                    {
                       $userDetails_array->unit_no  =  trim($json_arr['unit']);
                    }
                    
                    if(!empty( $json_arr['street']))
                    {
                       $userDetails_array->street  =  trim($json_arr['street']);
                    }
                    
                    if(!empty( $json_arr['other_area_name']))
                    {
                       $userDetails_array->other_area  =  trim($json_arr['other_area_name']);
                    }
                    
                    if(count($userDetails) > 0)
                    {
                        $this->customerappapi_model->update_customers($userDetails,$user_id);
                        
                        $cust_address_Info       = $this->customerappapi_model->get_customer_addresses($userInfo->customer_id);
                        if(empty($cust_address_Info))
                        {
                            $this->customerappapi_model->add_customer_address_new($userDetails_array);
                        } else {
                            $this->customerappapi_model->update_customer_address_new($userDetails_array,$user_id);
                        }
                        echo json_encode(array('status' => 'success','response_code'=>'200','message'=>'Profile Updated Successfully')); exit();  
                    } else {
                        echo json_encode(array('status' => 'failure','response_code'=>'103','message'=>'Profile Not Updated ')); exit();  
                    }
                } else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '103';
                    $response['message']    = 'User ID missing!';
                    echo json_encode($response);
                    exit();
                }
            } else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
            }
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';
            echo json_encode($response);
            exit();
        }
    }
	
	protected function saveImage($base64img, $is_proof = FALSE)
    {
        $base64img = 'data:image/png;base64,'. $base64img;
        
        $base64img = str_replace('\r\n', '', $base64img);  
        $base64img = str_replace('%2B', '+', $base64img);  
        $base64img = str_replace(' ', '+', $base64img);  
        $extension = str_replace("image/", "", substr($base64img, 5, strpos($base64img, ';')-5));
        $base64img = str_replace(substr($base64img, 0, strpos($base64img, ',')+1), "", $base64img);

        $data = base64_decode($base64img);

        $photo_type = 'user_photo';
        $target_file_name = $photo_type . '_' . time() . '.' . $extension;            
        $tmp_upload_folder = $is_proof ? 'customer_img/' : 'customer_img/';
        $file = $tmp_upload_folder . $target_file_name;
        file_put_contents($file, $data);
        return $target_file_name;

    }
	
	public function extraservices()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            $getextradetails = $this->customerappapi_model->get_extra_service_details();
            
            $response = array();
            
            //$response['extra_service_data'] = array();
            $i = 0;
            foreach ($getextradetails as $details) {
				$photourl = strlen(trim($details->image_url)) > 0 ? base_url().'images/extra/' . $details->image_url : '';
                $response[$i]['extra_service_id'] = $details->id;
                $response[$i]['image'] = $photourl;
                $response[$i]['name'] = $details->service;
                $response[$i]['rate'] = "AED ".$details->cost." for ".$details->duration."m";
                //$response['maids'][$i]['photo'] = strlen(trim($maid->maid_photo_file)) > 0 ? base_url() . 'maidimg/' . $maid->maid_photo_file : '';
                $i++;
            }

            //echo json_encode($response);
            echo json_encode(array('status'=>'success','extra_service_data'=>$response)); exit();  
            exit();
            
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';
            echo json_encode($response);
            exit();
        } 
    }
	
	public function cleaning_services()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            $getcleaningservices = $this->customerappapi_model->getcleaningservices();
            
            $response = array();
            $i = 0;
            foreach ($getcleaningservices as $details) {
				if($details->readmore_link != "")
				{
					$response[$i]['readmore_link'] = $details->readmore_link;
				} else {
					$response[$i]['readmore_link'] = "";
				}
                $photourl = strlen(trim($details->photourl)) > 0 ? base_url().'images/services/' . $details->photourl : '';
                $response[$i]['service_id'] = $details->service_type_id;
                $response[$i]['details'] = $details->detail;
                $response[$i]['image'] = $photourl;
                $response[$i]['service'] = $details->service_type_name;
                $response[$i]['readmore'] = $details->readmore;
				$response[$i]['show_status'] = $details->web_status;
				$response[$i]['material_incl'] = $details->material_incl;
				$response[$i]['warning_notes'] = $details->warning_notes;
                //$response[$i]['readmore_link'] = if($details->readmore_link != "") ;
				
                //$response['maids'][$i]['photo'] = strlen(trim($maid->maid_photo_file)) > 0 ? base_url() . 'maidimg/' . $maid->maid_photo_file : '';
                $i++;
            }

            //echo json_encode($response);
            echo json_encode(array('message'=>'success','responce_code'=>'200','service_data'=>$response)); exit();  
            exit();
            
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';
            echo json_encode($response);
            exit();
        } 
    }
	
	public function getfaq() 
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
			$faq[0]['question'] = "Is it possible to cancel my maid service for a certain time?";
            $faq[0]['answer']   = "Yes.  With a prior notice of at least two days, you can cancel your maid service.";
			
			$faq[1]['question'] = "Do you provide the same maid on every visit?";
            $faq[1]['answer']   = "We do provide the same maid for every visit if you are a regular customer unless you demand a replacement.";
            
			$faq[2]['question'] = "Do you provide any cleaning equipment?";
            $faq[2]['answer']   = "Yes we do provide cleaning equipment such as Cleaning Spray, Vacuum cleaner, Mop, Floor Cleaner, Polish, Microfiber cloth etc";
			
			$faq[3]['question'] = "Can you list your service locations?";
            $faq[3]['answer']   = "Here is the list: The Gardens, Discovery Gardens , Al Furjan, Jebel Ali Village, JLT, Remraam, Damac Hills, Studio City, Mudon, Motor City, Sports City, Layan Community, Dubailand, Mira Community, Al Barari, Sustainable City, Arabian Ranches 1, Arabian Ranches 2, IMPZ, Jumeirah Golf Estates, DIP 1 and 2, Jumeirah Village Circle, Al Barsha South, Dubai Hills, The Springs, Meadows, The Lakes, Jumeirah Island, Jumeirah Park, Jumeirah Heights, JVT, Greens/Views, Tecom/Barsha Heights, Al Barsha 1, Al Barsha 2, Al Barsha 3, Umm Suqeim 1, Umm Suqeim 2, Umm Suqeim 3, Al Wasl, Jumeira 1, Jumeira 2, Jumeira 3 and Other areas.";
            
            $faq[4]['question'] = "What are the payment methods you offer?";
            $faq[4]['answer']   = "You can simply pay online with your debit or credit card. You can pay cash in hand to our crew directly.";

            $faq[5]['question'] = "Do I have the option to specify for which day I need a maid appointment?";
            $faq[5]['answer']   = "Yes, you can specify your day of maid visit.";
            
            $faq[6]['question'] = "What if my required maid service is not listed in your website?";
            $faq[6]['answer']   = "Please contact us and let us know about the service, we will definitely work towards adding your requirements.";
            
            $faq[7]['question'] = "Is my Maid employed by Elite Maids?";
            $faq[7]['answer']   = "Yes, all of our maids employed by Elite Maids.";
            
            $faq[8]['question'] = "What is the minimum number of hours for a Maid Visit?";
            $faq[8]['answer']   = "Depending on the areas, the minimum number of working hours is between 2 to 4 hours.";
            
            $faq[9]['question'] = "Do I have to sign an agreement?";
            $faq[9]['answer']   = "For regular customers who want to use Elite Maids, they have to sign up for our service agreement.";
            
            echo json_encode(array('status' => 'success', 'faq' => $faq));
            exit();
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';

            echo json_encode($response);
            exit();
        }
    }
	
	public function forgot_password()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            if ($this->input->get('email') && strlen(trim($this->input->get('email'))) > 0) 
            {
                $email = trim($this->input->get('email'));

                if ($email) 
                {
                    $customer = $this->customerappapi_model->get_customers_by_field_value('email_address', $email);

                    if (!empty($customer)) 
                    {
                        $customer_id = $customer->customer_id;
                        $customer_details = $this->customerappapi_model->get_customer_by_id($customer_id);
                        //hided need to do
						$this->sendforgotmail($customer_details);

                        echo json_encode(array('status' => 'success' ,'response_code'=>'200','message' =>'We have send an email to  '.$email.' account . Please check the account for password. '));
                        exit();
                    } else {

                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '104';
                        $response['message'] = 'Invalid email address!';

                        echo json_encode($response);
                        exit();
                    }
                } else {

                    $response = array();
                    $response['status'] = 'error';
                    $response['error_code'] = '103';
                    $response['message'] = 'Parameter missing!';

                    echo json_encode($response);
                    exit();
                }
            } else {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '101';
                $response['message'] = 'Invalid request';

                echo json_encode($response);
                exit();
            }
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';
            echo json_encode($response);
            exit();
        }
    }
	
	public function change_password()
    { 
        if($this->config->item('ws') === $this->input->get('ws'))
        { 
            $handle     = fopen('php://input','r');
            $jsonInput  = fgets($handle); 
            //$jsonInput  = '{"user_id":"1","old_password":"123","new_password":"12345"}'; // normal login from mobile
            $json_arr   = json_decode($jsonInput,true);
            
            if(!empty($json_arr))
            { 
                if($json_arr['user_id'] && is_numeric($json_arr['user_id']) && $json_arr['user_id']> 0 && $json_arr['old_password'] && strlen($json_arr['old_password']) > 0 && $json_arr['new_password'] && strlen($json_arr['new_password']) > 0 )
                { 
                    $customer_id    = trim($json_arr['user_id']);
                    $customer       = $this->customerappapi_model->get_customer_by_id($customer_id);
                    if(isset($customer) && isset($customer->customer_id) && $customer->customer_id > 0)
                    { 
                        $old_password = trim($json_arr['old_password']);
                        $new_password = trim($json_arr['new_password']);
                        
                        if($old_password == $customer->customer_password)
                        { 
                            $update_fields              = array();
                            $update_fields['customer_password']  = $new_password;
                            $affected = $this->customerappapi_model->update_customers($update_fields, $customer_id);			

                            if($affected > 0)
                            {
                                echo json_encode(array('status' => 'success', 'message' => 'Password Changed Successfully'));
                                exit();
                            } 
                            else 
                            {
                                $response           = array();
                                $response['status'] = 'error';
                                $response['message'] = 'Unexpected error!';
                                $response['error_code'] = '105';

                                echo json_encode($response);
                                exit();
                            }
                        }
                        else
                        {
                            $response           = array();
                            $response['status'] = 'error';
                            $response['message']= 'Invalid old password!';
                            $response['error_code'] = '104';

                            echo json_encode($response);
                            exit();
                        }
                    }
                    else
                    {

                        $response = array();
                        $response['status'] = 'error';
                        $response['message'] = 'Invalid user!';
                        $response['error_code'] = '103';

                        echo json_encode($response);
                        exit();
                    }
                     
                }
                 else 
                {
                    $response           = array();
                    $response['status'] = 'error';
                    $response['message']= 'Parameter missing!';
                    $response['error_code'] = '103';

                    echo json_encode($response);
                    exit();	 
                }
            }
            else
            {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();	
            }
        }
        else
        {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';

            echo json_encode($response);
            exit();	
        }
    }
	
	public function crew_in()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            $response[0] = "At home";
            $response[1]   = "Key is with security";
            $response[2]   = "Key under the mat";
            $response[3]   = "Buzz the intercom";
            $response[4]   = "Door open";
            echo json_encode(array('status' => 'success', 'crew_detail' => $response));
            exit();
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';
            echo json_encode($response);
            exit();
        } 
    }
	
	public function service_time()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            if ($this->input->get('date') && strlen(trim($this->input->get('date'))) > 0) 
            {
                $servicedate = trim($this->input->get('date'));
                $s_hour = $this->input->get('hours');
                if ($servicedate) 
                {
                    $available_times = $this->customerappapi_model->get_available_times_new($servicedate);
                    
                    $times = array();
                    $current_hour_index = 0;
                    $time = '07:00 am';  
                    $time_stamp = strtotime($time);
                    $timingarray = array();
                    for ($i = 0; $i < 11; $i++)
                    {
                        //$selected = ($i == 0) ? 'class="selected"' : '';
                        $oneDimensionalArray = array_map('current', $available_times);

                        $time_stamp = strtotime('+60mins', strtotime($time));
                        $timess = date('H:i:s', $time_stamp);
                        $time = date('g:i a', $time_stamp);
                        
                        if( in_array( $timess ,$oneDimensionalArray ) )
                        {

                        } else {
                            if($servicedate == date('Y-m-d'))
                            {
                                $t_shrt = date('H:i:s', strtotime($time)); 
                                $cur_shrt = date('H:i:s');
                                $hours = ((strtotime($t_shrt) - strtotime($cur_shrt))/3600);
                                if($hours >= 2)
                                {
                                    $to_time = date('H', strtotime($timess.'+'.$s_hour.' hour'));
                                    if (((int) $to_time) <= 20 && ((int) $to_time) > 8) {
                                        
                                        $timingarray[] = $time;
                                    }
                                }
                            } else {
                                $to_time = date('H', strtotime($timess.'+'.$s_hour.' hour'));

                                if (((int) $to_time) <= 20 && ((int) $to_time) > 8) {
                                    $timingarray[] = $time;
                                }
                            }
                        }
                    }
                    
                    if (!empty($timingarray)) 
                    {
                        echo json_encode(array('status' => 'success' ,'booking_time_data' =>$timingarray));
                        exit();
                    } else {

                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '104';
                        $response['message'] = 'No shift available!';

                        echo json_encode($response);
                        exit();
                    }
                } else {

                    $response = array();
                    $response['status'] = 'error';
                    $response['error_code'] = '103';
                    $response['message'] = 'Parameter missing!';

                    echo json_encode($response);
                    exit();
                }
            } else {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '101';
                $response['message'] = 'Invalid request';

                echo json_encode($response);
                exit();
            }
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';

            echo json_encode($response);
            exit();
        }
    }
	
	public function price_calculation()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            $handle     = fopen('php://input','r');
            $jsonInput  = fgets($handle); 
            //$jsonInput  = '{"hours_no":"3","maids_no":"1","daycount":"1","cleaning_material":"N","extra_service":["1"],"service_type":"5","coupon_val":"","customer_id":""}'; // normal login from mobile
            //$jsonInput  = '{"hours_no":"3","maids_no":"1","daycount":"1","cleaning_material":"N","extra_service":["1"],"service_type":"5"}'; // normal login from mobile
            //$jsonInput  = '{"hours_no":"2","maids_no":"1","daycount":"1","cleaning_material":"N","service_type":"5"}'; // normal login from mobile
            $json_arr   = json_decode($jsonInput,true);
            
            if(!empty($json_arr))
            {
				if($json_arr['coupon_val'] == "")
				{
					if(!empty($json_arr['date_for_price']))
					{
						$today_date = $json_arr['date_for_price'];
					} else {
						$today_date = date('Y-m-d');
					}
					$weekday = date('w', strtotime($today_date));
					if($weekday == 1 || $weekday == 2)
					{
						//$json_arr['coupon_val'] = "Spec25";
					} else {
						//$json_arr['coupon_val'] = "Spec30";
					}
				}
                if($json_arr['hours_no'] && is_numeric($json_arr['hours_no']) && $json_arr['hours_no']> 0 && $json_arr['maids_no'] && strlen($json_arr['maids_no']) > 0 && $json_arr['daycount'] && strlen($json_arr['daycount']) > 0 )
                {
                    $no_hours = $json_arr['hours_no'];
                    $maids_no = $json_arr['maids_no'];
					$number_visits = $json_arr['visit_no'];
					$booking_type = $json_arr['booking_type'];
                    $cleaning_material = $json_arr['cleaning_material'];
                    $daycount = $json_arr['daycount'];
					$service_types = $json_arr['service_type'];
                    $interior_rate = $this->customerappapi_model->get_extraservice_rate(4);
                    $fridge_rate = $this->customerappapi_model->get_extraservice_rate(1);
                    $iron_rate = $this->customerappapi_model->get_extraservice_rate(2);
                    $oven_rate = $this->customerappapi_model->get_extraservice_rate(3);
                    //$today_date = $json_arr['date_for_price'];
					if(!empty($json_arr['date_for_price']))
					{
						$today_date = $json_arr['date_for_price'];
					} else {
						$today_date = date('Y-m-d');
					}
                    $vat_perc = 5;
                    
                    $fridge = 0;
                    $iron = 0;
                    $oven = 0;
                    $interior = 0;
                    if(!empty($json_arr['extra_service']))
                    {
                        $extra_service = $json_arr['extra_service'];
                        if (in_array(1, $extra_service)) {
                            $fridge = $fridge_rate->cost;
                        } else {
                            $fridge = 0;
                        }
                        if (in_array(2, $extra_service)) {
                            $iron = $iron_rate->cost;
                        } else {
                            $iron = 0;
                        }
                        if (in_array(3, $extra_service)) {
                            $oven = $oven_rate->cost;
                        } else {
                            $oven = 0;
                        }
                        if (in_array(4, $extra_service)) {
                            $interior = $interior_rate->cost;
                        } else {
                            $interior = 0;
                        }
                    }
                    $vat_percentage = 5;
                    
                    // $get_fee_details = $this->customerappapi_model->get_fee_details();
					$get_fee_details = $this->customerappapi_model->get_servicefee_details($service_types);
                    
					if($cleaning_material == "N")
					{
						$per_hour_rate = $get_fee_details->service_rate;
						$cleaning_material_rate = 0;
					} else {
						$per_hour_rate = $get_fee_details->service_rate;
						if($get_fee_details->material_incl == "Y")
						{
							$cleaning_material_rate = 0;
						} else {
							$cleaning_material_rate = 10;
						}
						$per_hour_rate = ($per_hour_rate + $cleaning_material_rate);
					}
                    
                    $service_rate_totals = ((((($no_hours * $per_hour_rate) * $maids_no) * $number_visits) + $interior + $fridge + $iron + $oven) * $daycount);
        
                    //$total_service_rate = ($service_rate - (($coupon_percentage/100) * $service_rate));

                    //$vat_charge = ($total_service_rate*($vat_percentage/100));
                    $vat_charge = ($service_rate_totals*($vat_percentage/100));

                    //$gross_amount = ($total_service_rate + $vat_charge);
                    $gross_total_amount = ($service_rate_totals + $vat_charge);
                    $discount = 0;
                    $coupons_id = 0;
                    $coupon = ucfirst($json_arr['coupon_val']);
                    
                    $price_array_inv = array();
                    $price_array_inv['vat_charge'] = $vat_charge;
                    $price_array_inv['service_rate'] = $service_rate_totals;
                    $price_array_inv['discount'] = $discount;
                    $price_array_inv['gross_amount'] = $gross_total_amount;
                    $price_array_inv['hour_rate'] = $per_hour_rate;
                    $price_array_inv['vat_percentage'] = $vat_percentage;
                    $price_array_inv['coupon_id'] = $coupons_id;
                    $price_array_inv['coupon_val'] = $coupon;
                    $price_array_inv['cleaning_material_price'] = 10;
                    
                    if($json_arr['coupon_val'] != "")
                    {
                        $coupon = ucfirst($json_arr['coupon_val']);
                        $get_coupon_id = $this->customerappapi_model->get_coupon_id($coupon);
                        $weekday = date('w', strtotime($today_date));
						
						$coupon_type=$get_coupon_id->coupon_type;
						$expiry_date=$get_coupon_id->expiry_date;
						$offer_type=$get_coupon_id->offer_type;
                        if(empty($get_coupon_id))
                        {
                            echo json_encode(array('status' => 'success','response_code'=>'200','promo_status'=>'not','message'=>'Coupon Code Invalid!','price'=>$price_array_inv));
                            exit();
                        }
                        else if($no_hours<$get_coupon_id->min_hrs)
                        {
                            
                            echo json_encode(array('status' => 'success','response_code'=>'200','promo_status'=>'not','message'=>'Coupon valid only for minimum '.$get_coupon_id->min_hrs.' hrs booking!','price'=>$price_array_inv));exit();

                        }else {
							$coupons_id = $get_coupon_id->coupon_id;
							
							if($expiry_date >= $today_date)
							{
								if($offer_type == "P")
								{
									if($coupon_type == "FT")
									{
										if(!empty($json_arr['customer_id']))
										{
											$customer_id = $json_arr['customer_id'];
											$checkbooking = $this->customerappapi_model->customer_new_booking($customer_id);
											if($checkbooking != 0)
											{
												echo json_encode(array('status' => 'success','response_code'=>'200','promo_status'=>'not','message'=>'Coupon Code Invalid!','price'=>$price_array_inv));
												exit();
											}
										}
									}
								
									$v_week_day = $get_coupon_id->valid_week_day;
									$weekArray = explode(',', $v_week_day);
								
									if( in_array( $weekday ,$weekArray ) )
									{
										if($cleaning_material == "N")
										{
											$per_hour_rate = $get_coupon_id->percentage;
										} else {
											$per_hour_rate = ($get_coupon_id->percentage + 10);
										}

										$service_rate_f = ((((($no_hours * $per_hour_rate) * $maids_no) * $number_visits) + $interior + $fridge + $iron + $oven) * $daycount);

										$discount = ($service_rate_totals - $service_rate_f);
										$getfee = ($service_rate_totals - $discount);

										$vat_charge = ($getfee*($vat_perc/100));
										$gross_total_amount = ($getfee + $vat_charge);

									} else {
										echo json_encode(array('status' => 'success','response_code'=>'200','promo_status'=>'not','message'=>'Coupon code not valid for this date!','price'=>$price_array_inv));
										exit();
									}
								} else {
									if($coupon_type == "FT")
									{
										if(!empty($json_arr['customer_id']))
										{
											$customer_id = $json_arr['customer_id'];
											$checkbooking = $this->customerappapi_model->customer_new_booking($customer_id);
											if($checkbooking != 0)
											{
												echo json_encode(array('status' => 'success','response_code'=>'200','promo_status'=>'not','message'=>'Coupon Code Invalid!','price'=>$price_array_inv));
												exit();
											}
										}
									}
									
									$v_week_day = $get_coupon_id->valid_week_day;
									$weekArray = explode(',', $v_week_day);
								
									if( in_array( $weekday ,$weekArray ) )
									{
										if($cleaning_material == "N")
										{
											$per_hour_rate = $get_fee_details->service_rate;
											$cleaning_material_rate = 0;
										} else {
											$per_hour_rate = $get_fee_details->service_rate;
											if($get_fee_details->material_incl == "Y")
											{
												$cleaning_material_rate = 0;
											} else {
												$cleaning_material_rate = 10;
											}
											$per_hour_rate = ($per_hour_rate + $cleaning_material_rate);
										}
										// if($cleaning_material == "N")
										// {
											// $per_hour_rate = 35;
										// } else {
											// $per_hour_rate = 45;
										// }

										$service_rate_f = ((((($no_hours * $per_hour_rate) * $maids_no) * $number_visit) + $interior + $fridge + $iron + $oven) * $daycount);

										//$discount = ($service_rate_totals - $service_rate_f);
										$discount = $get_coupon_id->percentage;
										$getfee = ($service_rate_totals - $discount);

										$vat_charge = ($getfee*($vat_perc/100));
										$gross_total_amount = ($getfee + $vat_charge);
										
									} else {
										echo json_encode(array('status' => 'success','response_code'=>'200','promo_status'=>'not','message'=>'Coupon code not valid for this date!','price'=>$price_array_inv));
										exit();
									}
								}
							} else {
								echo json_encode(array('status' => 'success','response_code'=>'200','promo_status'=>'not','message'=>'Coupon expired!','price'=>$price_array_inv));
								exit();
							}
						}
                        $msg = "Coupon added successfully";
                        $promo_stat = "applied";
                    } else {
                        $coupon = "";
                        $msg = "Invalid Coupon";
                        $promo_stat = "not";
                    }
                    
                    $price_array = array();
                    $price_array['vat_charge'] = $vat_charge;
                    $price_array['service_rate'] = $service_rate_totals;
                    $price_array['discount'] = $discount;
                    $price_array['gross_amount'] = $gross_total_amount;
                    $price_array['hour_rate'] = $per_hour_rate;
                    $price_array['vat_percentage'] = $vat_percentage;
                    $price_array['coupon_id'] = $coupons_id;
                    $price_array['coupon_val'] = $coupon;
                    $price_array['cleaning_material_price'] = 10;
                    
                    echo json_encode(array('status' => 'success','response_code'=>'200','promo_status'=>$promo_stat,'message'=>$msg,'price'=>$price_array));
                    exit();
                } else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '103';
                    $response['message']    = 'Parameter Missing';
                    echo json_encode($response);
                    exit();
                }
            } else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
            }
            //hours_no,mids_no,service_type,cleaning_material,extra_service:[{extra_service_id}],book_type,date_time:[{date,time}]
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';

            echo json_encode($response);
            exit();
        }
    }
	
	public function add_booking()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            $handle     = fopen('php://input','r');
            $jsonInput  = fgets($handle); 
           // $jsonInput  = '{
				// "booking_ype":"OD",
				// "cleaning_material":"N",
				// "coupon_val":"",
				// "coupon_id":"0",
				// "crew_in":"At home",
				// "discount":"0",
				// "extra_service_id":[
				// ],
				// "hour_count":"4",
				// "instructions":"no_notes",
				// "maid_count":"1",
				// "payment_type":"cash",
				// "price_per_hour":"35",
				// "schedule_list":[
				// {
				// "date":"2020-03-27",
				// "time":"8:00 am"
				// }
				// ],
				// "service_charge":"140",
				// "service_type":"1",
				// "total_amount":"147",
				// "customer_id":"1",
				// "vat_charge":"7",
				// "visit_no":"2"
			// }';
            $json_arr   = json_decode($jsonInput,true);
			
            if(!empty($json_arr))
            { 
                if($json_arr['customer_id'] && is_numeric($json_arr['customer_id']) && $json_arr['customer_id']> 0)
                {
                    $customerid = $json_arr['customer_id'];
					$number_visits = $json_arr['visit_no'];
                    $getaddressbycustomer = $this->customerappapi_model->get_address_details($customerid);
					$address_id = $getaddressbycustomer->customer_address_id;
                    $service_id = $json_arr['service_type'];
                    $instructions = $json_arr['instructions'];
                    $no_of_maids = $json_arr['maid_count'];
                    $hour_count = $json_arr['hour_count'];
                    $cleaning_material = $json_arr['cleaning_material'];
                    $service_cost = $json_arr['service_charge'];
                    $coupon_id = $json_arr['coupon_id'];
                    $discount = $json_arr['discount'];
                    $vat_cost = $json_arr['vat_charge'];
                    $booked_date = date('Y-m-d H:i:s');
                    $crew_in = $json_arr['crew_in'];
					$paytype = $json_arr['payment_type'];
                    $reference = "EM".mt_rand(1000, 9999);
                    $fridge = 0;
                    $iron = 0;
                    $oven = 0;
                    $interior = 0;
                    $userInfo = $this->customerappapi_model->get_customer_by_id($customerid);
                    $phone= $userInfo->mobile_number_1;
					$get_service_detail = $this->customerappapi_model->get_servicefee_details($service_id);
                            
                    if(!empty($json_arr['extra_service_id']))
                    {
                        $extra_service = $json_arr['extra_service_id'];
                        if (in_array(1, $extra_service)) {
                            $fridge = 1;
                        } else {
                            $fridge = 0;
                        }
                        if (in_array(2, $extra_service)) {
                            $iron = 1;
                        } else {
                            $iron = 0;
                        }
                        if (in_array(3, $extra_service)) {
                            $oven = 1;
                        } else {
                            $oven = 0;
                        }
                        if (in_array(4, $extra_service)) {
                            $interior = 1;
                        } else {
                            $interior = 0;
                        }
                    }
                    $schedulelist = $json_arr['schedule_list'];
                    $schedule_count = count($schedulelist);
//                    $serv_amt = ($service_cost / ($no_of_maids * $schedule_count));
                    $serv_amt = ($service_cost / $schedule_count);
//                    $vat_amt = ($vat_cost / ($no_of_maids * $schedule_count));
                    $vat_amt = ($vat_cost / $schedule_count);
                    $total_amt = ($serv_amt + $vat_amt);
					
					if($coupon_id > 0)
					{
						//$discount_amt = ($discount / ($no_of_maids * $schedule_count));
						$discount_amt = ($discount / ($schedule_count));
						$total_amt = ($total_amt - $discount_amt);
					}
					
                    foreach($schedulelist as $schedule)
                    {
                        $service_start_date = $schedule['date'];
                        $service_week_day   = date('w', strtotime($service_start_date));
						
						if($number_visits == 1)
						{
							$serviceenddate = $service_start_date;
							$buktype = 'OD';
						} else {
							if($json_arr['booking_ype'] == 'OD')
							{
								$buktype = 'WE';
							} else {
								$buktype = $json_arr['booking_ype'];
							}
							$tot_visits = ($number_visits - 1);
							$serviceenddate = date("Y-m-d", strtotime($service_start_date. "+$tot_visits week"));
							
						}
                        
                        $time_from          = date("H:i:s", strtotime($schedule['time']));
                        //$no_hrs     = $schedule['hour_count'];
                        $to_time    = date('H:i:s', strtotime($time_from.'+'.$hour_count.' hour'));
                        $book_type  = $buktype;
                        $service_end= '1';
                        $is_locked  = 0;
						
						
						if($get_service_detail->material_incl == 'N')
						{
							if($cleaning_material == 'Y')
							{
								$cleanfee = ((($no_of_maids * $number_visits) * $hour_count) * 10);
							} else {
								$cleanfee = 0;
							}
						} else {
							$cleanfee = 0;
						}
                        
                        //latestfor ($m = 1; $m <= $no_of_maids; $m++) 
                        //latest{
						$booking_fields                         = array();
						$booking_fields['reference_id'] = $reference;
						$booking_fields['customer_id']          = $customerid;
						$booking_fields['customer_address_id']  = $address_id;
						$booking_fields['service_type_id']      = $service_id;
						$booking_fields['service_start_date']   = $service_start_date;
						$booking_fields['service_week_day']     = $service_week_day;
						$booking_fields['time_from']            = $time_from;
						$booking_fields['time_to']              = $to_time;
						$booking_fields['booking_note']              = $instructions;
						$booking_fields['booking_type']         = $book_type;
						$booking_fields['booking_category']          = 'C';
						$booking_fields['service_end']          = $service_end;
						$booking_fields['service_end_date']     = $serviceenddate;
						$booking_fields['service_actual_end_date']   = $serviceenddate;
						$booking_fields['price_per_hr']    = $json_arr['price_per_hour'];
						$booking_fields['discount ']         = $discount;
						$booking_fields['service_charge']    = (($serv_amt/$no_of_maids)/$number_visits);
						$booking_fields['vat_charge']    = (($vat_amt/$no_of_maids)/$number_visits);
						if($paytype == 'cash')
						{
							$booking_fields['total_amount']    = ((($total_amt/$no_of_maids)/$number_visits)+5);
						} else {
							$booking_fields['total_amount']    = (($total_amt/$no_of_maids)/$number_visits);
						}
						if($paytype == 'cash')
						{
							$booking_fields['pay_by_cash_charge']    = 5;
						}
						$booking_fields['is_locked']            = $is_locked;
						$booking_fields['no_of_maids']          = $no_of_maids;
						$booking_fields['no_of_hrs']    = $hour_count;
						$booking_fields['number_of_visits']    = $number_visits;
						$booking_fields['cleaning_material']    = $cleaning_material;
						$booking_fields['cleaning_material_fee']    = (($cleanfee/$no_of_maids)/$number_visits);;
						$booking_fields['booked_from']          = 'M';
						$booking_fields['booking_from']          = 'EM';
						$booking_fields['booking_status']       = 0;
						$booking_fields['booked_datetime']      = date('Y-m-d H:i:s');
						$booking_fields['payment_type']         = 0;
						$booking_fields['coupon_id ']         = $coupon_id;
						$booking_fields['interior_window_clean']         = $interior;
						$booking_fields['fridge_cleaning']         = $fridge;
						$booking_fields['ironing_services']         = $iron;
						$booking_fields['oven_cleaning ']         = $oven;
						$booking_fields['crew_in ']         = $crew_in;
						$booking_fields['pay_by']         = $paytype;
						
						$booking_fields['net_cleaning_fee'] = $cleanfee;
						$booking_fields['net_service_charge'] = $serv_amt;
						$booking_fields['net_vat_charge'] = $vat_amt;
						if($paytype == 'cash')
						{
							$booking_fields['total_net_amount'] = ($total_amt+5);
						} else {
							$booking_fields['total_net_amount'] = $total_amt;
						}
						
                            // echo '<pre>';
							// print_r($booking_fields);
							
						$booking_id = $this->customerappapi_model->add_booking($booking_fields);
						if($booking_id)
						{
							$sms_date= $booking_fields['service_start_date']  ." from " . date('h:i a', strtotime($booking_fields['time_from'])) . ' to ' . date('h:i a', strtotime($booking_fields['time_to']));
							$sms_message="Your cleaning service booking dated ". $sms_date . " has been received. For further assistance, please call 800 258. Thank You!";
                
							//$this->_send_sms($phone,  $sms_message);
						}
                            
//                            if($booking_id > 0)
//                            {
//                                
//                            }
                            
                        //latest update}
                    }
                    
                    $coupon_fields = array();
                    $coupon_fields['customer_id'] = $customerid;
                    $coupon_fields['coupon_id'] = $coupon_id;
                    $coupon_fields['reference_id'] = $reference;
                    $coupon_fields['discount'] = $discount;
                    if($coupon_id > 0)
                    {
                        $update_coupon = $this->customerappapi_model->insert_coupon_for_customer($coupon_fields);
                    }
					
					if($paytype == 'cash')
					{
                       $justmop_ref=array();
                       $justmop_ref['booking_id']= $booking_id;
                       $justmop_ref['mop']= 'Cash';
                       $justmop_ref['ref']= $reference;
                       $justmop_ref['service_date']= $service_start_date;

                       $this->customerappapi_model->add_justmop_ref($justmop_ref);
//hided need to do
						$this->sent_booking_confirmation_mail($reference);
						$this->sent_booking_admin_confirmation_mail($reference);
						$this->_send_sms($phone,  $sms_message);
					}

                    
                    echo json_encode(array('status' => 'success','response_code'=>'200','booking_id'=>$booking_id,'reference_id'=>$reference,'message' => "Booking added successfully."));
                    exit();
                } else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '103';
                    $response['message']    = 'Parameter Missing';
                    echo json_encode($response);
                    exit();
                }
            } else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
            }
            
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';

            echo json_encode($response);
            exit();
        }
    }
	
	public function update_paytype_to_cash()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
			$handle     = fopen('php://input','r');
            $jsonInput  = fgets($handle);
			$json_arr   = json_decode($jsonInput,true);
			
            if(!empty($json_arr))
            {
				if($json_arr['reference_id'] && strlen($json_arr['reference_id'])>1)
                {
					
					$reference_id=$json_arr['reference_id'];
					
					$booking_fields = array();
					$get_update_paytype =  $this->customerappapi_model->update_payment_type($reference_id);
					
					echo json_encode(array('status' => 'success','response_code'=>'200','reference_id'=>$reference_id,'message' => "Booking updated successfully."));
                    exit();
				}
				else 
				{
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '103';
                    $response['message']    = 'Parameter Missing';
                    echo json_encode($response);
                    exit();
                }
				
			}
			else 
			{
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
            }
		}
	}
	public function contact_us()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            $handle     = fopen('php://input','r');
            $jsonInput  = fgets($handle);
            //$jsonInput  = '{"name":"vishnu","phone":"123456789","email":"vishnu.m@azinova.info","message":"Test message"}';
            $json_arr   = json_decode($jsonInput,true);
            if(!empty($json_arr))
            {
                if($json_arr['name'] && strlen($json_arr['name']) > 0)
                {
                    $name = $json_arr['name'];
                    $phone = $json_arr['phone'];
                    $email = $json_arr['email'];
                    $message = $json_arr['message'];
                    //hided need to do
                    $mail = $this->sent_main_contact_to_admin_mail($name, $phone, $message, $email);
                    
                    if($mail > 0)
                    {
                        echo json_encode(array('status' => 'success','response_code'=>'200','message' => "Enquiry submitted successfully."));
                        exit();
                    } else {
                        echo json_encode(array('status' => 'error','response_code'=>'200','message' => "Some thing went wrong try again..."));
                        exit();
                    }
                    
                } else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '103';
                    $response['message']    = 'Parameter Missing';
                    echo json_encode($response);
                    exit();
                }
            } else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
            }
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';

            echo json_encode($response);
            exit();
        } 
    }
	
	public function booking_history()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            $handle     = fopen('php://input','r');
            $jsonInput  = fgets($handle);
            //$jsonInput  = '{"user_id":"496"}';
            $json_arr   = json_decode($jsonInput,true);
            if(!empty($json_arr))
            {
                if($json_arr['user_id'] && strlen($json_arr['user_id']) > 0)
                {
                    $user_id = $json_arr['user_id'];
                    $userInfo = $this->customerappapi_model->get_customer_by_id($user_id);
                    if(empty($userInfo))
                    {
                        echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'Customer ID not found')); exit();   
                    }
                    $current_booking = $this->customerappapi_model->get_current_booking_mobile($user_id);
					$book_cur_array = array();
					if (!empty($current_booking)) {
						
						$i=0;
						foreach ($current_booking as $c_booking)
						{
							$reference_id = $c_booking->reference_id;
							$booking_id = $c_booking->booking_id;
							$online_pay = $this->customerappapi_model->check_online_pay_by_refid($reference_id);
							
							if($c_booking->booking_category == 'C')
							{
								$date = $c_booking->service_date;
								if($c_booking->cleaning_material == 'Y')
								{
									$clean_mat = "Yes";
								} else {
									$clean_mat = "No";
								}
								$no_of_maids = $c_booking->no_of_maids;
								//$total_rate = number_format($c_booking->total_amount, 2);
								$schedule = $c_booking->shift;
								if($c_booking->booking_status == 0)
								{
									$status = "Pending";
								} else if($c_booking->booking_status == 1){
									$status = "Confirmed";
								} else if($c_booking->booking_status == 2){
									$status = "Cancelled";
								}
								if($c_booking->payment_type != "")
								{
									$patype = $c_booking->payment_type;
								} else {
									$patype = 0;
								}
								//$status = $c_booking->booking_status;
								$type = 0;
								//$book_cur_array[$i]['date'] = $date;
								$book_cur_array[$i]['bookingid'] = $booking_id;
								$book_cur_array[$i]['reference_id'] = $reference_id;
								$book_cur_array[$i]['cleaning_mat'] = $clean_mat;
								$book_cur_array[$i]['maids_count'] = $no_of_maids;
								$book_cur_array[$i]['payment_type'] = $patype;
								$book_cur_array[$i]['booked_from'] = $c_booking->booked_from;
								//$book_cur_array[$i]['rate'] = $total_rate;
								//$book_cur_array[$i]['shift'] = $schedule;
								$book_cur_array[$i]['status'] = $status;
								$book_cur_array[$i]['type'] = $type;
								$book_cur_array[$i]['service_type'] = $c_booking->service_type_name;
								$book_cur_array[$i]['pay_status'] = 0;
								$book_cur_array[$i]['is_cancel'] = $c_booking->is_cancelled;
								$total_rate = 0;
								if(strtolower($online_pay->payment_status)=='success')
								{
									$book_cur_array[$i]['pay_status'] = '1';
								}
								if ($reference_id != "")
								{
									$shifts_data = $this->customerappapi_model->get_shift_booking_mobile($reference_id);
									$shift = array();
									$k = 0;
									foreach ($shifts_data as $shifts) 
									{
										if($shifts->booking_status == 0)
										{
											$statuss = "Pending";
										} else if($shifts->booking_status == 1){
											$statuss = "Confirmed";
										} else if($shifts->booking_status == 2){
											$statuss = "Cancelled";
										}
										$shift_date = $shifts->service_date;
										$schedule = $shifts->shift;
										$shift[$k]['date'] = $shift_date;
										$shift[$k]['shift'] = $schedule;
										$shift[$k]['status'] = $statuss;
										//$total_rate += $shifts->total_amount;
										if($shifts->total_net_amount != 0)
										{
											$total_rate += $shifts->total_net_amount;
										} else {
											$total_rate += $shifts->total_amount;
										}
										$k++;
									}
								}
                                                                else
                                                                {
                                                                        $shifts_data = $this->customerappapi_model->get_shift_booking_mobile_by_bookingid($booking_id);
									$shift = array();
									$k = 0;
									foreach ($shifts_data as $shifts) 
									{
										if($shifts->booking_status == 0)
										{
											$statuss = "Pending";
										} else if($shifts->booking_status == 1){
											$statuss = "Confirmed";
										} else if($shifts->booking_status == 2){
											$statuss = "Cancelled";
										}
										$shift_date = $shifts->service_date;
										$schedule = $shifts->shift;
										$shift[$k]['date'] = $shift_date;
										$shift[$k]['shift'] = $schedule;
										$shift[$k]['status'] = $statuss;
										// $total_rate += $shifts->total_amount;
										if($shifts->total_net_amount != 0)
										{
											$total_rate += $shifts->total_net_amount;
										} else {
											$total_rate += $shifts->total_amount;
										}
										$k++;
									}
                                                                }
								$book_cur_array[$i]['rate'] = number_format($total_rate, 2);
								$book_cur_array[$i]['vat'] = 5;
								//$book_cur_array[$i]['shift'] = $shift;
								if(!empty($shift))
								{
									$book_cur_array[$i]['shift'] = $shift;
								} else {
									$book_cur_array[$i]['shift'] = [];
								}
							} else if($c_booking->booking_category == 'M'){
								$date = $c_booking->service_date;
								$priority = $c_booking->priority_type;
								//$status = $c_booking->booking_status;
								if($c_booking->booking_status == 0)
								{
									$status = "Pending";
								} else if($c_booking->booking_status == 1){
									$status = "Confirmed";
								} else if($c_booking->booking_status == 2){
									$status = "Cancelled";
								}
								$time_type = $c_booking->time_type;
								if($time_type == 'flexible')
								{
									$hours = "";
								} else {
									$hours = $c_booking->time_val;
								}
								$type= 1;
								$rate = "";
                                                                $book_cur_array[$i]['bookingid'] = $booking_id;
								$book_cur_array[$i]['reference_id'] = $reference_id;
								$book_cur_array[$i]['date'] = $date;
								$book_cur_array[$i]['hours'] = $hours;
								$book_cur_array[$i]['priority'] = $priority;
								$book_cur_array[$i]['rate'] = $rate;
								$book_cur_array[$i]['vat'] = 5;
								$book_cur_array[$i]['status'] = $status;
								$book_cur_array[$i]['service_type'] = $c_booking->service_type_name;
								$book_cur_array[$i]['type'] = $type;
								$book_cur_array[$i]['is_cancel'] = $c_booking->is_cancelled;
							}
							$i++;
						}
					}
                    $previous_booking = $this->customerappapi_model->get_previous_booking_mobile($user_id,array('limit'=>'20'));
					// echo '<pre>';
					// print_r($previous_booking);
					// exit();
					
					$book_prev_array = array();
					if (!empty($previous_booking)) {
						
						$l=0;
						foreach ($previous_booking as $p_booking)
						{
							$reference_id = $p_booking->reference_id;
                                                        $booking_id = $p_booking->booking_id;
							$online_pay = $this->customerappapi_model->check_online_pay_by_bookid($reference_id);
							if($p_booking->booking_category == 'C')
							{
								$date = $p_booking->service_date;
								if($p_booking->cleaning_material == 'Y')
								{
									$clean_mat = "Yes";
								} else {
									$clean_mat = "No";
								}
								$no_of_maids = $p_booking->no_of_maids;
								//$total_rate = number_format($p_booking->total_amount, 2);
								$schedule = $p_booking->shift;
								//$status = $p_booking->booking_status;
								if($p_booking->service_status == 2)
								{
									$status = 'Finished';
								}
								if($p_booking->payment_type != "")
								{
									$patype = $p_booking->payment_type;
								} else {
									$patype = 0;
								}
								$type = 0;
								//$book_prev_array[$i]['date'] = $date;
								$book_prev_array[$l]['reference_id'] = $reference_id;
								$book_prev_array[$l]['cleaning_mat'] = $clean_mat;
								$book_prev_array[$l]['maids_count'] = $no_of_maids;
								//$book_prev_array[$l]['rate'] = $total_rate;
								//$book_prev_array[$j]['shift'] = $schedule;
								$book_prev_array[$l]['status'] = $status;
								$book_prev_array[$l]['type'] = $type;
								$book_prev_array[$l]['service_type'] = $p_booking->service_type_name;
								$book_prev_array[$l]['payment_type'] = $patype;
								$book_prev_array[$l]['booked_from'] = $p_booking->booked_from;
								$book_prev_array[$l]['pay_status'] = 0;
								$book_prev_array[$l]['is_cancel'] = $p_booking->is_cancelled;
								$total_rate = 0;
								if($online_pay->payment_status=='success')
								{
									$book_prev_array[$l]['pay_status'] = '1';
								}
								if($p_booking->payment_status==1)
								{
									$book_prev_array[$l]['pay_status'] = '1';
								}
								if ($reference_id != "")
								{
									$shifts_prevdata = $this->customerappapi_model->get_shift_prev_booking_mobile($reference_id);
									$shift_prev = array();
									$m = 0;
									foreach ($shifts_prevdata as $shifts) 
									{
										if($shifts->service_status == 2)
										{
											$statuss = 'Finished';
										}
										$shift_date = $shifts->service_date;
										$schedule = $shifts->shift;
										$shift_prev[$m]['date'] = $shift_date;
										$shift_prev[$m]['shift'] = $schedule;
										$shift_prev[$m]['status'] = $statuss;
										$total_rate += $shifts->total_amount;
										$m++;
									}
								}
                                                                else
                                                                {
                                                                        $shifts_prevdata = $this->customerappapi_model->get_shift_prev_booking_mobile_by_bookingid($booking_id);
									$shift_prev = array();
									$m = 0;
									foreach ($shifts_prevdata as $shifts) 
									{
										if($shifts->service_status == 2)
										{
											$statuss = 'Finished';
										}
										$shift_date = $shifts->service_date;
										$schedule = $shifts->shift;
										$shift_prev[$m]['date'] = $shift_date;
										$shift_prev[$m]['shift'] = $schedule;
										$shift_prev[$m]['status'] = $statuss;
										$total_rate += $shifts->total_amount;
										$m++;
									}
                                                                }
                                                                
								$book_prev_array[$l]['rate'] = number_format($total_rate, 2);
								$book_prev_array[$l]['vat'] = 5;
								//$book_prev_array[$l]['shift'] = $shift_prev;
								if(!empty($shift_prev))
								{
									$book_prev_array[$l]['shift'] = $shift_prev;
								} else {
									$book_prev_array[$l]['shift'] = [];
								}
							} else if($p_booking->booking_category == 'M'){
								$date = $p_booking->service_date;
								$priority = $p_booking->priority_type;
								$status = $p_booking->booking_status;
								$time_type = $p_booking->time_type;
								if($time_type == 'flexible')
								{
									$hours = "";
								} else {
									$hours = $p_booking->time_val;
								}
								$type= 1;
								$rate = "";
								$book_prev_array[$l]['reference_id'] = $reference_id;
								$book_prev_array[$l]['date'] = $date;
								$book_prev_array[$l]['hours'] = $hours;
								$book_prev_array[$l]['priority'] = $priority;
								$book_prev_array[$l]['rate'] = $rate;
								$book_prev_array[$l]['vat'] = 5;
								$book_prev_array[$l]['status'] = $status;
								$book_prev_array[$l]['service_type'] = $p_booking->service_type_name;
								$book_prev_array[$l]['type'] = $type;
								$book_prev_array[$l]['is_cancel'] = $p_booking->is_cancelled;
							}
							$l++;
						}
					}
                    
                    echo json_encode(array('status' => 'success','response_code'=>'200','booking_data_current'=>$book_cur_array,'booking_data_past'=>$book_prev_array)); exit();
                    
                } else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '103';
                    $response['message']    = 'Parameter Missing';
                    echo json_encode($response);
                    exit();
                }
            } else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
            }
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';

            echo json_encode($response);
            exit();
        } 
    }
	
	public function customer_app_update()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            $getversiondetails_android = $this->customerappapi_model->getversiondetails('android');
            $getversiondetails_ios = $this->customerappapi_model->getversiondetails('ios');
            //echo json_encode($response);
            echo json_encode(array('status'=>'success','android'=>$getversiondetails_android,'ios'=>$getversiondetails_ios)); exit();  
            exit();
            
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';
            echo json_encode($response);
            exit();
        } 
    }
	
	public function get_service_area_detail()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            $get_service_area_detail = $this->customerappapi_model->get_service_area_detail();
            
            $response = array();
            
            $i = 0;
            foreach ($get_service_area_detail as $details) {
                $response[$i]['name'] = $details->area_name;
                $response[$i]['latitude'] = $details->a_latitude;
                $response[$i]['longitude'] = $details->a_longitude;
                $i++;
            }

            //echo json_encode($response);
            echo json_encode(array('status'=>'success','area_data'=>$response));
            exit();
            
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';
            echo json_encode($response);
            exit();
        } 
    }
	
	public function sms_send()
    {
		if ($this->config->item('ws') == $this->input->get('ws') ) 
        {
            $handle = fopen('php://input', 'r');
            $jsonInput = fgets($handle);
            //$jsonInput  = '{"phone":"0562461721","customer_id":"596"}'; // normal login from mobile
            $json_arr = json_decode($jsonInput, true);
            if (!empty($json_arr))
            {
                if ($json_arr['phone'] && strlen($json_arr['phone']) > 0 && $json_arr['customer_id'] && strlen($json_arr['customer_id']) > 0) 
                {  
                    
                    $mobile_number=trim($json_arr['phone']);
                    $customer_id=trim($json_arr['customer_id']);
                    $userInfo = $this->customerappapi_model->get_customer_by_id($customer_id);
             
                    if(empty($userInfo))
                            {
                                echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'Customer not found')); exit();   
                            } 
                            
                            
                    if($userInfo->mobile_status==1)
                      {
                       echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'Mobile Verification already done')); exit();     
                          
                      }
                      
                    if($userInfo->mobile_number_1==$mobile_number)
                                 {
                                     
                                  $mobile_verification_code=$userInfo->mobile_verification_code;
                                  //hided need to do
                                  $this->_send_sms($mobile_number, 'EliteMaids New Registration OTP - '.$mobile_verification_code);
								  $this->_send_otp_mail($userInfo->customer_name,$userInfo->email_address,$mobile_verification_code);
                                  echo json_encode(array('status' => 'success','response_code'=>'200','message'=>'OTP has been sent')); exit();
                                     
                                 }
                                 else
                                 {
                                 echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'Mobile number not found')); exit();     
                                 }
                    
                    
                }
                 else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '103';
                    $response['message']    = 'Parameter missing!';
                    echo json_encode($response);
                    exit(); 
                     
                 }
            }
           else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
                }
        } 
        else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';

            echo json_encode($response);
            exit();
        }
        
        
        
        
    }
    
    public function verify_otp()
    {
         if ($this->config->item('ws') == $this->input->get('ws') ) 
        {
            $handle = fopen('php://input', 'r');
            $jsonInput = fgets($handle);
            //$jsonInput  = '{"otp":"63048","customer_id":"596"}'; // normal login from mobile
            $json_arr = json_decode($jsonInput, true);
            if (!empty($json_arr))
            {
                if ($json_arr['otp'] && strlen($json_arr['otp']) > 0 && $json_arr['customer_id'] && strlen($json_arr['customer_id']) > 0) 
                {  
                    
                  $otp=  trim($json_arr['otp']);
                  $customer_id=trim($json_arr['customer_id']);
                  $userInfo = $this->customerappapi_model->get_customer_by_id($customer_id);
                  if(empty($userInfo))
                            {
                                echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'Customer not found')); exit();   
                            }
                 if($userInfo->mobile_status==1)
                      {
                       echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'Mobile Verification already done')); exit();     
                          
                      }
                if($userInfo->mobile_verification_code==$otp)
                         {
                          $otp_status = array();
                          $otp_status['mobile_status']=1;
                          $this->customerappapi_model->update_customers($otp_status,$customer_id);
                          echo json_encode(array('status' => 'success','response_code'=>'200','message'=>'OTP has been verified')); exit();
                         }
                         else
                         {
                          echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'Wrong OTP')); exit();      
                         }
                    
                }
                else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '103';
                    $response['message']    = 'Parameter missing!';
                    echo json_encode($response);
                    exit(); 
                     
                 }
            }
             else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
                }
                
       }
       else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';

            echo json_encode($response);
            exit();
            }
    }
	
	public function get_sdk_token()
	{
           if($this->config->item('ws') === $this->input->get('ws')){

                $handle     = fopen('php://input','r');
                $jsonInput  = fgets($handle); 
               // $jsonInput='{"service_command":"SDK_TOKEN","merchant_identifier":"CKxScKab","access_code":"XIj8KVLKSPumGuyfnu6g","signature":"fb253c79df66d4a309f85189eabfbcf341925e2a485ee4d586fed219dafd7937","device_id":"ffffffff-eefe-0670-d57b-77750033c587","language":"en"}';
                if(!empty($jsonInput)){
                 $url = 'https://paymentservices.payfort.com/FortAPI/paymentApi';
                 $ch = curl_init( $url );
                 curl_setopt( $ch, CURLOPT_POSTFIELDS, $jsonInput );
                 curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Content-Length: ' . strlen($jsonInput)));
                 curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
                 $result = curl_exec($ch);
                 curl_close($ch);
                 
                 if($result){
                     
                  echo $result;
                  exit;
                     
                 }
                 else
                 {
                  echo json_encode(array('status' => 'error', 'message' => 'Something went wrong!'));
                  exit();   
                 }
  
                    
                }
                else
                {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '101';
                    $response['message']    = 'Invalid request';
                    echo json_encode($response);
                    exit();	
                }
                
                
                
            }

            else
            {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '104';
                $response['message']    = 'Invalid url';

                echo json_encode($response);
                exit();	
            }
            
            
	}
	
	public function payment_update()
    {
        if ($this->config->item('ws') == $this->input->get('ws') ) 
        { 
            $handle     = fopen('php://input', 'r');
            $jsonInput  = fgets($handle); 
            //$jsonInput  = '{"customer_id":"79","transaction_id":"256484","booking_id":"SM12345","amount":"100","payment_status":"success"}'; // normal login from mobile
            //$jsonInput  = '{"customer_id":"630","booking_refid":"SM6305747","payment_method":"card","transaction_id":"SM6305747","payment_status":"Failed","amount":"147"}';
			//$jsonInput  = '{"customer_id":"2127","booking_refid":"SM21279120","payment_method":"card","transaction_id":"152595194100009705","payment_status":"Success","amount":"147","is_from_retry":"n"}';
			$json_arr   = json_decode($jsonInput, true);
            
            if (!empty($json_arr))
            {
				if ( $json_arr['customer_id'] && strlen($json_arr['customer_id']) > 0 ) 
				{
					$payment_fields = array();
					$trans_id=$json_arr['transaction_id'];
					$trans_check=strtolower(substr($trans_id, 0, 3));
					
					$payment_fields['transaction_id'] = $json_arr['transaction_id'];
					if($trans_check=='sid'){$payment_fields['transaction_id'] = $this->get_tran_id_from_sess_id($trans_id);}
					$payment_fields['reference_id'] = $json_arr['booking_refid'];
					$payment_fields['amount'] = $json_arr['amount'];
					$payment_fields['customer_id'] = $json_arr['customer_id'];
					$payment_fields['payment_status'] = $json_arr['payment_status'];
					$payment_fields['paid_from'] = 'M';
					$payment_fields['payment_datetime'] = date('Y-m-d H:i:s');
				
					$check_payment_exist = $this->customerappapi_model->check_payment($payment_fields);
				
					if(count($check_payment_exist) == 0)
					{
						$payment_id = $this->customerappapi_model->add_payment($payment_fields);
					} else {
						$get_pay_id =  $this->customerappapi_model->update_payment($payment_fields);
						$payment_id = $get_pay_id->payment_id;
					}
				
					if($json_arr['payment_status'] == 'success')
					{

                    $get_booking_by_refer_id = $this->customerappapi_model->get_bookingdetails_by_refid($json_arr['booking_refid']);

                    if(!empty($get_booking_by_refer_id)){


                      $justmop_ref=array();
                      foreach($get_booking_by_refer_id as $bookings)
                      {
                      $justmop_ref['booking_id']= $bookings->booking_id;
                      $justmop_ref['mop']= 'Credit Card';
                      $justmop_ref['ref']= $bookings->reference_id;
                      $justmop_ref['service_date']= $bookings->service_start_date;
                      $this->customerappapi_model->add_justmop_ref($justmop_ref);
                      }
                        
                    }
						$this->sent_make_payment_confirmation_mail($json_arr['booking_refid'],$payment_id, $json_arr['transaction_id'], $payment_fields['payment_datetime'], $json_arr['amount']);
						$this->sent_admin_payment_confirmation_mail($json_arr['booking_refid'],$payment_id, $json_arr['transaction_id'], $payment_fields['payment_datetime'], $json_arr['amount']);
						echo json_encode(array('status' => 'success','message' => "Payment status updated."));
						exit();
					} else {
						if($json_arr['is_from_retry'] == 'Y')
						{
							if($json_arr['payment_method'] == 'cash')
							{
								$get_update_paytype =  $this->customerappapi_model->update_payment_type($json_arr['booking_refid']);
								if(isset($get_update_paytype))
								{
									$this->sent_booking_confirmation_mail($json_arr['booking_refid']);
									$this->sent_booking_admin_confirmation_mail($json_arr['booking_refid']);  
								}
								
							}
						}
						echo json_encode(array('status' => 'error','message' => "Payment status updated."));
						exit();
					}
					//$this->sent_maint_booking_confirmation_mail($booking_id);
					//$this->sent_maint_booking_admin_confirmation_mail($booking_id);
				} else {
					$response               = array();
					$response['status']     = 'error';
					$response['error_code'] = '103';
					$response['message']    = 'Parameter missing!';
					echo json_encode($response);
					exit();
				}
            } else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
            }
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';
            echo json_encode($response);
            exit();
        }
    }
	
	public function booking_cancel()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            $handle     = fopen('php://input','r');
            $jsonInput  = fgets($handle);
            //$jsonInput  = '{"reference_id":"SC14055948","customer_id":1405}';
            $json_arr   = json_decode($jsonInput,true);
            if(!empty($json_arr))
            {
                if($json_arr['reference_id'] && strlen($json_arr['reference_id']) > 0)
                {
                    $reference_id = $json_arr['reference_id'];
					$customer_id =  $json_arr['customer_id'];
                    
					$userInfo = $this->customerappapi_model->get_customer_by_id($customer_id);
                    $mobile=$userInfo->mobile_number_1;
                    if(empty($userInfo))
                    {
                        echo json_encode(array('status' => 'error','response_code'=>'103','message'=>'Customer ID not found')); exit();   
                    }
					
					$booking_details = $this->customerappapi_model->get_bookingdetails_by_refid($reference_id);
					
					foreach($booking_details as $details)
					{
						$booking_id = $details->booking_id;
						$check_booking_started = $this->customerappapi_model->get_bookingdetails_by_bookid($booking_id);
						if(count($check_booking_started) > 0){
							echo json_encode(array('status' => 'error','response_code'=>'103','message'=>'Service already started. Please contact our customer services for further query.')); exit();
						}
					}
					
					$cancel_array = array();
					$cancel_array['is_cancelled'] = 'yes';
					$cancel_array['cancel_date'] = date('Y-m-d H:i:s');
					$cancel_array['booking_status'] = 2;
					
					$update_booking = $this->customerappapi_model->update_booking_status($cancel_array,$reference_id);
					
					if($update_booking)
					{
						//hided need to do
						$this->sent_booking_cancel_admin_confirmation_mail($reference_id);
						$this->sent_booking_cancel_confirmation_mail($reference_id);  
						$message="As per your request, your booking for reference ID:  " . $reference_id ." is cancelled.";
                        //$message="Booking for reference id " . $reference_id ." has been cancelled."; 

                        $this->_send_sms($mobile,$message);
                        
						echo json_encode(array('status' => 'success','response_code'=>'200','message'=>'Booking cancelled successfully.')); exit();
					} else {
						echo json_encode(array('status' => 'error','response_code'=>'103','message'=>'Some thing went wrong. Try again.')); exit();
					}
                } else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '103';
                    $response['message']    = 'Parameter Missing';
                    echo json_encode($response);
                    exit();
                }
            } else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
            }
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';

            echo json_encode($response);
            exit();
        } 
    }
	
	public function list_customer_address()
	{
        if ($this->config->item('ws') == $this->input->get('ws') )
		{
            if ($this->config->item('ws') == $this->input->get('ws') )
			{
                $handle     = fopen('php://input', 'r');
                $jsonInput  = fgets($handle); 
//                $jsonInput  = '{"customer_id":"3"}';
              
                $json_arr   = json_decode($jsonInput, true);
            
            if (!empty($json_arr)) {
                 if ( $json_arr['customer_id'] && strlen($json_arr['customer_id']) > 0 ){
                    $cust_id        = trim($json_arr['customer_id']);
                    $userInfo       = $this->customers_model->get_customer_by_id($cust_id);
                       if(empty($userInfo)){
                         echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'User ID not found')); exit();   
                       }
                       $cust_address_Info       = $this->customers_model->get_customer_addresses($userInfo->customer_id);
                       $customer_add = array();
                       $i = 0;
                     foreach($cust_address_Info as $address){
                         $customer_add[$i]['address']   = $address->customer_address;
                         $customer_add[$i]['area_name'] = $address->area_name;
                         $customer_add[$i]['zone_id']   = $address->zone_id;
                         $customer_add[$i]['latitude']  = $address->latitude;
                         $customer_add[$i]['longitude'] = $address->longitude;
                         $i++;
                     }
                       echo json_encode(array('status' => 'success','response_code'=>'200','addressList'=>$customer_add)); exit();  
                 }
                   else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '103';
                    $response['message']    = 'Customer ID missing!';
                    echo json_encode($response);
                    exit();
                }
            }
             else {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '101';
                $response['message']    = 'Invalid request';
                echo json_encode($response);
                exit();
            }
            }
            else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '104';
            $response['message']    = 'Invalid url';
            echo json_encode($response);
            exit();
        }
        }
    }
	
    public function add_customer_address(){ 
     
              if ($this->config->item('ws') == $this->input->get('ws') ){
                $handle     = fopen('php://input', 'r');
                $jsonInput  = fgets($handle); 
//                $jsonInput  = '{"customer_id":"38","area_id":"2","customer_address":"tsdd","latitude":"81.5555","longitude":"87.4555"}';
              
                $json_arr   = json_decode($jsonInput, true);
                
                if (!empty($json_arr)){
                     if ( $json_arr['customer_id'] && strlen($json_arr['customer_id']) > 0 &&  $json_arr['area_id'] && strlen($json_arr['area_id']) > 0 &&  $json_arr['customer_address'] && strlen($json_arr['customer_address']) > 0 &&  $json_arr['latitude'] && strlen($json_arr['latitude']) > 0 &&  $json_arr['longitude'] && strlen($json_arr['longitude']) > 0 ){
                          $cust_id        = trim($json_arr['customer_id']);
                        $userInfo       = $this->customers_model->get_customer_by_id($cust_id);
                       if(empty($userInfo)){
                         echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'Customer ID not found')); exit();   
                       }
                       
                       $custDetails                     = new stdClass();
                       $custDetails->customer_id        = $userInfo->customer_id;
                       $custDetails->area_id            = trim($json_arr['area_id']);
                       $custDetails->customer_address   = trim($json_arr['customer_address']);
                       $custDetails->latitude           = trim($json_arr['latitude']);
                       $custDetails->longitude          = trim($json_arr['longitude']);
                       $custAddId       = $this->customers_model->update_customer_address($custDetails);
                       if(isset($custAddId)){
                            echo json_encode(array('status' => 'success','response_code'=>'200','message'=>'Address added successfully')); exit();   
                       }else {
                            echo json_encode(array('status' => 'failed','response_code'=>'103','message'=>'Something went wrong')); exit();   
                       }
                     }   
                     else {
                        $response               = array();
                        $response['status']     = 'error';
                        $response['error_code'] = '103';
                        $response['message']    = 'Parametres missing!';
                        echo json_encode($response);
                        exit();
                    }
                }
                 else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '101';
                    $response['message']    = 'Invalid request';
                    echo json_encode($response);
                    exit();
                }
              }
               else {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '104';
                    $response['message']    = 'Invalid url';
                    echo json_encode($response);
                    exit();
                }
          
    }
	
	public function get_offer()
    {
       if ($this->config->item('ws') == $this->input->get('ws') )
        {
            $offer_results=$this->service_types_model->get_offers();
            $i=0;
            if(!empty($offer_results)){
                foreach($offer_results as $results)
                {
                    $offer[$i]['type'] = @$results->type;
                    $offer[$i]['title'] = @$results->title;
                    $offer[$i]['subtitle'] = @$results->subtitle;
                    $offer[$i]['imageurl'] = base_url().'assets/offer/'.@$results->imageurl;
                    $offer[$i]['featured'] = @$results->featured;
                    $i++;
                }
                echo json_encode(array('status' => 'success', 'offers' => $offer));
             }
             else
             {
               echo json_encode(array('status' => 'error', 'message' =>"No offers found"));   
             }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '104';
            $response['message'] = 'Invalid url';

            echo json_encode($response);
            exit();
        }
             
        
    }
    
   protected function saveImageFromUrl($photoUrl)
    {
        define('DIRECTORY', $this->config->item('root_directory'));
        $photo_type         = 'cus_sm_photo';
        $target_file_name   = $photo_type . '_' . time() . '.jpg' ;      
        $tmp_upload_folder  = 'customer_img/';
        $file_name      = DIRECTORY .$tmp_upload_folder.$target_file_name;
        $photo_content  = file_get_contents($photoUrl);
        file_put_contents($file_name, $photo_content);

        return $target_file_name;

    } 
    
        public function update_photo_from_url(){
// echo '<pre>';print_r($_SERVER);exit;
            if($this->config->item('ws') === $this->input->get('ws')){

                $handle     = fopen('php://input','r');
                $jsonInput  = fgets($handle); 
//                $jsonInput  = '{"customer_id":"1","photo_url":"https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/17796786_407427796309621_2138313569488245364_n.jpg?oh=85040eb23ae5163a989f43ed3ae85115&oe=5A684861"}'; // normal login from mobile
                $json_arr   = json_decode($jsonInput,true);
                if(!empty($json_arr)){
                    if($json_arr['customer_id'] && is_numeric($json_arr['customer_id']) && !empty($json_arr['photo_url']) ){
                        
                        $cust_id        = trim($json_arr['customer_id']);
                        $photoUrl       = trim($json_arr['photo_url']);
                        $cust_photo_url = $this->saveImageFromUrl($photoUrl);
                      
                        $userDetails                     = new stdClass();
                        $userDetails->customer_photo_file = $cust_photo_url;
                        $affected = $this->customers_model->update_customers($userDetails,$cust_id);
                        
                        if($affected > 0)
                        {
                            echo json_encode(array('status' => 'success', 'message' => 'Profile image updated Successfully'));
                            exit();
                        } 
                        else 
                        {
                            $response           = array();
                            $response['status'] = 'error';
                            $response['message']    = 'Unexpected error!';
                            $response['error_code'] = '105';
                            echo json_encode($response);
                            exit();
                        }
                    }
                    else 
                    {
                        $response           = array();
                        $response['status'] = 'error';
                        $response['message']= 'Parameter missing!';
                        $response['error_code'] = '103';

                        echo json_encode($response);
                        exit();	 
                    }
                }
                else
                {
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '101';
                    $response['message']    = 'Invalid request';
                    echo json_encode($response);
                    exit();	
                }

            }

            else
            {
                $response               = array();
                $response['status']     = 'error';
                $response['error_code'] = '104';
                $response['message']    = 'Invalid url';

                echo json_encode($response);
                exit();	
            }
        }
		
		public function getCustomerDetails()
    {
        if ($this->config->item('ws') == $this->input->get('ws') )
        {
            if ($this->input->get('mobile') && strlen(trim($this->input->get('mobile'))) > 0) 
            {
                $mobile = trim($this->input->get('mobile'));

                if ($mobile != "") 
                {
                    $custDet = $this->customerappapi_model->getCustomerMobSearch($mobile);

                    if(!empty($custDet))
                    {
                        echo json_encode(array("status"=>"success","result"=> $custDet)); exit();
                    } else {

                        echo json_encode(array("status"=>"not found","result"=> 'Mobile number not found')); exit();
                    }
                } else {

                    echo json_encode(array("status"=>"not found","result"=> 'Mobile number not found')); exit();
                }
            } else {
                echo json_encode(array("status"=>"not found","result"=> 'Invalid Url')); exit();
            }
        } else {
            echo json_encode(array("status"=>"not found","result"=> 'Invalid Url')); exit();
        }
    }
	
	public function getCustomerDetailsnew()
    {
        if ($this->input->get('ws') == $this->config->item('ws') )
        {
            if ($this->input->get('mobile') && strlen(trim($this->input->get('mobile'))) > 0) 
            {
                $mobile = trim($this->input->get('mobile'));

                if ($mobile != "") 
                {
                    $custDet = $this->customerappapi_model->getCustomerMobSearch($mobile);
                    if(!empty($custDet))
                    {
						$ds_fields = array();
						$ds_fields['customer_id'] = $custDet[0]->custId;
						$ds_fields['mobile_no'] = $mobile;
						$ds_fields['added_date_time'] = date('Y-m-d H:i:s');
						$this->customerappapi_model->add_call_detail($ds_fields);
                        $body_content="Name : ". $custDet[0]->custName ."\n";
                        $body_content.="Mobile : ". $custDet[0]->mobile1 ."\n";
                        $body_content.="Area : ". $custDet[0]->area ."\n";
                        $body_content.="Outstanding balance : ". $custDet[0]->balance.$custDet[0]->signed."\n";
                        $click_url=$custDet[0]->url;
                        
                        $post_values['notification']=array('title' => 'Spectrum',
                       'body' => $body_content,
                       'click_action' => $click_url,
                       'icon' => 'https://www.spectrumservices.ae/assets/images/spectrum-logo-old.png',
                         );
                        $post_values['to']='/topics/spectrum_notification';


                        $data = json_encode($post_values);
                       
                        $url = 'https://fcm.googleapis.com/fcm/send';
                        $server_key = 'AAAAlQPxczw:APA91bEE7rAJbgAwvy0RWlsLPozEr7KAxJJ6T0eiFelIrTW2_hWPqJmmsTEkuqqQuM1b8MItskTj7NbIWt7Uf7yga0gUU7EgAmzX_KXdGqMRjLjQEy5uhy96zFWne9bsotXh4uI3O0C-';

                        $headers = array(
                            'Content-Type:application/json',
                            'Authorization:key='.$server_key
                        );

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                        $result = curl_exec($ch);
                 
                        if ($result === FALSE) {
                            die('Oops! FCM Send Error: ' . curl_error($ch));
                        }
                        curl_close($ch);
                        echo json_encode(array("status"=>"success")); exit();
                    } else {
						$ds_fields = array();
						$ds_fields['customer_id'] = "";
						$ds_fields['mobile_no'] = $mobile;
						$ds_fields['added_date_time'] = date('Y-m-d H:i:s');
						$this->customerappapi_model->add_call_detail($ds_fields);
                        $body_content="Customer not found.\nMob - ".$mobile;
                        $click_url=base_url().'customer/add/'.$mobile;
                        
                        $post_values['notification']=array('title' => 'Spectrum',
                       'body' => $body_content,
                       'click_action' => $click_url,
                       'icon' => 'https://www.spectrumservices.ae/assets/images/spectrum-logo-old.png',
                         );
                        $post_values['to']='/topics/spectrum_notification';


                        $data = json_encode($post_values);
                       
                        $url = 'https://fcm.googleapis.com/fcm/send';
                        $server_key = 'AAAAlQPxczw:APA91bEE7rAJbgAwvy0RWlsLPozEr7KAxJJ6T0eiFelIrTW2_hWPqJmmsTEkuqqQuM1b8MItskTj7NbIWt7Uf7yga0gUU7EgAmzX_KXdGqMRjLjQEy5uhy96zFWne9bsotXh4uI3O0C-';

                        $headers = array(
                            'Content-Type:application/json',
                            'Authorization:key='.$server_key
                        );

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                        $result = curl_exec($ch);
                 
                        if ($result === FALSE) {
                            die('Oops! FCM Send Error: ' . curl_error($ch));
                        }
                        curl_close($ch);

                        echo json_encode(array("status"=>"not found","result"=> 'Mobile number not found')); exit();
                    }
                } else {

                    echo json_encode(array("status"=>"not found","result"=> 'Mobile number not found')); exit();
                }
            } else {
                echo json_encode(array("status"=>"not found","result"=> 'Invalid Url')); exit();
            }
        } else {
            echo json_encode(array("status"=>"not found","result"=> 'Invalid Url')); exit();
        }
    }
	
	
	
	
	
	
	
	
	
	private function _send_sms($mobile_number, $message)
    {
        $num = substr($mobile_number, -9);
        
        //$sms_url = 'https://api.smsglobal.com/http-api.php?action=sendsms&user=jhh1a51c&password=LXoAEzMF&&from=TestJC&to=971' . urlencode($num) . '&text=' . urlencode($message) . '';
        $sms_url = 'http://sms.azinova.in/WebServiceSMS.aspx?User=elitemaids&passwd=emaid@123&mobilenumber=971'.$num.'&message='.urlencode($message).'&sid=EliteMaids&mtype=N';
        $sms_url = str_replace(" ", '%20', $sms_url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $sms_url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $res = curl_exec($ch);
        curl_close ($ch);
    }
	
	function sent_registration_confirmation_mail($customer_id) 
	{
        $this->load->library('email');
        $data['customer'] = $this->customerappapi_model->get_customer_by_id($customer_id);
        $name = $data['customer']->customer_name;
        $email = $data['customer']->email_address;
        $mobile = $data['customer']->mobile_number_1;
        $data['name'] = $name;
        $data['customer_name'] = $data['customer']->email_address;
        $data['password'] = $data['customer']->customer_password;
        $html = $this->load->view('registration_template_email_mobile', $data, True);

        // $config = array(
		// 	'protocol'  => 'smtp',
		// 	'smtp_host' => 'ssl://smtp.googlemail.com',
		// 	'smtp_port' => 465,
		// 	'smtp_user' => 'info@elitemaids.emaid.info',
        //     'smtp_pass' => 'Advance1234',
		// 	'mailtype'  => 'html',
		// 	'charset'   => 'utf-8'
		// );
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.sendgrid.net',
            'smtp_user' => 'apikey',
            // 'smtp_pass' => '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',
            'smtp_pass' => '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',
            'smtp_port' => 587,
            'mailtype'  => 'html',
            'crlf' => "\r\n",
            'newline' => "\r\n"
          );

		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");
		$this->email->from('info@elitemaids.emaid.info', 'ELITEMAIDS');
        $this->email->to($data['customer']->email_address);
        $this->email->subject('EliteMaids Registration Confirmation');
        $this->email->message($html);
        //$this->email->send();
    }
	
	function sendforgotmail($recepient_dtls) 
    {
        $this->load->library('email');
		$data['f_name'] = $recepient_dtls->customer_name;
		$data['f_password'] = $recepient_dtls->customer_password;
        $html = $this->load->view('forgot_email_template_mobile', $data, True);
        
		// $config = array(
		// 	'protocol'  => 'smtp',
		// 	'smtp_host' => 'ssl://smtp.googlemail.com',
		// 	'smtp_port' => 465,
		// 	'smtp_user' => 'info@elitemaids.emaid.info',
        //     'smtp_pass' => 'Advance1234',
		// 	'mailtype'  => 'html',
		// 	'charset'   => 'utf-8'
		// );

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.sendgrid.net',
            'smtp_user' => 'apikey',
            // 'smtp_pass' => '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',
            'smtp_pass' => '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',
            'smtp_port' => 587,
            'mailtype'  => 'html',
            'crlf' => "\r\n",
            'newline' => "\r\n"
          );

		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");
		$this->email->from('info@elitemaids.emaid.info', 'ELITEMAIDS');
		$this->email->to($recepient_dtls->email_address);
		$this->email->subject('Password Recovery');
		$this->email->message($html);
        // $mail=//$this->email->send();
        //$this->email->send();

        // return $mail;
    }
	
	function sent_main_contact_to_admin_mail($customer_name, $customer_mobile, $message, $emailid)
    {   
        $this->load->library('email');
		$data['name'] = $customer_name;
        $data['mobile'] = $customer_mobile;
        $data['email'] = $emailid;
        $data['message'] = $message;
		
		$html = $this->load->view('contact_template_email_mobile', $data, True);
		
        // $config = array(
		// 	'protocol'  => 'smtp',
		// 	'smtp_host' => 'ssl://smtp.googlemail.com',
		// 	'smtp_port' => 465,
		// 	'smtp_user' => 'info@elitemaids.emaid.info',
		// 	'smtp_pass' => 'Advance1234',
		// 	'mailtype'  => 'html',
		// 	'charset'   => 'utf-8'
		// );
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.sendgrid.net',
            'smtp_user' => 'apikey',
            // 'smtp_pass' => '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',
            'smtp_pass' => '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',
            'smtp_port' => 587,
            'mailtype'  => 'html',
            'crlf' => "\r\n",
            'newline' => "\r\n"
          );
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");
		$this->email->from('info@elitemaids.emaid.info', 'ELITEMAIDS');
		$this->email->to('info@elitemaids.emaid.info');
		//$this->email->bcc('vishnu.m@azinova.info');
		$this->email->subject('New Enquiry Recieved');
		$this->email->message($html);
        $mail=//$this->email->send();//echo $this->email->print_debugger();
        return $mail;
    }
	
	public function sent_booking_confirmation_mail($reference_id)
    {
        $this->load->library('email');
        $data['bookingdetails'] = $this->customerappapi_model->get_bookingdetails_by_refid($reference_id);
        $coupon_fee = $this->customerappapi_model->get_coupon_fee_refid($reference_id);
        if(!empty($coupon_fee))
        {
            $data['coupon_fee'] = $coupon_fee->discount;
        } else {
            $data['coupon_fee'] = 0;
        } 
        
        $name = $data['bookingdetails'][0]->customer_name;
        $email = $data['bookingdetails'][0]->email_address;
        $mobile = $data['bookingdetails'][0]->mobile_number_1;
        $data['name'] = $name;
        $data['customer_name'] = $data['bookingdetails'][0]->email_address;
        $data['password'] = $data['bookingdetails'][0]->customer_password;
        $html = $this->load->view('booking_confirmation_template_email_mobile', $data, True);
		
		// $config = array(
		// 	'protocol'  => 'smtp',
		// 	'smtp_host' => 'ssl://smtp.googlemail.com',
		// 	'smtp_port' => 465,
		// 	'smtp_user' => 'info@elitemaids.emaid.info',
  //           'smtp_pass' => 'Advance1234',
		// 	'mailtype'  => 'html',
		// 	'charset'   => 'utf-8'
		// );

        $config = array(
              'protocol' => 'smtp',
              'smtp_host' => 'smtp.sendgrid.net',
              'smtp_user' => 'apikey',
              // 'smtp_pass' => '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',
            'smtp_pass' => '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',
              'smtp_port' => 587,
              'mailtype'  => 'html',
              'crlf' => "\r\n",
              'newline' => "\r\n"
            );

		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");
		//$this->email->from('info@elitemaids.emaid.info', 'ELITEMAIDS');
        $this->email->from('info@elitemaids.emaid.info', 'ELITEMAIDS');
        $this->email->to($email);
        //$this->email->bcc("vishnu.m@azinova.info");
        $this->email->subject('Cleaning Booking Confirmation - '.$reference_id);
        $this->email->message($html);
        //$this->email->send();
    }


    public function sent_booking_confirmation_mail_test()
    {
        $reference_id='EM3302';
        $this->load->library('email');
        $data['bookingdetails'] = $this->customerappapi_model->get_bookingdetails_by_refid($reference_id);
        $coupon_fee = $this->customerappapi_model->get_coupon_fee_refid($reference_id);
        if(!empty($coupon_fee))
        {
            $data['coupon_fee'] = $coupon_fee->discount;
        } else {
            $data['coupon_fee'] = 0;
        } 
        
        $name = $data['bookingdetails'][0]->customer_name;
        $email = $data['bookingdetails'][0]->email_address;
        $mobile = $data['bookingdetails'][0]->mobile_number_1;
        $data['name'] = $name;
        $data['customer_name'] = $data['bookingdetails'][0]->email_address;
        $data['password'] = $data['bookingdetails'][0]->customer_password;
        $html = $this->load->view('booking_confirmation_template_email_mobile', $data, True);
        
        // $config = array(
        //     'protocol'  => 'smtp',
        //     'smtp_host' => 'ssl://smtp.googlemail.com',
        //     'smtp_port' => 465,
        //     'smtp_user' => 'info@elitemaids.emaid.info',
        //     'smtp_pass' => 'Advance1234',
        //     'smtp_crypto' => 'ssl',
        //     'mailtype'  => 'html',
        //     'charset'   => 'utf-8'
        // );

        
        $config = array(
              'protocol' => 'smtp',
              'smtp_host' => 'smtp.sendgrid.net',
              'smtp_user' => 'apikey',
              // 'smtp_pass' => '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',
            'smtp_pass' => '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',
              'smtp_port' => 587,
              'mailtype'  => 'html',
              'crlf' => "\r\n",
              'newline' => "\r\n"
            );
        $email='jose.robert@azinova.info';
        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");
        $this->email->from('info@elitemaids.emaid.info', 'ELITEMAIDS');
        $this->email->to($email);
        //$this->email->cc("vishnu.m@azinova.info");
        $this->email->subject('Cleaning Booking Confirmation - '.$reference_id);
        $this->email->message($html);
        //$this->email->send();
        echo $this->email->print_debugger();
    }
	
	public function sent_make_payment_confirmation_mail($reference_id,$payment_id, $transaction_id, $payment_datetime, $amount)
    {
        $this->load->library('email');
        $data['bookingdetails'] = $this->customerappapi_model->get_bookingdetails_by_refid($reference_id);
        $coupon_fee = $this->customerappapi_model->get_coupon_fee_refid($reference_id);
        if(!empty($coupon_fee))
        {
            $data['coupon_fee'] = $coupon_fee->discount;
        } else {
            $data['coupon_fee'] = 0;
        }
        
        $name = $data['bookingdetails'][0]->customer_name;
        $email = $data['bookingdetails'][0]->email_address;
        $mobile = $data['bookingdetails'][0]->mobile_number_1;
        $data['name'] = $name;
        $data['customer_name'] = $data['bookingdetails'][0]->email_address;
        $data['password'] = $data['bookingdetails'][0]->customer_password;
		$data['amount'] = "AED ".number_format($amount, 2);
		$data['transaction_id'] = $transaction_id;
		$data['payment_datetime'] = $payment_datetime;
        $html = $this->load->view('payment_confirmation_template_email_mobile', $data, True);

  //       $config = array(
		// 	'protocol'  => 'smtp',
		// 	'smtp_host' => 'ssl://smtp.googlemail.com',
		// 	'smtp_port' => 465,
		// 	'smtp_user' => 'info@elitemaids.emaid.info',
  //           'smtp_pass' => 'Advance1234',
		// 	'mailtype'  => 'html',
		// 	'charset'   => 'utf-8'
		// );

        $config = array(
              'protocol' => 'smtp',
              'smtp_host' => 'smtp.sendgrid.net',
              'smtp_user' => 'apikey',
              // 'smtp_pass' => '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',
            'smtp_pass' => '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',
              'smtp_port' => 587,
              'mailtype'  => 'html',
              'crlf' => "\r\n",
              'newline' => "\r\n"
            );

		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");
		//$this->email->from('info@elitemaids.emaid.info', 'ELITEMAIDS');
        $this->email->from('info@elitemaids.emaid.info', 'ELITEMAIDS');
		$this->email->to($email);
        $this->email->subject('Booking Confirmation - '.$reference_id);
        $this->email->message($html);
        //$this->email->send();
    }
	
	public function sent_admin_payment_confirmation_mail($reference_id,$payment_id, $transaction_id, $payment_datetime, $amount)
    {
        $this->load->library('email');
        $data['bookingdetails'] = $this->customerappapi_model->get_bookingdetails_by_refid($reference_id);
        $coupon_fee = $this->customerappapi_model->get_coupon_fee_refid($reference_id);
        if(!empty($coupon_fee))
        {
            $data['coupon_fee'] = $coupon_fee->discount;
        } else {
            $data['coupon_fee'] = 0;
        }
        $name = $data['bookingdetails'][0]->customer_name;
        $email = $data['bookingdetails'][0]->email_address;
        $mobile = $data['bookingdetails'][0]->mobile_number_1;
        $data['name'] = $name;
        $data['customer_name'] = $data['bookingdetails'][0]->email_address;
        $data['password'] = $data['bookingdetails'][0]->customer_password;
		$data['amount'] = "AED ".number_format($amount, 2);
		$data['transaction_id'] = $transaction_id;
		$data['payment_datetime'] = $payment_datetime;
        $html = $this->load->view('payment_admin_template_email_mobile', $data, True);

  //       $config = array(
		// 	'protocol'  => 'smtp',
		// 	'smtp_host' => 'ssl://smtp.googlemail.com',
		// 	'smtp_port' => 465,
		// 	'smtp_user' => 'info@elitemaids.emaid.info',
  //           'smtp_pass' => 'Advance1234',
		// 	'mailtype'  => 'html',
		// 	'charset'   => 'utf-8'
		// );

        $config = array(
              'protocol' => 'smtp',
              'smtp_host' => 'smtp.sendgrid.net',
              'smtp_user' => 'apikey',
              // 'smtp_pass' => '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',
            'smtp_pass' => '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',
              'smtp_port' => 587,
              'mailtype'  => 'html',
              'crlf' => "\r\n",
              'newline' => "\r\n"
            );

		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");
		//$this->email->from('info@elitemaids.emaid.info', 'ELITEMAIDS');
        $this->email->from('info@elitemaids.emaid.info', 'ELITEMAIDS');
		$this->email->to("info@elitemaids.emaid.info");
		//$this->email->bcc("vishnu.m@azinova.info");
		$this->email->subject('Payment Confirmation - '.$reference_id);
        $this->email->message($html);
        //$this->email->send();
    }
    
    public function hhh()
    {
        $ref_id='HS6497742';
        $data['bookingdetails'] = $this->customerappapi_model->get_bookingdetails_by_refid($ref_id);
        $coupon_fee = $this->customerappapi_model->get_coupon_fee_refid($ref_id);
        if(!empty($coupon_fee))
        {
            $data['coupon_fee'] = $coupon_fee->discount;
        } else {
            $data['coupon_fee'] = 0;
        }
        $name = $data['bookingdetails'][0]->customer_name;
        $email = $data['bookingdetails'][0]->email_address;
        $mobile = $data['bookingdetails'][0]->mobile_number_1;
        $data['name'] = $name;
        $data['customer_name'] = $data['bookingdetails'][0]->email_address;
        $data['password'] = $data['bookingdetails'][0]->customer_password;
        $html = $this->load->view('booking_admin_template_email', $data, True);
        echo $html;
    }
    public function sent_booking_admin_confirmation_mail($ref_id)
    {
        $this->load->library('email');
        $data['bookingdetails'] = $this->customerappapi_model->get_bookingdetails_by_refid($ref_id);
        $coupon_fee = $this->customerappapi_model->get_coupon_fee_refid($ref_id);
        if(!empty($coupon_fee))
        {
            $data['coupon_fee'] = $coupon_fee->discount;
        } else {
            $data['coupon_fee'] = 0;
        }
        $name = $data['bookingdetails'][0]->customer_name;
        $email = $data['bookingdetails'][0]->email_address;
        $mobile = $data['bookingdetails'][0]->mobile_number_1;
        $data['name'] = $name;
        $data['customer_name'] = $data['bookingdetails'][0]->email_address;
        $data['password'] = $data['bookingdetails'][0]->customer_password;
        $html = $this->load->view('booking_admin_template_email_mobile', $data, True);

  //       $config = array(
		// 	'protocol'  => 'smtp',
		// 	'smtp_host' => 'ssl://smtp.googlemail.com',
		// 	'smtp_port' => 465,
		// 	'smtp_user' => 'info@elitemaids.emaid.info',
  //           'smtp_pass' => 'Advance1234',
		// 	'mailtype'  => 'html',
		// 	'charset'   => 'utf-8'
		// );

        $config = array(
              'protocol' => 'smtp',
              'smtp_host' => 'smtp.sendgrid.net',
              'smtp_user' => 'apikey',
              // 'smtp_pass' => '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',
            'smtp_pass' => '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',
              'smtp_port' => 587,
              'mailtype'  => 'html',
              'crlf' => "\r\n",
              'newline' => "\r\n"
            );

		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");
		//$this->email->from('info@elitemaids.emaid.info', 'ELITEMAIDS');
        $this->email->from('info@elitemaids.emaid.info', 'ELITEMAIDS');
		//$this->email->to("info@elitemaids.emaid.info");
        $this->email->to("jose.robert@azinova.info");
		//$this->email->bcc("vishnu.m@azinova.info");
		$this->email->subject('Cleaning Booking Confirmation - '.$ref_id);
        $this->email->message($html);
        //$this->email->send();
        echo $this->email->print_debugger();
    }
	
	public function sent_booking_cancel_admin_confirmation_mail($ref_id)
    {
        $this->load->library('email');
        $data['bookingdetails'] = $this->customerappapi_model->get_bookingdetails_by_refid($ref_id);
        
        $name = $data['bookingdetails'][0]->customer_name;
        $email = $data['bookingdetails'][0]->email_address;
        $mobile = $data['bookingdetails'][0]->mobile_number_1;
        $data['name'] = $name;
        $data['customer_name'] = $data['bookingdetails'][0]->email_address;
        $data['password'] = $data['bookingdetails'][0]->customer_password;
        $html = $this->load->view('booking_admin_cancel_template_email_mobile', $data, True);

  //       $config = array(
		// 	'protocol'  => 'smtp',
		// 	'smtp_host' => 'ssl://smtp.googlemail.com',
		// 	'smtp_port' => 465,
		// 	'smtp_user' => 'info@elitemaids.emaid.info',
  //           'smtp_pass' => 'Advance1234',
		// 	'mailtype'  => 'html',
		// 	'charset'   => 'utf-8'
		// );

        $config = array(
              'protocol' => 'smtp',
              'smtp_host' => 'smtp.sendgrid.net',
              'smtp_user' => 'apikey',
              // 'smtp_pass' => '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',
            'smtp_pass' => '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',
              'smtp_port' => 587,
              'mailtype'  => 'html',
              'crlf' => "\r\n",
              'newline' => "\r\n"
            );

		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");
		//$this->email->from('info@elitemaids.emaid.info', 'ELITEMAIDS');
        $this->email->from('info@elitemaids.emaid.info', 'ELITEMAIDS');
		$this->email->to("info@elitemaids.emaid.info");
		//$this->email->bcc('vishnu.m@azinova.info');
		//$this->email->cc('vishnu.m@azinova.info');
		$this->email->subject('Cleaning Booking Cancellation - '.$ref_id);
        $this->email->message($html);
        //$this->email->send();
    }
	
	public function sent_booking_cancel_confirmation_mail($reference_id)
    {
        $this->load->library('email');
        $data['bookingdetails'] = $this->customerappapi_model->get_bookingdetails_by_refid($reference_id);
        $coupon_fee = $this->customerappapi_model->get_coupon_fee_refid($reference_id);
        if(!empty($coupon_fee))
        {
            $data['coupon_fee'] = $coupon_fee->discount;
        } else {
            $data['coupon_fee'] = 0;
        }
        
        $name = $data['bookingdetails'][0]->customer_name;
        $email = $data['bookingdetails'][0]->email_address;
        $mobile = $data['bookingdetails'][0]->mobile_number_1;
        $data['name'] = $name;
        $data['customer_name'] = $data['bookingdetails'][0]->email_address;
        $data['password'] = $data['bookingdetails'][0]->customer_password;
        $html = $this->load->view('booking_cancel_template_email_mobile', $data, True);

  //       $config = array(
		// 	'protocol'  => 'smtp',
		// 	'smtp_host' => 'ssl://smtp.googlemail.com',
		// 	'smtp_port' => 465,
		// 	'smtp_user' => 'info@elitemaids.emaid.info',
  //           'smtp_pass' => 'Advance1234',
		// 	'mailtype'  => 'html',
		// 	'charset'   => 'utf-8'
		// );

        $config = array(
              'protocol' => 'smtp',
              'smtp_host' => 'smtp.sendgrid.net',
              'smtp_user' => 'apikey',
              // 'smtp_pass' => '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',
            'smtp_pass' => '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',
              'smtp_port' => 587,
              'mailtype'  => 'html',
              'crlf' => "\r\n",
              'newline' => "\r\n"
            );

		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");
		//$this->email->from('info@elitemaids.emaid.info', 'ELITEMAIDS');
        $this->email->from('info@elitemaids.emaid.info', 'Elitemaids');
		$this->email->to($email);
        $this->email->subject('Cleaning Booking Cancellation - '.$reference_id);
        $this->email->message($html);
        //$this->email->send();
    }
	
	public function _send_otp_mail($name,$email,$otp)
    {
        $this->load->library('email');
        $data['otp'] = $otp;
		$data['name'] = $name;
        $html = $this->load->view('registration_otp_email', $data, True);
        // $config = array(
        //     'protocol'  => 'smtp',
        //     'smtp_host' => 'ssl://smtp.googlemail.com',
        //     'smtp_port' => 465,
        //     'smtp_user' => 'info@elitemaids.emaid.info',
        //     'smtp_pass' => 'Advance1234',
        //     'mailtype'  => 'html',
        //     'charset'   => 'utf-8'
        // );

        $config = array(
              'protocol' => 'smtp',
              'smtp_host' => 'smtp.sendgrid.net',
              'smtp_user' => 'apikey',
              // 'smtp_pass' => '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',
            'smtp_pass' => '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',
              'smtp_port' => 587,
              'mailtype'  => 'html',
              'crlf' => "\r\n",
              'newline' => "\r\n"
            );

        $this->email->initialize($config);
		//$this->load->library('email',$config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");
        //$this->email->from('info@elitemaids.emaid.info', 'ELITEMAIDS');
        $this->email->from('info@elitemaids.emaid.info', 'Elitemaids');
        $this->email->to($email);
        $this->email->subject('EliteMaids New Registration OTP');
        $this->email->message($html);
        //$this->email->send();
    }
	
	function get_tran_id_from_sess_id($sess_id) 
	{
		$data=array();
		$post_datas=json_encode($data);
		$ServiceURL='https://api.checkout.com/payments/'.$sess_id;
		$secret_key=$this->config->item('payment_secret_key');
		$Headers  = array("Authorization: ".$secret_key);
		$orderCreateResponse = $this->invokeCurlRequest("GET", $ServiceURL, $Headers, $post_datas);
		$paymentResult=json_decode($orderCreateResponse);
		
		$paymentid=$paymentResult->id;
		return $paymentid;
	}
	function invokeCurlRequest($type, $url, $headers, $post) 
	{

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		

		if ($type == "POST") {
		
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		
		}

		$server_output = curl_exec($ch);
		$curl_err = curl_error($ch);
		
		curl_close($ch);
		
		if($curl_err)
		{ return $curl_err;}
		else{return $server_output;}
		
		
	}
}
