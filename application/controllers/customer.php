<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Customer extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        if (!user_permission(user_authenticate(), 6)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $this->load->model('customers_model');
        $this->load->model('reports_model');
        $this->load->model('settings_model');
        $this->load->library("pagination");
        $this->load->model('service_types_model');
        $this->load->model('day_services_model');

        $this->load->model('maids_model');
        $this->load->model('bookings_model');
        $this->load->model('tablets_model');
        $this->load->helper('google_api_helper');
        $this->load->helper('curl_helper');
        $this->load->model('invoice_model');
        $this->load->model('quickbook_model');
    }

    public function index()
    {
        //error_reporting(1);
        // ini_set('memory_limit', '300M');
        ini_set('memory_limit', '-1');
		$settingss = $this->settings_model->get_settings();
        $customers = $this->customers_model->get_customers(FALSE);
        $all_customers = array();
        $customers_search_data = array();
        $i = 0;
        foreach ($customers as $customer) {
            $all_customers[$i] = new stdClass();
            $all_customers[$i]->id = $customer->customer_id;
            $all_customers[$i]->name = html_escape($customer->customer_name);
            $all_customers[$i]->contact_number = html_escape($customer->mobile_number_1);
            $all_customers[$i]->address = str_replace(array("\r\n", "\n\r", "\n", "\r"), ', ', (html_escape($customer->customer_address)));
            $all_customers[$i]->area = html_escape($customer->zone_name . ' - ' . $customer->area_name);
            $all_customers[$i]->status = $customer->customer_status;


            $customer_numbers = array();
            if (strlen(trim($customer->mobile_number_1)) > 0) {
                $customer_numbers[] = html_escape($customer->mobile_number_1);
            }
            if (strlen(trim($customer->mobile_number_2)) > 0) {
                $customer_numbers[] = html_escape($customer->mobile_number_2);
            }
            if (strlen(trim($customer->mobile_number_3)) > 0) {
                $customer_numbers[] = html_escape($customer->mobile_number_3);
            }
            if (strlen(trim($customer->phone_number)) > 0) {
                $customer_numbers[] = html_escape($customer->phone_number);
            }
            if (strlen(trim($customer->fax_number)) > 0) {
                $customer_numbers[] = html_escape($customer->fax_number);
            }

            $customers_search_data[$i] = new stdClass();
            $customers_search_data[$i]->value = $customer->customer_id;
            $customers_search_data[$i]->label = $this->config->item('customer_code_prepend') . $customer->customer_id . ' - ' . html_escape($customer->customer_name) . "<br />" . implode(', ', $customer_numbers);
            //$customers_search_data[$i]->label = $this->config->item('customer_code_prepend') . $customer->customer_id . ' - ' . html_escape($customer->customer_name) . ' (' . html_escape($customer->area_name) . ')' . "<br />" . implode(', ', $customer_numbers);
            $customers_search_data[$i]->name = html_escape($customer->customer_name);

            ++$i;
        }
        ///echo 'hi';exit;
        if ($this->input->is_ajax_request()) {
            if ($this->input->post('action') && $this->input->post('action') == 'get-customers') {
                echo json_encode($customers_search_data);
                exit();
            }
        }
        $data['message'] = "No message";
        if ($this->input->post('customer_sub')) {
            $this->load->library('form_validation');
            //added by vishnu
            $this->form_validation->set_error_delimiters('<div class="error" style="text-align:center;">', '</div>');
            if ($this->input->post('company') == "N") {
                $this->form_validation->set_rules('mobile_number1', 'Mobile number1', 'is_unique[customers.mobile_number_1]');
                $this->form_validation->set_rules('mobile_number2', 'Mobile number2', 'is_unique[customers.mobile_number_2]');
                $this->form_validation->set_rules('mobile_number3', 'Mobile number3', 'is_unique[customers.mobile_number_3]');
                $this->form_validation->set_rules('phone', 'Phone', 'is_unique[customers.phone_number]');
                $this->form_validation->set_message('is_unique', '%s already registered!');

            }
            $this->form_validation->set_rules('customer_name', 'Customer Name', 'required');
            $this->form_validation->set_rules('area', 'Area', 'required');

            $this->form_validation->set_rules('email', 'Email', 'is_unique[customers.email_address]');
            $this->form_validation->set_message('is_unique', '%s is already registered!');

            //            $this->form_validation->set_rules('mobile_number1', 'Mobile_number1', 'callback_rolekey_exists1');
//            $this->form_validation->set_rules('mobile_number2', 'Mobile_number2', 'required');
//            $this->form_validation->set_rules('mobile_number3', 'Mobile_number3', 'callback_rolekey_exists3');
//            $this->form_validation->set_rules('phone', 'Phone', 'callback_rolekey_exists4');

            if ($this->form_validation->run() == TRUE) {

                $customer_name = $this->input->post('customer_name');
                $customer_nick = $this->input->post('customer_nick');
                $area = $this->input->post('area');
                //$image = $this->input->post('img_name_resp');
                //$address = $this->input->post('address');
                $customer_type = $this->input->post('customer_type');
                $contact_person = $this->input->post('contact_person');
                if ($this->input->post('company') == "Y") {
                    $is_company = "Y";
                    $company_name = $this->input->post('company_name');
                } else if ($this->input->post('company') == "N") {
                    $is_company = "N";
                }
                $mobile_number1 = $this->input->post('mobile_number1');
                $mobile_number2 = $this->input->post('mobile_number2');
                $mobile_number3 = $this->input->post('mobile_number3');
                $phone = $this->input->post('phone');
                $fax = $this->input->post('fax');
                $website = $this->input->post('website');
                $user_name = $this->input->post('user_name');
                $email = $this->input->post('email');
                $password = $this->input->post('password');
                $is_flag = $this->input->post('flag');
                $flag_reason = $this->input->post('reasonflag');

                if (!$password || trim($password) == '') {
                    $password = $phone ? $phone : ($mobile_number1 ? $mobile_number1 : ($mobile_number2 ? $mobile_number2 : $mobile_number3));
                }
                $customer_booktype = $this->input->post('customers_type');
                $payment_type = $this->input->post('payment_type');
                $payment_mode = $this->input->post('payment_mode');
                $hourly = $this->input->post('hourly');
                $extra = $this->input->post('extra');
                $weekend = $this->input->post('weekend');
                $latitude = $this->input->post('latitude');
                $longitude = $this->input->post('longitude');
                $key = $this->input->post('key');
                $rating_mail_stat = $this->input->post('rating_mail_stat');
                $notes = $this->input->post('notes');
                $customer_source = $this->input->post('customer_source');
                if ($this->input->post('customer_source_others_val') != "") {
                    $customer_source_val = $this->input->post('customer_source_others_val');
                } else {
                    $customer_source_val = "";
                }
                $customer_source_id = $this->input->post('customer_source_id') ? $this->input->post('customer_source_id') : 0;
                $added = date('Y-m-d h:i:s');
				
				// $initialbal = $this->input->post('initial_balance');
				// $initialbalsign = $this->input->post('initial_bal_sign');
				// if($this->input->post('initial_balance_date') != NULL)
				// {
					// $initialbaldate = DateTime::createFromFormat('d/m/Y', $this->input->post('initial_balance_date'))->format('Y-m-d');
				// } else {
					// $initialbaldate = NULL;
				// }
				// $outstandingamt = 0;
				// if($initialbalsign == 'Dr' && $initialbal > 0)
				// {
					// $outstandingamt = $initialbal;
				// }

                $data = array(
                    'customer_password' => $password,
                    'customer_username' => $user_name,
                    'customer_name' => $customer_name,
                    'customer_nick_name' => $customer_nick,
                    'is_company' => $is_company,
                    'company_name' => $company_name,
                    'mobile_number_1' => $mobile_number1,
                    'mobile_number_2' => $mobile_number2,
                    'mobile_number_3' => $mobile_number3,
                    'whatsapp_no_1' => $this->input->post('whatsapp_no_1') ?: NULL,
                    'phone_number' => $phone,
                    'fax_number' => $fax,
                    'trnnumber' => $this->input->post('trnnumber'),
                    'vatnumber' => $this->input->post('vatnumber'),
                    'email_address' => $email,
                    'website_url' => $website,
                    'customer_type' => $customer_type,
                    'contact_person' => $contact_person,
                    'customer_booktype' => $customer_booktype,
                    'payment_type' => $payment_type,
                    'payment_mode' => $payment_mode,
                    'price_hourly' => $hourly,
                    'price_extra' => $extra,
                    'price_weekend' => $weekend,
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'key_given' => $key,
                    'rating_mail' => $rating_mail_stat,
                    'customer_notes' => $notes,
                    'customer_source' => $customer_source,
                    'customer_source_val' => $customer_source_val,
                    'customer_source_id' => $customer_source_id,
                    'customer_status' => 1,
                    'customer_added_datetime' => $added,
                    'is_flag' => $is_flag,
                    'flag_reason' => $flag_reason,
                    //
                    // 'initial_balance' => $initialbal,
                    // 'initial_bal_sign' => $initialbalsign,
                    // 'initial_bal_date' => $initialbaldate,
                    // 'total_invoice_amount' => $outstandingamt,
                    // 'balance' => $outstandingamt,
                );
                if ($this->input->post('avatar_base64') != null) {
                    $data['customer_photo_file'] = upload_base64($this->input->post('avatar_base64'));
                }
                $res = $this->customers_model->add_customers($data);
                $added_usr_id = user_authenticate();
                $added_user = $this->users_model->get_user_by_id($added_usr_id);
                if ($added_user->is_admin == 'Y') {
                    $msg_actvty = "A new customer " . $customer_name . " is created by " . $added_user->user_fullname;
                } else {
                    $msg_actvty = "A new customer " . $customer_name . " is added by " . $added_user->user_fullname;
                }
                $data_activity = array(
                    'added_user' => user_authenticate(),
                    'action_type' => 'Add_Customer',
                    'action_content' => $msg_actvty,
                    'addeddate' => date('Y-m-d H:i:s'),
                );

                $activty = $this->customers_model->add_user_actvty($data_activity);

                $area = $this->input->post('area');
                $address = $this->input->post('address');
                $apartment = $this->input->post('apartment_no');
                $latitude = $this->input->post('lat');
                $longitude = $this->input->post('lng');

                for ($i = 0; $i < count($address); $i++) {
                    if ($i == 0) {
                        $default = 1;
                    } else {
                        $default = 0;
                    }
                    $datas = array(
                        'customer_id' => $res,
                        'area_id' => $area[$i],
                        'customer_address' => $address[$i],
                        'building' => $apartment[$i],
                        'default_address' => $default,
                        'latitude' => $latitude[$i],
                        'longitude' => $longitude[$i]
                    );
                    $data['area_id'] = $area[0];
                    $data['customer_address'] = $address[0];
                    $data['customer_id'] = $res;
                    $data['apartment'] = $apartment[0];
                    $this->customers_model->add_customer_address($datas);
                }
				
				// if($initialbalsign == 'Dr' && $initialbal > 0)
				// {
					// $invoicecreation = $this->invoice_creation($res,$initialbal,$initialbaldate,$data);
				// }
				
				if($settingss->enableQuickBook == '1')
				{
					$update_to_quickbook = $this->quickbook_new_customer_add($data,$res);
				} else {
					$update_to_quickbook = "";
				}
                //lat hide
                // $odoo_new_customer_add = $this->odoo_new_customer_add($data,$res);
                // if($odoo_new_customer_add > 0)
                // {
                // $this->customers_model->update_customers(array('odoo_package_customer_id' => $odoo_new_customer_add,'odoo_package_customer_status' => 1), $res);
                // }

                $data['message'] = "success";
				$data['quickbook_message'] = $update_to_quickbook;
                redirect('customer/view/' . $res);
                exit();
            }
        }
		
        $data['flats'] = $this->settings_model->get_flats();
        $data['areas'] = $this->settings_model->get_areas();
        $data['maids'] = $this->reports_model->get_all_maids();
		$data['settings'] = $layout_data['settings'] = $settingss;
		$data['customer_sources'] = $this->db->select('cs.*')->from('customer_sources as cs')->where('cs.deleted_at',null)->order_by('cs.sort_order')->get()->result_array();
        $layout_data['content_body'] = $this->load->view('customer', $data, TRUE);
        $layout_data['page_title'] = 'New Customer';
        $layout_data['meta_description'] = 'customers';
        $layout_data['css_files'] = array('jquery.fancybox.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.fancybox.pack.js', 'base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
	
	public function invoice_creation($res,$initialbal,$initialbaldate,$data)
	{
		$this->db->trans_begin();
		$settings = $this->settings_model->get_settings();
		if ($data['apartment'] != "") {
			$apartment = ' , Apt No ' . $data['apartment'];
		} else {
			$apartment = "";
		}
		$addresssss = $data['customer_address'] . $apartment;
		$added_date_time = date('Y-m-d H:i:s');
		$invoice_array = array();
		$invoice_array['customer_id'] = $res;
		$invoice_array['customer_name'] = $data['customer_name'];
		$invoice_array['bill_address'] = $addresssss;
		$invoice_array['added'] = $added_date_time;
		$invoice_array['service_date'] = $initialbaldate;
		$invoice_array['invoice_date'] = $initialbaldate;
		$invoice_array['invoice_due_date'] = $initialbaldate;
		$invoice_array['invoice_added_by'] = user_authenticate();
		$invoice_array['invoice_status'] = 1;
		$invoice_array['invoice_net_amount'] = $initialbal;
        $invoice_array['invoice_tax_amount'] = ($initialbal * $settings->service_vat_percentage) / (100 + $settings->service_vat_percentage);
        $invoice_array['invoice_total_amount'] = $initialbal - $invoice_array['invoice_tax_amount'];
        $invoice_array['balance_amount'] = $initialbal;
		$invoice_array['invoice_notes'] = "Initial Balance";
		$invoice_array['quickbook_sync_show_status'] = 0;
		$invoice_id = $this->day_services_model->add_main_invoice($invoice_array);
		if($invoice_id > 0)
		{
			$main_inv_line = array();
			$main_inv_line['invoice_id'] = $invoice_id;
			$main_inv_line['day_service_id'] = null;
			$main_inv_line['maid_id'] = null;
			$main_inv_line['line_bill_address'] = $addresssss;
			$main_inv_line['description'] = "Initial Balance";
			$main_inv_line['line_net_amount'] = $initialbal;
            $main_inv_line['line_vat_amount'] = ($initialbal * $settings->service_vat_percentage) / (100 + $settings->service_vat_percentage);
            $main_inv_line['line_amount'] = $initialbal - $main_inv_line['line_vat_amount'];
			$invoice_line_id = $this->day_services_model->add_line_invoice($main_inv_line);
			$update_main_inv = array();
			$update_main_inv['invoice_num'] = "INV-".date('Y')."-".sprintf('%04s', $invoice_id);
			$updateinv = $this->day_services_model->update_service_invoice($invoice_id, $update_main_inv);
			if($updateinv)
			{
                $this->db->trans_commit();
				return $updateinv;
			}
		}
        else{
            $this->db->trans_rollback();
			return $res;
        }
	}
	
    function rolekey_exists($key)
    {
        $phnchk = $this->customers_model->role_exists1($key);
        if ($phnchk > 0) {
            $this->validation->set_message('role_exists', 'The %s already exists');
            return true;
        } else {
            return false;
        }
    }

    function customerimgupload()
    {
        $uploaddir = './customer_img/';
        $extension = end(explode(".", $_FILES['uploadfile']['name']));
        $uniqueid = date('Ymdhis');
        $file = $uploaddir . $uniqueid . "." . $extension;
        if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $file)) {
            echo $uniqueid . "." . $extension;
        } else {
            echo "error";
        }
    }

    function bulkmailbannerimgupload()
    {
        $files = glob('blk_mail_img/*'); //get all file names
        foreach ($files as $file) {
            if (is_file($file))
                unlink($file); //delete file
        }
        $uploaddir = './blk_mail_img/';
        $extension = end(explode(".", $_FILES['uploadfile']['name']));
        $uniqueid = date('Ymdhis');
        $file = $uploaddir . $uniqueid . "." . $extension;
        if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $file)) {
            echo $uniqueid . "." . $extension;
        } else {
            echo "error";
        }
    }


    function customer_list()
    {
        $config = array();
        $per_page = $this->uri->segment(2) ? $this->uri->segment(2) : 100;
        $active = $this->uri->segment(3) ? $this->uri->segment(3) : 2;

        $config["base_url"] = base_url() . "customers/" . $per_page . "/" . $active;
        $config["total_rows"] = $this->customers_model->record_count($active);
        $config["per_page"] = $per_page;
        $config["uri_segment"] = 4;
        $this->pagination->initialize($config);
        $limit = $config['per_page'];
        $page = $page = ($this->uri->segment(4)) ? ($this->uri->segment(4) / $limit) : 0;
        $starting = ($page) * $limit;
        $data['starting'] = $starting + 1;
        $data['per_page'] = $per_page;
        $data['active'] = $active;

        $data["customers"] = $this->customers_model->fetch_customers($config["per_page"], $starting, $active);
        $data["links"] = $this->pagination->create_links();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('customer_list', $data, TRUE);
        $layout_data['page_title'] = 'Customers';
        $layout_data['meta_description'] = 'customers';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css');
        $layout_data['external_js_files'] = array();
        $layout_data['customer_active'] = '1';
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }

    public function customer_list_new()
    {
        $data = array();
        $data['active'] = 2;
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $data['customer_sources'] = $this->db->select('cs.*')->from('customer_sources as cs')->where('cs.deleted_at',null)->order_by('cs.sort_order')->get()->result_array();
        $layout_data['content_body'] = $this->load->view('customer_list_new', $data, TRUE);
        $layout_data['page_title'] = 'Customers';
        $layout_data['meta_description'] = 'customers';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css');
        $layout_data['external_js_files'] = array();
        $layout_data['customer_active'] = '1';
        $layout_data['js_files'] = array('bootstrap-datepicker.js', 'jquery.dataTables.min.js', 'customer.js');
        $this->load->view('layouts/default', $layout_data);
    }

    public function list_ajax_customer_list()
    {
        $orders = array();
        $draw = $_POST['draw'];
        $start = $_POST['start'];
        $rowperpage = $_POST['length'];
        $columnIndex = $_POST['order'][0]['column'];
        $columnName = $_POST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_POST['order'][0]['dir'];
        $searchValue = $_POST['search']['value'];

        // Custom search filter 
        $useractive = $_POST['useractive'];
        $regdate = $_POST['regdate'];
        $regdateto = $_POST['regdateto'];
        $paytype = $_POST['paytype'];
        $sourceval = $_POST['sourceval'];
        $customertype = $_POST['customertype'];
        $keywordsearch = $_POST['keywordsearch'];

        $recordsTotal = $this->customers_model->count_all_customers();
        $recordsTotalFilter = $this->customers_model->get_all_customersnew($useractive, $regdate, $regdateto, $paytype, $sourceval, $customertype, $keywordsearch);

        $orders = $this->customers_model->get_all_newcustomers($useractive, $columnName, $columnSortOrder, $rowperpage, $start, $draw, $recordsTotal, $recordsTotalFilter, $regdate, $regdateto, $paytype, $sourceval, $customertype, $keywordsearch);
        //$orders_sum  = $this->login_model->get_all_neworders($searchQuery, $columnName, $columnSortOrder, $rowperpage, $start, $draw, $recordsTotal, $recordsTotalFilter);

        echo json_encode($orders);
        exit();
    }

    //    function customer_list()
//    {
//        //$data=array();
//        $data['customers'] = $this->customers_model->get_all_customers();
//        //print_r($data['customers']);exit;
//        $layout_data['content_body'] = $this->load->view('customer_list', $data, TRUE);
//	$layout_data['page_title'] = 'customers';
//	$layout_data['meta_description'] = 'customers';
//	$layout_data['css_files'] = array('demo.css');
//	$layout_data['external_js_files'] = array();
//        $layout_data['js_files'] = array('base.js', 'mymaids.js','ajaxupload.3.5.js','bootstrap-datepicker.js','jquery.dataTables.min.js');
//	$this->load->view('layouts/default', $layout_data);
//    }
    function view_customer()
    {
        $customer_id = $this->input->post('customer_id');
        //$data = $this->customers_model->get_customer_details($customer_id);
        $data = $this->customers_model->get_all_customer_details($customer_id);
        //print_r($data);exit;
        echo json_encode($data);
    }

    function view_customer_address()
    {
        $cust_id = $this->input->post('cust_id');
        $data = $this->customers_model->get_customer_address($cust_id);
        echo json_encode($data);
    }

    function edit_customer($customer_id)
    {
		$settingss = $this->settings_model->get_settings();
        $data['message'] = "No message";
		$update_to_quickbook = array();
		$update_to_quickbook['status'] = '';
        if ($this->input->post('customer_edit')) {

            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error" style="text-align:center;">', '</div>');

            $this->form_validation->set_rules('email', 'Email', 'callback_email_check');
            $this->form_validation->set_rules('mobile_number1', 'Mobile Number', 'callback_mobile_number_check');

            if ($this->form_validation->run() == TRUE) {

            $quickbookid = $this->input->post('quickbookid');
            $quickbooksyncid = $this->input->post('quickbooksyncid');
            $customer_name = $this->input->post('customer_name');
            $customer_nick = $this->input->post('customer_nick');
            $area = $this->input->post('area');

            //$address = $this->input->post('address');
            $customer_type = $this->input->post('customer_type');
            $contact_person = $this->input->post('contact_person');
            if ($this->input->post('company') == "Y") {
                $is_company = "Y";
                $company_name = $this->input->post('company_name');
            } else if ($this->input->post('company') == "N") {
                $is_company = "N";
                $company_name = "";
            }
            $mobile_number1 = $this->input->post('mobile_number1');
            $mobile_number2 = $this->input->post('mobile_number2');
            $mobile_number3 = $this->input->post('mobile_number3');
            $phone = $this->input->post('phone');
            $fax = $this->input->post('fax');
            $website = $this->input->post('website');
            $user_name = $this->input->post('user_name');
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            if (!$password || trim($password) == '') {
                $password = $phone ? $phone : ($mobile_number1 ? $mobile_number1 : ($mobile_number2 ? $mobile_number2 : $mobile_number3));
            }
            $payment_type = $this->input->post('payment_type');
            $customer_booktype = $this->input->post('customers_type');
            $payment_mode = $this->input->post('payment_mode');
            $hourly = $this->input->post('hourly');
            $extra = $this->input->post('extra');
            $weekend = $this->input->post('weekend');
            $latitude = $this->input->post('latitude');
            $longitude = $this->input->post('longitude');
            $key = $this->input->post('key');
            $rating_mail_stat = $this->input->post('rating_mail_stat');
            $notes = $this->input->post('notes');
            $customer_source = $this->input->post('customer_source');

            $is_flag = $this->input->post('flag');
            $flag_reason = $this->input->post('reasonflag');

            if ($this->input->post('customer_source_others_val') != "") {
                $customer_source_val = $this->input->post('customer_source_others_val');
            } else {
                $customer_source_val = "";
            }
            $customer_source_id = $this->input->post('customer_source') == 'Maid' && $this->input->post('customer_source_id') ? $this->input->post('customer_source_id') : 0;
            $added = date('Y-m-d h:i:s');
            //$old_image = $this->input->post('old_image');
            //$img = $this->input->post('img_name_resp');
            $lat_loc = $this->input->post('lat');
            $lng_loc = $this->input->post('lng');
            /*if ($img == "") {
                $image = $old_image;
            } else {
                $image = $img;
            }*/
			
			// $initialbal = $this->input->post('initial_balance');
			// $initialbalsign = $this->input->post('initial_bal_sign');
			// $initialbalhidden = $this->input->post('initial_balance_hidden');
			// if($this->input->post('initial_balance_date') != NULL)
			// {
				// $initialbaldate = DateTime::createFromFormat('d/m/Y', $this->input->post('initial_balance_date'))->format('Y-m-d');
			// } else {
				// $initialbaldate = NULL;
			// }
			
			// if($initialbal > 0)
			// {
				// $outstandingamt = 0;
				// if($initialbalsign == 'Dr' && $initialbal > 0)
				// {
					// $outstandingamt = $initialbal;
				// }
			// }


            $data = array(
                'customer_password' => $password,
                'customer_username' => $user_name,
                'customer_name' => $customer_name,
                'customer_nick_name' => $customer_nick,
                'is_company' => $is_company,
                'company_name' => $company_name,
                'mobile_number_1' => $mobile_number1,
                'mobile_number_2' => $mobile_number2,
                'mobile_number_3' => $mobile_number3,
                'whatsapp_no_1' => $this->input->post('whatsapp_no_1') ?: NULL,
                'phone_number' => $phone,
                'fax_number' => $fax,
                'trnnumber' => $this->input->post('trnnumber'),
                'vatnumber' => $this->input->post('vatnumber'),
                'email_address' => $email,
                'website_url' => $website,
                'customer_type' => $customer_type,
                'contact_person' => $contact_person,
                'payment_type' => $payment_type,
                'customer_booktype' => $customer_booktype,
                'payment_mode' => $payment_mode,
                'price_hourly' => $hourly,
                'price_extra' => $extra,
                'price_weekend' => $weekend,
                'latitude' => $latitude,
                'longitude' => $longitude,
                'key_given' => $key,
                'rating_mail' => $rating_mail_stat,
                'customer_notes' => $notes,
                'customer_source' => $customer_source,
                'customer_source_val' => $customer_source_val,
                'customer_source_id' => $customer_source_id,
                //'customer_status' => 1,
                'customer_last_modified_datetime' => $added,
                'is_flag' => $is_flag,
                'flag_reason' => $flag_reason,
                //
                // 'initial_balance' => $initialbal,
                // 'initial_bal_sign' => $initialbalsign,
                // 'initial_bal_date' => $initialbaldate,
            );
			
			// if($initialbalhidden > 0)
			// {
				
			// } else {
				// $totinvoiceamt = $this->input->post('totinvoiceamt');
				// $tottbal = $this->input->post('totbal');
				// $newoutstanding = ($initialbal + $totinvoiceamt);
				// $newbalance = ($initialbal + $tottbal);
				
				// $data['total_invoice_amount'] = $newoutstanding;
				// $data['balance'] = $newbalance;
			// }
            if ($this->input->post('avatar_base64') != null) {
                $data['customer_photo_file'] = upload_base64($this->input->post('avatar_base64'));
            }
            //print_r($data);exit;
            $this->customers_model->update_customers($data, $customer_id);
            //$this->customers_model->delete_customer_address($customer_id);                      

            $area = $this->input->post('area');
            $address = $this->input->post('address');
            $apartment = $this->input->post('apartment_no');
            $address_id = $this->input->post('address_id');
            //echo count($address);exit;

            for ($i = 0; $i < count($address); $i++) {
                $datas = array(
                    'customer_id' => $customer_id,
                    'area_id' => $area[$i],
                    'customer_address' => $address[$i],
                    'building' => $apartment[$i],
                    'latitude' => $lat_loc[$i],
                    'longitude' => $lng_loc[$i]
                );
                //$data['area_id'] = $area[$i];
                $data['area_id'] = $area[0];
                $data['customer_address'] = $address[0];
                $data['apartment'] = $apartment[0];
                $data['customer_id'] = $customer_id;

                $this->customers_model->update_customer_address($datas, $address_id[$i]);
            }
            $added_usr_id = user_authenticate();
            $added_user = $this->users_model->get_user_by_id($added_usr_id);

            if ($added_user->is_admin == 'Y') {
                $msg_actvty = "Customer " . $customer_name . " is edited by " . $added_user->user_fullname;
            } else {
                $msg_actvty = "Customer " . $customer_name . " is edited by " . $added_user->user_fullname;
            }

            $data_activity = array(
                'added_user' => user_authenticate(),
                'action_type' => 'Edit_Customer',
                'action_content' => $msg_actvty,
                'addeddate' => date('Y-m-d H:i:s'),
            );

            $activty = $this->customers_model->add_user_actvty($data_activity);

            //$customers = $this->customers_model->get_customer_by_id($customer_id);
            //lat hide
            // $odo_customer_sync = $this->customers_model->get_odoo_by_customerid($customer_id);
            // if($odo_customer_sync->odoo_package_customer_status == 0)
            // {
            // $odoo_new_customer_add = $this->odoo_new_customer_add($data,$customer_id);
            // if($odoo_new_customer_add > 0)
            // {
            // $this->customers_model->update_customers(array('odoo_package_customer_id' => $odoo_new_customer_add,'odoo_package_customer_status' => 1), $customer_id);
            // }
            // } else {
            // $odoo_new_customer_write = $this->odoo_new_customer_write($data,$customer_id,$odo_customer_sync->odoo_package_customer_id);
            // }
			
			// if($initialbalhidden > 0)
			// {
				
			// } else {
				// $invoicecreation = $this->invoice_creation($customer_id,$initialbal,$initialbaldate,$data);
			// }

			if($settingss->enableQuickBook == '1')
			{
				if($quickbookid > 0)
				{
					$update_to_quickbook = $this->quickbook_customer_update($data,$customer_id,$quickbookid,$quickbooksyncid);
				} else {
					$update_to_quickbook = $this->quickbook_new_customer_add($data,$customer_id);
				}
			} else {
				$update_to_quickbook = array();
				$update_to_quickbook['status'] = '';
			}


            $data['message'] = "success";
        }
    }
        //$data = array();
		$data['quickbookmessage'] = $update_to_quickbook;
        $data['customer_details'] = $this->customers_model->get_customer_details($customer_id);
        $data['customer_address'] = $this->customers_model->get_customer_address($customer_id);
        $data['maids'] = $this->reports_model->get_all_maids();
        $data['areas'] = $this->settings_model->get_areas();
        $data['settings'] = $layout_data['settings'] = $settingss;
		$data['customer_sources'] = $this->db->select('cs.*')->from('customer_sources as cs')->where('cs.deleted_at',null)->order_by('cs.sort_order')->get()->result_array();
        $layout_data['content_body'] = $this->load->view('edit_customer', $data, TRUE);
        $layout_data['page_title'] = 'Edit Customer - '.$data['customer_details'][0]['customer_name'];
        $layout_data['meta_description'] = 'customers';
        $layout_data['css_files'] = array('jquery.fancybox.css','toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['customer_active'] = '1';
        $layout_data['js_files'] = array('jquery.fancybox.pack.js', 'base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }

    function editcustomerimgupload()
    {
        $delmainfile = "./customer_img/" . $this->input->post('old_image');
        @unlink($delmainfile);
        $uploaddir = './customer_img/';
        $extension = end(explode(".", $_FILES['uploadfile']['name']));
        $uniqueid = date('Ymdhis');
        $file = $uploaddir . $uniqueid . "." . $extension;
        if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $file)) {
            echo $uniqueid . "." . $extension;
        } else {
            echo "error";
        }
    }

    function remove_customer_address()
    {
        $address_id = $this->input->post(address_id);
        $this->customers_model->remove_customer_address($address_id);
    }

    function remove_customer()
    {
        $customer_id = $this->input->post('customer_id');
        $status = $this->input->post('customer_status');
        $status = $status == 1 ? 0 : 1;

        //Edited by Geethu
        if ($status == 0) {
            $have_bookings = $this->customers_model->get_bookings_by_customer_id_new($customer_id);

            // echo "<pre>";print_r($have_bookings);die;

            if ($have_bookings > 0) {
                echo 'exist_bookings';
                exit();
            }
        }
        // End

        $this->customers_model->delete_customer($customer_id, $status);
        $customers = $this->customers_model->get_customer_by_id($customer_id);

        $added_usr_id = user_authenticate();
        $added_user = $this->users_model->get_user_by_id($added_usr_id);
        if ($added_user->is_admin == 'Y') {
            $msg_actvty = "Customer " . $customers->customer_name . " is enabled by " . $added_user->user_fullname;
        } else {
            $msg_actvty = "Customer " . $customers->customer_name . " is disabled by " . $added_user->user_fullname;
        }

        if ($status == 1) {
            //$msg_actvty = "Customer ".$customers->customer_name." is enabled by Admin user";
            $action_type = "Customer_enable";
        } else {
            //$msg_actvty = "Customer ".$customers->customer_name." is disabled by User";
            $action_type = "Customer_disable";
        }

        $data_activity = array(
            'added_user' => user_authenticate(),
            'action_type' => $action_type,
            'action_content' => $msg_actvty,
            'addeddate' => date('Y-m-d H:i:s'),
        );

        $activty = $this->customers_model->add_user_actvty($data_activity);
        echo $status;
        exit;
    }

    function disable_customer($customer_id, $status)
    {
        //$customer_id = $this->input->post('customer_id');
        //$status = $this->input->post('customer_status');
        $status = $status == 1 ? 0 : 1;
        $this->customers_model->delete_customer($customer_id, $status);
        $customers = $this->customers_model->get_customer_by_id($customer_id);

        $added_usr_id = user_authenticate();
        $added_user = $this->users_model->get_user_by_id($added_usr_id);
        if ($added_user->is_admin == 'Y') {
            $msg_actvty = "Customer " . $customers->customer_name . " is enabled by " . $added_user->user_fullname;
        } else {
            $msg_actvty = "Customer " . $customers->customer_name . " is disabled by " . $added_user->user_fullname;
        }

        if ($status == 1) {
            //$msg_actvty = "Customer ".$customers->customer_name." is enabled by Admin user";
            $action_type = "Customer_enable";
        } else {
            //$msg_actvty = "Customer ".$customers->customer_name." is disabled by User";
            $action_type = "Customer_disable";
        }

        $data_activity = array(
            'added_user' => user_authenticate(),
            'action_type' => $action_type,
            'action_content' => $msg_actvty,
            'addeddate' => date('Y-m-d H:i:s'),
        );

        $activty = $this->customers_model->add_user_actvty($data_activity);
        redirect('/customer/view/' . $customer_id, 'refresh');
    }

    function customer_view($customer_id)
    {

        $this->load->model('bookings_model');

        $data['customer_details'] = $this->customers_model->get_customer_details($customer_id);
        $data['customer_address'] = $this->customers_model->get_customer_address($customer_id);
        $data['areas'] = $this->settings_model->get_areas();
        $data['maid_history'] = $this->customers_model->get_maid_history($customer_id);
        $data['payment_history'] = $this->customers_model->get_payment_history($customer_id);
        $data['call_history'] = $this->reports_model->get_call_report($date = NULL, $customer_id);
        $current_service = $this->customers_model->get_current_service($customer_id);
        $data['start_service_date'] = date('d/m/Y');
        $data['start_time_from'] = strtotime('08.00 am');
        $data['start_time_to'] = strtotime(date('g:i', strtotime('12.00 pm')));
        $data['service_end'] = '';
        $data['service_actual_end_date'] = '';

        if ($this->uri->segment(4) != "") {
            $book_ID = $this->uri->segment(4);
            $data['book_ID'] = $book_ID;
            $booking_details = $this->bookings_model->get_booking_by_id($book_ID);
            // print_r($booking_details);
            $select_date = str_replace('-', '/', $booking_details->service_start_date);

            $data['start_service_date'] = date('d/m/Y', strtotime($select_date));
            $data['start_time_from'] = strtotime(date('g:i a', strtotime($booking_details->time_from)));
            $data['start_time_to'] = strtotime(date('g:i a', strtotime($booking_details->time_to)));
            $data['booking_type'] = $booking_details->booking_type;
            $data['justmop_areaid'] = $booking_details->area_id;
            $data['total_amount'] = $booking_details->total_amount;
            $data['booking_note'] = $booking_details->booking_note;
            $data['cleaning_material'] = $booking_details->cleaning_material;
            if ($booking_details->booking_type == "WE") {
                $data['service_end'] = $booking_details->service_end;
                $data['service_actual_end_date'] = $booking_details->service_actual_end_date;
            }
        }



        $data['customer_zone_area_province'] = $customer_zone_area_province = $this->customers_model->get_customer_zone_province_by_cust_id($customer_id);
        // echo $this->db->last_query();       
        $data['customer_id'] = $customer_id;
        //      print_r($customer_zone_area_province);

        $date_from = date('Y-m-d');
        $date_to = date('Y-m-d', strtotime('+1 month'));

        $times = array();
        $current_hour_index = 0;
        $time = '12:00 am';
        $time_stamp = strtotime($time);

        //for ($i = 0; $i < 24; $i++)
        for ($i = 0; $i < 48; $i++) {
            if (!isset($times['t-' . $i])) {
                $times['t-' . $i] = new stdClass();
            }

            $times['t-' . $i]->stamp = $time_stamp;
            $times['t-' . $i]->display = $time;

            //if(date('H') == $i && $service_date == date('Y-m-d'))
            if (date('H') == $i) {
                //$current_hour_index = 't-' . ($i - 1);
                $current_hour_index = 't-' . (($i * 2) - 1);
            }

            //$time_stamp = strtotime('+60mins', strtotime($time));
            $time_stamp = strtotime('+30mins', strtotime($time));
            $time = date('g:i a', $time_stamp);
        }
        if ($this->input->is_ajax_request()) {
            if ($this->input->post('action') && $this->input->post('action') == 'search-booking') {
                $f_date = explode("/", $this->input->post('date_from'));
                $t_date = explode("/", $this->input->post('date_to'));

                $date_from = $f_date[2] . '-' . $f_date[1] . '-' . $f_date[0];
                $date_to = $t_date[2] . '-' . $t_date[1] . '-' . $t_date[0];

                //
                $ndate = $date_from;
                $all_bookings = array();
                $maid_bookings = array();
                $i = 0;
                $maid_schedule = array();
                $ndate = $date_from;
                $i = 0;
                while (strtotime($date_to) >= strtotime($ndate)) {
                    $maid_schedule[$i] = new stdClass();
                    $maid_schedule[$i]->date = $ndate;

                    ++$i;
                    $ndate = date("Y-m-d", strtotime("+1 day", strtotime($ndate)));
                }
                //                if (!empty($all_bookings)) {
//                    $i = 0;
//                    foreach($maid_schedule as $maid)
//                    {
//                        $onclick = "pause_booking(" . html_escape($booking->booking_id) . ", '" . ($booking->service_date) . "', '" . html_escape($booking->booking_type) . "')";
//                        echo '<tr>
//                            <td style="line-height: 18px;"><center>' . ++$i . '</center></td>
//                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape(date("d/m/Y", strtotime($maid->date))) . '</td>
//                            <td style="line-height: 18px; cursor: pointer; text-align : center;"></td>
//                            <td style="line-height: 18px; cursor: pointer; text-align : center;"></td>
//                            <td style="line-height: 18px; cursor: pointer; text-align : center;"></td>
//                            <td style="line-height: 18px; cursor: pointer; text-align : center;"></td>    
//                            <td style="line-height: 18px; cursor: pointer; text-align : center;"><a class="btn btn-danger btn-small" href="javascript:void" onclick="' . $onclick . '"><i class="btn-icon-only icon-pause"> </i>Pause Booking</a></td>
//                        </tr>';
//                    
//                    }
//                }
//                echo "<pre>";
//                print_r($maid_schedule);
//                echo "</pre>";
                $i = 1;
                foreach ($maid_schedule as $rowschedule) {
                    $schedle_date = $rowschedule->date;
                    //edited by vishnu
                    //$booking_by_date = $this->bookings_model->get_schedule_by_date_and_customer($customer_id,$schedle_date);
                    $booking_by_date = $this->bookings_model->get_schedule_by_date_and_customer_new($customer_id, $schedle_date);
                    //ends
//                    echo "<pre>";
//                    print_r($booking_by_date);
//                    echo "</pre>";
                    if (!empty($booking_by_date)) {

                        foreach ($booking_by_date as $booking) {
                            $newDate = date("d/m/Y", strtotime($booking->scheduledates));
                            //$onclick = "pause_booking(" . html_escape($booking->booking_id) . ", '" . ($schedle_date) . "', '" . html_escape($booking->booking_type) . "')";
                            if ($booking->booking_type == "WE") {
                                $classes = "bg-we";
                            } else if ($booking->booking_type == "OD") {
                                $classes = "bg-od";
                            } else {
                                $classes = "bg-bw";
                            }
                            $ndate = date("Y/m/d", strtotime($booking->scheduledates));
                            $day = date('l', strtotime($ndate));

                            //added by vishnu
                            if ($booking->booking_type == "OD") {
                                $check_booking = $this->bookings_model->checkbooking_in_delete_oneday($booking->booking_id, $booking->scheduledates);
                                if ($check_booking == 0) {
                                    $onclick = "pause_booking_modal(" . html_escape($booking->booking_id) . ", '" . html_escape($schedle_date) . "', '" . html_escape($booking->booking_type) . "')";
                                    $pausetext = '<i class="btn-icon-only icon-pause"> </i>Pause';
                                    $btnclass = "n-btn red-btn mb-0";
                                } else {
                                    $pausetext = "<i class='btn-icon-only icon-play'> </i>Resume";
                                    $btnclass = "n-btn mb-0";
                                    $onclick = "resume_booking_modal(" . html_escape($booking->booking_id) . ", '" . html_escape($schedle_date) . "', '" . html_escape($booking->booking_type) . "')";
                                }
                            } else if ($booking->booking_type == "WE") {
                                $check_booking = $this->bookings_model->checkbooking_in_delete_weekly($booking->booking_id, $booking->scheduledates);
                                if ($check_booking == 0) {
                                    $onclick = "pause_booking_modal(" . html_escape($booking->booking_id) . ", '" . html_escape($schedle_date) . "', '" . html_escape($booking->booking_type) . "')";
                                    $pausetext = '<i class="btn-icon-only icon-pause"> </i>Pause';
                                    $btnclass = "n-btn red-btn mb-0";
                                } else {
                                    $onclick = "resume_booking_modal(" . html_escape($booking->booking_id) . ", '" . html_escape($schedle_date) . "', '" . html_escape($booking->booking_type) . "')";
                                    $pausetext = "<i class='btn-icon-only icon-play'> </i>Resume";
                                    $btnclass = "n-btn mb-0";
                                }
                            } else if ($booking->booking_type == "BW") {
                                $check_booking = $this->bookings_model->checkbooking_in_delete_weekly($booking->booking_id, $booking->scheduledates);
                                if ($check_booking == 0) {
                                    $onclick = "pause_booking_modal(" . html_escape($booking->booking_id) . ", '" . html_escape($booking->scheduledates) . "', '" . html_escape($booking->booking_type) . "')";
                                    $pausetext = '<i class="btn-icon-only icon-pause"> </i>Pause';
                                    $btnclass = "n-btn red-btn mb-0";
                                } else {
                                    $onclick = "resume_booking_modal(" . html_escape($booking->booking_id) . ", '" . html_escape($booking->scheduledates) . "', '" . html_escape($booking->booking_type) . "')";
                                    $pausetext = "<i class='btn-icon-only icon-play'> </i>Resume";
                                    $btnclass = "n-btn mb-0";
                                }
                            }
                            //ends

                            echo '<tr>
                                        <td style="line-height: 18px;"><center>' . $i . '</center></td>
                                        <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($newDate) . '</td>
                                        <td style="line-height: 18px; cursor: pointer; text-align : center;">' . $day . '</td>
                                        <td style="line-height: 18px; cursor: pointer; text-align : center;"><span type="button" class="btn btn-block ' . $classes . '">' . date("h:i A", strtotime($booking->time_from)) . ' - ' . date("h:i A", strtotime($booking->time_to)) . '</span></td>
                                        <td style="line-height: 18px; cursor: pointer; text-align : center;">' . booking_type($booking->booking_type) . '</td>
                                        <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($booking->maid_name) . '</td>    
                                        <td style="line-height: 18px; cursor: pointer; text-align : center;"><button type="button" class="btn-block ' . $btnclass . '" style="" href="javascript:void" onclick="' . $onclick . '">' . $pausetext . '</button></td>
                                </tr>';
                            $i++;
                        }

                    }
					
                    //                    else {
//                        echo '<tr><td style="line-height: 18px;" colspan="7"><center>No Records!</center></td></tr>';
//                    }
                }
                exit();

                //

                /*$bookings = $this->bookings_model->get_schedule_by_customer($customer_id, $date_from, $date_to);
                if (!empty($bookings)) {
                $i = 0;
                foreach ($bookings as $booking) {
                $onclick = "pause_booking(" . html_escape($booking->booking_id) . ", '" . ($booking->service_date) . "', '" . html_escape($booking->booking_type) . "')";
                echo '<tr>
                <td style="line-height: 18px;"><center>' . ++$i . '</center></td>
                <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($booking->service_date) . '</td>
                <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($booking->shift_day) . '</td>
                <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($booking->time_from) . '-' . html_escape($booking->time_to) . '</td>
                <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($booking->booking_type) . '</td>
                <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($booking->maid_name) . '</td>    
                <td style="line-height: 18px; cursor: pointer; text-align : center;"><a class="btn btn-danger btn-small" href="javascript:void" onclick="' . $onclick . '"><i class="btn-icon-only icon-pause"> </i>Pause Booking</a></td>
                </tr>';
                }
                } else {
                echo '<tr><td style="line-height: 18px;" colspan="7"><center>No Records!</center></td></tr>';
                }
                exit();*/
            }
            if ($this->input->post('action') && $this->input->post('action') == 'pause-booking') {
                if ($this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0 && $this->input->post('service_date') && strlen($this->input->post('service_date')) > 0 && $this->input->post('booking_type') && strlen($this->input->post('booking_type')) > 0) {
                    $s_date = explode("-", $this->input->post('service_date'));
                    $booking_id = trim($this->input->post('booking_id'));
                    $booking_type = trim($this->input->post('booking_type'));
                    $d_booking = $this->bookings_model->get_booking_by_id($booking_id);

                    if ($d_booking->is_locked == 1 && $d_booking->booked_by != user_authenticate()) {
                        echo 'locked';
                        exit();
                    }
                    $service_date = $s_date[0] . '-' . $s_date[1] . '-' . $s_date[2];
                    if ($service_date < date('Y-m-d')) {
                        $week_day = date('l', strtotime($service_date));
                        $service_date = date('Y-m-d', strtotime('next ' . $week_day, strtotime(date('Y-m-d'))));
                    }
                    $booking_delete_fields = array();
                    $booking_delete_fields['booking_id'] = $booking_id;
                    $booking_delete_fields['service_date'] = $service_date;
                    $booking_delete_fields['deleted_by'] = user_authenticate();

                    if ($booking_type == 'OD') {
                        $booking_delete_done = $this->bookings_model->update_booking($booking_id, array('booking_status' => 2), 'Delete');
                    } else {
                        $booking_delete_done = $this->bookings_model->add_booking_delete($booking_delete_fields);
                    }

                    if ($booking_delete_done) {
                        echo 'success';
                        exit();
                    } else {
                        echo 'error';
                        exit();
                    }
                }
            }

            //Edited by vishnu
            if ($this->input->post('action') && $this->input->post('action') == 'start-booking') {
                if ($this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0 && $this->input->post('service_date') && strlen($this->input->post('service_date')) > 0 && $this->input->post('booking_type') && strlen($this->input->post('booking_type')) > 0) {
                    $s_date = explode("-", $this->input->post('service_date'));
                    $booking_id = trim($this->input->post('booking_id'));
                    $booking_type = trim($this->input->post('booking_type'));
                    $d_booking = $this->bookings_model->get_booking_by_id($booking_id);

                    if ($d_booking->is_locked == 1 && $d_booking->booked_by != user_authenticate()) {
                        echo 'locked';
                        exit();
                    }
                    $service_date = $s_date[0] . '-' . $s_date[1] . '-' . $s_date[2];
                    if ($service_date < date('Y-m-d')) {
                        $week_day = date('l', strtotime($service_date));
                        $service_date = date('Y-m-d', strtotime('next ' . $week_day, strtotime(date('Y-m-d'))));
                    }
                    $booking_delete_fields = array();
                    $booking_delete_fields['booking_id'] = $booking_id;
                    $booking_delete_fields['service_date'] = $service_date;
                    $booking_delete_fields['deleted_by'] = user_authenticate();

                    if ($booking_type == 'OD') {
                        $booking_delete_done = $this->bookings_model->restart_booking($booking_id, array('booking_status' => 1), $service_date);
                    } else {
                        $booking_delete_done = $this->bookings_model->restart_booking($booking_id, array('booking_status' => 1), $service_date);
                    }

                    if ($booking_delete_done) {
                        echo 'success';
                        exit();
                    } else {
                        echo 'error';
                        exit();
                    }
                }
            }

            //
            //Edited by sandeep

            if ($this->input->post('action') && $this->input->post('action') == 'assign-maid' && $this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0) {

                $book_id = $this->input->post('booking_id');
                $customer_id = $this->input->post('customer_id');
                $customer_address_id = $this->input->post('customer_address_id');
                if ($this->input->post('clean_mat') == 'Y') {
                    $clean_mat = 'Y';
                } else {
                    $clean_mat = 'N';
                }
                $s_date = explode('/', trim($this->input->post('service_date')));
                $schedule_date = $s_date[0] . '-' . $s_date[1] . '-' . $s_date[2];
                $service_date = date('Y-m-d', strtotime($schedule_date));

                $booking_type = $this->input->post('booking_type');
                $book_fields['maid_id'] = $this->input->post('maid_id');
                $book_fields['total_amount'] = $this->input->post('total_amt');
                $book_fields['booking_note'] = $this->input->post('b_notes');
                $book_fields['cleaning_material'] = $clean_mat;
                $book_fields['booking_type'] = $booking_type;
                $book_fields['time_from'] = date('H:i', trim($this->input->post('time_from')));
                $book_fields['time_to'] = date('H:i', trim($this->input->post('time_to')));
                $book_fields['booked_by'] = user_authenticate();
                $book_fields['booking_status'] = '1';
                $book_fields['service_start_date'] = $service_date;
                $updatebooking = $this->bookings_model->update_booking($book_id, $book_fields);
                if ($updatebooking) {

                    $return = array();
                    $return['status'] = 'success';
                    $return['maid_id'] = $this->input->post('maid_id');
                    $return['customer_id'] = $customer_id;

                    echo json_encode($return);
                    exit();
                }
            }

            //        Edited by sandeep

            if ($this->input->is_ajax_request()) {
                if ($this->input->post('action') && $this->input->post('action') == 'book-maid') {
                    $s_date = explode('/', trim($this->input->post('service_date')));

                    $schedule_date = $s_date[0] . '-' . $s_date[1] . '-' . $s_date[2];

                    $week_day_names = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');

                    $service_date = date('Y-m-d', strtotime($schedule_date));
                    $service_week_day = date('w', strtotime($service_date));
                    $bookings = $this->bookings_model->get_schedule_by_date($service_date);
                    foreach ($bookings as $booking) {

                        $booked_slots[$booking->maid_id]['time'][strtotime($booking->time_from)] = strtotime($booking->time_to);

                    }


                    if ($this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0 && $this->input->post('customer_address_id') && is_numeric($this->input->post('customer_address_id')) && $this->input->post('customer_address_id') > 0 && $this->input->post('maid_id') && is_numeric($this->input->post('maid_id')) && $this->input->post('maid_id') > 0 && $this->input->post('time_from') && is_numeric($this->input->post('time_from')) && $this->input->post('time_from') > 0 && $this->input->post('time_to') && is_numeric($this->input->post('time_to')) && $this->input->post('time_to') > 0 && $this->input->post('time_from') < $this->input->post('time_to') && $this->input->post('booking_type') && ($this->input->post('booking_type') == 'OD' || $this->input->post('booking_type') == 'WE' || $this->input->post('booking_type') == 'BW')) {



                        $customer_id = trim($this->input->post('customer_id'));
                        $customer_address_id = trim($this->input->post('customer_address_id'));
                        $maid_id = trim($this->input->post('maid_id'));

                        $time_from = date('H:i', trim($this->input->post('time_from')));
                        $time_to = date('H:i', trim($this->input->post('time_to')));
                        $booking_type = trim($this->input->post('booking_type'));
                        $total_amt = trim($this->input->post('total_amt'));
                        $clean_mat = trim($this->input->post('clean_mat'));
                        $b_notes = trim($this->input->post('b_notes'));

                        $repeat_days = array($service_week_day);
                        $repeat_end = 'ondate';
                        $service_end_date = $service_date;


                        if (preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_from) == FALSE || preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_to) == FALSE) {
                            echo 'refresh';
                            exit();
                        }

                        if ($booking_type != 'OD') {


                            if ($this->input->post('repeat_end') && ($this->input->post('repeat_end') == 'never' || $this->input->post('repeat_end') == 'ondate')) {

                                $repeat_end = $this->input->post('repeat_end');
                                if ($repeat_end == 'ondate') {

                                    if ($this->input->post('repeat_end_date')) {
                                        $repeat_end_date = $this->input->post('repeat_end_date');

                                        $repeat_end_date_split = explode('/', $repeat_end_date);

                                        if (count($repeat_end_date_split) == 3 && checkdate($repeat_end_date_split[1], $repeat_end_date_split[0], $repeat_end_date_split[2])) {

                                            $s_date = new DateTime($service_date);
                                            $e_date = new DateTime($repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0]);


                                            $diff = abs(strtotime($repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0]) - strtotime($service_date));

                                            $years = floor($diff / (365 * 60 * 60 * 24));
                                            $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
                                            $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));

                                            if ($days < 7) //if($diff->days < 7)
                                            {
                                                echo 'refresh';
                                                exit();
                                            }

                                            $service_end_date = $repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0];



                                        } else {
                                            echo 'refresh';
                                            exit();
                                        }


                                    } else {
                                        echo 'refresh';
                                        exit();
                                    }


                                }

                            } else {
                                echo 'refresh';
                                exit();
                            }



                        }


                        $time_from_stamp = trim($this->input->post('time_from'));
                        $time_to_stamp = trim($this->input->post('time_to'));

                        if ($booking_type == 'OD') {
                            if (isset($booked_slots[$maid_id]['time'])) {
                                foreach ($booked_slots[$maid_id]['time'] as $f_time => $t_time) {
                                    if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
                                        $return = array();
                                        $return['status'] = 'error';
                                        $return['message'] = 'The selected time slot is not available for this maid';

                                        echo json_encode($return);
                                        exit();
                                    }
                                }
                            }
                        }

                        $today_week_day = date('w', strtotime($service_date));
                        if ($booking_type == 'WE') {

                            if ($service_week_day < $today_week_day) {
                                $day_diff = (6 - $today_week_day + $service_week_day + 1);
                            } else {
                                $day_diff = $service_week_day - $today_week_day;
                            }

                            $service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));
                            $bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($maid_id, $service_week_day);

                            foreach ($bookings_on_day as $booking_on_day) {

                                $s_date_stamp = strtotime($service_start_date);
                                $e_date_stamp = strtotime($service_end_date);
                                $bs_date_stamp = strtotime($booking_on_day->service_start_date);
                                $be_date_stamp = strtotime($booking_on_day->service_actual_end_date);

                                if (($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $booking_on_day->service_end == '0' && $repeat_end == 'never') || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
                                //if(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
                                {
                                    $f_time = strtotime($booking_on_day->time_from);
                                    $t_time = strtotime($booking_on_day->time_to);



                                    if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) //|| ($time_from_stamp <= $f_time && $time_to_stamp <= $t_time)
                                    {


                                        //echo "<br>".$booking_on_day->booking_id;
                                        $return = array();
                                        $return['status'] = 'error';
                                        $return['message'] = 'The selected time slot is not available for this maid on ' . $week_day_names[$service_week_day] . 's';

                                        echo json_encode($return);
                                        exit();
                                    }


                                }




                            }


                        }



                        $todays_new_booking = array();

                        if ($service_week_day < $today_week_day) {
                            $day_diff = (6 - $today_week_day + $service_week_day + 1);
                        } else {
                            $day_diff = $service_week_day - $today_week_day;
                        }

                        $service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));
                        $service_end_date = $repeat_end == 'never' ? $service_start_date : $service_end_date;

                        $booking_fields = array();
                        $booking_fields['customer_id'] = $customer_id;
                        $booking_fields['customer_address_id'] = $customer_address_id;
                        $booking_fields['maid_id'] = $maid_id;

                        $booking_fields['service_start_date'] = $service_start_date;
                        $booking_fields['service_week_day'] = $service_week_day;
                        $booking_fields['time_from'] = $time_from;
                        $booking_fields['time_to'] = $time_to;
                        $booking_fields['booking_type'] = $booking_type;
                        $booking_fields['booking_note'] = $b_notes;
                        if ($booking_type == 'OD') {
                            $booking_fields['service_end'] = 1;
                        } else {
                            $booking_fields['service_end'] = $repeat_end == 'ondate' ? 1 : 0;
                        }
                        $booking_fields['service_end_date'] = $service_end_date;
                        $booking_fields['total_amount'] = $total_amt;
                        if ($clean_mat == 'Y') {
                            $clean_mat = 'Y';
                        } else {
                            $clean_mat = 'N';
                        }
                        $booking_fields['booked_by'] = user_authenticate();
                        $booking_fields['cleaning_material'] = $clean_mat;
                        $booking_fields['booking_category'] = 'C';
                        $booking_fields['booking_status'] = 1;
                        $booking_fields['booked_datetime'] = date('Y-m-d H:i:s');
                        if ($booking_type == 'WE') {
                            $updatefield = array();
                            $updatefield['customer_booktype'] = 1;
                            $updatess = $this->customers_model->update_booktype($updatefield, $customer_id);
                        }

                        $booking_id = $this->bookings_model->add_booking($booking_fields);

                        if ($booking_id) {

                            $booking_done = TRUE;



                            if ($service_start_date == date('Y-m-d')) {
                                $todays_new_booking[] = $booking_id;
                            }
                            $c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
                            if (isset($c_address->zone_id)) {
                                $tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
                                $booking = $this->bookings_model->get_booking_by_id($booking_id);

                                $push_fields = array();
                                $push_fields['tab_id'] = $tablet->tablet_id;
                                $push_fields['type'] = 1;
                                $push_fields['message'] = 'New Booking. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->newtime_from . '-' . $booking->newtime_to;
                                $push_fields['maid_id'] = $booking->maid_id;
                                $push_fields['title'] = "New Booking";
                                $push_fields['customer_name'] = $booking->customer_name;
                                $push_fields['maid_name'] = $booking->maid_name;
                                $push_fields['booking_time'] = $booking->newtime_from . ' - ' . $booking->newtime_to;
                                if (strtotime($schedule_date) <= strtotime(date('d-M-Y'))) {
                                    $push = $this->bookings_model->add_push_notifications($push_fields);
                                }
                            }



                        }

                        if (isset($booking_done)) {

                            if (count($todays_new_booking) > 0 && isset($customer_address_id)) {
                                $c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);


                                if (isset($c_address->zone_id)) {
                                    $tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
                                    $booking = $this->bookings_model->get_booking_by_id($booking_id);
                                    if ($tablet) {

                                        if (strtotime($schedule_date) <= strtotime(date('d-M-Y'))) {
                                            $deviceid = $tablet->google_reg_id;
                                            // optional payload
                                            $payload = array();
                                            $payload['isfeedback'] = false;
                                            if (isset($push) && $push > 0) {
                                                $payload['pushid'] = $push;
                                            } else {
                                                $payload['pushid'] = 0;
                                            }

                                            $title = "New Booking";
                                            $message = 'New Booking. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->newtime_from . '-' . $booking->newtime_to;
                                            $res = array();
                                            $res['data']['title'] = $title;
                                            $res['data']['is_background'] = false;
                                            $res['data']['message'] = $message;
                                            $res['data']['image'] = "";
                                            $res['data']['payload'] = $payload;
                                            $res['data']['customer'] = $booking->customer_name;
                                            $res['data']['maid'] = $booking->maid_name;
                                            $res['data']['bookingTime'] = $booking->newtime_from . ' - ' . $booking->newtime_to;
                                            $res['data']['timestamp'] = date('Y-m-d G:i:s');
                                            $regId = $deviceid;
                                            $fields = array(
                                                'to' => $regId,
                                                'data' => $res,
                                            );
                                            $return = android_customer_push($fields);
                                            //$return = android_push(array($tablet->google_reg_id), array('action' => 'new-booking', 'booking-ids' => $todays_new_booking, 'message' => 'New Booking. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->time_from . '-' . $booking->time_to));

                                        }

                                    }


                                }


                            }

                            $return = array();
                            $return['status'] = 'success';
                            $return['maid_id'] = $maid_id;
                            $return['customer_id'] = $customer_id;
                            $return['time_from'] = $time_from;
                            $return['time_to'] = $time_to;


                            echo json_encode($return);
                            exit();


                        }


                    }


                }
            }


            if ($this->input->post('action') && $this->input->post('action') == 'get-free-maids' && $this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0) {
                $booking_id = trim($this->input->post('booking_id'));
                $booking = $this->bookings_model->get_booking_by_id($booking_id);
                $same_zone = $this->input->post('same_zone');

                $not_free_maids = $this->bookings_model->get_not_free_maids_by_booking_id($booking_id, $booking->service_week_day, /*$booking->service_start_date . ' ' .*/$booking->time_from, /*$booking->service_start_date . ' ' .*/$booking->time_to, $booking->service_start_date);
                $all_maids = $this->maids_model->get_maids();

                $nf_maids = array();
                $all_maid_list = array();
                foreach ($not_free_maids as $nmaid) {
                    array_push($nf_maids, $nmaid->maid_id);
                }
                foreach ($all_maids as $maid) {
                    array_push($all_maid_list, $maid->maid_id);
                }
                $free_maids = array_diff($all_maid_list, $nf_maids);


                if (!empty($free_maids)) {
                    //$f_maids = array_diff($free_maids, $not_free_maids);
                    $free_maid_dtls = array();
                    $free_maid_ids = array();
                    foreach ($free_maids as $f_maid) {
                        $maid = $this->maids_model->get_maid_by_id($f_maid);
                        array_push($free_maid_dtls, $maid);
                        array_push($free_maid_ids, $f_maid);
                    }
                    if ($same_zone == 1) {
                        $zone = $this->customers_model->get_customer_zone_by_address_id($booking->customer_address_id);
                        $same_zone_maids = $this->bookings_model->get_same_zone_maids($zone->zone_id, $free_maid_ids, $booking->service_week_day);
                        $free_maid_dtls = array();
                        foreach ($same_zone_maids as $same_zone_maid) {
                            $maid = $this->maids_model->get_maid_by_id($same_zone_maid->maid_id);
                            array_push($free_maid_dtls, $maid);
                        }
                        if (empty($free_maid_dtls)) {
                            $return = array();
                            $return['status'] = 'error';
                            $return['message'] = 'There are no same zone maids available';

                            echo json_encode($return);
                            exit();

                        }
                    }
                    echo json_encode($free_maid_dtls);
                    exit();
                } else {
                    $return = array();
                    $return['status'] = 'error';
                    $return['message'] = 'There are no maids available for the selected time slot';

                    echo json_encode($return);
                    exit();
                }
            }

            if ($this->input->post('action') && $this->input->post('action') == 'copy-maid') {
                if ($this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0 && $this->input->post('maid_id') && is_numeric($this->input->post('maid_id')) && $this->input->post('maid_id') > 0) {
                    if (strtotime($schedule_date) < strtotime(date('d-M-Y'))) {
                        echo 'refresh';
                        exit();
                    }
                    $booking_id = trim($this->input->post('booking_id'));
                    $booking = $this->bookings_model->get_booking_by_id($booking_id);

                    //                                        if($booking->is_locked == 1 && $booking->booked_by != user_authenticate())
//                                        {
//                                            echo 'locked';
//                                            exit();
//                                        }
                    $customer_id = html_escape($booking->customer_id);
                    $customer_address_id = html_escape($booking->customer_address_id);
                    $maid_id = trim($this->input->post('maid_id'));
                    $service_type_id = html_escape($booking->service_type_id);
                    $time_from = date('H:i', strtotime(html_escape($booking->time_from)));
                    $time_to = date('H:i', strtotime(html_escape($booking->time_to)));
                    $booking_type = html_escape($booking->booking_type);
                    $repeat_days = array(html_escape($booking->service_week_day));
                    $repeat_end = html_escape($booking->service_end) == 0 ? 'never' : 'ondate';
                    $service_end_date = html_escape($booking->service_actual_end_date);
                    $service_start_date = html_escape($booking->service_start_date);
                    $pending_amount = html_escape($booking->pending_amount);

                    $discount = html_escape($booking->discount);
                    $is_locked = html_escape($booking->is_locked) ? 1 : 0;
                    $booking_note = html_escape($booking->booking_note) ? html_escape($booking->booking_note) : '';

                    //if(preg_match("/([01]?[0-9]|2[0-3]):[0][0]/", $time_from) == FALSE || preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_to) == FALSE)
                    if (preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_from) == FALSE || preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_to) == FALSE) {
                        echo 'refresh';
                        exit();
                    }

                    if ($booking_type != 'OD') {
                        if ($repeat_days && is_array($repeat_days) && count($repeat_days) > 0 && count($repeat_days) <= 7 && $repeat_end && ($repeat_end == 'never' || $repeat_end == 'ondate')) {
                            //$repeat_days = $this->input->post('repeat_days');
                            //$repeat_end = $this->input->post('repeat_end');

                            if ($repeat_end == 'ondate') {
                                if ($this->input->post('repeat_end_date')) {
                                    $repeat_end_date = $this->input->post('repeat_end_date');

                                    $repeat_end_date_split = explode('/', $repeat_end_date);
                                    if (count($repeat_end_date_split) == 3 && checkdate($repeat_end_date_split[1], $repeat_end_date_split[0], $repeat_end_date_split[2])) {
                                        $s_date = new DateTime($service_date);
                                        $e_date = new DateTime($repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0]);
                                        //$diff = $s_date->diff($e_date);

                                        $diff = abs(strtotime($repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0]) - strtotime($service_date));

                                        $years = floor($diff / (365 * 60 * 60 * 24));
                                        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
                                        $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));

                                        if ($days < 7) //if($diff->days < 7)
                                        {
                                            echo 'refresh';
                                            exit();
                                        }

                                        $service_end_date = $repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0];
                                    } else {
                                        echo 'refresh';
                                        exit();
                                    }
                                } else {
                                    echo 'refresh';
                                    exit();
                                }
                            }
                        } else {
                            echo 'refresh';
                            exit();
                        }
                    }

                    $time_from_stamp = (html_escape($booking->time_from));
                    $time_to_stamp = (html_escape($booking->time_to));

                    if ($booking_type == 'OD') {
                        if (isset($booked_slots[$maid_id]['time'])) {
                            foreach ($booked_slots[$maid_id]['time'] as $f_time => $t_time) {
                                if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
                                    $return = array();
                                    $return['status'] = 'error';
                                    $return['message'] = 'The selected time slot is not available for this maid';

                                    echo json_encode($return);
                                    exit();
                                }
                            }
                        }
                    }

                    $today_week_day = date('w', strtotime($service_date));

                    if ($booking_type == 'WE') {
                        foreach ($repeat_days as $repeat_day) {
                            if ($repeat_day < $today_week_day) {
                                $day_diff = (6 - $today_week_day + $repeat_day + 1);
                            } else {
                                $day_diff = $repeat_day - $today_week_day;
                            }

                            $service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));

                            $bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($maid_id, $repeat_day);
                            foreach ($bookings_on_day as $booking_on_day) {
                                $s_date_stamp = strtotime($service_start_date);
                                $e_date_stamp = strtotime($service_end_date);
                                $bs_date_stamp = strtotime($booking_on_day->service_start_date);
                                $be_date_stamp = strtotime($booking_on_day->service_actual_end_date);

                                //if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
                                if (($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp)))) {
                                    $f_time = strtotime($booking_on_day->time_from);
                                    $t_time = strtotime($booking_on_day->time_to);

                                    if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
                                        $return = array();
                                        $return['status'] = 'error';
                                        $return['message'] = 'The selected time slot is not available for this maid on ' . $week_day_names[$repeat_day] . 's';

                                        echo json_encode($return);
                                        exit();
                                    }
                                }
                            }
                        }
                    }
                } else {
                    echo 'refresh';
                    exit();
                }

                $todays_new_booking = array();
                foreach ($repeat_days as $repeat_day) {
                    if ($repeat_day < $today_week_day) {
                        $day_diff = (6 - $today_week_day + $repeat_day + 1);
                    } else {
                        $day_diff = $repeat_day - $today_week_day;
                    }

                    $service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));
                    $service_end_date = $repeat_end == 'never' ? $service_start_date : $service_end_date;

                    $booking_fields = array();
                    $booking_fields['customer_id'] = $customer_id;
                    $booking_fields['customer_address_id'] = $customer_address_id;
                    $booking_fields['maid_id'] = $maid_id;
                    $booking_fields['service_type_id'] = $service_type_id;
                    $booking_fields['service_start_date'] = $service_start_date;
                    $booking_fields['service_week_day'] = $repeat_day;
                    $booking_fields['time_from'] = $time_from;
                    $booking_fields['time_to'] = $time_to;
                    $booking_fields['booking_type'] = $booking_type;
                    $booking_fields['service_end'] = $repeat_end == 'ondate' ? 1 : 0;
                    $booking_fields['service_end_date'] = $service_end_date;
                    $booking_fields['booking_note'] = $booking_note;
                    $booking_fields['pending_amount'] = $pending_amount;
                    $booking_fields['discount'] = $discount;
                    $booking_fields['booked_by'] = user_authenticate();
                    $booking_fields['booking_status'] = 1;
                    $booking_fields['booking_category'] = 'C';
                    $booking_fields['booked_datetime'] = date('Y-m-d H:i:s');

                    $booking_id = $this->bookings_model->add_booking($booking_fields);

                    if ($booking_id) {
                        $booking_done = TRUE;

                        //$this->appointment_bill($booking_id);

                        if ($service_start_date == date('Y-m-d')) {
                            $todays_new_booking[] = $booking_id;
                        }
                    }
                }

                if (isset($booking_done)) {
                    if (count($todays_new_booking) > 0 && isset($customer_address_id)) {
                        $c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
                        if (isset($c_address->zone_id)) {
                            $tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
                            if ($tablet) {
                                if (strtotime($schedule_date) <= strtotime(date('d-M-Y'))) {
                                    $booking = $this->bookings_model->get_booking_by_id($booking_id);
                                    $push_fields = array();
                                    $push_fields['tab_id'] = $tablet->tablet_id;
                                    $push_fields['type'] = 1;
                                    $push_fields['message'] = 'New Booking. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->newtime_from . '-' . $booking->newtime_to;
                                    $push_fields['maid_id'] = $maid_id;
                                    $push_fields['title'] = "New Booking";
                                    $push_fields['customer_name'] = $booking->customer_name;
                                    $push_fields['maid_name'] = $booking->maid_name;
                                    $push_fields['booking_time'] = $booking->newtime_from . ' - ' . $booking->newtime_to;
                                    $push = $this->bookings_model->add_push_notifications($push_fields);
                                    $deviceid = $tablet->google_reg_id;
                                    // optional payload
                                    $payload = array();
                                    $payload['isfeedback'] = false;
                                    if (isset($push) && $push > 0) {
                                        $payload['pushid'] = $push;
                                    } else {
                                        $payload['pushid'] = 0;
                                    }

                                    $title = "New Booking";
                                    $message = $push_fields['message'];
                                    $res = array();
                                    $res['data']['title'] = $title;
                                    $res['data']['is_background'] = false;
                                    $res['data']['message'] = $message;
                                    $res['data']['image'] = "";
                                    $res['data']['payload'] = $payload;
                                    $res['data']['customer'] = $booking->customer_name;
                                    $res['data']['maid'] = $booking->maid_name;
                                    $res['data']['bookingTime'] = $booking->newtime_from . ' - ' . $booking->newtime_to;
                                    $res['data']['timestamp'] = date('Y-m-d G:i:s');
                                    $regId = $deviceid;
                                    $fields = array(
                                        'to' => $regId,
                                        'data' => $res,
                                    );
                                    $return = android_customer_push($fields);
                                    //android_push(array($tablet->google_reg_id), array('action' => 'new-booking', 'booking-ids' => $todays_new_booking, 'message' => 'New Booking'));
                                }

                            }
                        }
                    }

                    $return = array();
                    $return['status'] = 'success';
                    $return['maid_id'] = $maid_id;
                    $return['customer_id'] = $customer_id;
                    $return['time_from'] = $time_from;
                    $return['time_to'] = $time_to;

                    echo json_encode($return);
                    exit();
                }
            }
            // End
            if ($this->input->post('action') && $this->input->post('action') == 'update-booking') {
                if ($this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0 && $this->input->post('time_from') && is_numeric($this->input->post('time_from')) && $this->input->post('time_from') > 0 && $this->input->post('time_to') && is_numeric($this->input->post('time_to')) && $this->input->post('time_to') > 0 && $this->input->post('time_from') < $this->input->post('time_to') && $this->input->post('update_type') && ($this->input->post('update_type') == 'permanent' || $this->input->post('update_type') == 'one-day')) {
                    $update_type = trim($this->input->post('update_type'));
                    $booking_id = trim($this->input->post('booking_id'));
                    $time_from = date('H:i', trim($this->input->post('time_from')));
                    $time_to = date('H:i', trim($this->input->post('time_to')));
                    $is_locked = $this->input->post('is_locked') ? 1 : 0;
                    $pending_amount = $this->input->post('pending_amount');
                    $discount = $this->input->post('discount');
                    $booking_note = $this->input->post('booking_note') ? trim($this->input->post('booking_note')) : '';

                    //if(preg_match("/([01]?[0-9]|2[0-3]):[0][0]/", $time_from) == FALSE || preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_to) == FALSE)
                    if (preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_from) == FALSE || preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_to) == FALSE) {
                        echo 'refresh';
                        exit();
                    }

                    $booking = $this->bookings_model->get_booking_by_id($booking_id);

                    if ($booking->is_locked == 1 && $booking->booked_by != user_authenticate()) {
                        echo 'locked';
                        exit();
                    }
                    if (!isset($booking->booking_id) || ($booking->service_end == 1 && strtotime($booking->service_actual_end_date) < strtotime(date('Y-m-d')))) {
                        echo 'refresh';
                        exit();
                    }

                    if (is_numeric(trim($this->input->post('customer_address_id'))) && trim($this->input->post('customer_address_id')) > 0 && $booking->customer_address_id != trim($this->input->post('customer_address_id'))) {
                        $customer_address_id = trim($this->input->post('customer_address_id'));
                    } else {
                        $customer_address_id = $booking->customer_address_id;
                    }

                    $time_from_stamp = trim($this->input->post('time_from'));
                    $time_to_stamp = trim($this->input->post('time_to'));

                    if ($booking->booking_type == 'OD') {
                        if (isset($booked_slots[$booking->maid_id]['time'])) {
                            foreach ($booked_slots[$booking->maid_id]['time'] as $f_time => $t_time) {
                                if (strtotime($booking->time_from) != $f_time && strtotime($booking->time_to) != $t_time) {
                                    if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
                                        $return = array();
                                        $return['status'] = 'error';
                                        $return['message'] = 'The selected time slot is not available for this maid';

                                        echo json_encode($return);
                                        exit();
                                    }
                                }
                            }
                        }

                        $update_fields = array();
                        $update_fields['customer_address_id'] = $customer_address_id;
                        $update_fields['time_from'] = $time_from;
                        $update_fields['time_to'] = $time_to;
                        $update_fields['pending_amount'] = $pending_amount;
                        $update_fields['discount'] = $discount;
                        $update_fields['is_locked'] = $is_locked;
                        $update_fields['booking_note'] = $booking_note;

                        $this->bookings_model->update_booking($booking_id, $update_fields);

                        if ($booking->service_start_date == date('Y-m-d')) {
                            $c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
                            if (isset($c_address->zone_id)) {
                                $tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
                                if ($tablet) {
                                    $upd_booking = $this->bookings_model->get_booking_by_id($booking_id);
                                    $push_fields = array();
                                    $push_fields['tab_id'] = $tablet->tablet_id;
                                    $push_fields['type'] = 5;
                                    $push_fields['message'] = 'Booking Updated. Maid : ' . $upd_booking->maid_name . ', Customer : ' . $upd_booking->customer_name . ', Shift : ' . $upd_booking->newtime_from . '-' . $upd_booking->newtime_to;
                                    $push_fields['maid_id'] = $upd_booking->maid_id;
                                    $push_fields['title'] = "Booking Updated";
                                    $push_fields['customer_name'] = $upd_booking->customer_name;
                                    $push_fields['maid_name'] = $upd_booking->maid_name;
                                    $push_fields['booking_time'] = $upd_booking->newtime_from . ' - ' . $upd_booking->newtime_to;
                                    if (strtotime($schedule_date) <= strtotime(date('d-M-Y')))
                                        $push = $this->bookings_model->add_push_notifications($push_fields);

                                    if (strtotime($schedule_date) <= strtotime(date('d-M-Y'))) {
                                        $deviceid = $tablet->google_reg_id;
                                        // optional payload
                                        $payload = array();
                                        $payload['isfeedback'] = false;
                                        if (isset($push) && $push > 0) {
                                            $payload['pushid'] = $push;
                                        } else {
                                            $payload['pushid'] = 0;
                                        }

                                        $title = "Booking Updated";
                                        $message = $push_fields['message'];
                                        $res = array();
                                        $res['data']['title'] = $title;
                                        $res['data']['is_background'] = false;
                                        $res['data']['message'] = $message;
                                        $res['data']['image'] = "";
                                        $res['data']['payload'] = $payload;
                                        $res['data']['customer'] = $upd_booking->customer_name;
                                        $res['data']['maid'] = $upd_booking->maid_name;
                                        $res['data']['bookingTime'] = $upd_booking->newtime_from . ' - ' . $upd_booking->newtime_to;
                                        $res['data']['timestamp'] = date('Y-m-d G:i:s');
                                        $regId = $deviceid;
                                        $fields = array(
                                            'to' => $regId,
                                            'data' => $res,
                                        );
                                        $return = android_customer_push($fields);
                                        //android_push(array($tablet->google_reg_id), array('action' => 'update-booking', 'booking-ids' => array($booking_id), 'message' => $push_fields['message']));
                                    }
                                }
                            }
                        }

                        $return = array();
                        $return['status'] = 'success';
                        $return['maid_id'] = $booking->maid_id;
                        $return['customer_id'] = $booking->customer_id;
                        $return['time_from'] = $time_from;
                        $return['time_to'] = $time_to;

                        echo json_encode($return);
                        exit();
                    } else {
                        if ($this->input->post('repeat_end') && (trim($this->input->post('repeat_end')) == 'never' || trim($this->input->post('repeat_end')) == 'ondate')) {
                            $repeat_end = $this->input->post('repeat_end');

                            if ($repeat_end == 'ondate') {
                                if ($this->input->post('repeat_end_date')) {
                                    $repeat_end_date = $this->input->post('repeat_end_date');

                                    $repeat_end_date_split = explode('/', $repeat_end_date);
                                    if (count($repeat_end_date_split) == 3 && checkdate($repeat_end_date_split[1], $repeat_end_date_split[0], $repeat_end_date_split[2])) {
                                        $s_date = new DateTime($service_date);
                                        $e_date = new DateTime($repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0]);
                                        //$diff = $s_date->diff($e_date);

                                        $diff = abs(strtotime($repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0]) - strtotime($service_date));

                                        $years = floor($diff / (365 * 60 * 60 * 24));
                                        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
                                        $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
                                        if ($days < 0) //if($diff->days < 0)
                                        {
                                            echo 'refresh';
                                            exit();
                                        }

                                        $service_end_date = $repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0];
                                    } else {
                                        echo 'refresh';
                                        exit();
                                    }
                                } else {
                                    echo 'refresh';
                                    exit();
                                }
                            } else {
                                $service_end_date = $service_date;
                            }

                            $today_week_day = date('w', strtotime($service_date));
                            $service_start_date = $service_date;

                            if ($booking->booking_type == 'WE') {
                                $bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($booking->maid_id, $today_week_day);
                                foreach ($bookings_on_day as $booking_on_day) {
                                    if ($booking_on_day->booking_id != $booking->booking_id) {
                                        $s_date_stamp = strtotime($service_start_date);
                                        $e_date_stamp = strtotime($service_end_date);
                                        $bs_date_stamp = strtotime($booking_on_day->service_start_date);
                                        $be_date_stamp = strtotime($booking_on_day->service_actual_end_date);

                                        //if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
                                        if (($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp)))) {
                                            $f_time = strtotime($booking_on_day->time_from);
                                            $t_time = strtotime($booking_on_day->time_to);

                                            if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
                                                $return = array();
                                                $return['status'] = 'error';
                                                $return['message'] = 'The selected time slot is not available for this maid on ' . $week_day_names[$today_week_day] . 's';

                                                echo json_encode($return);
                                                exit();
                                            }
                                        }
                                    }
                                }

                                if ($update_type == 'permanent') {
                                    $service_end_date = $repeat_end == 'never' ? $service_start_date : $service_end_date;

                                    // End current booking and add new booking if address id or time changes
                                    if ($booking->customer_address_id != $customer_address_id || $booking->time_from != $time_from || $booking->time_to != $time_to) {
                                        // End current booking
                                        $this->bookings_model->update_booking($booking_id, array('service_actual_end_date' => date('Y-m-d', strtotime($service_start_date . ' - 1 day')), 'service_end' => 1));

                                        $booking_fields = array();
                                        $booking_fields['customer_id'] = $booking->customer_id;
                                        $booking_fields['customer_address_id'] = $customer_address_id;
                                        $booking_fields['maid_id'] = $booking->maid_id;
                                        $booking_fields['service_type_id'] = $booking->service_type_id;
                                        $booking_fields['service_start_date'] = $service_start_date;
                                        $booking_fields['service_week_day'] = $today_week_day;
                                        $booking_fields['time_from'] = $time_from;
                                        $booking_fields['time_to'] = $time_to;
                                        $booking_fields['booking_type'] = $booking->booking_type;
                                        $booking_fields['service_end'] = $repeat_end == 'ondate' ? 1 : 0;
                                        $booking_fields['service_end_date'] = $service_end_date;
                                        $booking_fields['booking_note'] = $booking_note;
                                        $booking_fields['is_locked'] = $is_locked;
                                        $booking_fields['pending_amount'] = $pending_amount;
                                        $booking_fields['booked_by'] = user_authenticate();
                                        $booking_fields['booking_status'] = 1;
                                        $booking_fields['booking_category'] = 'C';
                                        $booking_fields['booked_datetime'] = date('Y-m-d H:i:s');

                                        $booking_id = $this->bookings_model->add_booking($booking_fields);

                                        if ($booking_id) {
                                            if ($service_start_date == date('Y-m-d')) {
                                                $c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
                                                if (isset($c_address->zone_id)) {
                                                    $tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
                                                    if ($tablet) {
                                                        $upd_booking = $this->bookings_model->get_booking_by_id($booking_id);
                                                        $push_fields = array();
                                                        $push_fields['tab_id'] = $tablet->tablet_id;
                                                        $push_fields['type'] = 5;
                                                        $push_fields['message'] = 'Booking Updated. Maid : ' . $upd_booking->maid_name . ', Customer : ' . $upd_booking->customer_name . ', Shift : ' . $upd_booking->newtime_from . '-' . $upd_booking->newtime_to;
                                                        $push_fields['maid_id'] = $upd_booking->maid_id;
                                                        $push_fields['title'] = "Booking Updated";
                                                        $push_fields['customer_name'] = $upd_booking->customer_name;
                                                        $push_fields['maid_name'] = $upd_booking->maid_name;
                                                        $push_fields['booking_time'] = $upd_booking->newtime_from . ' - ' . $upd_booking->newtime_to;
                                                        if (strtotime($schedule_date) <= strtotime(date('d-M-Y')))
                                                            $push = $this->bookings_model->add_push_notifications($push_fields);
                                                        if (strtotime($schedule_date) <= strtotime(date('d-M-Y'))) {
                                                            $deviceid = $tablet->google_reg_id;
                                                            // optional payload
                                                            $payload = array();
                                                            $payload['isfeedback'] = false;
                                                            if (isset($push) && $push > 0) {
                                                                $payload['pushid'] = $push;
                                                            } else {
                                                                $payload['pushid'] = 0;
                                                            }

                                                            $title = "Booking Updated";
                                                            $message = $push_fields['message'];
                                                            $res = array();
                                                            $res['data']['title'] = $title;
                                                            $res['data']['is_background'] = false;
                                                            $res['data']['message'] = $message;
                                                            $res['data']['image'] = "";
                                                            $res['data']['payload'] = $payload;
                                                            $res['data']['customer'] = $upd_booking->customer_name;
                                                            $res['data']['maid'] = $upd_booking->maid_name;
                                                            $res['data']['bookingTime'] = $upd_booking->newtime_from . ' - ' . $upd_booking->newtime_to;
                                                            $res['data']['timestamp'] = date('Y-m-d G:i:s');
                                                            $regId = $deviceid;
                                                            $fields = array(
                                                                'to' => $regId,
                                                                'data' => $res,
                                                            );
                                                            $return = android_customer_push($fields);
                                                            //android_push(array($tablet->google_reg_id), array('action' => 'new-booking', 'booking-ids' => array($booking_id), 'message' => $push_fields['message']));
                                                        }
                                                        $booking = $this->bookings_model->get_booking_by_id($booking_id);


                                                    }
                                                }
                                            }

                                            $return = array();
                                            $return['status'] = 'success';
                                            $return['maid_id'] = $booking->maid_id;
                                            $return['customer_id'] = $booking->customer_id;
                                            $return['time_from'] = $time_from;
                                            $return['time_to'] = $time_to;

                                            echo json_encode($return);
                                            exit();
                                        }
                                    } else {
                                        $update_fields = array();
                                        $update_fields['service_end'] = $repeat_end == 'ondate' ? 1 : 0;
                                        $update_fields['service_end_date'] = $service_end_date;
                                        $update_fields['service_actual_end_date'] = $service_end_date;
                                        $update_fields['pending_amount'] = $pending_amount;
                                        $update_fields['is_locked'] = $is_locked;
                                        $update_fields['booking_note'] = $booking_note;

                                        $this->bookings_model->update_booking($booking_id, $update_fields);

                                        if ($booking->service_start_date == date('Y-m-d')) {
                                            $c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
                                            if (isset($c_address->zone_id)) {
                                                $tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
                                                if ($tablet) {
                                                    $upd_booking = $this->bookings_model->get_booking_by_id($booking_id);
                                                    $push_fields = array();
                                                    $push_fields['tab_id'] = $tablet->tablet_id;
                                                    $push_fields['type'] = 5;
                                                    $push_fields['message'] = 'Booking Updated. Maid : ' . $upd_booking->maid_name . ', Customer : ' . $upd_booking->customer_name . ', Shift : ' . $upd_booking->newtime_from . '-' . $upd_booking->newtime_to;
                                                    $push_fields['maid_id'] = $upd_booking->maid_id;
                                                    $push_fields['title'] = "Booking Updated";
                                                    $push_fields['customer_name'] = $upd_booking->customer_name;
                                                    $push_fields['maid_name'] = $upd_booking->maid_name;
                                                    $push_fields['booking_time'] = $upd_booking->newtime_from . ' - ' . $upd_booking->newtime_to;
                                                    if (strtotime($schedule_date) <= strtotime(date('d-M-Y')))
                                                        $push = $this->bookings_model->add_push_notifications($push_fields);

                                                    if (strtotime($schedule_date) <= strtotime(date('d-M-Y'))) {
                                                        $deviceid = $tablet->google_reg_id;
                                                        // optional payload
                                                        $payload = array();
                                                        $payload['isfeedback'] = false;
                                                        if (isset($push) && $push > 0) {
                                                            $payload['pushid'] = $push;
                                                        } else {
                                                            $payload['pushid'] = 0;
                                                        }

                                                        $title = "Booking Updated";
                                                        $message = $push_fields['message'];
                                                        $res = array();
                                                        $res['data']['title'] = $title;
                                                        $res['data']['is_background'] = false;
                                                        $res['data']['message'] = $message;
                                                        $res['data']['image'] = "";
                                                        $res['data']['payload'] = $payload;
                                                        $res['data']['customer'] = $upd_booking->customer_name;
                                                        $res['data']['maid'] = $upd_booking->maid_name;
                                                        $res['data']['bookingTime'] = $upd_booking->newtime_from . ' - ' . $upd_booking->newtime_to;
                                                        $res['data']['timestamp'] = date('Y-m-d G:i:s');
                                                        $regId = $deviceid;
                                                        $fields = array(
                                                            'to' => $regId,
                                                            'data' => $res,
                                                        );
                                                        $return = android_customer_push($fields);
                                                        //android_push(array($tablet->google_reg_id), array('action' => 'update-booking', 'booking-ids' => array($booking_id), 'message' => $push_fields['message']));
                                                    }
                                                    $booking = $this->bookings_model->get_booking_by_id($booking_id);


                                                }
                                            }
                                        }

                                        $return = array();
                                        $return['status'] = 'success';
                                        $return['maid_id'] = $booking->maid_id;
                                        $return['customer_id'] = $booking->customer_id;
                                        $return['time_from'] = $time_from;
                                        $return['time_to'] = $time_to;

                                        echo json_encode($return);
                                        exit();
                                    }
                                } else if ($update_type == 'one-day') {
                                    /* Delete booking one day */
                                    $delete_b_fields = array();
                                    $delete_b_fields['booking_id'] = $booking->booking_id;
                                    $delete_b_fields['service_date'] = $service_date;
                                    $delete_b_fields['deleted_by'] = user_authenticate();

                                    $this->bookings_model->add_booking_delete($delete_b_fields);

                                    /* Add one day booking */
                                    $booking_fields = array();
                                    $booking_fields['customer_id'] = $booking->customer_id;
                                    $booking_fields['customer_address_id'] = $customer_address_id;
                                    $booking_fields['maid_id'] = $booking->maid_id;
                                    $booking_fields['service_type_id'] = $booking->service_type_id;
                                    $booking_fields['service_start_date'] = $service_date;
                                    $booking_fields['service_week_day'] = $today_week_day;
                                    $booking_fields['time_from'] = $time_from;
                                    $booking_fields['time_to'] = $time_to;
                                    $booking_fields['booking_type'] = 'OD';
                                    $booking_fields['service_end'] = 1;
                                    $booking_fields['service_end_date'] = $service_date;
                                    $booking_fields['booking_note'] = $booking_note;
                                    $booking_fields['is_locked'] = $is_locked;
                                    $booking_fields['pending_amount'] = $pending_amount;
                                    $booking_fields['booked_by'] = user_authenticate();
                                    $booking_fields['booking_status'] = 1;
                                    $booking_fields['booking_category'] = 'C';
                                    $booking_fields['booked_datetime'] = date('Y-m-d H:i:s');

                                    $booking_id = $this->bookings_model->add_booking($booking_fields);

                                    if ($booking_id) {
                                        if ($service_date == date('Y-m-d')) {
                                            $c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
                                            if (isset($c_address->zone_id)) {
                                                $tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
                                                if ($tablet) {
                                                    $push_fields = array();
                                                    $push_fields['tab_id'] = $tablet->tablet_id;
                                                    $push_fields['type'] = 3;
                                                    $push_fields['message'] = 'Booking Updated. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . date("h:i A", strtotime($time_from)) . '-' . date("h:i A", strtotime($time_to));
                                                    $push_fields['maid_id'] = $booking->maid_id;
                                                    $push_fields['title'] = "Booking Updated";
                                                    $push_fields['customer_name'] = $booking->customer_name;
                                                    $push_fields['maid_name'] = $booking->maid_name;
                                                    $push_fields['booking_time'] = date("h:i A", strtotime($time_from)) . ' - ' . date("h:i A", strtotime($time_to));
                                                    if (strtotime($schedule_date) <= strtotime(date('d-M-Y')))
                                                        $push = $this->bookings_model->add_push_notifications($push_fields);

                                                    if (strtotime($schedule_date) <= strtotime(date('d-M-Y'))) {
                                                        $deviceid = $tablet->google_reg_id;
                                                        // optional payload
                                                        $payload = array();
                                                        $payload['isfeedback'] = false;
                                                        if (isset($push) && $push > 0) {
                                                            $payload['pushid'] = $push;
                                                        } else {
                                                            $payload['pushid'] = 0;
                                                        }

                                                        $title = "Booking Updated";
                                                        $message = $push_fields['message'];
                                                        $res = array();
                                                        $res['data']['title'] = $title;
                                                        $res['data']['is_background'] = false;
                                                        $res['data']['message'] = $message;
                                                        $res['data']['image'] = "";
                                                        $res['data']['payload'] = $payload;
                                                        $res['data']['customer'] = $booking->customer_name;
                                                        $res['data']['maid'] = $booking->maid_name;
                                                        $res['data']['bookingTime'] = date("h:i A", strtotime($time_from)) . ' - ' . date("h:i A", strtotime($time_to));
                                                        $res['data']['timestamp'] = date('Y-m-d G:i:s');
                                                        $regId = $deviceid;
                                                        $fields = array(
                                                            'to' => $regId,
                                                            'data' => $res,
                                                        );
                                                        $return = android_customer_push($fields);
                                                        //android_push(array($tablet->google_reg_id), array('action' => 'new-booking', 'booking-ids' => array($booking_id), 'message' => $push_fields['message']));
                                                    }
                                                    $booking = $this->bookings_model->get_booking_by_id($booking_id);


                                                }
                                            }
                                        }

                                        $return = array();
                                        $return['status'] = 'success';
                                        $return['maid_id'] = $booking->maid_id;
                                        $return['customer_id'] = $booking->customer_id;
                                        $return['time_from'] = $time_from;
                                        $return['time_to'] = $time_to;

                                        echo json_encode($return);
                                        exit();
                                    }
                                }
                            }
                        } else {
                            echo 'refresh';
                            exit();
                        }
                    }

                    echo 'refresh';
                    exit();
                }
            }

            if ($this->input->post('action') && $this->input->post('action') == 'refresh-grid') {
                $data = array();
                $data['maids'] = $maids;
                $data['maid_bookings'] = $maid_bookings;
                $data['leave_maid_ids'] = $leave_maid_ids; // Maid Leave
                $data['all_bookings'] = $all_bookings;
                $data['times'] = $times;
                $data['booking_allowed'] = 1; // Enable past bookings by Geethu
                //$data['booking_allowed'] = strtotime($schedule_date) >= strtotime( date('d-M-Y')) ? 1 : 0;
                $schedule_day = date('d F Y, l', strtotime($schedule_date));
                $data['schedule_day'] = $schedule_day;
                $data['current_hour_index'] = $current_hour_index;
                $schedule_grid = $this->load->view('partials/schedule_grid', $data, TRUE);
                $schedule_report = $this->load->view('partials/schedule_report', $data, TRUE);

                echo json_encode(array('grid' => $schedule_grid, 'report' => $schedule_report));
                exit();
            }

            if ($this->input->post('action') && $this->input->post('action') == 'get-customer-address' && $this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0) {
                $customer_id = trim($this->input->post('customer_id'));

                $customer_addresses = $this->customers_model->get_customer_addresses($customer_id);


                echo json_encode($customer_addresses);
                exit();
            }
            if ($this->input->post('action') && $this->input->post('action') == 'get-no-of-customer-address' && $this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0) {
                $customer_id = trim($this->input->post('customer_id'));

                $customer_addresses = $this->customers_model->get_customer_addresses($customer_id);


                echo json_encode(array("address" => $customer_addresses, "no_of_address" => count($customer_addresses)));
                exit();
            }


            // End 
            if ($this->input->post('action') && $this->input->post('action') == 'delete-booking-permanent' && $this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0) {
                $booking_id = trim($this->input->post('booking_id'));
                $remarks = $this->input->post('remarks');
                $d_booking = $this->bookings_model->get_booking_by_id_fordelete($booking_id);

                if ($d_booking->is_locked == 1 && $d_booking->booked_by != user_authenticate()) {
                    echo 'locked';
                    exit();
                }
                if (isset($d_booking->booking_id)) {
                    if ($d_booking->booking_type == 'OD') {
                        $this->bookings_model->update_booking($booking_id, array('booking_status' => 2), 'Delete');

                    } else {
                        $schedule_date = date('d-M-Y');
                        $newsearchdate = date('Y-m-d', strtotime($schedule_date));
                        $enddate = $d_booking->service_start_date;
                        if ($d_booking->service_start_date == $newsearchdate && $d_booking->service_end_date == $newsearchdate && $d_booking->service_actual_end_date == $newsearchdate) {
                            $this->bookings_model->update_booking($booking_id, array('booking_status' => 2), 'Delete');
                        } else {
                            if ($d_booking->service_start_date > $newsearchdate && $d_booking->service_end_date > $newsearchdate && $d_booking->service_actual_end_date > $newsearchdate) {
                                $this->bookings_model->update_booking($booking_id, array('booking_status' => 2), 'Delete');
                            }
                            if ($d_booking->booking_type == 'WE') {
                                $current = strtotime($newsearchdate);
                                $last = strtotime($enddate);
                                while ($current >= $last) {
                                    $current = strtotime('-7 days', $current);
                                    $deletedatee = date('Y-m-d', $current);
                                    $check_buk_delete = $this->bookings_model->check_booking_deletes_by_id_and_date($d_booking->booking_id, $deletedatee);
                                    if (count($check_buk_delete) > 0) {

                                    } else {
                                        $this->bookings_model->update_booking($booking_id, array('service_actual_end_date' => $deletedatee, 'service_end_date' => $deletedatee, 'service_end' => 1), 'Delete');
                                        break;
                                    }
                                }
                            } else if ($d_booking->booking_type == 'BW') {
                                $current = strtotime($newsearchdate);
                                $last = strtotime($enddate);
                                while ($current >= $last) {
                                    $current = strtotime('-14 days', $current);
                                    $deletedatee = date('Y-m-d', $current);
                                    $check_buk_delete = $this->bookings_model->check_booking_deletes_by_id_and_date($d_booking->booking_id, $deletedatee);
                                    if (count($check_buk_delete) > 0) {

                                    } else {
                                        $this->bookings_model->update_booking($booking_id, array('service_actual_end_date' => $deletedatee, 'service_end_date' => $deletedatee, 'service_end' => 1), 'Delete');
                                        break;
                                    }
                                }
                            }
                        }
                        // $schedule_date = date('d-M-Y');
                        // $this->bookings_model->update_booking($booking_id, array('service_actual_end_date' => date('Y-m-d', strtotime($schedule_date . ' - 1 day')), 'service_end' => 1), 'Delete');
                    }

                    $delete_b_fields = array();
                    $delete_b_fields['booking_id'] = $booking_id;
                    $delete_b_fields['service_date'] = $service_date;
                    $delete_b_fields['deleted_by'] = user_authenticate();

                    $this->bookings_model->add_booking_cancel($delete_b_fields);
                    $delete_b_fields_new = array();
                    $delete_b_fields_new['booking_id'] = $booking_id;
                    $delete_b_fields_new['service_date'] = $service_date;
                    $delete_b_fields_new['remarks'] = $remarks;
                    $delete_b_fields_new['deleted_by'] = user_authenticate();
                    $delete_b_fields_new['deleted_date_time'] = date('Y-m-d H:i:s');


                    $this->bookings_model->add_booking_remarks($delete_b_fields_new);

                    //if(($d_booking->booking_type == 'WE' && $d_booking->service_start_date == date('Y-m-d')) || ($d_booking->service_week_day == date('w') && strtotime(date('Y-m-d')) >= strtotime($d_booking->service_start_date) && strtotime(date('Y-m-d')) < strtotime($schedule_date)))
                    //{
                    $c_address = $this->customers_model->get_customer_address_by_id($d_booking->customer_address_id);
                    if (isset($c_address->zone_id)) {
                        $tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
                        if ($tablet) {
                            $schedule_date = date('d-M-Y');
                            $push_fields = array();
                            $push_fields['tab_id'] = $tablet->tablet_id;
                            $push_fields['type'] = 2;
                            $push_fields['message'] = 'Booking Cancelled. Maid : ' . $d_booking->maid_name . ', Customer : ' . $d_booking->customer_name . ', Shift : ' . $d_booking->newtime_from . '-' . $d_booking->newtime_to;
                            $push_fields['maid_id'] = $d_booking->maid_id;
                            $push_fields['title'] = "Booking Cancelled";
                            $push_fields['customer_name'] = $d_booking->customer_name;
                            $push_fields['maid_name'] = $d_booking->maid_name;
                            $push_fields['booking_time'] = $d_booking->newtime_from . ' - ' . $d_booking->newtime_to;
                            if (strtotime($schedule_date) <= strtotime(date('d-M-Y')))
                                $push = $this->bookings_model->add_push_notifications($push_fields);

                            if (strtotime($schedule_date) <= strtotime(date('d-M-Y'))) {
                                $deviceid = $tablet->google_reg_id;
                                // optional payload
                                $payload = array();
                                $payload['isfeedback'] = false;
                                if (isset($push) && $push > 0) {
                                    $payload['pushid'] = $push;
                                } else {
                                    $payload['pushid'] = 0;
                                }


                                $title = "Booking Cancelled";
                                $message = $push_fields['message'];
                                $res = array();
                                $res['data']['title'] = $title;
                                $res['data']['is_background'] = false;
                                $res['data']['message'] = $message;
                                $res['data']['image'] = "";
                                $res['data']['payload'] = $payload;
                                $res['data']['customer'] = $d_booking->customer_name;
                                $res['data']['maid'] = $d_booking->maid_name;
                                $res['data']['bookingTime'] = $d_booking->newtime_from . ' - ' . $d_booking->newtime_to;
                                $res['data']['timestamp'] = date('Y-m-d G:i:s');
                                $regId = $deviceid;
                                $fields = array(
                                    'to' => $regId,
                                    'data' => $res,
                                );
                                $return = android_customer_push($fields);
                                //$return = android_push(array($tablet->google_reg_id), array('action' => 'delete-booking', 'booking-ids' => array($booking_id), 'message' => $push_fields['message']));
                            }
                        }
                        //print_r($return);exit;
                    }
                    //}

                    echo 'success';
                    exit();
                }

                echo 'refresh';
                exit();
            }

            if ($this->input->post('action') && $this->input->post('action') == 'delete-booking-one-day' && $this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0) {
                $booking_id = trim($this->input->post('booking_id'));

                $d_booking = $this->bookings_model->get_booking_by_id_fordelete($booking_id);

                if ($d_booking->is_locked == 1 && $d_booking->booked_by != user_authenticate()) {
                    echo 'locked';
                    exit();
                }
                if (isset($d_booking->booking_id)) {
                    if ($d_booking->booking_type != 'OD') {
                        $delete_b_fields = array();
                        $delete_b_fields['booking_id'] = $booking_id;
                        $delete_b_fields['service_date'] = ''; //$service_date;
                        $delete_b_fields['deleted_by'] = user_authenticate();

                        $this->bookings_model->add_booking_delete($delete_b_fields);
                    }
                }

                //if($service_date == date('Y-m-d'))
                //{
                $c_address = $this->customers_model->get_customer_address_by_id($d_booking->customer_address_id);
                if (isset($c_address->zone_id)) {
                    $tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
                    if ($tablet) {
                        $push_fields = array();
                        $push_fields['tab_id'] = $tablet->tablet_id;
                        $push_fields['type'] = 2;
                        $push_fields['message'] = 'Booking Cancelled. Maid : ' . $d_booking->maid_name . ', Customer : ' . $d_booking->customer_name . ', Shift : ' . $d_booking->newtime_from . '-' . $d_booking->newtime_to;
                        $push_fields['maid_id'] = $d_booking->maid_id;
                        $push_fields['title'] = "Booking Cancelled";
                        $push_fields['customer_name'] = $d_booking->customer_name;
                        $push_fields['maid_name'] = $d_booking->maid_name;
                        $push_fields['booking_time'] = $d_booking->newtime_from . ' - ' . $d_booking->newtime_to;
                        if (strtotime($schedule_date) <= strtotime(date('d-M-Y')))
                            $push = $this->bookings_model->add_push_notifications($push_fields);

                        if (strtotime($schedule_date) <= strtotime(date('d-M-Y'))) {
                            $deviceid = $tablet->google_reg_id;
                            // optional payload
                            $payload = array();
                            $payload['isfeedback'] = false;
                            if (isset($push) && $push > 0) {
                                $payload['pushid'] = $push;
                            } else {
                                $payload['pushid'] = 0;
                            }

                            $title = "Booking Cancelled";
                            $message = $push_fields['message'];
                            $res = array();
                            $res['data']['title'] = $title;
                            $res['data']['is_background'] = false;
                            $res['data']['message'] = $message;
                            $res['data']['image'] = "";
                            $res['data']['payload'] = $payload;
                            $res['data']['customer'] = $d_booking->customer_name;
                            $res['data']['maid'] = $d_booking->maid_name;
                            $res['data']['bookingTime'] = $d_booking->newtime_from . ' - ' . $d_booking->newtime_to;
                            $res['data']['timestamp'] = date('Y-m-d G:i:s');
                            $regId = $deviceid;
                            $fields = array(
                                'to' => $regId,
                                'data' => $res,
                            );
                            $return = android_customer_push($fields);
                            //android_push(array($tablet->google_reg_id), array('action' => 'delete-booking', 'booking-ids' => array($booking_id), 'message' => $push_fields['message']));
                        }
                    }
                }
                //}

                echo "success";
                exit();

            }

            echo 'refresh';
            exit();
        }
        //        Jiby ajax end




        $data['search_date_from'] = date('d/m/Y', strtotime($date_from));
        $data['search_date_to'] = date('d/m/Y', strtotime($date_to));
        //////
        //$data['bookings'] = $this->bookings_model->get_schedule_by_customer($customer_id, $date_from, $date_to);
        $f_date = explode("/", $data['search_date_from']);
        $t_date = explode("/", $data['search_date_to']);
        $date_from = $f_date[2] . '-' . $f_date[1] . '-' . $f_date[0];
        $date_to = $t_date[2] . '-' . $t_date[1] . '-' . $t_date[0];

        //
        $all_bookings = array();
        $maid_bookings = array();
        $maid_schedule = array();
        $ndate = $date_from;
        $m = 0;
        while (strtotime($date_to) >= strtotime($ndate)) {
            $maid_schedule[$m] = new stdClass();
            $maid_schedule[$m]->date = $ndate;

            ++$m;
            $ndate = date("Y-m-d", strtotime("+1 day", strtotime($ndate)));
        }
        $res_arr_values = array();
        foreach ($maid_schedule as $rowschedule) {
            $schedle_date = $rowschedule->date;
            //edited by vishnu
            //$booking_by_date = $this->bookings_model->get_schedule_by_date_and_customer($customer_id,$schedle_date);
            $booking_by_date = $this->bookings_model->get_schedule_by_date_and_customer_new($customer_id, $schedle_date);
            //ends
            if (!empty($booking_by_date)) {
                $res_arr_values[] = $booking_by_date;
            }

        }
        $data['bookings'] = $res_arr_values;



        /////////////
        $maid_id_loop = 0;
        $i = 0;
        $customer_active_maids = array();
        foreach ($current_service as $service) {
            //edited by vishnu
            $todates = date("Y-m-d");
            $btype = $service->booking_type;
            if ($service->booking_type == "WE") {
                $check_booking = $this->bookings_model->checkbooking_in_delete_weekly($service->booking_id, $todates);
                if ($check_booking == 0) {
                    $pausetext = '<span type="button" class="btn btn-block bg-'.strtolower($service->booking_type).'" rel="' . $service->booking_type . '" onclick=deletebookingservice(' . $service->booking_id . ',"' . $btype . '")>' . $service->shifts . ' ~ '.$service->booking_type. '</span>';
                } else {
                    $pausetext = '<span type="button" class="btn btn-block bg-'.strtolower($service->booking_type).'" rel="' . $service->booking_type . '" onclick=deletebookingservice(' . $service->booking_id . ',"' . $btype . '")>' . $service->shifts . ' ~ '.$service->booking_type. ' ( Paused )</span>';
                }
            }
            else if ($service->booking_type == "BW") {
                $check_booking = $this->bookings_model->checkbooking_in_delete_biweekly($service->booking_id, $todates);
                if ($check_booking == 0) {
                    $pausetext = '<span type="button" class="btn btn-block bg-' . strtolower($service->booking_type) . '" rel="' . $service->booking_type . '" onclick=deletebookingservice(' . $service->booking_id . ',"' . $btype . '")>' . $service->shifts . ' ~ '.$service->booking_type. '</span>';
                } else {
                    $pausetext = '<span type="button" class="btn btn-block bg-' . strtolower($service->booking_type) . '" rel="' . $service->booking_type . '" onclick=deletebookingservice(' . $service->booking_id . ',"' . $btype . '")>' . $service->shifts . ' ~ '.$service->booking_type. ' ( Paused )</span>';
                }
            }else if ($service->booking_type == "OD") {
                $pausetext = '<span type="button" class="btn btn-block bg-'.strtolower($service->booking_type).'" rel="' . $service->booking_type . '" onclick=deletebookingservice(' . $service->booking_id . ',"' . $btype . '")>' . $service->shifts . ' ~ '.$service->booking_type. '</span>';
            }
            //ends
            if ($maid_id_loop != $service->maid_id) {
                $i++;
                $customer_active_maids[$i] = new stdClass();
                $customer_active_maids[$i]->booking_type = $service->booking_type;
                $customer_active_maids[$i]->maid_name = $service->maid_name;
                $customer_active_maids[$i]->maid_photo_file = $service->maid_photo_file;
                $customer_active_maids[$i]->maid_nationality = $service->maid_nationality;
                $btype = $service->booking_type;
                //$customer_active_maids[$i]->shifts = '<span style="border-radius : 4px; padding : 5px;">' . $service->shifts . '</span>';
                //edited by vishnu
                //$customer_active_maids[$i]->shifts = '<span rel="'.$service->booking_type.'" onclick=deletebookingservice('.$service->booking_id.',"'.$btype.'") style="border-radius : 4px; padding : 5px;">' . $service->shifts . '</span>';
                $customer_active_maids[$i]->shifts = $pausetext;
                //ends

            } else {
                //$customer_active_maids[$i]->shifts .= '<br /><br /><span style="border-radius : 4px; padding : 5px;">' . $service->shifts . '</span>';
                $customer_active_maids[$i]->shifts .= '<span type="button" class="btn btn-block bg-'.strtolower($service->booking_type).'" rel="' . $service->booking_type . '" onclick=deletebookingservice(' . $service->booking_id . ',"' . $btype . '")>' . $service->shifts . ' ~ '.$service->booking_type.'</span>';
            }
            $maid_id_loop = $service->maid_id;
        }

        $data['customer_id'] = $customer_id;
        $data['customer_status'] = $data['customer_details'][0]['customer_status'];
        $data['current_service'] = $customer_active_maids;
        $data['times'] = $times;

        $data['service_types'] = $service_types = $this->service_types_model->get_service_types();
        $data['customers'] = $customers = $this->customers_model->get_customers();
        $today = date("Y-m-d");
        $data['day_number'] = date('w', strtotime($today));
        //$data['repeate_end_start_c'] = date('Y, m, d', strtotime($schedule_date . ' + 7 day'));
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('view_customer', $data, TRUE);
        $layout_data['page_title'] = 'Customer - '.$data['customer_details'][0]['customer_name'];
        $layout_data['meta_description'] = 'customers';
        $layout_data['css_files'] = array('datepicker.css','jquery.fancybox.css');
        $layout_data['external_js_files'] = array();
        $layout_data['customer_active'] = '1';
        $layout_data['js_files'] = array('jquery.fancybox.pack.js', 'base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);


    }


    public function maids_booking()
    {
        /*************************************************************************************************************** */
        showPrepareTimeError($this->input->post('book_date'), $this->input->post('from_time'),$this->input->post('book_type'),[],'html');
        /*************************************************************************************************************** */
        //            $this->load->model('bookings_model');
        $schedule_date = $this->input->post('book_date');
        $from_time = $this->input->post('from_time');
        $to_time = $this->input->post('to_time');
        $book_type = $this->input->post('book_type');
        $customer_id = $this->input->post('customer_id');
        //$area_id        = $this->input->post('area_id') ;
        $area_id = $this->input->post('jm_area');
        $zone_id = $this->input->post('zone_id');
        $province_id = $this->input->post('province_id');
        $location_type = $this->input->post('location_type');
        $location_val = $this->input->post('location_value');
        $address_id = $this->input->post('cust_address_id');
        $repeat_end = $this->input->post('repeat_end');
        $repeat_end_date = $this->input->post('repeat_end_date');
        $total_amt = $this->input->post('total_amt');
        $clean_mat = $this->input->post('cleaning_mat');
        $b_notes = $this->input->post('b_notes');
        $datassss = array(
            'area_id' => $area_id
        );
        $update_area = $this->customers_model->update_customer_address($datassss, $address_id);

        $filter = array();
        $filter['customer_id'] = $customer_id;
        $filter['location_type'] = $location_type;
        $filter['location_val'] = $location_val;
        $filter['area_id'] = $area_id;
        $filter['zone_id'] = $zone_id;
        $filter['province_id'] = $province_id;
        $service_date = date('Y-m-d', strtotime(str_replace('/', '-', $schedule_date)));
        $repeat_day = date('w', strtotime(str_replace('/', '-', $schedule_date)));
        $filter['service_date'] = $service_date;
        $filter['from_time'] = $from_time;
        $filter['to_time'] = $to_time;


        $data['service_date'] = $service_date;
        $data['from_time'] = $from_time;
        $data['to_time'] = $to_time;
        $data['customer_id'] = $customer_id;
        $data['location_type'] = $location_type;
        $data['area_id'] = $area_id;
        $data['zone_id'] = $zone_id;
        $data['province_id'] = $province_id;
        $data['new_book_type'] = $book_type;
        //$data['maids']          = $this->maids_model->get_all_maid($filter);
        $search_maids = $this->maids_model->get_all_maid($filter);
        //print_r($search_maids);
        //exit();

        $maids_leave_on_date = $this->maids_model->get_maids_leave_by_date($service_date);

        $leave_maid_ids = array();
        foreach ($maids_leave_on_date as $leave) {
            array_push($leave_maid_ids, $leave->maid_id);
        }
        $time_from_stamp = $from_time;
        $time_to_stamp = $to_time;
        $maid_array = array();
        $maids_det = array();

        if ($book_type == 'OD') {
            $today_bookingss = $this->bookings_model->get_schedule_by_date_avail($service_date);
        }

        if ($book_type == 'BW' || $book_type == 'WE') {
            $bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($s_maids->maid_id, $repeat_day);
        }

        foreach ($search_maids as $s_maids) {

            $today_week_day = date('w', strtotime($service_date));
            //One day
            if ($book_type == 'OD') {
                //$time_from_stamp = strtotime(trim($booking->time_from));
                //$time_to_stamp = strtotime(trim($booking->time_to));

                // $today_bookingss = $this->bookings_model->get_schedule_by_date_avail($service_date); habeeb moved this out of loop
                // echo $service_date;
                // exit();
                foreach ($today_bookingss as $t_booking) {
                    $booked_slots[$t_booking->maid_id]['time'][strtotime($t_booking->time_from)] = strtotime($t_booking->time_to);

                }

                //$maids_s = $this->maids_model->get_maids();

                //foreach($maids_s as $smaid)
                //{
                if (isset($booked_slots[$s_maids->maid_id]['time'])) {
                    foreach ($booked_slots[$s_maids->maid_id]['time'] as $f_time => $t_time) {
                        if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
                            //array_push($nf_maids, $smaid->maid_id);
                            array_push($maids_det, $s_maids->maid_id);
                        }
                    }
                }
                //}
            } else if ($book_type == 'BW' || $book_type == 'WE') {
                if ($repeat_day < $today_week_day) {
                    $day_diff = (6 - $today_week_day + $repeat_day + 1);
                } else {
                    $day_diff = $repeat_day - $today_week_day;
                }
                $service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));
                //echo $repeat_day;
                //$bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($s_maids->maid_id, $repeat_day); habeeb moved this out of loop
                //                    echo '<pre>';
                //                    print_r($bookings_on_day);
                //exit();
                if (!empty($bookings_on_day)) {
                    foreach ($bookings_on_day as $booking_on_day) {
                        $s_date_stamp = strtotime($service_start_date);
                        $e_date_stamp = strtotime($service_start_date);
                        $bs_date_stamp = strtotime($booking_on_day->service_start_date);
                        $be_date_stamp = strtotime($booking_on_day->service_actual_end_date);

                        if ($booking_on_day->booking_type == "BW" || $booking_on_day->booking_type == "WE") {
                            $now = strtotime($service_start_date);
                            $your_date = strtotime($booking_on_day->service_start_date);
                            $datediff = $now - $your_date;
                            //echo $booking_on_day->service_start_date;
                            $week_diff = round($datediff / (60 * 60 * 24));
                            $week_difference = fmod($week_diff, 14);
                            //echo $week_difference;
                            if ($week_difference == 0 || $week_difference == '-0') {
                                $f_time = strtotime($booking_on_day->time_from);
                                $t_time = strtotime($booking_on_day->time_to);

                                if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
                                    array_push($maids_det, $s_maids->maid_id);
                                } else {

                                }
                            }
                        } /* else {
                         if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $booking_on_day->service_end == '0' && $repeat_end == 'never') || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
                         //if(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
                         {
                         $f_time = strtotime($booking_on_day->time_from);
                         $t_time = strtotime($booking_on_day->time_to);
                         if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time) )//|| ($time_from_stamp <= $f_time && $time_to_stamp <= $t_time)
                         {
                         array_push($maids_det, $s_maids->maid_id);
                         } else {
                         }
                         }
                         }*/
                    }
                } else {

                }
            }
        }

        // $result = array_diff((array)$search_maids, (array)$maids_det);
        // echo '<pre>';
        // print_r($maids_det);
        // echo '</pre>';
        // exit();





        $data['maids'] = $search_maids;
        $data['n_maids'] = $maids_det;
        $data['leave_maid_ids'] = $leave_maid_ids;
        //          echo $this->db->last_query();
        $data['settings'] = $this->settings_model->get_settings();
        $maids = $this->load->view('view_maid_booking', $data, TRUE);
        echo $maids;
        exit();
    }


    public function senddueemail()
    {
        $this->load->library('email');
        $amount = $this->input->post('balance');
        $customer_id = $this->input->post('customer_id');
        $description = $this->input->post('description');

        //$this->load->library('email');
        $data = array();
        $data['customer_details'] = $this->customers_model->get_customer_by_id($customer_id);
        $data['customer_name'] = $data['customer_details']->customer_name;
        $data['customer_id'] = $customer_id;
        $data['balance'] = $amount;
        $data['description'] = $description;
        $email = $data['customer_details']->email_address;
        $subject = "Due Payment";
        if ($email == "") {
            echo "error";
        } else {
			$payment_link_array = array();
			$payment_link_array['customer_id'] = $customer_id;
			$payment_link_array['amount'] = $amount;
			$payment_link_array['description'] = $description;
			$payment_link_array['payment_link'] = "https://booking.dubaihousekeeping.com/payment?id=".$customer_id."&amount=".$amount."&message=".$description;
			$payment_link_array['type'] = "Email";
			$payment_link_array['sent_by'] = user_authenticate();
			$payment_link_array['added_date_time'] = $datetime;
			$insert_link = $this->customers_model->add_payment_link($payment_link_array);
			
            //$this->load->library('email');
            // $html = $this->load->view('amountdue_template_email', $data, True);
            $html = $this->load->view('amountdue_template_email_new', $data, True);
            
            $config = array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://mail.dubaihousekeeping.com',
				'smtp_user' => 'noreply@dubaihousekeeping.com',
				'smtp_pass' => 'DHK4201c&',
				'smtp_port' => 465,
				'mailtype'  => 'html',
				'crlf' => "\r\n",
				'newline' => "\r\n"
			);
            $this->email->initialize($config);
			$this->email->set_mailtype("html");
			$this->email->set_newline("\r\n");
			$this->email->from('noreply@dubaihousekeeping.com', 'Company Name');
            $this->email->to($email);
            $this->email->subject($subject);
            $this->email->message($html);
			
            if($this->email->send())
			{
				//echo $this->email->print_debugger();
				//exit();
				echo 'success';
			} else {
				echo 'error';
			}
        }
    }
	
	public function add_link()
	{
		if($this->input->post('cust_id') != "")
		{
			$datetime = date("Y-m-d H:i:s");
			$customer_id = $this->input->post('cust_id');
			$amount = $this->input->post('amount');
			$description = $this->input->post('description');
			$type = $this->input->post('type');
			
			$payment_link_array = array();
			$payment_link_array['customer_id'] = $customer_id;
			$payment_link_array['amount'] = $amount;
			$payment_link_array['description'] = $description;
			$payment_link_array['payment_link'] = "https://booking.dubaihousekeeping.com/payment?id=".$customer_id."&amount=".$amount."&message=".$description;
			$payment_link_array['type'] = $type;
			$payment_link_array['sent_by'] = user_authenticate();
			$payment_link_array['added_date_time'] = $datetime;
			$insert_link = $this->customers_model->add_payment_link($payment_link_array);
			if($insert_link > 0)
			{
				echo 'success';
				exit();
			} else {
				echo 'error';
				exit();
			}
		} else {
			echo 'error';
			exit();
		}
	}

    public function sms_list_upload()
    {
        $data = array();
        $layout_data = array();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('sms_list_upload', $data, TRUE);
        $layout_data['page_title'] = 'SMS Upload';
        $layout_data['meta_description'] = 'SMS Upload';
        $layout_data['css_files'] = array('jquery.fancybox.css', 'demo.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.fancybox.pack.js', 'base.js', 'bootstrap-datepicker.js', 'jquery.validate.min.js', 'mymaids.js');

        $this->load->view('layouts/default_dashboard', $layout_data);
    }





    public function import_sms_list()
    {
        if ($this->input->post('upload_sms_list')) {
            $config = array(
                'upload_path' => FCPATH . 'upload/phone_num/',
                'allowed_types' => 'xls|xlsx'
            );
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('file')) {
                $data = $this->upload->data();
                @chmod($data['full_path'], 0777);

                $this->load->library('Excel');

                $file = $data['full_path'];

                $obj = PHPExcel_IOFactory::load($file);
                $cell = $obj->getActiveSheet()->getCellCollection();
                foreach ($cell as $cl) {
                    $column = $obj->getActiveSheet()->getCell($cl)->getColumn();
                    $row = $obj->getActiveSheet()->getCell($cl)->getRow();
                    $data_value = $obj->getActiveSheet()->getCell($cl)->getFormattedValue();

                    if ($row == 1) {
                        $header[$row][$column] = $data_value;
                    } else {
                        $arr_data[$row][$column] = $data_value;
                    }
                }

                $group_data = array();
                $group_data['sms_group_name'] = $this->input->post('group_name');
                $group_data['created_date'] = date('Y-m-d h:i:s');
                $group_data['sms_group_status'] = 1;
                $add_group = $this->customers_model->add_sms_group($group_data);

                //$det_array = array();

                // echo '<pre>';
                // print_r($arr_data);
                // exit();
                if ($add_group) {
                    foreach ($arr_data as $val) {
                        $phone = $val['A'];

                        $group_data = array();
                        $group_data['number_value'] = $phone;
                        $group_data['number_group_id'] = $add_group;
                        $group_data['number_status'] = 1;
                        $add_group_numbers = $this->customers_model->add_group_numbers($group_data);
                    }
                    $msg = "<div class=\"alert alert-success text-center\"><strong>Numbers Inserted Successfully!</strong> </div>";
                    $this->session->set_flashdata('message', $msg);
                } else {
                    $msg = "<div class=\"alert alert-danger text-center\"><strong>Error! Data not added!</strong> </div>";
                    $this->session->set_flashdata('message', $msg);
                }

            } else {
                $msg = "<div class=\"alert alert-danger text-center\"><strong>" . $this->upload->display_errors() . "</strong> </div>";
                $this->session->set_flashdata('message', $msg);
            }



        }
        redirect('sms-list-upload');
    }

    public function email_list_upload()
    {

        $data = array();
        $layout_data = array();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('email_list_upload', $data, TRUE);
        $layout_data['page_title'] = 'Email Upload';
        $layout_data['meta_description'] = 'Email Upload';
        $layout_data['css_files'] = array('jquery.fancybox.css', 'demo.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.fancybox.pack.js', 'base.js', 'bootstrap-datepicker.js', 'jquery.validate.min.js', 'mymaids.js');

        $this->load->view('layouts/default_dashboard', $layout_data);
    }

    public function import_email_list()
    {
        if ($this->input->post('upload_email_list')) {
            $config = array(
                'upload_path' => FCPATH . 'upload/email_files/',
                'allowed_types' => 'xls|xlsx'
            );
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('file')) {
                $data = $this->upload->data();
                @chmod($data['full_path'], 0777);

                $this->load->library('Excel');

                $file = $data['full_path'];

                $obj = PHPExcel_IOFactory::load($file);
                $cell = $obj->getActiveSheet()->getCellCollection();
                foreach ($cell as $cl) {
                    $column = $obj->getActiveSheet()->getCell($cl)->getColumn();
                    $row = $obj->getActiveSheet()->getCell($cl)->getRow();
                    $data_value = $obj->getActiveSheet()->getCell($cl)->getFormattedValue();

                    if ($row == 1) {
                        $header[$row][$column] = $data_value;
                    } else {
                        $arr_data[$row][$column] = $data_value;
                    }
                }
                // echo '<pre>';
                // print_r($arr_data);
                // exit();

                $group_data = array();
                $group_data['email_group_name'] = $this->input->post('group_name');
                $group_data['created_date'] = date('Y-m-d h:i:s');
                $group_data['email_group_status'] = 1;
                $add_group = $this->customers_model->add_email_group($group_data);

                //$det_array = array();

                // echo '<pre>';
                // print_r($arr_data);
                // exit();
                if ($add_group) {
                    foreach ($arr_data as $val) {
                        $phone = $val['A'];

                        $group_data = array();
                        $group_data['email_value'] = $phone;
                        $group_data['email_group_id'] = $add_group;
                        $group_data['email_status'] = 1;
                        $add_group_numbers = $this->customers_model->add_group_emails($group_data);
                    }
                    $msg = "<div class=\"alert alert-success text-center\"><strong>Emails Inserted Successfully!</strong> </div>";
                    $this->session->set_flashdata('message', $msg);
                } else {
                    $msg = "<div class=\"alert alert-danger text-center\"><strong>Error! Data not added!</strong> </div>";
                    $this->session->set_flashdata('message', $msg);
                }

            } else {
                $msg = "<div class=\"alert alert-danger text-center\"><strong>" . $this->upload->display_errors() . "</strong> </div>";
                $this->session->set_flashdata('message', $msg);
            }



        }
        redirect('email-list-upload');
    }

    // Geethu
    function search()
    {
        $keyword = $this->input->post('search_keyword');

        $search_results = $this->customers_model->get_customers_by_keyword($keyword);
        if (!empty($search_results)) {
            $i = 0;
            foreach ($search_results as $customers) {
                if ($customers->building != "") {
                    $apartmnt_no = 'Apartment No:' . $customers->building . ', ' . $customers->unit_no . ', ' . $customers->street . '<br/>';
                } else {
                    $apartmnt_no = "";
                }
                $last_jobdate = $this->customers_model->get_last_job_date_by_customerid($customers->customer_id);
                if (empty($last_jobdate)) {
                    $last_date = "";
                } else {
                    $last_date = $last_jobdate->service_date;
                }
                $addedtime = ($customers->customer_added_datetime) ? date('d/m/Y H:i:s', strtotime($customers->customer_added_datetime)) : "";

                if ($customers->is_flag == "Y") {
                    $isflag = " -- Flagged (" . $customers->flag_reason . ")";
                } else if ($customers->is_flag == "N") {
                    $isflag = "";
                }

                echo '<div class="Row">

                                <div class="Cell">
                                    <p>' . ++$i . '</p>
                                </div>

                                <div class="Cell">
                                    <p><a href=' . base_url() . 'customer/view/' . $customers->customer_id . ' style="text-decoration: none;color:#333;">' . $customers->customer_name . '<br/><span style="color: red;">' . $isflag . '</span></a></p>
                                </div>

                                <div class="Cell">
                                    <p>' . $customers->mobile_number_1 . '</p>
                                </div>
                                <div class="Cell">
                                    <p>' . $customers->zone_name . '-' . $customers->area_name . '</p>
                                </div>
                                <div class="Cell">
                                    <p>' . $apartmnt_no . '' . $customers->customer_address . '</p>
                                </div>

                                <div class="Cell">
                                    <p>' . $customers->customer_source . '</p>
                                </div>
                                
                                <div class="Cell">
                                    <p>' . $last_date . '</p>
                                </div>


                                <div class="Cell">
                                    <p>' . $addedtime . '</p>
                                </div>

                                <div class="Cell">
                                    <p>
                                    <a class="btn btn-small btn-info" href="' . base_url() . 'customer/view/' . $customers->customer_id . '">
                                    <i class="btn-icon-only fa fa-eye "> </i>
                                    </a>
                                        <a class="btn btn-small btn-warning" href="' . base_url() . 'customer/edit/' . $customers->customer_id . '">
                                        <i class="btn-icon-only icon-pencil"> </i></a>';

                if ($customers->customer_status == 1) {
                    echo '<a class="btn btn-small btn-success" href="javascript::void();" onclick="delete_customer(this,' . $customers->customer_id . ',' . $customers->customer_status . ')" ><i class="btn-icon-only icon-ok"> </i></a>';
                } else {
                    echo '<a class="btn btn-small btn-danger" href="javascript::void();" onclick="delete_customer(this,' . $customers->customer_id . ',' . $customers->customer_status . ')" ><i class="btn-icon-only icon-remove"> </i></a>';
                }
                echo ' </p>
                                </div>


                            </div>';

            }
        } else {
            echo '<div class="Row"><p>No records found</p></div>';
        }
    }

    function tests()
    {
        $data = $this->customers_model->getdata();
        echo '<pre>';
        print_r($data);
        echo '</pre>';
    }

    function search_by_date()
    {
        $from_date = "";
        $to_date = "";
        if ($this->input->post('from_date'))
            $from_date = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('from_date'))));
        if ($this->input->post('to_date'))
            $to_date = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('to_date'))));
        if ($this->input->post('payment_type'))
            $payment_type = $this->input->post('payment_type');
        if ($this->input->post('all_customers'))
            $all_customers = $this->input->post('all_customers');
        if ($this->input->post('sort_custmer'))
            $sort_custmer = $this->input->post('sort_custmer');
        if ($this->input->post('sort_source'))
            $sort_source = $this->input->post('sort_source');
        if ($this->input->post('checkdate'))
            $checkdate = $this->input->post('checkdate');
        if ($this->input->post('cust_type'))
            $cust_type = $this->input->post('cust_type');
        else
            $cust_type = NULL;
        if ($checkdate == 0) {

            if ($sort_custmer != 3) {
                $search_results = $this->customers_model->search_by_date($from_date, $to_date, $payment_type, $all_customers, $sort_custmer, $sort_source, $cust_type);
                if (!empty($search_results)) {
                    $i = 0;
                    foreach ($search_results as $customers) {
                        $last_jobdate = $this->customers_model->get_last_job_date_by_customerid($$customers->customer_id);
                        if (empty($last_jobdate)) {
                            $last_date = "";
                        } else {
                            $last_date = $last_jobdate->service_date;
                        }
                        if ($customers->building != "") {
                            $apartmnt_no = 'Apartment No:' . $customers->building . ',' . $customers->unit_no . ',' . $customers->street . '<br/>';
                        } else {
                            $apartmnt_no = "";
                        }
                        $addedtime = ($customers->customer_added_datetime) ? date('d/m/Y H:i:s', strtotime($customers->customer_added_datetime)) : "";
                        echo '<div class="Row">
                    <div class="Cell"><p>' . ++$i . '</p></div>
                    <div class="Cell"><p>' . $customers->customer_name . '</p></div>';
                        if (user_authenticate() == 1) {
                            echo '<div class="Cell"><p>' . $customers->mobile_number_1 . '</p></div>';
                        }
                        echo '<div class="Cell"><p>' . $customers->zone_name . '-' . $customers->area_name . '</p></div>
                    <div class="Cell"><p>' . $apartmnt_no . '' . wordwrap($customers->customer_address, 25, "<br>") . '</p></div>
                    <div class="Cell"><p>' . $customers->customer_source . '</p></div>
					<div class="Cell"><p>' . $last_date . '</p></div>
                    <div class="Cell"><p>' . $addedtime . '</p></div>
                    <div class="Cell"><p>
                        <a class="btn btn-small btn-info" href="' . base_url() . 'customer/view/' . $customers->customer_id . '"><i class="btn-icon-only icon-search"> </i></a>
                        <a class="btn btn-small btn-warning" href="' . base_url() . 'customer/edit/' . $customers->customer_id . '"><i class="btn-icon-only icon-pencil"> </i></a>';
                        if ($customers->customer_status == 1) {
                            echo '<a class="btn btn-small btn-success" href="javascript::void();" onclick="delete_customer(this,' . $customers->customer_id . ',' . $customers->customer_status . ')" ><i class="btn-icon-only icon-ok"> </i></a>';
                        } else {
                            echo '<a class="btn btn-small btn-danger" href="javascript::void();" onclick="delete_customer(this,' . $customers->customer_id . ',' . $customers->customer_status . ')" ><i class="btn-icon-only icon-remove"> </i></a>';
                        }
                        echo ' </p></div>
                  </div>';
                    }
                } else {
                    echo '<div class="Row"><p>No Records!</p></div>';
                }
            } else {
                $search_results = $this->customers_model->search_by_date_sort($from_date, $to_date, $payment_type, $all_customers, $sort_custmer, $cust_type);

                if (!empty($search_results)) {
                    $i = 0;
                    foreach ($search_results as $search_custmr) {
                        $custs_id = $search_custmr->customer_id;
                        $secondsearchresults = $this->customers_model->search_cust_by_booking($custs_id, $from_date, $to_date, $payment_type, $all_customers);
                        //echo $this->db->last_query();
                        if (empty($secondsearchresults)) {
                            echo '<tr style="display:none;"><td colspan="6" style="line-height: 18px; text-align:center;">No Records!</td></tr>';
                            //break;
                        } else {
                            foreach ($secondsearchresults as $secondrow) {
                                if ($secondrow->building != "") {
                                    $apartmnt_no = 'Apartment No:' . $secondrow->building . ',' . $secondrow->unit_no . ',' . $secondrow->street . '<br/>';
                                } else {
                                    $apartmnt_no = "";
                                }
                                $addedtime = ($secondrow->customer_added_datetime) ? date('d/m/Y H:i:s', strtotime($secondrow->customer_added_datetime)) : "";
                                echo '<div class="Row">
                    <div class="Cell"><p>' . ++$i . '</p></div>
                    <div class="Cell"><p>' . $secondrow->customer_name . '</p></div>';
                                if (user_authenticate() == 1) {
                                    echo '<div class="Cell"><p>' . $secondrow->mobile_number_1 . '</p></div>';
                                }
                                echo '<div class="Cell"><p>' . $secondrow->zone_name . '-' . $secondrow->area_name . '</p></div>
                    <div class="Cell"><p>' . $apartmnt_no . '' . wordwrap($secondrow->customer_address, 25, "<br>") . '</p></div>
                    <div class="Cell"><p>' . $secondrow->customer_source . '</p></div>
                    <div class="Cell"><p>' . $addedtime . '</p></div>
                    <div class="Cell"><p>
                        <a class="btn btn-small btn-info" href="' . base_url() . 'customer/view/' . $secondrow->customer_id . '"><i class="btn-icon-only icon-search"> </i></a>
                        <a class="btn btn-small btn-warning" href="' . base_url() . 'customer/edit/' . $secondrow->customer_id . '"><i class="btn-icon-only icon-pencil"> </i></a>';
                                if ($secondrow->customer_status == 1) {
                                    echo '<a class="btn btn-small btn-success" href="javascript::void();" onclick="delete_customer(this,' . $secondrow->customer_id . ',' . $secondrow->customer_status . ')" ><i class="btn-icon-only icon-ok"> </i></a>';
                                } else {
                                    echo '<a class="btn btn-small btn-danger" href="javascript::void();" onclick="delete_customer(this,' . $secondrow->customer_id . ',' . $secondrow->customer_status . ')" ><i class="btn-icon-only icon-remove"> </i></a>';
                                }
                                echo ' </p></div>
                  </div>';
                            }
                        }
                    }
                } else {
                    echo '<div class="Row"><p>No Records!</p></div>';
                }

            }
        } else {



            if ($sort_custmer != 3) {
                if ($from_date && $to_date && $sort_custmer == 2) {
                    echo "no";
                } else {
                    $search_results = $this->customers_model->search_by_date1($from_date, $to_date, $payment_type, $all_customers, $sort_custmer, $cust_type);
                    //            echo $this->db->last_query();
//            exit();
                    if (!empty($search_results)) {
                        $i = 0;
                        foreach ($search_results as $customers) {
                            if ($customers->building != "") {
                                $apartmnt_no = 'Apartment No:' . $customers->building . ',' . $customers->unit_no . ',' . $customers->street . '<br/>';
                            } else {
                                $apartmnt_no = "";
                            }
                            $addedtime = ($customers->customer_added_datetime) ? date('d/m/Y H:i:s', strtotime($customers->customer_added_datetime)) : "";
                            echo '<div class="Row">
                    <div class="Cell"><p>' . ++$i . '</p></div>
                    <div class="Cell"><p>' . $customers->customer_name . '</p></div>';
                            if (user_authenticate() == 1) {
                                echo '<div class="Cell"><p>' . $customers->mobile_number_1 . '</p></div>';
                            }
                            echo '<div class="Cell"><p>' . $customers->zone_name . '-' . $customers->area_name . '</p></div>
                        <div class="Cell"><p>' . $apartmnt_no . '' . wordwrap($customers->customer_address, 25, "<br>") . '</p></div>
                    <div class="Cell"><p>' . $customers->customer_source . '</p></div>
                    <div class="Cell"><p>' . $addedtime . '</p></div>
                    <div class="Cell"><p>
                        <a class="btn btn-small btn-info" href="' . base_url() . 'customer/view/' . $customers->customer_id . '"><i class="btn-icon-only icon-search"> </i></a>
                        <a class="btn btn-small btn-warning" href="' . base_url() . 'customer/edit/' . $customers->customer_id . '"><i class="btn-icon-only icon-pencil"> </i></a>';
                            if ($customers->customer_status == 1) {
                                echo '<a class="btn btn-small btn-success" href="javascript::void();" onclick="delete_customer(this,' . $customers->customer_id . ',' . $customers->customer_status . ')" ><i class="btn-icon-only icon-ok"> </i></a>';
                            } else {
                                echo '<a class="btn btn-small btn-danger" href="javascript::void();" onclick="delete_customer(this,' . $customers->customer_id . ',' . $customers->customer_status . ')" ><i class="btn-icon-only icon-remove"> </i></a>';
                            }
                            echo ' </p></div>
                  </div>';
                        }
                    } else {
                        echo '<div class="Row"><p>No Records!</p></div>';
                    }
                }
            } else {
                $search_results = $this->customers_model->search_by_date_sort1($from_date, $to_date, $payment_type, $all_customers, $sort_custmer, $cust_type);
                //            echo '<pre>';
//            print_r($search_results);
//            echo '</pre>';
//            exit();
                if (!empty($search_results)) {
                    $i = 0;
                    foreach ($search_results as $search_custmr) {
                        $custs_id = $search_custmr->customer_id;
                        $secondsearchresults = $this->customers_model->search_cust_by_booking1($custs_id, $from_date, $to_date, $payment_type, $all_customers);
                        //                    echo $this->db->last_query();
//                    exit();
                        if (empty($secondsearchresults)) {
                            echo '<tr style="display:none;"><td colspan="6" style="line-height: 18px; text-align:center;">No Records!</td></tr>';
                            //break;
                        } else {
                            foreach ($secondsearchresults as $secondrow) {
                                if ($secondrow->building != "") {
                                    $apartmnt_no = 'Apartment No:' . $secondrow->building . ',' . $secondrow->unit_no . ',' . $secondrow->street . '<br/>';
                                } else {
                                    $apartmnt_no = "";
                                }
                                $addedtime = ($secondrow->customer_added_datetime) ? date('d/m/Y H:i:s', strtotime($secondrow->customer_added_datetime)) : "";
                                echo '<div class="Row">
                    <div class="Cell"><p>' . ++$i . '</p></div>
                    <div class="Cell"><p>' . $secondrow->customer_name . '</p></div>';
                                if (user_authenticate() == 1) {
                                    echo '<div class="Cell"><p>' . $secondrow->mobile_number_1 . '</p></div>';
                                }
                                echo '<div class="Cell"><p>' . $secondrow->zone_name . '-' . $secondrow->area_name . '</p></div>
                        <div class="Cell"><p>' . $apartmnt_no . '' . wordwrap($secondrow->customer_address, 25, "<br>") . '</p></div>
                    <div class="Cell"><p>' . $secondrow->customer_source . '</p></div>
                    <div class="Cell"><p>' . $addedtime . '</p></div>
                    <div class="Cell"><p>
                        <a class="btn btn-small btn-info" href="' . base_url() . 'customer/view/' . $secondrow->customer_id . '"><i class="btn-icon-only icon-search"> </i></a>
                        <a class="btn btn-small btn-warning" href="' . base_url() . 'customer/edit/' . $secondrow->customer_id . '"><i class="btn-icon-only icon-pencil"> </i></a>';
                                if ($secondrow->customer_status == 1) {
                                    echo '<a class="btn btn-small btn-success" href="javascript::void();" onclick="delete_customer(this,' . $secondrow->customer_id . ',' . $secondrow->customer_status . ')" ><i class="btn-icon-only icon-ok"> </i></a>';
                                } else {
                                    echo '<a class="btn btn-small btn-danger" href="javascript::void();" onclick="delete_customer(this,' . $secondrow->customer_id . ',' . $secondrow->customer_status . ')" ><i class="btn-icon-only icon-remove"> </i></a>';
                                }
                                echo ' </p></div>
                  </div>';
                            }
                        }
                    }
                } else {
                    echo '<div class="Row"><p style="text-align:center;">No Records!</p></div>';
                }

            }
        }


    }

    //    public function do_odoo($operation, $data = array(), $id = NULL) {
//        $user_id = odoo_login();
//        $data['customer_type'] = $data['customer_type'] == 'HO' ? 'home' : ($data['customer_type'] == 'OF' ? 'office' : 'warehouse');
//        $data['payment_type'] = $data['payment_type'] == 'D' ? 'daily' : ($data['payment_type'] == 'W' ? 'weekly' : 'monthly');
//        $data['payment_mode'] = strtolower($data['payment_mode']);
//        $data['key_given'] = $data['key_given'] == 'Y' ? TRUE : FALSE;
//
//        $fields = array(
//            'name' => new xmlrpcval($data['customer_name'], "string"),
//            'nick_name' => new xmlrpcval($data['customer_nick_name'], "string"),
//            'cust_type' => new xmlrpcval($data['customer_type'], "string"),
//            'phone' => new xmlrpcval($data['phone_number'], "string"),
//            'mobile' => new xmlrpcval($data['mobile_number_1'], "string"),
//            'mobile_2' => new xmlrpcval($data['mobile_number_2'], "string"),
//            'mobile_3' => new xmlrpcval($data['mobile_number_3'], "string"),
//            'fax' => new xmlrpcval($data['fax_number'], "string"),
//            'email' => new xmlrpcval($data['email_address'], "string"),
//            'area_id' => new xmlrpcval($data['area_id'], "int"),
//            'website' => new xmlrpcval($data['website_url'], "string"),
//            'user_name' => new xmlrpcval($data['customer_username'], "string"),
//            'password' => new xmlrpcval($data['customer_password'], "string"),
//            'payment_type' => new xmlrpcval($data['payment_type'], "string"),
//            'payment_mode' => new xmlrpcval($data['payment_mode'], "string"),
//            'hourly' => new xmlrpcval(floatval($data['price_hourly']), "double"),
//            'extra' => new xmlrpcval(floatval($data['price_extra']), "double"),
//            'weekend' => new xmlrpcval(floatval($data['price_weekend']), "double"),
//            'latitude' => new xmlrpcval($data['latitude'], "string"),
//            'longitude' => new xmlrpcval($data['longitude'], "string"),
//            'key' => new xmlrpcval($data['key_given'], "boolean"),
//            'comment' => new xmlrpcval($data['customer_notes'], "string"),
//        );
//
//        $response = odoo_operation($user_id, $operation, "res.partner", $fields, $id);
//        //echo '<pre>';print_r($response);exit();
//
//        return $response;
//    }


    public function get_zone_province()
    {
        $areaId = $this->input->post('area_id');
        $custZoneProv = $this->customers_model->get_customer_zone_province_by_area_id($areaId);
        echo json_encode($custZoneProv);
        exit();
    }

    public function toExcel()
    {
        $active = $this->uri->segment(3) ? $this->uri->segment(3) : 2;
        $data['active'] = $active;

        $data["customers"] = $this->customers_model->fetch_customers_excel($active);
        $this->load->view('customer_spreadsheetview', $data);
    }

    public function customer_statement()
    {
        $customers = $this->customers_model->get_customers();

        $date_from_job = date('Y-m-d', strtotime('-1 week'));
        $date_to_job = date('Y-m-d');

        $data = array();

        $data['search_date_from_statement'] = date('d/m/Y', strtotime($date_from_job));
        $data['search_date_to_statement'] = date('d/m/Y', strtotime($date_to_job));

        $customer_statement_array = array();

        if ($this->input->post('statement_customer_id') != "") {
            $customer_id = $this->input->post('statement_customer_id');
            $d_from = $this->input->post('cstatemnet-date-from');
            $s_date = explode("/", $d_from);
            $d_from_date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];

            $d_to = $this->input->post('cstatemnet-date-to');
            $s_date1 = explode("/", $d_to);
            $d_to_date = $s_date1[2] . '-' . $s_date1[1] . '-' . $s_date1[0];

            $get_the_previous_services = $this->day_services_model->get_the_previous_services($d_from_date, $customer_id);
            $balance_amt = (($get_the_previous_services->totalfee) - ($get_the_previous_services->outfee + $get_the_previous_services->paidamount));

            $startdate = DateTime::createFromFormat('d/m/Y', $this->input->post('cstatemnet-date-from'));
            $statementstartdate = $startdate->format('Y-m-d');
            $enddate = DateTime::createFromFormat('d/m/Y', $this->input->post('cstatemnet-date-to'));
            $statementenddate = $enddate->format('Y-m-d');

            $day = 86400; // Day in seconds  
            $format = 'Y-m-d'; // Output format (see PHP date funciton)  
            $sTime = strtotime($statementstartdate); // Start as time  
            $eTime = strtotime($statementenddate); // End as time  
            $numDays = round(($eTime - $sTime) / $day) + 1;
            $days = array();
            //$html = "";
            $i = 1;
            //$get_service_list_array = array();
            for ($d = 0; $d < $numDays; $d++) {
                $days = date($format, ($sTime + ($d * $day)));
                $get_service_list = $this->day_services_model->get_service_list($days, $customer_id);
                array_push($customer_statement_array, $get_service_list);
            }

            $data['search_date_from_statement'] = $this->input->post('cstatemnet-date-from');
            $data['search_date_to_statement'] = $this->input->post('cstatemnet-date-to');
            $data['search_cust_id'] = $customer_id;
        } else {
            $customer_statement_array = array();
            $balance_amt = 0;
        }
        $data['customers'] = $customers;
        $data['customer_statement_array'] = $customer_statement_array;
        $data['balance_amount'] = $balance_amt;

        $layout_data['content_body'] = $this->load->view('customer_statement', $data, TRUE);
        $layout_data['page_title'] = 'Customer Statement';
        $layout_data['meta_description'] = 'Customer Statement';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css');
        $layout_data['accounts_active'] = '1';
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('base.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js', 'mymaids.js');
        $this->load->view('layouts/default_dashboard', $layout_data);
    }


    public function bulk_sms()
    {
        $data = array();
        $customertype = 1;
        $customers = $this->customers_model->get_customer_by_type($customertype);
        $data['sms_groups'] = $this->customers_model->get_sms_groups();
        if (!empty($customers)) {

            foreach ($customers as $cus) {
                if ($cus->mobile_number_1 != "" && strlen($cus->mobile_number_1) >= 9) {
                    $mobile[] = $cus->mobile_number_1;
                }

                $data['customer_mob_numbers'] = implode(",", $mobile);
            }

        } else {
            $data['customer_mob_numbers'] = '';
        }
        if ($this->input->post()) {

            $moblist = explode(",", $this->input->post('mobnumber_list'));
            $message = $this->input->post("sms_message");
            if ($message != "") {
                if (!empty($moblist)) {

                    foreach ($moblist as $list) {

                        $mobile = $list;
                        if ($mobile != "") {

                            //$this->_send_sms($mobile, $message);
                        }

                    }

                } else {
                    $this->session->set_flashdata('sms_error', 'Mobile number is required !');
                    redirect("customer/bulk_sms");
                }

                $this->session->set_flashdata('sms_success', 'Sms has been sent !');
                redirect("customer/bulk_sms");
            } else {
                $this->session->set_flashdata('sms_error', 'Message is required !');
                redirect("customer/bulk_sms");
            }
        }
        $layout_data['content_body'] = $this->load->view('customer_bulk_sms', $data, TRUE);
        $layout_data['page_title'] = 'Send Sms';
        $layout_data['meta_description'] = 'Bulk Sms';
        $layout_data['css_files'] = array('demo.css', 'bootstrap-tagsinput.css');
        $layout_data['settings_active'] = '1';
        $layout_data['js_files'] = array('bootstrap-tagsinput.js');
        $layout_data['external_js_files'] = array('https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js');


        $this->load->view('layouts/default', $layout_data);

    }
    private function _send_sms($mobile_number, $message)
    {
        $num = substr($mobile_number, -9);

        $sms_url = 'http://sms.azinova.in/WebServiceSMS.aspx?User=elitemaids&passwd=emaid@123&mobilenumber=971' . $num . '&message=' . urlencode($message) . '&sid=EliteMaids&mtype=N';
        $sms_url = str_replace(" ", '%20', $sms_url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $sms_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $res = curl_exec($ch);
        curl_close($ch);
    }

    public function customer_bulk_ajax()
    {
        $type_customer = $this->input->post("customertype");

        if ($type_customer > 1) {
            $phone_nums = $this->customers_model->get_sms_group_numbers($type_customer);
            if (!empty($phone_nums)) {
                foreach ($phone_nums as $phone_num) {
                    if ($phone_num->number_value != "" && strlen($phone_num->number_value) >= 9) {
                        $mobile[] = $phone_num->number_value;
                    }
                }
                echo json_encode($mobile);
                exit;

            } else {
                echo json_encode(array());
            }
        } else {
            $customers = $this->customers_model->get_customer_by_type($type_customer);
            if (!empty($customers)) {
                foreach ($customers as $cus) {
                    if ($cus->mobile_number_1 != "" && strlen($cus->mobile_number_1) >= 9) {
                        $mobile[] = $cus->mobile_number_1;
                    }
                }
                echo json_encode($mobile);
                exit;

            } else {
                echo json_encode(array());
            }
        }
    }


    public function customer_bulk_email()
    {
        $data = array();
        $email = array();
        $customertype = 1;
        $customers = $this->customers_model->get_customer_by_type($customertype);
        $data['email_groups'] = $this->customers_model->get_email_groups();

        if (!empty($customers)) {

            foreach ($customers as $cus) {

                if ($cus->email_address != "") {
                    $email[] = $cus->email_address;
                }

                $data['customer_email_address'] = implode(",", $email);
            }

        } else {
            $data['customer_email_address'] = '';
        }
        if ($this->input->post()) {

            $emaillist = explode(",", $this->input->post('email_list'));
            $message = $this->input->post("email_message");
            $subject = $this->input->post("email_subject");
            $files = glob('blk_mail_img/*');
            $bnr_url = explode("/", $files[0]);
            $bnr_url = $bnr_url[1];

            if ($message != "") {
                if (!empty($emaillist)) {

                    foreach ($emaillist as $list) {

                        $email_list = $list;

                        if ($email_list != "") {

                            $this->_send_email($email_list, $message, $subject, $bnr_url);
                        }

                    }

                } else {
                    $this->session->set_flashdata('email_error', 'Email address is required !');
                    redirect("customer/customer_bulk_email");
                }

                $this->session->set_flashdata('email_success', 'Email has been sent !');
                redirect("customer/customer_bulk_email");
            } else {
                $this->session->set_flashdata('email_error', 'Message is required !');
                redirect("customer/customer_bulk_email");
            }
        }
        $layout_data['content_body'] = $this->load->view('customer_bulk_email', $data, TRUE);
        $layout_data['page_title'] = 'Send Email';
        $layout_data['meta_description'] = 'Bulk Email';
        $layout_data['css_files'] = array('demo.css', 'bootstrap-tagsinput.css');
        $layout_data['settings_active'] = '1';
        $layout_data['js_files'] = array('bootstrap-tagsinput.js', 'ajaxupload.3.5.js', 'bulkmail.js');
        $layout_data['external_js_files'] = array('https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js');


        $this->load->view('layouts/default', $layout_data);


    }

    public function customer_bulk_email_ajax()
    {
        $type_customer = $this->input->post("customertype");
        if ($type_customer > 1) {
            $emailss = $this->customers_model->get_email_group_list($type_customer);
            if (!empty($emailss)) {
                foreach ($emailss as $email_val) {
                    if ($email_val->email_value != "") {
                        $email[] = $email_val->email_value;
                    }
                }
                echo json_encode($email);
                exit;
            } else {
                echo json_encode(array());
            }
        } else {
            $customers = $this->customers_model->get_customer_by_type($type_customer);
            if (!empty($customers)) {
                foreach ($customers as $cus) {
                    if ($cus->email_address != "") {
                        $email[] = $cus->email_address;
                    }
                }
                echo json_encode($email);
                exit;
            }
        }
    }

    public function _send_email($email, $message, $subject = NULL, $bnr_url)
    {
        //$this->load->library('email');
        $img_url = $bnr_url == '' ? "images/dhk-email-banner.jpg" : 'blk_mail_img/' . $bnr_url;
        $message = '<div class="email-template-body" style="width:800px; margin: 0 auto;  background: #FFF; border-top:0px;">
                <div style="width:800px; margin: 0 auto;  background: #FFF; border-top:0px;">
                <div style="width:800px; height:231px;"><img src="' . base_url() . $img_url . '" width="800" height="231"></div>' . $message;
        $data['message'] = $message;
        $data['banner_url'] = $bnr_url;
        $html = $this->load->view('bulk_email_template', $data, True);

        $config = array(
                    'protocol'  => 'smtp',
					'smtp_host' => 'ssl://smtp.googlemail.com',
					'smtp_port' => 465,
					'smtp_user' => 'emaidmaps@gmail.com',
					'smtp_pass' => 'viltazemmwrmrybw',
					'mailtype'  => 'html',
					'charset'   => 'utf-8'
              );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        // $this->email->from('online@elitemaids.emaid.info', 'ELITEMAIDS');
        $this->email->from('emaidmaps@gmail.com', 'Company Name');
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($html);
        //$this->email->send();
    }

    function report_srch_usr_news()
    {
        $searchTerm = $this->input->post('searchTerm');

        // if (strlen(trim($searchTerm)) < 2) {
        //     echo json_encode([]);
        //     exit();
        // }

        // $search_result=$this->customers_model->report_srch_usr_news_new($searchTerm);
        $search_result = $this->customers_model->report_srch_usr_news($searchTerm);


        $data = array();

        foreach ($search_result as $user) {
            $userdetail = $user->customer_name . ',  ' . $user->mobile_number_1 . ', ' . $user->mobile_number_2;
            $data[] = array("id" => $user->customer_id, "text" => nl2br($userdetail));
        }

        echo json_encode($data);
        exit();
    }

    public function import_total_areas()
    {
        $this->load->library('Excel');
        $file = "./upload/customerdatalat.xlsx";
        $obj = PHPExcel_IOFactory::load($file);
        $cell = $obj->getActiveSheet()->getCellCollection();
        foreach ($cell as $cl) {
            $column = $obj->getActiveSheet()->getCell($cl)->getColumn();
            $row = $obj->getActiveSheet()->getCell($cl)->getRow();
            $data_value = $obj->getActiveSheet()->getCell($cl)->getFormattedValue();

            if ($row == 1) {
                $header[$row][$column] = $data_value;
            } else {
                $arr_data[$row][$column] = $data_value;
            }
        }
        $data['header'] = $header;
        $data['values'] = $arr_data;
        $i = 1;
        foreach ($arr_data as $val) {
            $area_name = $val['A'];
            $zone_id = 1;
            $data = array(
                'zone_id' => $zone_id,
                'area_name' => $area_name,
                'area_status' => 1,
            );
            $area_id = $this->settings_model->add_area($data);
            echo $i . '-' . $area_name . '-' . $area_id;
            echo '<br>';
        }
        // echo '<pre>';
        // print_r($arr_data);
        exit();
    }

    public function import_total_customers()
    {
        $this->load->library('Excel');
        $file = "./upload/customerdatalatnew.xlsx";
        $obj = PHPExcel_IOFactory::load($file);
        $cell = $obj->getActiveSheet()->getCellCollection();
        foreach ($cell as $cl) {
            $column = $obj->getActiveSheet()->getCell($cl)->getColumn();
            $row = $obj->getActiveSheet()->getCell($cl)->getRow();
            $data_value = $obj->getActiveSheet()->getCell($cl)->getFormattedValue();

            if ($row == 1) {
                $header[$row][$column] = $data_value;
            } else {
                $arr_data[$row][$column] = $data_value;
            }
        }
        $data['header'] = $header;
        $data['values'] = $arr_data;

        foreach ($arr_data as $val) {
            if ($val['D'] != 0) {
                $email = $val['D'];
            } else {
                $email = "";
            }
            $customer_source = $val['A'];
            $customer_name = $val['B'];
            $customer_username = $email;

            $customer_location = $val['F'];
            $area = $val['G'];

            $get_area_id = $this->settings_model->get_area_id_name($area);

            if (!empty($get_area_id)) {
                $area_id = $get_area_id->area_id;

                if ($val['H'] == 'Monthly') {
                    $payment_mode = 'M';
                } else if ($val['H'] == 'Weekly') {
                    $payment_mode = 'W';
                } else {
                    $payment_mode = 'D';
                }

                if ($val['H'] == 'Monthly') {
                    $reglar = 1;
                } else {
                    $reglar = 0;
                }

                if ($val['I'] == 'Active') {
                    $status = 1;
                } else {
                    $status = 0;
                }


                $customer_array = array();
                $customer_array['odoo_customer_id'] = 0;
                $customer_array['customer_username'] = $customer_username;
                $customer_array['customer_password'] = '123456';
                $customer_array['customer_type'] = 'HO';
                $customer_array['customer_name'] = $customer_name;
                $customer_array['customer_nick_name'] = $customer_name;
                $customer_array['is_company'] = 'N';
                $customer_array['company_name'] = '';
                $customer_array['mobile_number_1'] = $val['C'];
                $customer_array['email_address'] = $customer_username;
                $customer_array['contact_person'] = $customer_name;
                $customer_array['payment_type'] = $payment_mode;
                $customer_array['payment_mode'] = 'Cash';
                $customer_array['key_given'] = 'N';
                $customer_array['price_hourly'] = 35;
                $customer_array['price_extra'] = 35;
                $customer_array['price_weekend'] = 35;
                $customer_array['balance'] = 0;
                $customer_array['customer_source'] = $customer_source;
                $customer_array['customer_source_id'] = 0;
                $customer_array['customer_added_datetime'] = date('Y-m-d H:i:s');
                $customer_array['customer_last_modified_datetime'] = date('Y-m-d H:i:s');
                $customer_array['customer_booktype'] = $reglar;
                $customer_array['customer_status'] = $status;

                // echo '<pre>';
                // print_r($customer_array);
                // exit();

                $customer_address_array = array();
                $customer_address_array['area_id'] = $area_id;
                $customer_address_array['customer_address'] = $customer_location;
                $customer_address_array['default_address'] = 1;

                $customer_id = $this->customers_model->add_customers($customer_array);
                if ($customer_id > 0) {
                    $customer_address_array['customer_id'] = $customer_id;
                    $customer_address_id = $this->customers_model->add_customer_address($customer_address_array);
                    echo $customer_name . '-customer inserted';
                    echo '<br>';
                } else {
                    echo $customer_name . '-customer not inserted';
                    echo '<br>';
                }
            } else {
                echo $customer_name . '-area not inserted';
                echo '<br>';
            }
        }
        exit();
    }

    public function odoo_new_customer_add($data = array(), $res)
    {
        $data['customer_type'] = $data['customer_type'] == 'HO' ? 'home' : ($data['customer_type'] == 'OF' ? 'office' : 'warehouse');
        $data['payment_type'] = $data['payment_type'] == 'D' ? 'daily' : ($data['payment_type'] == 'W' ? 'weekly' : 'monthly');
        if ($data['payment_mode'] == 'Cash') {
            $data['payment_mode'] = 'cash';
        } else if ($data['payment_mode'] == 'Cheque') {
            $data['payment_mode'] = 'cheque';
        } else if ($data['payment_mode'] == 'Online') {
            $data['payment_mode'] = 'online';
        } else if ($data['payment_mode'] == 'Credit Card') {
            $data['payment_mode'] = 'credit_card';
        } else if ($data['payment_mode'] == 'Bank transfer') {
            $data['payment_mode'] = 'bank';
        }
        //$data['payment_mode'] = strtolower($data['payment_mode']);
        $data['key_given'] = $data['key_given'] == 'Y' ? 'yes' : 'no';
        $company_type = "person";
        if ($data['is_company'] == 'Y') {
            $company_name = $data['company_name'];
            $get_company_details = $this->customers_model->get_company_details($company_name);
            if (!empty($get_company_details)) {
                $parent_id = (int) $get_company_details->odoo_package_company_id;
            } else {
                $parent_id = null;
            }

        } else if ($data['is_company'] == 'N') {
            $parent_id = null;
        }
        if ($data['customer_booktype'] == 0) {
            $booktype = "nonregular";
        } else if ($data['customer_booktype'] == 1) {
            $booktype = "regular";
        }

        if ($data['apartment'] != "") {
            $apartment = ' , Apt No ' . $data['apartment'];
        } else {
            $apartment = "";
        }
        $addresssss = $data['customer_address'] . $apartment;

        $getarea = $this->settings_model->get_area_details($data['area_id']);

        $post['params']['user_id'] = 1;
        $post['params']['name'] = $data['customer_name'];
        $post['params']['mobile'] = $data['mobile_number_1'];
        $post['params']['company_type'] = $company_type;
        $post['params']['parent_id'] = $parent_id;
        $post['params']['customer_nick_name'] = $data['customer_nick_name'];
        $post['params']['customer_id'] = (int) $res;
        $post['params']['cust_book_type'] = $booktype;
        $post['params']['payment_type'] = $data['payment_type'];
        $post['params']['customer_type'] = $data['customer_type'];
        $post['params']['source'] = $data['customer_source'];
        $post['params']['street'] = $addresssss;
        $post['params']['area_id'] = (int) $getarea[0]['odoo_package_area_id'];
        $post['params']['website'] = $data['website_url'];
        $post['params']['phone'] = $data['phone_number'];
        $post['params']['mobile2'] = $data['mobile_number_2'];
        $post['params']['mobile3'] = $data['mobile_number_3'];
        $post['params']['fax'] = $data['fax_number'];
        $post['params']['email'] = $data['email_address'];
        //$post['params']['zone_id'] = $data['driver_name'];
        $post['params']['comment'] = $data['customer_notes'];
        $post['params']['hourly'] = $data['price_hourly'];
        $post['params']['extra'] = $data['price_extra'];
        $post['params']['weekend'] = $data['price_weekend'];
        $post['params']['latitude'] = $data['latitude'];
        $post['params']['longitude'] = $data['longitude'];
        $post['params']['key'] = $data['key_given'];
        $post['params']['trn_no'] = $data['trnnumber'];
        $post['params']['vat_no'] = $data['vatnumber'];
        $post_values = json_encode($post);

        $url = $this->config->item('odoo_url') . "customer_creation";
        $login_check = curl_api_service($post_values, $url);
        $returnData = json_decode($login_check);

        if ($returnData->result->status == "success") {
            return $odoo_customer_id = $returnData->result->response->id;
        }
    }

    public function odoo_new_customer_write($data = array(), $res, $odooid)
    {
        $data['customer_type'] = $data['customer_type'] == 'HO' ? 'home' : ($data['customer_type'] == 'OF' ? 'office' : 'warehouse');
        $data['payment_type'] = $data['payment_type'] == 'D' ? 'daily' : ($data['payment_type'] == 'W' ? 'weekly' : 'monthly');
        //$data['payment_mode'] = strtolower($data['payment_mode']);
        if ($data['payment_mode'] == 'Cash') {
            $data['payment_mode'] = 'cash';
        } else if ($data['payment_mode'] == 'Cheque') {
            $data['payment_mode'] = 'cheque';
        } else if ($data['payment_mode'] == 'Online') {
            $data['payment_mode'] = 'online';
        } else if ($data['payment_mode'] == 'Credit Card') {
            $data['payment_mode'] = 'credit_card';
        } else if ($data['payment_mode'] == 'Bank transfer') {
            $data['payment_mode'] = 'bank';
        }
        $data['key_given'] = $data['key_given'] == 'Y' ? 'yes' : 'no';
        $company_type = "person";
        if ($data['is_company'] == 'Y') {
            $company_name = $data['company_name'];
            $get_company_details = $this->customers_model->get_company_details($company_name);
            if (!empty($get_company_details)) {
                $parent_id = (int) $get_company_details->odoo_package_company_id;
            } else {
                $parent_id = null;
            }
        } else if ($data['is_company'] == 'N') {
            $parent_id = null;
        }
        $getarea = $this->settings_model->get_area_details($data['area_id']);
        if ($data['customer_booktype'] == 0) {
            $booktype = "nonregular";
        } else if ($data['customer_booktype'] == 1) {
            $booktype = "regular";
        }

        if ($data['apartment'] != "") {
            $apartment = ' , Apt No ' . $data['apartment'];
        } else {
            $apartment = "";
        }
        $addresssss = $data['customer_address'] . $apartment;

        $post['params']['user_id'] = 1;
        $post['params']['name'] = $data['customer_name'];
        $post['params']['mobile'] = $data['mobile_number_1'];
        $post['params']['company_type'] = $company_type;
        $post['params']['parent_id'] = $parent_id;
        $post['params']['customer_nick_name'] = $data['customer_nick_name'];
        $post['params']['customer_id'] = (int) $res;
        $post['params']['id'] = (int) $odooid;
        $post['params']['cust_book_type'] = $booktype;
        $post['params']['payment_type'] = $data['payment_type'];
        $post['params']['customer_type'] = $data['customer_type'];
        $post['params']['source'] = $data['customer_source'];
        $post['params']['street'] = $addresssss;
        $post['params']['area_id'] = (int) $getarea[0]['odoo_package_area_id'];
        $post['params']['website'] = $data['website_url'];
        $post['params']['phone'] = $data['phone_number'];
        $post['params']['mobile2'] = $data['mobile_number_2'];
        $post['params']['mobile3'] = $data['mobile_number_3'];
        $post['params']['fax'] = $data['fax_number'];
        $post['params']['email'] = $data['email_address'];
        //$post['params']['zone_id'] = $data['driver_name'];
        $post['params']['comment'] = $data['customer_notes'];
        $post['params']['hourly'] = $data['price_hourly'];
        $post['params']['extra'] = $data['price_extra'];
        $post['params']['weekend'] = $data['price_weekend'];
        $post['params']['latitude'] = $data['latitude'];
        $post['params']['longitude'] = $data['longitude'];
        $post['params']['key'] = $data['key_given'];
        $post['params']['trn_no'] = $data['trnnumber'];
        $post['params']['vat_no'] = $data['vatnumber'];
        $post_values = json_encode($post);
        //exit();

        $url = $this->config->item('odoo_url') . "customer_edition";
        $login_check = curl_api_service($post_values, $url);
        $returnData = json_decode($login_check);

        if ($returnData->result->status == "success") {
            return $odoo_customer_id = $returnData->result->response->id;
        }
    }

    public function update_customer_odoo()
    {
        // $get_customer_list = $this->customers_model->get_customer_new_odoolist();
        $get_customer_list = $this->customers_model->get_customer_new_packageodoolist();
        // echo '<pre>';
        // print_r($get_customer_list);
        // exit();
        foreach ($get_customer_list as $val) {
            if ($val->odoo_package_customer_status == 0) {
                if ($val->payment_type == 1) {
                    $val->payment_type = 'D';
                } else if ($val->payment_type == 2) {
                    $val->payment_type = 'W';
                } else if ($val->payment_type == 3) {
                    $val->payment_type = 'M';
                }
                $val->customer_type = $val->customer_type == 'HO' ? 'home' : ($val->customer_type == 'OF' ? 'office' : 'warehouse');
                $val->payment_type = $val->payment_type == 'D' ? 'daily' : ($val->payment_type == 'W' ? 'weekly' : 'monthly');
                if ($val->payment_mode == 'Cash') {
                    $val->payment_mode = 'cash';
                } else if ($val->payment_mode == 'Cheque') {
                    $val->payment_mode = 'cheque';
                } else if ($val->payment_mode == 'Online') {
                    $val->payment_mode = 'online';
                } else if ($val->payment_mode == 'Credit Card') {
                    $val->payment_mode = 'credit_card';
                } else if ($val->payment_mode == 'Bank transfer') {
                    $val->payment_mode = 'bank';
                }
                //$val->payment_mode = strtolower($val->payment_mode);
                $val->key_given = $val->key_given == 'Y' ? 'yes' : 'no';
                $company_type = "person";
                if ($val->is_company == 'Y') {
                    $company_name = $val->company_name;
                    $get_company_details = $this->customers_model->get_company_details($company_name);
                    if (!empty($get_company_details)) {
                        // $parent_id = $get_company_details->company_odoo_id;
                        $parent_id = (int) $get_company_details->odoo_package_company_id;
                    } else {
                        $parent_id = null;
                    }
                } else if ($val->is_company == 'N') {
                    $parent_id = null;
                }
                $getarea = $this->settings_model->get_area_details($val->area_id);
                if ($val->customer_booktype == 0) {
                    $booktype = "nonregular";
                } else if ($val->customer_booktype == 1) {
                    $booktype = "regular";
                }

                if ($val->apartment != "") {
                    $apartment = ' , Apt No ' . $val->apartment;
                } else {
                    $apartment = "";
                }
                $addresssss = $val->customer_address . $apartment;

                $post['params']['user_id'] = 1;
                $post['params']['name'] = $val->customer_name;
                $post['params']['mobile'] = $val->mobile_number_1;
                $post['params']['company_type'] = $company_type;
                $post['params']['parent_id'] = (int) $parent_id;
                $post['params']['customer_nick_name'] = $val->customer_nick_name;
                $post['params']['customer_id'] = (int) $val->customer_id;
                $post['params']['cust_book_type'] = $booktype;
                $post['params']['payment_type'] = $val->payment_type;
                $post['params']['customer_type'] = $val->customer_type;
                $post['params']['source'] = $val->customer_source;
                $post['params']['street'] = $addresssss;
                // $post['params']['area_id'] = $getarea[0]['odoo_new_area_id'];
                $post['params']['area_id'] = (int) $getarea[0]['odoo_package_area_id'];
                $post['params']['website'] = $val->website_url;
                $post['params']['phone'] = $val->phone_number;
                $post['params']['mobile2'] = $val->mobile_number_2;
                $post['params']['mobile3'] = $val->mobile_number_3;
                $post['params']['fax'] = $val->fax_number;
                $post['params']['email'] = $val->email_address;
                $post['params']['comment'] = $val->customer_notes;
                $post['params']['hourly'] = $val->price_hourly;
                $post['params']['extra'] = $val->price_extra;
                $post['params']['weekend'] = $val->price_weekend;
                $post['params']['latitude'] = $val->latitude;
                $post['params']['longitude'] = $val->longitude;
                $post['params']['key'] = $val->key_given;
                $post_values = json_encode($post);
                // echo '<pre>';
                // print_r($post_values);

                $url = $this->config->item('odoo_url') . "customer_creation";
                $login_check = curl_api_service($post_values, $url);
                //echo '<br>';
                $returnData = json_decode($login_check);

                if ($returnData->result->status == "success") {
                    $odoo_customer_id = $returnData->result->response->id;
                    // $this->customers_model->update_customers(array('odoo_customer_newid' => $odoo_customer_id,'odoo_customer_new_status' => 1), $val->customer_id);
                    $this->customers_model->update_customers(array('odoo_package_customer_id' => $odoo_customer_id, 'odoo_package_customer_status' => 1), $val->customer_id);
                    echo 'added' . $odoo_customer_id;
                    echo '<br>';
                }
                //exit();
            }

        }
        exit();
        // $old_odoo_id = "15588";
        // $get_customer_details = $this->customers_model->get_customer_details_by_odoo_id($old_odoo_id);

    }

    public function odoo_company_add()
    {
        $post['params']['user_id'] = 1;
        $post['params']['name'] = "Live Out";
        $post['params']['mobile'] = "123456";
        $post['params']['company_type'] = "company";
        $post['params']['customer_nick_name'] = "";
        $post['params']['customer_id'] = "";
        $post['params']['cust_book_type'] = "";
        $post['params']['payment_type'] = "";
        $post['params']['customer_type'] = "";
        $post['params']['source'] = "";
        $post['params']['street'] = "";
        $post['params']['area_id'] = "";
        $post['params']['website'] = "";
        $post['params']['phone'] = "";
        $post['params']['mobile2'] = "";
        $post['params']['mobile3'] = "";
        $post['params']['fax'] = "";
        $post['params']['email'] = "";
        //$post['params']['zone_id'] = $data['driver_name'];
        $post['params']['comment'] = "";
        $post['params']['hourly'] = "";
        $post['params']['extra'] = "";
        $post['params']['weekend'] = "";
        $post['params']['latitude'] = "";
        $post['params']['longitude'] = "";
        $post['params']['key'] = "";
        $post['params']['trn_no'] = "";
        $post['params']['vat_no'] = "";
        $post_values = json_encode($post);

        $url = $this->config->item('odoo_url') . "customer_creation";
        echo $login_check = curl_api_service($post_values, $url);
        $returnData = json_decode($login_check);

        if ($returnData->result->status == "success") {
            echo $odoo_company_id = $returnData->result->response->id;
            // $this->customers_model->update_partnercompany(array('company_odoo_id' => $odoo_company_id), 8);
            $this->customers_model->update_partnercompany(array('odoo_package_company_id' => $odoo_company_id, 'odoo_package_company_status' => 1), 10);
        } else {
            echo "Fail";
        }
        exit();
    }

    public function get_customer_not_active()
    {
        $custDet = $this->customers_model->getallactivecustomers();

        echo '<table>';
        foreach ($custDet as $det) {
            $last_jobdate = $this->customers_model->get_last_job_date_by_customerid_new($det->customer_id);

            if ($last_jobdate->service_date < '2022-12-07') {
                echo '<tr><td>' . $det->customer_name . '</td><td>' . $det->mobile_number_1 . '</td><td>' . $det->email_address . '</td><td>' . $last_jobdate->service_date . '</td></tr>';

            }
            // echo $i.'.'.$last_jobdate->service_date;
            // echo '<br>';
            //$i++;
        }
        echo '</table>';

        // echo '<pre>';
        // print_r($custDet);
        exit();
    }
    public function get_address_by_id()
    {
        // use in ajax call only
        header('Content-Type: application/json; charset=utf-8');
        $address_id = $this->uri->segment(3);
        $data = $this->customers_model->get_customer_address_by_id($address_id);
        echo json_encode($data, JSON_PRETTY_PRINT);
    }
    public function list_customer_payments()
	{
		$data = array();
		$date_from = NULL;
        $date_to = NULL;
		$customer_id = NULL;
		$company_vh_rep = $this->input->post('company_vh_rep')!=''?$this->input->post('company_vh_rep'):NULL;
		$type_vh_rep = $this->input->post('type_vh_rep')!=''?$this->input->post('type_vh_rep'):NULL;
		$data['search_date_from'] = NULL;
		$data['search_date_to'] = NULL;
		
		if($this->input->post())
		{
			if($this->input->post('customers_vh_rep') > 0)
			{
				$customer_id = $this->input->post('customers_vh_rep');
			} else {
				$customer_id = NULL;
			}
			if($this->input->post('vehicle_date') != "")
			{
				$from_date = $this->input->post('vehicle_date');
				$date_from = date('Y-m-d', strtotime(str_replace("/", "-", $from_date)));
				$data['search_date_from'] = date('d/m/Y', strtotime($date_from));
			} else {
				$date_from = NULL;
				$data['search_date_from'] = NULL;
			}
			
			if($this->input->post('vehicle_date_to') != "")
			{
				$to_date = $this->input->post('vehicle_date_to');
				$date_to = date('Y-m-d', strtotime(str_replace("/", "-", $to_date)));
				$data['search_date_to'] = date('d/m/Y', strtotime($date_to));
			} else {
				$date_to = NULL;
				$data['search_date_to'] = NULL;
			}
		}
		$data['customer_id'] = $customer_id;
		$data['company_vh_rep'] = $company_vh_rep;
		$data['type_vh_rep'] = $type_vh_rep;
        
        $data['payment_report']=$this->customers_model->get_customer_payments($date_from,$date_to,$customer_id,$company_vh_rep,$type_vh_rep);
        $data['customerlist'] = $this->invoice_model->get_customer_list();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('customer_payment_list', $data, TRUE);
        $layout_data['page_title'] = 'Customer Payments';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css','toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js','toastr.min.js');
        $this->load->view('layouts/default', $layout_data);
	}
    public function edit_payment()
    {
		$settingss = $this->settings_model->get_settings();
		$update_to_quickbook = array();
		$update_to_quickbook['status'] = '';
        $payment_id = $this->uri->segment(3);
        if($this->input->post('update_payment')){
			$quickbookCustomer_id = trim($this->input->post('quickbook_id'));
			$quickbookPayment_id = trim($this->input->post('quickbook_pay_id'));
			$quickbookPaymentSync_id = trim($this->input->post('quickbook_paysync_id'));
            $update_data = array(
                'paid_amount'=> $this->input->post('collected_amount'),
                'balance_amount'=> $this->input->post('collected_amount'),
                'payment_method'=> $this->input->post('paymenttype'),
                'paid_datetime'=> $this->db->escape(DateTime::createFromFormat('d/m/Y', $this->input->post('collect_date'))->format('Y-m-d')),
                'ps_no'=> $this->input->post('memo'),
                'receipt_no'=> $this->input->post('memo'),
                'pay_description'=> $this->db->escape($this->input->post('description')),
            );
            if($this->customers_model->update_customer_payment(array('payment_id'=>$payment_id),$update_data) >= 0){
                //redirect('customer/edit_payment/'.$payment_id);
				$errors['class'] = 'success';
                $errors['message'] = '<strong>Success!</strong> Payment updated Successfully.';
				// if($settingss->enableQuickBook == '1')
				// {
					// if($quickbookCustomer_id > 0)
					// {
						// if($quickbookPayment_id > 0)
						// {
							// $update_to_quickbook = $this->quickbook_update_customer_payment($update_data,$settingss,$quickbookCustomer_id,$quickbookPayment_id,$quickbookPaymentSync_id,$payment_id);
						// } else {
							// $update_to_quickbook = $this->quickbook_new_customer_payment($update_data,$settingss,$payment_id,$quickbookCustomer_id);
						// }
					// } else {
						// $update_to_quickbook = array();
						// $update_to_quickbook['status'] = 'error';
						// $update_to_quickbook['message'] = 'Quickbooks sync failed. Customer not available in quickbooks.';
					// }
				// } else {
					// $update_to_quickbook = array();
					// $update_to_quickbook['status'] = '';
				// }
            }
            else{
                $errors['class'] = 'warning';
                $errors['message'] = '<strong>Error!</strong> Something went wrong!';
				$update_to_quickbook = array();
				$update_to_quickbook['status'] = '';
            }
        }
        $data = array();
		$data['quickbookmessage'] = $update_to_quickbook;
        $data['errors'] = $errors;
        $data['collected_date'] = date('d/m/Y');
        //$data['customers'] = $this->customers_model->get_customers();
        $data['payment'] = $this->customers_model->get_customer_payment_byid($payment_id);
        $data['customer'] = $this->customers_model->get_customer($data['payment']->customer_id);
        $data['settings'] = $layout_data['settings'] = $settingss;
        $layout_data['content_body'] = $this->load->view('edit_payment', $data, TRUE);
        $layout_data['page_title'] = 'Edit Payment';
        $layout_data['meta_description'] = 'Edit Payment';
        $layout_data['css_files'] = array('demo.css', 'datepicker.css','toastr.min.css');
        $layout_data['external_js_files'] = array(); 
        $layout_data['js_files'] = array('bootstrap-datepicker.js','mymaids.js','toastr.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    public function add_payment()
    {
		$settingss = $this->settings_model->get_settings();
		$update_to_quickbook = array();
		$update_to_quickbook['status'] = '';
		if($this->input->is_ajax_request())
        {
                if($this->input->post('action') && $this->input->post('action') == 'get-outstanding-amount')
                {
                    if($this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0)
                    {
                        $customer_id = $this->input->post('customer_id');
                        
                        $customer_balance = $this->customers_model->get_outstanding_balance($customer_id);
                        
                        $response = array();
                        $response['status'] = 'success';
                        $response['balance'] = $customer_balance->amount ? $customer_balance->amount : 0;
                        $response['quickbookid'] = $customer_balance->quickbook_id ? $customer_balance->quickbook_id : 0;
                        
                        echo json_encode($response);
                        exit();
                    }
                    else
                    {
                        $response = array();
                        $response['status'] = 'error';
                        $response['message'] = 'Please select customer';
                        
                        echo json_encode($response);
                        exit();
                    }
                }
        }
        $errors = array();
        if($this->input->post('add_payment'))
        {
			
            $customer_id = $this->input->post('customer_id');
            $quickbook_customer_id = $this->input->post('quickbook_id');
			$requested_amount = $this->input->post('collected_amount');
			$payment_type = $this->input->post('paymenttype');
			$category = $this->input->post('paymentcategory');
			$memo = $this->input->post('memo');
			$description = trim($this->input->post('description'));
            $collected_date = date("Y-m-d", strtotime(str_replace("/", "-", trim($this->input->post('collect_date')))));
			
			if($category == "CM")
			{
				$memo_array = array();
				$memo_array['customer_id'] = $customer_id;
				$memo_array['memo_date'] = $collected_date;
				$memo_array['memo_reference'] = $memo;
				$memo_array['memo_description'] = $description;
				$memo_array['amount'] = $requested_amount;
				$memo_array['created_by'] = user_authenticate();
				$memo_array['created_at'] = date('Y-m-d H:i:s');
				
				$memo_id = $this->customers_model->add_customer_credit_memo($memo_array);
			} else {
				$memo_id = NULL;
			}
            
            $add_payment_fields = array();
            $add_payment_fields['customer_id'] = $customer_id;
            $add_payment_fields['paid_amount'] = $requested_amount;
            $add_payment_fields['balance_amount'] = $requested_amount;
            $add_payment_fields['payment_method'] = $payment_type;
            $add_payment_fields['payment_type'] = $category;
            $add_payment_fields['paid_at'] = 'P';
			$add_payment_fields['paid_at_id'] = user_authenticate();
			$add_payment_fields['paid_datetime'] = $collected_date;
			$add_payment_fields['createdAt'] = date('Y-m-d H:i:s');
			$add_payment_fields['ps_no'] = $memo;
			$add_payment_fields['receipt_no'] = $memo;
			$add_payment_fields['pay_description'] = $description;
			$add_payment_fields['creditMemoId'] = $memo_id;
			$add_payment_fields['verified_status'] = 1;
            
            $add_payment_id = $this->customers_model->add_customer_payment($add_payment_fields);
			
            if($add_payment_id > 0)
            {                
                $errors['class'] = 'success';
                $errors['message'] = '<strong>Success!</strong> Payment added Successfully.';
				
				if($category == "CM")
				{
					if($settingss->enableQuickBook == '1')
					{
						if($quickbook_customer_id > 0)
						{
							// $update_to_quickbook = $this->quickbook_new_customer_payment($add_payment_fields,$settingss,$add_payment_id,$quickbook_customer_id);
							$update_to_quickbook = $this->quickbook_new_customer_payment_memo($add_payment_fields,$settingss,$add_payment_id,$quickbook_customer_id,$memo_id);
						} else {
							$update_to_quickbook = array();
							$update_to_quickbook['status'] = 'error';
							$update_to_quickbook['message'] = 'Quickbooks sync failed. Customer not available in quickbooks.';
						}
					} else {
						$update_to_quickbook = array();
						$update_to_quickbook['status'] = '';
					}
				}
				// if($settingss->enableQuickBook == '1')
				// {
					// if($quickbook_customer_id > 0)
					// {
						// $update_to_quickbook = $this->quickbook_new_customer_payment($add_payment_fields,$settingss,$add_payment_id,$quickbook_customer_id);
					// } else {
						// $update_to_quickbook = array();
						// $update_to_quickbook['status'] = 'error';
						// $update_to_quickbook['message'] = 'Quickbooks sync failed. Customer not available in quickbooks.';
					// }
				// } else {
					// $update_to_quickbook = array();
					// $update_to_quickbook['status'] = '';
				// }
            }
            else
            {
                $errors['class'] = 'warning';
                $errors['message'] = '<strong>Error!</strong> Something went wrong!';
            }
            
        }
		
		$data = array();
		$data['quickbookmessage'] = $update_to_quickbook;
        $data['errors'] = $errors;
        $data['collected_date'] = date('d/m/Y');
        $data['customers'] = $this->customers_model->get_customers();
        $data['settings'] = $layout_data['settings'] = $settingss;
        $layout_data['content_body'] = $this->load->view('add_payment', $data, TRUE);
        $layout_data['page_title'] = 'Add Payment';
        $layout_data['meta_description'] = 'Add Payment';
        $layout_data['css_files'] = array('demo.css', 'datepicker.css','toastr.min.css');
        $layout_data['external_js_files'] = array(); 
        $layout_data['js_files'] = array('bootstrap-datepicker.js','mymaids.js','toastr.min.js');

        $this->load->view('layouts/default', $layout_data);
	}
    public function view_payment($pay_id)
	{
		$payment_detail = $this->customers_model->get_customer_payment_byid($pay_id);
		$get_non_posted_invoices = $this->customers_model->get_non_posted_invoices($payment_detail->customer_id,$payment_detail->payment_type);
		$get_allocated_invoices = $this->customers_model->get_allocated_invoices($pay_id,$payment_detail->payment_type);
		// echo '<pre>';
		// print_r($get_allocated_invoices);
		// exit();
		$data = array();
		$data['payment_detail'] = $payment_detail;
		$data['invoice_detail'] = $get_non_posted_invoices;
		$data['allocated_invoices'] = $get_allocated_invoices;
		$data['payid'] = $pay_id;
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('view_payment', $data, TRUE);
        $layout_data['page_title'] = 'View Payment';
        $layout_data['meta_description'] = 'View Payment';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css','jquery.fancybox.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.fancybox.pack.js','ajaxupload.3.5.js','bootstrap-datepicker.js','base.js','jquery.validate.min.js', 'mymaids.js', 'moment.min.js','jquery.dataTables.min.js', 'bootstrap.js'); //'hm.js');
        $this->load->view('layouts/default_dashboard', $layout_data);
    }
    public function online_payments()
	{
		$data = array();
		$date_from = NULL;
        $date_to = NULL;
		//$customer_id = NULL;
		$data['search_date_from'] = NULL;
		$data['search_date_to'] = NULL;
		
		if($this->input->post())
		{
			// if($this->input->post('customers_vh_rep') > 0)
			// {
				// $customer_id = $this->input->post('customers_vh_rep');
			// } else {
				// $customer_id = NULL;
			// }
			if($this->input->post('vehicle_date') != "")
			{
				$from_date = $this->input->post('vehicle_date');
				$date_from = date('Y-m-d', strtotime(str_replace("/", "-", $from_date)));
				$data['search_date_from'] = date('d/m/Y', strtotime($date_from));
			} else {
				$date_from = NULL;
				$data['search_date_from'] = NULL;
			}
			
			if($this->input->post('vehicle_date_to') != "")
			{
				$to_date = $this->input->post('vehicle_date_to');
				$date_to = date('Y-m-d', strtotime(str_replace("/", "-", $to_date)));
				$data['search_date_to'] = date('d/m/Y', strtotime($date_to));
			} else {
				$date_to = NULL;
				$data['search_date_to'] = NULL;
			}
		}
		// $data['customer_id'] = $customer_id;
        
        // $data['payment_report']=$this->customers_model->get_customer_payments($date_from,$date_to,$customer_id);
        $data['payment_report']=$this->customers_model->get_online_payments($date_from,$date_to);
        // $data['customerlist'] = $this->invoice_model->get_customer_list();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('online_payment_list', $data, TRUE);
        $layout_data['page_title'] = 'Online Payments';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
	}
    public function allocate()
    {
        $invoice_ids = $this->input->post('ids');
        $invoiceamts = $this->input->post('payvall');
        $invrefval = $this->input->post('refVal');
        $paid_amt = $this->input->post('payamt');
        $payid = $this->input->post('payid');
        
        $invoicearray = explode(',',$invoice_ids);
        
        // echo '<pre>';
        // print_r($invoicearray);
        // exit();
        $result=array();
        $allocate_amt = $paid_amt;
        if($allocate_amt != 0)
        {
            $qbsync_msg='';
            foreach ($invoicearray as $invoice_id)
            {
                $get_invoice_byid = $this->invoice_model->get_invoice_detailbyid($invoice_id);
                
                $invoiceamount = $get_invoice_byid[0]->balance_amount;
                if($paid_amt < $invoiceamount)
                {
                    $r_id = (1000+$payid);
                    $map_array = array();
                    $addeddatetime=date('Y-m-d H:i:s');
                    $map_array['pay_reference'] = 'CUST.IN-'.date("Y").'-'.$r_id;
                    $map_array['inv_reference'] = $get_invoice_byid[0]->invoice_num;
                    $map_array['inv_customer_id'] = $get_invoice_byid[0]->custid;
                    $map_array['paymentId'] = $payid;
                    $map_array['invoiceId'] = $invoice_id;
                    $map_array['payment_openingbal'] = $get_invoice_byid[0]->balance;
                    $map_array['payment_amount'] = $paid_amt;
                    $map_array['payment_closing_bal'] = ($get_invoice_byid[0]->balance - $paid_amt);
                    $map_array['added_user'] = user_authenticate();
                    $map_array['payment_added_datetime'] = $addeddatetime;
                    
                    $insert_map_id = $this->invoice_model->add_payment_invoice($map_array);
                    if($insert_map_id > 0)
                    {
                        $pay_ref = 'CUST.IN-'.date("Y").'-'.$r_id;
                        $cust_balance = $get_invoice_byid[0]->balance;
                        $payment_status = 1;
                        
                        $get_current_allocateamt = $this->invoice_model->get_current_allocateamt($payid);
                        
                        $update_cust_array = array();
                        $update_cust_array['payment_reference'] = $pay_ref;
                        $update_cust_array['allocated_amount'] = ($get_current_allocateamt->allocated_amount + $paid_amt);
                        $update_cust_array['balance_amount'] = 0;
                        $update_cust_array['payment_status'] = 1;
                        
                        $update_customer_array = array();
                        $update_customer_array['total_paid_amount'] = ($get_invoice_byid[0]->total_paid_amount + $paid_amt);
                        $update_customer_array['balance'] = ($get_invoice_byid[0]->balance - $paid_amt);
                        
                        $update_invoice_array = array();
                        $update_invoice_array['invoice_paid_status'] = 2;
                        $update_invoice_array['received_amount'] = ($get_invoice_byid[0]->received_amount + $paid_amt);
                        $update_invoice_array['balance_amount'] = ($get_invoice_byid[0]->balance_amount - $paid_amt);
                        
                        $this->invoice_model->update_customerpay_detail($payid,$update_cust_array);
                        $this->customers_model->update_customers($update_customer_array,$get_invoice_byid[0]->customer_id);
                        $this->invoice_model->update_invoicepay_detail($invoice_id,$update_invoice_array);
                        //echo 'success';
                        $qb_sync_msg='';
						// if($get_invoice_byid[0]->invoice_qb_sync_stat==1&&$get_invoice_byid[0]->invoice_qb_id>0&&$get_invoice_byid[0]->quickbook_id>0)
                        // {
                           // $qb_sync_stat=$this->add_payment_to_qb($paid_amt,$get_invoice_byid[0]->quickbook_id,$get_invoice_byid[0]->invoice_qb_id,$addeddatetime); 
                           // if($qb_sync_stat!='success')
                           // {
                             // $qb_sync_msg=$this->get_qb_payment_msg($qb_sync_stat);
                           // }
                           // else
                           // {  $qb_sync_msg='';}
                           

                        // }
                        $result['status']='success';
                        $result['msg']=$qb_sync_msg;
                        print_r(json_encode($result));
                        exit();
                    }
                } else if($paid_amt == $invoiceamount)
                {
                    $r_id = (1000+$payid);
                    $map_array = array();
                    $addeddatetime=date('Y-m-d H:i:s');
                    $map_array['pay_reference'] = 'CUST.IN-'.date("Y").'-'.$r_id;
                    $map_array['inv_reference'] = $get_invoice_byid[0]->invoice_num;
                    $map_array['inv_customer_id'] = $get_invoice_byid[0]->customer_id;
                    $map_array['paymentId'] = $payid;
                    $map_array['invoiceId'] = $invoice_id;
                    $map_array['payment_openingbal'] = $get_invoice_byid[0]->balance;
                    $map_array['payment_amount'] = $paid_amt;
                    $map_array['payment_closing_bal'] = ($get_invoice_byid[0]->balance - $paid_amt);
                    $map_array['added_user'] = user_authenticate();
                    $map_array['payment_added_datetime'] = $addeddatetime;
                    
                    $insert_map_id = $this->invoice_model->add_payment_invoice($map_array);
                    if($insert_map_id > 0)
                    {
                        $pay_ref = 'CUST.IN-'.date("Y").'-'.$r_id;
                        $cust_balance = $get_invoice_byid[0]->balance;
                        $payment_status = 1;
                        $get_current_allocateamt = $this->invoice_model->get_current_allocateamt($payid);
                        
                        $update_cust_array = array();
                        $update_cust_array['payment_reference'] = $pay_ref;
                        $update_cust_array['allocated_amount'] = ($get_current_allocateamt->allocated_amount + $paid_amt);
                        $update_cust_array['balance_amount'] = 0;
                        $update_cust_array['payment_status'] = 1;
                        
                        $update_customer_array = array();
                        $update_customer_array['total_paid_amount'] = ($get_invoice_byid[0]->total_paid_amount + $paid_amt);
                        $update_customer_array['balance'] = ($get_invoice_byid[0]->balance - $paid_amt);
                        
                        $update_invoice_array = array();
                        $update_invoice_array['invoice_status'] = 3;
                        $update_invoice_array['invoice_paid_status'] = 1;
                        $update_invoice_array['received_amount'] = ($get_invoice_byid[0]->received_amount + $paid_amt);
                        $update_invoice_array['balance_amount'] = ($get_invoice_byid[0]->balance_amount - $paid_amt);
                        
                        $this->invoice_model->update_customerpay_detail($payid,$update_cust_array);
                        $this->customers_model->update_customers($update_customer_array,$get_invoice_byid[0]->customer_id);
                        $this->invoice_model->update_invoicepay_detail($invoice_id,$update_invoice_array);
                        //echo 'success';
                        $qb_sync_msg='';
						// if($get_invoice_byid[0]->invoice_qb_sync_stat==1&&$get_invoice_byid[0]->invoice_qb_id>0&&$get_invoice_byid[0]->quickbook_id>0)
                        // {
                           // $qb_sync_stat=$this->add_payment_to_qb($paid_amt,$get_invoice_byid[0]->quickbook_id,$get_invoice_byid[0]->invoice_qb_id,$addeddatetime); 
                           // if($qb_sync_stat!='success')
                           // {
                             // $qb_sync_msg=$this->get_qb_payment_msg($qb_sync_stat);
                           // }
                           // else
                           // {  $qb_sync_msg='';}
                        // }
                        $result['status']='success';
                        $result['msg']=$qb_sync_msg;
                        print_r(json_encode($result));
                        exit();
                    }
                } else if($paid_amt > $invoiceamount){
                    $r_id = (1000+$payid);
                    $map_array = array();
                    $addeddatetime=date('Y-m-d H:i:s');
                    $map_array['pay_reference'] = 'CUST.IN-'.date("Y").'-'.$r_id;
                    $map_array['inv_reference'] = $get_invoice_byid[0]->invoice_num;
                    $map_array['inv_customer_id'] = $get_invoice_byid[0]->customer_id;
                    $map_array['paymentId'] = $payid;
                    $map_array['invoiceId'] = $invoice_id;
                    $map_array['payment_openingbal'] = $get_invoice_byid[0]->balance;
                    $map_array['payment_amount'] = $invoiceamount;
                    $map_array['payment_closing_bal'] = ($get_invoice_byid[0]->balance - $invoiceamount);
                    $map_array['added_user'] = user_authenticate();
                    $map_array['payment_added_datetime'] = $addeddatetime;
                    
                    $insert_map_id = $this->invoice_model->add_payment_invoice($map_array);
                    if($insert_map_id > 0)
                    {
                        $pay_ref = 'CUST.IN-'.date("Y").'-'.$r_id;
                        $cust_balance = $get_invoice_byid[0]->balance;
                        $payment_status = 1;
                        $get_current_allocateamt = $this->invoice_model->get_current_allocateamt($payid);
                        
                        $update_cust_array = array();
                        $update_cust_array['payment_reference'] = $pay_ref;
                        $update_cust_array['allocated_amount'] = ($get_current_allocateamt->allocated_amount + $invoiceamount);
                        $update_cust_array['balance_amount'] = ($paid_amt - $invoiceamount);
                        $update_cust_array['payment_status'] = 2;
                        
                        $update_customer_array = array();
                        $update_customer_array['total_paid_amount'] = ($get_invoice_byid[0]->total_paid_amount + $invoiceamount);
                        $update_customer_array['balance'] = ($get_invoice_byid[0]->balance - $invoiceamount);
                        
                        $update_invoice_array = array();
                        $update_invoice_array['invoice_status'] = 3;
                        $update_invoice_array['invoice_paid_status'] = 1;
                        $update_invoice_array['received_amount'] = ($get_invoice_byid[0]->received_amount + $invoiceamount);
                        $update_invoice_array['balance_amount'] = ($get_invoice_byid[0]->balance_amount - $invoiceamount);
                        
                        $this->invoice_model->update_customerpay_detail($payid,$update_cust_array);
                        $this->customers_model->update_customers($update_customer_array,$get_invoice_byid[0]->customer_id);
                        $this->invoice_model->update_invoicepay_detail($invoice_id,$update_invoice_array);
                        //echo 'success';
						$qb_sync_msg='';
                        // if($get_invoice_byid[0]->invoice_qb_sync_stat==1&&$get_invoice_byid[0]->invoice_qb_id>0&&$get_invoice_byid[0]->quickbook_id>0)
                        // {
                           // $qb_sync_stat=$this->add_payment_to_qb($invoiceamount,$get_invoice_byid[0]->quickbook_id,$get_invoice_byid[0]->invoice_qb_id,$addeddatetime); 
                           // if($qb_sync_stat!='success')
                           // {
                             // $qb_sync_msg.="Invoice ID - ".$get_invoice_byid[0]->invoice_id." , Error - ".$this->get_qb_payment_msg($qb_sync_stat).'<br>';
                           // }
                           // else
                           // {  $qb_sync_msg.='';}
                        // }
                        
                    }
                    $paid_amt = ($paid_amt - $invoiceamount);
                }
            }
            //echo 'success';
            $result['status']='success';
            if($qb_sync_msg!=''){$qb_sync_msg='Quickbook Sync error <br> '.$qb_sync_msg;}
            $result['msg']=$qb_sync_msg;
            print_r(json_encode($result));
            exit();
            
        } else {
            //echo 'success';
            $result['status']='success';
            $result['msg']='';
            print_r(json_encode($result));
            exit();
        }
    }
    public function view_customer_statement()
    {
        $this->load->library('pdf');
        $data = array();
        $statementtype = $this->uri->segment(4);
        $startdate = $this->uri->segment(5);
        $customer_id = $this->uri->segment(3);
        if($statementtype == 1)
        {
            $get_startingbalance = $this->customers_model->getinitialbal($customer_id);
            
            $get_customer_statement = $this->customers_model->get_customer_statement($customer_id);
            
            //usort($get_customer_statement, "date_compare");
            usort($get_customer_statement, array($this, 'date_compare'));
            
            if(!empty($get_startingbalance))
            {
                $data['initial_balance'] = $get_startingbalance->initial_balance;
                $data['initial_bal_sign'] = $get_startingbalance->initial_bal_sign;
                $data['initial_bal_date'] = $get_startingbalance->initial_bal_date;
                $data['customername'] = $get_startingbalance->customer_name;
                $data['address'] = $get_startingbalance->customer_address;
            } else {
                $data['initial_balance'] = 0;
                $data['initial_bal_sign'] = 'Dr';
                $data['initial_bal_date'] = '';
                $data['customername'] = "";
                $data['address'] = "";
            }
            $data['customer_statement'] = $get_customer_statement;
        } else {
            $data['initial_balance'] = 0;
            $data['initial_bal_sign'] = 'Dr';
            $data['initial_bal_date'] = '';
            $data['customername'] = "";
            $data['address'] = "";
            $data['customer_statement'] = array();
        }
        
        $html_content = $this->load->view('statementpdf', $data, TRUE);
        $this->pdf->loadHtml($html_content);
        $this->pdf->set_paper("a4", "portrait");
        $this->pdf->render();
        $this->pdf->stream("".$invoiceid.".pdf", array("Attachment"=>0));
        exit();
        
        
    }
    

    public function view_customer_statement_new()
    {
		// error_reporting(E_ALL);
// ini_set('display_errors', '1');
        $data = array();
        $this->load->library('pdf');
        $customer_id = $this->uri->segment(3);
        $statementtype = $this->uri->segment(4);
        $startdate = $this->uri->segment(5);
		$enddate = $this->uri->segment(6);
        if($statementtype == 1)
        {
            $get_startingbalance = $this->customers_model->getinitialbal($customer_id);
            
            // $get_customer_statement = $this->customers_model->get_customer_statement_new($customer_id,$startdate,$enddate);
            $get_customer_statement = $this->customers_model->get_customer_statement_new_opening($customer_id);
			
            //usort($get_customer_statement, "date_compare");
            usort($get_customer_statement, array($this, 'date_compare'));
			// echo '<pre>';
			// print_r($get_customer_statement);
			// exit();
            
            if(!empty($get_startingbalance))
            {
                $data['initial_balance'] = $get_startingbalance->initial_balance;
                $data['initial_bal_sign'] = $get_startingbalance->initial_bal_sign;
                $data['initial_bal_date'] = $get_startingbalance->initial_bal_date;
                $data['customername'] = $get_startingbalance->customer_name;
                $data['address'] = $get_startingbalance->customer_address;
            } else {
                $data['initial_balance'] = 0;
                $data['initial_bal_sign'] = 'Dr';
                $data['initial_bal_date'] = '';
                $data['customername'] = "";
                $data['address'] = "";
            }
            $data['customer_statement'] = $get_customer_statement;
        } else if($statementtype == 2)
		{
            /************************************************ */
            $statement_all = $this->customers_model->get_customer_statement_new_opening($customer_id);
            $statement_before = [];
            $data['total_credit_before_start_date'] = 0;
            $data['total_debit_before_start_date'] = 0;
            foreach($statement_all as $key => $statement){
                if($statement->dateval < $startdate){
                    $statement_before[] = $statement;
                    if($statement->stattype == 'A'){
                        $data['total_debit_before_start_date'] += $statement->amount;
                    }
                    else{
                        $data['total_credit_before_start_date'] += $statement->amount;
                    }      
                }
            }
            //echo'<pre>';var_dump($statement_before);echo'</pre>';die();
            /************************************************ */
			$get_startingbalance = $this->customers_model->getinitialbal($customer_id);
            // $get_customer_statement = $this->customers_model->get_customer_statement_new($customer_id,$startdate,$enddate);
            $get_customer_statement = $this->customers_model->get_customer_statement_new_opening_dates($customer_id,$startdate,$enddate);
            //usort($get_customer_statement, "date_compare");
            usort($get_customer_statement, array($this, 'date_compare'));
			// echo '<pre>';
			// print_r($get_customer_statement);
			// exit();
            
            if(!empty($get_startingbalance))
            {
                $data['initial_balance'] = $get_startingbalance->initial_balance;
                $data['initial_bal_sign'] = $get_startingbalance->initial_bal_sign;
                $data['initial_bal_date'] = $get_startingbalance->initial_bal_date;
                $data['customername'] = $get_startingbalance->customer_name;
                $data['address'] = $get_startingbalance->customer_address;
            } else {
                $data['initial_balance'] = 0;
                $data['initial_bal_sign'] = 'Dr';
                $data['initial_bal_date'] = '';
                $data['customername'] = "";
                $data['address'] = "";
            }
            $data['customer_statement'] = $get_customer_statement;
		} else {
            $data['initial_balance'] = 0;
            $data['initial_bal_sign'] = 'Dr';
            $data['initial_bal_date'] = '';
            $data['customername'] = "";
            $data['address'] = "";
            $data['customer_statement'] = array();
        }
        //echo'<pre>';var_dump($data);echo'</pre>';die();
        $data['settings'] = $this->settings_model->get_settings();
        $html_content = $this->load->view('statementpdf', $data, TRUE);
        $this->pdf->loadHtml($html_content);
        $this->pdf->set_paper("a4", "portrait");
        $this->pdf->render();
        $this->pdf->stream("Statement ".$data['customername']." ".time().".pdf", array("Attachment"=>0));
        exit();
    }
	
	function date_compare($element1, $element2) {
		$datetime1 = strtotime($element1->dateval);
		$datetime2 = strtotime($element2->dateval);
		return $datetime1 - $datetime2;
	} 

    function customer_payment_update(){
        if($this->input->is_ajax_request())
        {
            if($this->input->post('action') == 'delete-customer-payment'){
                $safe = is_safe_to_delete_customer_payment($this->input->post('id'));
                if($safe === true){
                    if($this->customers_model->update_customer_payment(array('payment_id'=>$this->input->post('id')),array('deleted_by'=>user_authenticate(),'deleted_at'=>'NOW()'))){
                        die(json_encode(array('status'=>true,'message'=>'Payment deleted successfully !','type'=>'success')));
                    }
                    else{
                        die(json_encode(array('status'=>false,'message'=>'Delete action failed !','type'=>'error')));
                    }
                }
                else{
                    die(json_encode(array('status'=>false,'message'=>$safe,'type'=>'error')));
                }
            }
            else{
                die(json_encode(array('status'=>false,'message'=>'Update action not specified !','type'=>'error')));
            }
        }
    }
	
	public function customer_list_outstanding()
    {
        $data = array();
        $data['active'] = 2;
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
		$data['customerlist'] = $this->invoice_model->get_customer_list();
        $layout_data['content_body'] = $this->load->view('customer_list_outstanding', $data, TRUE);
        $layout_data['page_title'] = 'Customer Outstanding Report';
        $layout_data['meta_description'] = 'Customer Outstanding Report';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('bootstrap-datepicker.js', 'jquery.dataTables.min.js', 'customerout.js');
        $this->load->view('layouts/default', $layout_data);
    }
	
	public function customer_outstanding_report()
    {
        $sql = "select c.customer_id, c.customer_name, a.area_name, ca.customer_address_id, c.mobile_number_1, CASE WHEN c.payment_type = 'D' THEN 'Daily' ELSE (CASE WHEN c.payment_type = 'M' THEN 'Monthly' ELSE (CASE WHEN c.payment_type = 'MA' THEN 'Monthly Advance' ELSE 'Unknown' END) END) END as payment_type, ci.last_invoice_date, ci.invoice_net_amount, cp.paid_amount, (ci.invoice_net_amount - cp.paid_amount) as balance from customers as c left join customer_addresses as ca on c.customer_id = ca.customer_id and ca.default_address = 1 left join areas as a on ca.area_id = a.area_id left join (select i.customer_id, SUM(i.invoice_net_amount) as invoice_net_amount, MAX(i.invoice_date) as last_invoice_date from invoice as i where i.invoice_status not in (0, 2) and i.showStatus = 1 group by i.customer_id) as ci on c.customer_id = ci.customer_id inner join (select cp.customer_id, SUM(cp.paid_amount) as paid_amount from customer_payments as cp left join (select i.customer_id, SUM(i.invoice_net_amount) as invoice_net_amount, MAX(i.invoice_date) as last_invoice_date from invoice as i where i.invoice_status not in (0, 2) and i.showStatus = 1 group by i.customer_id) as ci on cp.customer_id = ci.customer_id where cp.deleted_at is null and cp.show_status = 1 group by cp.customer_id) as cp on c.customer_id = cp.customer_id where (ci.invoice_net_amount - cp.paid_amount) > 0 and c.customer_status = 1 group by c.customer_id order by balance desc";
        $query = $this->db->query($sql);
        $data = array();
        $data['report_rows'] = $query->result_array();
        //var_dump($data['report_rows']);die();
        $data['active'] = 2;
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $data['customerlist'] = $this->invoice_model->get_customer_list();
        $layout_data['content_body'] = $this->load->view('reports/customer_outstanding_report', $data, TRUE);
        $layout_data['page_title'] = 'Customer Outstanding Report';
        $layout_data['meta_description'] = 'Customer Outstanding Report';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('bootstrap-datepicker.js', 'jquery.dataTables.min.js', 'customer-outstanding-report.js');
        $this->load->view('layouts/default', $layout_data);
    }

    public function list_ajax_customer_list_outstanding()
    {
        $orders = array();
        $draw = $_POST['draw'];
        $start = $_POST['start'];
        $rowperpage = $_POST['length'];
        //$columnIndex = isset($_POST['order'][0]['column'])?$_POST['order'][0]['column']:"";
        $columnName = 'customer_id';//$_POST['columns'][$columnIndex]['data'];
        $columnSortOrder = 'asc';//$_POST['order'][0]['dir'];
        $searchValue = $_POST['search']['value'];

        // Custom search filter 
        $useractive = $_POST['useractive'];
        $regdate = $_POST['regdate'];
        $regdateto = $_POST['regdateto'];
        $custselect = $_POST['custselect'];

        $recordsTotal = $this->customers_model->count_all_customers_outstanding();
        $recordsTotalFilter = $this->customers_model->get_all_customers_outstanding($useractive, $regdate, $regdateto, $custselect);

        /************************************************ */
        // get sort data from datatable
        $sort_column = $_POST['columns'][$_POST['order'][0]['column']]['data'];
        $sort_order= $_POST['order'][0]['dir'];
        /************************************************ */

        $orders = $this->customers_model->get_all_newcustomers_outstanding($useractive, $columnName, $columnSortOrder, $rowperpage, $start, $draw, $recordsTotal, $recordsTotalFilter, $regdate, $regdateto, $custselect,$sort_column,$sort_order);
        //$orders_sum  = $this->login_model->get_all_neworders($searchQuery, $columnName, $columnSortOrder, $rowperpage, $start, $draw, $recordsTotal, $recordsTotalFilter);

        echo json_encode($orders);
        exit();
    }
    public function list_ajax_customer_list_outstanding_report()
    {
        $orders = array();
        $draw = $_POST['draw'];
        $start = $_POST['start'];
        $rowperpage = $_POST['length'];
        //$columnIndex = isset($_POST['order'][0]['column'])?$_POST['order'][0]['column']:"";
        $columnName = 'customer_id'; //$_POST['columns'][$columnIndex]['data'];
        $columnSortOrder = 'asc'; //$_POST['order'][0]['dir'];
        $searchValue = $_POST['search']['value'];

        // Custom search filter 
        $useractive = $_POST['useractive'];
        $regdate = $_POST['regdate'];
        $regdateto = $_POST['regdateto'];
        $custselect = $_POST['custselect'];

        $recordsTotal = $this->customers_model->count_all_customers_outstanding();
        $recordsTotalFilter = $this->customers_model->get_all_customers_outstanding($useractive, $regdate, $regdateto, $custselect);

        /************************************************ */
        // get sort data from datatable
        $sort_column = $_POST['columns'][$_POST['order'][0]['column']]['data'];
        $sort_order = $_POST['order'][0]['dir'];
        /************************************************ */

        $orders = $this->customers_model->get_all_newcustomers_outstanding($useractive, $columnName, $columnSortOrder, $rowperpage, $start, $draw, $recordsTotal, $recordsTotalFilter, $regdate, $regdateto, $custselect, $sort_column, $sort_order);
        //$orders_sum  = $this->login_model->get_all_neworders($searchQuery, $columnName, $columnSortOrder, $rowperpage, $start, $draw, $recordsTotal, $recordsTotalFilter);

        echo json_encode($orders);
        exit();
    }
	
	public function quickbook_new_customer_add($data = array(), $res)
    {
		$quickbookvalues = get_quick_book_values();
		if($quickbookvalues->access_token != "")
		{
			$sync_cust_count=0;
			$error_cust_count=0;
			$error_array=array();
			
			$mob = substr($data['mobile_number_1'], -9);
			$check_quickbook = $this->quickbook_model->get_customer_details_for_update($mob);
			if(!empty($check_quickbook))
			{
				$updatefield = array();
				$updatefield['quickbook_id'] = $check_quickbook->quickBookId;
				$updatefield['quickbookTokenId'] = $check_quickbook->syncToken;
				$updatefield['quickBookName'] = $check_quickbook->FullyQualifiedName;
				// $updatefield['balance'] = $check_quickbook->balance;
				$updatefield['quickbook_sync_status'] = 1;
				$updatess = $this->quickbook_model->update_customers($updatefield,$res);
				
				$addfield = array();
				$addfield['syncstatus'] = 1;
				$updatessnew = $this->quickbook_model->update_quickbookcustomers($addfield,$check_quickbook->id);
				
				$sync_cust_count++;
				$ret_array = array();
				$ret_array['status'] = 'success';
				$ret_array['message'] = 'Synched to quickbooks successfully';
				return $ret_array;
			} else {
			
				$customer_array = array();
				$customer_array['DisplayName'] = $data['customer_name'];
				$customer_array['PrimaryPhone'] = array(
					"FreeFormNumber" => $data['mobile_number_1']
				);
				if($data['email_address'] != "")
				{
					$customer_array['PrimaryEmailAddr'] = array(
						"Address" => $data['email_address']
					);
				}
				if ($data['apartment'] != "") {
					$apartment = ' , Apt No ' . $data['apartment'];
				} else {
					$apartment = "";
				}
				$addresssss = $data['customer_address'] . $apartment;
				
				$getarea = $this->settings_model->get_area_details($data['area_id']);

				$customer_array['BillAddr'] = array(
					"City" => $getarea->area_name,
					"Line1" => $addresssss,
					"Country" => "United Arab Emirates"
				);
							
				$input_json = json_encode($customer_array);
				$headerarray = array(
					"Accept: application/json",
					"Content-Type: application/json",
					"Authorization: Bearer ".$quickbookvalues->access_token
				);
				//print_r(json_encode($headerarray));exit();
				$ch = curl_init();
				$url = "https://quickbooks.api.intuit.com/v3/company/".$quickbookvalues->quickbookCompanyId."/customer?minorversion=57";
				// $url = "https://sandbox-quickbooks.api.intuit.com/v3/company/".$quickbookvalues->quickbookCompanyId."/customer?minorversion=57";
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $input_json);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headerarray);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				$output = curl_exec($ch); 
				if (curl_errno($ch)) {
					// echo $error_msg = curl_error($ch);
					// exit();
				}
				curl_close($ch);
				$json = json_decode($output, true);
				// print_r($json);//exit();
				if(!empty($json['Customer']))
				{
					if($json['Customer']['Id'] != "")
					{
						$updatefield = array();
						$updatefield['quickbook_id'] = $json['Customer']['Id'];
						$updatefield['quickbookTokenId'] = $json['Customer']['SyncToken'];
						$updatefield['quickbook_sync_status'] = 1;
						$updatess = $this->customers_model->update_booktype($updatefield,$res);
						$ret_array = array();
						$ret_array['status'] = 'success';
						$ret_array['message'] = 'Synched to quickbooks successfully';
						return $ret_array;
					}
				}
							
				if(isset($json['fault']))
				{
					if($json['fault']['error'][0]['code']=='3200')
					{
						$ret_array = array();
						$ret_array['status'] = 'error';
						$ret_array['message'] = $json['fault']['error'][0]['detail'];
						return $ret_array;
					} else {
						$ret_array = array();
						$ret_array['status'] = 'error';
						$ret_array['message'] = $json['fault']['error'][0]['detail'];
						return $ret_array;
					}					
				}
				if(isset($json['Fault']))
				{
					if($json['Fault']['Error'][0]['code']=='3200')
					{
						$ret_array = array();
						$ret_array['status'] = 'error';
						$ret_array['message'] = $json['Fault']['Error'][0]['Detail'];
						return $ret_array;
					} else {
						$ret_array = array();
						$ret_array['status'] = 'error';
						$ret_array['message'] = $json['Fault']['Error'][0]['Detail'];
						return $ret_array;
					}					
				}
			}
			
		} else {
			$ret_array = array();
			$ret_array['status'] = 'error';
			$ret_array['message'] = 'Token not available. Quickbook sync failed.';
			return $ret_array;
		}
    }
	
	public function quickbook_customer_update($data = array(), $res, $quickbookid,$quickbooksyncid)
    {
		$quickbookvalues = get_quick_book_values();
		if($quickbookvalues->access_token != "")
		{
			$sync_cust_count=0;
			$error_cust_count=0;
			$error_array=array();
			$customer_array = array();
			// $customer_array['domain'] = $data['QBO'];
			$customer_array['DisplayName'] = $data['customer_name'];
			$customer_array['SyncToken'] = $quickbooksyncid;
			$customer_array['sparse'] = false;
			$customer_array['Id'] = $quickbookid;
			$customer_array['PrimaryPhone'] = array(
				"FreeFormNumber" => $data['mobile_number_1']
			);
			if($data['email_address'] != "")
			{
				$customer_array['PrimaryEmailAddr'] = array(
					"Address" => $data['email_address']
				);
			}
			if ($data['apartment'] != "") {
				$apartment = ' , Apt No ' . $data['apartment'];
			} else {
				$apartment = "";
			}
			$addresssss = $data['customer_address'] . $apartment;
			
			$getarea = $this->settings_model->get_area_details($data['area_id']);

			$customer_array['BillAddr'] = array(
				"City" => $getarea->area_name,
				"Line1" => $addresssss,
				"Country" => "United Arab Emirates"
			);
						
			$input_json = json_encode($customer_array);
			$headerarray = array(
				"Accept: application/json",
				"Content-Type: application/json",
				"Authorization: Bearer ".$quickbookvalues->access_token
			);
			//print_r(json_encode($headerarray));exit();
			$ch = curl_init();
			$url = "https://quickbooks.api.intuit.com/v3/company/".$quickbookvalues->quickbookCompanyId."/customer?minorversion=57";
			// $url = "https://sandbox-quickbooks.api.intuit.com/v3/company/".$quickbookvalues->quickbookCompanyId."/customer?minorversion=57";
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $input_json);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headerarray);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			$output = curl_exec($ch); 
			if (curl_errno($ch)) {
				// echo $error_msg = curl_error($ch);
				// exit();
			}
			curl_close($ch);
			$json = json_decode($output, true);
			// print_r($json);//exit();
			if(!empty($json['Customer']))
			{
				if($json['Customer']['Id'] != "")
				{
					$updatefield = array();
					$updatefield['quickbookTokenId'] = $json['Customer']['SyncToken'];
					$updatess = $this->customers_model->update_booktype($updatefield,$res);
					$ret_array = array();
					$ret_array['status'] = 'success';
					$ret_array['message'] = 'Synched to quickbooks successfully';
					return $ret_array;
				}
			}
						
			if(isset($json['fault']))
			{
				if($json['fault']['error'][0]['code']=='3200')
				{
					$ret_array = array();
					$ret_array['status'] = 'error';
					$ret_array['message'] = $json['fault']['error'][0]['detail'];
					return $ret_array;
				} else {
					$ret_array = array();
					$ret_array['status'] = 'error';
					$ret_array['message'] = $json['fault']['error'][0]['detail'];
					return $ret_array;
				}					
			}
			
			if(isset($json['Fault']))
			{
				if($json['Fault']['Error'][0]['code']=='3200')
				{
					$ret_array = array();
					$ret_array['status'] = 'error';
					$ret_array['message'] = $json['Fault']['Error'][0]['Detail'];
					return $ret_array;
				} else {
					$ret_array = array();
					$ret_array['status'] = 'error';
					$ret_array['message'] = $json['Fault']['Error'][0]['Detail'];
					return $ret_array;
				}					
			}
			
		} else {
			$ret_array = array();
			$ret_array['status'] = 'error';
			$ret_array['message'] = 'Token not available. Quickbook sync failed.';
			return $ret_array;
		}
    }
	
	public function quickbook_new_customer_payment($data = array(), $settings,$add_payment_id,$quickbook_customer_id)
    {
		if($settings->access_token != "")
		{
			if($data['payment_method'] == 0)
			{
				$payId = 1;
				$bankId = 856;
			} else if($data['payment_method'] == 1)
			{
				$payId = 3;
				$bankId = 846;
			} else if($data['payment_method'] == 2)
			{
				$payId = 5;
				$bankId = 105;
			} else if($data['payment_method'] == 3)
			{
				$payId = 5;
				$bankId = 105;
			} else if($data['payment_method'] == 4)
			{
				$payId = 3;
				$bankId = 846;
			}
			$amount = $data['paid_amount'];
			$customer_array = array();
			$customer_array['CustomerRef'] = array('value' => $quickbook_customer_id);
			$customer_array['TxnDate'] = $data['paid_datetime'];
			$customer_array['TotalAmt'] = number_format((float)$amount, 2, '.', '');
			$customer_array['PaymentMethodRef'] = array('value' => $payId);
			$customer_array['DepositToAccountRef'] = array('value' => $bankId);
			if($data['receipt_no'] != "")
			{
				$customer_array['PaymentRefNum'] = $data['receipt_no'];
			}
						
			$input_json = json_encode($customer_array);
			$headerarray = array(
				"Accept: application/json",
				"Content-Type: application/json",
				"Authorization: Bearer ".$settings->access_token
			);
			//print_r(json_encode($headerarray));exit();
			$ch = curl_init();
			$url = "https://quickbooks.api.intuit.com/v3/company/".$settings->quickbookCompanyId."/payment?minorversion=57";
			// $url = "https://sandbox-quickbooks.api.intuit.com/v3/company/".$settings->quickbookCompanyId."/payment?minorversion=57";
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $input_json);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headerarray);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			$output = curl_exec($ch); 
			if (curl_errno($ch)) {
				// echo $error_msg = curl_error($ch);
				// exit();
			}
			curl_close($ch);
			$json = json_decode($output, true);
			// print_r($json);//exit();
			if(!empty($json['Payment']))
			{
				if($json['Payment']['Id'] != "")
				{
					$updatefield = array();
					$updatefield['quickbook_payment_id'] = $json['Payment']['Id'];
					$updatefield['quickbookPaymentTokenId'] = $json['Payment']['SyncToken'];
					$updatefield['quickbook_payment_sync_status'] = 1;
					$updatess = $this->customers_model->update_customer_quickbookpayment($updatefield,$add_payment_id);
					$ret_array = array();
					$ret_array['status'] = 'success';
					$ret_array['message'] = 'Synched to quickbooks successfully';
					return $ret_array;
				}
			}
						
			if(isset($json['fault']))
			{
				if($json['fault']['error'][0]['code']=='3200')
				{
					$ret_array = array();
					$ret_array['status'] = 'error';
					$ret_array['message'] = $json['fault']['error'][0]['detail'];
					return $ret_array;
				} else {
					$ret_array = array();
					$ret_array['status'] = 'error';
					$ret_array['message'] = $json['fault']['error'][0]['detail'];
					return $ret_array;
				}					
			}
			if(isset($json['Fault']))
			{
				if($json['Fault']['Error'][0]['code']=='3200')
				{
					$ret_array = array();
					$ret_array['status'] = 'error';
					$ret_array['message'] = $json['Fault']['Error'][0]['Detail'];
					return $ret_array;
				} else {
					$ret_array = array();
					$ret_array['status'] = 'error';
					$ret_array['message'] = $json['Fault']['Error'][0]['Detail'];
					return $ret_array;
				}					
			}
			
		} else {
			$ret_array = array();
			$ret_array['status'] = 'error';
			$ret_array['message'] = 'Token not available. Quickbook sync failed.';
			return $ret_array;
		}
    }
	
	public function quickbook_update_customer_payment($data,$settingss,$quickbookCustomer_id,$quickbookPayment_id,$quickbookPaymentSync_id,$payment_id)
	{
		if($settingss->access_token != "")
		{
			if($data['payment_method'] == 0)
			{
				$payId = 1;
				$bankId = 856;
			} else if($data['payment_method'] == 1)
			{
				$payId = 3;
				$bankId = 846;
			} else if($data['payment_method'] == 2)
			{
				$payId = 5;
				$bankId = 105;
			} else if($data['payment_method'] == 3)
			{
				$payId = 5;
				$bankId = 105;
			} else if($data['payment_method'] == 4)
			{
				$payId = 3;
				$bankId = 846;
			}
			$customer_array = array();
			$customer_array['CustomerRef'] = array('value' => $quickbookCustomer_id);
			$customer_array['SyncToken'] = $quickbookPaymentSync_id;
			$customer_array['Id'] = $quickbookPayment_id;
			$customer_array['TxnDate'] = $data['paid_datetime'];
			$customer_array['TotalAmt'] = $data['paid_amount'];
			$customer_array['PaymentMethodRef'] = array('value' => $payId);
			$customer_array['DepositToAccountRef'] = array('value' => $bankId);
			if($data['receipt_no'] != "")
			{
				$customer_array['PaymentRefNum'] = $data['receipt_no'];
			}
						
			$input_json = json_encode($customer_array);
			$headerarray = array(
				"Accept: application/json",
				"Content-Type: application/json",
				"Authorization: Bearer ".$settingss->access_token
			);
			//print_r(json_encode($headerarray));exit();
			$ch = curl_init();
			$url = "https://quickbooks.api.intuit.com/v3/company/".$settings->quickbookCompanyId."/payment?minorversion=57";
			// $url = "https://sandbox-quickbooks.api.intuit.com/v3/company/".$settingss->quickbookCompanyId."/payment?minorversion=57";
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $input_json);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headerarray);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			$output = curl_exec($ch); 
			if (curl_errno($ch)) {
				// echo $error_msg = curl_error($ch);
				// exit();
			}
			curl_close($ch);
			$json = json_decode($output, true);
			// print_r($json);//exit();
			if(!empty($json['Payment']))
			{
				if($json['Payment']['Id'] != "")
				{
					$updatefield = array();
					// $updatefield['quickbook_payment_id'] = $json['Payment']['Id'];
					$updatefield['quickbookPaymentTokenId'] = $json['Payment']['SyncToken'];
					// $updatefield['quickbook_payment_sync_status'] = 1;
					$updatess = $this->customers_model->update_customer_quickbookpayment($updatefield,$payment_id);
					$ret_array = array();
					$ret_array['status'] = 'success';
					$ret_array['message'] = 'Synched to quickbooks successfully';
					return $ret_array;
				}
			}
						
			if(isset($json['fault']))
			{
				if($json['fault']['error'][0]['code']=='3200')
				{
					$ret_array = array();
					$ret_array['status'] = 'error';
					$ret_array['message'] = $json['fault']['error'][0]['detail'];
					return $ret_array;
				} else {
					$ret_array = array();
					$ret_array['status'] = 'error';
					$ret_array['message'] = $json['fault']['error'][0]['detail'];
					return $ret_array;
				}					
			}
			
			if(isset($json['Fault']))
			{
				if($json['Fault']['Error'][0]['code']=='3200')
				{
					$ret_array = array();
					$ret_array['status'] = 'error';
					$ret_array['message'] = $json['Fault']['Error'][0]['Detail'];
					return $ret_array;
				} else {
					$ret_array = array();
					$ret_array['status'] = 'error';
					$ret_array['message'] = $json['Fault']['Error'][0]['Detail'];
					return $ret_array;
				}					
			}
			
		} else {
			$ret_array = array();
			$ret_array['status'] = 'error';
			$ret_array['message'] = 'Token not available. Quickbook sync failed.';
			return $ret_array;
		}
	}
	
	public function update_customer_balance()
    {
        $this->load->library('Excel');
        $file = "./upload/opening-bal.xls";
        $obj = PHPExcel_IOFactory::load($file);
        $cell = $obj->getActiveSheet()->getCellCollection();
        foreach ($cell as $cl) {
            $column = $obj->getActiveSheet()->getCell($cl)->getColumn();
            $row = $obj->getActiveSheet()->getCell($cl)->getRow();
            $data_value = $obj->getActiveSheet()->getCell($cl)->getFormattedValue();

            if ($row == 1) {
                $header[$row][$column] = $data_value;
            } else if ($row == 2) {
				$header[$row][$column] = $data_value;
			} else {
                $arr_data[$row][$column] = $data_value;
            }
        }
        $data['header'] = $header;
        $data['values'] = $arr_data;
		
		// echo '<pre>';
			// print_r($arr_data);
			// exit();
		
        foreach ($arr_data as $val)
		{
			$bal = $val['F'];
            $customer_array = array();
			$customer_array['record'] = $val['A'];
			$customer_array['customer'] = $val['D'];
			$customer_array['mobile'] = $val['E'];
			$customer_array['balance'] = str_replace(',', '', $bal);
			
			$customer_id = $this->customers_model->add_brillion_customers($customer_array);

            if ($customer_id > 0) {
				echo $val['A'] . "-customer inserted";
				echo '<br>';
			} else {
				echo $val['A'] . "-customer not inserted";
				echo '<br>';
			}
        }
        exit();
    }
	
	public function update_balance_emaid()
	{
		$get_all_customers = $this->customers_model->get_all_newcustomers_for_sync();
		foreach($get_all_customers as $val)
		{
			$mob = substr($val['mobile'], -9);
			$check_quickbook = $this->customers_model->get_customer_details_for_update($mob);
			// echo $check_quickbook->quickBookId;
			// echo '<pre>';
			// print_r($check_quickbook);
			// exit();
			if(!empty($check_quickbook))
			{
				$amount = $val['balance'];
				if(substr($amount, 0, 1) == "-")
				{
					$bal = ltrim($amount,'-') ;
					$updatefield = array();
					$updatefield['initial_balance'] = $bal;
					$updatefield['initial_bal_sign'] = "Dr";
					$updatefield['initial_bal_date'] = "2023-06-30";
					$updatefield['total_invoice_amount'] = $bal;
					$updatefield['balance'] = $bal;
					$updatess = $this->customers_model->update_customers($updatefield,$check_quickbook->customer_id);
					
					$data['apartment'] = $check_quickbook->building;
					$data['customer_address'] = $check_quickbook->customer_address;
					$data['customer_name'] = $check_quickbook->customer_name;
					
					$invoicecreation = $this->invoice_creation($check_quickbook->customer_id,$bal,"2023-06-30",$data);
					
					$addfield = array();
					$addfield['tried'] = 1;
					$addfield['updateStatus'] = 1;
					$updatessnew = $this->customers_model->update_brillioncustomers($addfield,$val['id']);
					
					echo $val['record']."- updated";
					echo '<br/>';
				} else {
					$updatefield = array();
					$updatefield['initial_balance'] = $amount;
					$updatefield['initial_bal_sign'] = "Cr";
					$updatefield['initial_bal_date'] = "2023-06-30";
					
					$updatess = $this->customers_model->update_customers($updatefield,$check_quickbook->customer_id);
					
					$data['apartment'] = $check_quickbook->building;
					$data['customer_address'] = $check_quickbook->customer_address;
					$data['customer_name'] = $check_quickbook->customer_name;
					
					$paymentcreation = $this->payment_creation($check_quickbook->customer_id,$amount,"2023-06-30");
					
					$addfield = array();
					$addfield['tried'] = 1;
					$addfield['updateStatus'] = 1;
					$updatessnew = $this->customers_model->update_brillioncustomers($addfield,$val['id']);
					
					echo $val['record']."- updated";
					echo '<br/>';
				}
			} else {
				$addfield = array();
				$addfield['tried'] = 1;
				$updatessnew = $this->customers_model->update_brillioncustomers($addfield,$val['id']);
				echo $val['record']."- not updated";
				echo '<br/>';
			}
		}
		exit();
	}
	
	public function payment_creation($res,$initialbal,$initialbaldate)
	{
		$add_payment_fields = array();
		$add_payment_fields['customer_id'] = $res;
		$add_payment_fields['paid_amount'] = $initialbal;
		$add_payment_fields['balance_amount'] = $initialbal;
		$add_payment_fields['payment_method'] = 0;
		$add_payment_fields['paid_at'] = 'P';
		$add_payment_fields['paid_at_id'] = user_authenticate();
		$add_payment_fields['paid_datetime'] = $initialbaldate;
		$add_payment_fields['createdAt'] = date('Y-m-d H:i:s');
		$add_payment_fields['ps_no'] = 'INITIAL';
		$add_payment_fields['receipt_no'] = 'INITIAL';
		$add_payment_fields['pay_description'] = "Credit Balance";
		$add_payment_fields['verified_status'] = 1;
		$add_payment_fields['quickbook_payment_show_status'] = 0;
		$add_payment_fields['show_status'] = 1;
		
		$add_payment_id = $this->customers_model->add_customer_payment($add_payment_fields);
		return $add_payment_id;
	}
	
	public function quickbook_new_customer_payment_memo($data = array(), $settings,$add_payment_id,$quickbook_customer_id,$memo_id)
    {
		if($settings->access_token != "")
		{
			$line=array();
			$line[0]['DetailType']='SalesItemLineDetail';
			$line[0]['Amount']=$data['paid_amount'];
			$line[0]['SalesItemLineDetail']['Qty']=1;
			$line[0]['SalesItemLineDetail']['UnitPrice']=$data['paid_amount'];
			$line[0]['SalesItemLineDetail']['ItemRef']['value']=1;
			$line[0]['SalesItemLineDetail']['ItemRef']['name']="Sales";
					
			$memo_array=array();
			$memo_array['TxnDate'] = $data['paid_datetime'];
			$memo_array['TotalAmt'] = $data['paid_amount'];
			$memo_array['Line'] = $line;
			$memo_array['CustomerRef'] = array('value' => $quickbook_customer_id);
			$memo_array['DocNumber'] = $data['receipt_no'];
					
			$input_jsons = json_encode($memo_array);
			$headerarrays = array(
				"Accept: application/json",
				"Content-Type: application/json",
				"Authorization: Bearer ".$settings->access_token
			);
			$headerss = [];
			//print_r(json_encode($headerarray));exit();
			$ch = curl_init();
			// $url = "https://quickbooks.api.intuit.com/v3/company/".$settings->quickbookCompanyId."/payment?minorversion=57";
			$url = "https://sandbox-quickbooks.api.intuit.com/v3/company/".$settings->quickbookCompanyId."/creditmemo?minorversion=57";
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $input_jsons);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headerarrays);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_HEADERFUNCTION,
				function ($curl, $header) use (&$headerss) {
					$len = strlen($header);
					$header = explode(':', $header, 2);
					if (count($header) < 2) // ignore invalid headers
						return $len;

					$headerss[strtolower(trim($header[0]))][] = trim($header[1]);

					return $len;
				}
			);
			$outputs = curl_exec($ch); 
			if (curl_errno($ch)) {
				// echo $error_msg = curl_error($ch);
				// exit();
			}
			curl_close($ch);
			$json = json_decode($outputs, true);
			$jsonheaderss = json_encode($headerss, true);
			// print_r($json);//exit();
			if(!empty($json['CreditMemo']))
			{
				if($json['CreditMemo']['Id'] != "")
				{
					$credit_quickbook_id = $json['CreditMemo']['Id'];
					$updatefields = array();
					$updatefields['quickbook_credit_memo_id'] = $json['CreditMemo']['Id'];
					$updatefields['quickbookCreditMemoTokenId'] = $json['CreditMemo']['SyncToken'];
					$updatefields['quickbook_creditmemo_sync_status'] = 1;
					// $updatess = $this->customers_model->update_customer_quickbookpayment($updatefield,$add_payment_id);
					$this->quickbook_model->update_quick_credit_memo_by_payid($memo_id,$updatefields,$input_jsons,$outputs,$jsonheaderss,$credit_quickbook_id);
					
					$ret_array = array();
					$ret_array['status'] = 'success';
					$ret_array['message'] = 'Synched to quickbooks successfully';
					return $ret_array;
				}
			}
					
			if(isset($json['fault']))
			{
				if($json['fault']['error'][0]['code']=='3200')
				{
					$ret_array = array();
					$ret_array['status'] = 'error';
					$ret_array['message'] = $json['fault']['error'][0]['detail'];
					return $ret_array;
				} else {
					$ret_array = array();
					$ret_array['status'] = 'error';
					$ret_array['message'] = $json['fault']['error'][0]['detail'];
					return $ret_array;
				}					
			}
			if(isset($json['Fault']))
			{
				if($json['Fault']['Error'][0]['code']=='3200')
				{
					$ret_array = array();
					$ret_array['status'] = 'error';
					$ret_array['message'] = $json['Fault']['Error'][0]['Detail'];
					return $ret_array;
				} else {
					$ret_array = array();
					$ret_array['status'] = 'error';
					$ret_array['message'] = $json['Fault']['Error'][0]['Detail'];
					return $ret_array;
				}					
			}
		} else {
			$ret_array = array();
			$ret_array['status'] = 'error';
			$ret_array['message'] = 'Token not available. Quickbook sync failed.';
			return $ret_array;
		}
    }
	
	public function quickbook_new_customer_payment_memo_old($data = array(), $settings,$add_payment_id,$quickbook_customer_id,$memo_id)
    {
		if($settings->access_token != "")
		{
			$line=array();
			$line[0]['DetailType']='SalesItemLineDetail';
			$line[0]['Amount']=$data['paid_amount'];
			$line[0]['SalesItemLineDetail']['Qty']=1;
			$line[0]['SalesItemLineDetail']['UnitPrice']=$data['paid_amount'];
			$line[0]['SalesItemLineDetail']['ItemRef']['value']=1;
			$line[0]['SalesItemLineDetail']['ItemRef']['name']="Sales";
					
			$memo_array=array();
			$memo_array['TxnDate'] = $data['paid_datetime'];
			$memo_array['TotalAmt'] = $data['paid_amount'];
			$memo_array['Line'] = $line;
			$memo_array['CustomerRef'] = array('value' => $quickbook_customer_id);
			$memo_array['DocNumber'] = $data['receipt_no'];
					
			$input_jsons = json_encode($memo_array);
			$headerarrays = array(
				"Accept: application/json",
				"Content-Type: application/json",
				"Authorization: Bearer ".$settings->access_token
			);
			$headerss = [];
			//print_r(json_encode($headerarray));exit();
			$ch = curl_init();
			// $url = "https://quickbooks.api.intuit.com/v3/company/".$settings->quickbookCompanyId."/payment?minorversion=57";
			$url = "https://sandbox-quickbooks.api.intuit.com/v3/company/".$settings->quickbookCompanyId."/creditmemo?minorversion=57";
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $input_jsons);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headerarrays);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_HEADERFUNCTION,
				function ($curl, $header) use (&$headerss) {
					$len = strlen($header);
					$header = explode(':', $header, 2);
					if (count($header) < 2) // ignore invalid headers
						return $len;

					$headerss[strtolower(trim($header[0]))][] = trim($header[1]);

					return $len;
				}
			);
			$outputs = curl_exec($ch); 
			if (curl_errno($ch)) {
				// echo $error_msg = curl_error($ch);
				// exit();
			}
			curl_close($ch);
			$json = json_decode($outputs, true);
			$jsonheaderss = json_encode($headerss, true);
			// print_r($json);//exit();
			if(!empty($json['CreditMemo']))
			{
				if($json['CreditMemo']['Id'] != "")
				{
					$credit_quickbook_id = $json['CreditMemo']['Id'];
					$updatefields = array();
					$updatefields['quickbook_credit_memo_id'] = $json['CreditMemo']['Id'];
					$updatefields['quickbookCreditMemoTokenId'] = $json['CreditMemo']['SyncToken'];
					$updatefields['quickbook_creditmemo_sync_status'] = 1;
					// $updatess = $this->customers_model->update_customer_quickbookpayment($updatefield,$add_payment_id);
					$this->quickbook_model->update_quick_credit_memo_by_payid($memo_id,$updatefields,$input_jsons,$outputs,$jsonheaderss,$credit_quickbook_id);
					
					if($data['payment_method'] == 0)
					{
						$payId = 1;
						$bankId = 92;
					} else if($data['payment_method'] == 1)
					{
						$payId = 3;
						$bankId = 846;
					} else if($data['payment_method'] == 2)
					{
						$payId = 5;
						$bankId = 105;
					} else if($data['payment_method'] == 3)
					{
						$payId = 5;
						$bankId = 105;
					} else if($data['payment_method'] == 4)
					{
						$payId = 3;
						$bankId = 846;
					}
					// $amount = 0;
					$customer_array = array();
					$customer_array['CustomerRef'] = array('value' => $quickbook_customer_id);
					$customer_array['TxnDate'] = $data['paid_datetime'];
					$customer_array['TotalAmt'] = 0;
					$customer_array['PaymentMethodRef'] = array('value' => $payId);
					$customer_array['DepositToAccountRef'] = array('value' => $bankId);
					if($data['receipt_no'] != "")
					{
						$customer_array['PaymentRefNum'] = $data['receipt_no'];
					}
					
					$newmemoarray = array();
					$newmemoarray[0]['LinkedTxn'][0]['TxnId']=435;
					$newmemoarray[0]['LinkedTxn'][0]['TxnType']='Invoice';
					$newmemoarray[0]['Amount']= $data['paid_amount'];
					
					$newmemoarray[1]['LinkedTxn'][0]['TxnId']=$credit_quickbook_id;
					$newmemoarray[1]['LinkedTxn'][0]['TxnType']='CreditMemo';
					$newmemoarray[1]['Amount']= $data['paid_amount'];
					
					$customer_array['Line'] = $newmemoarray;
								
					$input_json = json_encode($customer_array);
					
					$headerarray = array(
						"Accept: application/json",
						"Content-Type: application/json",
						"Authorization: Bearer ".$settings->access_token
					);
					$headers = [];
					//print_r(json_encode($headerarray));exit();
					$ch = curl_init();
					// $url = "https://quickbooks.api.intuit.com/v3/company/".$settings->quickbookCompanyId."/payment?minorversion=57";
					$url = "https://sandbox-quickbooks.api.intuit.com/v3/company/".$settings->quickbookCompanyId."/payment?minorversion=57";
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $input_json);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headerarray);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					curl_setopt($ch, CURLOPT_HEADERFUNCTION,
						function ($curl, $header) use (&$headers) {
							$len = strlen($header);
							$header = explode(':', $header, 2);
							if (count($header) < 2) // ignore invalid headers
								return $len;

							$headers[strtolower(trim($header[0]))][] = trim($header[1]);

							return $len;
						}
					);
					$output = curl_exec($ch); 
					if (curl_errno($ch)) {
						// echo $error_msg = curl_error($ch);
						// exit();
					}
					curl_close($ch);
					$json = json_decode($output, true);
					$jsonheaders = json_encode($headers, true);
					// print_r($json);//exit();
					if(!empty($json['Payment']))
					{
						if($json['Payment']['Id'] != "")
						{
							$quickbook_iddd = $json['Payment']['Id'];
							
							$updatefield = array();
							$updatefield['quickbook_payment_id'] = $json['Payment']['Id'];
							$updatefield['quickbookPaymentTokenId'] = $json['Payment']['SyncToken'];
							$updatefield['quickbook_payment_sync_status'] = 1;
							// $updatess = $this->customers_model->update_customer_quickbookpayment($updatefield,$add_payment_id);
							$this->quickbook_model->update_quick_invoice_payment_by_payid($add_payment_id,$updatefield,$input_json,$output,$jsonheaders,$quickbook_iddd);
							
							$ret_array = array();
							$ret_array['status'] = 'success';
							$ret_array['message'] = 'Synched to quickbooks successfully';
							return $ret_array;
						}
					}
				}
			}
					
			if(isset($json['fault']))
			{
				if($json['fault']['error'][0]['code']=='3200')
				{
					$ret_array = array();
					$ret_array['status'] = 'error';
					$ret_array['message'] = $json['fault']['error'][0]['detail'];
					return $ret_array;
				} else {
					$ret_array = array();
					$ret_array['status'] = 'error';
					$ret_array['message'] = $json['fault']['error'][0]['detail'];
					return $ret_array;
				}					
			}
			if(isset($json['Fault']))
			{
				if($json['Fault']['Error'][0]['code']=='3200')
				{
					$ret_array = array();
					$ret_array['status'] = 'error';
					$ret_array['message'] = $json['Fault']['Error'][0]['Detail'];
					return $ret_array;
				} else {
					$ret_array = array();
					$ret_array['status'] = 'error';
					$ret_array['message'] = $json['Fault']['Error'][0]['Detail'];
					return $ret_array;
				}					
			}
		} else {
			$ret_array = array();
			$ret_array['status'] = 'error';
			$ret_array['message'] = 'Token not available. Quickbook sync failed.';
			return $ret_array;
		}
    }

     // ************************************************************

     public function email_check($email)
     {
         $customer_id = $this->input->post('customer_id'); 
         $this->db->where('email_address', $email);
         $this->db->where('customer_id !=', $customer_id);
         $query = $this->db->get('customers');
 
         if ($query->num_rows() > 0) {
             $this->form_validation->set_message('email_check', '%s is already registered for another customer!');
             return FALSE;
         } else {
             return TRUE;
         }
     }
 // *****************************************************
     public function mobile_number_check($mobile_number)
     {
         $customer_id = $this->input->post('customer_id'); 
         $this->db->where('mobile_number_1', $mobile_number);
         $this->db->where('customer_id !=', $customer_id);
         $query = $this->db->get('customers');
 
         if ($query->num_rows() > 0) {
             $this->form_validation->set_message('mobile_number_check', 'The %s is already registered for another customer!');
             return FALSE;
         } else {
             return TRUE;
         }
     }
     // ***********************************************************
     public function editvalidatemobilenumber()
     {
         log_message('error','post mobile number1'.json_encode($this->input->post()));
 
         if ($this->input->post('mobile')) {
             $mobile_number_1 = $this->input->post("mobile");
             $id = $this->input->post("id");
             $form = $this->input->post("form");
             $result = $this->customers_model->check_mob_number($mobile_number_1, $id, $form);
             echo json_encode($result);
         }
     }
     // ***************************************************
     public function editvalidateemail()
     {
         log_message('error','post email'.json_encode($this->input->post()));
         if ($this->input->post('email')) {
             $email = $this->input->post('email');
             $id = $this->input->post("id");
             $form = $this->input->post("form");
             $result = $this->customers_model->check_email_customers($email, $id, $form);
             echo json_encode($result);
         }
     }
     // *************************************************

     
}
