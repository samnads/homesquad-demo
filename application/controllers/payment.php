<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment extends CI_Controller {
	
    public function __construct()
    {
        parent::__construct();
        $this->load->model('customers_model');
        $this->load->model('maids_model');
        $this->load->model('invoice_model');
        $this->load->model('zones_model');
        $this->load->model('bookings_model');
        $this->load->model('day_services_model');
        $this->load->helper('google_api_helper');
        $this->load->model('tablets_model');
        $this->load->model('settings_model');
    }
    
    public function index()
    {
		
	}
	
	public function payment_receipt($paymentid)
	{
		$this->load->library('pdf');
		$payment_detail = $this->customers_model->get_customer_payment_byid($paymentid);
		$get_allocated_invoices = $this->customers_model->get_allocated_invoices($paymentid);
		$data = array();
		$data['payment_detail'] = $payment_detail;
		$data['allocated_invoices'] = $get_allocated_invoices;
        $data['settings'] = $this->settings_model->get_settings();
		$html_content = $this->load->view('paymentreceiptpdf', $data, TRUE);
		$this->pdf->loadHtml($html_content);
		$this->pdf->set_paper("a4", "portrait");
		$this->pdf->render();
        
		$this->pdf->stream("".$paymentid.".pdf", array("Attachment"=>0));
        //file_put_contents(base_url().''.$invoiceid.".pdf", $this->pdf->output());
		exit();
	}
	
	public function send_receipt_email()
    {
        //ini_set('display_errors', 1);
        //ini_set('display_startup_errors', 1);
        //error_reporting(E_ALL);
        $paymentid=$this->input->post('paymentid');
        $paymentcontent=$this->input->post('paymentcontent');
        $customer_email=$this->input->post('paymentemail');
        $this->load->library('pdf');
        $payment_detail = $this->customers_model->get_customer_payment_byid($paymentid);
		$get_allocated_invoices = $this->customers_model->get_allocated_invoices($paymentid);
        $data['payment_detail'] = $payment_detail;
		$data['allocated_invoices'] = $get_allocated_invoices;
        $data['settings'] = $this->settings_model->get_settings();
		$html_content = $this->load->view('paymentreceiptpdf', $data, TRUE);
        $this->pdf->loadHtml($html_content);
        $this->pdf->set_paper("a4", "portrait");
        $this->pdf->render();
        
        //$customer_email=$invoice_detail[0]->email_address;
        
        file_put_contents('/var/www/html/emaid-demo/receipts/'.$paymentid.".pdf", $this->pdf->output());
        
        $this->load->library('email');
        $config = array(
                    'protocol'  => 'smtp',
					'smtp_host' => 'ssl://smtp.googlemail.com',
					'smtp_port' => 465,
					'smtp_user' => 'emaidmaps@gmail.com',
					'smtp_pass' => 'viltazemmwrmrybw',
					'mailtype'  => 'html',
					'charset'   => 'utf-8'
              );

        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");
        $this->email->from('emaidmaps@gmail.com', 'Company Name');
        $this->email->to($customer_email);
        $this->email->subject('Company Name Receipt Email');
        //$this->email->message("Sir ,<br/>&nbsp;&nbsp;&nbsp;&nbsp;Kindly check the attached invoice.");
        $this->email->message($paymentcontent);
        $this->email->attach('/var/www/html/emaid-demo/receipts/'.$paymentid.".pdf");
        if($this->email->send())
        {
            echo "success";
        }
        else
        {
            if(strlen($customer_email)<2)
            {echo "email_error";}
            else
            {echo "error";}            
        }
        unlink('/var/www/html/emaid-demo/receipts/'.$paymentid.".pdf");
        //$this->email->print_debugger();
        exit();
    }
	
}