<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('users_model');
        $this->load->helper('cookie');
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->model('settings_model');
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        /************************************ AJAX LOGIN *********************************** */
        if ($this->input->is_ajax_request()) {
            // if ajax request found
            header('Content-Type: application/json; charset=utf-8');
            if ($this->input->post('login')) {
				//$this->session->sess_destroy();
                $this->form_validation->set_rules('username', 'Username', 'required|min_length[1]');
                $this->form_validation->set_rules('password', 'Password', 'required|min_length[1]');
                if ($this->form_validation->run()) {
                    // for validation done - now check credentials
                    $username = trim($this->input->post('username'));
                    $password = trim($this->input->post('password'));
                    $user_login = user_login($username, $password);
                    if (isset($user_login['error'])) {
                        $errors = array();
                        switch ($user_login['error']) {
                            case 'Invalid login':
                                $errors[0] = 'Invalid username and / or password !';
                                break;
                            case 'Inactive':
                                $errors[0] = 'Your account is not active !';
                                break;
                            default:
                                $errors[0] = 'Error, Please try again !';
                                break;
                        }
						die(json_encode(array("status" => false, 'message' => $errors[0]), true));
                    } else {
                        if ($this->input->cookie('ci_session')) {
                            $data = unserialize($this->input->cookie('ci_session'));
                            $fields = array();
                            $fields['session_id'] = $data['session_id'];
                            $fields['ip_address'] = $data['ip_address'];
                            $fields['user_agent'] = $data['user_agent'];
                            $fields['last_activity'] = $data['last_activity'];
                            $fields['user_data'] = serialize($this->session->userdata('user_logged_in'));
                            $this->users_model->add_sessions($fields);
                        }
                        $user_detail = $this->users_model->get_user_by_id($user_login['user_id']);
                        if ($user_detail->login_type == 'C') {
                            die(json_encode(array("status" => true, 'message' => 'Logged in successfully !', 'redirect' => base_url('dashboard')), true));
                        } else {
                            die(json_encode(array("status" => true, 'message' => 'Logged in successfully !', 'redirect' => base_url('maintenance')), true));
                        }
                    }
                } else {
                    $errors = $this->form_validation->error_array();
                    die(json_encode(array("status" => false, 'errors' => $errors, 'message' => $errors['username'] ?: $errors['password']), true));
                }
            } else {
                die(json_encode(array("status" => false, 'message' => 'Action not specified !'), true));
            }
        }
        /********************************************************************************** */
        if (is_user_loggedin()) {
            redirect('dashboard');
        }

        $this->load->library('form_validation');
        $this->load->helper('form');

        if ($this->input->post('login')) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_message('required', 'Please enter username', 'username');

            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_message('required', 'Please enter password', 'password');

            if ($this->form_validation->run()) {
                $username = trim($this->input->post('username'));
                $password = trim($this->input->post('password'));

                $user_login = user_login($username, $password);

                if (isset($user_login['error'])) {
                    $errors = array();
                    switch ($user_login['error']) {
                        case 'Invalid login':
                            $errors[0] = 'Invalid username and/or password.';
                            break;

                        case 'Inactive':
                            $errors[0] = 'Your account is not active.';
                            break;

                        default:
                            $errors[0] = 'Error!! Please try again.';
                            break;
                    }
                } else {
                    if ($this->input->cookie('ci_session')) {
                        $data = unserialize($this->input->cookie('ci_session'));

                        $fields = array();
                        $fields['session_id'] = $data['session_id'];
                        $fields['ip_address'] = $data['ip_address'];
                        $fields['user_agent'] = $data['user_agent'];
                        $fields['last_activity'] = $data['last_activity'];
                        $fields['user_data'] = serialize($this->session->userdata('user_logged_in'));
                        $this->users_model->add_sessions($fields);
                    }
                    $user_detail = $this->users_model->get_user_by_id($user_login['user_id']);
                    if ($user_detail->login_type == 'C') {
                        redirect('dashboard');
                    } else {
                        redirect('maintenance');
                    }
                    exit();
                }
                //$errors = array();
                //$errors[0] = 'Something went wrong. Please try again!';
            } else {
                $errors = $this->form_validation->error_array();
            }
        }

        $data = array();
        $data['error'] = isset($errors) ? array_shift($errors) : '';
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('login', $data, true);
        $layout_data['page_title'] = 'Login';
        $layout_data['meta_description'] = 'Login';
        $layout_data['css_files'] = array();
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array();
        $this->load->view('layouts/default', $layout_data);
    }

    public function loginn()
    {
        $user = $this->db->from('users')->where('user_status', '1')->order_by('user_id', 'ASC')->get()->row();

        $sess_array = array();
        $sess_array['user_id'] = $user->user_id;
        $sess_array['user_name'] = $user->username;
        $sess_array['user_fullname'] = $user->user_fullname;
        $sess_array['user_permissions'] = $user->permissions;
        $sess_array['user_admin'] = $user->is_admin;
        $sess_array['login_type'] = $user->login_type;

        // echo "<pre>";print_r($sess_array);die;

        $this->session->set_userdata('user_logged_in', $sess_array);
        redirect('dashboard');

    }

    /**
     * User Log Out
     */
    public function logout()
    {
        user_logout();
        redirect('login');
    }
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */
