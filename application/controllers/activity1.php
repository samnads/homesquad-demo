<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Activity extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!user_permission(user_authenticate(), 5)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $this->load->model('customers_model');
        $this->load->model('maids_model');
        //$this->load->model('areas_model');
        $this->load->model('zones_model');
        $this->load->model('bookings_model');
        $this->load->model('day_services_model');
        $this->load->helper('google_api_helper');
        $this->load->model('tablets_model');
        $this->load->helper('curl_helper');
        $this->load->model('service_types_model');
        $this->load->model('settings_model');
    }
    public function index()
    {
        if ($this->input->is_ajax_request()) {
            $booking_id = $this->input->post('booking_id');
            $s_date = explode("/", $this->input->post('service_date'));
            $service_date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];

            $chk_day_service = $this->day_services_model->get_day_service_by_booking_id($service_date, $booking_id);
            $chk_booking = $this->bookings_model->get_booking_by_id($booking_id);

            if ($this->input->post('action') && $this->input->post('action') == 'service-start') {


                if (isset($chk_day_service->day_service_id)) {
                    /*
                                $response = array();
                                $response['status'] = 'error';
                                $response['error_code'] = '106';
                                $response['message'] = $chk_day_service->service_status == 1 ? 'Service already started' : 'Service already finished';
                                echo json_encode($response);
                                exit();
                                */
                    $ds_fields = array();
                    $ds_fields['start_time'] = date('H:i:s');
                    $ds_fields['service_status'] = 1;
                    $ds_fields['service_added_by'] = 'B';
                    $ds_fields['service_added_by_id'] = user_authenticate();

                    $this->day_services_model->update_day_service($chk_day_service->day_service_id, $ds_fields);
                    $this->day_services_model->add_activity($chk_booking, user_authenticate(), 'Customer_IN');

                    $response = array();
                    $response['status'] = 'success';
                    $response['booking_id'] = $booking_id;
                    $response['service_status'] = 1;

                    echo json_encode($response);
                    exit();
                } else {
                    $booking = $this->bookings_model->get_booking_by_id($booking_id);

                    $normal_hours = 0;
                    $extra_hours = 0;
                    $weekend_hours = 0;

                    $normal_from = strtotime('08:00:00');
                    $normal_to = strtotime('20:00:00');

                    $shift_from = strtotime($booking->time_from . ':00');
                    $shift_to = strtotime($booking->time_to . ':00');

                    $total_hours = ($shift_to - $shift_from) / 3600;
                    if ($booking->cleaning_material == 'Y') {
                        $materialfee = ($total_hours * 5);
                    } else {
                        $materialfee = 0;
                    }

                    //                                        if(date('w') >= 0 && date('w') < 5)      // except friday & saturday case
                    if (date('w') != 5)                        // except friday  case // changed on 23-01-2017
                    {
                        if ($shift_from < $normal_from) {
                            if ($shift_to <= $normal_from) {
                                $extra_hours = ($shift_to - $shift_from) / 3600;
                            }

                            if ($shift_to > $normal_from && $shift_to <= $normal_to) {
                                $extra_hours = ($normal_from - $shift_from) / 3600;
                                $normal_hours = ($shift_to - $normal_from) / 3600;
                            }

                            if ($shift_to > $normal_to) {
                                $extra_hours = ($normal_from - $shift_from) / 3600;
                                $extra_hours += ($shift_to - $normal_to) / 3600;
                                $normal_hours = ($normal_to - $normal_from) / 3600;
                            }
                        }

                        if ($shift_from >= $normal_from && $shift_from < $normal_to) {
                            if ($shift_to <= $normal_to) {
                                $normal_hours = ($shift_to - $shift_from) / 3600;
                            }

                            if ($shift_to > $normal_to) {
                                $normal_hours = ($normal_to - $shift_from) / 3600;
                                $extra_hours = ($shift_to - $normal_to) / 3600;
                            }
                        }

                        if ($shift_from > $normal_to) {
                            $extra_hours = ($shift_to - $shift_from) / 3600;
                        }
                    } else {
                        $weekend_hours = ($shift_to - $shift_from) / 3600;
                    }

                    $service_description = array();

                    $service_description['normal'] = new stdClass();
                    $service_description['normal']->hours = $normal_hours;
                    if ($booking->booked_from == "W") {
                        $service_description['normal']->fees = $normal_hours * $booking->price_per_hr;
                    } else {
                        $service_description['normal']->fees = $normal_hours * $booking->price_hourly;
                    }

                    $service_description['extra'] = new stdClass();
                    $service_description['extra']->hours = $extra_hours;
                    $service_description['extra']->fees = $extra_hours * $booking->price_extra;

                    $service_description['weekend'] = new stdClass();
                    $service_description['weekend']->hours = $weekend_hours;
                    $service_description['weekend']->fees = $weekend_hours * $booking->price_weekend;

                    /*
                                 * Location charge
                                 */
                    $location_charge = 0;
                    /*
                                $customer_address = $this->customers_model->get_customer_address_by_id($booking->customer_address_id);
                                if($customer_address){
                                    $area = $this->areas_model->get_area_by_id($customer_address->area_id);
                                    if($area){
                                        $location_charge = $area->area_charge;
                                    }
                                }*/

                    //$total_fee = $service_description['normal']->fees + $service_description['extra']->fees + $service_description['weekend']->fees+ $location_charge - $booking->discount;
                    if ($booking->customer_address == "") {
                        $booking->customer_address = 'Building - ' . $booking->building . ', ' . $booking->unit_no . '' . $booking->street;
                    } else {
                        if ($booking->building != "") {
                            $addressss = "Apt No: " . $booking->building . ", " . $booking->customer_address;
                        } else {
                            $addressss = $booking->customer_address;
                        }
                        $booking->customer_address = $addressss;
                    }


                    $total_fee = $booking->total_amount;
                    $ds_fields = array();
                    $ds_fields['booking_id'] = $booking_id;
                    $ds_fields['customer_id'] = $booking->customer_id;
                    $ds_fields['customer_name'] = $booking->customer_name;
                    $ds_fields['customer_address'] = $booking->customer_address;
                    $ds_fields['customer_payment_type'] = $booking->payment_type;
                    $ds_fields['maid_id'] = $booking->maid_id;
                    $ds_fields['service_description'] = serialize($service_description);
                    $ds_fields['total_fee'] = $total_fee;
                    $ds_fields['material_fee'] = $materialfee;
                    $ds_fields['service_date'] = $service_date;
                    $ds_fields['start_time'] = date('H:i:s');
                    $ds_fields['service_status'] = 1;
                    $ds_fields['service_added_by'] = 'B';
                    $ds_fields['service_added_by_id'] = user_authenticate();

                    $day_service = $this->day_services_model->add_day_service($ds_fields);
                    $this->day_services_model->add_activity($booking, user_authenticate(), 'Customer_IN');

                    if ($day_service) {
                        $response = array();
                        $response['status'] = 'success';
                        $response['booking_id'] = $booking_id;
                        $response['service_status'] = 1;

                        echo json_encode($response);
                        exit();
                    } else {
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '105';
                        $response['message'] = 'Unexpected error';

                        echo json_encode($response);
                        exit();
                    }
                }
            }
            if ($this->input->post('action') && $this->input->post('action') == 'service-stop') {

                if ($this->input->post('payment') !== FALSE && ($this->input->post('payment') == 0 ||  $this->input->post('payment') == 3 ||  $this->input->post('payment') == 1)) {
                    $payment_status = $this->input->post('payment');
                } else {    //$zone_id = trim($this->input->post('zone_id'));
                    $response = array();
                    $response['status'] = 'error';
                    $response['error_code'] = '101';
                    $response['message'] = 'Invalid request. Invalid payment status.';

                    echo json_encode($response);
                    exit();
                }

                if (isset($chk_day_service->day_service_id) && isset($payment_status)) {

                    if ($chk_day_service->service_status == 1 || $chk_day_service->service_status == 2) {
                        if ($payment_status == 1) {
                            if ($this->input->post('amount') && is_numeric($this->input->post('amount')) &&  $this->input->post('amount') > 0) {
                                $amount = $this->input->post('amount');
                                if ($this->input->post('psno') && is_numeric($this->input->post('psno')) &&  $this->input->post('psno') > 0) {
                                    $psno = $this->input->post('psno');
                                } else {
                                    $psno = "";
                                }
                                /*if($amount < $chk_day_service->total_fee)
                                                        {
                                                                $response = array();
                                                                $response['status'] = 'error';
                                                                $response['error_code'] = '99';
                                                                $response['message'] = 'Customer has to pay AED' . $chk_day_service->total_fee;

                                                                echo json_encode($response);
                                                                exit();
                                                        }*/

                                $payment_method = 0;
                                //if($this->input->post('method') && is_numeric($this->input->post('method')) &&  $this->input->post('method') > 0)
                                //{
                                //$payment_method = 1;
                                //}

                                $chk_payment = $this->day_services_model->get_payment_by_day_service_id($service_date, $chk_day_service->day_service_id);

                                if (isset($chk_payment) && $chk_payment->payment_id) {
                                    $payment_fields = array();
                                    $payment_fields['customer_id'] = $chk_day_service->customer_id;
                                    $payment_fields['day_service_id'] = $chk_day_service->day_service_id;
                                    $payment_fields['paid_amount'] = $amount;
                                    $payment_fields['receipt_no'] = $psno;
                                    //$payment_fields['paid_at'] = 'B';
                                    //$payment_fields['paid_at_id'] = user_authenticate();
                                    $payment_fields['payment_method'] = $payment_method;


                                    $add_payment = $this->day_services_model->update_payment($chk_payment->payment_id, $payment_fields);
                                } else {
                                    $payment_fields = array();
                                    $payment_fields['customer_id'] = $chk_day_service->customer_id;
                                    $payment_fields['day_service_id'] = $chk_day_service->day_service_id;
                                    $payment_fields['paid_amount'] = $amount;
                                    $payment_fields['receipt_no'] = $psno;
                                    $payment_fields['paid_at'] = 'B';
                                    $payment_fields['paid_at_id'] = user_authenticate();
                                    $payment_fields['payment_method'] = $payment_method;
                                    $payment_fields['paid_datetime'] = date('Y-m-d H:i:s', strtotime($service_date));

                                    $add_payment = $this->customers_model->add_customer_payment($payment_fields);
                                }


                                $this->day_services_model->add_activity($chk_booking, user_authenticate(), 'Customer_OUT');
                                $this->day_services_model->add_activity($chk_booking, user_authenticate(), 'Payment', $amount);
                            } else {
                                $response = array();
                                $response['status'] = 'error';
                                $response['error_code'] = '101';
                                $response['message'] = 'Invalid request. Invalid amount.';

                                echo json_encode($response);
                                exit();
                            }
                        } else if ($chk_day_service->payment_type == 'D') {
                            // Daily paying custmer, not paid. Send notification to manager
                            //$this->push_notification(3);
                        }
                        if ($payment_status == 3) {
                            // service not done
                            /*if(isset($chk_day_service->day_service_id))
                                            {*/
                            if ($chk_day_service->service_status == 1) {
                                $ds_fields = array();
                                $ds_fields['end_time'] = date('H:i:s');
                                $ds_fields['service_status'] = 3;

                                $this->day_services_model->update_day_service($chk_day_service->day_service_id, $ds_fields);
                                $this->day_services_model->add_activity($chk_booking, user_authenticate(), 'Service_Cancel');

                                //$this->push_notification(2);

                                $response = array();
                                $response['status'] = 'success';
                                $response['booking_id'] = $booking_id;
                                $response['service_status'] = 3;
                                $response['payment_status'] = $payment_status;
                                echo json_encode($response);
                                exit();
                            } else {
                                $response = array();
                                $response['status'] = 'error';
                                $response['error_code'] = '106';
                                $response['message'] = 'Service already finished';

                                echo json_encode($response);
                                exit();
                            }
                            /*}
                                            else
                                            {
                                                    $response = array();
                                                    $response['status'] = 'error';
                                                    $response['error_code'] = '106';
                                                    $response['message'] = 'Service not started';

                                                    echo json_encode($response);
                                                    exit();
                                            }*/
                        } else {
                            $ds_fields = array();
                            $ds_fields['end_time'] = date('H:i:s');
                            $ds_fields['service_status'] = 2;
                            $ds_fields['payment_status'] = isset($add_payment) && $add_payment ? 1 : 0;

                            if ($ds_fields['payment_status'] === 0) {
                                $chk_payment = $this->day_services_model->get_payment_by_day_service_id($service_date, $chk_day_service->day_service_id);

                                if (isset($chk_payment) && $chk_payment->payment_id) {
                                    $this->day_services_model->delete_payment($chk_payment->payment_id);
                                }
                            }
                            $this->day_services_model->update_day_service($chk_day_service->day_service_id, $ds_fields);
                            $this->day_services_model->add_activity($chk_booking, user_authenticate(), 'Customer_OUT');
                            if (isset($add_payment) && $add_payment)
                                $this->day_services_model->add_activity($chk_booking, user_authenticate(), 'Payment', $amount);

                            $response = array();
                            $response['status'] = 'success';
                            $response['booking_id'] = $booking_id;
                            $response['service_status'] = 2;
                            $response['payment_status'] = $payment_status;
                            if ($chk_day_service->is_company == 'N' && $chk_day_service->rating_mail == 'Y') {
                                //$this->send_service_status_email($chk_day_service->booking_id,$chk_day_service->day_service_id,$service_date);
                            }
                            echo json_encode($response);
                            exit();
                        }
                    } else {
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '106';
                        $response['message'] = 'Service already finished';

                        echo json_encode($response);
                        exit();
                    }
                } else {
                    $response = array();
                    $response['status'] = 'error';
                    $response['error_code'] = '106';
                    $response['message'] = 'Service not started';

                    echo json_encode($response);
                    exit();
                }
            }
            if ($this->input->post('action') && $this->input->post('action') == 'service-cancel') {
                echo 'success';
                exit();
            }
            if ($this->input->post('action') && $this->input->post('action') == 'transfer') {
                $maid_id = $chk_booking->maid_id;
                $bookingid = $chk_booking->booking_id;
                $users_id = user_authenticate();
                if (user_authenticate() != 1) {
                    if ($chk_booking->is_locked == 1 && $chk_booking->booked_by != $users_id) {
                        $response = array();
                        $response['status'] = 'error';
                        $response['message'] = 'Booking is locked.';
                        echo json_encode($response);
                        exit();
                    }
                }
                $check_booking_transferred = $this->bookings_model->check_booking_transferred_new($bookingid, $service_date);
                if (count($check_booking_transferred) > 0) {
                    $transferids = $check_booking_transferred->booking_transfer_tablet_id;
                } else {
                    $transferids = 0;
                }
                $datetimes = date('Y-m-d H:i:s');
                $toid = $this->input->post('tablet_id');
                $driver_chng_array = array();
                $driver_chng_array['booking_id'] = $bookingid;
                $driver_chng_array['transfering_to_tablet'] = $toid;
                $driver_chng_array['service_date'] = $service_date;
                $driver_chng_array['transferred_date_time'] = $datetimes;
                $driver_chng_array['transferred_from'] = 'B';
                $driver_chng_array['transferred_from_id'] = $chk_booking->tabletid;
                $driver_chng_array['transferred_by'] = $users_id;
                $activityid = $this->bookings_model->insert_driverchnage($driver_chng_array, $transferids);
                if ($activityid > 0) {
                    /* Check if maid already in */
                    $chk_attandence = $this->maids_model->get_maid_attandence_by_date($maid_id, $service_date);
                    if (isset($chk_attandence->attandence_id) && $chk_attandence->attandence_status == 1) {
                        $maid_out_fields = array();
                        $maid_out_fields['maid_out_time'] = date('H:i:s');
                        $maid_out_fields['attandence_status'] = 2;

                        $maid_out = $this->maids_model->update_maid_attandence($chk_attandence->attandence_id, $maid_out_fields);
                        $this->day_services_model->add_activity($chk_booking, user_authenticate(), 'Maid_OUT');
                    }
                    $tzone_tablet = $this->tablets_model->get_tablet_by_zone_tabid($toid);
                    if (isset($tzone_tablet->google_reg_id)) {
                        $push_fields = array();
                        $push_fields['tab_id'] = $tzone_tablet->tablet_id;
                        $push_fields['title'] = "Booking Transfered";
                        $push_fields['type'] = 1;
                        $push_fields['message'] = 'Booking on ' . $service_date . ' from ' . $chk_booking->newtime_from . ' to ' . $chk_booking->newtime_to . ' of maid ' . $chk_booking->maid_name . ' is transfered to ' . $tzone_tablet->tablet_driver_name;
                        $push_fields['customer_name'] = $chk_booking->customer_name;
                        $push_fields['maid_name'] = $chk_booking->maid_name;
                        $push_fields['booking_time'] = $chk_booking->newtime_from . ' - ' . $chk_booking->newtime_to;
                        $push = $this->bookings_model->add_push_notifications($push_fields);
                        $deviceid = $tzone_tablet->google_reg_id;
                        $title = "Booking Transfered";
                        $payload = array();
                        $payload['isfeedback'] = false;
                        if (isset($push) && $push > 0) {
                            $payload['pushid'] = $push;
                        } else {
                            $payload['pushid'] = 0;
                        }
                        $message = 'Booking on ' . $service_date . ' from ' . $chk_booking->newtime_from . 'to ' . $chk_booking->newtime_to . ' of maid ' . $chk_booking->maid_name . ' is transfered to ' . $tzone_tablet->tablet_driver_name;
                        $res = array();
                        $res['title'] = $title;
                        $res['is_background'] = false;
                        $res['body'] = $message;
                        $res['image'] = "";
                        $res['payload'] = $payload;
                        $res['customer'] = $chk_booking->customer_name;
                        $res['maid'] = $chk_booking->maid_name;
                        $res['bookingTime'] = $chk_booking->newtime_from . ' - ' . $chk_booking->newtime_to;
                        $res['timestamp'] = date('Y-m-d G:i:s');
                        $regId = $deviceid;

                        $fields = array(
                            'to' => $regId,
                            'notification' => $res,
                            'data' => $res,
                        );

                        android_customer_app_push($fields);
                    }
                    $response = array();
                    $response['status'] = 'success';
                    echo json_encode($response);
                    exit();
                }
            }
            if ($this->input->post('action') && $this->input->post('action') == 'transfer-old') {

                if ($this->input->post('zone_id') != FALSE &&  $this->input->post('zone_id') != 0  && $this->input->post('zone_id') != '') {
                    $zone_id = trim($this->input->post('zone_id'));
                    $zone = $this->zones_model->get_zone_by_id($zone_id);
                    $maid_id = $chk_booking->maid_id;

                    if (isset($zone->zone_id) && $zone->zone_status == 1) {
                        $booking_transfers = $this->bookings_model->get_booking_transfers_by_date($service_date);
                        $yes_transferred_booking_zones = array();
                        $no_transferred_booking_zones = array();
                        foreach ($booking_transfers as $b_transfer) {
                            if ($b_transfer->zone_id == $chk_booking->zone_id) {
                                $chk_bookings = $this->bookings_model->get_booking_by_id($b_transfer->booking_id);

                                if (isset($chk_bookings->maid_id) && $chk_bookings->maid_id == $maid_id) {
                                    $yes_transferred_booking_zones[$b_transfer->booking_transfer_id] = $b_transfer->booking_id;
                                }
                            } else if ($b_transfer->booking_id == $chk_booking->booking_id) {
                                $chk_bookings = $this->bookings_model->get_booking_by_id($b_transfer->booking_id);

                                if (isset($chk_bookings->maid_id) && $chk_bookings->maid_id == $maid_id) {
                                    $yes_transferred_booking_zones[$b_transfer->booking_transfer_id] = $b_transfer->booking_id;
                                    $no_transferred_booking_zones[$b_transfer->booking_id] = $b_transfer->zone_id;
                                }
                            } else {
                                $no_transferred_booking_zones[$b_transfer->booking_id] = $b_transfer->zone_id;
                            }
                        }


                        $bookings = $this->bookings_model->get_maid_bookings_on_zone_by_date($maid_id, $chk_booking->zone_id, $service_date);

                        $transferred_bookings  = array();
                        foreach ($bookings as $booking) {
                            if (!isset($no_transferred_booking_zones[$booking->booking_id])) {
                                $b_transfer_fields = array();
                                $b_transfer_fields['booking_id'] = $booking->booking_id;
                                $b_transfer_fields['zone_id'] = $zone->zone_id;
                                $b_transfer_fields['service_date'] = $service_date;
                                $b_transfer_fields['transferred_time'] = date('H:i:s');
                                $b_transfer_fields['transferred_by'] = 'B';
                                $b_transfer_fields['transferred_by_id'] = user_authenticate();

                                $this->bookings_model->add_booking_transfer($b_transfer_fields);
                                $this->day_services_model->add_activity($chk_booking, user_authenticate(), 'Transfer', '', $zone->zone_name);

                                $transferred_bookings[] = $booking->booking_id;
                            }
                        }

                        foreach ($yes_transferred_booking_zones as $booking_transfer_id => $booking_id) {
                            $b_transfer_fields = array();
                            $b_transfer_fields['booking_id'] = $booking_id;
                            $b_transfer_fields['zone_id'] = $zone->zone_id;
                            $b_transfer_fields['service_date'] = $service_date;
                            $b_transfer_fields['transferred_time'] = date('H:i:s');
                            $b_transfer_fields['transferred_by'] = 'T';
                            $b_transfer_fields['transferred_by_id'] = user_authenticate();

                            $this->bookings_model->add_booking_transfer($b_transfer_fields);
                            $this->day_services_model->add_activity($chk_booking, user_authenticate(), 'Transfer', '', $zone->zone_name);

                            $this->bookings_model->delete_booking_transfer($booking_transfer_id);

                            $transferred_bookings[] = $booking_id;
                        }

                        /* Check if maid already in */
                        $chk_attandence = $this->maids_model->get_maid_attandence_by_date($maid_id, $service_date);
                        if (isset($chk_attandence->attandence_id) && $chk_attandence->attandence_status == 1) {
                            $maid_out_fields = array();
                            $maid_out_fields['maid_out_time'] = date('H:i:s');
                            $maid_out_fields['attandence_status'] = 2;

                            $maid_out = $this->maids_model->update_maid_attandence($chk_attandence->attandence_id, $maid_out_fields);
                            $this->day_services_model->add_activity($chk_booking, user_authenticate(), 'Maid_OUT');
                        }

                        if (count($transferred_bookings) > 0) {
                            $response = array();
                            $response['status'] = 'success';
                            $response['maid_id'] = $maid_id;
                            $response['service_date'] = $service_date;
                            $response['bookings'] = $transferred_bookings;
                        } else {
                            $response = array();
                            $response['status'] = 'success';
                            $response['maid_id'] = $maid_id;
                            $response['service_date'] = $service_date;
                            $response['bookings'] = $transferred_bookings;
                        }

                        $tzone_tablet = $this->tablets_model->get_tablet_by_zone($zone->zone_id);

                        if (isset($tzone_tablet->google_reg_id)) {
                            $deviceid = $tzone_tablet->google_reg_id;

                            $title = "Maid Transfered";
                            $payload = array();
                            $payload['isfeedback'] = false;

                            $message = 'Booking on ' . $service_date . ' from ' . $chk_booking->newtime_from . 'to ' . $chk_booking->newtime_to . ' of maid ' . $chk_booking->maid_name . ' is transfered to ' . $zone->zone_name;



                            $res = array();

                            $res['title'] = $title;
                            $res['is_background'] = false;
                            $res['body'] = $message;
                            $res['image'] = "";
                            $res['payload'] = $payload;
                            $res['timestamp'] = date('Y-m-d G:i:s');
                            $regId = $deviceid;

                            $fields = array(
                                'to' => $regId,
                                'notification' => $res,
                                'data' => $res,
                            );
                            $push_fields = array();
                            $push_fields['tab_id'] = $tzone_tablet->tablet_id;
                            $push_fields['type'] = 1;
                            $push_fields['message'] = 'Booking on ' . $service_date . ' from ' . $chk_booking->newtime_from . ' to ' . $chk_booking->newtime_to . ' of maid ' . $chk_booking->maid_name . ' is transfered to ' . $zone->zone_name;


                            $push = $this->bookings_model->add_push_notifications($push_fields);

                            android_customer_app_push($fields);
                        }

                        echo json_encode($response);
                        exit();
                    } else if (isset($zone->zone_id) && $zone->zone_status == 0 && $zone->zone_id != $chk_booking->zone_id) {
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '104';
                        $response['message'] = 'Zone not active';

                        echo json_encode($response);
                        exit();
                    } else {
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '105';
                        $response['message'] = 'Invalid zone id';

                        echo json_encode($response);
                        exit();
                    }
                } else {    //$zone_id = trim($this->input->post('zone_id'));
                    $response = array();
                    $response['status'] = 'error';
                    $response['error_code'] = '101';
                    $response['message'] = 'Invalid request. Invalid zone.';

                    echo json_encode($response);
                    exit();
                }
            }
        }

        $data = array();
        if ($this->input->post('service_date')) {
            $s_date = $this->input->post('service_date');
            $s_date = explode("/", $s_date);
            $service_date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        } else {
            $service_date = date('Y-m-d');
            //$service_date = '2015-06-17';
        }
        $data['formatted_date'] = $service_date;
        $data['zones'] = $this->day_services_model->get_zones();
        $data['tablets'] = $this->tablets_model->get_all_newdrivers();
        $data['service_date'] = date('d/m/Y', strtotime($service_date));
        $d_services_sorted = $this->day_services_model->get_activity_by_date_plan_new($service_date);
        usort($d_services_sorted, array($this, 'cmp'));

        $data['dayservices'] = $d_services_sorted; //$this->day_services_model->get_activity_by_date($service_date);


        /*
                echo '<pre>';
                print_r($data['dayservices']);
                exit();
                */
        $layout_data = array();
        $layout_data['content_body'] = $this->load->view('activity', $data, TRUE);
        $layout_data['page_title'] = 'Plan';
        $layout_data['meta_description'] = 'Activity';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'hm.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('base.js', 'ajaxupload.3.5.js', 'mymaids.js', 'bootstrap-datepicker.js', 'bootstrap.js');
        $layout_data['plan_active'] = '1';
        $this->load->view('layouts/default', $layout_data);
    }

    public function plan_export()
    {
        $data = array();
        $service_date = $this->uri->segment(3);
        $d_services_sorted = $this->day_services_model->get_activity_by_date_plan($service_date);
        usort($d_services_sorted, array($this, 'cmp'));

        $data['formatted_date'] = $service_date;
        $data['dayservices'] = $d_services_sorted;
        $this->load->view('plan_export_excel', $data);
    }

    public function activitylist()
    {

        $data = array();
        if ($this->input->post('service_date')) {
            $s_date = $this->input->post('service_date');
            $s_date = explode("/", $s_date);
            $service_date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        } else {
            $service_date = date('Y-m-d');
            //$service_date = '2015-06-17';
        }
        $data['formatted_date'] = $service_date;
        $data['zones'] = $this->day_services_model->get_zones();
        $data['service_date'] = date('d/m/Y', strtotime($service_date));
        $d_services_sorted = $this->day_services_model->get_activity_by_date($service_date);
        usort($d_services_sorted, array($this, 'cmp'));

        $data['dayservices'] = $d_services_sorted; //$this->day_services_model->get_activity_by_date($service_date);

        /*
                echo '<pre>';
                print_r($data['dayservices']);
                exit();
                */
        $layout_data = array();
        $layout_data['content_body'] = $this->load->view('activitylist', $data, TRUE);
        $layout_data['page_title'] = 'Activity';
        $layout_data['meta_description'] = 'Activity';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'hm.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('base.js', 'ajaxupload.3.5.js', 'mymaids.js', 'bootstrap-datepicker.js', 'bootstrap.js', 'hm.js');
        $layout_data['activity_active'] = '1';
        $this->load->view('layouts/default', $layout_data);
    }

    public function zoneactivity()
    {


        $data = array();
        if ($this->input->post('service_date')) {
            $s_date = $this->input->post('service_date');
            $s_date = explode("/", $s_date);
            $service_date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        } else {
            $service_date = date('Y-m-d');
            //$service_date = '2015-06-17';
        }
        $data['formatted_date'] = $service_date;
        $data['zones'] = $this->day_services_model->get_zones();
        $data['service_date'] = date('d/m/Y', strtotime($service_date));
        $d_services_sorted = $this->day_services_model->get_zone_activity_by_date($service_date);
        usort($d_services_sorted, array($this, 'cmp'));

        $data['dayservices'] = $d_services_sorted; //$this->day_services_model->get_activity_by_date($service_date);
        /*
                echo '<pre>';
                print_r($data['dayservices']);
                exit();
                */
        $layout_data = array();
        $layout_data['content_body'] = $this->load->view('zone_activity', $data, TRUE);
        $layout_data['page_title'] = 'Activity';
        $layout_data['meta_description'] = 'Activity';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'hm.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('base.js', 'ajaxupload.3.5.js', 'mymaids.js', 'bootstrap-datepicker.js', 'bootstrap.js', 'hm.js');

        $this->load->view('layouts/default', $layout_data);
    }
    private function cmp($a, $b)
    {
        if ($a->zone_id ==  $b->zone_id) {
            return 0;
        }
        return ($a->zone_id < $b->zone_id) ? -1 : 1;
    }


    public function jobs()
    {
        //$date = date('Y-m-d');
        if ($this->input->post('service_date')) {
            $s_date = $this->input->post('service_date');
            $s_date = explode("/", $s_date);
            $date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        } else {
            $date = date('Y-m-d');
            //$service_date = '2015-06-17';
        }
        $date_from_job = date('Y-m-d');
        $date_to_job = date('Y-m-d', strtotime('+1 week'));

        if ($this->input->is_ajax_request()) {
            if ($this->input->post('action') && $this->input->post('action') == 'unscheduled') {
                $datejob = $this->input->post('servicedate');
                $approval_list = $this->bookings_model->get_all_booking_approval_list_job('Pending', $datejob);
                $i = 1;
                $html = "";
                if (!empty($approval_list)) {
                    foreach ($approval_list as $ap_list) {
                        $checkcomplaint = $this->bookings_model->getjobcomplaintbyid($ap_list->booking_id, $datejob);
                        if ($ap_list->booking_type == "WE") {
                            $booking_type = "Weekly";
                        } else if ($ap_list->booking_type == "OD") {
                            $booking_type = "One Day";
                        }

                        if ($ap_list->customer_address == "") {
                            $a_address = 'Building - ' . $ap_list->building . ', ' . $ap_list->unit_no . '' . $ap_list->street;
                        } else {
                            $a_address = $ap_list->customer_address;
                        }

                        $html .= '<tr>
                                <td></td>
                                <td><a href="' . base_url() . 'activity/job_view/' . $datejob . '/' . $ap_list->booking_id . '">' . $i . '</a></td>'
                            . '<td>' . $ap_list->customer_name . ' [<a href="' . base_url() . 'customer/view/' . $ap_list->customer_id . '">Details</a>] </td>'
                            . '<td>' . $a_address . '</td>'
                            . '<td>' . $ap_list->shift . '</td>'
                            . '<td>' . $booking_type . '</td>
                                        <td>
                                            <i class="fa fa-phone"></i> ' . $ap_list->mobile_number_1 . '
                                            <br>
                                            <i class="fa fa-envelope"></i> ' . $ap_list->email_address . '
                                        </td>
                                        <td class="' . $b_list->booking_id . '-' . strtotime($datejob) . 'complaintclass">';

                        if (empty($checkcomplaint)) {
                            $html .= '<span class="btn add-complaint-job"><i class="fa fa-plus"></i></span>';
                        } else {
                            $html .= '<span class="btn edit-complaint-job"><i class="fa fa-pencil"></i></span>
                                            <span class="btn view-complaint-job"><i class="fa fa-eye"></i></span>';
                        }
                        $html .= '</td>
                                        </tr>';
                        $i++;
                    }
                }
                //                 else {
                //                    $html .= '<tr>
                //                                <td class="dataTables_empty" colspan="7" valign="top">No data available in table</td></tr>';
                //                }
                echo $html;
                exit();
            }
            if ($this->input->post('action') && $this->input->post('action') == 'scheduled') {
                $datejob = $this->input->post('servicedate');
                $booking_list = $this->bookings_model->get_schedule_by_date_job($datejob);
                $i = 1;
                $html = "";
                if (!empty($booking_list)) {
                    foreach ($booking_list as $b_list) {
                        $checkcomplaint = $this->bookings_model->getjobcomplaintbyid($b_list->booking_id, $datejob);
                        if ($b_list->booking_type == "WE") {
                            $booking_type = "Weekly";
                        } else if ($b_list->booking_type == "OD") {
                            $booking_type = "One Day";
                        }

                        if ($b_list->customer_address == "") {
                            $a_address = 'Building - ' . $b_list->building . ', ' . $b_list->unit_no . '' . $b_list->street;
                        } else {
                            $a_address = $b_list->customer_address;
                        }

                        $html .= '<tr>
                                <td></td>
                                <td><a href="' . base_url() . 'activity/job_view/' . $datejob . '/' . $b_list->booking_id . '">' . $i . '</a></td>'
                            . '<td>' . $b_list->customer_name . ' [<a href="' . base_url() . 'customer/view/' . $b_list->customer_id . '">Details</a>] </td>'
                            . '<td>' . $a_address . '</td>'
                            . '<td>' . $b_list->shift . '</td>'
                            . '<td>' . $booking_type . '</td>
                                        <td>
                                            <i class="fa fa-phone"></i> ' . $b_list->mobile_number_1 . '
                                            <br>
                                            <i class="fa fa-envelope"></i> ' . $b_list->email_address . '
                                        </td>
                                        <td class="' . $b_list->booking_id . '-' . strtotime($datejob) . 'complaintclass">';
                        if (empty($checkcomplaint)) {
                            $html .= '<span class="btn add-complaint-job" onclick="addcomplaint(' . $b_list->booking_id . ',' . strtotime($datejob) . ')"><i class="fa fa-plus"></i></span>';
                        } else {
                            $html .= '<span class="btn edit-complaint-job" onclick="editcomplaint(' . $checkcomplaint->cmp_id . ')"><i class="fa fa-pencil"></i></span>
                                            <span class="btn view-complaint-job" onclick="viewcomplaint(' . $checkcomplaint->cmp_id . ')"><i class="fa fa-eye"></i></span>';
                        }
                        $html .= '</td>
                                        </tr>';
                        $i++;
                    }
                }
                //                 else {
                //                    $html .= '<tr>
                //                                <td class="dataTables_empty" colspan="7" valign="top">No data available in table</td></tr>';
                //                }
                echo $html;
                exit();
            }
            if ($this->input->post('action') && $this->input->post('action') == 'inprogress') {
                $datejob = $this->input->post('servicedate');
                $progress_list = $this->day_services_model->get_schedule_progress_date_job($datejob, 1);
                $i = 1;
                $html = "";
                if (!empty($progress_list)) {
                    foreach ($progress_list as $p_list) {
                        if ($p_list->booking_type == "WE") {
                            $booking_type = "Weekly";
                        } else if ($p_list->booking_type == "OD") {
                            $booking_type = "One Day";
                        }

                        if ($p_list->customer_address == "") {
                            $a_address = 'Building - ' . $p_list->building . ', ' . $p_list->unit_no . '' . $p_list->street;
                        } else {
                            $a_address = $p_list->customer_address;
                        }
                        $html .= '<tr>
                                <td></td>
                                <td><a href="' . base_url() . 'activity/job_view/' . $datejob . '/' . $p_list->booking_id . '">' . $i . '</a></td>'
                            . '<td>' . $p_list->customer_name . ' [<a href="' . base_url() . 'customer/view/' . $p_list->customer_id . '">Details</a>] </td>'
                            . '<td>' . $a_address . '</td>'
                            . '<td>' . date("h:i A", strtotime($p_list->time_from)) . ' - ' . date("h:i A", strtotime($p_list->time_to)) . '</td>'
                            . '<td>' . $booking_type . '</td>
                                        <td>
                                            <i class="fa fa-phone"></i> ' . $p_list->mobile_number_1 . '
                                            <br>
                                            <i class="fa fa-envelope"></i> ' . $p_list->email_address . '
                                        </td>
                                        <td>' . date("h:i A", strtotime($p_list->start_time)) . '</td></tr>';
                        $i++;
                    }
                }
                //                 else {
                //                    $html .= '<tr>
                //                                <td class="dataTables_empty" colspan="8" valign="top">No data available in table</td></tr>';
                //                }
                echo $html;
                exit();
            }
            if ($this->input->post('action') && $this->input->post('action') == 'finished') {
                $datejob = $this->input->post('servicedate');
                $finish_list = $this->day_services_model->get_schedule_progress_date_job($datejob, 2);
                $i = 1;
                $html = "";
                if (!empty($finish_list)) {
                    foreach ($finish_list as $f_list) {
                        if ($f_list->booking_type == "WE") {
                            $booking_type = "Weekly";
                        } else if ($f_list->booking_type == "OD") {
                            $booking_type = "One Day";
                        }

                        if ($f_list->customer_address == "") {
                            $a_address = 'Building - ' . $f_list->building . ', ' . $f_list->unit_no . '' . $f_list->street;
                        } else {
                            $a_address = $f_list->customer_address;
                        }

                        $html .= '<tr>
                                <td></td>
                                <td><a href="' . base_url() . 'activity/job_view/' . $datejob . '/' . $f_list->booking_id . '">' . $i . '</a></td>'
                            . '<td>' . $f_list->customer_name . ' [<a href="' . base_url() . 'customer/view/' . $f_list->customer_id . '">Details</a>] </td>'
                            . '<td>' . $a_address . '</td>'
                            . '<td>' . date("h:i A", strtotime($f_list->time_from)) . ' - ' . date("h:i A", strtotime($f_list->time_to)) . '</td>'
                            . '<td>' . $booking_type . '</td>
                                        <td>
                                            <i class="fa fa-phone"></i> ' . $f_list->mobile_number_1 . '
                                            <br>
                                            <i class="fa fa-envelope"></i> ' . $f_list->email_address . '
                                        </td>
                                        <td>' . date("h:i A", strtotime($f_list->start_time)) . ' - ' . date("h:i A", strtotime($f_list->end_time)) . '</td></tr>';
                        $i++;
                    }
                }
                //                else {
                //                    $html .= '<tr>
                //                                <td class="dataTables_empty" colspan="8" valign="top">No data available in table</td></tr>';
                //                }
                echo $html;
                exit();
            }
            if ($this->input->post('action') && $this->input->post('action') == 'recurring') {
                $datejob = $this->input->post('servicedate');
                $recurring_list = $this->bookings_model->get_schedule_recurring_date_job($datejob);
                $i = 1;
                $html = "";
                if (!empty($recurring_list)) {
                    foreach ($recurring_list as $r_list) {
                        if ($r_list->booking_type == "WE") {
                            $booking_type = "Weekly";
                        } else if ($r_list->booking_type == "OD") {
                            $booking_type = "One Day";
                        }

                        if ($r_list->customer_address == "") {
                            $a_address = 'Building - ' . $r_list->building . ', ' . $r_list->unit_no . '' . $r_list->street;
                        } else {
                            $a_address = $r_list->customer_address;
                        }

                        $html .= '<tr>
                                <td></td>
                                <td><a href="' . base_url() . 'activity/job_view/' . $datejob . '/' . $r_list->booking_id . '">' . $i . '</a></td>'
                            . '<td>' . $r_list->customer_name . ' [<a href="' . base_url() . 'customer/view/' . $r_list->customer_id . '">Details</a>] </td>'
                            . '<td>' . $a_address . '</td>'
                            . '<td>' . $r_list->shift . '</td>'
                            . '<td>' . $booking_type . '</td>
                                        <td>
                                            <i class="fa fa-phone"></i> ' . $r_list->mobile_number_1 . '
                                            <br>
                                            <i class="fa fa-envelope"></i> ' . $r_list->email_address . '
                                        </td>
                                        </tr>';
                        $i++;
                    }
                }
                //                 else {
                //                    $html .= '<tr>
                //                                <td class="dataTables_empty" colspan="7" valign="top">No data available in table</td></tr>';
                //                }
                echo $html;
                exit();
            }
            if ($this->input->post('action') && $this->input->post('action') == 'delayed') {
                $datejob = $this->input->post('servicedate');
                $booking_list = $this->bookings_model->get_delayed_schedule_by_date_job($datejob);
                $i = 1;
                $html = "";
                if (!empty($booking_list)) {
                    foreach ($booking_list as $b_list) {
                        //                    $cur_time = strtotime(date('H:i:s'));
                        //                    $db_time = strtotime('13:28:05');
                        //                    if (($cur_time - $db_time) > (15 * 60)) {
                        //                        echo "15 mins has passed";
                        //                    } else {
                        //                        echo "not";
                        //                    }
                        $cur_time = strtotime(date('H:i:s'));
                        $db_time = strtotime($b_list->timefrom);
                        $time = ($cur_time - $db_time) / 60;
                        if (($time > 0 && $time < 15)) {
                            //$fromtimes = strtotime($b_list->timefrom);
                            //$cur_time = strtotime(date('H:i:s'));
                            //if (($cur_time - $fromtimes) < (15 * 60)) {
                            $checkcomplaint = $this->bookings_model->getjobcomplaintbyid($b_list->booking_id, $datejob);
                            if ($b_list->booking_type == "WE") {
                                $booking_type = "Weekly";
                            } else if ($b_list->booking_type == "OD") {
                                $booking_type = "One Day";
                            }

                            if ($b_list->customer_address == "") {
                                $a_address = 'Building - ' . $b_list->building . ', ' . $b_list->unit_no . '' . $b_list->street;
                            } else {
                                $a_address = $b_list->customer_address;
                            }
                            $html .= '<tr>
                                    <td></td>
                                    <td><a href="' . base_url() . 'activity/job_view/' . $datejob . '/' . $b_list->booking_id . '">' . $i . '</a></td>'
                                . '<td>' . $b_list->customer_name . ' [<a href="' . base_url() . 'customer/view/' . $b_list->customer_id . '">Details</a>] </td>'
                                . '<td>' . $a_address . '</td>'
                                . '<td>' . $b_list->shift . '</td>'
                                . '<td>' . $booking_type . '</td>
                                            <td>

                                                <i class="fa fa-phone"></i> ' . $b_list->mobile_number_1 . '
                                                <br>
                                                <i class="fa fa-envelope"></i> ' . $b_list->email_address . '
                                            </td>
                                            <td class="' . $b_list->booking_id . '-' . strtotime($datejob) . 'complaintclass">';
                            if (empty($checkcomplaint)) {
                                $html .= '<span class="btn add-complaint-job" onclick="addcomplaint(' . $b_list->booking_id . ',' . strtotime($datejob) . ')"><i class="fa fa-plus"></i></span>';
                            } else {
                                $html .= '<span class="btn edit-complaint-job" onclick="editcomplaint(' . $checkcomplaint->cmp_id . ')"><i class="fa fa-pencil"></i></span>
                                                <span class="btn view-complaint-job" onclick="viewcomplaint(' . $checkcomplaint->cmp_id . ')"><i class="fa fa-eye"></i></span>';
                            }
                            $html .= '</td>
                                            </tr>';
                            $i++;
                        }
                    }
                }
                //                 else {
                //                    $html .= '<tr>
                //                                <td class="dataTables_empty" colspan="7" valign="top">No data available in table</td></tr>';
                //                }
                echo $html;
                exit();
            }
            if ($this->input->post('action') && $this->input->post('action') == 'missed') {
                $datejob = $this->input->post('servicedate');
                $booking_list = $this->bookings_model->get_delayed_schedule_by_date_job($datejob);
                $i = 1;
                $html = "";
                if (!empty($booking_list)) {
                    foreach ($booking_list as $b_list) {
                        //                    $cur_time = strtotime(date('H:i:s'));
                        //                    $db_time = strtotime('13:28:05');
                        //                    if (($cur_time - $db_time) > (15 * 60)) {
                        //                        echo "15 mins has passed";
                        //                    } else {
                        //                        echo "not";
                        //                    }
                        $fromtimes = strtotime($b_list->timefrom);
                        $cur_time = strtotime(date('H:i:s'));
                        if (($cur_time - $fromtimes) > (15 * 60)) {
                            $checkcomplaint = $this->bookings_model->getjobcomplaintbyid($b_list->booking_id, $datejob);
                            if ($b_list->booking_type == "WE") {
                                $booking_type = "Weekly";
                            } else if ($b_list->booking_type == "OD") {
                                $booking_type = "One Day";
                            }

                            if ($b_list->customer_address == "") {
                                $a_address = 'Building - ' . $b_list->building . ', ' . $b_list->unit_no . '' . $b_list->street;
                            } else {
                                $a_address = $b_list->customer_address;
                            }
                            $html .= '<tr>
                                    <td></td>
                                    <td><a href="' . base_url() . 'activity/job_view/' . $datejob . '/' . $b_list->booking_id . '">' . $i . '</a></td>'
                                . '<td>' . $b_list->customer_name . ' [<a href="' . base_url() . 'customer/view/' . $b_list->customer_id . '">Details</a>] </td>'
                                . '<td>' . $a_address . '</td>'
                                . '<td>' . $b_list->shift . '</td>'
                                . '<td>' . $booking_type . '</td>
                                            <td>

                                                <i class="fa fa-phone"></i> ' . $b_list->mobile_number_1 . '
                                                <br>
                                                <i class="fa fa-envelope"></i> ' . $b_list->email_address . '
                                            </td>
                                            <td class="' . $b_list->booking_id . '-' . strtotime($datejob) . 'complaintclass">';
                            if (empty($checkcomplaint)) {
                                $html .= '<span class="btn add-complaint-job" onclick="addcomplaint(' . $b_list->booking_id . ',' . strtotime($datejob) . ')"><i class="fa fa-plus"></i></span>';
                            } else {
                                $html .= '<span class="btn edit-complaint-job" onclick="editcomplaint(' . $checkcomplaint->cmp_id . ')"><i class="fa fa-pencil"></i></span>
                                                <span class="btn view-complaint-job" onclick="viewcomplaint(' . $checkcomplaint->cmp_id . ')"><i class="fa fa-eye"></i></span>';
                            }
                            $html .= '</td>
                                            </tr>';
                            $i++;
                        }
                    }
                }
                //                $datejob = $this->input->post('servicedate');
                //                $missed_list = $this->bookings_model->get_all_booking_approval_list_job('Expired',$datejob);
                //                $i=1;
                //                $html = "";
                //                if(!empty($missed_list))
                //                {
                //                foreach($missed_list as $m_list)
                //                {
                //                    if($m_list->booking_type == "WE")
                //                    {
                //                        $booking_type = "Weekly";
                //                    } else if($m_list->booking_type == "OD"){
                //                        $booking_type = "One Day";
                //                    }
                //                    $html .= '<tr>
                //                                <td></td>
                //                                <td><a href="'. base_url().'activity/job_view/'.$datejob.'/'.$m_list->booking_id.'">'.$i.'</a></td>'
                //                            . '<td>'.$m_list->customer_name.' [<a href="'.base_url().'customer/view/'.$m_list->customer_id.'">Details</a>] </td>'
                //                            . '<td>'.$m_list->customer_address.'</td>'
                //                            . '<td>'.$m_list->shift.'</td>'
                //                            . '<td>'.$booking_type.'</td>
                //                                        <td>
                //                                            <i class="fa fa-phone"></i> '.$m_list->mobile_number_1.'
                //                                            <br>
                //                                            <i class="fa fa-envelope"></i> '.$m_list->email_address.'
                //                                        </td>
                //                                        </tr>';
                //                    $i++;
                //                }
                //                } 
                //                else {
                //                    $html .= '<tr>
                //                                <td class="dataTables_empty" colspan="7" valign="top">No data available in table</td></tr>';
                //                }
                echo $html;
                exit();
            }
            if ($this->input->post('action') && $this->input->post('action') == 'cancelled') {
                $datejob = $this->input->post('servicedate');
                $cancel_list = $this->day_services_model->get_schedule_progress_date_job($datejob, 3);
                $i = 1;
                $html = "";
                if (!empty($cancel_list)) {
                    foreach ($cancel_list as $c_list) {
                        if ($c_list->booking_type == "WE") {
                            $booking_type = "Weekly";
                        } else if ($c_list->booking_type == "OD") {
                            $booking_type = "One Day";
                        }

                        if ($c_list->customer_address == "") {
                            $a_address = 'Building - ' . $c_list->building . ', ' . $c_list->unit_no . '' . $c_list->street;
                        } else {
                            $a_address = $c_list->customer_address;
                        }

                        $html .= '<tr>
                                <td></td>
                                <td><a href="' . base_url() . 'activity/job_view/' . $datejob . '/' . $c_list->booking_id . '">' . $i . '</a></td>'
                            . '<td>' . $c_list->customer_name . ' [<a href="' . base_url() . 'customer/view/' . $c_list->customer_id . '">Details</a>] </td>'
                            . '<td>' . $a_address . '</td>'
                            . '<td>' . date("h:i A", strtotime($c_list->time_from)) . ' - ' . date("h:i A", strtotime($c_list->time_to)) . '</td>'
                            . '<td>' . $booking_type . '</td>
                                        <td>
                                            <i class="fa fa-phone"></i> ' . $c_list->mobile_number_1 . '
                                            <br>
                                            <i class="fa fa-envelope"></i> ' . $c_list->email_address . '
                                        </td>
                                        </tr>';
                        $i++;
                    }
                }
                //                 else {
                //                    $html .= '<tr>
                //                                <td class="dataTables_empty" colspan="7" valign="top">No data available in table</td></tr>';
                //                }
                echo $html;
                exit();
            }
            if ($this->input->post('action') && $this->input->post('action') == 'all') {

                $startdate = DateTime::createFromFormat('d/m/Y', $this->input->post('startdate'));
                $startdatejob = $startdate->format('Y-m-d');
                $enddate = DateTime::createFromFormat('d/m/Y', $this->input->post('enddate'));
                $enddatejob = $enddate->format('Y-m-d');

                $day = 86400; // Day in seconds  
                $format = 'Y-m-d'; // Output format (see PHP date funciton)  
                $sTime = strtotime($startdatejob); // Start as time  
                $eTime = strtotime($enddatejob); // End as time  
                $numDays = round(($eTime - $sTime) / $day) + 1;
                $days = array();
                $html = "";
                $i = 1;
                for ($d = 0; $d < $numDays; $d++) {
                    $days = date($format, ($sTime + ($d * $day)));
                    $all_list = $this->day_services_model->get_activity_by_date_plan_job($days);


                    if (!empty($all_list)) {
                        foreach ($all_list as $a_list) {
                            if ($a_list->booking_type == "WE") {
                                $booking_type = "Weekly";
                            } else if ($a_list->booking_type == "OD") {
                                $booking_type = "One Day";
                            }
                            if ($a_list->booking_status == 0) {
                                $status = "New";
                            } else if ($a_list->booking_status == 1) {
                                if ($a_list->service_status == 1) {
                                    $status = "In Progress";
                                } else if ($a_list->service_status == 2) {
                                    $status = "Finished";
                                } else if ($a_list->service_status == 3) {
                                    $status = "Cancelled";
                                } else {
                                    $status = "Scheduled";
                                }
                            } else if ($a_list->booking_status == 2) {
                                $status = "Deleted";
                            }

                            if ($a_list->customer_address == "") {
                                $a_address = 'Building - ' . $a_list->building . ', ' . $a_list->unit_no . '' . $a_list->street;
                            } else {
                                $a_address = $a_list->customer_address;
                            }

                            $checkcomplaint = $this->bookings_model->getjobcomplaintbyid($a_list->booking_id, $days);
                            $html .= '<tr>
                                    <td></td>
                                    <td><a href="' . base_url() . 'activity/job_view/' . $days . '/' . $a_list->booking_id . '">' . $i . '</a></td>'
                                . '<td>' . $a_list->customer_name . ' [<a href="' . base_url() . 'customer/view/' . $a_list->customer_id . '">Details</a>] </td>'
                                . '<td>' . $a_address . '</td>'
                                . '<td>' . $days . '</td>'
                                . '<td>' . date("h:i A", strtotime($a_list->time_from)) . ' - ' . date("h:i A", strtotime($a_list->time_to)) . '</td>'
                                . '<td>' . $booking_type . '</td>
                                            <td>
                                                <i class="fa fa-phone"></i> ' . $a_list->mobile_number_1 . '
                                                <br>
                                                <i class="fa fa-envelope"></i> ' . $a_list->email_address . '
                                            </td>
                                            <td>' . $status . '</td>
                                <td class="' . $a_list->booking_id . '-' . strtotime($days) . 'complaintclass">';
                            if (empty($checkcomplaint)) {
                                $html .= '<span class="btn add-complaint-job" onclick="addcomplaint(' . $a_list->booking_id . ',' . strtotime($days) . ')"><i class="fa fa-plus"></i></span>';
                            } else {
                                $html .= '<span class="btn edit-complaint-job" onclick="editcomplaint(' . $checkcomplaint->cmp_id . ')"><i class="fa fa-pencil"></i></span>
                                            <span class="btn view-complaint-job" onclick="viewcomplaint(' . $checkcomplaint->cmp_id . ')"><i class="fa fa-eye"></i></span>';
                            }
                            $html .= '</td>
                                </tr>';
                            $i++;
                        }
                    }
                }


                //$all_list = $this->day_services_model->get_activity_by_date_plan_job($datejob);

                //                else {
                //                    $html .= '<tr>
                //                                <td class="dataTables_empty" colspan="7" valign="top">No data available in table</td></tr>';
                //                }
                echo $html;
                exit();
            }
            if ($this->input->post('action') && $this->input->post('action') == 'add-complaint-by-id') {
                $booking_id = $this->input->post('booking_id');
                $services_date = date('Y-m-d', $this->input->post('service_date'));
                if ($this->input->post('complaint') != "") {
                    $complaints = $this->input->post('complaint');
                } else {
                    $complaints = "";
                }
                $addeddate = date('Y-m-d H:i:s');
                $complaint_b_fields = array();
                $complaint_b_fields['booking_id'] = $booking_id;
                $complaint_b_fields['ds_id'] = 0;
                $complaint_b_fields['service_date'] = $services_date;
                $complaint_b_fields['complaint'] = $complaints;
                $complaint_b_fields['added_by'] = user_authenticate();
                $complaint_b_fields['created_date'] = $addeddate;

                $complaint_id = $this->bookings_model->add_complaints($complaint_b_fields);
                if ($complaint_id) {
                    $response = array();
                    $response['status'] = 'success';
                    $response['complaint_id'] = $complaint_id;
                } else {
                    $response = array();
                    $response['status'] = 'error';
                }
                echo json_encode($response);
                exit();
            }
            if ($this->input->post('action') && $this->input->post('action') == 'view-complaint-by-id') {
                $complaintid = $this->input->post(complaint_id);
                $getcomplaintbyid = $this->bookings_model->get_complaint_by_cid($complaintid);
                if ($getcomplaintbyid) {
                    $addeddate = date("d/M/Y", strtotime($getcomplaintbyid->created_date));
                    $addedtime = date("h:iA", strtotime($getcomplaintbyid->created_date));
                    $response = array();
                    $response['status'] = 'success';
                    $response['complaint'] = $getcomplaintbyid->complaint;
                    $response['added_by'] = $getcomplaintbyid->user_fullname;
                    $response['added_date'] = $addeddate;
                    $response['added_time'] = $addedtime;
                } else {
                    $response = array();
                    $response['status'] = 'error';
                }
                echo json_encode($response);
                exit();
            }
            if ($this->input->post('action') && $this->input->post('action') == 'edit-complaint-by-id') {
                $complaintid = $this->input->post(complaint_id);
                $getcomplaintbyid = $this->bookings_model->get_complaint_by_cid($complaintid);
                if ($getcomplaintbyid) {
                    //$addeddate = date("d/M/Y", strtotime($getcomplaintbyid->created_date));
                    //$addedtime = date("h:iA", strtotime($getcomplaintbyid->created_date));
                    $response = array();
                    $response['status'] = 'success';
                    $response['complaint'] = $getcomplaintbyid->complaint;
                    //$response['added_by'] = $getcomplaintbyid->user_fullname;
                    //$response['added_date'] = $addeddate;
                    //$response['added_time'] = $addedtime;
                } else {
                    $response = array();
                    $response['status'] = 'error';
                }
                echo json_encode($response);
                exit();
            }
            if ($this->input->post('action') && $this->input->post('action') == 'update-complaint-by-id') {
                $complaint_id = $this->input->post('complaint_id');
                if ($this->input->post('complaint') != "") {
                    $complaints = $this->input->post('complaint');
                } else {
                    $complaints = "";
                }
                $addeddate = date('Y-m-d H:i:s');
                $complaint_b_fields = array();
                $complaint_b_fields['ds_id'] = 0;
                $complaint_b_fields['complaint'] = $complaints;
                $complaint_b_fields['added_by'] = user_authenticate();
                $complaint_b_fields['created_date'] = $addeddate;

                $complaint_id = $this->bookings_model->update_complaints($complaint_b_fields, $complaint_id);
                if ($complaint_id) {
                    $response = array();
                    $response['status'] = 'success';
                } else {
                    $response = array();
                    $response['status'] = 'error';
                }
                echo json_encode($response);
                exit();
            }
        }
        $data = array();
        $data['formatted_date'] = $date;
        $data['service_date'] = date('d/m/Y', strtotime($date));

        $data['search_date_from_job'] = date('d/m/Y', strtotime($date_from_job));
        $data['search_date_to_job'] = date('d/m/Y', strtotime($date_to_job));

        $data['approval_list'] = $this->bookings_model->get_all_booking_approval_list('Pending');


        $layout_data = array();
        $layout_data['content_body'] = $this->load->view('jobs', $data, TRUE);
        $layout_data['page_title'] = 'Jobs';
        $layout_data['meta_description'] = 'Jobs';
        $layout_data['css_files'] = array('demo.css', 'jquery.fancybox.css');
        $layout_data['jobs_active'] = '1';
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.fancybox.pack.js', 'base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'moment.min.js', 'jquery.dataTables.min.js', 'dataTables.responsive.min.js', 'dataTables.checkboxes.min.js');

        $this->load->view('layouts/default_dashboard', $layout_data);
    }

    public function job_view()
    {
        if ($this->uri->segment(3) && strlen(trim($this->uri->segment(3) > 0))) {
            $schedule_date = $this->uri->segment(3);
            //            $s_date = explode('-', trim($this->uri->segment(3)));
            //            if(count($s_date) == 3 && checkdate($s_date[1], $s_date[0], $s_date[2]) && $s_date[2] >= 2014 && $s_date[2] <= date('Y') + 2)
            //            {
            //                $schedule_date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
            //            }				
        } else {
            $schedule_date = date('Y-m-d');
            //$schedule_date = date('d-M-Y', strtotime('-1 day'));
        }

        if ($this->uri->segment(4) && strlen(trim($this->uri->segment(4) > 0))) {
            $booking_id = $this->uri->segment(4);
        } else {
            $booking_id = '';
        }
        if ($this->input->is_ajax_request()) {
            $booking_id = $this->input->post('booking_id');
            //$s_date = explode("/", $this->input->post('service_date'));
            //$service_date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
            $service_date = $this->input->post('service_date');

            $chk_day_service = $this->day_services_model->get_day_service_by_booking_id($service_date, $booking_id);
            $chk_booking = $this->bookings_model->get_booking_by_id($booking_id);

            if ($this->input->post('action') && $this->input->post('action') == 'service-start') {
                if (isset($chk_day_service->day_service_id)) {
                    /*
                    $response = array();
                    $response['status'] = 'error';
                    $response['error_code'] = '106';
                    $response['message'] = $chk_day_service->service_status == 1 ? 'Service already started' : 'Service already finished';
                    echo json_encode($response);
                    exit();
                    */
                    $ds_fields = array();
                    $ds_fields['start_time'] = date('H:i:s');
                    $ds_fields['service_status'] = 1;
                    $ds_fields['service_added_by'] = 'B';
                    $ds_fields['service_added_by_id'] = user_authenticate();

                    $this->day_services_model->update_day_service($chk_day_service->day_service_id, $ds_fields);
                    $this->day_services_model->add_activity($chk_booking, user_authenticate(), 'Customer_IN');

                    $response = array();
                    $response['status'] = 'success';
                    $response['booking_id'] = $booking_id;
                    $response['service_status'] = 1;

                    echo json_encode($response);
                    exit();
                } else {
                    $booking = $this->bookings_model->get_booking_by_id($booking_id);

                    $normal_hours = 0;
                    $extra_hours = 0;
                    $weekend_hours = 0;

                    $normal_from = strtotime('08:00:00');
                    $normal_to = strtotime('18:00:00');

                    $shift_from = strtotime($booking->time_from . ':00');
                    $shift_to = strtotime($booking->time_to . ':00');

                    $total_hours = ($shift_to - $shift_from) / 3600;
                    if ($booking->cleaning_material == 'Y') {
                        $materialfee = ($total_hours * 5);
                    } else {
                        $materialfee = 0;
                    }

                    //                                        if(date('w') >= 0 && date('w') < 5)      // except friday & saturday case
                    if (date('w') != 5)                        // except friday  case // changed on 23-01-2017
                    {
                        if ($shift_from < $normal_from) {
                            if ($shift_to <= $normal_from) {
                                $extra_hours = ($shift_to - $shift_from) / 3600;
                            }

                            if ($shift_to > $normal_from && $shift_to <= $normal_to) {
                                $extra_hours = ($normal_from - $shift_from) / 3600;
                                $normal_hours = ($shift_to - $normal_from) / 3600;
                            }

                            if ($shift_to > $normal_to) {
                                $extra_hours = ($normal_from - $shift_from) / 3600;
                                $extra_hours += ($shift_to - $normal_to) / 3600;
                                $normal_hours = ($normal_to - $normal_from) / 3600;
                            }
                        }

                        if ($shift_from >= $normal_from && $shift_from < $normal_to) {
                            if ($shift_to <= $normal_to) {
                                $normal_hours = ($shift_to - $shift_from) / 3600;
                            }

                            if ($shift_to > $normal_to) {
                                $normal_hours = ($normal_to - $shift_from) / 3600;
                                $extra_hours = ($shift_to - $normal_to) / 3600;
                            }
                        }

                        if ($shift_from > $normal_to) {
                            $extra_hours = ($shift_to - $shift_from) / 3600;
                        }
                    } else {
                        $weekend_hours = ($shift_to - $shift_from) / 3600;
                    }

                    $service_description = array();

                    $service_description['normal'] = new stdClass();
                    $service_description['normal']->hours = $normal_hours;
                    if ($booking->booked_from == "W") {
                        $service_description['normal']->fees = $normal_hours * $booking->price_per_hr;
                    } else {
                        $service_description['normal']->fees = $normal_hours * $booking->price_hourly;
                    }

                    $service_description['extra'] = new stdClass();
                    $service_description['extra']->hours = $extra_hours;
                    $service_description['extra']->fees = $extra_hours * $booking->price_extra;

                    $service_description['weekend'] = new stdClass();
                    $service_description['weekend']->hours = $weekend_hours;
                    $service_description['weekend']->fees = $weekend_hours * $booking->price_weekend;

                    /*
                     * Location charge
                     */
                    $location_charge = 0;
                    /*
                    $customer_address = $this->customers_model->get_customer_address_by_id($booking->customer_address_id);
                    if($customer_address){
                        $area = $this->areas_model->get_area_by_id($customer_address->area_id);
                        if($area){
                            $location_charge = $area->area_charge;
                        }
                    }*/

                    $total_fee = $service_description['normal']->fees + $service_description['extra']->fees + $service_description['weekend']->fees + $location_charge - $booking->discount;

                    $ds_fields = array();
                    $ds_fields['booking_id'] = $booking_id;
                    $ds_fields['customer_id'] = $booking->customer_id;
                    $ds_fields['customer_name'] = $booking->customer_name;
                    $ds_fields['customer_address'] = $booking->customer_address;
                    $ds_fields['customer_payment_type'] = $booking->payment_type;
                    $ds_fields['maid_id'] = $booking->maid_id;
                    $ds_fields['service_description'] = serialize($service_description);
                    //$ds_fields['total_fee'] = $total_fee;
                    $ds_fields['total_fee'] = $booking->total_amount;
                    $ds_fields['material_fee'] = $materialfee;
                    $ds_fields['service_date'] = $service_date;
                    $ds_fields['start_time'] = date('H:i:s');
                    $ds_fields['service_status'] = 1;
                    $ds_fields['service_added_by'] = 'B';
                    $ds_fields['service_added_by_id'] = user_authenticate();

                    $day_service = $this->day_services_model->add_day_service($ds_fields);
                    $this->day_services_model->add_activity($booking, user_authenticate(), 'Customer_IN');

                    if ($day_service) {
                        $response = array();
                        $response['status'] = 'success';
                        $response['booking_id'] = $booking_id;
                        $response['service_status'] = 1;

                        echo json_encode($response);
                        exit();
                    } else {
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '105';
                        $response['message'] = 'Unexpected error';

                        echo json_encode($response);
                        exit();
                    }
                }
            }

            if ($this->input->post('action') && $this->input->post('action') == 'transfer') {
                if ($this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0) {
                    $booking = $this->bookings_model->get_booking_by_id($this->input->post('booking_id'));
                    $users_id = user_authenticate();
                    if (user_authenticate() != 1) {
                        if ($booking->is_locked == 1 && $booking->booked_by != $users_id) {
                            $response = array();
                            $response['status'] = 'error';
                            $response['message'] = 'Booking is locked.';
                            echo json_encode($response);
                            exit();
                        }
                    }
                    $service_date_new = $this->input->post('service_date');
                    $check_booking_transferred = $this->bookings_model->check_booking_transferred_new($this->input->post('booking_id'), $service_date_new);
                    if (count($check_booking_transferred) > 0) {
                        $transferids = $check_booking_transferred->booking_transfer_tablet_id;
                    } else {
                        $transferids = 0;
                    }
                    $datetimes = date('Y-m-d H:i:s');
                    //$dateservice = $this->input->post('dateservice');
                    $toid = $this->input->post('tablet_id');
                    $driver_chng_array = array();
                    $driver_chng_array['booking_id'] = $this->input->post('booking_id');
                    $driver_chng_array['transfering_to_tablet'] = $toid;
                    $driver_chng_array['service_date'] = $service_date_new;
                    $driver_chng_array['transferred_date_time'] = $datetimes;
                    $driver_chng_array['transferred_from'] = 'B';
                    $driver_chng_array['transferred_from_id'] = $booking->tabletid;
                    $driver_chng_array['transferred_by'] = $users_id;
                    $activityid = $this->bookings_model->insert_driverchnage($driver_chng_array, $transferids);
                    if ($activityid > 0) {
                        if ($toid > 0) {
                            $push_fields = array();
                            $push_fields['tab_id'] = $toid;
                            $push_fields['type'] = 1;
                            $push_fields['message'] = 'Booking transferred. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->newtime_from . '-' . $booking->newtime_to;
                            // code moved starts here
                            $push_fields['title'] = "Booking Transfered";
                            $push_fields['customer_name'] = $booking->customer_name;
                            $push_fields['maid_name'] = $booking->maid_name;
                            $push_fields['booking_time'] = $booking->newtime_from . ' - ' . $booking->newtime_to;
                            // code moved ends here
                            if (strtotime($service_date_new) <= strtotime(date('Y-m-d'))) {
                                $push = $this->bookings_model->add_push_notifications($push_fields);
                            }
                            $tablet = $this->tablets_model->get_tablet_by_zone_tabid($toid);
                            $getmaiddeviceid = $this->maids_model->get_maid_by_id($booking->maid_id);
                            if ($tablet) {
                                if (strtotime($service_date_new) <= strtotime(date('Y-m-d'))) {
                                    $deviceid = $tablet->google_reg_id;
                                    // optional payload
                                    $payload = array();
                                    $payload['isfeedback'] = false;
                                    if (isset($push) && $push > 0) {
                                        $payload['pushid'] = $push;
                                    } else {
                                        $payload['pushid'] = 0;
                                    }

                                    $title = "Booking Transferred";
                                    $message = 'Booking transferred. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->newtime_from . '-' . $booking->newtime_to;
                                    $res = array();
                                    $res['title'] = $title;
                                    $res['is_background'] = false;
                                    $res['body'] = $message;
                                    $res['image'] = "";
                                    $res['payload'] = $payload;
                                    $res['customer'] = $booking->customer_name;
                                    $res['maid'] = $booking->maid_name;
                                    $res['bookingTime'] = $booking->newtime_from . ' - ' . $booking->newtime_to;
                                    $res['timestamp'] = date('Y-m-d G:i:s');
                                    $regId = $deviceid;
                                    $fields = array(
                                        'to' => $regId,
                                        'notification' => $res,
                                        'data' => $res,
                                    );
                                    $return = android_customer_app_push($fields);
                                    if ($getmaiddeviceid->device_id != "") {
                                        $maid_fields = array(
                                            'to' => $getmaiddeviceid->device_id,
                                            'notification' => $res,
                                            'data' => $res,
                                        );
                                        android_customer_app_push($maid_fields);
                                    }
                                }
                            }
                        }
                        $response = array();
                        $response['status'] = 'success';
                        echo json_encode($response);
                        exit();
                    } else {
                        $response = array();
                        $response['status'] = 'error';
                        $response['message'] = 'Something went wrong...';
                        echo json_encode($response);
                        exit();
                    }
                }
            }

            if ($this->input->post('action') && $this->input->post('action') == 'transfer-old') {

                if ($this->input->post('zone_id') != FALSE &&  $this->input->post('zone_id') != 0  && $this->input->post('zone_id') != '') {
                    $zone_id = trim($this->input->post('zone_id'));
                    $zone = $this->zones_model->get_zone_by_id($zone_id);
                    $maid_id = $chk_booking->maid_id;

                    if (isset($zone->zone_id) && $zone->zone_status == 1) {
                        $booking_transfers = $this->bookings_model->get_booking_transfers_by_date($service_date);
                        $yes_transferred_booking_zones = array();
                        $no_transferred_booking_zones = array();
                        foreach ($booking_transfers as $b_transfer) {
                            if ($b_transfer->zone_id == $chk_booking->zone_id) {
                                $chk_booking = $this->bookings_model->get_booking_by_id($b_transfer->booking_id);

                                if (isset($chk_booking->maid_id) && $chk_booking->maid_id == $maid_id) {
                                    $yes_transferred_booking_zones[$b_transfer->booking_transfer_id] = $b_transfer->booking_id;
                                }
                            } else {
                                $no_transferred_booking_zones[$b_transfer->booking_id] = $b_transfer->zone_id;
                            }
                        }

                        $bookings = $this->bookings_model->get_maid_bookings_on_zone_by_date($maid_id, $chk_booking->zone_id, $service_date);

                        $transferred_bookings  = array();
                        foreach ($bookings as $booking) {
                            if (!isset($no_transferred_booking_zones[$booking->booking_id])) {
                                $b_transfer_fields = array();
                                $b_transfer_fields['booking_id'] = $booking->booking_id;
                                $b_transfer_fields['zone_id'] = $zone->zone_id;
                                $b_transfer_fields['service_date'] = $service_date;
                                $b_transfer_fields['transferred_time'] = date('H:i:s');
                                $b_transfer_fields['transferred_by'] = 'B';
                                $b_transfer_fields['transferred_by_id'] = user_authenticate();

                                $this->bookings_model->add_booking_transfer($b_transfer_fields);
                                $this->day_services_model->add_activity($chk_booking, user_authenticate(), 'Transfer', '', $zone->zone_name);

                                $transferred_bookings[] = $booking->booking_id;
                            }
                        }

                        foreach ($yes_transferred_booking_zones as $booking_transfer_id => $booking_id) {
                            $b_transfer_fields = array();
                            $b_transfer_fields['booking_id'] = $booking_id;
                            $b_transfer_fields['zone_id'] = $zone->zone_id;
                            $b_transfer_fields['service_date'] = $service_date;
                            $b_transfer_fields['transferred_time'] = date('H:i:s');
                            $b_transfer_fields['transferred_by'] = 'B';
                            $b_transfer_fields['transferred_by_id'] = user_authenticate();

                            $this->bookings_model->add_booking_transfer($b_transfer_fields);
                            $this->day_services_model->add_activity($chk_booking, user_authenticate(), 'Transfer', '', $zone->zone_name);

                            $this->bookings_model->delete_booking_transfer($booking_transfer_id);

                            $transferred_bookings[] = $booking_id;
                        }

                        /* Check if maid already in */
                        $chk_attandence = $this->maids_model->get_maid_attandence_by_date($maid_id, $service_date);
                        if (isset($chk_attandence->attandence_id) && $chk_attandence->attandence_status == 1) {
                            $maid_out_fields = array();
                            $maid_out_fields['maid_out_time'] = date('H:i:s');
                            $maid_out_fields['attandence_status'] = 2;

                            $maid_out = $this->maids_model->update_maid_attandence($chk_attandence->attandence_id, $maid_out_fields);
                            $this->day_services_model->add_activity($chk_booking, user_authenticate(), 'Maid_OUT');
                        }

                        if (count($transferred_bookings) > 0) {
                            $response = array();
                            $response['status'] = 'success';
                            $response['maid_id'] = $maid_id;
                            $response['service_date'] = $service_date;
                            $response['bookings'] = $transferred_bookings;
                        } else {
                            $response = array();
                            $response['status'] = 'success';
                            $response['maid_id'] = $maid_id;
                            $response['service_date'] = $service_date;
                            $response['bookings'] = $transferred_bookings;
                        }

                        $tzone_tablet = $this->tablets_model->get_tablet_by_zone($zone->zone_id);

                        if (isset($tzone_tablet->google_reg_id)) {
                            //android_push(array($tzone_tablet->google_reg_id), array('action' => 'transfer-maid', 'message' => 'Maid Transfered'));
                        }

                        echo json_encode($response);
                        exit();
                    } else if (isset($zone->zone_id) && $zone->zone_status == 0 && $zone->zone_id != $chk_booking->zone_id) {
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '104';
                        $response['message'] = 'Zone not active';

                        echo json_encode($response);
                        exit();
                    } else {
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '105';
                        $response['message'] = 'Invalid zone id';

                        echo json_encode($response);
                        exit();
                    }
                } else {    //$zone_id = trim($this->input->post('zone_id'));
                    $response = array();
                    $response['status'] = 'error';
                    $response['error_code'] = '101';
                    $response['message'] = 'Invalid request. Invalid zone.';

                    echo json_encode($response);
                    exit();
                }
            }
            if ($this->input->post('action') && $this->input->post('action') == 'service-stop') {

                if ($this->input->post('payment') !== FALSE && ($this->input->post('payment') == 0 ||  $this->input->post('payment') == 3 ||  $this->input->post('payment') == 1)) {
                    $payment_status = $this->input->post('payment');
                } else {    //$zone_id = trim($this->input->post('zone_id'));
                    $response = array();
                    $response['status'] = 'error';
                    $response['error_code'] = '101';
                    $response['message'] = 'Invalid request. Invalid payment status.';

                    echo json_encode($response);
                    exit();
                }

                if (isset($chk_day_service->day_service_id) && isset($payment_status)) {

                    if ($chk_day_service->service_status == 1 || $chk_day_service->service_status == 2) {
                        if ($payment_status == 1) {
                            if ($this->input->post('amount') && is_numeric($this->input->post('amount')) &&  $this->input->post('amount') > 0) {
                                $amount = $this->input->post('amount');
                                if ($this->input->post('psno') && is_numeric($this->input->post('psno')) &&  $this->input->post('psno') > 0) {
                                    $psno = $this->input->post('psno');
                                } else {
                                    $psno = "";
                                }
                                /*if($amount < $chk_day_service->total_fee)
                                {
                                        $response = array();
                                        $response['status'] = 'error';
                                        $response['error_code'] = '99';
                                        $response['message'] = 'Customer has to pay AED' . $chk_day_service->total_fee;

                                        echo json_encode($response);
                                        exit();
                                }*/

                                $payment_method = 0;
                                //if($this->input->post('method') && is_numeric($this->input->post('method')) &&  $this->input->post('method') > 0)
                                //{
                                //$payment_method = 1;
                                //}

                                $chk_payment = $this->day_services_model->get_payment_by_day_service_id($service_date, $chk_day_service->day_service_id);

                                if (isset($chk_payment) && $chk_payment->payment_id) {
                                    $payment_fields = array();
                                    $payment_fields['customer_id'] = $chk_day_service->customer_id;
                                    $payment_fields['day_service_id'] = $chk_day_service->day_service_id;
                                    $payment_fields['paid_amount'] = $amount;
                                    $payment_fields['receipt_no'] = $psno;
                                    //$payment_fields['paid_at'] = 'B';
                                    //$payment_fields['paid_at_id'] = user_authenticate();
                                    $payment_fields['payment_method'] = $payment_method;


                                    $add_payment = $this->day_services_model->update_payment($chk_payment->payment_id, $payment_fields);
                                } else {
                                    $payment_fields = array();
                                    $payment_fields['customer_id'] = $chk_day_service->customer_id;
                                    $payment_fields['day_service_id'] = $chk_day_service->day_service_id;
                                    $payment_fields['paid_amount'] = $amount;
                                    $payment_fields['receipt_no'] = $psno;
                                    $payment_fields['paid_at'] = 'B';
                                    $payment_fields['paid_at_id'] = user_authenticate();
                                    $payment_fields['payment_method'] = $payment_method;
                                    $payment_fields['paid_datetime'] = date('Y-m-d H:i:s', strtotime($service_date));

                                    $add_payment = $this->customers_model->add_customer_payment($payment_fields);
                                }


                                $this->day_services_model->add_activity($chk_booking, user_authenticate(), 'Customer_OUT');
                                $this->day_services_model->add_activity($chk_booking, user_authenticate(), 'Payment', $amount);
                            } else {
                                $response = array();
                                $response['status'] = 'error';
                                $response['error_code'] = '101';
                                $response['message'] = 'Invalid request. Invalid amount.';

                                echo json_encode($response);
                                exit();
                            }
                        } else if ($chk_day_service->payment_type == 'D') {
                            // Daily paying custmer, not paid. Send notification to manager
                            //$this->push_notification(3);
                        }
                        if ($payment_status == 3) {
                            // service not done
                            /*if(isset($chk_day_service->day_service_id))
                            {*/
                            if ($chk_day_service->service_status == 1) {
                                $ds_fields = array();
                                $ds_fields['end_time'] = date('H:i:s');
                                $ds_fields['service_status'] = 3;

                                $this->day_services_model->update_day_service($chk_day_service->day_service_id, $ds_fields);
                                $this->day_services_model->add_activity($chk_booking, user_authenticate(), 'Service_Cancel');

                                //$this->push_notification(2);

                                $response = array();
                                $response['status'] = 'success';
                                $response['booking_id'] = $booking_id;
                                $response['service_status'] = 3;
                                $response['payment_status'] = $payment_status;
                                echo json_encode($response);
                                exit();
                            } else {
                                $response = array();
                                $response['status'] = 'error';
                                $response['error_code'] = '106';
                                $response['message'] = 'Service already finished';

                                echo json_encode($response);
                                exit();
                            }
                            /*}
                            else
                            {
                                    $response = array();
                                    $response['status'] = 'error';
                                    $response['error_code'] = '106';
                                    $response['message'] = 'Service not started';

                                    echo json_encode($response);
                                    exit();
                            }*/
                        } else {
                            $ds_fields = array();
                            $ds_fields['end_time'] = date('H:i:s');
                            $ds_fields['service_status'] = 2;
                            $ds_fields['payment_status'] = isset($add_payment) && $add_payment ? 1 : 0;

                            if ($ds_fields['payament_status'] === 0) {
                                $chk_payment = $this->day_services_model->get_payment_by_day_service_id($service_date, $chk_day_service->day_service_id);

                                if (isset($chk_payment) && $chk_payment->payment_id) {
                                    $this->day_services_model->delete_payment($chk_payment->payment_id);
                                }
                            }
                            $this->day_services_model->update_day_service($chk_day_service->day_service_id, $ds_fields);
                            $this->day_services_model->add_activity($chk_booking, user_authenticate(), 'Customer_OUT');
                            if (isset($add_payment) && $add_payment)
                                $this->day_services_model->add_activity($chk_booking, user_authenticate(), 'Payment', $amount);

                            $response = array();
                            $response['status'] = 'success';
                            $response['booking_id'] = $booking_id;
                            $response['service_status'] = 2;
                            $response['payment_status'] = $payment_status;

                            echo json_encode($response);
                            exit();
                        }
                    } else {
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '106';
                        $response['message'] = 'Service already finished';

                        echo json_encode($response);
                        exit();
                    }
                } else {
                    $response = array();
                    $response['status'] = 'error';
                    $response['error_code'] = '106';
                    $response['message'] = 'Service not started';

                    echo json_encode($response);
                    exit();
                }
            }
            if ($this->input->post('action') && $this->input->post('action') == 'delete-booking-permanent-job' && $this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0) {
                $booking_id = trim($this->input->post('booking_id'));
                $servce_date = date('Y-m-d', strtotime($this->input->post('service_date')));

                $checkservicestart = $this->day_services_model->get_day_service_by_booking_id($servce_date, $booking_id);

                if (!empty($checkservicestart)) {
                    echo 'start';
                    exit();
                }

                $d_booking = $this->bookings_model->get_booking_by_id($booking_id);
                if (user_authenticate() != 1) {
                    if ($d_booking->is_locked == 1 && $d_booking->booked_by != user_authenticate()) {
                        echo 'locked';
                        exit();
                    }
                }

                if (isset($d_booking->booking_id)) {
                    if ($d_booking->booking_type == 'OD') {
                        $this->bookings_model->update_booking($booking_id, array('booking_status' => 2), 'Delete');
                    } else {
                        //$this->bookings_model->update_booking($booking_id, array('service_actual_end_date' => date('Y-m-d', strtotime($schedule_date . ' - 1 day')), 'service_end' => 1));$ser_ac_end_date=date('Y-m-d', strtotime($schedule_date . ' - 1 day'));
                        if ($d_booking->booking_type == 'WE') {
                            $last_repeat_date = date('Y-m-d', strtotime($schedule_date . ' - 7 day'));
                            $ser_ac_end_date = date('Y-m-d', strtotime($schedule_date . ' - 6 day'));
                            $check_buk_delete = $this->bookings_model->check_booking_deletes_by_id_and_date($d_booking->booking_id, $last_repeat_date);
                            if (count($check_buk_delete) > 0) {
                                $ser_ac_end_date = date('Y-m-d', strtotime($schedule_date . ' - 8 day'));
                            }
                        } else if ($d_booking->booking_type == 'BW') {
                            $last_repeat_date = date('Y-m-d', strtotime($schedule_date . ' - 14 day'));
                            $ser_ac_end_date = date('Y-m-d', strtotime($schedule_date . ' - 13 day'));
                            $check_buk_delete = $this->bookings_model->check_booking_deletes_by_id_and_date($d_booking->booking_id, $last_repeat_date);
                            if (count($check_buk_delete) > 0) {
                                $ser_ac_end_date = date('Y-m-d', strtotime($schedule_date . ' - 15 day'));
                            }
                        }

                        //$this->bookings_model->update_booking($booking_id, array('service_actual_end_date' => $ser_ac_end_date, 'service_end' => 1), 'Delete');    
                        $this->bookings_model->update_booking($booking_id, array('booking_status' => 2, 'service_actual_end_date' => $ser_ac_end_date, 'service_end' => 1), 'Delete');
                    }
                    $delete_b_fields = array();
                    $delete_b_fields['booking_id'] = $booking_id;
                    $delete_b_fields['service_date'] = $service_date;
                    $delete_b_fields['deleted_by'] = user_authenticate();

                    $this->bookings_model->add_booking_cancel($delete_b_fields);

                    //if(($d_booking->booking_type == 'WE' && $d_booking->service_start_date == date('Y-m-d')) || ($d_booking->service_week_day == date('w') && strtotime(date('Y-m-d')) >= strtotime($d_booking->service_start_date) && strtotime(date('Y-m-d')) < strtotime($schedule_date)))
                    //{
                    $c_address = $this->customers_model->get_customer_address_by_id($d_booking->customer_address_id);
                    if (isset($c_address->zone_id)) {
                        $tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
                        $getmaiddeviceid = $this->maids_model->get_maid_by_id($d_booking->maid_id);
                        if ($tablet) {
                            $push_fields = array();
                            $push_fields['tab_id'] = $tablet->tablet_id;
                            $push_fields['type'] = 2;
                            $push_fields['message'] = 'Booking Cancelled. Maid : ' . $d_booking->maid_name . ', Customer : ' . $d_booking->customer_name . ', Shift : ' . $d_booking->newtime_from . '-' . $d_booking->newtime_to;
                            // code moved starts here
                            $push_fields['maid_id'] = $d_booking->maid_id;
                            $push_fields['title'] = "Booking Cancelled";
                            $push_fields['customer_name'] = $d_booking->customer_name;
                            $push_fields['maid_name'] = $d_booking->maid_name;
                            $push_fields['booking_time'] = $d_booking->newtime_from . ' - ' . $d_booking->newtime_to;
                            // code moved starts here
                            if (strtotime($schedule_date) <= strtotime(date('d-M-Y')))
                                $push = $this->bookings_model->add_push_notifications($push_fields);

                            if (strtotime($schedule_date) <= strtotime(date('d-M-Y')))
                                // $return = android_push(array($tablet->google_reg_id), array('action' => 'delete-booking', 'booking-ids' => array($booking_id), 'message' => $push_fields['message']));
                                // code moved starts here
                                $deviceid = $tablet->google_reg_id;
                            // optional payload
                            $payload = array();
                            $payload['isfeedback'] = false;
                            if (isset($push) && $push > 0) {
                                $payload['pushid'] = $push;
                            } else {
                                $payload['pushid'] = 0;
                            }

                            $title = "Booking Cancelled";
                            $message = 'Booking Cancelled. Maid : ' . $d_booking->maid_name . ', Customer : ' . $d_booking->customer_name . ', Shift : ' . $d_booking->newtime_from . '-' . $d_booking->newtime_to;
                            $res = array();
                            $res['title'] = $title;
                            $res['is_background'] = false;
                            $res['body'] = $message;
                            $res['image'] = "";
                            $res['payload'] = $payload;
                            $res['customer'] = $d_booking->customer_name;
                            $res['maid'] = $d_booking->maid_name;
                            $res['bookingTime'] = $d_booking->newtime_from . ' - ' . $d_booking->newtime_to;
                            $res['timestamp'] = date('Y-m-d G:i:s');
                            $regId = $deviceid;
                            $fields = array(
                                'to' => $regId,
                                'notification' => $res,
                                'data' => $res,
                            );
                            $return = android_customer_app_push($fields);
                            if ($getmaiddeviceid->maid_device_token != "") {
                                $maid_fields = array(
                                    'to' => $getmaiddeviceid->maid_device_token,
                                    'notification' => $res,
                                    'data' => $res,
                                );
                                android_customer_app_push($maid_fields);
                            }
                            // code moved ends here

                        }
                        //print_r($return);exit;
                    }
                    //}

                    echo 'success';
                    exit();
                }

                echo 'refresh';
                exit();
            }

            if ($this->input->post('action') && $this->input->post('action') == 'delete-booking-one-day-job' && $this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0) {
                $booking_id = trim($this->input->post('booking_id'));
                $servce_date = date('Y-m-d', strtotime($this->input->post('service_date')));

                $checkservicestart = $this->day_services_model->get_day_service_by_booking_id($servce_date, $booking_id);
                if (!empty($checkservicestart)) {
                    echo 'start';
                    exit();
                }

                $d_booking = $this->bookings_model->get_booking_by_id($booking_id);
                if (user_authenticate() != 1) {
                    if ($d_booking->is_locked == 1 && $d_booking->booked_by != user_authenticate()) {
                        echo 'locked';
                        exit();
                    }
                }

                if (isset($d_booking->booking_id)) {
                    if ($d_booking->booking_type != 'OD') {
                        $delete_b_fields = array();
                        $delete_b_fields['booking_id'] = $booking_id;
                        $delete_b_fields['service_date'] = $service_date;
                        $delete_b_fields['deleted_by'] = user_authenticate();

                        $this->bookings_model->add_booking_delete($delete_b_fields);
                    }
                }

                $c_address = $this->customers_model->get_customer_address_by_id($d_booking->customer_address_id);
                if (isset($c_address->zone_id)) {
                    $tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
                    $getmaiddeviceid = $this->maids_model->get_maid_by_id($d_booking->maid_id);
                    if ($tablet) {
                        $push_fields = array();
                        $push_fields['tab_id'] = $tablet->tablet_id;
                        $push_fields['type'] = 2;
                        $push_fields['message'] = 'Booking Cancelled. Maid : ' . $d_booking->maid_name . ', Customer : ' . $d_booking->customer_name . ', Shift : ' . $d_booking->newtime_from . '-' . $d_booking->newtime_to;
                        // code moved starts here
                        $push_fields['maid_id'] = $d_booking->maid_id;
                        $push_fields['title'] = "Booking Cancelled";
                        $push_fields['customer_name'] = $d_booking->customer_name;
                        $push_fields['maid_name'] = $d_booking->maid_name;
                        $push_fields['booking_time'] = $d_booking->newtime_from . ' - ' . $d_booking->newtime_to;
                        // code moved ends here
                        if (strtotime($schedule_date) <= strtotime(date('d-M-Y')))
                            $push = $this->bookings_model->add_push_notifications($push_fields);

                        if (strtotime($schedule_date) <= strtotime(date('d-M-Y'))) {

                            // android_push(array($tablet->google_reg_id), array('action' => 'delete-booking', 'booking-ids' => array($booking_id), 'message' => $push_fields['message']));
                            // code moved starts here
                            $deviceid = $tablet->google_reg_id;
                            // optional payload
                            $payload = array();
                            $payload['isfeedback'] = false;
                            if (isset($push) && $push > 0) {
                                $payload['pushid'] = $push;
                            } else {
                                $payload['pushid'] = 0;
                            }

                            $title = "Booking Cancelled";
                            $message = 'Cancelled Booking. Maid : ' . $d_booking->maid_name . ', Customer : ' . $d_booking->customer_name . ', Shift : ' . $d_booking->newtime_from . '-' . $d_booking->newtime_to;
                            $res = array();
                            $res['title'] = $title;
                            $res['is_background'] = false;
                            $res['body'] = $message;
                            $res['image'] = "";
                            $res['payload'] = $payload;
                            $res['customer'] = $d_booking->customer_name;
                            $res['maid'] = $d_booking->maid_name;
                            $res['bookingTime'] = $d_booking->newtime_from . ' - ' . $d_booking->newtime_to;
                            $res['timestamp'] = date('Y-m-d G:i:s');
                            $regId = $deviceid;
                            $fields = array(
                                'to' => $regId,
                                'notification' => $res,
                                'data' => $res,
                            );
                            $return = android_customer_app_push($fields);
                            if ($getmaiddeviceid->maid_device_token != "") {
                                $maid_fields = array(
                                    'to' => $getmaiddeviceid->maid_device_token,
                                    'notification' => $res,
                                    'data' => $res,
                                );
                                android_customer_app_push($maid_fields);
                            }
                            // code moved ends here
                        }
                    }
                }

                echo 'success';
                exit();
            }
        }

        $jobdetail = $this->day_services_model->get_activity_by_date_plan_job_view($schedule_date, $booking_id);

        $data = array();
        $data['jobdetail'] = $jobdetail;
        $data['jobdate'] = date('d F Y', strtotime($schedule_date));
        $data['scheduledates'] = $schedule_date;
        $data['zones'] = $this->day_services_model->get_zones();
        $data['tablets'] = $this->tablets_model->get_all_newdrivers();
        $layout_data = array();
        $layout_data['content_body'] = $this->load->view('job_view', $data, TRUE);
        $layout_data['page_title'] = 'Job Detail';
        $layout_data['meta_description'] = 'Job Detail';
        $layout_data['css_files'] = array('jquery.fancybox.css', 'demo.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.fancybox.pack.js', 'base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'moment.min.js', 'jquery.dataTables.min.js', 'dataTables.responsive.min.js', 'dataTables.checkboxes.min.js', 'bootstrap.js');

        $this->load->view('layouts/default_dashboard', $layout_data);
    }

    public function addjob()
    {
        $schedule_date = date('d-M-Y');
        $day_number = date('w', strtotime($schedule_date));
        $times = array();
        $current_hour_index = 0;
        $time = '12:00 am';
        $time_stamp = strtotime($time);

        for ($i = 0; $i < 24; $i++) {
            if (!isset($times['t-' . $i])) {
                $times['t-' . $i] = new stdClass();
            }

            $times['t-' . $i]->stamp = $time_stamp;
            $times['t-' . $i]->display = $time;

            //if(date('H') == $i && $service_date == date('Y-m-d'))
            if (date('H') == $i) {
                $current_hour_index = 't-' . ($i - 1);
            }

            $time_stamp = strtotime('+60mins', strtotime($time));
            $time = date('g:i a', $time_stamp);
        }
        if ($this->input->is_ajax_request()) {
            if ($this->input->post('action') && $this->input->post('action') == 'get-no-of-customer-address' && $this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0) {
                $customer_id = trim($this->input->post('customer_id'));
                $customer_addresses = $this->customers_model->get_customer_addresses($customer_id);
                echo json_encode(array("address" => $customer_addresses, "no_of_address" => count($customer_addresses)));
                //echo 'refresh';
                exit();
            }

            if ($this->input->post('action') && $this->input->post('action') == 'get-details-customer' && $this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0) {
                $customer_id = trim($this->input->post('customer_id'));
                $customer_details = $this->customers_model->get_customer_by_id($customer_id);
                echo json_encode($customer_details);
                exit();
            }

            if ($this->input->post('action') && $this->input->post('action') == 'get-customer-address' && $this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0) {
                $customer_id = trim($this->input->post('customer_id'));

                $customer_addresses = $this->customers_model->get_customer_addresses($customer_id);


                echo json_encode($customer_addresses);
                exit();
            }

            if ($this->input->post('action') && $this->input->post('action') == 'book-maid') {
                if ($this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0 && $this->input->post('customer_address_id') && is_numeric($this->input->post('customer_address_id')) && $this->input->post('customer_address_id') > 0 && $this->input->post('maid_id') && $this->input->post('service_type_id') && is_numeric($this->input->post('service_type_id')) && $this->input->post('service_type_id') > 0 && $this->input->post('time_from') && is_numeric($this->input->post('time_from')) && $this->input->post('time_from') > 0 && $this->input->post('time_to') && is_numeric($this->input->post('time_to')) && $this->input->post('time_to') > 0 && $this->input->post('time_from') < $this->input->post('time_to') && $this->input->post('booking_type') && ($this->input->post('booking_type') == 'OD' || $this->input->post('booking_type') == 'WE' || $this->input->post('booking_type') == 'BW')) {
                    if (strtotime($schedule_date) < strtotime(date('d-M-Y'))) {
                        echo 'refresh';
                        exit();
                    }
                    $week_day_names = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
                    $service_date = date('Y-m-d', strtotime($schedule_date));
                    $service_week_day = date('w', strtotime($schedule_date));
                    $customer_id = trim($this->input->post('customer_id'));
                    $customer_address_id = trim($this->input->post('customer_address_id'));
                    $maid_id = trim($this->input->post('maid_id'));
                    $job_maid_name = $this->maids_model->get_maid_by_id($maid_id);
                    $service_type_id = trim($this->input->post('service_type_id'));
                    $time_from =  date('H:i', trim($this->input->post('time_from')));
                    $time_to = date('H:i', trim($this->input->post('time_to')));
                    $booking_type = trim($this->input->post('booking_type'));
                    $is_locked = $this->input->post('is_locked') ? 1 : 0;
                    $repeat_days = array($service_week_day);
                    $repeat_end = 'ondate';
                    $service_end_date = $service_date;
                    //$pending_amount = $this->input->post('pending_amount');
                    //$hourly_amount = $this->input->post('price_per_amount');
                    $cleaning_material = $this->input->post('cleaning_material');
                    $total_amount = $this->input->post('total_amount');

                    //$discount = trim($this->input->post('discount'));
                    //$booking_note = $this->input->post('booking_note') ? trim($this->input->post('booking_note')) : '';
                    $email_notifications = $this->input->post('email_notifications');
                    $sms_notifications = $this->input->post('sms_notifications');

                    $bookings = $this->bookings_model->get_schedule_by_date($service_date);
                    foreach ($bookings as $booking) {
                        $booked_slots[$booking->maid_id]['time'][strtotime($booking->time_from)] = strtotime($booking->time_to);
                    }

                    if (preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_from) == FALSE || preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_to) == FALSE) {
                        echo 'refresh';
                        exit();
                    }

                    if ($booking_type != 'OD') {
                        if ($this->input->post('repeat_days') && is_array($this->input->post('repeat_days')) && count($this->input->post('repeat_days')) > 0 &&  count($this->input->post('repeat_days')) <= 7 && $this->input->post('repeat_end') && ($this->input->post('repeat_end') == 'never' || $this->input->post('repeat_end') == 'ondate')) {
                            $repeat_days = $this->input->post('repeat_days');
                            $repeat_end = $this->input->post('repeat_end');

                            if ($repeat_end == 'ondate') {
                                if ($this->input->post('repeat_end_date')) {
                                    $repeat_end_date = $this->input->post('repeat_end_date');

                                    $repeat_end_date_split = explode('/', $repeat_end_date);
                                    if (count($repeat_end_date_split) == 3 && checkdate($repeat_end_date_split[1], $repeat_end_date_split[0], $repeat_end_date_split[2])) {
                                        $s_date = new DateTime($service_date);
                                        $e_date = new DateTime($repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0]);
                                        //$diff = $s_date->diff($e_date);

                                        $diff = abs(strtotime($repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0]) - strtotime($service_date));

                                        $years = floor($diff / (365 * 60 * 60 * 24));
                                        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
                                        $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));

                                        if ($days < 7) //if($diff->days < 7)
                                        {
                                            echo 'refresh';
                                            exit();
                                        }

                                        $service_end_date = $repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0];
                                    } else {
                                        echo 'refresh';
                                        exit();
                                    }
                                } else {
                                    echo 'refresh';
                                    exit();
                                }
                            }
                        } else {
                            echo 'refresh';
                            exit();
                        }
                    }

                    $time_from_stamp = trim($this->input->post('time_from'));
                    $time_to_stamp = trim($this->input->post('time_to'));

                    if ($booking_type == 'OD') {
                        if (isset($booked_slots[$maid_id]['time'])) {
                            foreach ($booked_slots[$maid_id]['time'] as $f_time => $t_time) {
                                if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
                                    $return = array();
                                    $return['status'] = 'error';
                                    $return['message'] = 'The selected time slot is not available for maid ' . $job_maid_name->maid_name;

                                    echo json_encode($return);
                                    exit();
                                }
                            }
                        }
                    }

                    $today_week_day = date('w', strtotime($service_date));

                    if ($booking_type == 'WE') {
                        foreach ($repeat_days as $repeat_day) {
                            if ($repeat_day < $today_week_day) {
                                $day_diff = (6 - $today_week_day + $repeat_day + 1);
                            } else {
                                $day_diff = $repeat_day - $today_week_day;
                            }

                            $service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));
                            $bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($maid_id, $repeat_day);

                            foreach ($bookings_on_day as $booking_on_day) {
                                $s_date_stamp = strtotime($service_start_date);
                                $e_date_stamp = strtotime($service_end_date);
                                $bs_date_stamp = strtotime($booking_on_day->service_start_date);
                                $be_date_stamp = strtotime($booking_on_day->service_actual_end_date);

                                if (($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $booking_on_day->service_end == '0' && $repeat_end == 'never') || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
                                //if(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
                                {
                                    $f_time = strtotime($booking_on_day->time_from);
                                    $t_time = strtotime($booking_on_day->time_to);
                                    if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) //|| ($time_from_stamp <= $f_time && $time_to_stamp <= $t_time)
                                    {
                                        //echo "<br>".$booking_on_day->booking_id;
                                        $return = array();
                                        $return['status'] = 'error';
                                        $return['message'] = 'The selected time slot is not available for maid ' . $job_maid_name->maid_name . ' on ' . $week_day_names[$repeat_day] . 's';

                                        echo json_encode($return);
                                        exit();
                                    }
                                }
                            }
                        }
                    }
                } else {
                    echo 'refresh';
                    exit();
                }

                $todays_new_booking = array();
                foreach ($repeat_days as $repeat_day) {
                    if ($repeat_day < $today_week_day) {
                        $day_diff = (6 - $today_week_day + $repeat_day + 1);
                    } else {
                        $day_diff = $repeat_day - $today_week_day;
                    }

                    $service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));
                    $service_end_date = $repeat_end == 'never' ? $service_start_date : $service_end_date;

                    $booking_fields = array();
                    $booking_fields['customer_id'] = $customer_id;
                    $booking_fields['customer_address_id'] = $customer_address_id;
                    $booking_fields['maid_id'] = $maid_id;
                    $booking_fields['service_type_id'] = $service_type_id;
                    $booking_fields['service_start_date'] = $service_start_date;
                    $booking_fields['service_week_day'] = $repeat_day;
                    $booking_fields['time_from'] = $time_from;
                    $booking_fields['time_to'] = $time_to;
                    $booking_fields['booking_type'] = $booking_type;
                    $booking_fields['service_end'] = $repeat_end == 'ondate' ? 1 : 0;
                    $booking_fields['service_end_date'] = $service_end_date;
                    $booking_fields['booking_category'] = 'C';
                    $booking_fields['is_locked'] = $is_locked;
                    $booking_fields['total_amount'] = $total_amount;
                    //$booking_fields['pending_amount'] = $pending_amount;
                    //$booking_fields['price_per_hr'] = $hourly_amount;
                    if ($cleaning_material == 'Y') {
                        $booking_fields['cleaning_material'] = 'Y';
                    } else {
                        $booking_fields['cleaning_material'] = 'N';
                    }


                    //$booking_fields['discount'] = $discount;
                    $booking_fields['booked_by'] = user_authenticate();
                    $booking_fields['booking_status'] = 1;
                    $booking_fields['booked_datetime'] = date('Y-m-d H:i:s');
                    if ($booking_type == 'WE') {
                        $updatefield = array();
                        $updatefield['customer_booktype'] = 1;
                        $updatess = $this->customers_model->update_booktype($updatefield, $customer_id);
                    }
                    $booking_id = $this->bookings_model->add_booking($booking_fields);





                    if ($booking_id) {
                        //Email and SMS Notifications
                        if ($booking_type == "WE") {
                            $last_sunday = strtotime('last Sunday');
                            $service_start_date_for_not = date('l', strtotime('+' . $repeat_day . ' day', $last_sunday));
                        } else if ($booking_type == "OD") {
                            $service_start_date_for_not = date('d/m/Y', strtotime($service_start_date));
                        }
                        $no_of_hourss = $this->get_time_difference($time_from, $time_to);
                        $time_from_not =  date('g:i A', strtotime(html_escape($time_from)));
                        $booking_not = $this->bookings_model->get_booking_by_id($booking_id);
                        $email_address_not = $booking_not->email_address;
                        $mobile = $booking_not->mobile_number_1;
                        $content = "Your scheduled booking on " . $service_start_date_for_not . " for " . $no_of_hourss . " hrs  from " . $time_from_not . " has been confirmed.";
                        if ($email_notifications == "Y") {
                            //$this->send_email_notifications($content,$email_address_not);
                        }
                        if ($sms_notifications == 'Y') {
                            //$sms_send = $this->send_sms_notifications($content,$mobile);
                            //                                                $sms_val = $this->input->post('sms_val');
                            //                                                if($sms_val == 'N')
                            //                                                {
                            //                                                    //$this->send_sms_notifications($content,$mobile);
                            //                                                } else {
                            //                                                    echo $sms_send_date = $this->input->post('sms_send_date');
                            //                                                    exit();
                            //                                                    $this->send_sms_notifications_later($content,$mobile);
                            //                                                }
                        }
                        $booking_done = TRUE;
                        //$this->appointment_bill($booking_id);

                        if ($service_start_date == date('Y-m-d')) {
                            $todays_new_booking[] = $booking_id;
                        }
                        $c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
                        if (isset($c_address->zone_id)) {
                            $tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
                            if ($tablet) {
                                $booking = $this->bookings_model->get_booking_by_id($booking_id);

                                $push_fields = array();
                                $push_fields['tab_id'] = $tablet->tablet_id;
                                $push_fields['type'] = 1;
                                $push_fields['message'] = 'New Booking. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->newtime_from . '-' . $booking->newtime_to;
                                // code moved starts here

                                $push_fields['maid_id'] = $booking->maid_id;
                                $push_fields['title'] = "New Booking";
                                $push_fields['customer_name'] = $booking->customer_name;
                                $push_fields['maid_name'] = $booking->maid_name;
                                $push_fields['booking_time'] = $booking->newtime_from . ' - ' . $booking->newtime_to;

                                // code moved ends here
                                if (strtotime($schedule_date) <= strtotime(date('d-M-Y'))) {
                                    $push = $this->bookings_model->add_push_notifications($push_fields);
                                }
                            }
                        }
                    }
                }

                if (isset($booking_done)) {
                    if (count($todays_new_booking) > 0 && isset($customer_address_id)) {
                        $c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
                        if (isset($c_address->zone_id)) {
                            $tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
                            $booking = $this->bookings_model->get_booking_by_id($booking_id);
                            $getmaiddeviceid = $this->maids_model->get_maid_by_id($booking->maid_id);
                            if ($tablet) {
                                if (strtotime($schedule_date) <= strtotime(date('d-M-Y'))) {
                                    $deviceid = $tablet->google_reg_id;
                                    // optional payload
                                    $payload = array();
                                    $payload['isfeedback'] = false;
                                    if (isset($push) && $push > 0) {
                                        $payload['pushid'] = $push;
                                    } else {
                                        $payload['pushid'] = 0;
                                    }

                                    $title = "New Booking";
                                    $message = 'New Booking. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->newtime_from . '-' . $booking->newtime_to;
                                    $res = array();
                                    $res['title'] = $title;
                                    $res['is_background'] = false;
                                    $res['body'] = $message;
                                    $res['image'] = "";
                                    $res['payload'] = $payload;
                                    $res['customer'] = $booking->customer_name;
                                    $res['maid'] = $booking->maid_name;
                                    $res['bookingTime'] = $booking->newtime_from . ' - ' . $booking->newtime_to;
                                    $res['timestamp'] = date('Y-m-d G:i:s');
                                    $regId = $deviceid;
                                    $fields = array(
                                        'to' => $regId,
                                        'notification' => $res,
                                        'data' => $res,
                                    );
                                    $return = android_customer_app_push($fields);
                                    if ($getmaiddeviceid->maid_device_token != "") {
                                        $maid_fields = array(
                                            'to' => $getmaiddeviceid->maid_device_token,
                                            'notification' => $res,
                                            'data' => $res,
                                        );
                                        android_customer_app_push($maid_fields);
                                    }
                                    //$return = android_push(array($tablet->google_reg_id), array('action' => 'new-booking', 'booking-ids' => $todays_new_booking, 'message' => 'New Booking. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->time_from . '-' . $booking->time_to));                                    }
                                }
                                //print_r($return);exit;
                            }
                        }

                        $return = array();
                        $return['status'] = 'success';
                        $return['maid_id'] = $maid_id;
                        $return['maid_name'] = $job_maid_name->maid_name;
                        $return['customer_id'] = $customer_id;
                        $return['time_from'] = $time_from;
                        $return['time_to'] = $time_to;
                        //$user_logged_in_session = $CI->session->userdata('user_logged_in');
                        //$return['user'] = $user_logged_in_session['user_fullname'];

                        echo json_encode($return);
                        exit();
                    }
                }
            }
        }

        $maids_leave_on_date = $this->maids_model->get_maids_leave_by_date(date('Y-m-d'));
        $leave_maid_ids = array();
        foreach ($maids_leave_on_date as $leave) {
            array_push($leave_maid_ids, $leave->maid_id);
        }


        //$customers = $this->customers_model->get_customers();
        $maids = $this->maids_model->get_maids();
        $service_types = $this->service_types_model->get_service_types();
        $data = array();
        //$data['customers'] = $customers;
        $data['maids'] = $maids;
        $data['service_types'] = $service_types;
        $data['times'] = $times;
        $data['day_number'] = $day_number;
        $data['leave_maid_ids'] = $leave_maid_ids;
        $layout_data = array();
        $layout_data['content_body'] = $this->load->view('add_job', $data, TRUE);
        $layout_data['page_title'] = 'New Job';
        $layout_data['meta_description'] = 'New Job';
        $layout_data['css_files'] = array('jquery.fancybox.css', 'datepicker.css', 'bootstrap-datetimepicker.min.css');
        $layout_data['jobs_active'] = '1';
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.fancybox.pack.js', 'bootstrap-datetimepicker.min.js', 'bootstrap-datepicker.js', 'jobs.js', 'moment.min.js', 'bootstrap.js');

        $this->load->view('layouts/default_job', $layout_data);
    }

    function get_time_difference($time1, $time2)
    {
        $time1 = strtotime("1/1/1980 $time1");
        $time2 = strtotime("1/1/1980 $time2");
        if ($time2 < $time1) {
            $time2 = $time2 + 86400;
        }
        return ($time2 - $time1) / 3600;
    }




    function send_email_notifications($content, $emailid)
    {
        $this->load->library('email');
        $c_message = 'Thanks for booking with us!';
        $c_message .= $content;
        $b_message = $c_message;
        $html = '<html>
    <head>
    </head>
    <body>
            <div style="margin:0;padding:0;background-color:#ffffff;min-height:100%!important;width:100%!important">
                    <center>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;margin:0;padding:0;background-color:#ffffff;height:100%!important;width:100%!important">
                                    <tbody>
                                            <tr>
                                                    <td align="center" valign="top" style="margin:0;padding:20px;border-top:0;height:100%!important;width:100%!important"><table border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse;border:0">
                                                            <tbody>
                                                                    <tr>
                                                                            <td align="center" valign="top">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse;background-color:#ffffff;border-top:0;border-bottom:0">
                                                                                            <tbody>
                                                                                              <tr>
                                                                                                    <td valign="top"></td>
                                                                                              </tr>
                                                                                            </tbody>
                                                                                    </table>
                                                                            </td>
                                                                    </tr>
                                                                    <tr>
                                                                            <td align="center" valign="top">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse;background-color:#ffffff;border-top:0;border-bottom:0">
                                            <tbody>
                                                                                                    <tr>
                                                                                                            <td valign="top">
                                                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                                                                                                    <tbody>
                                                                                                                            <tr>
                                                                                                                                    <td valign="top" style="padding:0px">
                                                                                                                                    <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
                                                                                                                                            <tbody>
                                                                                                                                                    <tr>
                                                                                                                                                            <td valign="top" style="padding-right:0px;padding-left:0px;padding-top:0;padding-bottom:0"><img align="left" alt="" src="' . base_url() . 'images/elitemaid_emaid_banner.jpg" width="800" style="max-width:1144px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;outline:none;text-decoration:none" class="CToWUd a6T" tabindex="0">
                                                                                                                                                                    <div class="a6S" dir="ltr" style="opacity: 0.01; left: 632px; top: 304px;">
                                                                                                                                                                    <div id=":1dx" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" title="Download" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V">
                                                                                                                                                                            <div class="aSK J-J5-Ji aYr"></div>
                                                                                                                                                                    </div>
                                                                                                                                                                    </div>
                                                                                                                                                            </td>
                                                                                                                                                    </tr>
                                                                                                                                            </tbody>
                                                                                                                                    </table>
                                                                                                                                    </td>
                                                                                                                            </tr>
                                                                                                                    </tbody>
                                                                                                            </table>
                                                                                                            <table width="800" cellspacing="0" cellpadding="0" border="0" align="left" style="border-collapse:collapse">
                                                                                                                    <tbody>
                                                                                                                            <tr>
                                                                                                                                    <td valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left">
                                                                                                                               <br>
                                                                                                                                    Dear Customer,<br>
                                                                                                                                    <br>
                                                                                                                                    <br> ' . $b_message . '<br>

                                                                                                                                    <br>
                                                                                                                                            Thanks & Regards<br/>
                                                                                                                                            Elitemaids Team<br />
                                                                                                                                    </td>
                                                                                                                            </tr>
                                                                                                                    </tbody>
                                                                                                            </table>
                                                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                                                                                                    <tbody>
                                                                                                                    <tr>
                                                                                                                      <td valign="top"><table align="left" border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse">
                                                                                                                              <tbody>
                                                                                                                                    <tr>
                                                                                                                                      <td valign="top" style="padding-top:40px;padding-right:18px;padding-bottom:20px;padding-left:18px;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left;"><div style="text-align:center"><span style="color:#606060;font-family:helvetica;font-size:15px;line-height:22.5px;text-align:center">Download the My maid app in the following stores:</span><br>
                                                                                                                                              <br style="color:#606060;text-align:center;font-family:helvetica;font-size:15px;line-height:22.5px">
                                                                                                                                              <a href="#" style="word-wrap:break-word;color:#606060;text-align:center;font-family:helvetica;font-size:15px;line-height:22.5px;font-weight:normal;text-decoration:underline" target="_blank"><img align="none" height="44" src="' . base_url() . 'images/ios.png" style="border:0px;outline:none;text-decoration:none;min-height:44px;width:150px" width="150" class="CToWUd"></a><span style="color:#606060;font-family:helvetica;font-size:15px;line-height:22.5px;text-align:center">&nbsp;</span>
                                                                                                                                              <a href="#" style="word-wrap:break-word;color:#606060;text-align:center;font-family:helvetica;font-size:15px;line-height:22.5px;font-weight:normal;text-decoration:underline" target="_blank"><img align="none" height="44" src="' . base_url() . 'images/android.png" style="border:0px;outline:none;text-decoration:none;min-height:44px;width:150px" width="150" class="CToWUd"></a><span style="color:#606060;font-family:helvetica;font-size:15px;line-height:22.5px;text-align:center">&nbsp;</span><span style="color:#606060;font-family:helvetica;font-size:15px;line-height:22.5px;text-align:center">&nbsp;</span></div></td>
                                                                                                                                    </tr>
                                                                                                                              </tbody>
                                                                                                                            </table></td>
                                                                                                                    </tr>
                                                                                                                    </tbody>
                                                                                                            </table>
                                                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                                                                                              <tbody>
                                                                                                                    <tr>
                                                                                                                      <td valign="top"><table align="left" border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse">
                                                                                                                              <tbody>
                                                                                                                                    <tr>
                                                                                                                                      <td valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left"><div style="text-align:center"><em style="color:#606060;font-family:helvetica;font-size:11px;line-height:15.6199998855591px;text-align:center">Copyright &copy; 2020 Elitemaids.&nbsp;All rights reserved.</em><br style="color:#606060;font-family:Helvetica;font-size:11px;text-align:center;line-height:15.6199998855591px">
                                                                                                                                              <span style="color:#606060;font-family:helvetica;font-size:11px;line-height:15.6199998855591px;text-align:center">You are receiving this email because you signed up to Elitemaids</span></div>
                                                                                                                                            <div style="text-align:center; padding-bottom: 20px;"><a href="#" style="word-wrap:break-word;color:#606060;font-family:Helvetica;font-size:11px;line-height:13.75px;text-align:center;font-weight:normal;text-decoration:underline; " target="_blank"><!--unsubscribe from this list--></a>&nbsp;</div></td>
                                                                                                                                    </tr>
                                                                                                                              </tbody>
                                                                                                                            </table></td>
                                                                                                                    </tr>
                                                                                                              </tbody>
                                                                                                            </table>
                                                                                                    </td>
                                                                                            </tr>
                                                                                    </tbody>
                                                                            </table>
                                                                            </td>
                                                                    </tr>
                                                            </tbody>
                                                    </table>
                                            </td>
                                    </tr>
                                    </tbody>
                            </table>
                            </center>
                    </div>
            </body>
        </html>';

        $config = array(
            'protocol'  => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'booking.elitemaids@gmail.com',
            'smtp_pass' => 'Elitemaids@2020#',
            'mailtype'  => 'html',
            'charset'   => 'utf-8'
        );

        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");
        $this->email->from('booking.elitemaids@gmail.com', 'ELITEMAIDS');
        $this->email->to($emailid);
        $this->email->subject('Booking Confirmation Mail');
        $this->email->message($html);
        //$this->email->send();

        // $config['mailtype'] = 'html';
        // $config['charset'] = 'iso-8859-1';
        // $this->email->initialize($config);
        // $this->email->from('vishnu.m@azinova.info', 'MyMaid');
        // $this->email->cc('siby@azinova.info');
        // $this->email->to($emailid);
        // $this->email->subject('Booking Confirmation Mail');
        // $this->email->message($html);
        // //$this->email->send();
    }

    function send_sms_notifications($content, $mobile)
    {
        $smsdetails = $this->settings_model->get_sms_info();
        //Please Enter Your Details
        $user = $smsdetails[0]['user']; //"mymaidapi"; //your username
        $password = $smsdetails[0]['pass']; //"Knkmz7ig"; //your password
        //$m_number = "56456654645";
        $final_number = substr(trim(preg_replace('/[^0-9]*/', '', $mobile)), -9);
        $mobilenumbers = "971" . $final_number; //enter Mobile numbers comma seperated
        $message = $content; //enter Your Message
        $senderid = $smsdetails[0]['sender_id']; //"800 MyMaid"; //Your senderid
        $messagetype = "N"; //Type Of Your Message
        $DReports = "Y"; //Delivery Reports
        $url = $smsdetails[0]['api_url'];
        $message = urlencode($message);
        $ch = curl_init();
        if (!$ch) {
            die("Couldn't initialize a cURL handle");
        }
        $ret = curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "User=$user&passwd=$password&mobilenumber=$mobilenumbers&message=$message&sid=$senderid&mtype=$messagetype&DR=$DReports");
        $ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //If you are behind proxy then please uncomment below line and provide your proxy ip with port.
        // $ret = curl_setopt($ch, CURLOPT_PROXY, "PROXY IP ADDRESS:PORT");
        $curlresponse = curl_exec($ch); // execute
        if (curl_errno($ch))
            echo 'curl error : ' . curl_error($ch);
        if (empty($ret)) {
            // some kind of an error happened
            die(curl_error($ch));
            curl_close($ch); // close cURL handler
        } else {
            $info = curl_getinfo($ch);
            curl_close($ch); // close cURL handler  
            return $curlresponse; //echo "Message Sent Succesfully" ;
        }
    }

    function clean($string)
    {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public function send_service_status_email($booking_id, $day_service_id, $service_date)
    {
        //$booking_id=1109;$day_service_id=1;$service_date='2020-05-20';
        $this->load->library('email');
        $data['booking'] = $this->bookings_model->get_booking_by_id($booking_id);
        $data['service_id'] = $day_service_id;
        $data['booking_id'] = $booking_id;
        $data['servicedate'] = $service_date;
        $html = $this->load->view('service_template_email', $data, True);
        $email_id = $data['booking']->email_address;
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.sendgrid.net',
            'smtp_user' => 'apikey',
            //   'smtp_pass' => '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',
            'smtp_pass' => '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',
            'smtp_port' => 587,
            'mailtype'  => 'html',
            'crlf' => "\r\n",
            'newline' => "\r\n"
        );

        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");
        $this->email->from('info@elitemaids.emaid.info', 'ELITEMAIDS');
        $this->email->to($email_id);
        $this->email->subject('EliteMaids Service Notification');
        $this->email->message($html);
        //$this->email->send();
    }

    public function synch_to_odoo_common($service_date)
    {
        //error_reporting(E_ALL);
        //ini_set('display_errors', 1);
        //$service_date = date('Y-m-d');
        //$service_date = '2020-01-29';
        $day_services = $this->day_services_model->get_activity_by_date_new_odoo_common($service_date);
        // echo '<pre>';
        // print_r($day_services);
        // exit();

        $i = 0;

        foreach ($day_services as $service) {
            if ($service->odoo_package_customer_status == 0) {
                $get_customer_details = $this->customers_model->get_customer_details_by_customerid($service->customer_id);

                if ($get_customer_details->odoo_package_customer_status == 0) {
                    $get_customer_details->customer_type = $get_customer_details->customer_type == 'HO' ? 'home' : ($get_customer_details->customer_type == 'OF' ? 'office' : 'warehouse');
                    $get_customer_details->payment_type = $get_customer_details->payment_type == 'D' ? 'daily' : ($get_customer_details->payment_type == 'W' ? 'weekly' : 'monthly');
                    if ($get_customer_details->payment_mode == 'Cash') {
                        $get_customer_details->payment_mode = 'cash';
                    } else if ($get_customer_details->payment_mode == 'Cheque') {
                        $get_customer_details->payment_mode = 'cheque';
                    } else if ($get_customer_details->payment_mode == 'Online') {
                        $get_customer_details->payment_mode = 'online';
                    } else if ($get_customer_details->payment_mode == 'Credit Card') {
                        $get_customer_details->payment_mode = 'credit_card';
                    } else if ($get_customer_details->payment_mode == 'Bank transfer') {
                        $get_customer_details->payment_mode = 'bank';
                    }
                    //$get_customer_details->payment_mode = strtolower($get_customer_details->payment_mode);
                    $get_customer_details->key_given = $get_customer_details->key_given == 'Y' ? 'yes' : 'no';
                    $company_type = "person";
                    if ($get_customer_details->is_company == 'Y') {
                        $company_name = $get_customer_details->company_name;
                        $get_company_details = $this->customers_model->get_company_details($company_name);
                        if (!empty($get_company_details)) {
                            $parent_id = (int)$get_company_details->company_odoo_id;
                        } else {
                            $parent_id = null;
                        }
                    } else if ($get_customer_details->is_company == 'N') {
                        $parent_id = null;
                    }
                    $getarea = $this->settings_model->get_area_details($get_customer_details->area_id);
                    if ($get_customer_details->customer_booktype == 0) {
                        $booktype = "nonregular";
                    } else if ($get_customer_details->customer_booktype == 1) {
                        $booktype = "regular";
                    }

                    if ($get_customer_details->building != "") {
                        $apartment = ' , Apt No ' . $get_customer_details->building;
                    } else {
                        $apartment = "";
                    }
                    $addresssss = $get_customer_details->customer_address . $apartment;

                    $post['params']['user_id'] = 1;
                    $post['params']['name'] = $get_customer_details->customer_name;
                    $post['params']['mobile'] = $get_customer_details->mobile_number_1;
                    $post['params']['company_type'] = $company_type;
                    $post['params']['parent_id'] = $parent_id;
                    $post['params']['customer_nick_name'] = $get_customer_details->customer_nick_name;
                    $post['params']['customer_id'] = (int)$get_customer_details->customer_id;
                    //$post['params']['id'] = $odooid;
                    $post['params']['cust_book_type'] = $booktype;
                    $post['params']['payment_type'] = $get_customer_details->payment_type;
                    $post['params']['customer_type'] = $get_customer_details->customer_type;
                    $post['params']['source'] = $get_customer_details->customer_source;
                    $post['params']['street'] = $addresssss;
                    $post['params']['area_id'] = (int)$getarea[0]['odoo_package_area_id'];
                    $post['params']['website'] = $get_customer_details->website_url;
                    $post['params']['phone'] = $get_customer_details->phone_number;
                    $post['params']['mobile2'] = $get_customer_details->mobile_number_2;
                    $post['params']['mobile3'] = $get_customer_details->mobile_number_3;
                    $post['params']['fax'] = $get_customer_details->fax_number;
                    $post['params']['email'] = $get_customer_details->email_address;
                    //$post['params']['zone_id'] = $data['driver_name'];
                    $post['params']['comment'] = $get_customer_details->customer_notes;
                    $post['params']['hourly'] = $get_customer_details->price_hourly;
                    $post['params']['extra'] = $get_customer_details->price_extra;
                    $post['params']['weekend'] = $get_customer_details->price_weekend;
                    $post['params']['latitude'] = $get_customer_details->latitude;
                    $post['params']['longitude'] = $get_customer_details->longitude;
                    $post['params']['key'] = $get_customer_details->key_given;
                    $post['params']['trn_no'] = $get_customer_details->trnnumber;
                    $post['params']['vat_no'] = $get_customer_details->vatnumber;

                    $post_values = json_encode($post);

                    $url = $this->config->item('odoo_url') . "customer_creation";
                    $login_check = curl_api_service($post_values, $url);
                    $returnData = json_decode($login_check);

                    if ($returnData->result->status == "success") {
                        $odoo_customer_id = $returnData->result->response->id;
                        $this->customers_model->update_customers(array('odoo_package_customer_id' => $odoo_customer_id, 'odoo_package_customer_status' => 1), $get_customer_details->customer_id);
                        $service->odoo_customer_newid = $odoo_customer_id;
                    }
                } else {
                    $service->odoo_customer_newid = $get_customer_details->odoo_package_customer_id;
                }
            } else {
                $service->odoo_customer_newid = $service->odoo_package_customer_id;
            }


            $service->service_date = $service_date;
            if ($service->paid_date == "") {
                $paid_date = $service->service_date;
            } else {
                $paid_date = $service->paid_date;
            }
            $total_time = ((strtotime($service->end_time) - strtotime($service->start_time)) / 3600);
            if ($service->payment_type == 1) {
                $service->payment_type = 'D';
            } else if ($service->payment_type == 2) {
                $service->payment_type = 'W';
            } else if ($service->payment_type == 3) {
                $service->payment_type = 'M';
            }
            if ($service->cleaning_material == 'Y') {
                $mat_used = "yes";
            } else {
                $mat_used = "no";
            }
            $service->payment_type = $service->payment_type == 'D' ? 'daily' : ($service->payment_type == 'W' ? 'weekly' : 'monthly');
            //$total_fee = ($service->total_fee + $service->material_fee + $service->discount);
            $total_fee = ($service->total_fee);
            $unit_price = $service->price_hourly;

            if ($service->odoo_new_sync_status == 0) {
                // if($service->payment_type == "daily")
                // {
                // $post['params']['user_id'] = 1;
                // $post['params']['partner_id'] = $service->odoo_customer_newid;
                // $post['params']['service_date'] = $service->service_date;
                // $post['params']['amount_total'] = (float)$total_fee;

                // $linearray = array();
                // $linearray[0]['employee_id'] = $service->odoo_new_maid_id;
                // $linearray[0]['quantity'] = $total_time;
                // $linearray[0]['start_time'] = $service->time_from;
                // $linearray[0]['end_time'] = $service->time_to;
                // $linearray[0]['unit_price'] = (float)$unit_price;
                // $linearray[0]['amount_discount'] = (float)$service->discount;
                // $linearray[0]['material_used'] = $mat_used;
                // $linearray[0]['material_unit_price'] = 10;
                // $linearray[0]['delivery_address'] = $service->delvry_addr;
                // $post['params']['lines'] = $linearray;
                // $post_values=json_encode($post);

                // $url = $this->config->item('odoo_url')."invoice_creation";
                // echo $login_check = curl_api_service($post_values,$url);
                // $returnData=json_decode($login_check);

                // if($returnData->result->status=="success")
                // {
                // $odoo_invoice_id = $returnData->result->response->invoice_id;
                // $this->day_services_model->update_day_service($service->day_service_id,array('odoo_new_invoice_id' => $odoo_invoice_id,'odoo_new_sync_status' => 1));
                // if($odoo_invoice_id > 0)
                // {
                // if($service->payment_status == 1)
                // {
                // $post_p['params']['user_id'] = 1;
                // $post_p['params']['partner_id'] = $service->odoo_customer_newid;
                // $post_p['params']['amount'] = (float)$service->collected_amount;
                // $post_p['params']['payment_method'] = 'cash';
                // $post_p['params']['payment_date'] = $paid_date;
                // $post_p['params']['reference'] = $service->ps_no;
                // $post_p['params']['notes'] = "";
                // $post_p['params']['area_id'] = $service->odoo_new_area_id;
                // $post_p['params']['zone_id'] = $service->odoo_new_zone_id;
                // $post_p['params']['employee_id'] = $service->odoo_new_maid_id;
                // $post_p['params']['start_time'] = $service->time_from;
                // $post_p['params']['end_time'] = $service->time_to;
                // $post_p['params']['driver'] = "";
                // $post_values_p=json_encode($post_p);

                // $url = $this->config->item('odoo_url')."payments";
                // echo $login_check = curl_api_service($post_values_p,$url);
                // $returnData=json_decode($login_check);
                // if($returnData->result->status=="success")
                // {
                // $odoo_pay_id = $returnData->result->response->payment_id;
                // $this->day_services_model->update_day_service($service->day_service_id,array('odoo_new_paymentid' => $odoo_pay_id));
                // }
                // }
                // }
                // }
                // } else {
                //newly added
                if ($service->booked_from == 'W' || $service->booked_from == 'M') {
                    // $vat_exc_amt = ($total_fee/1.05);
                    $vat_exc_amt = ($total_fee);
                } else {
                    $vat_exc_amt = ($total_fee);
                }
                $service->discount = 0;
                //ends


                $post['params']['user_id'] = 1;
                $post['params']['partner_id'] = (int)$service->odoo_customer_newid;
                $post['params']['partner_shipping'] = $service->delvry_addr;
                $post['params']['employee_id'] = (int)$service->odoo_package_maid_id;
                $post['params']['service_date'] = $service->service_date;
                $post['params']['start_time'] = $service->time_from;
                $post['params']['end_time'] = $service->time_to;
                $post['params']['total_time'] = $total_time;
                //$post['params']['total_time'] = 1;
                //$post['params']['unit_price'] = (float)$unit_price;
                $post['params']['unit_price'] = (float)$vat_exc_amt;
                $post['params']['amount_discount'] = (float)$service->discount;
                $post['params']['material_used'] = "no";
                $post['params']['material_unit_price'] = 10;
                $post['params']['delivery_address'] = $service->delvry_addr;
                $post['params']['amount_total'] = (float)$total_fee;
                $post['params']['driver'] = $service->zone_name;
                $post['params']['amount_paid'] = (float)$service->collected_amount;
                $post['params']['paid_date'] = $paid_date;
                $post['params']['receipt_ref'] = $service->ps_no;
                $post['params']['payment_method'] = 'Cash';
                if ($mat_used == "yes") {
                    $post['params']['remarks'] = 'Service for ' . $total_time . ' hours with cleaning material';
                } else {
                    $post['params']['remarks'] = 'Service for ' . $total_time . ' hours';
                }
                // echo '<pre>';
                // print_r($post);
                // exit();
                $post_values = json_encode($post);

                $url = $this->config->item('odoo_url') . "activity_creation";
                $login_check = curl_api_service($post_values, $url);
                $returnData = json_decode($login_check);

                if ($returnData->result->status == "success") {
                    $odoo_invoice_id = $returnData->result->response->id;
                    //echo '<br>';
                    $this->day_services_model->update_day_service($service->day_service_id, array('odoo_package_activity_id' => $odoo_invoice_id, 'odoo_package_activity_status' => 1));
                    // if($odoo_invoice_id > 0)
                    // {
                    // if($service->payment_status == 1)
                    // {
                    // $post_p['params']['user_id'] = 1;
                    // $post_p['params']['partner_id'] = $service->odoo_customer_newid;
                    // $post_p['params']['amount'] = (float)$service->collected_amount;
                    // $post_p['params']['payment_method'] = 'cash';
                    // $post_p['params']['payment_date'] = $paid_date;
                    // $post_p['params']['reference'] = $service->ps_no;
                    // $post_p['params']['notes'] = "";
                    // $post_p['params']['area_id'] = $service->odoo_new_area_id;
                    // $post_p['params']['zone_id'] = $service->odoo_new_zone_id;
                    // $post_p['params']['employee_id'] = $service->odoo_new_maid_id;
                    // $post_p['params']['start_time'] = $service->time_from;
                    // $post_p['params']['end_time'] = $service->time_to;
                    // $post_p['params']['driver'] = "";
                    // $post_values_p=json_encode($post_p);

                    // $url = $this->config->item('odoo_url')."payments";
                    // echo $login_check = curl_api_service($post_values_p,$url);
                    // $returnData=json_decode($login_check);
                    // if($returnData->result->status=="success")
                    // {
                    // $odoo_pay_id = $returnData->result->response->payment_id;
                    // $this->day_services_model->update_day_service($service->day_service_id,array('odoo_new_paymentid' => $odoo_pay_id));
                    // }
                    // }
                    // }
                }
                //}
            }
        }
        redirect('activity');
        exit();
    }
}
