
<style>
    #area select{ width: 15% !important;}
</style>

<div class="row">
        
      <div class="span6" id="add_hourly_price" style="display: none;">      		
	<div class="widget ">
	    <div class="widget-header">
        <ul>
        <li>
	      	<i class="icon-cogs"></i>
                    <h3>Add Hourly Price</h3>
                    
                    </li>
                    <li class="mr-0 float-right">
                    <div class="topiconnew border-0 green-btn">
                       <a onclick="hideadd_hrly_price();" title="Hide"> <i class="fa fa-minus"></i></a>
                    </div> 
                    
                    </li>
            </div> <!-- /widget-header -->
	
            <div class="widget-content">
		<div class="tabbable">
                    <div class="tab-content">
<!--                        <form id="zone" class="form-horizontal" method="post" action="<?php //echo base_url();?>zones/add_zone">-->
                        <form id="area" class="form-horizontal" method="post">
                            <fieldset>
                                    <div class="control-group">											
					<label class="control-label" for="flatname">Select From Hours</label>
                                            <div class="controls">
                                                <!--<input type="text" class="span3" id="from_hrly_name" name="from_hrly_name" required="required">-->
                                                <select class="span3" id="from_hrly_name" name="from_hrly_name" >
                                                    <?php $from_timr = 0;
                                                    while($from_timr <=80){
                                                        ?>
                                                    <option value="<?php echo $from_timr; ?>"><?php echo $from_timr; ?></option>
                                                    <?php
                                                        
                                                        $from_timr++;
                                                    }
                                                    ?>
                                                </select>
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
                                    <div class="control-group">											
					<label class="control-label" for="flatname">Select To Hours</label>
                                            <div class="controls">
                                                <!--<input type="text" class="span3" id="to_hrly_name" name="to_hrly_name" required="required">-->
                                                
                                                <select class="span3" id="to_hrly_name" name="to_hrly_name" >
                                                    <?php $to_timr = 0;
                                                    while($to_timr <=80){
                                                        ?>
                                                    <option value="<?php echo $to_timr; ?>"><?php echo $to_timr; ?></option>
                                                    <?php
                                                        
                                                        $to_timr++;
                                                    }
                                                    ?>
                                                </select>
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
										
                                    <div class="control-group">											
					<label class="control-label" for="tablet_imei">Hourly Charges</label>
                                            <div class="controls">
                                                <input type="text" class="span3" id="hrly_price" name="hrly_price" required="required">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
                                    
                                    
                                   
										
                                    <br />
										
                                    <div class="form-actions">
                                        <input type="submit" class="btn mm-btn pull-right" value="Submit" name="hrly_price_sub">
<!--					<button class="btn">Cancel</button>-->
                                    </div> <!-- /form-actions -->
                            </fieldset>
                        </form>
			
                    </div>
		</div>
            </div> <!-- /widget-content -->
	</div> <!-- /widget -->
    </div> <!-- /span6 -->
    <div class="span6" id="edit_price" style="display: none;">      		
	<div class="widget ">
	    <div class="widget-header">
	      	<i class="icon-cogs"></i>
                    <h3>Edit Charges</h3>
                    <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="hideedit_hrly_price();"><img src="<?php echo base_url();?>img/minus.png" title="Hide"/></a>
            </div> <!-- /widget-header -->
	
            <div class="widget-content">
		<div class="tabbable">
                    <div class="tab-content">
<!--                        <form id="zone" class="form-horizontal" method="post" action="<?php //echo base_url();?>zones/add_zone">-->
                        <form id="area" class="form-horizontal" method="post">
                            <fieldset>

                                    
                                    
                                                <input type="hidden" class="span3" id="edit_ps_id" name="edit_ps_id">
                                    
                                     <div class="control-group">											
					<label class="control-label" for="flatname">Select From Hours</label>
                                            <div class="controls">
                                                <!--<input type="text" class="span3" id="from_hrly_name" name="from_hrly_name" required="required">-->
                                                <select class="span3" id="edit_from_hrly_name" name="edit_from_hrly_name" >
                                                    <?php $from_timr = 0;
                                                    while($from_timr <=80){
                                                        ?>
                                                    <option value="<?php echo $from_timr; ?>"><?php echo $from_timr; ?></option>
                                                    <?php
                                                        
                                                        $from_timr++;
                                                    }
                                                    ?>
                                                </select>
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
                                    <div class="control-group">											
					<label class="control-label" for="price">Select To Hours</label>
                                            <div class="controls">
                                                <!--<input type="text" class="span3" id="to_hrly_name" name="to_hrly_name" required="required">-->
                                                
                                                <select class="span3" id="edit_to_hrly_name" name="edit_to_hrly_name" >
                                                    <?php $to_timr = 0;
                                                    while($to_timr <=80){
                                                        ?>
                                                    <option value="<?php echo $to_timr; ?>"><?php echo $to_timr; ?></option>
                                                    <?php
                                                        
                                                        $to_timr++;
                                                    }
                                                    ?>
                                                </select>
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
										
                                    <div class="control-group">											
					<label class="control-label" for="tablet_imei">Service Rate</label>
                                            <div class="controls">
                                                <input type="text" class="span3" id="edit_hourly_rate" name="edit_hourly_rate" required="required">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
                                    
                                    
                                   
										
                                    <br />
										
                                    <div class="form-actions">
                                        <input type="submit" class="btn mm-btn pull-right" value="Submit" name="price_charges_edit">
<!--					<button class="btn">Cancel</button>-->
                                    </div> <!-- /form-actions -->
                            </fieldset>
                        </form>
			
                    </div>
		</div>
            </div> <!-- /widget-content -->
	</div> <!-- /widget -->
    </div> <!-- /span6 -->
    
    <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Price Settings</h3>
              <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="add_hourly_price();"><img src="<?php echo base_url();?>img/add.png" title="Add Services"/></a>
                    
            </div>
            <!-- /widget-header -->
             <div class="widget-content">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 20px"> Sl.No. </th>
                            <th style="line-height: 18px"> Hours </th>
                            <th style="line-height: 18px"> Rate</th>
                            <th style="line-height: 18px" class="td-actions">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($price_charges) > 0) {
                            $i = 1;
                            foreach ($price_charges as $price_val) {
                                ?>  
                        <tr>
                            <td style="line-height: 18px; width: 20px"><?php echo $i; ?></td>
                            <td style="line-height: 18px"><?php echo $price_val->from_hr.' - '.$price_val->to_hr ?></td>
                            <td style="line-height: 18px"><?php echo $price_val->price ?></td>
                            <td style="line-height: 18px" class="td-actions"><a href="javascript:;" class="btn btn-small btn-warning" onclick="edit_hourly_prices(<?php echo $price_val->ps_id ?>);"><i class="btn-icon-only icon-pencil"> </i></a><a href="javascript:;" class="btn btn-danger btn-small" onclick="delete_hourly_price(<?php echo $price_val->ps_id  ?>);"><i class="btn-icon-only icon-remove"> </i></a></td>
                        </tr>
                        <?php
                                $i++;
                            }
                        }
                        ?>   
                        
                    </tbody>
                </table>
            </div>
            <!-- /widget-content --> 
        </div>
        <!-- /widget --> 
    </div>
    <!-- /span6 --> 
  
</div>