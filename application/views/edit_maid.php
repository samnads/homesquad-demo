<script>
   var maid_skills = <?php echo json_encode($maid_skills_added); ?>;
</script>
<link rel="stylesheet" href="https://fengyuanchen.github.io/cropperjs/css/cropper.css">
<script src="https://fengyuanchen.github.io/cropperjs/js/cropper.js"></script>
<div id="crop-popup" style="display:none;">
    <div class="popup-main-box">
        <div class="col-md-12 col-sm-12 green-popup-head">
            <span id="b-maid-name">Image Cropper</span>
            <span id="b-time-slot"></span>
            <span class="pop_close n-close-btn" onclick="closeCropper()">&nbsp;</span>
        </div>
        <div id="" class="col-12 p-0">
            <div class="modal-body">
                <div class="img-container">
                    <img id="image" src="#">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="n-btn red-btn mb-0" onclick="closeCropper()">Cancel</button>
                <button type="button" class="n-btn mb-0" id="crop">Crop</button>
            </div>
        </div>
    </div>
</div>
<div class="row m-0">
    <div class="col-md-12">
        <div class="widget ">
            <div class="widget-header mb-0">
                <ul>
                    <li>
                        <i class="icon-user"></i>
                        <h3>Edit Staff - <?php echo $maid_details[0]['maid_name'] ?></h3>
                    </li>
                    <li class="mr-0 float-right">
                        <div class="topiconnew border-0 green-btn"> <a href="<?php echo base_url('maids'); ?>" title="Maid List"> <i class="fa fa-users"></i></a> </div>
                    </li>
                </ul>
            </div> <!-- /widget-header -->
            <?php
            if (count($maid_details) > 0) {
                foreach ($maid_details as $maid_val) {
            ?>
                    <div class="widget-content">
                        <div class="tabbable">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#personal" data-toggle="tab">Personal Details</a></li>
                                <li><a href="#skills" data-toggle="tab">Skills</a></li>
                            </ul>
                            <br>
                            <?php
                            if ($message == "success") { ?>
                                <div class="control-group">
                                    <label class="control-label"></label>
                                    <div class="controls">
                                        <div class="alert alert-success">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            <strong>Success!</strong> Staff Details Updated Successfully.
                                        </div>
                                    </div>
                                </div> <!-- /control-group -->
                            <?php
                            }
                            ?>
                            <form id="edit-profile" class="form-horizontal" method="post" enctype="multipart/form-data">
                                <div class="tab-content">
                                    <div class="tab-pane" id="skills">
                                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-4">
                                            <select id="skill_id" class="sel2" style="width:100%">
                                                <option value="">-- Select Skill --</option>
                                                <?php foreach($maid_skills as $skill): ?>
                                                    <option value="<?= $skill->value ?>"><?= $skill->text ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="col-xs-6 col-sm-4">
                                            <select id="skill_rating_id" class="sel2" style="width:100%">
                                                <option value="">-- Select Skill Level --</option>
                                                <?php foreach ($rating_levels as $rating_level): ?>
                                                    <option value="<?=$rating_level->value?>"><?=$rating_level->text?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                        <div class="col-xs-6 col-sm-4">
                                            <button type="button" class="n-btn purple-btn" data-action="select-skill">Add <i class="fa fa-plus" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-12 p-0" id="skills-holder">
                                            <!-- -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                                    <div class="tab-pane active" id="personal">
                                        <fieldset>
                                            <div class="col-sm-4">
                                                <fieldset>
                                                    <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                        <p>Staff Name</p>
                                                        <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                            <input type="text" class="" id="maid_name" name="maid_name" value="<?php echo $maid_val['maid_name'] ?>">
                                                            <input type="hidden" class="" id="maid_id" name="maid_id" value="<?php echo $maid_val['maid_id'] ?>">
                                                        </div>
                                                    </div>
                                                    <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                        <p>Gender</p>
                                                        <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                            <div class="row m-0">
                                                                <div class="col-sm-6 n-radi-check-main p-0">
                                                                    <input type="radio" value="M" id="gender" name="gender" class="" <?php echo isset($maid_val['maid_gender']) ? ($maid_val['maid_gender'] == "M" ? 'checked="checked"' : '') : '' ?>>
                                                                    <label for="gender"> <span class="border-radius-3"></span>
                                                                        <p>Male</p>
                                                                    </label>
                                                                </div>
                                                                <div class="col-sm-6 n-radi-check-main p-0">
                                                                    <input type="radio" value="F" id="gender-f" name="gender" class="" <?php echo isset($maid_val['maid_gender']) ? ($maid_val['maid_gender'] == "F" ? 'checked="checked"' : '') : '' ?>>
                                                                    <label for="gender-f"> <span class="border-radius-3"></span>
                                                                        <p>Female</p>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                        <p>Nationality</p>
                                                        <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                            <input type="text" class="" id="nationality" name="nationality" value="<?php echo $maid_val['maid_nationality'] ?>">
                                                        </div>
                                                    </div>
                                                    <div class="row m-0 n-field-main">
                                                        <p>Change Photo</p>
                                                        <label>
                                                            <img class="thumbnail" id="avatar_img" src="<?= check_and_get_img_url('./upload/maid_avatars/' . $maid_val['maid_photo_file'], 'maid-avatar-upload.png'); ?>" style="width:80px;" alt="Promotion Image">
                                                            <input type="file" class="sr-only" id="input-image" accept="image/*">
                                                        </label>
                                                        <input type="hidden" name="avatar_base64" id="avatar_base64">
                                                    </div>
                                                    <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                        <p>Present Address</p>
                                                        <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                            <textarea class="" rows="5" id="present_address" name="present_address"><?php echo $maid_val['maid_present_address'] ?></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                        <p>Permanent Address</p>
                                                        <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                            <textarea class="" rows="5" id="permanent_address" name="permanent_address"><?php echo $maid_val['maid_permanent_address'] ?></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                        <p>Mobile Number 1</p>
                                                        <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                            <input type="text" class="" id="mobile1" name="mobile1" value="<?php echo $maid_val['maid_mobile_1'] ?>">
                                                        </div>
                                                    </div>
                                                    <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                        <p>Mobile Number 2</p>
                                                        <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                            <input type="text" class="" id="mobile2" name="mobile2" value="<?php echo $maid_val['maid_mobile_2'] ?>">
                                                        </div>
                                                    </div>
                                                    <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                        <p>WhatsApp No.</p>
                                                        <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                            <input type="text" class="" id="whatsapp_number_1" name="whatsapp_number_1" value="<?php echo $maid_val['maid_whatsapp_no_1'] ?>">
                                                        </div>
                                                    </div>
                                                    <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                        <p>Flat</p>
                                                        <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                            <select name="flat" id="flat" class="" required>
                                                                <?php
                                                                if (count($flats) > 0) {
                                                                    foreach ($flats as $flatsVal) {
                                                                        if ($flatsVal['flat_id'] == $maid_val['flat_id']) {
                                                                            $selected = "selected";
                                                                        } else {
                                                                            $selected = "";
                                                                        }
                                                                ?>
                                                                        <option value="<?php echo $flatsVal['flat_id'] ?>" <?php echo $selected; ?>><?php echo $flatsVal['flat_name'] ?></option>
                                                                <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row m-0 n-field-main" id="days-off">
                                                        <p>Days off</p>
                                                        <div class="col-sm-12 pl-0 pr-0 n-field-box n-field-day">
                                                        <?php
                                                            $daysOfWeek = array('','Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat','Sun');
                                                            $offDays = $maid_val['off_days']; // Replace with your off-days value

                                                            for ($i = 1; $i <= 7; $i++) {
                                                                $isChecked = in_array($i, explode(',', $offDays)) ? 'checked' : '';
                                                            ?>
                                                            <div class="n-days">
                                                                <input id="repeat-on-<?php echo $i; ?>" type="checkbox" value="<?php echo $i; ?>" name="off_days[]" class="w_day" <?php echo $isChecked; ?>>
                                                                <label for="repeat-on-<?php echo $i; ?>"> <span class="border-radius-3"></span>
                                                                    <p><?php echo $daysOfWeek[$i]; ?></p>
                                                                </label>
                                                            </div>
                                                            <?php
                                                            }
                                                        ?>
                                                            <div class="clear"></div>
                                                        </div>
                                                        </div>
                                                </fieldset>
                                            </div>
                                            <div class="col-sm-1">&nbsp;</div>
                                            <div class="col-sm-4">
                                                <div id="target-2" class="">
                                                    <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                        <p>Services</p>
                                                        <?php
                                                        if (count($services) > 0) {
                                                            foreach ($services as $key => $serviceVal) {
                                                        ?>
                                                                <div class="col-sm-12 pr-0 pl-0 n-field-box pb-3">
                                                                    <div class="col-sm-12 pr-0 pl-0 n-radi-check-main">
                                                                        <input type="checkbox" value="<?php echo $serviceVal['service_type_id'] ?>" id="services-<?= $key; ?>" name="services[]" class="services" <?php echo (in_array($serviceVal['service_type_id'], $service_maid)) ? "checked='checked'" : ''; ?>>
                                                                        <label for="services-<?= $key; ?>"> <span class="border-radius-3"></span>
                                                                            <p><?php echo $serviceVal['service_type_name'] ?></p>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                        <?php
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                        <p>Notes</p>
                                                        <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                            <textarea class="" rows="5" id="notes" name="notes"><?php echo $maid_val['maid_notes'] ?></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                        <p>Username</p>
                                                        <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                            <input type="text" class="" id="username" name="username" value="<?php echo $maid_val['username'] ?>">
                                                        </div>
                                                    </div>
                                                    <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                        <p>Password</p>
                                                        <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                            <input type="text" class="" id="password" name="password" value="<?php echo $maid_val['password'] ?>">
                                                        </div>
                                                    </div>
                                                    <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                        <p>App Login Status</p>
                                                        <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                            <select name="appstatus" id="appstatus" class="">
                                                                <option value="0" <?php echo isset($maid_val['maid_login_status']) ? ($maid_val['maid_login_status'] == "0" ? 'selected="selected"' : '') : '' ?>>Inactive</option>
                                                                <option value="1" <?php echo isset($maid_val['maid_login_status']) ? ($maid_val['maid_login_status'] == "1" ? 'selected="selected"' : '') : '' ?>>Active</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                        <p>Priority Number</p>
                                                        <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                            <input type="text" id="maid-priority" name="maid_priority" value="<?php echo $maid_val['maid_priority'] ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <div class="col-sm-12 mt-2 mb-4">
                                            <input type="hidden" name="call_method" id="call_method" value="maid/editmaidimgupload" />
                                            <input type="hidden" name="img_fold" id="img_fold" value="maidimg" />
                                            <input type="hidden" name="img_name_resp" id="img_name_resp" />
                                            
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="attachments">
                                        <fieldset>
                                            <div class="span5">
                                                <div class="widget">
                                                    <div class="widget-content" style="border: 0px">
                                                        <fieldset>
                                                            <div class="row m-0 n-field-main">
                                                                <p>Passport Number</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                    <input type="text" class="" id="passport_number" name="passport_number" value="<?php echo $maid_val['maid_passport_number'] ?>">
                                                                </div>
                                                            </div>
                                                            <?php
                                                            if ($maid_val['maid_passport_expiry_date'] != '' && $maid_val['maid_passport_expiry_date'] != '0000-00-00') {
                                                                list($year, $month, $day) = explode("-", $maid_val['maid_passport_expiry_date']);
                                                                $passport_expiry = "$day/$month/$year";
                                                            } else if ($maid_val['maid_passport_expiry_date'] = '0000-00-00') {
                                                                $passport_expiry = "";
                                                            }
                                                            ?>
                                                            <div class="row m-0 n-field-main">
                                                                <p>Passport Expiry</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                    <input type="text" class="" id="passport_expiry" name="passport_expiry" value="<?php echo $passport_expiry ?>">
                                                                </div>
                                                            </div>
                                                            <?php
                                                            if ($maid_val['maid_passport_file'] != '') {
                                                                $passport_image = base_url() . "maid_passport/" . $maid_val['maid_passport_file'];
                                                            ?>
                                                                <div class="row m-0 n-field-main">
                                                                    <p>Current Attachment</p>
                                                                    <div class="col-sm-12 p-0 n-field-box">
                                                                        <span><img src="<?php echo $passport_image; ?>" style="height: 100px; width: 100px" /></span>
                                                                        <input type="hidden" class="" id="old_attach_passport" name="old_attach_passport" value="<?php echo $maid_val['maid_passport_file'] ?>">
                                                                    </div>
                                                                </div>
                                                            <?php
                                                            } else {
                                                                $passport_image = base_url() . "img/no_image.jpg";
                                                            ?>
                                                                <div class="row m-0 n-field-main">
                                                                    <p>Current Attachment</p>
                                                                    <div class="col-sm-12 p-0 n-field-box">
                                                                        <span><img src="<?php echo $passport_image; ?>" style="height: 100px; width: 100px" /></span>
                                                                        <input type="hidden" class="" id="old_attach_passport" name="old_attach_passport" value="">
                                                                    </div>
                                                                </div>
                                                            <?php
                                                            }
                                                            ?>
                                                            <div class="row m-0 n-field-main">
                                                                <p>Attach Passport</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                    <input type="file" class="" id="attach_passport" name="attach_passport">
                                                                </div>
                                                            </div>
                                                            <div class="row m-0 n-field-main">
                                                                <p>Visa Number</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                    <input type="text" class="" id="visa_number" name="visa_number" value="<?php echo $maid_val['maid_visa_number'] ?>">
                                                                </div>
                                                            </div>
                                                            <?php
                                                            if ($maid_val['maid_visa_expiry_date'] != '' && $maid_val['maid_visa_expiry_date'] != '0000-00-00') {
                                                                list($year, $month, $day) = explode("-", $maid_val['maid_visa_expiry_date']);
                                                                $visa_expiry = "$day/$month/$year";
                                                            } else if ($maid_val['maid_visa_expiry_date'] = '0000-00-00') {
                                                                $visa_expiry = "";
                                                            }
                                                            ?>
                                                            <div class="row m-0 n-field-main">
                                                                <p>Visa Expiry</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                    <input type="text" class="" id="visa_expiry" name="visa_expiry" value="<?php echo $visa_expiry ?>">
                                                                </div>
                                                            </div>
                                                            <?php
                                                            if ($maid_val['maid_visa_file'] != '') {
                                                                $visa_image = base_url() . "maid_visa/" . $maid_val['maid_visa_file'];
                                                            ?>
                                                                <div class="row m-0 n-field-main">
                                                                    <p>Current Attachment</p>
                                                                    <div class="col-sm-12 p-0 n-field-box">
                                                                        <span><img src="<?php echo $visa_image; ?>" style="height: 100px; width: 100px" /></span>
                                                                        <input type="hidden" class="" id="old_attach_visa" name="old_attach_visa" value="<?php echo $maid_val['maid_visa_file'] ?>">
                                                                    </div>
                                                                </div>
                                                            <?php
                                                             } else {
                                                                $visa_image = base_url() . "img/no_image.jpg";
                                                            ?>
                                                                <div class="row m-0 n-field-main">
                                                                    <p>Current Attachment</p>
                                                                    <div class="col-sm-12 p-0 n-field-box">
                                                                        <span><img src="<?php echo $visa_image; ?>" style="height: 100px; width: 100px" /></span>
                                                                        <input type="hidden" class="" id="old_attach_visa" name="old_attach_visa" value="">
                                                                    </div>
                                                                </div>
                                                            <?php
                                                            }
                                                            ?>
                                                            <div class="row m-0 n-field-main">
                                                                <p>Attach Visa</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                    <input type="file" class="" id="attach_visa" name="attach_visa">
                                                                </div>
                                                            </div>
                                                        </fieldset>
                                                    </div> <!-- /widget-content -->
                                                </div> <!-- /widget -->
                                            </div> <!-- /span6 -->
                                            <div class="span5">
                                                <div id="target-2" class="widget">
                                                    <div class="widget-content" style="border: 0px">
                                                        <fieldset>
                                                            <div class="row m-0 n-field-main">
                                                                <p>Labour Card Number</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                    <input type="text" class="" id="labour_number" name="labour_number" value="<?php echo $maid_val['maid_labour_card_number'] ?>">
                                                                </div>
                                                            </div>
                                                            <?php
                                                            if ($maid_val['maid_labour_card_expiry_date'] != '' && $maid_val['maid_labour_card_expiry_date'] != '0000-00-00') {
                                                                list($year, $month, $day) = explode("-", $maid_val['maid_labour_card_expiry_date']);
                                                                $labour_expiry = "$day/$month/$year";
                                                            } else if ($maid_val['maid_labour_card_expiry_date'] = '0000-00-00') {
                                                                $labour_expiry = "";
                                                            }
                                                            ?>
                                                            <div class="row m-0 n-field-main">
                                                                <p>Labour Card Expiry</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                    <input type="text" class="" id="labour_expiry" name="labour_expiry" value="<?php echo $labour_expiry ?>">
                                                                </div>
                                                            </div>
                                                            <?php
                                                            if ($maid_val['maid_labour_card_file'] != '') {
                                                                $labour_image = base_url() . "maid_labour/" . $maid_val['maid_labour_card_file'];
                                                            ?>
                                                                <div class="row m-0 n-field-main">
                                                                    <p>Current Attachment</p>
                                                                    <div class="col-sm-12 p-0 n-field-box">
                                                                        <span><img src="<?php echo $visa_image; ?>" style="height: 100px; width: 100px" /></span>
                                                                        <input type="hidden" class="" id="old_attach_labour" name="old_attach_labour" value="<?php echo $maid_val['maid_labour_card_file'] ?>">
                                                                    </div>
                                                                </div>
                                                            <?php
                                                                //
                                                            } else {
                                                                $labour_image = base_url() . "img/no_image.jpg";
                                                            ?>
                                                                <div class="row m-0 n-field-main">
                                                                    <p>Current Attachment</p>
                                                                    <div class="col-sm-12 p-0 n-field-box">
                                                                        <span><img src="<?php echo $visa_image; ?>" style="height: 100px; width: 100px" /></span>
                                                                        <input type="hidden" class="" id="old_attach_labour" name="old_attach_labour" value="">
                                                                    </div>
                                                                </div>
                                                            <?php
                                                            }
                                                            ?>
                                                            <div class="row m-0 n-field-main">
                                                                <p>Attach Labour Card</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                    <input type="file" class="" id="attach_labour" name="attach_labour">
                                                                </div>
                                                            </div>
                                                            <div class="row m-0 n-field-main">
                                                                <p>Emirates Id</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                    <input type="text" class="" id="emirates_id" name="emirates_id" value="<?php echo $maid_val['maid_emirates_id'] ?>">
                                                                </div>
                                                            </div>
                                                            <?php
                                                            if ($maid_val['maid_emirates_expiry_date'] != '' && $maid_val['maid_emirates_expiry_date'] != '0000-00-00') {
                                                                list($year, $month, $day) = explode("-", $maid_val['maid_emirates_expiry_date']);
                                                                $emirates_expiry = "$day/$month/$year";
                                                            } else if ($maid_val['maid_emirates_expiry_date'] = '0000-00-00') {
                                                                $emirates_expiry = "";
                                                            }
                                                            ?>
                                                            <div class="row m-0 n-field-main">
                                                                <p>Emirates Id Expiry</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                    <input type="text" class="" id="emirates_expiry" name="emirates_expiry" value="<?php echo $emirates_expiry ?>">
                                                                </div>
                                                            </div>
                                                            <?php
                                                            if ($maid_val['maid_emirates_file'] != '') {
                                                                $emirates_image = base_url() . "maid_labour/" . $maid_val['maid_labour_card_file'];
                                                            ?>
                                                                <div class="row m-0 n-field-main">
                                                                    <p>Current Attachment</p>
                                                                    <div class="col-sm-12 p-0 n-field-box">
                                                                        <span><img src="<?php echo $emirates_image; ?>" style="height: 100px; width: 100px" /></span>
                                                                        <input type="hidden" class="" id="old_attach_emirates" name="old_attach_emirates" value="<?php echo $maid_val['maid_emirates_file'] ?>">
                                                                    </div>
                                                                </div>
                                                            <?php
                                                                //
                                                            } else {
                                                                $emirates_image = base_url() . "img/no_image.jpg";
                                                            ?>
                                                                <div class="row m-0 n-field-main">
                                                                    <p>Current Attachment</p>
                                                                    <div class="col-sm-12 p-0 n-field-box">
                                                                        <span><img src="<?php echo $emirates_image; ?>" style="height: 100px; width: 100px" /></span>
                                                                        <input type="hidden" class="" id="old_attach_emirates" name="old_attach_emirates" value="">
                                                                    </div>
                                                                </div>
                                                            <?php
                                                            }
                                                            ?>
                                                            <div class="row m-0 n-field-main">
                                                                <p>Attach Emirates Card</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                    <input type="file" class="" id="attach_emirates" name="attach_emirates">
                                                                </div>
                                                            </div>
                                                            <br />
                                                        </fieldset>
                                                    </div> <!-- /widget-content -->
                                                </div> <!-- /widget -->
                                            </div> <!-- /span5 -->
                                        </fieldset>
                                    </div>
                                </div>
                                <input type="submit" class="n-btn" value="Update" name="maid_edit" onclick="return validate_maid();">
                            </form>
                        </div>
                    </div> <!-- /widget-content -->
            <?php
                }
            }
            ?>
        </div> <!-- /widget -->
    </div> <!-- /span8 -->
</div> <!-- /row -->
<script>
    $(function() {
        var current = window.location.href;
        $('#primary_nav_wrap li a').each(function() {
            var $this = $(this);
            // if the current path is like this link, make it active
            if ($this.attr('href') === '<?php echo base_url('maids '); ?>') {
                $this.addClass('active');
            }
        })
    })
    var cropper;

    function showCropper() {
        cropper = new Cropper(image, {
            aspectRatio: 300 / 300,
            viewMode: 0,
            fillColor: '#fff',
            imageSmoothingEnabled: true,
            imageSmoothingQuality: 'high',
        });
        $.fancybox.open({
            autoCenter: true,
            fitToView: false,
            scrolling: false,
            openEffect: 'none',
            openSpeed: 1,
            autoSize: false,
            width: 450,
            height: 'auto',
            helpers: {
                overlay: {
                    css: {
                        'background': 'rgba(0, 0, 0, 0.3)'
                    },
                    closeClick: false
                }
            },
            padding: 0,
            closeBtn: false,
            content: $('#crop-popup'),
        });
    }

    function closeCropper() {
        cropper.destroy();
        cropper = null;
        $.fancybox.close();
    }
    window.addEventListener('DOMContentLoaded', function() {
        var avatar_img = document.getElementById('avatar_img');
        var image = document.getElementById('image');
        var input = document.getElementById('input-image');
        input.addEventListener('change', function(e) {
            var files = e.target.files;
            var done = function(url) {
                input.value = '';
                image.src = url;
                showCropper();
            };
            var reader;
            var file;
            var url;
            if (files && files.length > 0) {
                file = files[0];
                if (URL) {
                    done(URL.createObjectURL(file));
                } else if (FileReader) {
                    reader = new FileReader();
                    reader.onload = function(e) {
                        done(reader.result);
                    };
                    reader.readAsDataURL(file);
                }
            }
        });
        document.getElementById('crop').addEventListener('click', function() {
            var initialpromo_imgURL;
            var canvas;
            if (cropper) {
                canvas = cropper.getCroppedCanvas({
                    width: 300,
                    height: 300,
                });
                initialpromo_imgURL = avatar_img.src;
                avatar_img.src = canvas.toDataURL();
                $('#avatar_base64').val(avatar_img.src);
            }
            closeCropper();
        });
    });
    $(function() {
        let current = window.location.href;
        $('#primary_nav_wrap li a').each(function() {
            var $this = $(this);
            // if the current path is like this link, make it active
            if ($this.attr('href') === '<?php echo base_url('maids'); ?>') {
                $this.addClass('active');
            }
        })
    })
/************************************************************************** */
$('[data-action="select-skill"]').click(function (event) {
    var skill_id = $('select[id="skill_id"]').val();
    var skill = $('select[id="skill_id"]').select2('data')[0]['text'];
    var rating = $('select[id="skill_rating_id"]').select2('data')[0]['text'];
    var skill_rating_id = $('select[id="skill_rating_id"]').val();
    if (skill_id && skill_rating_id) {
        if (document.getElementById("skill_" + skill_id)) {
            alert("Skill already selected.");
            return false;
        }
        var html = `<div class="col-sm-3" id="skill_` + skill_id + `">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">` + skill + `<span class="pull-right">`+rating+`</span></h3>
                            </div>
                            <div class="panel-body">
                                <p>Notes</p>
                                <input type="hidden" name="skill_ids[]" value="` + skill_id + `"/>
                                <input type="hidden" name="skill_rating_level_ids[]" value="` + skill_rating_id + `"/>
                                <textarea style="width:100%" name="skill_notes[]"></textarea>
                            </div>
                            <div class="panel-footer text-right"><button type="button" class="btn btn-danger" data-id="` + skill_id + `" data-action="remove-skill"><i class="fa fa-trash" aria-hidden="true"></i></button></div>
                        </div>
                    </div>`;
        $('#skills-holder').append(html);
        $('[data-action="remove-skill"]').off();
        $('[data-action="remove-skill"]').click(function (event) {
            var skill_id = $(this).attr("data-id");
            $(this).closest('div#skill_' + skill_id).remove();
        });
    }
});
/************************************************************************** */
function skillRenderHtml(skill_id, skill, skill_rating_id, rating_level, notes) {
    return `<div class="col-sm-3" id="skill_` + skill_id + `">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">` + skill + `<span class="pull-right">` + rating_level + `</span></h3>
                    </div>
                    <div class="panel-body">
                        <p>Notes</p>
                        <input type="hidden" name="skill_ids[]" value="` + skill_id + `"/>
                        <input type="hidden" name="skill_rating_level_ids[]" value="` + skill_rating_id + `"/>
                        <textarea style="width:100%" name="skill_notes[]">` +(notes ? notes : '') + `</textarea>
                    </div>
                    <div class="panel-footer text-right"><button type="button" class="btn btn-danger" data-id="` + skill_id + `" data-action="remove-skill"><i class="fa fa-trash" aria-hidden="true"></i></button></div>
                </div>
            </div>`;
}
$().ready(function () {
    // employee skills render
    var skillHtml = ``;
    maid_skills.forEach(function (skill) {
        skillHtml += skillRenderHtml(skill.maid_skill_id, skill.skill, skill.rating_level_id, skill.rating_level, skill.notes);
    });
    $('#edit-profile #skills-holder').html(skillHtml);
    $('#edit-profile [data-action="remove-skill"]').off();
    $('#edit-profile [data-action="remove-skill"]').click(function (event) {
        var skill_id = $(this).attr("data-id");
        $(this).closest('div#skill_' + skill_id).remove();
    });
});
</script>