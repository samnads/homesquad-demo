<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?= $settings->site_name; ?> Invoice - <?= $invoice->reference; ?></title>
</head>
<style>
@page {
	margin: 0px 0px 0px 0px !important;
	padding: 0px 0px 0px 0px !important;
}


</style>
<body style="padding: 0px; margin: 0px;">
	<div class="main" style="width:793px; height:auto; padding: 0px; margin: 0px auto;">
		<header style="width:100%; height: 200px; overflow: hidden;  position: fixed; left:30px; top: 26px; z-index: 999;">
            <div style="width: 150px; height:auto; float: left; margin: 0px; padding: 3px 20px 0px 0px;">
                 <img src="<?= base_url('uploads/images/settings/home-squad-logo.png'); ?>" width="200" height="50" />
            </div>
            
            <div style="width: auto; height:auto; float: right;">
                 <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 20px; line-height: 16px; color: #333; padding: 15px 25px 0px 0px; margin: 0px;">
                 <?= $settings->site_name; ?>
                 </p>
                 
                 <p style="font-family: Arial, Helvetica, sans-serif; font-size: 15.5px; line-height: 20px; color: #333; padding: 10px 25px 0px 0px; margin: 0px;">
                    <?php $this->load->view('includes/company_invoice_address'); ?>
                 </p>
            </div>
            
            <div style="clear:both;"></div>
            
		</header>
		<section style="width:100%; height: 700px;  padding:200px 30px 0px 30px;">
			<div style="width: 100%; height:auto; margin: 0px; padding: 0px;">
				<div style="width: 50%; height:auto; float: left; margin: 0px; padding: 0px;">
					<p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 14px; line-height: 16px; color: #333; padding: 0px 0px 10px 0px; margin: 0px; text-transform: uppercase;">
                         Recipient :
					</p>
                     
					<p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 15px; line-height: 20px; color: #333; padding: 0px 0px 0px 20px; margin: 0px;">
                         <b><?php echo $invoice->customer_name; ?></b>,
					</p>
                     
					<p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; color: #333; padding: 0px 0px 0px 20px; margin: 0px;">
						<?php
						if($invoice->building != "")
						{
							echo $invoice->building.", ";
						}
						?>
                        <?php echo $invoice->customer_address; ?><br/><?php echo $invoice->area_name ?>
					</p>
				</div>
                 
				<div style="width: 50%; height:auto; float: left; margin: 0px; padding: 0px;">
					<div style="width: 100%; height:auto; margin: 0px; padding: 0px; background: #62A6C9;">
						<p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 16px; line-height: 16px; color: #FFF; padding: 10px 20px; margin: 0px;">
                         Tax Invoice :	 <?php echo $invoice->reference; ?>
						</p>
					</div>
                      
					<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 25px; color: #333; padding: 5px 0px 0px; margin: 0px; background: #eee;">
						<tr>
							<td width="50%" style="padding-left: 20px;">Issued</td>
                            <td width="50%" align="right" style="padding-right: 20px;"><?php echo date('d F - Y',strtotime($invoice->issue_date)) ?></td>
						</tr>
						<tr>
                            <td style="padding-left: 20px;">Due</td>
                            <td align="right" style="padding-right: 20px;"><?php echo date('d F - Y',strtotime($invoice->due_date)) ?></td>
						</tr>
						<tr>
                            <td style="padding-left: 20px;padding-bottom: 10px;">Attn</td>
                            <td align="right" style="padding-right: 20px; line-height:15px; padding-bottom: 5px;"><?php echo $invoice->customer_name; ?></td>
						</tr>
                          <?php
                          if($invoice->customer_trn != "")
                          {
                          ?>
						<tr>
                            <td style="padding-left: 20px; padding-bottom: 10px;">TRN</td>
                            <td align="right" style="padding-right: 20px; padding-bottom: 5px;"><?php echo $invoice->customer_trn; ?></td>
						</tr>
                          <?php
                          }
                          ?>
						<tr style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 16px; line-height: 16px; color: #FFF; padding: 0px; margin: 0px;">
                            <td style="padding: 15px 20px; background: #6098b7; ">Total</td>
                            <td align="right" style="padding: 15px 20px; background: #6098b7; "><span style="font-size: 14px;">AED</span> <?php echo number_format($invoice->total_amount,2); ?></td>
						</tr>
					</table>
				</div>
				<div style="clear:both;"></div>
			</div>
             
			<div style="width: 100%; height:auto; margin: 0px; padding: 10px 0px 0px 0px;">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 16px; color: #333; padding: 0px; margin: 0px;">
					<tr style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 13px; line-height: 20px; color: #FFF; background: #62A6C9; ">
						<td width="15%" align="center" style="padding: 3px 0px 3px 6px; border: 1px solid; border-color: #CCC;">Sl. No.</td>
						<td width="22%" align="center" style="padding: 3px 0px 3px 6px; border: 1px solid; border-color: #CCC;">Service Date</td>
						<td width="26%" align="center" style="padding: 3px 0px 3px 6px; border: 1px solid; border-color: #CCC;">Service</td>
						<td width="17%" align="center" style="padding: 3px 0px 3px 6px; border: 1px solid; border-color: #CCC;">Hour(s)</td>
						<td width="17%" align="center" style="padding: 3px 0px 3px 6px; border: 1px solid; border-color: #CCC;">Net Amount</td>
						<td width="17%" align="center" style="padding: 3px 0px 3px 6px; border: 1px solid; border-color: #CCC;">VAT</td>
						<td width="20%" align="center" style="padding: 3px 0px 3px 6px; border: 1px solid; border-color: #CCC;">TOTAL</td>
                    </tr>
                    
                    <?php
                    foreach ($invoice_lines as $key => $service)
                    {
                    ?>
                    <tr>
                      <td align="center" style="padding: 0px 0px 0px 5px;border: 1px solid; border-left: 1px solid; border-color: #CCC;"><?php echo $key+1; ?></td>
                      <td align="center" style="padding: 5px; border-bottom: 1px solid; border-right: 1px solid; border-color: #CCC;"><?php echo date('d-m-Y',strtotime($service->service_date)); ?></td>
                      <td align="center" style="border-bottom: 1px solid; border-right: 1px solid; border-color: #CCC;"><?php echo $service->service_type_name; ?></td>
                      <td align="center" style="border-bottom: 1px solid; border-right: 1px solid; border-color: #CCC;"><?php echo number_format($service->working_hours,2); ?></td>
                      <td align="center" style="border-bottom: 1px solid; border-right: 1px solid; border-color: #CCC;"><?php echo $service->service_charge; ?></td>
                      <td align="center" style="border-bottom: 1px solid; border-right: 1px solid; border-color: #CCC;"><?php echo $service->vat_charge; ?></td>
                      <td align="center" style="border-bottom: 1px solid; border-right: 1px solid; border-color: #CCC;"><span style="font-size: 11px;">AED</span> <?php echo number_format($service->total_amount,2); ?></td>
                    </tr>
                     <?php
					  } 
                    ?>
				</table>
                  
				<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 15px; color: #333; font-weight: bold; padding: .5px 0px 5px -5px; margin: 0px;">
                    <tr style="">
                      <td width="30%">&nbsp;</td>
                      <td width="35%" style="padding:7px 10px; background: #eee; border-left: 1px solid; border-color: #CCC;">Subtotal</td>
                      <td width="35%" align="right" style="padding: 7px 10px; background: #eee; border-right: 1px solid; border-color: #CCC;"><span style="font-size:10px;">AED</span> <?php echo number_format($invoice->net_amount,2); ?></td>
                    </tr>
                    
                    <tr>
                      <td>&nbsp;</td> 
                      <?php
                      $service_vat_percentage = $invoice->vat_charge > 0 ? (($invoice->vat_charge * 100)/$invoice->net_amount) : 0;
                      ?>
                      <td style="padding: 7px 10px; border-left: 1px solid; border-color: #CCC;">VAT (<?= number_format($service_vat_percentage,2); ?>%)</td>
                      <td align="right" style="padding: 7px 10px; border-right: 1px solid; border-color: #CCC;"><span style="font-size:10px;">AED</span> <?php echo number_format($invoice->vat_charge,2); ?></td>
                    </tr>
                    
                    <tr style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 14px; line-height: 14px; color: #FFF; padding: 0px; margin: 0px;">
                        <td>&nbsp;</td> 
                        <td style="padding: 10px 10px; background: #6098b7; ">Total</td>
                        <td align="right" style="padding: 10px 10px; background: #6098b7; "><span style="font-size: 13px;">AED</span> <?php echo number_format($invoice->total_amount,2); ?></td>
                    </tr>
				</table>
			</div>
			 
			<table width="100%"  border="0" cellspacing="0" cellpadding="0" style="margin-top: 20px;" >
				<tr>
					<td width="22%"><p style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; line-height: 16px; color: #333; padding: 5px 15px 5px 0px; margin: 0px;"><strong>Bank Details</strong></p></td>
					<td width="4%">&nbsp;</td>
					<td width="74%">&nbsp;</td>
				</tr>
	
				<tr>
					<td><p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 16px; color: #333; padding: 5px 15px 5px 15px; margin: 0px;">BANK NAME</p></td>
					<td>&nbsp;</td>
					<td><p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 16px; color: #333; padding: 5px 15px 5px 0px; margin: 0px;">Example Bank Name</p></td>
				</tr>
	
				<tr>
					<td><p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 16px; color: #333; padding: 5px 15px 5px 15px; margin: 0px;">ACCOUNT NAME</p></td>
					<td>&nbsp;</td>
					<td><p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 16px; color: #333; padding: 5px 15px 5px 0px; margin: 0px;">Example Account Name</p></td>
				</tr>
	
				<tr>
					<td><p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 16px; color: #333; padding: 5px 15px 5px 15px; margin: 0px;">ACCOUNT NUMBER</p></td>
					<td>&nbsp;</td>
					<td><p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 16px; color: #333; padding: 5px 15px 5px 0px; margin: 0px;">0000000000</p></td>
				</tr>
	
				<tr>
					<td><p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 16px; color: #333; padding: 5px 15px 5px 15px; margin: 0px;">IBAN NUMBER</p></td>
					<td>&nbsp;</td>
					<td><p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 16px; color: #333; padding: 5px 15px 5px 0px; margin: 0px;">0000000000</p></td>
				</tr>
			</table>
		</section>
    
		<footer style="height: 50px; position: fixed; left:0; bottom:0; z-index: 999;">
			<div style=" width: 100%; margin-top: 25px; border-top: 1px solid #888;">
				<p style="font-family: Verdana, Geneva, sans-serif; font-size: 11px;color: #333; line-height: 16px; text-align: center; padding: 0px; margin: 0 auto; font-weight: 600; ">This is computer generated invoice and does not require a signature.</p>
			</div>  
		</footer>
	</div>

</body>
</html>
