<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<section>
  <div class="row dash-top-wrapper no-left-right-margin">
      <div class="col-md-12 col-sm-12 no-left-right-padding">
      
      		
        <div class="col-md-3 col-sm-3 colour-box-main">
        	<div class="colour-box light-blue dash-suitcase">
            	<div class="total-booking-num"><?php echo $total_booking_count; ?></div><!--total-booking-num end-->
                <div class="total-booking-text">Total Jobs</div><!--total-booking-num end-->
                <?php
                if(user_permission(user_authenticate(), 2)) {
                ?>
                <div class="view-all-booking">
                    <a href="<?php echo base_url() . 'booking'; ?>" title="Create new job"><span class="colour-rit-icon no-left-padding"><i class="fa fa-briefcase"></i></span> Create<div class="clear"></div></a>
                    <a class="text-right" href="<?php echo base_url() . 'booking'; ?>" title="View all job">View <span class="colour-rit-icon no-right-padding"><i class="fa fa-arrow-circle-o-right"></i></span> <div class="clear"></div></a>
                    <div class="clear"></div>
                </div><!--colour-box end-->
                  
                <?php
                } else {
                ?>
                <div class="view-all-booking">
                    <a href="#" title="Create new job"><span class="colour-rit-icon no-left-padding"><i class="fa fa-briefcase"></i></span> Create<div class="clear"></div></a>
                    <a class="text-right" href="#" title="View all job">View <span class="colour-rit-icon no-right-padding"><i class="fa fa-arrow-circle-o-right"></i></span> <div class="clear"></div></a>
                    <div class="clear"></div>
                </div><!--colour-box end-->
                <?php } ?>
            </div><!--colour-box end-->
        </div>
        
        
         
        <div class="col-md-3 col-sm-3 colour-box-main">
        	<div class="colour-box violet dash-calender">
            	<div class="total-booking-num"><?php echo $maid_count; ?></div><!--total-booking-num end-->
                <div class="total-booking-text">Scheduled Jobs</div><!--total-booking-num end-->
                <?php if(user_permission(user_authenticate(), 3)) {?>
                <div class="view-all-booking">
                    <a href="<?php echo base_url() . 'booking'; ?>" title="Schedule new jobs"><span class="colour-rit-icon no-left-padding"><i class="fa fa-calendar"></i></span> Create<div class="clear"></div></a>
                    <a class="text-right" href="<?php echo base_url() . 'booking'; ?>" title="View scheduled jobs">View <span class="colour-rit-icon no-right-padding"><i class="fa fa-arrow-circle-o-right"></i></span> <div class="clear"></div></a>
                    <div class="clear"></div>
                </div><!--colour-box end-->
                
                <?php } else { ?>
                <div class="view-all-booking">
                    <a href="#" title="Schedule new jobs"><span class="colour-rit-icon no-left-padding"><i class="fa fa-briefcase"></i></span> Create<div class="clear"></div></a>
                    <a class="text-right" href="#" title="View scheduled jobs">View <span class="colour-rit-icon no-right-padding"><i class="fa fa-arrow-circle-o-right"></i></span> <div class="clear"></div></a>
                    <div class="clear"></div>
                </div><!--colour-box end-->
                <?php } ?>
            </div><!--colour-box end-->
        </div>
        
         
        
        <div class="col-md-3 col-sm-3 colour-box-main">
        	<div class="colour-box brown dash-group">
            	<div class="total-booking-num"><?php echo $customer_count; ?></div><!--total-booking-num end-->
                <div class="total-booking-text">Customers</div><!--total-booking-num end-->
                <?php if(user_permission(user_authenticate(), 6)) {?>
                <div class="view-all-booking">
                    <a href="<?php echo base_url() . 'customer/add'; ?>" title="Create new customer"><span class="colour-rit-icon no-left-padding"><i class="fa fa-users"></i></span> Create<div class="clear"></div></a>
                    <a class="text-right" href="<?php echo base_url() . 'customers'; ?>" title="View all customers">View <span class="colour-rit-icon no-right-padding"><i class="fa fa-arrow-circle-o-right"></i></span> <div class="clear"></div></a>
                    <div class="clear"></div>
                </div><!--colour-box end-->
                
                <?php } else { ?>
                <div class="view-all-booking">
                    <a href="#" title="Create new customer"><span class="colour-rit-icon no-left-padding"><i class="fa fa-users"></i></span> Create<div class="clear"></div></a>
                    <a class="text-right" href="#" title="View all customers">View <span class="colour-rit-icon no-right-padding"><i class="fa fa-arrow-circle-o-right"></i></span> <div class="clear"></div></a>
                    <div class="clear"></div>
                </div><!--colour-box end-->
                <?php } ?>
            </div><!--colour-box end-->
        </div>
        
        
      
        
        
        
        <div class="col-md-3 col-sm-3 colour-box-main">
        	<div class="colour-box yellow dash-paperclip">
            	<div class="total-booking-num"><?php echo $total_invoices; ?></div><!--total-booking-num end-->
                <div class="total-booking-text">Invoices</div><!--total-booking-num end-->
                <?php if(user_permission(user_authenticate(), 5)) {?>
                <div class="view-all-booking">
                    <a href="<?php echo base_url() . 'invoice'; ?>" title="Create new invoice"><span class="colour-rit-icon no-left-padding"><i class="fa fa-paperclip"></i></span> Create<div class="clear"></div></a>
                    <a class="text-right" href="<?php echo base_url() . 'invoice'; ?>" title="View all invoices">View <span class="colour-rit-icon no-right-padding"><i class="fa fa-arrow-circle-o-right"></i></span> <div class="clear"></div></a>
                    <div class="clear"></div>
                </div><!--colour-box end-->
                <?php } else { ?>
                <div class="view-all-booking">
                    <a href="#" title="Create new invoice"><span class="colour-rit-icon no-left-padding"><i class="fa fa-file-text-o"></i></span> Create<div class="clear"></div></a>
                    <a class="text-right" href="#" title="View all invoices">View <span class="colour-rit-icon no-right-padding"><i class="fa fa-arrow-circle-o-right"></i></span> <div class="clear"></div></a>
                    <div class="clear"></div>
                </div><!--colour-box end-->
                <?php } ?>
            </div><!--colour-box end-->
        </div>
        

      </div><!--welcome-text-main end--> 
      
      
      
      
      
      
  </div><!--row content-wrapper end--> 
</section><!--welcome-text end-->












<section>
  <div class="row content-wrapper no-left-right-margin">
      <div class="col-md-12 col-sm-12 no-left-right-padding">



         <div class="col-md-6 col-sm-6">
         
           <div class="col-md-12 col-sm-12 tab-hed no-left-right-padding">
              <h2><i class="fa fa-clock-o">&nbsp;</i> Recent Activity
              <span class="pull-right">
                  <a href="<?php echo base_url() ?>reports/useractivity" title="View All"><i class="btn-icon-only fa fa-eye "> </i></a>
              </span>
              </h2>
              
           </div><!--tab-hed end-->
           
           
           <div class="col-md-12 col-sm-12 tab-cont no-padding rcnt-actvty-box" >
              
              
              <div class="report-cont-det">
                <?php
                if(!empty($recent_activity))
                {
                ?>
              	<ul>
                    <?php
                        foreach ($recent_activity as $recent)
                        {
                            
                    ?>
                   <li><a href="#" <?php if(strpos($recent->action_type, 'delete') || strpos($recent->action_type, 'disable') || strpos($recent->action_type, 'Cancel') ) { ?> style="color:#f00;" <?php } else { ?> style="color:#428bca" <?php }?>><?php echo activity_icon($recent->action_type) ?>&nbsp;&nbsp;<?php echo $recent->action_content; ?><span class="activity-time"><i class="fa fa-clock-o"></i> <b><?php echo $recent->user_fullname; ?></b>   <i><?php echo time_elapsed_string($recent->addeddate); ?></i> </span><div class="clear"></div></a></li>
                    <?php
                        }
                    ?>
                    
                    
                </ul>
                <?php }else { ?>
                  <p>No recent activities!</p> 
                <?php }?>
              </div><!--report-cont-det end-->
              
              
              
              
              
              
           </div><!--tab-hed end-->
           
           
           
           
           
           
           
           
           
         
         </div>

         <div class="col-md-6 col-sm-6 no-left-right-padding">
             		
        <div class="col-md-6 col-sm-6 colour-box-main" style="padding-bottom: 10px;">
        	<div class="colour-box green dash-files">
                <div class="total-booking-text">Booking Summary</div><!--total-booking-num end-->
                <div class="dash-rght-box">Daily - <?php echo @round($dailySummaryperc,2); ?> % &nbsp;&nbsp;&nbsp;<i class="fa fa-info-circle" title="(Total booking hrs/(total schedule maids * 8))*100"></i></div><!--total-booking-num end-->
                <div  class="dash-rght-box">Weekly - <?php echo @round($weeklySummaryperc,2); ?> % &nbsp;&nbsp;&nbsp;<i class="fa fa-info-circle" title="(Weekly total booking hrs/(weekly total scheduled maids * 8 * total week days))*100"></i></div><!--total-booking-num end-->
                <div class="dash-rght-box">Monthly - <?php echo @round($mnthlySummaryperc,2); ?> % &nbsp;&nbsp;&nbsp;<i class="fa fa-info-circle" title="(Monthly total booking hrs / (monthly total scheduled maids * 8 * total days in month)) * 100"></i></div><!--total-booking-num end-->
                <div class="view-all-booking"> </div><!--colour-box end-->
            </div><!--colour-box end-->
        </div> 
        <div class="col-md-6 col-sm-6 colour-box-main" style="padding-bottom: 10px;">
        	<div class="colour-box red dash-complaint">
                <div class="total-booking-text">Complaints</div><!--total-booking-num end-->
                <div class="dash-rght-box">Today - <?php echo @$total_daily_complaint_count; ?></div><!--total-booking-num end-->
                <div  class="dash-rght-box"> Weekly - <?php echo @$total_weekly_complaint_count; ?></div><!--total-booking-num end-->
                <div class="dash-rght-box"> Monthly - <?php echo @$total_monthly_complaint_count; ?></div><!--total-booking-num end-->
                <div class="view-all-booking"> </div><!--colour-box end-->
            </div><!--colour-box end-->
        </div> 

<div class="col-md-12 col-sm-12 colour-box-main" style="padding-bottom: 0px;">
            <div class="report-cont-graph">
                <div id="chart_div"></div>
              </div>
        </div>
        
          
          
         </div>
         

      </div><!--welcome-text-main end--> 
  </div><!--row content-wrapper end--> 
</section><!--welcome-text end-->


<?php 
function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}
?>
<script>
    google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
     var data = new google.visualization.DataTable();
        data.addColumn('date', 'Date');
        data.addColumn('number', 'Schedules');
        data.addColumn({type: 'string', role: 'tooltip'});
        //data.addColumn('number', 'persone2');
        //data.addColumn('number', 'persone3');
        data.addRows([
            <?php
            foreach ($grapharray as $graphval)
            {
            ?>
    [<?php echo $graphval; ?>],
            <?php } ?>
])

        var options = {
          legend: 'none',
          //hAxis: { minValue: 0, maxValue: 9 },
          //curveType: 'function',
          pointSize: 10,
      };

      var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    
    
    //     var chart = new google.visualization.AnnotatedTimeLine(document.getElementById('chart_div'));
    //  chart.draw(data, {displayAnnotations: true});
      }
    
    </script>