<?php
foreach($maids as $maid)
{
	$top = 15;
	?>
        <div class="row tb-slider" id="<?php echo 'maid-' . $maid->maid_id; ?>">
		<?php
		$booked = isset($maid_bookings[$maid->maid_id]) ? $maid_bookings[$maid->maid_id] : array();
		$f_slot = FALSE;
		$b_slot = FALSE;
		foreach($times as $time_index=>$time)
		{
			if($b_slot && isset($tb_end_time) && $tb_end_time == $time->stamp)
			{
				echo '</div>';
				$b_slot = FALSE;
				$f_slot = FALSE;
			}

			if(!$b_slot && isset($booked[$time->stamp]['end_time']))
			{
				if($f_slot)
				{
					echo '</div>';
				}

				$f_slot = FALSE;
				$b_slot = TRUE;
				$tb_end_time = $booked[$time->stamp]['end_time'];
				echo '<div class="slot">';
			}

			if(!$b_slot && !$f_slot)
			{
				$f_slot = TRUE;
                                
                                if(in_array($maid->maid_id, $leave_maid_ids)) // Maid Leave
                                {
									foreach($leave_maid_types as $typevalue)
									{
										if($typevalue['maid_id'] == $maid->maid_id)
										{
											if($typevalue['maid_type'] == "leave")
											{
												echo '<div class="slot casualleave"><div class="leave">Casual Leave</div>';
											} else if($typevalue['maid_type'] == "absent"){
												echo '<div class="slot absent"><div class="leave">Absent</div>';
											} else if($typevalue['maid_type'] == "dayoff"){
												echo '<div class="slot dayoff"><div class="leave">DAY OFF</div>';
											} else if($typevalue['maid_type'] == "offset"){
												echo '<div class="slot offset"><div class="leave">Offset</div>';
											} else if($typevalue['maid_type'] == "sick leave"){
												echo '<div class="slot sickleave"><div class="leave">Sick Leave</div>';
											}else if($typevalue['maid_type'] == "sm_replacement"){
												echo '<div class="slot sm_replacement"><div class="leave">SM Replacement</div>';
											}
											break;
										}
									}	
                                }
                                else
                                {
                                    echo '<div class="' . ($booking_allowed == 1 ? 'selectable' : '') . ' slot">';
                                }
				
			}
			?>

			<div class="cell" id="<?php echo $time_index; ?>" <?php if($current_hour_index === $time_index) { echo 'style="border-right:1px solid #F00;"'; } ?>><?php // echo $time->display . '-' . $current_hour_index; ?>
				<?php
				if($b_slot && isset($booked[$time->stamp]['end_time']) && isset($tb_end_time))
				{
					//$diff = (($tb_end_time - $time->stamp) / 60) / 60;
					$diff = (($tb_end_time - $time->stamp) / 60) / 30;
					$width = ($diff * 71) - 22 + 20;
					//$width = ($diff * 100) - 22 + 20;
					//$width = ($diff * 100) - 22;
					$top = 3;
					//$top = $top == 3 ? 20 : 3;
                                        //Payment Type
                                        if($booked[$time->stamp]['payment_type'] == "D")
                                        {
                                            $paytype = "(D)";
                                        } else if($booked[$time->stamp]['payment_type'] == "W")
                                        {
                                            $paytype = "(W)";
                                        } else if($booked[$time->stamp]['payment_type'] == "M")
                                        {
                                            $paytype = "(M)";
                                        } else
                                        {
                                            $paytype = "";
                                        }
					//$top = $top == 3 ? 34 : 3;
                                        $progress_service_icon='';
                                        $scheduled_date = date("Y-m-d"); 
                                        //$job_service=$this->bookings_model->job_start_or_finish($booked[$time->stamp]['booking_id'],$scheduled_date);
                                        // if(!empty($job_service)){
                                        // if($job_service[0]->service_status==1){
                                        // $progress_service_icon='<i class="fa fa-gear fa-spin" style="font-size:18px;" title="In progress"></i>';
                                        // }
                                        // if($job_service[0]->service_status==2){
                                        // $progress_service_icon='<i class="fa fa-check" aria-hidden="true" style="font-size:18px;"></i>';
                                        // }
                                        
                                        // }
										if($booked[$time->stamp]['servicestatus'] != "")
										{
											if($booked[$time->stamp]['servicestatus'] == 1)
											{
												$progress_service_icon='<i class="fa fa-gear fa-spin" style="font-size:18px;" title="In progress"></i>';
											}
											if($booked[$time->stamp]['servicestatus'] == 2)
											{
												$progress_service_icon='<i class="fa fa-check" aria-hidden="true" style="font-size:18px;"></i>';
											}
										}
                                        
					//echo '<div class="schedule_bubble ' . strtolower($booked[$time->stamp]['type']) . '" id="booking-' . $booked[$time->stamp]['booking_id'] . '" style="top:' . $top . 'px; width:' . $width . 'px" title="' . html_escape($booked[$time->stamp]['customer'] . ' - ' . $booked[$time->stamp]['zone'] . ' (' . date('g:i a', $booked[$time->stamp]['start_time']) . ' to ' . date('g:i a', $booked[$time->stamp]['end_time']) . ')') . '"><div>' . html_escape($booked[$time->stamp]['customer'] . ' - ' . $booked[$time->stamp]['zone']) . '<br /> ' . date('g:i a', $booked[$time->stamp]['start_time']) . ' to ' . date('g:i a', $booked[$time->stamp]['end_time']) . ' <span style="border-radius:1px; padding:3px;"> - ' . $booked[$time->stamp]['user'] . '<br>B.A : '.$booked[$time->stamp]['balance'].$booked[$time->stamp]['signed'].' '. $paytype .'</span>'.$progress_service_icon.'</div></div>';
					// echo '<div class="schedule_bubble ' . strtolower($booked[$time->stamp]['zonetype']) . '" id="booking-' . $booked[$time->stamp]['booking_id'] . '" style="top:' . $top . 'px; width:' . $width . 'px" title="' . html_escape($booked[$time->stamp]['customer'] . ' - ' . $booked[$time->stamp]['zone'] . ' (' . date('g:i a', $booked[$time->stamp]['start_time']) . ' to ' . date('g:i a', $booked[$time->stamp]['end_time']) . ')') . '"><div>' . html_escape($booked[$time->stamp]['customer'] . ' - ' . $booked[$time->stamp]['zone']) . '<br /> ' . date('g:i a', $booked[$time->stamp]['start_time']) . ' to ' . date('g:i a', $booked[$time->stamp]['end_time']) . ' <span style="border-radius:1px; padding:3px;"> - ' . $booked[$time->stamp]['user'] . '<br>B.A : '.$booked[$time->stamp]['balance'].$booked[$time->stamp]['signed'].' '. $paytype .' - '.$booked[$time->stamp]['ref'] .'</span>'.$progress_service_icon.'</div></div>';
					echo '<div class="schedule_bubble ' . strtolower($booked[$time->stamp]['type']) . '" id="booking-' . $booked[$time->stamp]['booking_id'] . '" style="top:' . $top . 'px; width:' . $width . 'px;" title="' . html_escape($booked[$time->stamp]['customer'] . ' - ' . $booked[$time->stamp]['zone'] . ' (' . date('g:i a', $booked[$time->stamp]['start_time']) . ' to ' . date('g:i a', $booked[$time->stamp]['end_time']) . ')') . '"><div>' . html_escape($booked[$time->stamp]['customer'] . ' - ' . $booked[$time->stamp]['zone']) . '<br /> ' . date('g:i a', $booked[$time->stamp]['start_time']) . ' to ' . date('g:i a', $booked[$time->stamp]['end_time']) . ' <span style="border-radius:1px; padding:3px;"> - ' . $booked[$time->stamp]['user'] . '<br>B.A : '.$booked[$time->stamp]['balance'].$booked[$time->stamp]['signed'].' '. $paytype .' - '.$booked[$time->stamp]['ref'] .'</span>'.$progress_service_icon.'</div><h4 href="#" onclick="copyBookingMessage(this)" data-id="'.$booked[$time->stamp]['booking_id'].'" style="posiion:absolute;z-index:99;color:#fff;"><i class="fa fa-clipboard fa-1x" aria-hidden="true"></i></h4></div>';
				}
				?>
			</div>
		<?php
		}

		if($f_slot || $b_slot)
		{
			echo '</div>';
		}
		?>						
		<div class="clear"></div>
	</div>
	<?php
        
}

?>
<!--<input type="hidden" id="all-bookings" value='<?php //echo json_encode($all_bookings); ?>' />-->
<input type="hidden" id="all-bookings" value='<?php echo htmlspecialchars(json_encode($all_bookings), ENT_QUOTES, 'UTF-8'); ?>' />