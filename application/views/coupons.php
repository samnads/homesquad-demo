<div id="disable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Deactivate ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to deactivate this coupon ?</h3>
      <input type="hidden" id="deactivate_id">
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0" data-dismiss="modal" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" data-dismiss="modal" onclick="confirm_disable()">Deactivate</button>
    </div>
  </div>
</div>
<div class="row m-0">  
    <div class="col-md-12">
        <div class="widget widget-table action-table">
            
               
            
            
            
            
            <div class="widget-header">
     <div class="book-nav-top">
            <ul>
              <li><i class="icon-th-list"></i><h3>Coupons</h3></li>
              
          
          
      
      
              <li class="mr-0 float-right">
                  <div class="topiconnew border-0 green-btn">
                      <a href="<?php echo base_url(); ?>coupons/add" title="Add"> <i class="fa fa-plus"></i></a>
                 </div>
              </li>
      
      
      
              <div class="clear"></div>
            </ul>
     </div>
</div>
            
            
            
            
                     
            <div class="widget-content">

                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 30px; text-align:center;"> Sl.No. </th>
                            <th style="line-height: 18px;">Name </th>
							<th style="line-height: 18px;">Service</th>
                            <th style="line-height: 18px;"> Discount Price</th>
                            <th style="line-height: 18px;"> Offer Type</th>
                            <!--<th style="line-height: 18px;"> Discount Type</th>-->
							<th style="line-height: 18px;"> Coupon Type</th>
                            <th style="line-height: 18px;"> Week Days</th>
                            <th style="line-height: 18px;">Validity </th>
                            <th style="line-height: 18px;">Min Hrs </th>
                            <th style="line-height: 18px;">For App Only </th>
                            <th style="line-height: 18px; text-align: center;"> Status</th>
                            <th style="line-height: 18px; text-align: center;"> Action </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($coupons) > 0) {
                            $j = 1;
                            foreach ($coupons as $coupon) {
								if($coupon->status == 1)
								{
									$status = "Active";
								} else {
									$status = "Inactive";
								}
								
								if($coupon->coupon_type == "FT")
								{
									$coupon_type = "First Booking";
								} else {
									$coupon_type = "All Bookings";
								}
								
								if($coupon->offer_type == "F")
								{
									if($coupon->discount_type == 0)
									{
										$offertype = "Percentage";
									}
									else
									{
										$offertype = "Flat Rate";
									}
									
								} else {
									$offertype = "Per Hour";
								}
								
								// if($coupon->discount_type == "0")
								// {
									// $discounttype = "Percentage based";
								// } else {
									// $discounttype = "Price based";
								// }
								
								if($coupon->valid_week_day != "")
								{
									$arr = explode(",",$coupon->valid_week_day);
									$weekarrays = array();
									for($i=0; $i<7; $i++)
									{
										if(in_array($i, $arr))
										{
											if($i == 0)
											{
												$dayname = "SUN";
											} else if($i == 1){
												$dayname = "MON";
											} else if($i == 2){
												$dayname = "TUE";
											} else if($i == 3){
												$dayname = "WED";
											} else if($i == 4){
												$dayname = "THU";
											} else if($i == 5){
												$dayname = "FRI";
											} else if($i == 6){
												$dayname = "SAT";
											}
											array_push($weekarrays, $dayname);
										}
									}
									$validweekday = implode(",", $weekarrays);
									
								} else {
									$validweekday = "";
								}
                                ?>  
                                <tr>
                                    <td style="line-height: 18px; width: 20px; text-align:center;"><?php echo $j; ?></td>
                                    <td style="line-height: 18px;"><?php echo $coupon->coupon_name ?></td>
									<td style="line-height: 18px;"><?php echo $coupon->service_type_name ?></td>
                                    <td style="line-height: 18px;"><?php echo $coupon->percentage ?></td>
                                    <td style="line-height: 18px;"><?php echo $offertype; ?></td>
                                    <!--<td style="line-height: 18px;"><?php// echo $discounttype; ?></td>-->
									<td style="line-height: 18px;"><?php echo $coupon_type; ?></td>
                                    <td style="line-height: 18px;"><?php echo $validweekday; ?></td>
                                    <td style="line-height: 18px;"><?php echo $coupon->expiry_date ?></td>
                                    <td style="line-height: 18px;"><?php echo $coupon->min_hrs; ?></td>
                                    <td style="line-height: 18px;">
										<?php echo $coupon->forApp == 1 ? 'Yes' : 'No'; ?>
									</td>
                                    <td style="line-height: 18px; text-align: center;"><?php echo $coupon->status == 1 ? '<span class="n-btn-icon green-btn"><i class="btn-icon-only icon-ok"> </i></span>' : '<span class="n-btn-icon red-btn"><i class="btn-icon-only icon-remove-sign"> </i></span>'; ?></td>
									<td style="line-height: 18px; text-align: center;">
										
                                        <a class="n-btn-icon purple-btn" title="Edit" href="<?php echo base_url() . 'coupons/edit/' . $coupon->coupon_id ?>"><i class="btn-icon-only icon-pencil" style="font-size: 13px;"> </i></a>
                                        <!--<a class="btn btn-small btn-info" title="Login Info" href="<?php// echo base_url() . 'users/userlogininfo/' . $coupon->coupon_id ?>"><i class="btn-icon-only icon-search" style="font-size: 13px;"> </i></a>-->
                                        
                                        <a href="javascript:;" class="n-btn-icon red-btn" title="Deactivate" onclick="confirm_disable_modal(<?php echo $coupon->coupon_id; ?>);"><i class="btn-icon-only icon-remove"> </i></a>
									</td>
                                </tr>
                            <?php
                            $j++;
                        }
                    }
                    ?>   

                    </tbody>
                </table>                 
            </div>            
        </div>        
    </div>

</div>
<script>
function closeFancy() {
	$.fancybox.close();
}
function confirm_disable_modal(id) {
	$('#deactivate_id').val(id);
	$.fancybox.open({
		autoCenter: true,
		fitToView: false,
		scrolling: false,
		openEffect: 'none',
		openSpeed: 1,
		autoSize: false,
		width: 450,
		height: 'auto',
		helpers: {
			overlay: {
				css: {
					'background': 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding: 0,
		closeBtn: false,
		content: $('#disable-popup'),
	});
}
function confirm_disable() {
	$.ajax({
		type: "POST",
		url: _base_url + "settings/remove_coupon",
		data: {
			coupon_id: $('#deactivate_id').val()
		},
		dataType: "text",
		cache: false,
		success: function(result) {
			window.location.assign(_base_url + 'coupons');
		},
		error: function() {
			alert('Error !')
		}
	});
}
</script>

