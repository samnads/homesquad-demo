<html>
    <head>
    </head>
    <body>
        <div style="margin:0;padding:0;background-color:#f2f2f2;min-height:100%!important;width:100%!important">
            <center>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;margin:0;padding:0;background-color:#f2f2f2;">
                    <tbody>
                        <tr>
                            <td align="center" valign="top" style="margin:0;padding:20px;border-top:0;height:100%!important;width:100%!important">
                                <table border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse;border:0">
                                    <tbody>
                                        <tr>
                                            <td align="center" valign="top">

                                                <table border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse;background-color:#ffffff;border-top:0;border-bottom:0">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="top"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse;background-color:#ffffff;border-top:0;border-bottom:0">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td valign="top" style="padding:0px">
                                                                                <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td valign="top" style="padding-right:0px;padding-left:0px;padding-top:0;padding-bottom:0">
                                                                                                <img align="left" alt="" src="<?php echo base_url() ?>images/email-banner-new.jpg" width="800" style="max-width:1144px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;outline:none;text-decoration:none" class="CToWUd a6T" tabindex="0"><div class="a6S" dir="ltr" style="opacity: 1; left: 629px; top: 304px;"><div id=":1z9" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V" data-tooltip="Download"><div class="aSK J-J5-Ji aYr"></div></div></div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td valign="top">
                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left">
                                                                                                <br>
                                                                                                <?php echo $content; ?>
                                                                                                <br>
                                                                                                <br>
                                                                                                <br>    
                                                                                                <br>
                                                                                                <b>Crystal Blu Cleaning Services</b><br>
                                                                                                Dusseldorf Business Center,<br>
                                                                                                Al Barsha, Office 201,<br>
                                                                                                Dubai, UAE<br>
                                                                                                For Bookings : 8002258 / 04- 2424131 <br>
																								Email : info@crystalblu.ae<br>     
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>        
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td valign="top">
                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left">
                                                                                                <div style="text-align:center"><em style="color:#606060;font-family:helvetica;font-size:11px;line-height:15.6199998855591px;text-align:center">&copy; <?php echo date('Y'); ?> Crystalblu All Rights Reserved.</em><br style="color:#606060;font-family:Helvetica;font-size:11px;text-align:center;line-height:15.6199998855591px">
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse;background-color:#ffffff;border-top:0;border-bottom:0">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td valign="top">
                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left"></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse;background-color:#ffffff;border-top:0;border-bottom:0">
                                                    <tbody>
                                                        <tr>
                                                            <td align="left" valign="top" width="50%">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td valign="top"></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td align="left" valign="top" width="50%">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td valign="top"></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top">

                                                <table border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse;background-color:#ffffff;border-top:0;border-bottom:0">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="top" style="padding-bottom:9px"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </center>
        </div>
    </body>
</html>