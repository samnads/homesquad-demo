<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
    .topiconnew{cursor: pointer;}
</style>
<!-- Include xlsx library -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.17.5/xlsx.full.min.js"></script>

<!-- Include file-saver library -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/2.0.5/FileSaver.min.js"></script>

<div class="row m-0">   
    <div class="col-sm-12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form class="form-horizontal" method="post">
                   <ul>
                   <li>
                    <i class="icon-th-list"></i>
                    <h3>Maid Hours Report</h3> 
                    
                    </li>
                    
                    <li>                  
                   
                    <input type="text" readonly="readonly" style="width: 160px;" id="vehicle_date" name="from_date" value="<?php echo $activity_date ?>">  
                    
                    </li>
                    
                    <li class="mr-2"> 
                                         
                    <input type="text" readonly="readonly" style=" width: 160px;" id="schedule_date" name="to_date" value="<?php echo $activity_date_to ?>">   
                    </li>
                    
                    <li>                     
                    <input type="submit" class="btn" value="Go" name="vehicle_report">
                    </li>
                    
                    <li class="mr-0 float-right">
                    
                
                
                    
                    <div class="topiconnew border-0 green-btn">
                       <a onclick="exportF(this)" title="Download to Excel"> <i class="fa fa-download"></i></a>
                    </div> 
                  
                  
                    </li>
                    </ul>              
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px;"> Sl No.</th>
                            <th style="line-height: 18px;"> Maid</th>
                            <th style="line-height: 18px;"> Hours</th> 
                            <th style="line-height: 18px;"> Revenue</th> 
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($maid_hours_report))
                        {
                            $i = 0;
                            $total_hrs=0;
                            foreach ($maid_hours_report as $report)
                            {
                                $maid_time=explode(":",$report->hours);
                                $maid_time=$maid_time[0]+($maid_time[1]/60);
                                $total_hrs+=$maid_time;
                                echo '<tr>'
                                        . '<td style="line-height: 18px;">' . ++$i . '</td>'
                                        . '<td style="line-height: 18px;">' . $report->maid_name . '</td>'
                                        . '<td style="line-height: 18px;">' . $maid_time . '</td>'
                                        . '<td style="line-height: 18px;">' . $report->revenue . '</td>'
                                    .'</tr>';
                            }
                            //echo '<tr>'. '<td style="line-height: 18px;"></td>'. '<td style="line-height: 18px;font-weight:bold;">Total</td>'. '<td style="line-height: 18px;">' . $total_hrs . '</td>'.'</tr>';
                        }
                        else
                        {
                            //echo '<tr><td colspan="5">No Results!</td></tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>


<div id="divToPrint" style="display: none">
    <div class="widget-content" style="margin-bottom:30px">
        <table border="1" width="100%" cellspacing="0" cellpadding = "10" style="font-size:11px; border-color: #ccc;" class="ptable">
                    <thead>
                        <tr>
                            <th style="line-height: 18px;"> Sl No.</th>
                            <th style="line-height: 18px;"> Maid</th>
                            <th style="line-height: 18px;"> Hours</th> 
                            <th style="line-height: 18px;"> Revenue</th> 
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($maid_hours_report))
                        {
                            $i = 0;
                            $total_hrs=0;
                            foreach ($maid_hours_report as $report)
                            {
                                $maid_time=explode(":",$report->hours);
                                $maid_time=$maid_time[0]+($maid_time[1]/60);
                                $total_hrs+=$maid_time;
                                echo '<tr>'
                                        . '<td style="line-height: 18px;">' . ++$i . '</td>'
                                        . '<td style="line-height: 18px;">' . $report->maid_name . '</td>'
                                        . '<td style="line-height: 18px;">' . $maid_time . '</td>'
										. '<td style="line-height: 18px;">' . $report->revenue . '</td>'
                                    .'</tr>';
                            }
                            //echo '<tr>'. '<td style="line-height: 18px;"></td>'. '<td style="line-height: 18px;font-weight:bold;">Total</td>'. '<td style="line-height: 18px;">' . $total_hrs . '</td>'.'</tr>';
                        }
                        else
                        {
                            //echo '<tr><td colspan="5">No Results!</td></tr>';
                        }
                        ?>
                    </tbody>            
            </table>
    </div>
</div>

<script type="text/javascript">


function formatDate(dateString) {
      if (dateString && typeof dateString === 'string') {
        var parts = dateString.split('/');
        if (parts.length === 3) {
          var formattedDate = parts[0].padStart(2, '0') + '-' + parts[1].padStart(2, '0') + '-' + parts[2];
          return formattedDate;
        }
      }
      return dateString;
    }

  function exportF(elem) 
  {
    var startDate = $("#vehicle_date").val();
      var endDate = $("#schedule_date").val();

      var formattedStartDate = formatDate(startDate);
      var formattedEndDate = formatDate(endDate);

      var fileName = "Staff_Hours_Report";
      fileName += "_" + formattedStartDate + "_" + formattedEndDate;
      var fileType = "xlsx";
      var table = document.getElementById("divToPrint");
      var wb = XLSX.utils.table_to_book(table, {
        sheet: "Report",
        dateNF: 'dd/mm/yyyy;@',
        cellDates: true,
        raw: true
      });
      const ws = wb.Sheets['Report'];
      var wscols = [{
          wch: 15
        },
        {
          wch: 25
        },
        {
          wch: 30
        },
        {
          wch: 25
        },
        {
          wch: 20
        },
      ];
      ws['!cols'] = wscols;
      return XLSX.writeFile(wb, null || fileName + "." + (fileType || "xlsx"));
  }  

</script>
