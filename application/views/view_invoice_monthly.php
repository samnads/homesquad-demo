<script src="<?php echo base_url('js/jquery-1.7.2.min.js'); ?>"></script>
<div id="cancel-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Cancel Invoice</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure want to cancel this invoice ?</h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_cancel_invoice()">Yes</button>
      <button type="button" class="n-btn mb-0" onclick="closeFancy()">No</button>
    </div>
  </div>
</div>
<div id="validate-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Validate Invoice</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure want to validate this invoice ?</h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0" onclick="confirm_validate_invoice()">Yes</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">No</button>
    </div>
  </div>
</div>
<section>
    <div class="row invoice-field-wrapper no-left-right-margin">
        <div class="widget-header">
            <i class="icon-th-list"></i>
            <h3>View Invoice</h3>
			<a href="<?php echo base_url(); ?>invoice/list_customer_invoices" class="btn" style="float: right; margin: 5px 15px 5px 0px;">Back</a>
			<a class="btn" style="margin: 5px 15px 5px 5px; float:right;" href="<?php echo base_url(); ?>invoice/generateinvoice_monthly/<?php echo $invoice_detail[0]->invoice_id; ?>" target="_blank" title="Download Invoice">
				<i class="btn-icon-only fa fa-download "> </i>
			</a>
			<?php
			if($invoice_detail[0]->invoice_status == 0)
			{
			?>
			<!--<a class="btn" id="inv-edit" style="margin: 5px 10px 5px 5px; float:right;">Edit</a>-->
			<div style="margin: 5px 10px 5px 5px; float:right;" class="btn" id="inv-validate" data-custid="<?php echo $invoice_detail[0]->customer_id; ?>" data-invamt="<?php echo $invoice_detail[0]->invoice_net_amount; ?>" data-invid="<?php echo $invoice_detail[0]->invoice_id; ?>">Validate</div>
			<?php
			}
			?>
			<?php
			// if($invoice_detail[0]->invoice_status == 0 || $invoice_detail[0]->invoice_status == 1 && $invoice_detail[0]->invoice_paid_status == 0)
			if($invoice_detail[0]->invoice_status == 0)
			{
			?>
			<div style="margin: 5px 10px 5px 5px; float:right;" class="btn" id="inv-cancel" data-custid="<?php echo $invoice_detail[0]->customer_id; ?>" data-invamt="<?php echo $invoice_detail[0]->invoice_net_amount; ?>" data-invid="<?php echo $invoice_detail[0]->invoice_id; ?>" data-invstat="<?php echo $invoice_detail[0]->invoice_status; ?>">Cancel Invoice</div>
			<?php
			}
			?>
			<!--<div style="margin: 5px 10px 5px 5px; float:right;" class="btn send_inv_mail"   data-invoiceid="<?php echo $invoice_detail[0]->invoice_id; ?>"  data-invoiceemail="<?php echo $invoice_detail[0]->email_address; ?>"><span class="btntxt">Send Invoice Mail</span></div>-->
        </div>
        <div class="col-md-12 col-sm-12 invoice-box-main  box-center no-left-right-padding">
            
            <!--<h2 class="text-center">Invoice Details</h2>-->
            <div class="col-md-12 col-sm-12 invoice-box-right-main no-left-padding">
                <div class="col-md-12 col-sm-12 invoice-box-right">
                    <div class="col-md-9 col-sm-9 invoice-logo-box">
                        <div class="invoice-logo">
                            <div class="invoice-logo"><img src="<?= check_and_get_img_url('./uploads/images/settings/'.$settings->invoice_logo, 'invoice-logo.png'); ?>"></div>
                        </div>
                    </div>
					<div class="col-md-3 col-sm-3 invoice-logo-box">
                        <div class="invoice-logo-text" style="padding-top: 12px; font-weight: bold; font-size:20px; color: #3a2c70;">
							<?php
							if($invoice_detail[0]->invoice_paid_status == 2)
							{
								$paystat = " (Partially Paid)";
							} else {
								$paystat = "";
							}
							if($invoice_detail[0]->invoice_status == '0')
							{
								$invstatus = "Draft";
							} else if($invoice_detail[0]->invoice_status == '1')
							{
								$invstatus = "Open";
							} else  if($invoice_detail[0]->invoice_status == '2'){
								$invstatus = "Cancelled";
							} else  if($invoice_detail[0]->invoice_status == '3'){
								$invstatus = "Paid";
							}
							?>
							Status : <span id="invoice-stat-text"><?php echo $invstatus.$paystat; ?></span>
						</div>
                    </div>
                    <!--<form name="invoiceaddform" action="<?php// echo base_url(); ?>invoice/add_invoice" id="invoiceaddform" method="POST">-->
                        <!--<input type="hidden" value="<?php// echo $jobdetail[$bookings_id]->customer_id; ?>" name="jobhiddencustomerid" id="jobhiddencustomerid" />    -->
                        <div class="col-md-12 col-sm-12 invoice-address-box">
                            <div class="col-md-6 col-sm-12 invoice-address no-left-right-padding">
                                <p><strong><?= $settings->company_name ?></strong><br>
                                    <?= $settings->company_address_lines_html ?>
                                    <br><br>
                                    Tel &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;    : &nbsp; <strong> <?= $settings->company_tel_number ?></strong><br>
                                    Email &nbsp;&nbsp;&nbsp;&nbsp; : &nbsp; <strong><?= $this->config->item('mail_admin') ?></strong>
                                </p>
                            </div>
                            <div class="col-md-6 col-sm-12 invoice-date no-left-right-padding">
                                <div class="col-md-6 col-sm-12 no-left-padding">  
                                    <div class="text-field-main no-bottom-padding">
                                        <p>Issue date :</p>
                                        <!--<input name="invoiceissuedate" id="invoiceissuedate" class="text-field" type="text">-->
                                        <p class="issue_date"><?php echo $invoice_detail[0]->invoice_date; ?> 
                                        <?php if($invoice_detail[0]->invoice_status == '0'){ ?>
                                        <button class="btn btn-small btn-info issuedt_edtbtn" ><i class="btn-icon-only fa fa-edit"> </i></button>
                                    	<?php } ?>
                                    	</p>
                                        <p class="issue_date_edit" style="display: none;">
                                            <input type="text" id="invoice_date" value="<?php echo date("d/m/Y",strtotime($invoice_detail[0]->invoice_date));?>" style="width: 92px;">
                                            <button class="btn btn-small btn-info upd_new_iss_dt" data-invoiceid="<?php echo $invoice_detail[0]->invoice_id; ?>">Update</button>
                                            <i class="fa fa-spinner fa-spin iss_dt_loader" style="font-size:20px;display: none;position: relative;top: 3px;"></i>
                                        </p>
                                    </div><!--text-field-main end-->
                                </div>
                                <div class="col-md-6 col-sm-12 no-right-padding">  
                                    <div class="text-field-main no-bottom-padding">
                                        <p>Due date :</p>
<!--                                        <input name="invoiceduedate" id="invoiceduedate" class="text-field" type="text">-->
                                        <p class="due_date"><?php echo $invoice_detail[0]->invoice_due_date; ?> 
                                        <?php if($invoice_detail[0]->invoice_status == '0'){ ?>
	                                        <button class="btn btn-small btn-info duedt_edtbtn" ><i class="btn-icon-only fa fa-edit"> </i></button>
	                                    <?php } ?>
	                                    </p>
                                        <p class="due_date_edit" style="display: none;">
                                            <input type="text" id="search_date_to" value="<?php echo date("d/m/Y",strtotime($invoice_detail[0]->invoice_due_date));?>" style="width: 92px;">
                                            <button class="btn btn-small btn-info upd_new_due_dt" data-invoiceid="<?php echo $invoice_detail[0]->invoice_id; ?>">Update</button>
                                            <i class="fa fa-spinner fa-spin due_dt_loader" style="font-size:20px;display: none;position: relative;top: 3px;"></i>
                                        </p>
                                    </div><!--text-field-main end-->
                                </div>
                                <!--<div class="col-md-12 col-sm-12 no-right-padding">  
                                    <div class="text-field-main">
                                        <p class="text-right">Amount Due : <strong>$0.00</strong></p>
                                    </div><!--text-field-main end-->
                                <!--</div>-->
                            </div>
                            <div class="col-md-12 col-sm-12 invoice-to-address no-left-right-padding">
                                <p><strong>To,</strong><br>
                                <span><strong><?php echo $invoice_detail[0]->customer_name; ?></strong><br>
                                <?php echo $invoice_detail[0]->bill_address; ?><br>
                                </span></p>
                            </div>
                            <div class="col-md-12 col-sm-12 invoice-job-det no-left-right-padding">
                                <p><strong>Job</strong><br>
                                <?php
                                $nameOfDay = date('l', strtotime($invoice_detail[0]->service_date));
                                $day = date('d', strtotime($invoice_detail[0]->service_date));
                                $month = date('F', strtotime($invoice_detail[0]->service_date));
                                $year = date('Y', strtotime($invoice_detail[0]->service_date));
                                ?>
                                <span>Work carried out at: <?php echo $invoice_detail[0]->bill_address; ?>. Month:  <?php echo $month; ?> </span></p>
                            </div>
                            <div class="col-md-12 col-sm-12 invoice-det-table no-left-right-padding">
                                <div class="Table table-top-style-box no-top-border">
                                    <div class="Heading table-head">
										<div class="Cell">
                                            <p><strong>Sl.No</strong></p>
                                        </div>
										<div class="Cell">
                                            <p><strong>Date</strong></p>
                                        </div>
										<div class="Cell">
                                            <p><strong>Product/Service</strong></p>
                                        </div>
										<div class="Cell">
                                            <p><strong>From</strong></p>
                                        </div>
										<div class="Cell">
                                            <p><strong>To</strong></p>
                                        </div>
                                        <div class="Cell">
                                            <p><strong>Description</strong></p>
                                        </div>
                                        <div class="Cell">
                                            <p><strong>Qty</strong></p>
                                        </div>
                                        <div class="Cell">
                                            <p><strong>Amount</strong></p>
                                        </div>
                                        <div class="Cell">
                                            <p><strong>VAT <?= number_format($settings->service_vat_percentage,2); ?> %</strong></p>
                                        </div>
                                        <div class="Cell">
                                            <p><strong>Net Amount</strong></p>
                                        </div>
                                    </div>
                                    <?php
									$i = 1;
                                    foreach ($invoice_detail as $jobs)
                                    {
										$tot_hrs = $jobs->service_hrs;
                                        
                                    ?>
                                    <div class="Row">
										<div class="Cell">
                                            <p><?php echo $i; ?></p>
                                        </div>
										<div class="Cell">
                                            <p><?php echo date('d/m/Y', strtotime($jobs->service_date)); ?></p>
                                        </div>
										<div class="Cell">
                                            <p><?php echo $jobs->monthly_product_service; ?></p>
                                        </div>
										<div class="Cell">
                                            <p><?php echo date('H:i A', strtotime($jobs->service_from_time)); ?></p>
                                        </div>
										<div class="Cell">
                                            <p><?php echo date('H:i A', strtotime($jobs->service_to_time)); ?></p>
                                        </div>
                                        <div class="Cell">
                                            <p class="jobdes<?php echo $jobs->invoice_line_id;?>"><span class="jobdestext<?php echo $jobs->invoice_line_id;?>"><?php echo $jobs->description; ?></span>
                                            <button class="btn btn-small btn-info text-right descedit" style="margin-top:10px;float: right;"data-lineid="<?php echo $jobs->invoice_line_id;?>"><i class="btn-icon-only fa fa-edit"> </i></button>
                                            </p>
                                            <p class="jobtxa<?php echo $jobs->invoice_line_id;?> jobtxa" style="display: none;">
                                                <textarea rows="4" style="width:100%;" class="newdesc_<?php echo $jobs->invoice_line_id;?>"><?php echo $jobs->description; ?></textarea>

                                                <br/>
                                                <button class="btn btn-small btn-info descupdatebtn" style="margin-top:10px;" data-lineid="<?php echo $jobs->invoice_line_id;?>">Update</button>
                                                <i class="fa fa-spinner fa-spin descload<?php echo $jobs->invoice_line_id;?>" style="font-size:20px;position: relative;top: 7px;display: none;"></i>
                                            </p>
                                        </div>
										<div class="Cell">
                                            <p><?php echo $tot_hrs; ?></p>
                                        </div>
										<div class="Cell">
                                            <p class="lineamt<?php echo $jobs->invoice_line_id;?>">
                                            <span class="lineamttext<?php echo $jobs->invoice_line_id;?>"><?php echo $jobs->line_amount; $x='p';?></span>
                                            <?php if($invoice_detail[0]->invoice_status == '0' || ($invoice_detail[0]->invoice_status == '1'&&$invoice_detail[0]->invoice_paid_status!=2)){ ?>
                                            <!--<button class="btn btn-small btn-info text-right amtedit " style="margin-top:10px;float: right;" data-lineid="<?php// echo $jobs->invoice_line_id;?>" ><i class="btn-icon-only fa fa-edit"> </i></button>-->      
                                            <?php } ?>           
                                            </p>
                                            <p class="amttxt<?php echo $jobs->invoice_line_id;?> amttxt" style="display: none;">
                                                <input type="text" class="amtinp_<?php echo $jobs->invoice_line_id;?>" value="<?php echo $jobs->line_amount; ?>" style="width:87px;">
                                                <br/>
                                                <button class="btn btn-small btn-info amtupdatebtn" style="margin-top:10px;" data-lineid="<?php echo $jobs->invoice_line_id;?>" data-invstat="<?php echo $invoice_detail[0]->invoice_status?>">Update</button>
                                                <i class="fa fa-spinner fa-spin amtload<?php echo $jobs->invoice_line_id;?>" style="font-size:20px;position: relative;top: 7px;display: none;"></i>
                                            </p>
                                        </div>
										<div class="Cell">
                                            <p>
                                            	<span class="linevatamttext<?php echo $jobs->invoice_line_id;?>">
                                            		<?php echo $jobs->line_vat_amount; ?> 
                                            	</span>
                                            </p>
                                        </div>
										<div class="Cell">
                                            <p>
                                               <span class="linenetamttext<?php echo $jobs->invoice_line_id;?>">
                                            	<?php echo $jobs->line_net_amount; ?>
                                               </span>
                                            </p>
                                        </div>
                                    </div>
                                    <?php
									$i++; } ?>
                                    <div class="Row">
                                        <div class="Cell total-box no-right-border">
                                          &nbsp;
                                        </div>
										<div class="Cell total-box no-right-border">
                                          &nbsp;
                                        </div>
										<div class="Cell total-box no-right-border">
                                          &nbsp;
                                        </div>
										<div class="Cell total-box no-right-border">
                                          &nbsp;
                                        </div>
										<div class="Cell total-box no-right-border">
                                          &nbsp;
                                        </div>
										<div class="Cell total-box no-right-border">
                                          &nbsp;
                                        </div>
                                        <div class="Cell total-box no-left-border">
                                          &nbsp;
                                          <span class="total-text text-right"><b class="pull-right">Total &nbsp;&nbsp; </b></span>
                                        </div>
                                        <div class="Cell total-box light-green">
                                           <span class="total-text text-right"><b class="invtotamt">AED <?php echo $invoice_detail[0]->invoice_total_amount; ?></b></span>
                                        </div>
                                        <div class="Cell total-box light-green">
                                            <span class="total-text text-right"><b class="invtaxamt">AED <?php echo $invoice_detail[0]->invoice_tax_amount; ?></b></span>
                                        </div>
                                        <div class="Cell total-box light-green">
                                            <span class="total-text text-right"><b class="invnetamt">AED <?php echo $invoice_detail[0]->invoice_net_amount; ?></b></span>
                                        </div>
                                    </div>
                                </div><!--Table table-top-style-box end--> 
                            </div>
                            <div class="col-md-12 col-sm-12 invoice-det-note no-left-right-padding">
                                <p><!--<strong>Note</strong>--><br></p>
                                <!--<span><?php// echo $notes; ?></span>-->
                            </div>
<!--                            <div class="col-md-12 col-sm-12 invoice-button-main no-left-right-padding">
                                <div class="col-md-6 col-sm-12 invoice-button">
                                   &nbsp;
                                </div>
                                <div class="col-md-3 col-sm-6 invoice-button no-left-padding">
                                    <input value="CANCEL" class="text-field-but dark" type="button">
                                </div>
                                <div class="col-md-3 col-sm-6 invoice-button no-right-padding">
                                    <input value="SAVE" name="jobinvoicesave" id="jobinvoicesave" class="text-field-but" type="submit">
                                </div>
                            </div>-->
                        </div>
                    </form>
                </div><!--invoice-box-right end-->
            </div><!--invoice-box-right-main end-->
        </div>
    </div>
</section>

<button type="button" class="btn btn-info btn-lg openemailmodal hidden" data-toggle="modal" data-target="#emailModal">Open Modal</button>

<!-- Modal -->
<div id="emailModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" style="margin-top: -6px;font-size:32px;">&times;</button>
        <h4 class="modal-title">&nbsp;</h4>
      </div>
      <div class="modal-body">
          <div id="emailpopmsg"></div>
          <div class="form-group">
            <label for="email_content">Email Content:</label>
            <textarea class="form-control" id="email_content"></textarea>
          </div>
          <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" class="form-control" id="email">
            <input type="hidden" class="form-control" id="hidinvid">
          </div>
          
          <button type="submit" class="btn btn-default emailsubbtn">Submit</button>
          <button type="submit" class="btn btn-default emailloadbtn" style="display:none;"><i class="fa fa-spinner fa-spin" ></i></button>
      </div>
      <!-- <div class="modal-footer text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
    </div>

  </div>
</div>

<script type="text/javascript">
$( document ).ready(function() {

    $(".descedit").click(function() {
        var lineid=$(this).data("lineid");
        $(".jobtxa").hide();
        $(".jobdes"+lineid).hide();        
        $(".jobtxa"+lineid).show();
    });  
    $(".descupdatebtn").click(function() {
        var lineid=$(this).data("lineid");
        $(this).prop('disabled', true);
        var description=$(".newdesc_"+lineid).val();   
                
        $(".descload"+lineid).show();
        $.ajax({
                   url: "<?php echo base_url();?>invoice/update_line_desc",
                   type: "post",
                   data: {line_description:description,lineid:lineid} ,
                   success: function (response) {
                        $(".descload"+lineid).hide();
                        $(".descupdatebtn[data-lineid='"+lineid+"']").prop('disabled', false);
                        $(".jobdestext"+lineid).html(description); 
                        $(".jobdes"+lineid).show();        
                        $(".jobtxa"+lineid).hide();

                   },
                   error: function(jqXHR, textStatus, errorThrown) {
                        $(".descload"+lineid).hide();
                        $(".descupdatebtn[data-lineid='"+lineid+"']").prop('disabled', false);
                        alert("Error!Try again later.");

                   }
               });
    });  

    $(".send_inv_mail").click(function() {
        var invoiceid=$(this).data('invoiceid');
        var emailid=$(this).data('invoiceemail');
        $("#email").val(emailid);$("#hidinvid").val(invoiceid);
        $("#emailpopmsg").html('');
        $("#emailpopmsg").html('');
        $(".openemailmodal").click();
    });


    $(".emailsubbtn").click(function() {
        $("#emailpopmsg").html('');
        var invoiceid=$("#hidinvid").val();
        var invoicecontent=$("#email_content").val();
        var invoiceemail=$("#email").val();
        //console.log(invoiceid);
        if(invoicecontent=='')
        {
            $("#emailpopmsg").html('<div class="alert alert-warning text-center"><strong>Please enter email content!</strong></div>');
            return false;
        }validateEmail

        if(!validateEmail(invoiceemail))
        {
            $("#emailpopmsg").html('<div class="alert alert-warning text-center"><strong>Please check the email entered!</strong></div>');
            return false;
        }



        $(this).hide();
        $(".emailloadbtn").show();
        $.ajax({
                   url: "<?php echo base_url();?>invoice/send_invoice_email_monthly",
                   type: "post",
                   data: {invoiceid:invoiceid,invoicecontent:invoicecontent,invoiceemail:invoiceemail} ,
                   success: function (response) {
                        $(".emailsubbtn").show();
                        $(".emailloadbtn").hide();
                        if(response=='success')
                        {
                            $("#emailpopmsg").html('<div class="alert alert-success text-center"><strong>Invoice email sent successfully!</strong></div>');
                            $("#email_content,#email").val('')
                            //$(".openmodal").click(); 
                        }
                        else if(response=='email_error')
                        {
                            $("#emailpopmsg").html('<div class="alert alert-warning text-center"><strong>Please check customer email!</strong></div>');
                            //$(".openmodal").click();
                        }
                        else
                        {
                            $("#emailpopmsg").html('<div class="alert alert-danger text-center"><strong>Error!Try again later</strong></div>');
                            //$(".openmodal").click();
                        }

                   },
                   error: function(jqXHR, textStatus, errorThrown) {
                        $(".emailsubbtn").show();
                        $(".emailloadbtn").hide();
                        $("#emailpopmsg").html('<div class="alert alert-danger text-center"><strong>Error!Try again later</strong></div>');
                        //$(".openmodal").click();

                   }
               });
    });



    $('body').on('click',".issuedt_edtbtn",function() {
        $(".issue_date").hide();
        $(".issue_date_edit").show();
    });



    $(".upd_new_iss_dt").click(function() {
        var invoice_id=$(this).data('invoiceid');
        var new_issue_date=$("#invoice_date").val();
        $(".iss_dt_loader").show();
        $(this).prop('disabled', true);
        $.ajax({
                   url: "<?php echo base_url();?>invoice/update_invoice_issue_date",
                   type: "post",
                   data: {invoice_id:invoice_id,new_issue_date:new_issue_date} ,
                   success: function (response) {
                        $(".iss_dt_loader").hide();
                        $(".upd_new_iss_dt").prop('disabled', false);
                        if(response=='success')
                        {
                            var date_parts = new_issue_date.split('/');
                            var newdate_ymd=date_parts[2] +'-'+ date_parts[1] +'-'+ date_parts[0];
                            $(".issue_date").html(newdate_ymd+' <button class="btn btn-small btn-info issuedt_edtbtn" ><i class="btn-icon-only fa fa-edit"> </i></button>');
                            $(".issue_date").show();
                            $(".issue_date_edit").hide();
                        }

                   },
                   error: function(jqXHR, textStatus, errorThrown) {
                        $(".iss_dt_loader").hide();
                        $(".upd_new_iss_dt").prop('disabled', false);                       

                   }
               });

    });


    $('body').on('click',".duedt_edtbtn",function() {
        $(".due_date").hide();
        $(".due_date_edit").show();
    });


    $(".upd_new_due_dt").click(function() {
        var invoice_id=$(this).data('invoiceid');
        var new_due_date=$("#search_date_to").val();
        $(".due_dt_loader").show();
        $(this).prop('disabled', true);
        $.ajax({
                   url: "<?php echo base_url();?>invoice/update_invoice_due_date",
                   type: "post",
                   data: {invoice_id:invoice_id,new_due_date:new_due_date} ,
                   success: function (response) {
                        $(".due_dt_loader").hide();
                        $(".upd_new_due_dt").prop('disabled', false);
                        if(response=='success')
                        {
                            var date_parts = new_due_date.split('/');
                            var newdate_ymd=date_parts[2] +'-'+ date_parts[1] +'-'+ date_parts[0];
                            $(".due_date").html(newdate_ymd+' <button class="btn btn-small btn-info duedt_edtbtn" ><i class="btn-icon-only fa fa-edit"> </i></button>');
                            $(".due_date").show();
                            $(".due_date_edit").hide();
                        }

                   },
                   error: function(jqXHR, textStatus, errorThrown) {
                        $(".due_dt_loader").hide();
                        $(".upd_new_due_dt").prop('disabled', false);                       

                   }
               });

    });


    $('body').on('click',".amtedit",function() {
    //$(".descedit").click(function() {
        var lineid=$(this).data("lineid");
        //$(".jobtxa").hide();
        $(".lineamt"+lineid).hide();        
        $(".amttxt"+lineid).show();
    });  

    $(".amtupdatebtn").click(function() {
        var lineid=$(this).data("lineid");
        var custid='<?php echo $invoice_detail[0]->customer_id; ?>';
        var invstat=$(this).data("invstat");
        var updateamt=$(".amtinp_"+lineid).val();
        var line_old_amount=$(".lineamttext"+lineid).html();
        var line_old_net_amount=$.trim($(".linenetamttext"+lineid).html());
        var invid='<?php echo $invoice_detail[0]->invoice_id; ?>';
        if(updateamt.length==0 || isNaN(updateamt)){alert("Please check the entered amount");return false;}
        $(".amtload"+lineid).show();
        $(this).prop('disabled', true);

        $.ajax({
                   url: "<?php echo base_url();?>invoice/update_line_amt",
                   type: "post",
                   dataType: "json",
                   data: {line_amount:updateamt,invoice_id:invid,lineid:lineid,line_old_amount:line_old_amount,custid:custid,line_old_net_amount:line_old_net_amount,invstat:invstat} ,
                   success: function (response) {
                        $(".amtload"+lineid).hide();
                        $(".amtupdatebtn[data-lineid='"+lineid+"']").prop('disabled', false);
                        $(".lineamttext"+lineid).html(updateamt); 
                        $(".lineamt"+lineid).show();        
                        $(".amttxt"+lineid).hide();
                        $(".linevatamttext"+lineid).html(response.line_vat_amount);
                        $(".linenetamttext"+lineid).html(response.line_net_amount);

                        $(".invtotamt").html('AED '+response.invoice_total_amount);
                        $(".invtaxamt").html('AED '+response.invoice_tax_amount);
                        $(".invnetamt").html('AED '+response.invoice_net_amount);

                   },
                   error: function(jqXHR, textStatus, errorThrown) {
                        $(".amtload"+lineid).hide();
                        $(".amtupdatebtn[data-lineid='"+lineid+"']").prop('disabled', false);
                        //alert("Error!Try again later.");

                   }
               });
        
    });

});

function validateEmail(email) 
{
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}
var invoice_id = "";
var invoice_amt = "";
var cust_id = "";
var inv_status = "";
$(document).on('click', '#inv-cancel', function () {
    invoice_id = $(this).attr('data-invid');
	invoice_amt = $(this).attr('data-invamt');
	cust_id = $(this).attr('data-custid');
	inv_status = $(this).attr('data-invstat');
    $.fancybox.open({
		autoCenter: true,
		fitToView: false,
		scrolling: false,
		openEffect: 'none',
		openSpeed: 1,
		autoSize: false,
		width: 450,
		height: 'auto',
		helpers: {
			overlay: {
				css: {
					'background': 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding: 0,
		closeBtn: false,
		content: $('#cancel-popup'),
	});
});
function confirm_cancel_invoice() {
	$('.mm-loader').css('display', 'block');
        $.ajax({
            type: "POST",
            url: _base_url + "invoice/cancel_invoice_new",
            data: { invoice_id: invoice_id, invoice_amt: invoice_amt, cust_id: cust_id, inv_status: inv_status },
            dataType: "text",
            cache: false,
            success: function (result) {
                closeFancy();
                if (result == 'success') {
                    $('.mm-loader').css('display', 'none');
                    toastr.success("Invoice Cancelled Successfully.");
                    $("#inv-validate,#inv-edit,#inv-cancel").hide();
                    $("#invoice-stat-text").text('Cancelled');
                } else {
                    $('.mm-loader').css('display', 'none');
                    toastr.error("Something went wrong.");;
                }
            }
        });
}
$(document).on('click', '#inv-validate', function () {
    invoice_id = $(this).attr('data-invid');
	invoice_amt = $(this).attr('data-invamt');
	cust_id = $(this).attr('data-custid');
    $.fancybox.open({
		autoCenter: true,
		fitToView: false,
		scrolling: false,
		openEffect: 'none',
		openSpeed: 1,
		autoSize: false,
		width: 450,
		height: 'auto',
		helpers: {
			overlay: {
				css: {
					'background': 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding: 0,
		closeBtn: false,
		content: $('#validate-popup'),
	});
});
function confirm_validate_invoice() {
	$('.mm-loader').css('display', 'block');
        $.ajax({
            type: "POST",
            url: _base_url + "invoice/validate_invoice",
            data: { invoice_id: invoice_id, invoice_amt: invoice_amt, cust_id: cust_id },
            dataType: "text",
            cache: false,
            success: function (result) {
                closeFancy();
                if (result == 'success') {
                    $('.mm-loader').css('display', 'none');
                    toastr.success("Invoice Validated Successfully.");
                    $("#inv-validate,#inv-edit").hide();
                    $("#invoice-stat-text").text('Open');
                } else {
                    $('.mm-loader').css('display', 'none');
                    toastr.error("Something went wrong.");;
                }
            }
        });
}
function closeFancy(){
  $.fancybox.close();
}
</script>