<style>
/*    .save-but{
        background: #4897e6 !important;
    }*/
.select2-arrow{visibility : hidden;}
	.select2-container .select2-choice{
	-moz-appearance: none;
    background: #fff url("http://demo.azinova.info/php/mymaid/css/../images/ash-arrow.png") no-repeat scroll right 7px top 11px;
   border: 1px solid #ccc;
    border-radius: 3px;
    cursor: pointer;
    font-size: 14px;
    height: 30px;
    line-height: 24px;
   padding: 3px 0 3px 10px;

    text-indent: 0.01px;
    /*width:200px;*/
	}
 .leave-but { color: #FFF; text-transform: uppercase; font-size:14px; float: left; text-decoration: none; border: 0px; cursor: pointer; padding: 4px 15px; background-color:#0756a5; border-radius: 4px; font-weight: bold; font-family:Arial, Helvetica, sans-serif; margin-right: 20px;}     
.popup-main-box .booking_form .row .cell1{
            width: 160px !important;
        }

#schedule-top .select2 {
  width: 210px !important;
}

.row.maid-schedule-top {
  margin: 0 0 0 10px !important;
}

</style>
<div id="alert-popup" style="background:none;display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="alert-title"></span>
    </div>
    <div class="modal-body">
      <h3 id="alert-message"></h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0 pop_close">OK</button>
    </div>
  </div>
</div>

<div class="content-wrapper">
	
	<div id="schedule-top" class="pt-0 pb-2">	
            <form method="post" action="<?php echo base_url() . 'maid/schedule'?>">
		<div class="row maid-schedule-top" style="margin: 0 0 0 10px !important;">
                    
                       <div class="book-nav-top">

                           <ul>
                           <li><div class="mm-color-box purple">One Day</div>
                    <div class="mm-color-box green">Weekly</div>
                    <div class="mm-color-box orange">Biweekly</div></li>
                               <li>
                                   <select name="maid_id" id="search-maid-id" class="sel2" style="width:250px !important;">
                                <option value="0">Select Maid</option>
                                    <?php
                                    foreach($maids as $maid)
                                    {       
                                            $selected = $maid_id == $maid->maid_id ? 'selected="selected"' : '';
                                            echo '<option value="' . $maid->maid_id . '" ' . $selected . '>' . html_escape($maid->maid_name) . '</option>';
                                    }
                                    ?>
                            </select>
                               </li>
                                <li>
				<input type="text" name="start_date" id="start-date" class="n-calendar-icon" value="<?php echo $start_date ?>">
                                </li>
                                <li>
                                <input type="text" name="end_date" id="end-date" class="n-calendar-icon" value="<?php echo $end_date ?>">
                                    
                                </li>
                                <li>
                                    
                                <input type="submit" class="n-btn mb-0" name="search_maid_schedule" style="float: none;" value="Search" />	
                                </li>
                                <li class="no-right-margin">
                                    
                                <input type="button" class="n-btn purple-btn mb-0" name="mark_maid_leave" id="mark_maid_leave" style="float: none;display:none; " value="Mark Leave" />
                                
                                </li>
                                <div class="clear"></div>
                                    
                           </ul>
</div>                    
<!--			<div class="cell1">
				
			</div>
                    <div class="cell2" style="width: 50%;">
                             </div>
			<div class="cell3">
                            			
			</div>-->
                    
                    
                    
		</div>
            </form>
	</div>
	
<style>
.row.maid-schedule-top { margin: 0 auto !important; display: table !important;}
.n-time-section { width: 100%; position:relative;}
.n-grid-section {width: 100%;}
#schedule .time_grid .grids .row .slot { height: 70px;}
.slot { background: #fafafa !important;}

#schedule { margin: 0 auto !important;}

</style>
    
    
    
	<div id="schedule-wrapper"><!-- style="min-height: <?php //echo ((count($maids) * 80) + 70); ?>px;" -->
		<div id="schedule">
             
            <div class="n-time-section">
                <div id="tb-slide-left" class="time-nav previous-time" title="Previous Time">&nbsp;</div>
                
			    <div class="head">Maid Name</div>
                <div class="time_line">	
                    <div class="time_slider">
                        <div style="width:3408px;">
                            <?php
                            foreach($times as $time_index=>$time)
                            {
                                echo '<div><span>' . $time->display .' </span></div>';
                            }
                            ?>
                        </div>
                    </div>
                </div>
                
                <div id="tb-slide-right" class="time-nav next-time" title="Next Time">&nbsp;</div>
                <div class="next-time-bg">&nbsp;</div>
        
                <div class="clear"></div>
			</div>
            
            
            
			<div class="n-grid-section">

                <div class="maids">
                    <?php
                    foreach($maid_schedule as $maid)
                    {
                        $nameOfDay = date('l', strtotime($maid->date));
                        ?>
                                <div class="maid"><?php echo html_escape(date("d/m/Y", strtotime($maid->date))); ?> (<?php echo $nameOfDay; ?>)</div>
                        <?php
                    }
                    ?>
                </div><!--Maid section-->

                <div class="time_grid" style="height: <?php echo (count($maid_schedule) * 71.1); ?>px;">	
                <!--<div class="time_grid" style="height: <?php// echo (count($maid_schedule) * 79) + count($maid_schedule) + 1; ?>px;">-->	
                    <div class="grids"><div id="schedule-grid-rows"><?php echo $schedule_grid; ?></div></div>
                </div>
                
			    <div class="clear"></div>
            </div>
            
		</div>
	</div>
</div>


<input type="hidden" id="all-maids" value='<?php echo $all_maids; ?>' />
<input type="hidden" id="time-slots" value='<?php echo json_encode($times); ?>' />
<input type="hidden" id="repeate-end-start" value="<?php echo html_escape($repeate_end_start_c); ?>" />

<div id="booking-popup" style="display:none;">
	<div class="popup-main-box">
		<div class="green-popup-head"><span id="b-maid-name"></span> <span id="b-time-slot"></span> <span class="pop_close"><img src="<?php echo base_url() ?>images/pup_close.png" alt="" /></span> </div>
		<div class="white-content-box">
			<div id="b-error"></div>
			<div class="booking_form" id="customer-info">
				<div class="row">
					<div class="cell1"><span></span> Customer</div>
					<div class="cell2">:</div>
					<div class="cell3" id="b-customer-id-cell">
                                            <select name="customer_id" id="b-customer-id" data-placeholder="Select customer" class="" style="width:320px !important;">
							<option></option>
							<?php
							// foreach($customers as $customer)
							// {
                                                                // $p_number = array();
                                                                // if($customer->phone_number != NULL)
                                                                    // array_push ($p_number, $customer->phone_number);
                                                                // if($customer->mobile_number_1 != NULL)
                                                                    // array_push ($p_number, $customer->mobile_number_1);
                                                                // if($customer->mobile_number_2 != NULL)
                                                                    // array_push ($p_number, $customer->mobile_number_2);
                                                                // if($customer->mobile_number_3 != NULL)
                                                                    // array_push ($p_number, $customer->mobile_number_3);
                                                                
                                                                // $phone_number = !empty($p_number) ? ' - ' . join(', ', $p_number) : '';
                                                                
								// echo '<option value="' . $customer->customer_id . '">' . html_escape($customer->customer_nick_name) . $phone_number . '</option>';
							// }
							?>
						</select>
						<div id="customer-picked-address"></div>
					</div>				
				</div>
			</div>
			<div id="customer-address-panel">
				<div class="head">Pick One Address <span class="close">Close</span></div>
				
				<div class="inner">
					Loading<span class="dots_loader"></span>
				</div>			
			</div>
                        <!-- Edited by Geethu -->
                       <div id="maids-panel"> <!--style=" min-height:  338px; box-shadow: 0px 0px 0px;" -->
				<div class="head">Pick Maids <span class="close">Close</span></div>
				<div class="controls">
                                    <label class="radio inline">
                                        
                                    </label>
                                    <label class="radio inline">
                                        <input type="radio" name="same_zone" value="0" checked="checked"> All
                                    </label>
                                    <label class="radio inline">
                                        <input type="radio" name="same_zone" value="1"> Same Zone
                                    </label>
                                    
                                    
                                </div>
				<div class="inner"> <!-- height:260px; overflow: scroll; overflow-x:hidden; padding-top:  0px;  -->
					Loading<span class="dots_loader"></span>
				</div>			
			</div>
                        <!-- End Edited by Geethu -->
			<div class="booking_form">
				<div class="row">
					<div class="cell1"><span class="icon_stype"></span> Service Type</div>
					<div class="cell2">:</div>
					<div class="cell3">
						<select name="service_type_id" id="b-service-type-id" data-placeholder="Select service type" class="sel2">
							<option></option>
							<?php
							foreach($service_types as $service_type)
							{
                                                                $selected = $service_type->service_type_id == 1 ? 'selected="selected"' : '';
								echo '<option value="' . $service_type->service_type_id . '" ' . $selected . '>' . html_escape($service_type->service_type_name) . '</option>';
							}
							?>
						</select>
					</div>				
				</div>
                                <div class="row">
                                    <div class="cell1"><!--<span class="icon_clean"></span>--><i class="fa fa-bitbucket"></i> Cleaning Materials</div>
                                    <div class="cell2">:</div>
                                    <div class="cell3">
                                        <label>  
                                            <!--<input type="checkbox" name="vaccum_cleaning" id="b-vaccum-cleaning" value="1">Vaccum Cleaning <br/>-->
                                            <input type="checkbox" name="cleaning_materials" id="b-cleaning-materials" value="Y"> Yes
                                            <!--<input type="checkbox" name="cleaning_chemicals" id="b-cleaning-chemicals" value="1">Cleaning Chemicals-->
                                        </label>
                                    </div>
                                </div>
                                <!--<div class="row">

                                    <div class="cell1"><span class="icon_clean"></span> Cleaning Type</div>
                                    <div class="cell2">:</div>
                                    <div class="cell3">
                                        <label>   <input type="checkbox" name="vaccum_cleaning" id="b-vaccum-cleaning" value="1">Vaccum Cleaning <br/>
                                            <input type="checkbox" name="cleaning_materials" id="b-cleaning-materials" value="1">Cleaning Materials <br/>
                                            <input type="checkbox" name="cleaning_chemicals" id="b-cleaning-chemicals" value="1">Cleaning Chemicals
                                        </label>
                                    </div>
                                </div>-->
				<div class="row">
					<div class="cell1"><span class="icon_time"></span> Time</div>
					<div class="cell2">:</div>
					<div class="cell3">
						From: <select name="time_from" id="b-from-time" data-placeholder="Select" class="sel2 small mr15">
								<option></option>
								<?php
								foreach($times as $time_index=>$time)
								{
									echo '<option value="' . $time->stamp . '">' . $time->display . '</option>';
								}
								?>
							</select>
						To: <select name="time_to" id="b-to-time" data-placeholder="Select" class="sel2 small">
								<option></option>
								<?php
								foreach($times as $time_index=>$time)
								{
									if($time_index == 't-0')
									{                                                                                
										continue;
									}
									
									echo '<option value="' . $time->stamp . '">' . $time->display . '</option>';
								}
								?>
							</select>
                                                        
					</div>				
				</div>
				<div class="row">
					<div class="cell1"><span class="icon_btype"></span> Repeats</div>
					<div class="cell2">:</div>
					<div class="cell3">
						<select name="booking_type" id="b-booking-type" data-placeholder="Select repeat type" class="sel2">
							<option></option>
							<option value="OD">Never Repeat - One Day Only</option>
							<option value="WE">Every Week</option>
                                                        <option value="BW">Every 2 Week</option>
							<!--
							<option value="BW">Every Other Week</option>
							-->
						</select>	
					</div>				
				</div>
				<div class="row" id="repeat-days">
					<div class="cell1"><span class="icon_btype"></span> Repeat On</div>
					<div class="cell2">:</div>
					<div class="cell3">
						<label><input type="checkbox" name="w_day[]" id="repeat-on-0" value="0" <?php if($day_number == 0) { echo 'checked="checked"'; } ?> /> Sun</label>
						<label><input type="checkbox" name="w_day[]" id="repeat-on-1" value="1" <?php if($day_number == 1) { echo 'checked="checked"'; } ?> /> Mon</label>
						<label><input type="checkbox" name="w_day[]" id="repeat-on-2" value="2" <?php if($day_number == 2) { echo 'checked="checked"'; } ?> /> Tue</label>
						<label><input type="checkbox" name="w_day[]" id="repeat-on-3" value="3" <?php if($day_number == 3) { echo 'checked="checked"'; } ?> /> Wed</label>
						<label><input type="checkbox" name="w_day[]" id="repeat-on-4" value="4" <?php if($day_number == 4) { echo 'checked="checked"'; } ?> /> Thu</label>
						<label><input type="checkbox" name="w_day[]" id="repeat-on-5" value="5" <?php if($day_number == 5) { echo 'checked="checked"'; } ?> /> Fri</label>
						<label><input type="checkbox" name="w_day[]" id="repeat-on-6" value="6" <?php if($day_number == 6) { echo 'checked="checked"'; } ?> /> Sat</label>
					</div>				
				</div>
				<div class="row" id="repeat-ends">
					<div class="cell1"><span class="icon_btype"></span> Ends</div>
					<div class="cell2">:</div>
					<div class="cell3">
						<label class="mr15"><input type="radio" name="repeat_end" id="repeat-end-never" value="never" checked="checked" /> Never</label>
						<label><input type="radio" name="repeat_end" id="repeat-end-ondate" value="ondate" /> On</label> 
						<input type="text" class="end_datepicker" id="repeat-end-date" data-date-format="dd/mm/yyyy" readonly="readonly" disabled="disabled" />
					</div>				
				</div>
                                <div class="row">
					<div class="cell1"><span class="icon_lock"></span> Lock Booking</div>
					<div class="cell2">:</div>
					<div class="cell3">
						<label><input name="lock_booking" id="lock-booking" type="checkbox" /> Lock Booking</label>
					</div>				
				</div>
                                <div class="row">
                                    <div class="cell1"><span class="icon_time"></span> Total Amount </div>
                                    <div class="cell2">:</div>
                                    <div class="cell3">
                                        <label>
                                            <input type="hidden" id="total_prebook_hrs" value="0">
                                            <input type="hidden" id="total_week_days" value="1">
                                            <!--<input name="hrs_per_week" id="b-hrs_per_week" value="" readonly="" type="text" class="popup-disc-fld" style="width: 80px;"/>-->
                                            <input name="tot_amout" id="tot_amout" value="" type="text" class="popup-disc-fld" style="width: 80px;"/>
                                        </label>
                                        <label style="font-size:13px;">&nbsp;&nbsp;<i class="fa fa-dollar"></i> Rate&nbsp;:&nbsp; 
                                            <input name="rate_per_hr" id="b-rate_per_hr" readonly="" value="" type="text" class="popup-disc-fld" style="width: 80px;"/>
                                        /hr
                                        </label>
                                    </div>
				</div>
                            <div class="row">
					<div class="cell1"><span class="icon_disc"></span> Pending Amount</div>
					<div class="cell2">:</div>
					<div class="cell3">
                                            <label><input name="pending_amount" id="b-pending-amount" type="text" class="popup-disc-fld" style="width: 100px;"/></label>
					</div>				
				</div>
                            <div class="row">
					<div class="cell1"><span class="icon_disc"></span> Discount</div>
					<div class="cell2">:</div>
					<div class="cell3">
                                            <label><input name="discount" id="b-discount" type="text" class="popup-disc-fld" style="width: 100px;" /></label>
					</div>				
				</div>
				
			</div>			
			<div style="clear:both; padding: 5px;"></div>
			<div class="pop-main-cont-fld-box"><textarea name="booking_note" id="booking-note" class="popup-note-fld" placeholder="Note to Driver"></textarea></div>
			
			<div class="pop-main-button">
				<input type="hidden" id="booking-id" />
				<input type="hidden" id="customer-address-id" />
				<input type="hidden" id="maid-id" />
                                <input type="hidden" id="service-date" />
				<input type="button" class="save-but" id="save-booking" value="Save" />
                                <!--<input type="button" class="copy-but" id="copy-booking" value="Copy" />-->
                                
				<input type="button" class="delete-but" id="delete-booking" value="Delete" />
                               
				<div class="clear"></div>
			</div>
		</div>
	</div>
</div>

<div id="schedule-report" style="display: none !important;"></div>