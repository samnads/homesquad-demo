<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
    .topiconnew{cursor: pointer;}
</style>
<div class="row m-0">  
    <div class="col-md-12">
    
    
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form class="form-horizontal" method="post">
                <ul>
                <li>
                    <i class="icon-th-list"></i>
                    <h3>User Activity Report</h3>  
                    </li>
                    <li>                 
                   
                    <input type="text" readonly="readonly" style=" width: 160px;" id="vehicle_date" name="activity_date" value="<?php echo $activity_date ?>">  
                    </li>
                    <li class="mr-2">                     
                    <input type="text" readonly="readonly" style="width: 160px; " id="schedule_date" name="activity_date_to" value="<?php echo $activity_date_to ?>">  
                    </li>
                    <li>                     
                    <input type="submit" class="n-btn" value="Go" name="vehicle_report">
                    </li>
                    
                    <li class="mr-0 float-right">
                   
                   
                    
                    <div class="topiconnew border-0 green-btn">
                       <a onclick="exportF(this)" title="Download to Excela"> <i class="fa fa-download"></i></a>
                    </div> 
                    
                    
                    </li>
                    </ul>              
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px;"> Sl No.</th>
                            <th style="line-height: 18px;"> Type</th>
                            <th style="line-height: 18px;"> Booking Type</th>
                            <th style="line-height: 18px;"> Shift</th>                            
                            <th style="line-height: 18px;"> Action</th>     
                            <th style="line-height: 18px;"> Done By</th>  
                            <th style="line-height: 18px;"> Time</th>     
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($user_activity_report))
                        {
                            $i = 0;
                            
                            foreach ($user_activity_report as $activity)
                            {
                                $action_type = $activity->action_type;
                                switch ($activity->action_type)
                                {
                                    case 'Add' : $action_type = 'New Booking';
                                        break;
                                    case 'Update' : $action_type = 'Booking Update';
                                        break;
                                    case 'Delete' : $action_type = 'Booking Delete';
                                        break;
                                }
                                echo '<tr>'
                                        . '<td style="line-height: 18px;">' . ++$i . '</td>'
                                        . '<td style="line-height: 18px;">' . str_replace("_",' ',$action_type) . '</td>'
                                        . '<td style="line-height: 18px;">' . $activity->booking_type . '</td>'
                                        . '<td style="line-height: 18px;">' . $activity->shift . '</td>'
                                        . '<td style="line-height: 18px;">' . $activity->action_content . '</td>'
                                        . '<td style="line-height: 18px;">' . $activity->user_fullname . '</td>'
                                        . '<td style="line-height: 18px;">' . $activity->added . '</td>'
                                    .'</tr>';
                            }
                        }
                        else
                        {
                            //echo '<tr><td colspan="5">No Results!</td></tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>

<div id="divToPrint" style="display: none">
    <div class="widget-content" style="margin-bottom:30px">
        <table border="1" width="100%" cellspacing="0" cellpadding = "10" style="font-size:11px; border-color: #ccc;" class="ptable">
            <thead>
                <tr>
                    <th style="line-height: 18px;"> Sl No.</th>
                    <th style="line-height: 18px;"> Type</th>
                    <th style="line-height: 18px;"> Booking Type</th>
                    <th style="line-height: 18px;"> Shift</th>                            
                    <th style="line-height: 18px;"> Action</th>     
                    <th style="line-height: 18px;"> Added By</th>  
                    <th style="line-height: 18px;"> Time</th>     
                </tr>
            </thead>
            <tbody>
                <?php
                if(!empty($user_activity_report))
                {
                    $i = 0;
                    
                    foreach ($user_activity_report as $activity)
                    {
                        $action_type = $activity->action_type;
                        switch ($activity->action_type)
                        {
                            case 'Add' : $action_type = 'New Booking';
                                break;
                            case 'Update' : $action_type = 'Booking Update';
                                break;
                            case 'Delete' : $action_type = 'Booking Delete';
                                break;
                        }
                        echo '<tr>'
                                . '<td style="line-height: 18px;">' . ++$i . '</td>'
                                . '<td style="line-height: 18px;">' . str_replace("_",' ',$action_type) . '</td>'
                                . '<td style="line-height: 18px;">' . $activity->booking_type . '</td>'
                                . '<td style="line-height: 18px;">' . $activity->shift . '</td>'
                                . '<td style="line-height: 18px;">' . $activity->action_content . '</td>'
                                . '<td style="line-height: 18px;">' . $activity->user_fullname . '</td>'
                                . '<td style="line-height: 18px;">' . $activity->added . '</td>'
                            .'</tr>';
                    }
                }
                else
                {
                    //echo '<tr><td colspan="5">No Results!</td></tr>';
                }
                ?>
            </tbody>
        </table>
    </div>
</div>


<script type="text/javascript">
  function exportF(elem) 
  {
      var table = document.getElementById("divToPrint");
      var html = table.outerHTML;
      var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url 
      elem.setAttribute("href", url);
      elem.setAttribute("download", "BookingReport.xls"); // Choose the file name
      return false;
  }  
</script>
