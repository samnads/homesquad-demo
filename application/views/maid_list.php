<style type="text/css">
.widget .widget-header {
	margin-bottom: 0px;
}
table.da-table tr td {
	padding: 0px 6px;
}
</style>
<div id="disable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Disable ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to disable this staff ?</h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_disable()">Disable</button>
    </div>
  </div>
</div>
<div id="enable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Enable ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to enable this staff ?</h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn mb-0" onclick="confirm_enable()">Enable</button>
    </div>
  </div>
</div>
<div id="avatar-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name" class="avatar-popup-name"></span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="" class="col-12 p-0">
      <div class="modal-body">
        <div class="img-container">
          <img class="thumbnail" id="avatar-popup-image" src="#">
        </div>
      </div>
    </div>
  </div>
</div>
<div id="info-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="maid_name" class="avatar-popup-name"></span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="" class="col-12 p-0">
      <div class="modal-body">
        <img id="maid_photo" class="thumbnail" src="<?php echo base_url('uploads/images/default/maid-avatar.png?v='.IMG_VERSION); ?>" style="height: 150px; width: 150px" />
    <table>
      <tbody>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Gender </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td style="line-height: 30px;"><span id="maid_gender"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Nationality </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_nationality"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Present Address </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_present_address"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Permanent Address </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_permanent_address"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Mobile Number 1 </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_mobile_1"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Mobile Number 2 </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_mobile_2"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>WhatsApp No.</b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_whatsapp_no_1"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Flat </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="flat_name"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Passport Number </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_passport_number"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Passport Expiry Date </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_passport_expiry_date"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Visa Number </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_visa_number"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Visa Expiry Date </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_visa_expiry_date"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Labour Card Number </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_labour_card_number"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Labour card Expiry Date </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_labour_card_expiry_date"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Emirates Id </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_emirates_id"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Emirates Id Expiry Date </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_emirates_expiry_date"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Notes </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_notes"></span></td>
        </tr>
      </tbody>
    </table>
      </div>
    </div>
  </div>
</div>
<div class="row m-0">
  <div class="col-md-12">
    <div class="widget widget-table action-table">
      <div class="widget-header">

      <ul>
      <li>

      <i class="icon-th-list"></i>
        <h3>Staffs</h3>
<li>
				<span style="margin-left:5px;"></span>
				<div class="mm-drop">
					<select style="width: 100px;" id="maid-status">
						 <option value="1">All</option>
          <option value="2">Active</option>
          <option value="3">Inactive</option>
          <option value="4">Deleted</option>
					</select>
				</div>

               </li>
        </li>

        <li class="mr-0 float-right">
        <div class="topiconnew border-0 green-btn"> <a href="<?php echo base_url(); ?>maid/toExcel/<?php echo $active; ?>" title="Download"> <i class="fa fa-file-excel-o"></i></a> </div>
        <div class="topiconnew border-0 green-btn"> <a href="<?php echo base_url(); ?>maid/add" title="New"> <i class="fa fa-user-plus"></i></a> </div>

        </li>
        </ul>

      </div>
      <!-- /widget-header -->

      <div class="widget-content">
        <table id="maids-list-table" class="table table-hover da-table" width="100%">
          <thead>
            <tr>
              <th style="width:45px;">Sl. No.</th>
              <th>Image</th>
              <th>Name</th>
              <th>Present Address</th>
              <th>Mobile Number</th>
              <!--                    <th style="line-height: 18px"> <center>Photo</center></th>-->
              <th class="td-actions" style="width:120px !important;"> Action </th>
            </tr>
          </thead>
          <tbody>
            <?php
if (count($maids) > 0) {
    $i = 1;
    foreach ($maids as $maids_val) {
        ?>
            <tr>
              <?php
//                                    if ($maids_val['maid_photo_file'] == "") {
        //                                        $image = base_url() . "img/no_image.jpg";
        //                                    } else {
        //                                        $image = base_url() . "maid_img/" . $maids_val['maid_photo_file'];
        //                                    }
        ?>
              <td style="line-height: 18px;"><center>
                  <?php echo $i; ?>
                </center></td>
                <?php
$image = $maids_val['maid_photo_file'];
        ?>
              <td style="line-height: 18px; cursor: pointer;text-align: center;" onclick="showAvatar('<?=$maids_val['maid_name']?>','<?=$maids_val['maid_photo_file']?>')">
                <img  src="<?php echo $maids_val['maid_photo_file']; ?>"
                        style="height: 40px; width: 40px; margin: 3px auto 3px; border-radius: 50%;" /></td>
              <td style="line-height: 18px; cursor: pointer;" onclick="view_maid(<?php echo $maids_val['maid_id'] ?>);"><?php echo $maids_val['maid_name'] ?>
</td>
              <td style="line-height: 18px; cursor: pointer" onclick="view_maid(<?php echo $maids_val['maid_id'] ?>);"><?php echo $maids_val['maid_present_address'] ?></td>
              <td style="line-height: 18px; cursor: pointer" onclick="view_maid(<?php echo $maids_val['maid_id'] ?>);"><?php echo $maids_val['maid_mobile_1'] ?></td>

              <!--                                    <td style="line-height: 18px"><center><img height="135" width="95" src="<?php //echo $image;
        ?>"/></center></td>-->
              <td style="line-height: 18px;width:120px !important" class="td-actions"><center>
                  <!--                                <a href="#myModal" role="button" class="btn btn-small btn-info" data-toggle="modal"><i class="btn-icon-only icon-search"></i></a>-->
                  <!--                                            <a class="btn btn-small btn-info" href="javascript:void();" onclick="view_maid(<?php //echo $maids_val['maid_id']
        ?>);"><i class="btn-icon-only icon-search"> </i></a>-->
                  <a class="n-btn-icon blue-btn"
                        href="<?php echo base_url(); ?>maid/view/<?php echo $maids_val['maid_id'] ?>" title="View"><i
                          class="btn-icon-only fa fa-eye"> </i></a>
                  <?php if ($maids_val['maid_status'] != 2) {?>
                  <a class="n-btn-icon purple-btn"
                          href="<?php echo base_url(); ?>maid/edit/<?php echo $maids_val['maid_id'] ?>" title="Edit"><i
                            class="btn-icon-only icon-pencil"> </i></a>
                  <?php }?>
                  <?php if ($maids_val['maid_status'] == 2) {?>
                  <p class="n-btn-icon red-btn" style="cursor:default;" title="Deleted"><i class="fa fa-ban"></i></p>
                  <?php }?>
                  <?php
//if(user_authenticate() == 1)
        if ($this->session->userdata('user_logged_in')['user_admin'] == 'Y') {
            if ($maids_val['maid_status'] == 1) {
                ?>
                  <a href="javascript:void(0)" class="n-btn-icon green-btn" title="Disable"
                            onclick="confirm_disable_modal(<?php echo $maids_val['maid_id'] ?>,<?php echo $maids_val['maid_status'] ?>);"><i
                              class="btn-icon-only fa fa-toggle-on "> </i></a>
                  <?php
}
            if ($maids_val['maid_status'] == 0) {
                ?>
                  <a href="javascript:void(0)" class="n-btn-icon red-btn" title="Enable"
                            onclick="confirm_enable_modal(<?php echo $maids_val['maid_id'] ?>,<?php echo $maids_val['maid_status'] ?>);"><i
                              class="btn-icon-only fa fa-toggle-off"> </i></a>
                  <?php
}
            //                                         if ($maids_val['maid_status']!=2) {
            ?>

                  <!--                                        <a href="javascript:void(0)" class="btn btn-danger btn-small" onclick="confirm_delete_modal(<?php echo $maids_val['maid_id'] ?>,'2');" title="Delete"><i class="btn-icon-only icon-trash"></i> </a>    -->

                  <?php
//                                         }
        }
        ?>
                </center></td>
            </tr>
            <?php
$i++;
    }
} else {
    ?>
            <tr class="bg-warning">
              <td colspan="6" style="text-align:center;">No Data Found !</td>
            <tr>
              <?php
}
?>
          </tbody>
        </table>
      </div>
      <!-- /widget-content -->
    </div>
    <!-- /widget -->
  </div>
  <!-- /span6 -->
</div>

<!-- Modal
<div id="myModal" class="modal hide fade" style="width: 700px; left: 45%" tabindex="-1" role="dialog"
  aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><span id="maid_nae">Maid</span></h3>
  </div>
  <div class="modal-body"> <a style="float:right ; margin-right:20px; cursor:pointer;">
  <img id="maid_photo"
        src="<?php echo base_url(); ?>img/profile_pic.jpg" style="height: 150px; width: 150px" /> </a>
    <table>
      <tbody>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Gender </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td style="line-height: 30px;"><span id="maid_gender"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Nationality </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_nationality"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Present Address </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_present_address"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Permanent Address </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_permanent_address"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Mobile Number 1 </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_mobile_1"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Mobile Number 2 </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_mobile_2"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Flat </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="flat_name"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Passport Number </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_passport_number"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Passport Expiry Date </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_passport_expiry_date"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Visa Number </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_visa_number"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Visa Expiry Date </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_visa_expiry_date"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Labour Card Number </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_labour_card_number"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Labour card Expiry Date </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_labour_card_expiry_date"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Emirates Id </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_emirates_id"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Emirates Id Expiry Date </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_emirates_expiry_date"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Notes </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_notes"></span></td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="modal-footer">
  </div>
</div>-->

<!--<style>

    .imge{
        display: none
    }

    td.dispimage:hover img{
        display: block;
    }
</style>-->
<script>
  var filter_status = <?= $this->uri->segment(2) ?: 2 ?>;
  function maidFilter(id) {
    window.location.href = _base_url + "maids/" + id;
  }
  (function (a) {
    a(document).ready(function (b) {
      if (a('#maids-list-table').length > 0) {
        a("table#maids-list-table").dataTable({
          'sPaginationType': "full_numbers", "bSort": true, "iDisplayLength": 100, "scrollY": true,"orderMulti": false,
          "scrollX": true,
          'columnDefs': [
            {
              'targets': [1, 4, 5],
              'orderable': false
            },
          ]
        });
      }
    });
  })(jQuery);
  function showAvatar(name,src) {
		$('.avatar-popup-name').html(name);
		$('#avatar-popup-image').attr('src', src);
	$.fancybox.open({
		autoCenter: true,
		fitToView: false,
		scrolling: false,
		openEffect: 'none',
		openSpeed: 1,
		autoSize: false,
		width: 400,
		height: 'auto',
		helpers: {
			overlay: {
				css: {
					'background': 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: true
			}
		},
		padding: 0,
		closeBtn: false,
		content: $('#avatar-popup'),
	});
}
var maid_id;
var maid_status;
function confirm_disable() {
	$.ajax({
		type: "POST",
		url: _base_url + "maid/change_status",
		data: {
			maid_id: maid_id,
			status: maid_status
		},
		dataType: "text",
		cache: false,
		success: function(result) {
			window.location.assign(_base_url + 'maids/'+filter_status);
		}
	});
}
function confirm_enable() {
	$.ajax({
		type: "POST",
		url: _base_url + "maid/change_status",
		data: {
			maid_id: maid_id,
			status: maid_status
		},
		dataType: "text",
		cache: false,
		success: function(result) {
			window.location.assign(_base_url + 'maids/'+filter_status);
		}
	});
}
function confirm_delete() {
	$.ajax({
		type: "POST",
		url: _base_url + "maid/change_status",
		data: {
			maid_id: maid_id,
			status: maid_status
		},
		dataType: "text",
		cache: false,
		success: function(result) {
			window.location.assign(_base_url + 'maids');
		}
	});
}
function confirm_disable_modal(maid, status) {
	maid_id = maid;
	maid_status = status;
	$.fancybox.open({
		autoCenter: true,
		fitToView: false,
		scrolling: false,
		openEffect: 'none',
		openSpeed: 1,
		autoSize: false,
		width: 450,
		height: 'auto',
		helpers: {
			overlay: {
				css: {
					'background': 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding: 0,
		closeBtn: false,
		content: $('#disable-popup'),
	});
}
function confirm_enable_modal(maid, status) {
	maid_id = maid;
	maid_status = status;
	$.fancybox.open({
		autoCenter: true,
		fitToView: false,
		scrolling: false,
		openEffect: 'none',
		openSpeed: 1,
		autoSize: false,
		width: 450,
		height: 'auto',
		helpers: {
			overlay: {
				css: {
					'background': 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding: 0,
		closeBtn: false,
		content: $('#enable-popup'),
	});
}
$(function() {
	let current = window.location.href;
	$('#primary_nav_wrap li a').each(function() {
		var $this = $(this);
		// if the current path is like this link, make it active
		if ($this.attr('href') === '<?php echo base_url('maids'); ?>') {
			$this.addClass('active');
		}
	})
})
$("#maid-status > option").each(function() {
  if(filter_status == this.value){
     $(this).attr("selected","selected");
  }
});
$('#maid-status').change(function () {
        window.location = _base_url + 'maids/' + $('#maid-status').val();
    });
</script>