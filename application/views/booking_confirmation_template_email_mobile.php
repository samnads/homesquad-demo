<html>
<head>
</head>

<body>

<div class="main-wrapper" style="width: 800px; margin:0 auto;">

     <div style="width: 800px;"><img src="<?php echo base_url(); ?>images/elitemaidemaidbanner.jpg" width="800" height="232" alt="" /></div>
     
     <div style="border-left: 1px solid #ccc; border-right: 1px solid #ccc; width: 798px;">
	 <div style="width: 600px; height:auto; margin: 0 auto; padding: 0px 0px 0px 0px;">
     
     <div style="width: 600px; height:auto; padding: 0px 0px 30px 0px; margin-bottom: 30px;">
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding:20px 0px 0px 0px; font-weight: bold; margin:0px;">Dear <?php echo $name;  ?>,</p>
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px;">Welcome to Elitemaids Cleaning Services. Thank You for booking with us. You can find your booking ID below. We hope you have a great experience with our services...</p>
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:15px; line-height:20px; color: #FFF; padding: 12px 0px 0px 0px; margin:0px;">
          <span style="font-size:15px; background: #00bff3; padding: 12px 30px; border-radius: 50px; margin:0px; font-weight: bold;">Your Reference ID - <?php echo $bookingdetails[0]->reference_id;?></span>
          </p>
     </div>
     
     
     <div style="width: 560px; height:auto; background: #fafafa; border:1px solid #e5e2e2; padding: 20px 20px; ">
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding:0px 0px 0px 0px; font-weight: bold; margin:0px;">Address Summary</p>
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding:5px 0px 0px 0px; margin:0px;">Building : <?php echo $bookingdetails[0]->building; ?><br />
             Unit : <?php echo $bookingdetails[0]->unit_no; ?><br />
             Street : <?php echo $bookingdetails[0]->street; ?><br />
             Area - <?php if($bookingdetails[0]->area_name == 'Other') { echo $bookingdetails[0]->other_area; } else { echo $bookingdetails[0]->area_name; } ?> </p>
     </div>
     
     
     
     <div style="width: 600px; height:auto; background: #fafafa; border:1px solid #e5e2e2; border-top:0px; padding:0px 0px;">
          <table width="600" border="0" cellspacing="0" cellpadding="0" bordercolor="#FFF">
            <tr style="background: #FFF;">
              <td width="298"><p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">No of Maids</p></td>
              <td width="4">:</td>
              <td width="298"><p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;"><?php echo $bookingdetails[0]->no_of_maids; ?></p></td>
          
            </tr>
			
			<tr style="background: #FFF;">
              <td width="298"><p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">No of Visits</p></td>
              <td width="4">:</td>
              <td width="298"><p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;"><?php echo $bookingdetails[0]->number_of_visits; ?></p></td>
          
            </tr>
			
			<?php
			$clean_fee = 0;
			$service_charge = 0;
			$vat_charge = 0;
			$tot_service = 0;
			foreach ($bookingdetails as $details)
			{
				// $service_charge += $details->service_charge;
				// $vat_charge += $details->vat_charge;
				// $tot_service += $details->total_amount;
				$service_charge += $details->net_service_charge;
				$vat_charge += $details->net_vat_charge;
				$tot_service += $details->total_net_amount;
			?>
			<tr style="  ">
              <td><p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Service Date & Time</p></td>
              <td style="">:</td>
              <td><p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;"><?php echo $details->service_start_date; ?> (One Day) (<?php echo date("l",strtotime($details->service_start_date)); ?>)<br/><?php echo date('h:i a', strtotime($details->time_from)) . ' - ' . date('h:i a', strtotime($details->time_to)) ?></p></td>
            </tr>
			<?php
			}
			?>
            
            <tr style="background: #FFF;">
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Crew In</p></td>
              <td style="border-top:1px solid #e5e2e2;">:</td>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;"><?php echo $bookingdetails[0]->crew_in; ?></p></td>
            </tr>
            
            
            <tr>
              <td colspan="3">
              <p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px 0px; margin:0px;">Instructions</p>
              
              <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 5px 20px 15px; margin:0px;"><?php echo $bookingdetails[0]->booking_note; ?></p></td>
            </tr>
			
			<?php
			if($bookingdetails[0]->cleaning_material=="Y"): ?>
			<tr style="background: #FFF;">
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Cleaning Material</p></td>
              <td style="border-top:1px solid #e5e2e2;">:</td>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;">Yes</p></td>
            </tr>
			<?php endif; ?>
			
			<?php
			if($bookingdetails[0]->interior_window_clean == 1)
			{
				$intrior = "Yes";
			} else {
				$intrior = "No";
			}
			if($bookingdetails[0]->fridge_cleaning  == 1)
			{
				$fridge = "Yes";
			} else {
				$fridge = "No";
			}
			if($bookingdetails[0]->ironing_services  == 1)
			{
				$ironing = "Yes";
			} else {
				$ironing = "No";
			}
			if($bookingdetails[0]->oven_cleaning  == 1)
			{
				$oven = "Yes";
			} else {
				$oven = "No";
			}
			?>
			<?php
			if($bookingdetails[0]->interior_window_clean == 1 || $bookingdetails[0]->fridge_cleaning == 1 || $bookingdetails[0]->ironing_services == 1 || $bookingdetails[0]->oven_cleaning == 1)
			{
			?>
			<tr>
              <td colspan="3">
              <p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px 0px; margin:0px;">Extra Services</p>
              
              <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 5px 20px 15px; margin:0px;">
			  
			  <?php
				if($bookingdetails[0]->interior_window_clean == 1)
				{
				?>
				Interior Windows - <?php echo $intrior; ?><br>
				<?php } ?>
				<?php
				if($bookingdetails[0]->fridge_cleaning == 1)
				{
				?>
				Fridge Cleaning - <?php echo $fridge; ?>&nbsp;<br>
				<?php
				}
				?>
				<?php
				if($bookingdetails[0]->ironing_services  == 1)
				{
				?>
				Ironing - <?php echo $ironing; ?>&nbsp;<br>
				<?php } ?>
				<?php
				if($bookingdetails[0]->oven_cleaning  == 1)
				{
				?>
				Oven Cleaning - <?php echo $oven; ?><br />
				<?php
				}
				?>
			  
			  </p></td>
            </tr>
			<?php
			}
			?>
			
			<tr style="background: #FFF;">
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Total Hours</p></td>
              <td style="border-top:1px solid #e5e2e2;">:</td>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;"><?php echo $bookingdetails[0]->no_of_hrs ?></p></td>
            </tr>
            
            <tr>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Payment Method</p></td>
              <td style="border-top:1px solid #e5e2e2;">:</td>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;"><?php if($bookingdetails[0]->pay_by == 'card') { echo 'Card'; } else { echo 'Cash'; } ?></p></td>
            </tr>
			
			<?php
			//$service_charges = ($service_charge + $coupon_fee);
			//$service_charges = ($service_charge);
			if($coupon_fee > 0)
			{
			?>
			<tr style="background: #FFF;">
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Service Cost</p></td>
              <td style="border-top:1px solid #e5e2e2;">:</td>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;">AED <?php echo number_format($service_charge, 2); ?></p></td>
            </tr>
			<tr>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Discount(Coupon used)</p></td>
              <td style="border-top:1px solid #e5e2e2;">:</td>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;">AED <?php echo number_format($coupon_fee, 2); ?></p></td>
            </tr>
			<?php    
			}
			?>
			
			<?php
			if($coupon_fee > 0)
			{
				$fe = ($service_charge - $coupon_fee);
			?>
			<tr style="background: #FFF;">
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Total Cost</p></td>
              <td style="border-top:1px solid #e5e2e2;">:</td>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;">AED <?php echo number_format($fe, 2); ?></p></td>
            </tr>
			<?php
			} else {
			?>
			<tr>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Total Cost</p></td>
              <td style="border-top:1px solid #e5e2e2;">:</td>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;">AED <?php echo number_format($service_charge, 2); ?></p></td>
            </tr>
			<?php } ?>
			
			<tr>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Vat Cost</p></td>
              <td style="border-top:1px solid #e5e2e2;">:</td>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;">AED <?php echo number_format($vat_charge, 2); ?></p></td>
            </tr>
			
			<?php
			if($bookingdetails[0]->pay_by_cash_charge > 0): ?>
			<tr>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Pay by cash charge</p></td>
              <td style="border-top:1px solid #e5e2e2;">:</td>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;">AED <?php echo $bookingdetails[0]->pay_by_cash_charge; ?></p></td>
            </tr>
			<?php endif; ?>
            
            <tr style="background: #FFF;">
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Net Payable</p></td>
              <td style="border-top:1px solid #e5e2e2;">:</td>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; font-weight: bold; padding: 12px 10px; margin:0px;">AED <?php echo number_format($tot_service, 2); ?></p></td>
            </tr>
            
          </table>

     </div>
     
     
     
     
     <div style="width: 600px; height:auto; padding: 20px 0px 0px 0px; margin-bottom: 30px; text-align: center;">
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px;">
          <strong>Elite Maids Cleaning Services</strong><br />
						  Office 201B, Prime Business Center,<br />
						  Jumeirah Village Circle, Dubai,<br />
						  United Arab Emirates.<br />
						  For Bookings : +971 800 258 / +971 58 286 4783<br />
						  Email : info@elitemaids.ae</p>
     </div>
     
     

     
     </div>
     
<div style="width: 798px; height:auto; padding: 0px 0px 0px 0px; background: #fafafa; text-align: center;">
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:12px; color: #555; line-height:20px; padding: 12px 0px 10px 0px; margin:0px;">© <?php echo date('Y'); ?> Elitemaids All Rights Reserved.</p>
     </div>

</div>
	 
	 </div>


</body>
</html>
