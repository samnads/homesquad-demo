<script src="<?php echo base_url('js/jquery-1.7.2.min.js'); ?>"></script>
<style type="text/css">
    .select2-arrow{visibility : hidden;}
	.select2-container .select2-choice{
	-moz-appearance: none;
    background: #fff url("../images/ash-arrow.png") no-repeat scroll right 7px top 11px;
   border: 1px solid #ccc;
    border-radius: 3px;
    cursor: pointer;
    font-size: 12px;
    height: 30px;
    line-height: 24px;
   padding: 3px 0 3px 10px;

    text-indent: 0.01px;
	}
        
    #user-repeat-days label{
		display:inline;
	}
	.in-bookingform-field-droop-main label{
		display:inline;
	}
	#user-repeat-end-date {
		width: 73%;
	}
   
</style>
<section>
    <div class="row dash-top-wrapper no-left-right-margin">
		<div class="col-md-12 col-sm-12 no-left-right-padding bg-white">
			<div class="widget-header" style="margin-bottom: 0;">
				<i class="icon-th-list"></i>
				<h3>Payment Details</h3>
				<a href="<?php echo base_url(); ?>customer/list_customer_payments" class="btn" style="float: right; margin: 5px 20px;">Back</a>
				<a class="btn" style="margin: 5px 15px 5px 5px; float:right;" href="<?php echo base_url(); ?>payment/payment_receipt/<?php echo $payid; ?>" target="_blank" title="Download Receipt">
					<i class="btn-icon-only fa fa-download "> </i>
				</a>
				<?php
				if($payment_detail->email_address != "")
				{
				?>
				<!--<div style="margin: 5px 10px 5px 5px; float:right;" class="btn send_receipt_mail" data-payid="<?php echo $payid; ?>" data-payemail="<?php echo $payment_detail->email_address; ?>"><span class="btntxt">Send Receipt Mail</span></div>-->
				<?php
				}
				?>
			</div>
			<div class="col-md-12 col-sm-12 confirm-det-cont-box no-left-right-padding borderbox">
				<div class="col-md-6 col-sm-6 confi-det-cont-det new-booking-box-main no-left-padding">
					<div class="col-md-12 col-sm-12 text-field-main no-top-padding">
						<div id="u-error" style="color:red;"></div>
					</div>
					<div class="col-md-12 col-sm-12 text-field-main no-top-padding">
						<div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
							Customer <span>:</span>
						</div>
						<div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
							
							<div class="in-bookingform-field-droop-main" style="padding: 7px;">
								<?php echo $payment_detail->customer_name; ?>
							</div>
						</div>
					</div>
					<div class="col-md-12 col-sm-12 text-field-main no-top-padding">
						<div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
							Memo <span>:</span>
						</div>
						<div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
							
							<div class="in-bookingform-field-droop-main" style="padding: 7px;">
								<?php
									if($payment_detail->payment_type == "CM")
									{
										$creditmemo = " - Credit Memo";
									} else {
										$creditmemo = "";
									}
									echo $payment_detail->ps_no.$creditmemo;
								?>
							</div>
						</div>
					</div>
					<div class="col-md-12 col-sm-12 text-field-main no-top-padding">
						<div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
							Paid Amount <span>:</span>
						</div>
						<div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
							
							<div class="in-bookingform-field-droop-main" style="padding: 7px;">
								<?php echo 'AED '.$payment_detail->paid_amount; ?>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 confi-det-cont-det no-right-padding">
					<div id="customer-detail-popup" class="col-md-12 col-sm-12" style="float:right;display:block;">
						<div class="booking_form">
							<div class="col-md-12 col-sm-12 text-field-main no-top-padding">
								
							</div>
							<div class="col-md-12 col-sm-12 text-field-main no-top-padding">
								<div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
									Payment Date <span>:</span>
								</div>
								<div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
									<div class="in-bookingform-field-droop-main" style="padding: 7px;">
									<?php echo date('d/m/Y',strtotime($payment_detail->paid_datetime)) ?>    
									</div>
								</div>
							</div>
							<div class="col-md-12 col-sm-12 text-field-main no-top-padding">
								<div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
									Payment Method <span>:</span>
								</div>
								<div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
									<div class="in-bookingform-field-droop-main" style="padding: 7px;">
									<?php
									if($payment_detail->payment_method == 0)
									{
										echo "Cash";
									} else if($payment_detail->payment_method == 1){
										echo "Card";
									} else if($payment_detail->payment_method == 2){
										echo "Cheque";
									} else if($payment_detail->payment_method == 3){
										echo "Bank";
									} else if($payment_detail->payment_method == 4){
										echo "Credit Card";
									}
									?>    
									</div>
								</div>
							</div>
							<div class="col-md-12 col-sm-12 text-field-main no-top-padding">
								<div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
									Balance Amount <span>:</span>
								</div>
								<div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
									<div class="in-bookingform-field-droop-main" style="padding: 7px;">
										<?php echo 'AED '.$payment_detail->balance_amount; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!--welcome-text-main end-->
		
		<div class="col-md-12 col-sm-12 no-left-right-padding">
			<div class="widget-header" style="margin-bottom: 0; margin-top: 10px;">
				<i class="icon-th-list"></i>
				<h3>Unpaid Invoices</h3> 
				<?php if($payment_detail->balance_amount > 0): ?>
				<button class="btn" name="allocate-btn" id="allocate-btn" data-payid="<?php echo $payment_detail->payment_id; ?>" data-totamt="<?php echo $payment_detail->balance_amount; ?>" style="float: right; margin: 5px 20px;">Allocate</button>
				<?php endif; ?>
			</div>
			<div class="widget-content borderbox" style="margin-bottom:30px">
                <table id="" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Sl.No</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Invoice No</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Date</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Amount</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Received Amount</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Amount to Pay</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> <input type="checkbox" id="master-pay" style="margin-right: 5px;">Allocate</th>
                        </tr>
                    </thead>
                    <tbody>
					<?php
					$i = 1;
					$sub_t = 0;
                    foreach ($invoice_detail as $inv_detail)
					{
						$sub_t += round($inv_detail->invoice_net_amount,2);
					?>
						<tr>
							<td style="line-height: 18px; text-align: center"><?php echo $i; ?> </td>
							<td style="line-height: 18px; text-align: center"><?php echo $inv_detail->invoice_num; ?> </td>
							<td style="line-height: 18px; text-align: center"><?php echo date('d/m/Y',strtotime($inv_detail->invoice_date)); ?> </td>
							<td style="line-height: 18px; text-align: center"><?php echo $inv_detail->invoice_net_amount; ?></td>
							<td style="line-height: 18px; text-align: center"><?php echo $inv_detail->received_amount; ?></td>
							<td style="line-height: 18px; text-align: center"><?php echo $inv_detail->balance_amount; ?></td>
							<td style="line-height: 18px; text-align: center">
								<input type="checkbox" class="sub_chk" data-amt="<?php echo $inv_detail->balance_amount; ?>" data-id="<?php echo $inv_detail->invoice_id; ?>" data-ref="<?php echo $inv_detail->invoice_num; ?>">
							</td>
						</tr>
					<?php
					$i++;	
					}
					?>
						<tr>
							<td></td>
							<td></td>
							<td style="text-align: center">Total</td>
							<td style="text-align: center"><?php echo $sub_t; ?></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 
		</div>
		
		<div class="col-md-12 col-sm-12 no-left-right-padding">
			<div class="widget-header" style="margin-bottom: 0; margin-top: 10px;">
				<i class="icon-th-list"></i>
				<h3>Paid Invoices</h3>
			</div>
			<div class="widget-content borderbox" style="margin-bottom:30px">
                <table id="" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Sl.No</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Invoice No</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Date</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Amount</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Paid Amount</th>
							<!--<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Amount to Pay</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> <input type="checkbox" id="master-pay" style="margin-right: 5px;">Allocate</th>-->
                        </tr>
                    </thead>
                    <tbody>
					<?php
					$i = 1;
					$sub_t = 0;
                    foreach ($allocated_invoices as $invs_detail)
					{
						$sub_t += round($invs_detail->payment_amount,2);
					?>
						<tr>
							<td style="line-height: 18px; text-align: center"><?php echo $i; ?> </td>
							<td style="line-height: 18px; text-align: center"><?php echo $invs_detail->invoice_num; ?> </td>
							<td style="line-height: 18px; text-align: center"><?php echo date('d/m/Y',strtotime($invs_detail->invoice_date)); ?> </td>
							<td style="line-height: 18px; text-align: center"><?php echo $invs_detail->invoice_net_amount; ?></td>
							<td style="line-height: 18px; text-align: center"><?php echo $invs_detail->payment_amount; ?></td>
							<!--<td style="line-height: 18px; text-align: center"><?php// echo $inv_detail->balance_amount; ?></td>
							<td style="line-height: 18px; text-align: center">
								<input type="checkbox" class="sub_chk" data-amt="<?php// echo $inv_detail->balance_amount; ?>" data-id="<?php// echo $inv_detail->invoice_id; ?>" data-ref="<?php// echo $inv_detail->invoice_num; ?>">
							</td>-->
						</tr>
					<?php
					$i++;	
					}
					?>
						<tr>
							<td></td>
							<td></td>
							<td style="text-align: center"></td>
							<td style="text-align: center">Total</td>
							<td style="text-align: center"><?php echo $sub_t; ?></td>
							<!--<td></td>
							<td></td>-->
						</tr>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 
		</div>
    </div><!--row content-wrapper end-->
</section><!--welcome-text end-->

<button type="button" class="btn btn-info btn-lg openpaymodal hidden" data-toggle="modal" data-target="#payModal">Open Modal</button>

<!-- Modal -->
<div id="payModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" style="margin-top: -6px;font-size:32px;">&times;</button>
        <h4 class="modal-title">&nbsp;</h4>
      </div>
      <div class="modal-body">
          <div id="emailpopmsg"></div>
          <div class="form-group">
            <label for="email_content">Email Content:</label>
            <textarea class="form-control" id="email_content"></textarea>
          </div>
          <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" class="form-control" id="email">
            <input type="hidden" class="form-control" id="hidinvid">
          </div>
          
          <button type="submit" class="btn btn-default emailsubbtn">Submit</button>
          <button type="submit" class="btn btn-default emailloadbtn" style="display:none;"><i class="fa fa-spinner fa-spin" ></i></button>
      </div>
      <!-- <div class="modal-footer text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
    </div>

  </div>
</div>
<script type="text/javascript">
$( document ).ready(function() {
	$(".emailsubbtn").click(function() {
        $("#emailpopmsg").html('');
        var payementid=$("#hidinvid").val();
        var paymentcontent=$("#email_content").val();
        var invoiceemail=$("#email").val();
        //console.log(invoiceid);
        if(paymentcontent=='')
        {
            $("#emailpopmsg").html('<div class="alert alert-warning text-center"><strong>Please enter email content!</strong></div>');
            return false;
        }

        if(!validateEmail(invoiceemail))
        {
            $("#emailpopmsg").html('<div class="alert alert-warning text-center"><strong>Please check the email entered!</strong></div>');
            return false;
        }

        $(this).hide();
        $(".emailloadbtn").show();
        $.ajax({
		   url: "<?php echo base_url();?>payment/send_receipt_email",
		   type: "post",
		   data: {paymentid:payementid,paymentcontent:paymentcontent,paymentemail:invoiceemail} ,
		   success: function (response) {
				$(".emailsubbtn").show();
				$(".emailloadbtn").hide();
				if(response=='success')
				{
					$("#emailpopmsg").html('<div class="alert alert-success text-center"><strong>Receipt email sent successfully!</strong></div>');
					$("#email_content,#email").val('')
					//$(".openmodal").click(); 
				}
				else if(response=='email_error')
				{
					$("#emailpopmsg").html('<div class="alert alert-warning text-center"><strong>Please check customer email!</strong></div>');
					//$(".openmodal").click();
				}
				else
				{
					$("#emailpopmsg").html('<div class="alert alert-danger text-center"><strong>Error!Try again later</strong></div>');
					//$(".openmodal").click();
				}

		   },
		   error: function(jqXHR, textStatus, errorThrown) {
				$(".emailsubbtn").show();
				$(".emailloadbtn").hide();
				$("#emailpopmsg").html('<div class="alert alert-danger text-center"><strong>Error!Try again later</strong></div>');
				//$(".openmodal").click();

		   }
	   });
    });
});
function validateEmail(email) 
{
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}
$(".send_receipt_mail").click(function () {
        var paymentid = $(this).data('payid');
        var emailid = $(this).data('payemail');
        $("#email").val(emailid); $("#hidinvid").val(paymentid);
        $("#emailpopmsg").html('');
        $("#emailpopmsg").html('');
        $(".openpaymodal").click();
    });

$('#master-pay').on('click', function (e) {
    if ($(this).is(':checked', true)) {
        $(".sub_chk").prop('checked', true);
    } else {
        $(".sub_chk").prop('checked', false);
    }
});
$('#allocate-btn').on('click', function (e) {
    var allVals = [];
    var payVals = [];
    var refVals = [];
    var payamt = $(this).attr('data-totamt');
    var orgamt = $(this).attr('data-orgamt');
    var payid = $(this).attr('data-payid');
    $(".sub_chk:checked").each(function () {
        allVals.push($(this).attr('data-id'));
        payVals.push($(this).attr('data-amt'));
        refVals.push($(this).attr('data-ref'));
    });
    if (allVals.length <= 0) {
        $.fancybox.open({
            autoCenter: true,
            fitToView: false,
            scrolling: false,
            openEffect: 'none',
            openSpeed: 1,
            helpers: {
                overlay: {
                    css: {
                        'background': 'rgba(0, 0, 0, 0.3)'
                    },
                    closeClick: true
                }
            },
            padding: 0,
            closeBtn: true,
            content: _alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close2"></span></div><div class="content padd20">Please select invoice...</div><div class="bottom"><input type="button" value="Close" class="assign_no pop_close2"  /></div></div>',
            topRatio: 0.2,

        });
    } else {
        var check = confirm("Are you sure you want to allocate payment?");
        if (check == true) {
            var join_selected_values = allVals.join(",");
            var join_selected_payvalues = payVals.join(",");
            var join_selected_refvalues = refVals.join(",");
            $.ajax({
                url: _base_url + "customer/allocate",
                type: 'POST',
                //data: 'ids='+join_selected_values,
                dataType: "json",
                data: { ids: join_selected_values, payvall: join_selected_payvalues, refVal: join_selected_refvalues, payamt: payamt, payid: payid },
                success: function (data) {
                    if (data.status == "success") {
                        var messag = '';
                        /*if(data.msg!=''){var messag='Quickbook errors <br/>'.data.msg;}*/
                        $.fancybox.open({
                            autoCenter: true,
                            fitToView: false,
                            scrolling: false,
                            openEffect: 'none',
                            openSpeed: 1,
                            helpers: {
                                overlay: {
                                    css: {
                                        'background': 'rgba(0, 0, 0, 0.3)'
                                    },
                                    closeClick: true
                                }
                            },
                            padding: 0,
                            closeBtn: true,
                            content: _alert_html = '<div id="alert-popup"><div class="head">Success ' + messag + '<span class="alert-popup-close pop_close2"></span></div><div class="content padd20">Payment allocated successfully.</div><div class="bottom"><input type="button" value="Close" class="assign_no pop_close2"  /></div></div>',
                            topRatio: 0.2,

                        });
                        window.location.reload();
                    }
                },
                error: function (data) {
                    alert(JSON.stringify(data));
                }
            });
        }
    }
});
</script>