<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=delayed_report.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<table border="1">
                    <thead>
                        <tr>
                            <!--<th class="no-left-border"></th>-->
                            <th>Sl</th>
                            <th>Customer</th>
                            <th>Maid</th>
                            <th>Booking Time</th>
                            <th>Drop & Pick Time</th>
                            <th>Zone(Driver)</th>
                            <th>Amount</th>
                            <th>Company</th>
                            <th>Location</th>
                            <!-- <th>SMS</th> -->
                        </tr>
                    </thead>
                    <tbody id="invoice-tabtbody1">
                        <?php
                        $sln = 1;
                        foreach($current_booking as $val)
                        {
							$sms_button="";
							$newtime = date('H:i:s',strtotime($val['starttime'] . ' -15 minutes'));
                     
                            $starttime=date('H:i:s',strtotime($val['from_time'] . ' +15 minutes'));
                            $time_after_15=date('h:i a',  strtotime($starttime));
							//echo $currenttimeold.','.$newtime.','.$val['starttime'];
							//echo '<br>';
							if($currenttimeold > $newtime && $currenttimeold < $val['starttime'])
							{
								$style = 'style="background:#78b7ed !important;"';
							} else
							{
								if($val['drop_time'] == "")
								{
									$style = 'style="background:#ffc2b7 !important;"';
                               
								} else {
									if($val['drop_time_new'] > $val['starttime'] && $val['drop_time_new'] < $val['starttimenew'])
									{
										$style = 'style="background:#78b7ed !important;"';
									} else {
										$style = "";
    
									}
								}
							}
                            $sms_button="<a href='javascript:void(0);' onclick='send_delay_sms(\"".$val["mobile_number_1"]."\",\"".$time_after_15."\");' title='Send SMS'><i class='fa fa-mobile' style='font-size:20px;'></i></a>";
                        ?>
                        <tr <?php echo $style; ?>>
                            <td <?php echo $style; ?>><?php echo $sln++; ?></td>
                            <td <?php echo $style; ?>><?php echo $val['customer_name']; ?></td>
                            <td <?php echo $style; ?>><?php echo $val['maid_name']; ?></td>
                            <td <?php echo $style; ?>><?php echo $val['from_time']; ?> - <?php echo $val['to_time']; ?></td>
                            <td <?php echo $style; ?>><?php echo $val['drop_time']; ?><?php echo $val['pick_time']; ?></td>
                            <td <?php echo $style; ?>>
								<?php echo $val['zone_name']; ?> <?php if($val['trans_driver_name'] != ""){ ?>(<?php echo $val['trans_driver_name']; ?>) <?php } ?></td>
                            <td <?php echo $style; ?>><?php echo $val['total_amount']; ?></td>
                            <td <?php echo $style; ?>><?php echo $val['company_name']; ?></td>
							<?php
							if($val['cust_latitude'] != "")
							{
							?>
							<td <?php echo $style; ?>>https://www.google.com/maps/dir/<?php echo $val['tab_latitude']; ?>,<?php echo $val ['tab_longitude']; ?>/<?php echo $val ['cust_latitude']; ?>,<?php echo $val ['cust_longitude']; ?></td>
                         
                            <?php
							} else {
							?>
							<td <?php echo $style; ?>>https://www.google.com/maps/dir/<?php echo $val['tab_latitude']; ?>,<?php echo $val ['tab_longitude']; ?>/<?php echo $val ['customer_address']; ?></td>
							<?php
							}

							?>
                            <!-- <td <?php echo $style; ?>><?php echo  $sms_button; ?></td> -->
							
							<!--<td <?php// echo $style; ?>><a target="_blank" href="<?php// echo base_url(); ?>map?lat=<?php// echo $val['tab_latitude']; ?>&long=<?php// echo $val ['tab_longitude']; ?>"><i class="icon-map-marker"><i></a></td>-->
                        </tr>
                        <?php
                        }
                        ?>
                        
                        
                    </tbody>
                 </table> 