<style>
  .add-leave {
    background-color: #b2d157;
    background-image: linear-gradient(to bottom, #b2d157, #64a434);
    border: 0 none;
    border-radius: 4px;
    color: #fff;
    cursor: pointer;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 14px;
    font-weight: bold;
    margin-right: 20px;
    padding: 6px 15px;
    text-decoration: none;
    text-transform: uppercase;
  }
</style>
<style type="text/css">
  .widget .widget-header {
    margin-bottom: 0px;
  }

  .topiconnew {
    cursor: pointer;
  }
</style>
<!-- Include xlsx library -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.17.5/xlsx.full.min.js"></script>

<!-- Include file-saver library -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/2.0.5/FileSaver.min.js"></script>

<div id="disable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Disable ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure want to disable this leave ?</h3>
      <input type="hidden" id="leave_id" value="">
      <input type="hidden" id="leave_status" value="">
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_disable()">Disable</button>
    </div>
  </div>
</div>
<div class="row m-0">
  <div class="col-sm-12">
    <div class="widget widget-table action-table" style="margin-bottom:30px">
      <div class="widget-header">
        <form id="maid_leave" class="form-horizontal" method="post" action="<?php echo base_url() . 'reports/maid-leave-report' ?>">

          <ul>
            <li><i class="icon-th-list"></i>
              <h3>Staff Leave Reports</h3>

            </li>
            <li>
              <select name="maid_id" id="search-maid-id" class="sel2">
                <option value="0">All Staffs</option>
                <?php
                $maid_id = "";
                foreach ($maids as $maid) {
                  $selected = $maid_id == $maid->maid_id ? 'selected="selected"' : '';
                  echo '<option value="' . $maid->maid_id . '" ' . $selected . '>' . html_escape($maid->maid_name) . '</option>';
                }
                ?>
              </select>

            </li>
            <li>
              <?php
              if (isset($startdate)) {
                $s_date = $startdate;
              } else {
                $s_date = date('d/m/Y');
              }
              if (isset($enddate)) {
                $e_date = $enddate;
              } else {
                $e_date = date('d/m/Y');
              }
              ?>
              <input type="text" name="start_date" id="start-date" class="span3" style="width: 90px;" value="<?php echo $s_date ?>" placeholder="Start date">
            </li>
            <li class="mr-2">
              <input type="text" name="end_date" id="end-date" class="span3" style="width: 90px;" value="<?php echo $e_date ?>" placeholder="End date">

            </li>
            <li>
              <input type="submit" class="n-btn" value="Go" id="searchgo" name="search">
            </li>
            <li>
              <input type="button" class="add-leave n-btn" value="Add Leave" id="add_leave" name="leave" style="display: none;">

            </li>


            <li class="mr-0 float-right">

              <div class="topiconnew border-0 green-btn">
                <a id="printButnforleave" title="Print"> <i class="fa fa-print"></i></a>
              </div>


              <div class="topiconnew border-0 green-btn">
                <a onclick="exportF(this)" title="Download to Excel"> <i class="fa fa-download"></i></a>
              </div>

            </li>
          </ul>
        </form>
      </div>
      <div class="widget-content" style="margin-bottom:30px">
        <div id="LoadingImage" style="text-align:center;display:none;"><img src="<?php echo base_url() ?>img/loader.gif"></div>
        <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th style="line-height: 18px; width: 30px;"> Sl. No.</th>
              <?php if (!@$maid_id) { ?>
                <th style="line-height: 18px; width: 30px;"> Staff</th>
              <?php } ?>
              <th style="line-height: 18px; width: 50px;"> Date</th>
              <th style="line-height: 18px; width: 50px;"> Type</th>
              <th style="line-height: 18px; width: 50px;"> Leave Type</th>
              <th style="line-height: 18px; width: 50px;"> User</th>
              <th style="line-height: 18px; width: 50px;"> Actions </th>
            </tr>
          </thead>
          <tbody>
            <?php

            if (!empty($results)) {
              $i = 0;
              foreach ($results as $lists) {
                if ($lists->typeleaves == "leave") {
                  $leave = "Leave";
                } else if ($lists->typeleaves == "absent") {
                  $leave = "Absent";
                } else if ($lists->typeleaves == "dayoff") {
                  $leave = "Dayoff";
                } else if ($lists->typeleaves == "offset") {
                  $leave = "Offset";
                } else if ($lists->typeleaves == "sick leave") {
                  $leave = "Sick Leave";
                } else if ($lists->typeleaves == "sm_replacement") {
                  $leave = "SM Replacement";
                } else {
                  $leave = "";
                }
                $leavedate = @$lists->leave_date;
                //$originalDate = "2010-03-21";
                $newDate = date("d-m-Y", strtotime($leavedate));

                echo '<tr>'
                  . '<td style="line-height: 18px;">' . ++$i . '</td>';
                if (!@$maid_id) {
                  echo '<td style="line-height: 18px;">' . $lists->maid_name . '</td>';
                }
                echo '<td style="line-height: 18px;">' . $newDate . '</td>'
                  . '<td style="line-height: 18px;">' . @$type[$lists->leave_type] . '</td>'
                  . '<td style="line-height: 18px;">' . $leave . '</td>';
            ?>
                <?php echo '<td style="line-height: 18px;">' . $lists->username . '</td>'; ?>
                <td style="line-height: 18px;"><a href="javascript:void(0);" onclick="confirm_disable_modal(this,<?php echo $lists->leave_id ?>, <?php echo $lists->leave_status ?>);" style="text-decoration:none;"><?php echo $lists->leave_status == 1 ? '<i class="btn-icon-only icon-remove" style="color:red; font-size:25px;"> </i>' : '<i class="btn-icon-only icon-ok" style="font-size:25px;"> </i>' ?></a></td>
            <?php echo '</tr>';
              }
            } else {
              // echo '<tr><td>No Results!</td></tr>';
            }
            ?>
          </tbody>
        </table>
      </div>
      <!-- /widget-content -->

    </div>
    <!-- /widget -->
  </div>
  <!-- /span12 -->
</div>

<!-- For Print -->
<div id="divForPrintLeave" style="display: none">
  <div class="widget-content" style="margin-bottom:30px">
    <table border="1" width="70%" cellspacing="0" cellpadding="0" align="center">
      <thead>
        <tr>
          <th style="line-height: 18px; width: 75px; padding: 4px;"> Sl.NO</th>
          <th style="line-height: 18px; width: 350px; padding: 4px;"> Maid Name</th>
          <th style="line-height: 18px; width: 350px; padding: 4px;"> Date</th>
          <th style="line-height: 18px; width: 175px; padding: 4px;"> Total Full Day</th>
          <th style="line-height: 18px; width: 175px; padding: 4px;">Total Half Day</th>
          <th style="line-height: 18px; width: 175px; padding: 4px;">Total Day</th>
        </tr>
      </thead>
      <tbody>
        <?php
        if ($leaveresults) {
          $i = 0;
          $total = 0;
          foreach ($leaveresults as $leaveresultsrow) {
            $totalday = ($leaveresultsrow->fullday + ($leaveresultsrow->halfday / 2));
            $total += $totalday;
        ?>
            <tr>
              <td style="line-height: 18px; text-align: center; padding: 4px;"><?php echo ++$i; ?></td>
              <td style="line-height: 18px; padding: 4px;"><?php echo $leaveresultsrow->maid_name; ?></td>
              <td style="line-height: 18px; padding: 4px;" class="excel-date"><?php echo date("d-m-Y", strtotime($leaveresultsrow->leave_date)); ?></td>
              <td style="line-height: 18px; text-align: center; padding: 4px;"><?php echo $leaveresultsrow->fullday; ?></td>
              <td style="line-height: 18px; text-align: center; padding: 4px;"><?php echo $leaveresultsrow->halfday; ?></td>
              <td style="line-height: 18px; text-align: center; padding: 4px;"><?php echo $totalday; ?></td>
            </tr>
          <?php
          }
          ?>
          <tr>
            <td style="line-height: 18px; text-align: center; padding: 4px;"></td>
            <td style="line-height: 18px; padding: 4px;"></td>
            <td style="line-height: 18px; padding: 4px;"></td>
            <td style="line-height: 18px; text-align: center; padding: 4px;"></td>
            <td style="line-height: 18px; text-align: center; padding: 4px;">Total :</td>
            <td style="line-height: 18px; text-align: center; padding: 4px;"><?php echo number_format($total, 1); ?></td>
          </tr>
        <?php }
        ?>
      </tbody>
    </table>
  </div>
  <!-- /widget-content -->
</div>
<script type="text/javascript">
  $(document).ready(function() {

    /**==========================================
     * Function to export report as xlsx
    ============================================= */
    function formatDate(dateString) {
      if (dateString && typeof dateString === 'string') {
        var parts = dateString.split('/');
        if (parts.length === 3) {
          var formattedDate = parts[0].padStart(2, '0') + '-' + parts[1].padStart(2, '0') + '-' + parts[2];
          return formattedDate;
        }
      }
      return dateString;
    }

    exportF = function(elem) {

      var maidId = $("#search-maid-id").val();
      var startDate = $("#start-date").val();
      var endDate = $("#end-date").val();

      var formattedStartDate = formatDate(startDate);
      var formattedEndDate = formatDate(endDate);

      // Construct the file name
      var fileName = "Staff_Leave_Report";
      if (maidId !== "0") {
        fileName += "_" + maidId;
      }
      fileName += "_" + formattedStartDate + "_" + formattedEndDate;
      var fileType = "xlsx";
      var table = document.getElementById("divForPrintLeave");
      var wb = XLSX.utils.table_to_book(table, {
        sheet: "Report",
        dateNF: 'dd/mm/yyyy;@',
        cellDates: true,
        raw: true
      });
      const ws = wb.Sheets['Report'];
      var wscols = [{
          wch: 15
        },
        {
          wch: 25
        },
        {
          wch: 30
        },
        {
          wch: 25
        },
        {
          wch: 20
        },
        {
          wch: 20
        },
        {
          wch: 20
        },
      ];
      ws['!cols'] = wscols;
      return XLSX.writeFile(wb, null || fileName + "." + (fileType || "xlsx"));
    };
  });

  var del_fun_this = null;

  function confirm_disable() {
    $.ajax({
      type: "POST",
      url: _base_url + "reports/remove_leave",
      data: {
        leave_id: $('#leave_id').val(),
        leave_status: $('#leave_status').val()
      },
      // dataType: "text",
      // cache: false,
      success: function(result) {
        //window.location.assign(_base_url + 'customers');
        if (result == 1) {
          //$($this).attr('class', 'btn btn-success btn-small');
          $(del_fun_this).html('<i class="btn-icon-only icon-remove" style="color:red; font-size:25px;"></i>');
        } else {
          //$($this).attr('class', 'btn btn-danger btn-small');
          $('#searchgo').trigger('click');
          //$($this).html('<i class="btn-icon-only icon-remove" style="color:red; font-size:25px;"> </i>');
        }
        $(del_fun_this).attr('onclick', 'confirm_disable_modal(this, ' + $('#leave_id').val() + ', ' + result + ')');
      }
    });
  }

  function confirm_disable_modal($this, id, status) {
    del_fun_this = $this;
    $('#leave_id').val(id);
    $('#leave_status').val(status);
    $.fancybox.open({
      autoCenter: true,
      fitToView: false,
      scrolling: false,
      openEffect: 'none',
      openSpeed: 1,
      autoSize: false,
      width: 450,
      height: 'auto',
      helpers: {
        overlay: {
          css: {
            'background': 'rgba(0, 0, 0, 0.3)'
          },
          closeClick: false
        }
      },
      padding: 0,
      closeBtn: false,
      content: $('#disable-popup'),
    });
  }
</script>
