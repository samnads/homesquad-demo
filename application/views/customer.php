<script src="https://maps.googleapis.com/maps/api/js?key=<?= $settings->google_map_api_key; ?>&amp;libraries=places"></script>
<!-- this is replaced from PM -->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/datepicker.css" />
<style>
    #edit-profile label, input, button, select, textarea{font-size: 12px !important;}
</style>
<link rel="stylesheet" href="https://fengyuanchen.github.io/cropperjs/css/cropper.css">
<script src="https://fengyuanchen.github.io/cropperjs/js/cropper.js"></script>
<div id="crop-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Image Cropper</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn" onclick="closeCropper()">&nbsp;</span>
    </div>
    <div id="" class="col-12 p-0">
      <div class="modal-body">
        <div class="img-container">
          <img id="image" src="#">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeCropper()">Cancel</button>
        <button type="button" class="n-btn mb-0" id="crop">Crop</button>
      </div>
    </div>
  </div>
</div>
<div class="row m-0">
    <div class="col-sm-12">
        <div class="widget">

             <!-- /widget-header -->





            <div class="widget-header">
     <div class="book-nav-top">
            <ul>
              <li><i class="icon-th-list"></i><h3>New Customer</h3></li>





              <li class="mr-0 float-right">
                  <div class="topiconnew border-0 green-btn">
                      <a href="<?php echo base_url(); ?>customers" title="Maid List"> <i class="fa fa-users"></i></a>
                 </div>
              </li>



              <div class="clear"></div>
            </ul>
     </div>
</div>
















            <div class="widget-content">
                <div class="tabbable">
                    <ul class="nav nav-tabs" id="mytabs">
                        <li class="active" id="firstli"><a href="#personal" data-toggle="tab">Personal Details</a></li>
                        <li id="secondli"><a href="#address-details" data-toggle="tab">Address Details</a></li>
                        <li id="thirdli"><a href="#account" data-toggle="tab">Account and Other Details</a></li>
                    </ul>
                    <br>
                    <form id="edit-profile" class="form-horizontal" method="post">
                        <div class="tab-content">
                            <div class="tab-pane active" id="personal">
                                <!--                                                <form id="edit-profile" class="form-horizontal">                                                                                                    -->

                                <?php
if ($message == "success") {?>
                                    <div class="control-group">
                                        <label class="control-label"></label>
                                            <div class="controls">
                                                <div class="alert alert-success">
                                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                    <strong>Success!</strong> Customer added Successfully.
                                                </div>
                                            </div>
                                    </div>
                               <?php
}
?>



                                <fieldset>
                                    <div class="col-sm-4">

                                                <fieldset>



                                                    <div class="row m-0 n-field-main">
                                                        <p>Customer Name</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="text" name="customer_name" class="" value="<?=$this->input->post('customer_name')?>" id="customer_name" onchang="document.getElementById('customer_nick').value = this.value">
                                                        </div>
                                                    </div>










                                                    <div class="row m-0 n-field-main">
                                                        <p>Customer Nick Name</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="text" class="" value="<?=$this->input->post('customer_nick')?>" id="customer_nick" name="customer_nick">
                                                        </div>
                                                    </div>










                                                    <div class="row m-0 n-field-main">
                                                        <p>Is Flagged </p>
                                                        <div class="col-sm-12 p-0 n-field-box">


                                            <div class="row m-0">
                                                  <div class="col-sm-6 n-radi-check-main p-0">
                                                       <input type="radio" value="Y" id="flag" name="flag" <?php echo isset($_POST['flag']) && $_POST['flag'] == 'Y' ? 'checked' : ''; ?> class="">
                                                       <label for="flag"> <span class="border-radius-3"></span> <p>Yes</p></label>
                                                  </div>
                                                  <div class="col-sm-6 n-radi-check-main p-0">
                                                        <input type="radio" value="N" id="flag1" name="flag" <?php echo isset($_POST['flag']) && $_POST['flag'] == 'N' ? 'checked' : ''; ?> class="" checked="">
                                                        <label for="flag1"> <span class="border-radius-3"></span> <p>No</p></label>
                                                  </div>
                                            </div>

                                                        </div>
                                                    </div>









                                                    <div class="row m-0 n-field-main">
                                                        <p>Reason For Flagging</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                        <textarea class="" rows="1" id="reasonflag" name="reasonflag"><?php echo isset($_POST['reasonflag']) ? htmlspecialchars($_POST['reasonflag']) : ''; ?></textarea>
                                                        </div>
                                                    </div>









                                                 <div class="row m-0 n-field-main">
                                                      <p>Mobile Number 1</p>
                                                      <div class="col-sm-12 p-0 n-field-box">
                                                           <input type="text" class=""  value="<?=$this->input->post('mobile_number1')?>" id="mobile_number1" name="mobile_number1">
                                                        <?php echo form_error('mobile_number1'); ?>
                                                      </div>
                                                  </div>









                                                <div class="row m-0 n-field-main">
                                                        <p>Mobile Number 2</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                        <input type="text" class="" value="<?=$this->input->post('mobile_number2')?>" id="mobile_number2" name="mobile_number2">
                                                        <?php echo form_error('mobile_number2'); ?>
                                                        </div>
                                                    </div>









                                                <div class="row m-0 n-field-main">
                                                        <p>Mobile Number 3</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="text" class="" value="<?=$this->input->post('mobile_number3')?>"  id="mobile_number3" name="mobile_number3">
                                                        <?php echo form_error('mobile_number3'); ?>
                                                        </div>
                                                    </div>



                                                    <div class="row m-0 n-field-main">
                                                        <p>WhatsApp No.</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                              <input type="text" class="" value="<?=$this->input->post('whatsapp_no_1')?>" id="whatsapp_no_1" name="whatsapp_no_1">
                                                        </div>
                                                    </div>



















                                                </fieldset>



                                    </div>


                                    <div class="col-sm-4">



                                                <div class="row m-0 n-field-main">
                                                        <p>Phone</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="text" class="" value="<?=$this->input->post('phone')?>" id="phone" name="phone">
                                                        <?php echo form_error('phone'); ?>
                                                        </div>
                                                    </div>









                                                <div class="row m-0 n-field-main">
                                                        <p>Fax</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="text" class="" value="<?=$this->input->post('fax')?>" id="fax" name="fax">
                                                        </div>
                                                    </div>




                                                <div class="row m-0 n-field-main">
                                                        <p>TRN#</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="text" class="" value="<?=$this->input->post('trnnumber')?>" id="trnnumber" name="trnnumber">
                                                        </div>
                                                    </div>










                                                <div class="row m-0 n-field-main">
                                                        <p>VAT#</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="text" class="" value="<?=$this->input->post('vatnumber')?>" id="vatnumber" name="vatnumber">
                                                        </div>
                                                    </div>












                                                    <div class="row m-0 n-field-main">
                                                        <p>Source</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <select name="customer_source" id="customer_source" class="sel2">
                                                                <option value="">-- Select --</option>
                                                                <?php foreach($customer_sources as $customer_source): ?>
                                                                    <?php 
                                                                    $selected = '';
                                                                    if(isset($_POST['customer_source']) && $_POST['customer_source'] == $customer_source['source']) {
                                                                        $selected = 'selected';
                                                                    }
                                                                ?>
                                                                <option value="<?= $customer_source['source'] ?>" <?= $selected ?>><?= $customer_source['source'] ?></option>
                                                            <?php endforeach ?>
                                                            </select>
                                                        </div>
                                                    </div>








                                                    <div class="row m-0 n-field-main">
                                                        <p>Source Reference</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="text" class="" value="<?=$this->input->post('customer_source_others_val')?>" id="customer_source_others_val" name="customer_source_others_val">
                                                        </div>
                                                    </div>









                                                    <div class="row m-0 n-field-main" id="customer_source_reference"style="display: none;">
                                                        <p>Source Reference</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <select name="customer_source_id" id="customer_source_id" class="" >
                                                                <option value="">-- Select Maids --</option>
                                                                <?php
if (!empty($maids) > 0) {
    foreach ($maids as $maid) {
        ?>
                                                                        <option value="<?php echo $maid['maid_id']; ?>"><?php echo $maid['maid_name']; ?></option>

                                                                        <?php
}
}
?>
                                                            </select>
                                                        </div>
                                                    </div>



                                          <div class="row m-0 n-field-main">
                                                        <p>Customer Type</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <select name="customer_type" class="" required >
                                                             <option value="HO" <?php echo isset($_POST['customer_type']) && $_POST['customer_type'] == 'HO' ? 'selected="selected"' : ''; ?>>Home</option>
                                                            <option value="OF" <?php echo isset($_POST['customer_type']) && $_POST['customer_type'] == 'OF' ? 'selected="selected"' : ''; ?>>Office</option>
                                                            <option value="WH" <?php echo isset($_POST['customer_type']) && $_POST['customer_type'] == 'WH' ? 'selected="selected"' : ''; ?>>Warehouse</option>
                                                            </select>
                                                        </div>
                                                    </div>











                                                <div class="row m-0 n-field-main">
                                                        <p>Is Company</p>
                                                        <div class="col-sm-12 p-0 n-field-box">


                                             <div class="row m-0">
                                                  <div class="col-sm-6 n-radi-check-main p-0">
                                                       <input type="radio" value="Y" id="company" name="company" class="" <?php echo isset($_POST['company']) && $_POST['company'] == 'Y' ? 'checked' : ''; ?>>
                                                       <label for="company"> <span class="border-radius-3"></span> <p>Yes</p></label>
                                                  </div>
                                                  <div class="col-sm-6 n-radi-check-main p-0">
                                                        <input type="radio" value="N" id="company1" name="company" class="" checked="" <?php echo isset($_POST['company']) && $_POST['company'] == 'N' ? 'checked' : ''; ?>>
                                                        <label for="company1"> <span class="border-radius-3"></span> <p>No</p></label>
                                                  </div>
                                            </div>


                                                        </div>
                                                    </div>



                                    </div>

                                    <div class="col-sm-4">











                                                <div class="row m-0 n-field-main">
                                                        <p>Select Company</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <select name="company_name" id="company_name" class="">
                                                            <option value="">-Select-</option>
															<option value="Live In" <?php echo isset($_POST['company_name']) && $_POST['company_name'] == 'Live In' ? 'selected' : ''; ?>>Live In</option>
                                                            <option value="Live Out" <?php echo isset($_POST['company_name']) && $_POST['company_name'] == 'Live Out' ? 'selected' : ''; ?>>Live Out</option>
                                                            <option value="Rizek" <?php echo isset($_POST['company_name']) && $_POST['company_name'] == 'Rizek' ? 'selected' : ''; ?>>Rizek</option>
                                                            <option value="Justmop" <?php echo isset($_POST['company_name']) && $_POST['company_name'] == 'Justmop' ? 'selected' : ''; ?>>Justmop</option>
                                                            <option value="Matic" <?php echo isset($_POST['company_name']) && $_POST['company_name'] == 'Matic' ? 'selected' : ''; ?>>Matic</option>
                                                            <option value="ServiceMarket" <?php echo isset($_POST['company_name']) && $_POST['company_name'] == 'ServiceMarket' ? 'selected' : ''; ?>>ServiceMarket</option>
                                                            <option value="Helpling" <?php echo isset($_POST['company_name']) && $_POST['company_name'] == 'Helpling' ? 'selected' : ''; ?>>Helpling</option>
                                                            <option value="Urban Clap" <?php echo isset($_POST['company_name']) && $_POST['company_name'] == 'Urban Clap' ? 'selected' : ''; ?>>Urban Clap</option>
                                                            <option value="Emaar" <?php echo isset($_POST['company_name']) && $_POST['company_name'] == 'Emaar' ? 'selected' : ''; ?>>Emaar</option>
                                                            <option value="MyHome" <?php echo isset($_POST['company_name']) && $_POST['company_name'] == 'MyHome' ? 'selected' : ''; ?>>MyHome</option>
                                                        </select>
                                                        </div>
                                                    </div>













                                                <div class="row m-0 n-field-main">
                                                        <p>Contact Person</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="text" class="" value="<?=$this->input->post('contact_person');?>"  id="contact_person" name="contact_person">
                                                        </div>
                                                    </div>










                                                <div class="row m-0 n-field-main">
                                                        <p>Website URL</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="text" class="" value="<?=$this->input->post('website');?>" id="website" name="website">
                                                        </div>
                                                    </div>



















                                                <div class="row m-0 n-field-main">
                                                        <p>Email Address</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="email" class="" value="<?=$this->input->post('email');?>" id="email" name="email" >
                                                             <?php echo form_error('email'); ?>
                                                            </div>
                                                    </div>



<div class="row m-0 n-field-main">
                                                        <p>User Name</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="text" class="" id="user_name" value="<?=$this->input->post('user_name');?>" name="user_name" autocomplete="off">
                                                        </div>
                                                    </div>




                                                <div class="row m-0 n-field-main">
                                                        <p>Password</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="password" class="" id="password1" value="<?=$this->input->post('password');?>" name="password" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');">
                                                        </div>
                                                    </div>










                                                <div class="row m-0 n-field-main">
                                                        <p>Notes</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                        <textarea class="" rows="5" id="notes" name="notes"><?php echo isset($_POST['notes']) ? htmlspecialchars($_POST['notes']) : ''; ?></textarea>
                                                        </div>
                                                    </div>








                                    </div>

                                </fieldset>
                                <div class="col-sm-12"><button type="button" class="n-btn" id="clickfirst">Next</button></div>
                            </div>
                            <div class="tab-pane" id="address-details">
                                <fieldset>
                                    <div class="col-sm-4">




                                                <div class="row m-0 n-field-main">
                                                        <p>Area</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <select name="area[]" id="area" class=" area_customer" style="min-width: 270px;" required >
                                                             <option value="" <?php echo !isset($_POST['area']) ? 'selected' : ''; ?>>Select Area</option>
                                                                <?php
                                                                if (count($areas) > 0) {
                                                                    foreach ($areas as $areasVal) {
                                                                        ?>
                                                                        <option value="<?php echo $areasVal['area_id'] ?>" <?php echo isset($_POST['area']) && in_array($areasVal['area_id'], $_POST['area']) ? 'selected' : ''; ?>><?php echo $areasVal['area_name'] ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>

                                                            </select>
                                                        </div>
                                                    </div>








                                                <div class="row m-0 n-field-main">
                                                        <p>Apartment No</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                              <input id="apartment_no" type="text" name="apartment_no[]" class="" style="min-width: 270px;"/>
                                                        </div>
                                                    </div>








                                                 <div class="row m-0 n-field-main">
                                                        <p>Address</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                              <input id="address" type="text" name="address[]" onkeyup="loadLocationField('address','1');" class="" style="min-width: 270px;"/>
                                                           <input type="hidden" name="lat[]" id="latitude_1" value=""/>
                                                           <input type="hidden" name="lng[]" id="longitude_1" value=""/>
                                                        </div>
                                                    </div>












                                                    <div class="row m-0 n-field-main">
                                                        <div id="map_canvas_1"></div>
                                                        <div class="input_fields_wrap"></div>

                                                       <!-- <div class="col-sm-12 p-0 n-field-box">
                                                             <a style="float:right ; margin-right:200px; cursor:pointer;" class="add_field_button">Add More Address</a>
                                                        </div>-->


                                                        <div class="row n-field-main m-0 pt-3">
                                              <p class="text-right"><a class="add_field_button n-btn n-btn-sml">Add More Address</a></p>
                                         </div>
                                                    </div>







                                            </div>



                                 </fieldset>
                                <div class="col-sm-12"><button type="button" class="n-btn" id="clicksecond">Next</button></div>
                              </div>

                            <div class="tab-pane" id="account">

                                <fieldset>
                                    <div class="col-sm-4">

                                                <fieldset>



                                                    <div class="row m-0 n-field-main">
                                                        <p>Customer Book Type</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <select name="customers_type" id="customers_type" class="" required >
                                                                <option value="0" selected="selected">Non Regular</option>
                                                                <option value="1">Regular</option>
                                                            </select>
                                                        </div>
                                                    </div>











                                                    <div class="row m-0 n-field-main">
                                                        <p>Payment Type</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <select name="payment_type" id="payment_type" class="" required >
                                                                <option value="D" selected="selected">Daily Paying</option>
                                                                <option value="W">Weekly Paying</option>
                                                                <option value="M">Monthly Paying</option>
                                                                <option value="MA">Monthly Advance</option>
                                                            </select>
                                                        </div>
                                                    </div>











                                                    <div class="row m-0 n-field-main">
                                                        <p>Payment Mode</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <select name="payment_mode" id="payment_mode" class="" required >
                                                                <option value="Cash" selected="selected">Cash</option>
                                                                <option value="Cheque">Cheque</option>
                                                                <option value="Online">Online</option>
																<option value="Credit Card">Credit Card</option>
                                                            </select>
                                                        </div>
                                                    </div>










                                                    <div class="row m-0 n-field-main">
                                                        <p>Hourly</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="text" class="" id="hourly" name="hourly" value="35">
                                                        </div>
                                                    </div>










                                                    <div class="row m-0 n-field-main">
                                                        <p>Extra</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="text" class="" id="extra" name="extra" value="35">
                                                        </div>
                                                    </div>











                                                    <div class="row m-0 n-field-main">
                                                        <p>Weekend</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="text" class="" id="weekend" name="weekend" value="35">
                                                        </div>
                                                    </div>







                                                </fieldset>


                                    </div>


                                    <div class="col-sm-4">
                                        <div id="target-2" class="">

                                                <fieldset>



                                                    <div class="row m-0 n-field-main">
                                                        <p>Latitude</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="text" class="" id="latitude" name="latitude">
                                                        </div>
                                                    </div>











                                                    <div class="row m-0 n-field-main">
                                                        <p>Longitude</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="text" class="" id="longitude" name="longitude">
                                                        </div>
                                                    </div>

                                                    <!--<div class="row m-0 n-field-main">
                                                        <p>Opening Balance</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="text" class="" id="initial_balance" name="initial_balance">
                                                        </div>
                                                    </div>
													
													<div class="row m-0 n-field-main">
                                                        <p>Balance Till</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="text" class="" id="vehicle_date" name="initial_balance_date">
                                                        </div>
                                                    </div>

                                                    <div class="row m-0 n-field-main">
                                                        <p>Opening Balance Type</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <select name="initial_bal_sign" id="initial_bal_sign" class="" required >
                                                                <option value="Cr" selected="selected">Credit</option>
                                                                <option value="Dr">Debit</option>
                                                            </select>
                                                        </div>
                                                    </div>-->










                                                    <div class="row m-0 n-field-main">
                                                        <p>Key</p>
                                                        <div class="col-sm-12 p-0 n-field-box">



                                            <div class="row m-0">
                                                  <div class="col-sm-6 n-radi-check-main p-0">
                                                       <input type="radio" value="Y" id="key" name="key" class="">
                                                       <label for="key"> <span class="border-radius-3"></span> <p>Yes</p></label>
                                                  </div>
                                                  <div class="col-sm-6 n-radi-check-main p-0">
                                                        <input type="radio" value="N" id="key1" name="key" class="" checked="">
                                                        <label for="key1"> <span class="border-radius-3"></span> <p>No</p></label>
                                                  </div>
                                            </div>
                                                        </div>
                                                    </div>









                                                    <div class="row m-0 n-field-main">
                                                        <p>Rating Mail</p>
                                                        <div class="col-sm-12 p-0 n-field-box">




                                                            <div class="row m-0">
                                                  <div class="col-sm-6 n-radi-check-main p-0">
                                                       <input type="radio" value="Y" id="rating_mail_stat" name="rating_mail_stat" class="" checked="">
                                                       <label for="rating_mail_stat"> <span class="border-radius-3"></span> <p>Yes</p></label>
                                                  </div>
                                                  <div class="col-sm-6 n-radi-check-main p-0">
                                                        <input type="radio" value="N" id="rating_mail_stat1" name="rating_mail_stat" class="">
                                                        <label for="rating_mail_stat1"> <span class="border-radius-3"></span> <p>No</p></label>
                                                  </div>
                                            </div>
                                                        </div>
                                                    </div>









                                                    <!--<div class="row m-0 n-field-main">
                                                        <p>Upload Photo</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <div id="me" class="styleall" style=" cursor:pointer;">
                                                                <span style=" cursor:pointer;">
                                                                    <img src="<?php echo base_url(); ?>img/profile_pic.jpg" style="height: 100px; width: 100px"/>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>-->

                                                    <div class="row m-0 n-field-main">
                                                                <p>Profile Photo</p>
                                    <label>
                                        <img id="avatar_img" src="<?php echo base_url(DEFAULT_AVATAR); ?>" style="width:100px;">
                                            <input type="file" class="sr-only" id="input-image" accept="image/*">
                                    </label>
                                    <input type="hidden" name="avatar_base64" id="avatar_base64">
                                    </div>








                                                    <br />
                                                </fieldset>

                                        </div>
                                    </div>
                                </fieldset>

                                <div class="col-sm-12">
                                    <input type="hidden" name="call_method" id="call_method" value="customer/customerimgupload"/>
                                    <input type="hidden" name="img_fold" id="img_fold" value="customer_img"/>
                                    <input type="hidden" name="img_name_resp" id="img_name_resp"/>
                                    <input type="submit" class="n-btn" value="Save" name="customer_sub" onclick="return validate_customer();">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div> <!-- /widget-content -->
        </div> <!-- /widget -->
    </div> <!-- /span8 -->
</div> <!-- /row -->
<script>
$(function() {
	let current = window.location.href;
	$('#primary_nav_wrap li a').each(function() {
		var $this = $(this);
		// if the current path is like this link, make it active
		if ($this.attr('href') === _base_url+'customers') {
			$this.addClass('active');
		}
	})
})
var cropper;
function showCropper() {
	cropper = new Cropper(image, {
		aspectRatio: 300 / 300,
		viewMode: 1,
	});
	$.fancybox.open({
		autoCenter: true,
		fitToView: false,
		scrolling: false,
		openEffect: 'none',
		openSpeed: 1,
		autoSize: false,
		width: 450,
		height: 'auto',
		helpers: {
			overlay: {
				css: {
					'background': 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding: 0,
		closeBtn: false,
		content: $('#crop-popup'),
	});
}

function closeCropper() {
	cropper.destroy();
	cropper = null;
	$.fancybox.close();
}
window.addEventListener('DOMContentLoaded', function() {
	var avatar_img = document.getElementById('avatar_img');
	var image = document.getElementById('image');
	var input = document.getElementById('input-image');
	input.addEventListener('change', function(e) {
		var files = e.target.files;
		var done = function(url) {
			input.value = '';
			image.src = url;
			showCropper();
		};
		var reader;
		var file;
		var url;
		if (files && files.length > 0) {
			file = files[0];
			if (URL) {
				done(URL.createObjectURL(file));
			} else if (FileReader) {
				reader = new FileReader();
				reader.onload = function(e) {
					done(reader.result);
				};
				reader.readAsDataURL(file);
			}
		}
	});
	document.getElementById('crop').addEventListener('click', function() {
		var initialpromo_imgURL;
		var canvas;
		if (cropper) {
			canvas = cropper.getCroppedCanvas({
				width: 300,
				height: 300,
			});
			initialpromo_imgURL = avatar_img.src;
			avatar_img.src = canvas.toDataURL();
			$('#avatar_base64').val(avatar_img.src);
		}
		closeCropper();
	});
});
</script>