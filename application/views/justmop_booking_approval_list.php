<div class="row m-0">      
    <div class="col-md-12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> 
                
                <form class="form-horizontal mm-drop" method="post">
                <ul>
                <li>
                    <i class="icon-th-list"></i>
                    <h3>Booking Approval List</h3>         
                 
                 </li>
                 <li class="mr-2">
                    <select name="filter" id="filter" class="span3" required style="width: 160px;">
                        <option value="All" <?php echo $filter == 'All' ? 'selected="selected"' : '' ?>>All</option>
                        <option value="Pending" <?php echo $filter == 'Pending' ? 'selected="selected"' : '' ?>>Pending</option>
                        <option value="Expired" <?php echo $filter == 'Expired' ? 'selected="selected"' : '' ?>>Missed</option>
                        <option value="Assigned" <?php echo $filter == 'Assigned' ? 'selected="selected"' : '' ?>>Assigned</option>
                        <option value="Deleted" <?php echo $filter == 'Deleted' ? 'selected="selected"' : '' ?>>Deleted</option>
                    </select>
                    
                    </li>
                 <li>
                    <input type="text" style="width: 160px;" id="vehicle_date" name="service_date" value="<?php echo $service_date ?>">
                    
                    </li>
                 <li>
                    <input type="submit" class="n-btn" id="deleteassignmaidfilter" value="Go" name="vehicle_report">                    
                    </li>
                 </ul>
                </form> 
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 10px;"><center> Sl.No.</center> </th>
                            <th style="line-height: 10px;"> <center>Customer </center></th>
                            <th style="line-height: 10px; width: 150px !important; overflow-wrap: break-word; word-break: break-all;"> <center>Address</center></th>
                            <th style="line-height: 10px;"> <center>Day</center></th>
                            <th style="line-height: 20px;"> <center>Requested Maid</center></th>
<!--                             <th style="line-height: 20px; width: 10px !important;"> <center>Cleaning Material</center></th>-->
                            <th style="line-height: 10px;"> <center>Shift</center></th>
                            <!--<th style="line-height: 10px;"> <center>Added</center></th>-->
<!--                            <th style="line-height: 20px;"> <center>Booked From</center></th>-->
							<!--<th style="line-height: 20px;"> <center>Old Ref</center></th>-->
                            <th style="line-height: 20px;"> <center>JustMop Ref</center></th>
							<th style="line-height: 20px;"> <center>MOP</center></th>
							<th style="line-height: 20px; width: 150px !important;"> <center>Notes</center></th>
<th style="line-height: 10px; width: 150px !important;" class="td-actions"><center><?php if($filter == 'Deleted'){ echo 'Remarks'; } else { echo 'Actions'; } ?></center></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(empty($approval_list)){
                            ?>
                        <tr><td colspan="9"  style="line-height: 20px; text-align:justify;">No records found</td></tr>
                                <?php
                        }
                            $i = 0;
                            $attribute = $filter == 'Assigned' || $filter == 'Expired' ? 'disabled' : ''; 
                            foreach ($approval_list as $booking) 
                            {
                                //Payment Type
                                    if($booking->payment_type == "D")
                                    {
                                        $paytype = "(D)";
                                    } else if($booking->payment_type == "W")
                                    {
                                        $paytype = "(W)";
                                    } else if($booking->payment_type == "M")
                                    {
                                        $paytype = "(M)";
                                    } else
                                    {
                                        $paytype = "";
                                    }
                                     if($booking->cleaning_material == "Y")
                                    {
                                     $request_clean_material=" + <span style='color:green';><br>cleaning material</span>";   
                                    }
                                    else
                                    {
                                      $request_clean_material="";     
                                    }
									
									$address_detail = wordwrap($booking->customer_address, 20, '<br>');
                                   
                                   echo '<tr>'
                                           . '<td style="line-height: 20px; text-align:center;">' . ++$i . '</td>'
                                           . '<td style="line-height: 20px; text-align:left;">' . $booking->customer_name.$paytype. '<br /> ' . ($booking->phone_number ? $booking->phone_number : ($booking->mobile_number_1 ? $booking->mobile_number_1 : ($booking->mobile_number_2 ? $booking->mobile_number_2 : $booking->mobile_number_3))) . '</td>'
                                           . '<td style="line-height: 20px; width: 150px !important; text-align:left; overflow-wrap: break-word; word-break: break-all;">' . $address_detail . '<br />' . $booking->area_name.'<br />'. $booking->zone_name . '</td>'
                                           . '<td style="line-height: 20px; text-align:left;">' . $booking->service_date . ' <br /> ' . $booking->weekday . ' ( '. $booking->booking_type . ')</td>'
                                           . '<td style="line-height: 20px; text-align:center;">' . $booking->maid_name . '('. $booking->no_of_maids .')' . $request_clean_material.'</td>'
                               
                                           . '<td style="line-height: 20px; text-align:left;">' . $booking->shift . '</td>'
                                           //. '<td style="line-height: 20px; text-align:left;">' . $booking->booked_datetime . '</td>'
//                                           . '<td  style="line-height: 20px; text-align:left;"><span class="book_source_'.$source[$booking->booked_from].'">' . $source[$booking->booked_from] . '</span></td>'
                                           //. '<td  style="line-height: 20px; text-align:left;">' . $booking->justmop_reference . '</td>'
										   . '<td  style="line-height: 20px; text-align:left;">' . $booking->just_mop_ref . '</td>'
										   . '<td  style="line-height: 20px; text-align:left;">' . $booking->mop . '</td>'
										   . '<td  style="line-height: 20px; text-align:left; width: 150px !important; text-align:left; overflow-wrap: break-word; word-break: break-all;">' . $booking->booking_note . '</td>'
                                           . '<td vstyle="line-height: 20px;" class="td-actions" style="padding: 7px 10px;">';
//                                           . '<input type="button" ' . $attribute . ' onclick="assignMaid(' . $booking->booking_id . ', ' . $booking->no_of_maids . ')" class="save-but" value="Assign Maid" style="font-size:9px; line-height:12px;" />'
//                                           . '<input type="button" ' . $attribute . ' onclick="deleteBooking(' . $booking->booking_id . ')" class="delete-but" value="Delete"  style="font-size:9px; line-height:12px;"/>'
                                   if($booking->booking_status == '0' ) {
                                   
                                       
                                   //echo '<input type="button" ' . $attribute . ' onclick="Maid_booking(' . $booking->booking_id . ', ' . $booking->no_of_maids . ', ' . $booking->customer_id. ')" class="save-but" value="Assign" style="font-size:9px; line-height:12px; margin-right: 0px!important;" />';
                                   echo '<input type="button" ' . $attribute . ' onclick="Maid_booking_view(' . $booking->booking_id . ', ' . $booking->no_of_maids . ', ' . $booking->customer_id. ')" class="delete-but" value="Assign"  style="font-size:9px; line-height:14px; float:left; margin: 0 4px; padding: 5px 12px !important;"/>';
                                   //if(user_authenticate() == 1)
                                   //{
                                   echo '<span class="btn btn-danger" onclick="delete_booking(' . $booking->booking_id . ',' . $booking->customer_id. ')" style="margin-right: 0px ! important; float: left; padding: 0px 4px;"><i class="btn-icon-only icon-remove" style="font-size:9px; line-height:12px; margin-right: 0px!important;"> </i></span>';
                                   //}
                                   //echo '<input type="button" ' . $attribute . ' onclick="delete_booking(' . $booking->booking_id . ',' . $booking->customer_id. ')" class="btn btn-danger" value="Delete" style="font-size:9px; line-height:12px; margin-right: 0px!important;" />';
                                   } else if($booking->booking_status == '2' )
                                   {
                                   echo $booking->delete_remarks;
                                   }   else {
                                       
                                   echo '<input type="button" ' . $attribute . '  class="save-but" value="Assigned" style="font-size:9px; line-height:12px; margin-right: 5px !important;" />';
//                                   echo '<span class="not-started">Assigned</span>';
                                   if(user_authenticate() == 1)
                                   {
                                   echo '<span class="btn btn-danger" onclick="delete_booking(' . $booking->booking_id . ',' . $booking->customer_id. ')" style="margin-right: 0px ! important; float: left; padding: 0px 4px;"><i class="btn-icon-only icon-remove" style="font-size:9px; line-height:12px; margin-right: 0px!important;"> </i></span>';
                                   }
                                    //echo '<input type="button" ' . $attribute . ' onclick="delete_booking(' . $booking->booking_id . ',' . $booking->customer_id. ')" class="btn btn-danger" value="Delete" style="font-size:9px; line-height:12px; margin-right: 0px!important;" />';
                                   }    
                                           
                                      echo '</td>'
                                        . '</tr>';
                                
                            }
                        
                        ?>  
                                

                    </tbody>
                </table>
            </div>            
        </div>        
    </div>    
</div>



<div id="free-maid-list-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 style="color:white; padding-bottom: 0px !important;">Free Maid List</h3>
    </div>
    <div class="modal-body">
           
        
            <div class="controls">
                
                <label class="radio inline">
                    <input type="radio" name="same_zone" value="0" checked="checked"> All
                </label>
                <label class="radio inline">
                    <input type="radio" name="same_zone" value="1"> Same Zone
                </label>


            </div>
        <p id="frm-transfer"> 
                <select id="free-maid-id" data-placeholder="Select service type" class="sel2">
                    <option value="">Select Maid</option>
                    
                </select>
        </p>
        <p id="no-maids-selected"></p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <a class="save-but" style="text-decoration: none;" href="javascript:void" id="assign-maid">Submit</a>
    </div>
</div>