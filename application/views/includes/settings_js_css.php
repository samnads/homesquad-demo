<script type="application/javascript">
    var _base_url = '<?php echo base_url(); ?>';
    var _page_url = '<?php echo current_url(); ?>';
    /* settings */
    var color_bg_booking_od         = '<?= $settings->color_bg_booking_od; ?>';
    var color_bg_booking_we         = '<?= $settings->color_bg_booking_we; ?>';
    var color_bg_booking_bw         = '<?= $settings->color_bg_booking_bw; ?>';
    var od_booking_text             = '<?= $settings->od_booking_text; ?>';
    var we_booking_text             = '<?= $settings->we_booking_text; ?>';
    var bw_booking_text             = '<?= $settings->bw_booking_text; ?>';
    var service_vat_percentage      =  <?= $settings->service_vat_percentage; ?>;
</script>