<div id="delete-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Delete ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to delete this staff ?</h3>
      <input type="hidden" id="delete_id">
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_delete()">Delete</button>
    </div>
  </div>
</div>
<div class="row m-0">
    <div class="col-sm-12">
        <div class="widget ">
            <div class="widget-header mb-0">
            <ul>
            <li>
                <i class="icon-user"></i>
                <h3>Staff Details</h3>

</li>




<li class="mr-0 float-right">

                <div class="topiconnew border-0 green-btn"> <a href="<?php echo base_url(); ?>maids" title="Maid List"> <i class="fa fa-users"></i></a> </div>


                <div class="topiconnew border-0 green-btn"> <a href="<?php echo base_url(); ?>maid/edit/<?php echo $link_maid_id; ?>" title="Edit Maid"> <i class="fa fa-pencil"></i></a> </div>

</li>
</ul>


            </div> <!-- /widget-header -->
            <div class="widget-content">
                <div class="tabbable">




                    <form id="edit-profile" class="form-horizontal" method="post">
                        <div class="tab-content">

                            <?php
foreach ($maid_details as $maid_val) {
    ?>

                                <div class="tab-pane active" id="personal">
                                    <fieldset>

                                            <div class="col-sm-12">

                                                        <?php
    if ($maid_val['maid_gender'] == "F") {
        $gender = "Female";
    } else if ($maid_val['maid_gender'] == "M") {
        $gender = "Male";
    }
    if ($maid_val['maid_passport_expiry_date'] != '' && $maid_val['maid_passport_expiry_date'] != '0000-00-00') {
        list($year, $month, $day) = explode("-", $maid_val['maid_passport_expiry_date']);
        $passport_expiry = "$day/$month/$year";
    } else if ($maid_val['maid_passport_expiry_date'] = '0000-00-00') {
        $passport_expiry = "";
    }
    if ($maid_val['maid_passport_file'] != '') {
        $passport_image = base_url() . "maid_passport/" . $maid_val['maid_passport_file'];
    } else {
        $passport_image = base_url() . "img/no_image.jpg";
    }
    if ($maid_val['maid_visa_expiry_date'] != '' && $maid_val['maid_visa_expiry_date'] != '0000-00-00') {
        list($year, $month, $day) = explode("-", $maid_val['maid_visa_expiry_date']);
        $visa_expiry = "$day/$month/$year";
    } else if ($maid_val['maid_visa_expiry_date'] = '0000-00-00') {
        $visa_expiry = "";
    }
    if ($maid_val['maid_visa_file'] != '') {
        $visa_image = base_url() . "maid_visa/" . $maid_val['maid_visa_file'];
    } else {
        $visa_image = base_url() . "img/no_image.jpg";
    }
    if ($maid_val['maid_labour_card_expiry_date'] != '' && $maid_val['maid_labour_card_expiry_date'] != '0000-00-00') {
        list($year, $month, $day) = explode("-", $maid_val['maid_labour_card_expiry_date']);
        $labour_expiry = "$day/$month/$year";
    } else if ($maid_val['maid_labour_card_expiry_date'] = '0000-00-00') {
        $labour_expiry = "";
    }
    if ($maid_val['maid_labour_card_file'] != '') {
        $labour_image = base_url() . "maid_labour/" . $maid_val['maid_labour_card_file'];
    } else {
        $labour_image = base_url() . "img/no_image.jpg";
    }
    if ($maid_val['maid_emirates_expiry_date'] != '' && $maid_val['maid_emirates_expiry_date'] != '0000-00-00') {
        list($year, $month, $day) = explode("-", $maid_val['maid_emirates_expiry_date']);
        $emirates_expiry = "$day/$month/$year";
    } else if ($maid_val['maid_emirates_expiry_date'] = '0000-00-00') {
        $emirates_expiry = "";
    }
    if ($maid_val['maid_emirates_file'] != '') {
        $emirates_image = base_url() . "maid_labour/" . $maid_val['maid_labour_card_file'];
    } else {
        $emirates_image = base_url() . "img/no_image.jpg";
    }

    if ($maid_val['maid_joining'] != '' && $maid_val['maid_joining'] != '0000-00-00') {
        list($year, $month, $day) = explode("-", $maid_val['maid_joining']);
        $doj = "$day/$month/$year";
    } else if ($maid_val['maid_joining'] = '0000-00-00') {
        $doj = "";
    }
    ?>

                                                        <table class="table table-striped table-bordered">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="line-height: 20px; width: 200px"><b>Name</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $maid_val['maid_name'] ?></td>
                                                                    <td style="line-height: 20px; width: 200px"><b>Gender</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $gender ?></td>
                                                                    <td style="line-height: 20px; width: 200px" rowspan="7">
                                                            <center><img class="thumbnail" src="<?= check_and_get_img_url('./upload/maid_avatars/'.$maid_val['maid_photo_file'], 'maid-avatar.png'); ?>" style="height: 150px; width: 150px"/></center>

                                                        </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="line-height: 20px; width: 200px"><b>Nationality</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $maid_val['maid_nationality'] ?></td>
                                                                    <td style="line-height: 20px; width: 200px"><b>Flat</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $maid_val['flat_name'] ?: '-' ?></td>

                                                                </tr>
                                                                <tr>
                                                                    <td style="line-height: 20px; width: 200px"><b>Mobile Number 1</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $maid_val['maid_mobile_1'] ?></td>
                                                                    <td style="line-height: 20px; width: 200px"><b>Mobile Number 2</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $maid_val['maid_mobile_2'] ?: '-' ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="line-height: 20px; width: 200px"><b>WhatsApp No.</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $maid_val['maid_whatsapp_no_1'] ?: '-' ?></td>
                                                                    <td style="line-height: 20px; width: 200px"><b>Priority No.</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $maid_val['maid_priority'] ?: '-' ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="line-height: 20px; width: 200px"><b>Present Address</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $maid_val['maid_present_address'] ?></td>
                                                                    <td style="line-height: 20px; width: 200px"><b>Permanent Address</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $maid_val['maid_permanent_address'] ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="line-height: 20px; width: 200px"><b>Joining Date</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $doj; ?></td>
                                                                    <td style="line-height: 20px; width: 200px"><b>Team</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $maid_val['team_name'] ?: '-' ?></td>
                                                                </tr>

                                                                <tr>
                                                                    <td style="line-height: 20px;" colspan="2"><b><center>Services</center></b></td>
                                                                    <td style="line-height: 20px;" colspan="1"><b><center>Notes</center></b></td>
                                                                    <td style="line-height: 20px;" colspan="1"><b><center>Skills</center></b></td>
                                                                </tr>
                                                                <tr>


                                                                    <td style="line-height: 20px;" rowspan="2" colspan="2">
                                                                        <?php if (count($maid_services) > 0): ?>
                                                                        <ol>
                                                                        <?php foreach ($maid_services as $maid_services_val): ?>
                                                                            <li><?php echo $maid_services_val['service_type_name'] ?></li>
                                                                        <?php endforeach; ?>
                                                                        </ol>
                                                                        <?php endif; ?>
                                                                    </td>
                                                                    <td style="line-height: 20px;" rowspan="2" colspan="1" ><?php echo $maid_val['maid_notes'] ?: '-' ?></td>
                                                                    <td style="line-height: 20px;" rowspan="2" colspan="1" >
                                                                    <?php if(sizeof($maid_skills_added) == 0): ?>
                                                                        <p class="text-center">No skills added !</p>
                                                                    <?php else: ?>
                                                                        <ol>
                                                                        <?php foreach ($maid_skills_added as $key => $skill): ?>
                                                                            <li><?= $skill->skill?><span class="pull-right"><?=$skill->rating_level?></span></li>
                                                                        <?php endforeach;?>
                                                                        </ol>
                                                                    <?php endif; ?>
                                                                    </td>
                                                                    <td align="center" valign="middle">

                                                                <?php
if (($maid_val['maid_status'] != 2) && (user_authenticate() == 1)) {
        ?>

                                                                            <input class="n-btn red-btn" value="Delete" name="maid_delete" onclick="return confirm_delete_modal(<?php echo $maid_val['maid_id'] ?>);" type="button" style="display: block; float: none; margin: 40% auto !important;">

                                        <?php
}
    ?>


                                                                    </td>
                                                                </tr>

                                                            </tbody>
                                                        </table>
                                                    </div><!-- /widget-content -->

                                    </fieldset>
                                </div>


<?php }
?>
                        </div>
                    </form>
                </div>
            </div> <!-- /widget-content -->
        </div> <!-- /widget -->
    </div> <!-- /span8 -->
</div> <!-- /row -->
<script>
$(function() {
	var current = window.location.href;
	$('#primary_nav_wrap li a').each(function() {
		var $this = $(this);
		// if the current path is like this link, make it active
		if ($this.attr('href') === _base_url +'maids') {
			$this.addClass('active');
		}
	})
})

function confirm_delete_modal(id) {
	$('#delete_id').val(id);
	$.fancybox.open({
		autoCenter: true,
		fitToView: false,
		scrolling: false,
		openEffect: 'none',
		openSpeed: 1,
		autoSize: false,
		width: 450,
		height: 'auto',
		helpers: {
			overlay: {
				css: {
					'background': 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding: 0,
		closeBtn: false,
		content: $('#delete-popup'),
	});
}
function closeFancy() {
	$.fancybox.close();
}
function confirm_delete() {
	$.ajax({
		type: "POST",
		url: _base_url + "maid/change_status",
		data: {
			maid_id: $('#delete_id').val(),
			status: 2
		},
		dataType: "text",
		cache: false,
		success: function(result) {
			window.location.assign(_base_url + 'maids');
		}
	});
}
</script>
