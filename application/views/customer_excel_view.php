<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=customers.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border="1">
    <thead>
        <tr>
            <th> Sl No.</th>
            <th> Customer Name</th>
            <th> Contact No</th>
			<th> Email-Id</th>
            <th> Area</th>
            <th> Address</th>
            <th> Source</th>
            <th> Paytype</th>
            <th> Added Date time</th>
            <th> Status</th> 
        </tr>
    </thead>
    <tbody>
        <?php
        $sln = 1;
        foreach($customers as $customer)
        {
            if($customer->building != "")
            {
                $apartmnt_no = 'Apartment No:'.$customer->building.', '.$customer->unit_no.', '.$customer->street.'<br/>';
            } else {
                $apartmnt_no = "";
            }
            if ($customer->payment_type == "D") {
                $paytype = "Daily";
            } else if ($customer->payment_type == "W") {
                $paytype = "Weekly";
            } else if ($customer->payment_type == "M") {
                $paytype = "Monthly";
            } else {
                $paytype = "";
            }
        ?>
        <tr>
            <td><?php echo $sln++; ?></td>
            <td><?php echo $customer->customer_name; ?></td>
            <td><?php echo $customer->mobile_number_1; ?></td>
			<td><?php echo $customer->email_address; ?></td>
            <td><?php echo $customer->area_name; ?></td>
            <td><?php echo $apartmnt_no; ?><?php echo $customer->customer_address; ?></td>
            <td><?php echo $customer->customer_source; ?></td>
            <td><?php echo $paytype; ?></td>
            <td><?php echo $customer->customer_added_datetime; ?></td>
            <td>
                <?php
                if($customer->status == 1)
                {
                    echo 'Active';
                } else {
                    echo 'Inactive';
                }
                ?>
            </td>
        </tr>
        <?php
        }
        ?>
    </tbody>
</table>