<script>
    var time_from = '<?=$booking->time_from;?>';
    var time_to = '<?=$booking->time_to;?>';
</script>
<style>
        .color-box {
    padding: 5px 10px;
    border-radius: 3px;
    line-height: 20px;
    display: inline-block;
    margin: 5px 5px 0px 5px;
    cursor:default;
    font-size: smaller;
}
  .bg-od{
    background-color: <?=$settings->color_bg_booking_od;?> !important;
    color: <?=$settings->color_bg_text_booking_od;?> !important;
  }

  .bg-we{
    background-color: <?=$settings->color_bg_booking_we;?> !important;
    color: <?=$settings->color_bg_text_booking_we;?> !important;
  }

  .bg-bw{
    background-color: <?=$settings->color_bg_booking_bw;?> !important;
    color: <?=$settings->color_bg_text_booking_bw;?> !important;
  }
</style>
<section>
    <form id="assign_form">
        <input type="hidden" name="booking_id" value="<?=$booking->booking_id?>">
    <div class="row m-0">
        <div class="col-md-12 col-sm-12">
            <!--<div class="widget widget-table action-table" style="margin-bottom:30px">-->
            <div class="widget-header" style="margin-bottom: 0;">
            <ul>
                <li>
                    <i class="icon-th-list"></i>
                    <h3>Assign Maid</h3>
                </li>
            </ul>
            </div>
            <div class="col-md-12 col-sm-12 confirm-det-cont-box borderbox" style="background: #FFF;">
                <div class="col-sm-12 confi-det-cont-det new-booking-box-main no-left-padding">
                    <div class="col-md-12 col-sm-12 text-field-main">
                        <div id="u-error" style="color:red;"></div>
                    </div>
                    <div class="row m-0">
                    <div class="col-sm-6">
                    <div class="row m-0 n-field-main">
                        <div class="col-sm-4 pl-0 n-field-box">
                            <p>Booking Ref. No.</p>
                             <input type="text" name="reference_id" class="popup-disc-fld" value="<?=$booking->reference_id?>" readonly>
                        </div>
                        <div class="col-sm-4 pl-0 n-field-box">
                            <p>Customer</p>
                             <input type="text" name="customer_name" class="popup-disc-fld" value="" readonly>
                        </div>
                        <div class="col-sm-4 n-field-box">
                            <p>No. of Maids</p>
                             <input type="text" name="no_of_maids" class="popup-disc-fld" value="<?=$booking->no_of_maids?>" readonly>
                        </div>
                    </div>
                    <div class="row m-0 n-field-main">
                        <div class="col-sm-4 pl-0 n-field-box">
                            <p>Repeat</p>
                            <input type="hidden" name="booking_type" value="<?=$booking->booking_type?>">
                            <select name="booking_type" data-placeholder="Select repeat type" class="sel2">
                                <option value="">-- Select Repeat -- </option>
                                <option value="OD" <?=$booking->booking_type == "OD" ? 'selected' : ''?>><?=$settings->od_booking_text;?></option>
                                <option value="WE" <?=$booking->booking_type == "WE" ? 'selected' : ''?>><?=$settings->we_booking_text;?></option>
                                <option value="BW" <?=$booking->booking_type == "BW" ? 'selected' : ''?>><?=$settings->bw_booking_text;?></option>
                            </select>
                        </div>
                        <div class="col-sm-4 n-field-box pl-0">
                            <p><?=$booking->booking_type != "OD" ? 'Service Start Date' : 'Service Date';?></p>
                             <input name="service_start_date" type="text" class="popup-disc-fld" value="<?=date("d/m/Y", strtotime($booking->service_start_date))?>" readonly>
                        </div>
                        <?php if ($booking->booking_type != "OD"): ?>
                        <?php if ($booking->service_end == 1 && $booking->booking_type != "OD"): ?>
                        <div class="col-sm-4 n-field-box">
                            <p>Service End Date</p>
                             <input name="service_end_date" type="text" class="popup-disc-fld" value="<?=date("d/m/Y", strtotime($booking->service_end_date))?>" readonly>
                        </div>
                        <?php else: ?>
                        <div class="col-sm-4 n-field-box">
                            <p>Service End</p>
                             <input type="text" class="popup-disc-fld" value="Never" readonly>
                        </div>
                        <?php endif;?>
                        <?php endif;?>
                    </div>
                    <div class="row m-0 n-field-main">
                        <div class="col-sm-3 pl-0 n-field-box">
                            <p>Booked Amount</p>
                             <input type="number" step="any" name="booked_amount" class="popup-disc-fld" value="0" readonly>
                        </div>
                        <div class="col-sm-3 pl-0 n-field-box">
                            <p>Discount</p>
                             <input type="number" step="any" name="discount" class="popup-disc-fld" value="0" readonly>
                        </div>
                        <div class="col-sm-3 n-field-box">
                            <p>Convenience Fee</p>
                             <input type="number" step="any" name="convenience_fee" class="popup-disc-fld" value="0" readonly>
                        </div>
                        <div class="col-sm-3 n-field-box">
                            <p>Total Amount</p>
                             <input type="number" step="any" name="total_amount" class="popup-disc-fld" value="0" readonly>
                        </div>
                    </div>
                    <div class="row m-0 n-field-main">
                        <div class="col-sm-6 pl-0 n-field-box">
                            <p>Time From</p>
                             <select name="time_from" class="sel2">
                                <?php foreach ($time_slots as $value => $text): ?>
                                    <option value="<?=$value;?>"><?=$text;?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="col-sm-6 n-field-box">
                            <p>Time To</p>
                            <select name="time_to" class="sel2">
                            </select>
                        </div>
                    </div>
                    <div class="row m-0 n-field-main">
                        <div class="col-sm-6 pl-0 n-field-box">
                            <div id="maids-list-holder" style="display:none">
                            <p>Selected Maids</p>
                            <ol>
                            </ol>
                        </div>
                        </div>
                        <div class="col-sm-6 pl-0 n-field-box">
                            <div class="row n-field-main">
                                <div class="col-sm-8 pl-0 n-field-box">
                                </div>
                                <div class="col-sm-4 n-field-box">
                                    <input type="button" class="n-btn" value="Book Now" data-action="book-now">
                                </div>
                            </div>
                        </div>
                    </div>



                    </div>
                    <div class="col-sm-6">
                        <table class="table table-striped table-bordered">
                            <thead >
                                <tr>
                                <th style="background-color:#4b8882;width:80px;vertical-align: middle;" class="text-center">
                                    Sl. No.
                                </th>
                                <th style="background-color:#4b8882;width:80px;vertical-align: middle;" class="text-left">
                                    Select
                                </th>
                                <th style="background-color:#4b8882;min-width:150px;vertical-align: middle;" class="text-left">
                                    Maid Name
                                </th>
                                <th style="background-color:#4b8882" class="text-left">
                                    Shifts<span class="color-box bg-od">One Day Only</span><span class="color-box bg-we">Every Week</span><span class="color-box bg-bw">Every 2 Week</span>
                                </th>
                                </tr>
                            </thead>
                            <tbody id="maids-holder">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!--welcome-text-main end-->

    </div>
    <!--row content-wrapper end-->
</form>
</section>
<!--welcome-text end-->
<script>
    $(function() {
	let current = window.location.href;
	$('#primary_nav_wrap li a').each(function() {
		var $this = $(this);
		// if the current path is like this link, make it active
		if ($this.attr('href') === '<?php echo base_url('booking/approvallist'); ?>') {
			$this.addClass('active');
		}
	})
})
</script>