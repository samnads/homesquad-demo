<style type="text/css">
    .widget .widget-header {
        margin-bottom: 0px;
    }

    .topiconnew {
        cursor: pointer;
    }
</style>
<div class="row m-0">
    <div class="col-md-12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header">
                <form id="edit-profile" class="form-horizontal" method="post">
                    <ul>
                        <li>
                            <i class="icon-th-list"></i>
                            <h3>One Day Cancel Report</h3>

                        </li>
                        <li class="mr-2">
                            <input type="text" style="width: 160px;" id="OneDayDate" name="search_date" value="<?php echo isset($search_date) ? $search_date : date('d/m/Y'); ?>">
                        </li>
                        <li>
                            <input type="submit" class="n-btn" value="Go" name="ondeday_report">
                        </li>

                        <li class="mr-0 float-right">

                            <!--<div class="topiconnew border-0 green-btn">
                    	<a href="#" id="printButton" title="Print"> <i class="fa fa-print"></i></a>
                    </div>-->

                            <div class="topiconnew border-0 green-btn">
                                <a title="Download to Excel" data-action="excel-export"> <i class="fa fa-download"></i></a>
                            </div>

                        </li>
                    </ul>

                </form>
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px;">
                                <center>Sl. No.<center>
                            </th>
                            <th style="line-height: 18px">Customer Name</th>
                            <th style="line-height: 18px">Maid Name</th>
                            <th style="line-height: 18px">Shift</th>
                            <th style="line-height: 18px">Added</th>
                            <th style="line-height: 18px">Canceled User</th>
                            <th style="line-height: 18px">
                                <center>Action</center>
                            </th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        if (!empty($reports)) {
                            $i = 0;
                            foreach ($reports as $report) {
                                //Payment Type
                                if ($report->payment_type == "D") {
                                    $paytype = "(D)";
                                } else if ($report->payment_type == "W") {
                                    $paytype = "(W)";
                                } else if ($report->payment_type == "M") {
                                    $paytype = "(M)";
                                } else {
                                    $paytype = "";
                                }
                                $i++;
                        ?>
                                <tr>
                                    <td style="line-height: 18px;">
                                        <center><?php echo $i; ?></center>
                                    </td>
                                    <td style="line-height: 18px">
                                        <?php echo $report->customer_name; ?> <?php echo $paytype; ?>
                                    </td>
                                    <td style="line-height: 18px">
                                        <?php echo $report->maid_name; ?>
                                    </td>
                                    <td style="line-height: 18px">
                                        <?php echo $report->shift; ?>
                                    </td>
                                    <td style="line-height: 18px">
                                        <?php echo $report->added ? date('d-m-Y H:i:s A', strtotime($report->added)) : date('d-m-Y H:i:s A', strtotime($report->service_date)); ?>
                                    </td>
                                    <td style="line-height: 18px">
                                        <?php echo $report->user_fullname; ?>
                                    </td>
                                    <td>
                                        <center>
                                            <a class="n-btn-icon red-btn" onclick="deleteOnedayCancel(this,<?php echo $report->booking_delete_id; ?>)" href="javascript:void(0)"><i class="btn-icon-only icon-remove"> </i></a>
                                        </center>
                                    </td>
                                </tr>
                        <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div><!-- /widget-content -->

        </div><!-- /widget -->
    </div><!-- /span12 -->
</div>
<div style="display: none;" id="OneDayReportPrint">
    <table border="1" cellpadding="0" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th style="line-height: 18px;">Sl. No.</th>
                <th style="line-height: 18px">Customer Name</th>
                <th style="line-height: 18px">Maid Name</th>
                <th style="line-height: 18px">Shift</th>
                <th style="line-height: 18px">Added</th>
                <th style="line-height: 18px">Canceled User</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (!empty($reports)) {
                $i = 0;
                foreach ($reports as $report) {
                    //Payment Type
                    if ($report->payment_type == "D") {
                        $paytype = "(D)";
                    } else if ($report->payment_type == "W") {
                        $paytype = "(W)";
                    } else if ($report->payment_type == "M") {
                        $paytype = "(M)";
                    } else {
                        $paytype = "";
                    }
                    $i++;
            ?>
                    <tr>
                        <td style="line-height: 18px;">
                            <?php echo $i; ?>
                        </td>
                        <td style="line-height: 18px">
                            <?php echo $report->customer_name; ?> <?php echo $paytype; ?>
                        </td>
                        <td style="line-height: 18px">
                            <?php echo $report->maid_name; ?>
                        </td>
                        <td style="line-height: 18px">
                            <?php echo $report->shift; ?>
                        </td>
                        <td style="line-height: 18px">
                            <?php echo $report->added ? date('d-m-Y H:i:s A', strtotime($report->added)) : date('d-m-Y H:i:s A', strtotime($report->service_date)); ?>
                        </td>
                        <td style="line-height: 18px">
                            <?php echo $report->user_fullname; ?>
                        </td>
                    </tr>
            <?php
                }
            }
            ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    function exportF(elem) {
        var table = document.getElementById("OneDayReportPrint");
        var html = table.outerHTML;
        var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url 
        elem.setAttribute("href", url);
        elem.setAttribute("download", "OneDayCancelReport.xls"); // Choose the file name
        return false;
    }
    $('[data-action="excel-export"]').click(function() {
        var fileName = "One Day Cancel Report " + "<?= str_replace("/", "-", $search_date ?: date('d/m/Y')) ?>";
        var fileType = "xlsx";
        var table = document.getElementById("OneDayReportPrint");
        var wb = XLSX.utils.table_to_book(table, {
            sheet: "Report"
        });
        const ws = wb.Sheets['Report'];
        var wscols = [{
                wch: 15
            },
            {
                wch: 25
            },
            {
                wch: 30
            },
            {
                wch: 25
            },
            {
                wch: 20
            }
        ];
        ws['!cols'] = wscols;
        return XLSX.writeFile(wb, null || fileName + "." + (fileType || "xlsx"));
    });
</script>
