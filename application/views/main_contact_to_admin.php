<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style>
.mailspan a{color:#FFF !important;}
.table_content td {border: 1px solid #70dbd5; }
.table_content{border-collapse: collapse;}	
</style>
</head>

<body>

<table width="657" border="0" style="margin: 0 auto;" class="table_content" cellpadding="0">
  <tr>
    <td style="border-bottom: 1px solid #FFF;"><img src="<?php echo base_url();?>images/email-banner.jpg" width="655" height="264" alt="" style="border-bottom: 1px solid #70dbd5;"/></td>
  </tr>
  <tr>
    <td>
        <p style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size: 16px; color: #555; line-height: 20px; padding: 15px 10px 5px 10px; margin: 0px 0px 0px 0px; font-weight: bold;">Dear Admin,</p>
        <p style="font-family:Arial, Helvetica, sans-serif; font-size: 14px; color: #555; line-height: 20px; padding: 5px 10px 15px 10px; margin: 0px 0px 0px 0px;">New enquiry recieved.</p>
    </td>
  </tr>
  <tr style="background: #70dbd5;">
    <td><p style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size: 15px; color: #FFF; line-height: 20px; padding: 5px 10px 5px 10px; margin: 0px 0px 0px 0px; font-weight: bold;">Details</p></td>
  </tr>
  
</table>



<table width="656" border="0" style="margin: 0 auto;" class="table_content" cellspacing="0px">
  <tr >
    <td width="202"><p style="font-family:Arial, Helvetica, sans-serif; font-size: 14px; color: #555; line-height: 20px; padding: 5px 10px 5px 10px; margin: 0px 0px 0px 0px;">Customer</p></td>
    <td width="444"><p style="font-family:Arial, Helvetica, sans-serif; font-size: 14px; color: #555; line-height: 20px; padding: 5px 10px 5px 10px; margin: 0px 0px 0px 0px;"><?php echo $customer;?></p></td>
  </tr>
  <tr >
    <td><p style="font-family:Arial, Helvetica, sans-serif; font-size: 14px; color: #555; line-height: 20px; padding: 5px 10px 5px 10px; margin: 0px 0px 0px 0px;">Mobile</p></td>
    <td><p style="font-family:Arial, Helvetica, sans-serif; font-size: 14px; color: #555; line-height: 20px; padding: 5px 10px 5px 10px; margin: 0px 0px 0px 0px;"><?php echo $mobile;?></p></td>
  </tr>
  <tr >
    <td><p style="font-family:Arial, Helvetica, sans-serif; font-size: 14px; color: #555; line-height: 20px; padding: 5px 10px 5px 10px; margin: 0px 0px 0px 0px;">Email</p></td>
    <td><p style="font-family:Arial, Helvetica, sans-serif; font-size: 14px; color: #555; line-height: 20px; padding: 5px 10px 5px 10px; margin: 0px 0px 0px 0px;"><?php echo $email;?></p></td>
  </tr>
  <?php if($address){?>
  <tr >
    <td><p style="font-family:Arial, Helvetica, sans-serif; font-size: 14px; color: #555; line-height: 20px; padding: 5px 10px 5px 10px; margin: 0px 0px 0px 0px;">Area</p></td>
    <td><p style="font-family:Arial, Helvetica, sans-serif; font-size: 14px; color: #555; line-height: 20px; padding: 5px 10px 5px 10px; margin: 0px 0px 0px 0px;"><?php echo $address;?></p></td>
  </tr>
  <?php } ?>
  <?php if($message){?>
  <tr >
    <td><p style="font-family:Arial, Helvetica, sans-serif; font-size: 14px; color: #555; line-height: 20px; padding: 5px 10px 5px 10px; margin: 0px 0px 0px 0px;">Message</p></td>
    <td><p style="font-family:Arial, Helvetica, sans-serif; font-size: 14px; color: #555; line-height: 20px; padding: 5px 10px 5px 10px; margin: 0px 0px 0px 0px;"><?php echo $message;?></p></td>
  </tr>
  <?php } ?>
  
</table>


<table width="656" border="0" style="margin: 0 auto;"  class="table_content" cellspacing="0px">
  <tr>
    <td><img src="<?php echo base_url();?>images/email-img1.jpg" width="650" height="81" alt="" /></td>
  </tr>
</table>


<table width="656" border="0" style="margin: 0 auto; background: #70dbd4; padding: 30px 0px">
  <tr>
    <td>&nbsp;</td>
    <td width="50" height="50"><a href="https://www.facebook.com/HomeSquadUAE" target="_blank" title="Facebook"><img src="<?php echo base_url();?>images/email-img2.jpg" width="40" height="40" alt="" /></a></td>
    <td width="50" height="50"><a href="https://twitter.com/homesquad_uae" target="_blank" title="Twitter"><img src="<?php echo base_url();?>images/email-img3.jpg" width="40" height="40" alt="" /></a></td>
    <td width="50" height="50"><a href="https://www.instagram.com/homesquaduae" target="_blank" title="Instagram"><img src="<?php echo base_url();?>images/email-img6.jpg" width="40" height="40" alt="" /></a></td>
    <td>&nbsp;</td>
  </tr>
</table>


<table width="656" border="0" style="margin: 0 auto; background: #734fbd; padding: 20px 0px 20px 0px; border-bottom: 15px solid  #70dbd4;">
  <tr>
    <td width="200" valign="top">
<p style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size: 15px; color: #FFF; line-height: 20px; padding: 5px 10px 5px 10px; margin: 0px 0px 0px 0px; font-weight: bold;">Our Services</p>
<p style="font-family:Arial, Helvetica, sans-serif; font-size: 13px; color: #FFF; line-height: 20px; padding: 5px 10px 3px 10px; margin: 0px 0px 0px 0px;"><a href="<?php echo $this->config->item('web_url');?>cleaning-service/house-cleaning" target="_blank" style="color: #FFF;">Residential Cleaning</a></p>
<p style="font-family:Arial, Helvetica, sans-serif; font-size: 13px; color: #FFF; line-height: 20px; padding: 5px 10px 3px 10px; margin: 0px 0px 0px 0px;"><a href="<?php echo $this->config->item('web_url');?>cleaning-service/window-cleaning" target="_blank" style="color: #FFF;">Window Cleaning</a></p>
<p style="font-family:Arial, Helvetica, sans-serif; font-size: 13px; color: #FFF; line-height: 20px; padding: 5px 10px 3px 10px; margin: 0px 0px 0px 0px;"><a href="<?php echo $this->config->item('web_url');?>cleaning-service/iron-services" target="_blank" style="color: #FFF;">Ironing Services</a></p>
<p style="font-family:Arial, Helvetica, sans-serif; font-size: 13px; color: #FFF; line-height: 20px; padding: 5px 10px 3px 10px; margin: 0px 0px 0px 0px;"><a href="<?php echo $this->config->item('web_url');?>cleaning-service/after-party-cleaning" target="_blank" style="color: #FFF;">After Party Cleaning</a></p>
<p style="font-family:Arial, Helvetica, sans-serif; font-size: 13px; color: #FFF; line-height: 20px; padding: 5px 10px 3px 10px; margin: 0px 0px 0px 0px;"><a href="<?php echo $this->config->item('web_url');?>cleaning-service/babysitting-services" target="_blank" style="color: #FFF;">Babysitting Services</a></p>

</td>
    <td width="234" valign="top">
    
<p style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size: 15px; color: #FFF; line-height: 20px; padding: 5px 10px 5px 10px; margin: 0px 0px 0px 0px; font-weight: bold;">Find our Location</p>

<p style="font-family:Arial, Helvetica, sans-serif; font-size: 13px; color: #FFF; line-height: 18px; padding: 5px 10px 15px 10px; margin: 0px 0px 0px 0px;">Home Squad<br />The Metropolis Tower,<br />17th Floor, Office 1701<br />Business Bay, Dubai<br /></p>

<p style="font-family:Arial, Helvetica, sans-serif; font-size: 13px; color: #FFF; line-height: 18px; padding: 5px 10px 0px 10px; margin: 0px 0px 0px 0px; font-weight: bold;">Phone &nbsp;&nbsp; : 8009876</p>
<p style="font-family:Arial, Helvetica, sans-serif; font-size: 13px; color: #FFF; line-height: 18px; padding: 5px 10px 5px 10px; margin: 0px 0px 0px 0px; font-weight: bold;">Email&nbsp; &nbsp;&nbsp;  : info@homesquad.ae</p>
    
    </td>
    <td width="200" valign="top"><p style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size: 15px; color: #FFF; line-height: 20px; padding: 5px 10px 5px 10px; margin: 0px 0px 0px 0px; font-weight: bold;">Why receiving this email?</p>
<p style="font-family:Arial, Helvetica, sans-serif; font-size: 13px; color: #FFF; line-height: 18px; padding: 5px 10px 5px 10px; margin: 0px 0px 0px 0px;">Because you signed up to our mailing list.
Not Interested anymore? <a href="#" title="Click to Unsubscribe" style="color: #70dbd4; font-weight: bold;">Unsubscribe</a> from our list.</p></td>
  </tr>
</table>




<table width="650" border="0" style="margin: 0 auto;">
  <tr>
    <td><p style="font-family:Arial, Helvetica, sans-serif; font-size: 12px; color: #555; line-height: 20px; text-align:center; padding: 5px 10px 0px 10px; margin: 0px 0px 0px 0px;">&copy; 2019 Home Squad. All Rights Reserved.</p></td>
  </tr>
</table>

</body>
</html>

