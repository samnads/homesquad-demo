<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=invoice_report_".date('d-m-Y',strtotime($date)).".xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<table border="1">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Sl.No</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Number</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Customer</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Invoice Date</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Due Date</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Total</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Paid Amount</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Due Amount</th>
                            <!--<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Amount Due</th>-->
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Status</th>
                        </tr>
                    </thead>
                    <tbody>
					<?php
					$sub_t = 0;
					$grand_paid = 0;
					$grand_due = 0;
					$i = 1;
                    foreach ($invoice_report as $inv){
						$sub_t += round($inv->invoice_net_amount,2);
						$grand_paid += round($inv->received_amount,2);
						$grand_due += round($inv->balance_amount,2);
					?>
						<tr>
							<td style="line-height: 18px;"><?php echo $i; ?> </td>
							<td style="line-height: 18px;"><?php echo $inv->invoice_num; ?> </td>
							<td style="line-height: 18px;"><?php echo $inv->userName; ?></td>
							<td style="line-height: 18px;"><?php echo $inv->invoice_date; ?></td>
							<td style="line-height: 18px;"><?php echo $inv->invoice_due_date; ?></td>
							<td style="line-height: 18px;"><?php echo $inv->invoice_net_amount; ?></td>
							<td style="line-height: 18px;"><?php echo $inv->received_amount; ?></td>
							<td style="line-height: 18px;"><?php echo $inv->balance_amount; ?></td>
							<!--<td style="line-height: 18px;"></td>-->
							<td style="line-height: 18px">
								<?php
								if($inv->invoice_paid_status == 2)
								{
									$paystat = " (Partially Paid)";
								} else {
									$paystat = "";
								}
								if($inv->invoice_status == '0')
								{
									$invstatus = "Draft";
								} else if($inv->invoice_status == '1')
								{
									$invstatus = "Open";
								} else  if($inv->invoice_status == '2'){
									$invstatus = "Cancelled";
								} else  if($inv->invoice_status == '3'){
									$invstatus = "Paid";
								}
								echo $invstatus.$paystat;
								?>
							</td>
							
						</tr>
					<?php
					$i++;	
					}
					?>
                    </tbody>
					<tfoot>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>Total</td>
							<td><?php echo $sub_t; ?></td>
							<td><?php echo $grand_paid; ?></td>
							<td><?php echo $grand_due; ?></td>
							<td></td>
						</tr>
					</tfoot>
                </table>