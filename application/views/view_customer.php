<style type="text/css">
.bg-od,.schedule_bubble.od {
  background-color: <?= $settings->color_bg_booking_od; ?> !important;
  color: <?= $settings->color_bg_text_booking_od; ?> !important;
}
.bg-we,.schedule_bubble.we {
  background-color: <?= $settings->color_bg_booking_we; ?> !important;
  color: <?= $settings->color_bg_text_booking_we; ?> !important;
}
.bg-bw,.schedule_bubble.bw {
  background-color: <?= $settings->color_bg_booking_bw; ?> !important;
  color: <?= $settings->color_bg_text_booking_bw; ?> !important;
}
</style>
<style>
.tabbable .nav-tabs > li > a {
	padding: 7px 12px;
}

.width-auto { width: auto;}

.m-auto { margin: 0 auto;}


</style>
<div id="pause-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Pause ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure want to pause this booking ?</h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_pause()"><i class='btn-icon-only icon-pause'></i>&nbsp;Pause</button>
    </div>
  </div>
</div>
<div id="resume-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Resume ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure want to resume this booking ?</h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn mb-0" onclick="confirm_resume()"><i class='btn-icon-only icon-play'></i>&nbsp;Resume</button>
    </div>
  </div>
</div>
<div class="row m-0">
  <div class="col-sm-12">
    <div class="widget">
      <div class="widget-header">
      <ul>
      <li>
      <i class="icon-user"></i>
        <h3>Customer</h3>
        
        </li>
       <li class="mr-0 float-right">
        <?php
                if(user_authenticate() == 1)
                {
                ?>
        <div class="topiconnew border-0 "> <a href="<?php echo base_url() . 'customer/disable/' . $customer_id . '/' . $customer_status; ?>" class="<?php echo $customer_status == 1 ? 'red-btn' : 'green-btn'; ?>" title="<?php echo $customer_status == 1 ? 'Delete' : 'Recover'; ?>" ><?php echo $customer_status == 1 ? '<i class="icon-trash m-0"> </i>' : '<i class="icon-ok m-0"> </i>' ?></a> </div>
        <?php
                }
                ?>
                
                
                
        <div class="topiconnew border-0 green-btn"> <a href="<?php echo base_url(); ?>customers" title="Customer List"> <i class="fa fa-users"></i></a> </div>
        <div class="topiconnew border-0 green-btn"> <a href="<?php echo base_url() . 'customer/edit/' . $customer_id; ?>" title="Edit Customer"> <i class="fa fa-pencil"></i></a> </div>
        </li>
        </ul>
      </div>
      <!-- /widget-header -->
      <div class="widget-content no-top-padding">
        <div class="tabbable">
        
<style>


.table-bordered {border-radius: 0px !important;}


.table-bordered thead:first-child tr:first-child th:first-child, .table-bordered tbody:first-child tr:first-child td:first-child {
    border-radius: 0px !important;
}
.table-bordered thead:first-child tr:first-child th:last-child, .table-bordered tbody:first-child tr:first-child td:last-child {
    border-radius: 0px !important;
}
.table-bordered thead:last-child tr:last-child th:first-child, .table-bordered tbody:last-child tr:last-child td:first-child {
    border-radius: 0px !important;
}
.table-bordered thead:last-child tr:last-child th:last-child, .table-bordered tbody:last-child tr:last-child td:last-child {
    border-radius: 0px !important;
}

table  td { font-family: Arial, Helvetica, sans-serif !important; text-align: left !important;}



</style> 
<script>
  jQuery(function ($) {
	  //$(".accordion-content").css("display", "none");
	  $(".accordion-title").click(function () {
		//$(".accordion-title").not(this).removeClass("open");
		//$(".accordion-title").not(this).next().slideUp(300);
		$(this).toggleClass("open");
		$(this).next().slideToggle(300);
	  });
  });
</script>

<div id="accordion" class="accordion-container">
       <form id="edit-profile" class="form-horizontal" method="post">
          <h4 class="accordion-title open">Personal Details</h4>
          <div class="accordion-content" style="display:block !important">
              <?php
					//if (count($customer_details) > 0) {
					foreach ($customer_details as $customer_val) {
			  ?>
                <fieldset>
                  <div class="col-sm-12 p-0 p-0" id="personal">
                          <?php
							  if ($customer_val['customer_photo_file'] == "") {
								  $image = base_url() . "img/no_image.jpg";
							  } else {
								  $image = base_url() . "customer_img/" . $customer_val['customer_photo_file'];
							  }
							  if ($customer_val['customer_type'] == "HO") {
								  $cust_type = "Home";
							  } else if ($customer_val['customer_type'] == "OF") {
								  $cust_type = "Office";
							  } else if ($customer_val['customer_type'] == "WH") {
								  $cust_type = "Warehouse";
							  } else if ($customer_val['customer_type'] == "") {
								  $cust_type = "";
							  }
							  if($customer_val['customer_booktype'] == 0) {
								  $customerbooktype = "Non Regular";
							  } else if($customer_val['customer_booktype'] == 1) {
								  $customerbooktype = "Regular";
							  }
							  if ($customer_val['payment_type'] == "D") {
								  $pay_type = "Daily Paying";
							  } else if ($customer_val['payment_type'] == "W") {
								  $pay_type = "Weekly Paying";
							  } else if ($customer_val['payment_type'] == "M") {
								  $pay_type = "Monthly Paying";
							  } else if ($customer_val['payment_type'] == "") {
								  $pay_type = "";
							  }
							  
							  if($customer_val['is_company'] == "Y") {
								  $iscompany = "YES";
							  } else if($customer_val['is_company'] == "N") {
								  $iscompany = "NO";
							  }
							  
							  if($customer_val['is_flag'] == "Y") {
								  $isflag = "YES";
							  } else if($customer_val['is_flag'] == "N") {
								  $isflag = "NO";
							  }
							  ?>
							  <input type="hidden" value="<?php echo $customer_val['customer_name'] ?>" id="hiddencustomername" />
														<input type="hidden" value="<?php echo $customer_val['mobile_number_1'] ?>" id="hiddenmobile" />
                          <table class="table table-striped table-bordered">
  <tbody>
    <tr>
      <td style="line-height: 18px; width: 200px">Customer Name</td>
      <td style="line-height: 18px; width: 300px"><b><?php echo $customer_val['customer_name'] ?></b></td>
      <td style="line-height: 18px; width: 200px">Customer Nick Name</td>
      <td style="line-height: 18px; width: 300px"><b><?php echo $customer_val['customer_nick_name'] ?></b></td>
    </tr>
    <tr>
      <td style="line-height: 18px; width: 200px">Flag Status</td>
      <td style="line-height: 18px; width: 300px"><b><?php echo $isflag ?></b></td>
      <td style="line-height: 18px; width: 200px">Flag Reason</td>
      <td style="line-height: 18px; width: 300px"><b><?php echo $customer_val['flag_reason'] ?: '-' ?></b></td>
    </tr>
    <tr>
      <td style="line-height: 18px; width: 200px">Contact Person</td>
      <td style="line-height: 18px; width: 300px"><b><?php echo $customer_val['contact_person'] ?: '-' ?></b></td>
      <td style="line-height: 18px; width: 200px">Phone</td>
      <td style="line-height: 18px; width: 300px"><b><?php echo $customer_val['phone_number'] ?: '-' ?></b></td>
    </tr>
    <tr>
      <td style="line-height: 18px; width: 200px">Mobile Number 1</td>
      <td style="line-height: 18px; width: 300px"><b><?php echo $customer_val['mobile_number_1'] ?: '-' ?></b></td>
      <td style="line-height: 18px; width: 200px">Mobile Number 2</td>
      <td style="line-height: 18px; width: 300px"><b><?php echo $customer_val['mobile_number_2'] ?: '-' ?></b></td>
    </tr>
    <tr>
      <td style="line-height: 18px; width: 200px">Mobile Number 3</b></td>
      <td style="line-height: 18px; width: 300px"><b><?php echo $customer_val['mobile_number_3'] ?: '-' ?></td>
      <td style="line-height: 18px; width: 200px">Fax</b></td>
      <td style="line-height: 18px; width: 300px"><b><?php echo $customer_val['fax_number'] ?: '-' ?></td>
    </tr>
    <tr>
      <td style="line-height: 18px; width: 200px">Is Company?</td>
      <td style="line-height: 18px; width: 300px"><b><?php echo $iscompany ?></b></td>
      <td style="line-height: 18px; width: 200px">Company Name</td>
      <td style="line-height: 18px; width: 300px"><b><?php echo $customer_val['company_name'] ?: '-' ?></b></td>
    </tr>
    <tr>
      <td style="line-height: 18px; width: 200px">User Name</td>
      <td style="line-height: 18px; width: 300px"><b><?php echo $customer_val['customer_username'] ?></b></td>
      <td style="line-height: 18px; width: 200px">Password</td>
      <td style="line-height: 18px; width: 300px"><b><?php echo $customer_val['customer_password'] ?></b></td>
    </tr>
    <tr>
      <td style="line-height: 18px; width: 200px">Customer ID</td>
      <td style="line-height: 18px; width: 300px"><b><?php echo $customer_val['customer_id'] ?></b></td>
      <td style="line-height: 18px; width: 200px">Email</td>
      <td style="line-height: 18px; width: 300px"><b><?php echo $customer_val['email_address'] ?></b></td>
    </tr>
    <tr>
      <td style="line-height: 18px; width: 200px">WhatsApp No.</td>
      <td style="line-height: 18px; width: 300px"><b><?php echo $customer_val['whatsapp_no_1'] ?: '-' ?></b></td>
      <td style="line-height: 18px; width: 200px"></td>
      <td style="line-height: 18px; width: 300px"><b></b></td>
    </tr>
  </tbody>
</table>
                  </div>
                </fieldset>
          </div>
              
              
          <h4 class="accordion-title">Account and Other Details</h4>    
          <div class="accordion-content">
                <fieldset>
                  <div class="col-sm-12 p-0" id="account">
                          <table class="table table-striped table-bordered">
                            <tbody>
                              <tr>
                                <td style="line-height: 18px; width: 200px">Payment Type</td>
                                <td style="line-height: 18px; width: 300px"><b><?php echo $pay_type ?: '-' ?></b></td>
                                <td style="line-height: 18px; width: 200px">Customer Book Type</td>
                                <td style="line-height: 18px; width: 300px"><b><?php echo $customerbooktype ?: '-' ?></b></td>
                              </tr>
                              <tr>
                                <td style="line-height: 18px; width: 200px">Customer Type</td>
                                <td style="line-height: 18px; width: 300px"><b><?php echo $cust_type ?: '-' ?></b></td>
                                <td style="line-height: 18px;">Payment Mode</td>
                                <td style="line-height: 18px;"><b><?php echo $customer_val['payment_mode'] ?: '-' ?></b></td>
                              </tr>
                              <tr>
                                <td style="line-height: 18px; width: 200px">Latitude</td>
                                <td style="line-height: 18px; width: 300px"><b><?php echo $customer_val['latitude'] ?: '-' ?></b></td>
                                <td style="line-height: 18px; width: 200px">Longitude</td>
                                <td style="line-height: 18px; width: 300px"><b><?php echo $customer_val['longitude'] ?: '-' ?></b></td>
                              </tr>
                              <tr>
                                <td style="line-height: 18px;">Hourly</td>
                                <td style="line-height: 18px;"><b><?php echo $customer_val['price_hourly'] ?: '-' ?></b></td>
                                <td style="line-height: 18px;">Weekly</td>
                                <td style="line-height: 18px;"><b><?php echo $customer_val['price_weekend'] ?: '-' ?></b></td>
                              </tr>
                              <tr>
                                <td style="line-height: 18px; width: 200px">Key Given</td>
                                <td style="line-height: 18px; width: 300px"><b><?php echo $customer_val['key_given'] == 'Y' ? 'Yes' : 'No' ?></b></td>
                                <td style="line-height: 18px;">Extras</td>
                                <td style="line-height: 18px;"><b><?php echo $customer_val['price_extra'] ?: '-' ?></b></td>
                                
                                <!--<td style="line-height: 18px;" colspan="2"><b><center>Notes</center></b></td>--> 
                                
                              </tr>
                              <tr>
                                <td style="line-height: 18px;">Pending Amount</td>
                                <td style="line-height: 18px; color:<?php if($customer_val['signed'] == 'Cr') echo 'green'; else echo 'red'; ?>; font-weight:bold;"><b><?php echo $customer_val['balance'].$customer_val['signed'] ?></b></td>
                                <td style="line-height: 18px;">Notes</td>
                                <td style="line-height: 18px;"><b><?php echo $customer_val['customer_notes'] ?: '-' ?></b></td>
                              </tr>
                              <tr>
                                <td style="line-height: 18px;">Opening Balance</td>
                                <td style="line-height: 18px; color:<?php if($customer_val['initial_bal_sign'] == 'Dr') echo 'green'; else echo 'red'; ?>; font-weight:bold;"><b><?php echo $customer_val['initial_balance'].' '.$customer_val['initial_bal_sign'] ?></b></td>
                                <td style="line-height: 18px;"></td>
                                <td style="line-height: 18px;"></td>
                              </tr>
                            </tbody>
                          </table>
                  </div>
                </fieldset>
              <?php
                    }
                   //}
              ?>
                            
         </div>   
         
         
         
         <h4 class="accordion-title ">Customer Address</h4>
          <div class="accordion-content">
                <fieldset>
                  <div class="col-sm-12 p-0" id="personal">
                       <table class="table table-striped table-bordered">
                            <thead>
                              <tr>
                                <th style="line-height: 18px; width: 20px"><center>
                                    Sl.No.
                                  </center>
                                </th>
                                <th style="line-height: 18px"> <center>
                                    Area
                                  </center></th>
                                <th style="line-height: 18px"> <center>
                                    Zone
                                  </center></th>
                                <th style="line-height: 18px"> <center>
                                    Address
                                  </center></th>
                            </thead>
                            <tbody>
                              
                              <?php
                                                                if (count($customer_address) > 0) {
                                                                    $j = 1;
                                                                    foreach ($customer_address as $customer_address_val) {
																		if($customer_address_val['customer_address'] == "")
																		{
																			$cust_address = "Building: ".$customer_address_val['building'].", Unit No: ".$customer_address_val['unit_no'].", Street : ".$customer_address_val['street'];
                                                                        } else {
																			$cust_address = $customer_address_val['customer_address'];
																		}
																		?>
                              <tr>
                                <td style="line-height: 18px; width: 50px"><b><?php echo $j++; ?></b></td>
                                <td style="line-height: 18px; width: 300px"><?php echo $customer_address_val['area_name'] ?></td>
                                <td style="line-height: 18px; width: 300px"><?php echo $customer_address_val['zone_name'] ?></td>
                                <td style="line-height: 18px; width: 300px"><?php echo $cust_address; ?></td>
                              </tr>
                              <?php
                                                                    }
                                                                }
                                                                ?>
                            </tbody>
                          </table>
                  </div>
                </fieldset>
          </div>          
                            
             
         <h4 class="accordion-title">Maid history</h4>    
         <div class="accordion-content">
                <fieldset>
                  <div class="col-sm-12 p-0" id="maid_history">
                       <table class="table table-striped table-bordered">
                            <thead>
                              <tr>
                                <th style="line-height: 18px; width: 20px"><center>
                                    Sl.No.
                                  </center>
                                </th>
                                <th style="line-height: 18px"> <center>
                                    Maid Name
                                  </center></th>
                                <th style="line-height: 18px"> <center>
                                    Date
                                  </center></th>
                                <th style="line-height: 18px"> <center>
                                    Shift
                                  </center></th>
                                <th style="line-height: 18px"> <center>
                                    Status
                                  </center></th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                                                            if (!empty($maid_history)) {
                                                                $i = 0;
                                                                foreach ($maid_history as $m_history) {
                                                                    echo '<tr>
                                                                            <td style="line-height: 18px; width: 20px"><center>' . ++$i . '</center></td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($m_history->maid_name) . '</td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($m_history->service_date) . '</td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($m_history->start_from_time) . '-' . html_escape($m_history->end_to_time) . '</td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($m_history->service_status) . '</td>
                                                                        </tr>';
                                                                }
                                                            } else {
                                                                echo '<tr><td style="line-height: 18px;" colspan="5"><center>No Records!</center></td>';
                                                            }
                                                            ?>
                            </tbody>
                          </table>
                  </div>
                </fieldset>
         </div>
              
              
         <h4 class="accordion-title">Payment History</h4>     
         <div class="accordion-content">
                <fieldset>
                  <div class="col-sm-12 p-0" id="payment_history">
                          <table class="table table-striped table-bordered">
                            <thead>
                              <tr>
                                <th style="line-height: 18px; width: 20px"><center>
                                    Sl.No.
                                  </center>
                                </th>
                                <th style="line-height: 18px"> <center>
                                    Date
                                  </center></th>
                                <th style="line-height: 18px"> <center>
                                    Shift
                                  </center></th>
                                <th style="line-height: 18px"> <center>
                                    Maid Name
                                  </center></th>
                                <th style="line-height: 18px"> <center>
                                    Extra Hours
                                  </center></th>
                                <th style="line-height: 18px"> <center>
                                    Payment
                                  </center></th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                                                            if (!empty($payment_history)) {
                                                                $i = 0;
                                                                foreach ($payment_history as $p_history) {
                                                                    echo '<tr>
                                                                            <td style="line-height: 18px; width: 20px"><center>' . ++$i . '</center></td>

                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($p_history->service_date) . '</td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($p_history->start_from_time) . '-' . html_escape($p_history->end_to_time) . '</td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($p_history->maid_name) . '</td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : right;">0</td>    
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : right;">' . html_escape($p_history->paid_amount) . '</td>
                                                                        </tr>';
                                                                }
                                                            } else {
                                                                echo '<tr><td style="line-height: 18px;" colspan="6"><center>No Records!</center></td>';
                                                            }
                                                            ?>
                            </tbody>
                          </table>
                  </div>
                </fieldset>
          </div>
              
              
          <h4 class="accordion-title">Current Services</h4>    
          <div class="accordion-content">
                <fieldset>
                  <div class="col-sm-12 p-0" id="current-service">
                          <table class="table table-striped table-bordered">
                            <thead>
                              <tr>
                                <th style="width: 60px">Sl. No.</th>
                                <th>Maid Name</th>
                                <th>Photo</th>
                                <th>Country</th>
                                <th width="300px">Shift</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                                                            if (!empty($current_service)) {
                                                                $i = 0;
                                                                foreach ($current_service as $service) {
                                                                    /*$path = './maidimg/thumb/'.$service->maid_photo_file;
                                                                    $path2 = './maidimg/'.$service->maid_photo_file;
                                                                    if (file_exists($path)) {
                                                                        $imgpath = base_url() . 'maidimg/thumb/' . html_escape($service->maid_photo_file);
                                                                    } elseif (file_exists($path2)){
                                                                        $imgpath = base_url() . 'maidimg/' . html_escape($service->maid_photo_file);
                                                                    } else {
                                                                        $imgpath = base_url() . 'img/no_image.jpg';
                                                                    }*/
                                                                    $imgpath = base_url($maid_val['maid_photo_file'] ? MAID_AVATAR_PATH . $maid_val['maid_photo_file'] : DEFAULT_AVATAR);
                                                                    //$filename = $service->maid_photo_file;
                                                                    //$color = substr($service->shifts, 0,2) == 'OD' ? '#43ACB0' :'#ff7223';
                                                                    $color = $service->booking_type == 'OD' ? '#8793d6' : ($service->booking_type == 'WE' ? '#7ac255' : '#fd8e41');
                                                                    //$service->shifts = str_replace(substr($service->shifts, 0,2) .'_', "", $service->shifts);
                                                                    echo '<tr>
                                                                            <td style="line-height: 18px; width: 20px"><center>' . ++$i . '</center></td>

                                                                            <td style="line-height: 18px;text-align : center;">' . html_escape($service->maid_name) . '</td>
                                                                            <td style="line-height: 18px;text-align : center;"><img style="width:25px; height:25px;" src="' . $imgpath . '" /></td>
                                                                            <td style="line-height: 18px;text-align : center;">' . html_escape($service->maid_nationality) . '</td>

                                                                            <td style="line-height: 18px;text-align : center; color:#ffffff;">' . ($service->shifts) . '</td>
                                                                                                                                        </tr>';
                                                                }
                                                            } else {
                                                                echo '<tr><td style="line-height: 18px;" colspan="5"><center>No Records!</center></td>';
                                                            }
                                                            ?>
                            </tbody>
                          </table>
                  </div>
                </fieldset>
			  
         </div>
              
               
              
         <h4 class="accordion-title">Pause Booking</h4>     
         <div class="accordion-content">
                <fieldset>
                  <div class="col-sm-12 p-0" id="pause-booking" style="position:relative;">
                        <div style="position: absolute;top: -36px;right: 5px;">
                          <input type="text" class="n-calendar-icon" id="b-date-from" data-date="<?php echo $search_date_from; ?>" readonly value="<?php echo $search_date_from; ?>" data-date-format="dd/mm/yyyy"/>
                          <input type="text" class="n-calendar-icon" id="b-date-to" data-date='<?php echo $search_date_to ?>' readonly value='<?php echo $search_date_to ?>' data-date-format="dd/mm/yyyy"/>
                          <!--<a href="javascript:;" class="btn btn-medium btn-success" id='btn-search-booking'>Search</a>-->
                          <input type="button" class="n-btn" id="btn-search-booking" value="Search" />
                        </div>

                          <table class="table table-striped table-bordered table-hover">
                            <thead>
                              <tr>
                                <th class="text-center">Sl. No.</th>
                                <th>Service Start Date</th>
                                <th>Day</th>
                                <th class="text-center" width="120px">Shift</th>
                                <th>Booking Type</th>
                                <th>Maid</th>
                                <th class="text-center" >Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                                                            if (!empty($bookings)) {
                                                                $i = 0;
                                                                foreach ($bookings as $subcat){
                                                                    foreach ($subcat as $booking)
                                                                    {
                                                                        $newDate = date("d/m/Y", strtotime($booking->scheduledates));
                                                                        //$onclick = "pause_booking(" . html_escape($booking->booking_id) . ", '" . html_escape($booking->scheduledates) . "', '" . html_escape($booking->booking_type) . "')";
                                                                        if($booking->booking_type == "WE"){
                                                                            $classes = "bg-we";
                                                                        } else if($booking->booking_type == "OD") {
                                                                            $classes = "bg-od";
                                                                        } else {
                                                                            $classes = "bg-bw";
                                                                        }
                                                                        $ndate = date("Y/m/d", strtotime($booking->scheduledates));
                                                                        $day = date('l', strtotime($ndate));
                                                                        //added by vishnu
                                                                        if($booking->booking_type == "OD")
                                                                            {
                                                                                $check_booking = $this->bookings_model->checkbooking_in_delete_oneday($booking->booking_id,$booking->scheduledates);
                                                                                if($check_booking == 0){
                                                                                    $onclick = "pause_booking_modal(" . html_escape($booking->booking_id) . ", '" . html_escape($booking->scheduledates) . "', '" . html_escape($booking->booking_type) . "')";
                                                                                    $pausetext = '<i class="btn-icon-only icon-pause"> </i>Pause';
                                                                                    $btnclass = "n-btn red-btn mb-0 m-auto";
                                                                                } else {
                                                                                    $pausetext = "<i class='btn-icon-only icon-play'> </i>Resume";
                                                                                    $onclick = "resume_booking_modal(" . html_escape($booking->booking_id) . ", '" . html_escape($booking->scheduledates) . "', '" . html_escape($booking->booking_type) . "')";
                                                                                    $btnclass = "n-btn mb-0";
                                                                                }
                                                                                $booking_type_text = $settings->od_booking_text;
                                                                            } 
                                                                        else if($booking->booking_type == "WE")
                                                                            {
                                                                                $check_booking = $this->bookings_model->checkbooking_in_delete_weekly($booking->booking_id,$booking->scheduledates);
                                                                                if($check_booking == 0){
                                                                                    $onclick = "pause_booking_modal(" . html_escape($booking->booking_id) . ", '" . html_escape($booking->scheduledates) . "', '" . html_escape($booking->booking_type) . "')";
                                                                                    $pausetext = '<i class="btn-icon-only icon-pause"> </i>Pause';
                                                                                    $btnclass = "n-btn red-btn mb-0 m-auto";
                                                                                } else {
                                                                                    $onclick = "resume_booking_modal(" . html_escape($booking->booking_id) . ", '" . html_escape($booking->scheduledates) . "', '" . html_escape($booking->booking_type) . "')";
                                                                                    $pausetext = "<i class='btn-icon-only icon-play'> </i>Resume";
                                                                                    $btnclass = "n-btn mb-0";
                                                                                }
                                                                                $booking_type_text = $settings->we_booking_text	;
                                                                            }
                                                                        else if($booking->booking_type == "BW")
                                                                            {
                                                                                $check_booking = $this->bookings_model->checkbooking_in_delete_weekly($booking->booking_id,$booking->scheduledates);
                                                                                if($check_booking == 0){
                                                                                    $onclick = "pause_booking_modal(" . html_escape($booking->booking_id) . ", '" . html_escape($booking->scheduledates) . "', '" . html_escape($booking->booking_type) . "')";
                                                                                    $pausetext = '<i class="btn-icon-only icon-pause"> </i>Pause';
                                                                                    $btnclass = "n-btn red-btn mb-0 m-auto";
                                                                                } else {
                                                                                    $onclick = "resume_booking_modal(" . html_escape($booking->booking_id) . ", '" . html_escape($booking->scheduledates) . "', '" . html_escape($booking->booking_type) . "')";
                                                                                    $pausetext = "<i class='btn-icon-only icon-play'> </i>Resume";
                                                                                    $btnclass = "n-btn mb-0";
                                                                                }
                                                                                $booking_type_text = $settings->bw_booking_text	;
                                                                            }
                                                                        
                                                                        //ends
                                                                        
																		
                                                                        echo '<tr>
                                                                            <td style="line-height: 18px;"><center>' . ++$i . '</center></td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($newDate) . '</td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . $day . '</td>
                                                                            <td style="line-height: 18px;"><span class="btn btn-block '.$classes.'">' . date("h:i A", strtotime($booking->time_from)) . ' - ' . date("h:i A", strtotime($booking->time_to)) . '</span></td>
                                                                            <td style="line-height: 18px;">' . $booking_type_text . '</td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($booking->maid_name) . '</td>    
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;"><button type="button" class="btn-block '.$btnclass.'" href="javascript:void" onclick="' . $onclick . '">'.$pausetext.'</button></td>
                                                                        </tr>';
                                                                    }
                                                                    
                                                                }
//                                                                foreach ($bookings as $booking) {
//                                                                    $onclick = "pause_booking(" . html_escape($booking->booking_id) . ", '" . ($booking->service_date) . "', '" . html_escape($booking->booking_type) . "')";
//                                                                    echo '<tr>
//                                                                            <td style="line-height: 18px;"><center>' . ++$i . '</center></td>
//                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($booking->service_date) . '</td>
//                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($booking->shift_day) . '</td>
//                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($booking->time_from) . '-' . html_escape($booking->time_to) . '</td>
//                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($booking->booking_type) . '</td>
//                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($booking->maid_name) . '</td>    
//                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;"><a class="btn btn-danger btn-small" href="javascript:void" onclick="' . $onclick . '"><i class="btn-icon-only icon-pause"> </i>Pause Booking</a></td>
//                                                                        </tr>';
//                                                                }
                                                            } else {
                                                                echo '<tr><td style="line-height: 18px;" colspan="7"><center>No Records!</center></td></tr>';
                                                            }
                                                            ?>
                            </tbody>
                          </table>
                  </div>
                </fieldset>
          </div>
          
          
          
         <h4 class="accordion-title">Booking</h4>    
         <div class="accordion-content" id="booking" style="display:<?= $this->uri->segment(4) ? 'block' : ''; ?>;">
               <style type="text/css">

                                .new-form-main { width: 100%; height: auto; padding: 0px 0px 20px 0px;}
                                .new-form-sub-box { width: 100%; height: auto; float: left; padding: 15px 0px 0px 15px;}
                                .div_left{ width: 50%; height: auto; float: left; padding: 0px 0px 0px 0px;}
                                .div_left-cont { width: 100%; height: auto; float: left; padding: 0px 0px 15px 0px;}
                                .div_right { width: 50%; height: auto; float: right; padding: 0px 0px 0px 0px;}

                                .new-form-sub-cont { width: 30%; height: auto; float: left; padding: 0px 0px 0px 0px;}
                                .new-form-sub-field { width: 30%; height: auto; float: left; padding: 0px 0px 0px 0px;}

                                .new-radio-sub-box { width: 25%; height: auto; float: left; padding: 0px 0px 0px 0px;}



                                /*
                                * Date - 12-09-16 : Date picker Calender style
                                */

                                .active.day { background: #00a642; color: #fff; }
                                .old.disabled.day { color: #CCC;}
                                .new.day { color: #CCC;}
                                .datepicker { padding: 5px 15px;}
                                .disabled.day {  color :#8f8f8f;}

                                #repeat-days-search,#repeat-ends-search { display: none;}

                                .cell3 > label { float: left; padding-right: 16px;}

                                input[disabled], select[disabled], textarea[disabled], input[readonly], select[readonly], textarea[readonly] { background-color: #fff;}
                            </style>


                            <!--Booking -->

                            <div class="tab-pane" id="booking">




                                <!--New starts-->
                                <fieldset>     

                                    <input type="hidden" id="cust_id" name="cust_id"  value="<?php echo $customer_id; ?>">
                                    <input type="hidden" id="cust_zone_id" name="cust_zone_id"  value="<?php echo $customer_zone_area_province->zone_id; ?>">
                                    <input type="hidden" id="cust_area_id" name="cust_area_id"  value="<?php echo $customer_zone_area_province->area_id; ?>">
                         <input type="hidden" id="cust_prov_id" name="cust_prov_id"  value="<?php echo $customer_zone_area_province->province_id; ?>">
                         <input type="hidden" id="booking_ID" name="booking_ID"  value="<?php if($book_ID != ""){ echo $book_ID; } ?>">
                         <?php if(@$justmop_areaid == "") { ?>
                         <input type="hidden" id="area_justmop" name="area_justmop" value="<?php echo $customer_zone_area_province->area_id; ?>">
                         <?php } else { ?> 
                         <input type="hidden" id="area_justmop" name="area_justmop" value="<?php echo $justmop_areaid; ?>">
                         <?php } ?>
                                    <input type="hidden" id="cust_add_id" name="cust_add_id"  value="<?php echo $customer_zone_area_province->customer_address_id; ?>">


                                    <div class="span12" style="width: 100%;margin-left: 0px;">
                                        <div class="widget" style="border: 0px;">
                                            <div class="widget widget-table action-table">

                                                <div class="widget-header"> <i class="icon-th-list"></i>
                                                    <h3>Filter Maid Details</h3> 
                                                </div>
                                                <!-- /widget-header -->


                                                <div class="widget-content">


                                                    <table class="table table-striped table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box ">
                                                                        <div class="new-form-sub-cont">Select Date</div><!--new-form-sub-cont end-->
                                                                        <div class="new-form-sub-field"><input type="text"  name="booking_date" id="booking_date" value="<?php echo $start_service_date ?>" /></div><!--new-form-sub-field end-->
                                                                        <div class="clear"></div><!--clear end-->
                                                                    </div><!--new-form-sub-main end-->
                                                                </td>
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box">
                                                                        <div class="new-form-sub-cont">Time From</div><!--new-form-sub-cont end-->
                                                                        <div class="new-form-sub-field"><select name="time_from" id="book-from-time" data-placeholder="Select">
                                                                                <!--<option value="">Select</option>-->
                                                                                <?php
                                                                                foreach ($times as $time_index => $time) {
                                                                                    $selected = $time->stamp == @$start_time_from ? 'selected="selected"' : '';
                                                                                    echo '<option value="' . $time->stamp . '" ' . $selected . ' >' . $time->display . '</option>';
                                                                                }
                                                                                ?>
                                                                            </select></div><!--new-form-sub-field end-->
                                                                        <div class="clear"></div><!--clear end-->
                                                                    </div><!--new-form-sub-main end-->

                                                                </td>
                                                            </tr>



                                                            <tr>
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box">
                                                                        <div class="new-form-sub-cont">Repeat</div><!--new-form-sub-cont end-->
                                                                        <div class="new-form-sub-field"><select name="booking_type" id="book-booking-type" data-placeholder="Select repeat type">
                                                                                <option value="">Select</option>
                                                                                <option value="OD" <?php if ($booking_type == "OD") { ?> selected="selected" <?php } ?>><?= $settings->od_booking_text; ?></option>
                                                                                <option value="WE" <?php if ($booking_type == "WE") { ?> selected="selected" <?php } ?>><?= $settings->we_booking_text; ?></option>
                                                                                <option value="BW" <?php if ($booking_type == "BW") { ?> selected="selected" <?php } ?>><?= $settings->bw_booking_text; ?></option>
                                                                            </select>
                                                                        </div><!--new-form-sub-field end-->

                                                                        <div class="clear"></div><!--clear end-->
                                                                    </div><!--new-form-sub-main end-->


                                                                    <div class="new-form-sub-box" id="repeat-ends-search" <?php if ($booking_type == "WE" || $booking_type == "BW") { ?> style="display:inline;" <?php } ?>>
                                                                        <div class="cell1" style="float: left;width: 20%;"><span class="icon_btype"></span> Ends</div>
                                                                        <div class="cell2" style="float: left;width: 2%;">:</div>
                                        <div class="cell3" style="float: left; width: 78%;">
                                                <label class="mr15"><input type="radio" name="repeat_end" id="repeat-end-never" value="never" <?php if(@$service_end==0) { ?> checked="checked" <?php } ?> /> Never</label>
						<label><input type="radio" name="repeat_end" id="repeat-end-ondate" value="ondate" <?php if(@$service_end==1) { ?> checked="checked" <?php } ?> /> On</label> 
						<input type="text" class="end_datepicker" id="repeat-end-date" data-date-format="dd/mm/yyyy" readonly disabled="disabled" value="<?php echo @$service_actual_end_date  ?>" />
                                        <div class="clear"></div><!--clear end-->
					</div>	

                                                                        <div class="clear"></div><!--clear end-->
                                                                    </div>   






                                                                </td>
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box">
                                                                        <div class="new-form-sub-cont">Time To</div><!--new-form-sub-cont end-->
                                                                        <div class="new-form-sub-field"><select name="time_to" id="book-to-time" data-placeholder="Select">
                                                                                <!--<option value="">Select</option>-->
                                                                                <?php
                                                                                foreach ($times as $time_index => $time) {
                                                                                    $selected = $time->stamp == @$start_time_to ? 'selected="selected"' : '';
                                                                                    if ($time_index == 't-0') {
                                                                                        continue;
                                                                                    }
                                                                                    echo '<option value="' . $time->stamp . '" ' . $selected . '>' . $time->display . '</option>';
                                                                                }
                                                                                ?>
                                                                            </select></div><!--new-form-sub-field end-->
                                                                        <div class="clear"></div><!--clear end-->
                                                                    </div><!--new-form-sub-main end-->

                                                                </td>
                                                            </tr>
                                                            <tr>
																<td style="line-height: 18px; width: 500px">
																<div class="new-form-sub-box">
                                                                            <div class="new-form-sub-cont">Total Amount</div><!--new-form-sub-cont end-->
                                                                            <div class="new-form-sub-field">
																			<?php
																			if($total_amount != "")
																			{
																				$tot_amt = $total_amount;
																			} else {
																				$tot_amt = "";
																			}
																			?>
																				<input type="number" id="c_total_amt" name="c_total_amt"  value="<?php echo $tot_amt; ?>">
                                                                            </div><!--new-form-sub-field end-->

                                                                            <div class="clear"></div><!--clear end-->
                                                                        </div><!--new-form-sub-main end-->
																</td>
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <?php
                                                                    if($justmop_areaid !="")
                                                                    {
                                                                    if ($justmop_areaid == 0): ?>

                                                                        <div class="new-form-sub-box">
                                                                            <div class="new-form-sub-cont">Select Area</div><!--new-form-sub-cont end-->
                                                                            <div class="new-form-sub-field">
                                                                                <select name="just_address_area" id="just_address_area">
                                                                                    <option value="0">Select Area</option>
                                                                                    <?php
                                                                                    foreach ($areas as $area) :
                                                                                        ?>
                                                                                        <option value="<?php echo $area['area_id']; ?>"><?php echo $area['area_name']; ?></option>

                                                                                        <?php
                                                                                    endforeach;
                                                                                    ?>                      

                                                                                </select>
                                                                            </div><!--new-form-sub-field end-->

                                                                            <div class="clear"></div><!--clear end-->
                                                                        </div><!--new-form-sub-main end-->
                                                                    <?php endif; ?>
                                                                    <?php } ?>
                                                                </td>
															</tr>
															<tr>
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box ">
                                                                        <div class="new-form-sub-cont">Cleaning Material</div><!--new-form-sub-cont end-->
																		<?php
																			if($cleaning_material == "Y")
																			{
																				$selected = 'checked="checked"';
																			} else {
																				$selected = "";
																			}
																			?>
                                                                        <!--<div class="new-form-sub-field">
                                                                        <input id="c-cleaning-materials" name="c_cleaning_materials" value="Y" <?php //echo $selected; ?> type="checkbox"> Yes
                                                                        </div>-->
                                                                        
                                                                        
                                                                        
                                                                        <div class="switch-main">         
                                                                            <label class="switch">
                                                                               <input type="checkbox" name="c-cleaning-materials" id="c-cleaning-materials" value="Y">
                                                                               <span class="slider round"></span>
                                                                            </label>
                                                                        </div>
                                                                        
                                                                        <div class="clear"></div><!--clear end-->
                                                                    </div><!--new-form-sub-main end-->
                                                                </td>
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box">
                                                                        <div class="new-form-sub-cont">Notes</div><!--new-form-sub-cont end-->
                                                                        <div class="new-form-sub-field">
																			<?php
																			if($booking_note != "")
																			{
																				$booking_note = $booking_note;
																			} else {
																				$booking_note = "";
																			}
																			?>
																			<textarea id="c-booking-note" class="popup-note-fld" style="width:225px;" name="c_booking_note" placeholder="Note to Driver"><?php echo $booking_note; ?></textarea>
																		</div><!--new-form-sub-field end-->
                                                                        <div class="clear"></div><!--clear end-->
                                                                    </div><!--new-form-sub-main end-->

                                                                </td>
                                                            </tr>
															<tr>
																<td style="line-height: 18px; width: 500px">
																</td>
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box">
                                                                        <div class="new-form-sub-cont">&nbsp;</div><!--new-form-sub-cont end-->
                                                                        <div class="new-form-sub-field">
                                                                            <input type="button" class="save-but n-btn" id="btn-search-maid" value="Search" />
                                                                        </div><!--new-form-sub-field end-->
                                                                        <div class="clear"></div><!--clear end-->
                                                                    </div><!--new-form-sub-main end-->

                                                                </td>
                                                            </tr>

                                                        </tbody>
                                                    </table>

                                                </div><!-- /widget-content --> 





                                            </div> 
                                        </div> <!-- /widget -->
                                    </div> <!-- /span6 -->

                                </fieldset> 
                                <!--New endss-->

                                <fieldset>
                                    <div style="clear:both"></div><br>

                                    <div id="LoadingImage" style="text-align:center;display:none;width:50px;height:50px;"><img src="<?php echo base_url() ?>img/loader.gif"></div>
                                    <!--<div class="widget-content" id="maid_search">-->
                                    <div id="maid_search" style="border: medium none !important;"></div>
                                </fieldset>   


                            </div>

         </div> 
              
              
              
              
          <h4 class="accordion-title">Send Mail</h4>       
          <div class="accordion-content" id="sent-mail">
                        <table class="table table-striped table-bordered">
                          <tbody>
                            <tr>
                              <td style="line-height: 18px; width: 500px"><div class="new-form-sub-box ">
                                  <div class="new-form-sub-cont">Amount</div>
                                  <!--new-form-sub-cont end-->
                                  <div class="new-form-sub-field">
                                    <input type="number"  name="send_amount" id="send_amount" value="" />
                                  </div>
                                  <!--new-form-sub-field end-->
                                  <div class="clear"></div>
                                  <!--clear end--> 
                                </div>
                                <!--new-form-sub-main end--></td>
                              <td style="line-height: 18px; width: 500px"><div class="new-form-sub-box"> 
                                  <!--new-form-sub-field end-->
                                  <div class="clear"></div>
                                  <!--clear end--> 
                                </div>
                                <!--new-form-sub-main end--></td>
                            </tr>
                            <tr>
                              <td style="line-height: 18px; width: 500px"><div class="new-form-sub-box">
                                  <div class="new-form-sub-cont">Description</div>
                                  <!--new-form-sub-cont end-->
                                  <div class="new-form-sub-field">
                                    <textarea name="send_description" id="send_description"></textarea>
                                    <input type="hidden" id="custtid" name="custtid" value="<?php echo $customer_id;?>"/>
                                  </div>
                                  <!--new-form-sub-field end-->
                                  
                                  <div class="clear"></div>
                                  <!--clear end--> 
                                </div>
                                <!--new-form-sub-main end--></td>
                              <td style="line-height: 18px; width: 500px"><div class="new-form-sub-box"> 
                                  <!--new-form-sub-field end-->
                                  <div class="clear"></div>
                                  <!--clear end--> 
                                </div>
                                <!--new-form-sub-main end--></td>
                            </tr>
                            <tr>
                              <td style="line-height: 18px; width: 500px"><div class="new-form-sub-box">
                                  <div class="new-form-sub-cont">&nbsp;</div>
                                  <!--new-form-sub-cont end-->
                                  <div class="new-form-sub-field" style="width: auto !important;">
                                    <input type="button" class="save-but n-btn" id="btn-snd-mail" value="Send" />
									<input type="button" class="save-but n-btn" id="btn-snd-mail-whatsapp" value="Whatsapp" onclick="open_link()" />
									<input type="button" class="save-but n-btn" id="btn-snd-mail-copy" value="Copy Link" onclick="copy_link()" />
									<a href='#' target='_blank' id='open_link'></a>
                                  </div>
                                  <!--new-form-sub-field end-->
                                  <div class="clear"></div>
                                  <!--clear end--> 
                                </div></td>
                              <td style="line-height: 18px; width: 500px"></td>
                            </tr>
                          </tbody>
                        </table>
          </div>
          <h4 class="accordion-title">Statement</h4>
           <div class="accordion-content" id="statement">
           <table class="table table-striped table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box ">
                                                                        <div class="new-form-sub-cont">Statement Type</div><!--new-form-sub-cont end-->
                                                                        <div class="new-form-sub-field">
																			<select id="statementtype" name="statementtype">
																				<option value="">Select a type</option>
																				<option value="1">All</option>
																				<option value="2">Select Date Range</option>
																			</select>
																			<input type="hidden" id="cust-stat-id" name="cust-stat-id" value="<?php echo $customer_id;?>"/>
																		</div>
                                                                        <div class="clear"></div>
                                                                    </div>

                                                                    
                                                                    <!--<div class="new-form-sub-box statementrange" style="display: none;">
                                                                        <div class="new-form-sub-cont">Start Date</div>
                                                                        <div class="new-form-sub-field">
                                                                            <input type="text" id="cust_state_frmdt" name="cust_state_frmdt" value="<?php// echo date('d/m/Y');?>"/>
                                                                        </div>
                                                                        <div class="clear"></div>
                                                                    </div>


                                                                    <div class="new-form-sub-box statementrange" style="display: none;">
                                                                        <div class="new-form-sub-cont">End Date</div>
                                                                        <div class="new-form-sub-field">
                                                                            <input type="text" id="cust_state_todt" name="cust_state_todt" value="<?php// echo date('d/m/Y');?>"/>
                                                                        </div>
                                                                        <div class="clear"></div>
                                                                    </div>-->

                                                                </td>
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box">
                                                                        <div class="clear"></div>
                                                                    </div>

                                                                </td>
                                                            </tr>



                                                            <tr class="dateview" style="display:none;">
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box">
                                                                        <div class="new-form-sub-cont">Start Date</div>
                                                                        <div class="new-form-sub-field">
                                                                            <input type="text" id="startdate" name="startdate" value=""/>
                                                                        </div>

                                                                        <div class="clear"></div>
                                                                    </div>


                                                                     

                                                                </td>
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box">
                                                                        <div class="clear"></div>
                                                                    </div>

                                                                </td>
                                                            </tr>
															<tr class="dateview" style="display:none;">
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box">
                                                                        <div class="new-form-sub-cont">End Date</div>
                                                                        <div class="new-form-sub-field">
                                                                            <input type="text" id="enddate" name="enddate" value=""/>
                                                                        </div>

                                                                        <div class="clear"></div>
                                                                    </div>


                                                                     

                                                                </td>
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box">
                                                                        <div class="clear"></div>
                                                                    </div>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                            <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box">
                                                                        <div class="new-form-sub-cont">&nbsp;</div>
                                                                        <div class="new-form-sub-field">
                                                                            <input type="button" class="save-but" id="view-statement" value="View" />
                                                                        </div>
                                                                        <div class="clear"></div>
                                                                    </div>
                                                            </td>
                                                            <td style="line-height: 18px; width: 500px"></td>   
                                                            </tr>
                                                            

                                                        </tbody>
                                                    </table>
          </div>
            
            
          </form>
</div>
          
        </div>
      </div>
      <!-- /widget-content --> 
    </div>
    <!-- /widget --> 
  </div>
  <!-- /span8 --> 
</div>
<!-- /row -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script>
  $(function() {
	let current = window.location.href;
	$('#primary_nav_wrap li a').each(function() {
		var $this = $(this);
		// if the current path is like this link, make it active
		if ($this.attr('href') === '<?php echo base_url('customers'); ?>') {
			$this.addClass('active');
		}
	})
})
var _pause_booking_id = '';
var _pause_booking_date = '';
var _pause_booking_type = '';

function pause_booking_modal(booking_id, schedule_date, booking_type) {
	_pause_booking_id = booking_id;
	_pause_booking_date = schedule_date;
	_pause_booking_type = booking_type;
	fancybox_show('pause-popup');
}
function resume_booking_modal(booking_id, schedule_date, booking_type) {
	_pause_booking_id = booking_id;
	_pause_booking_date = schedule_date;
	_pause_booking_type = booking_type;
	fancybox_show('resume-popup');
}

function confirm_pause() {
	$.post(_page_url, {
		action: 'pause-booking',
		booking_id: _pause_booking_id,
		service_date: _pause_booking_date,
		booking_type: _pause_booking_type
	}, function(response) {
		if (response == 'error') {
			alert('Unexpected error!');
		} else if (response == 'locked') {
			alert('Booking Locked by another user!');
		} else {
			$('#btn-search-booking').click();
      closeFancy();
		}
	});
}

function confirm_resume() {
	$.post(_page_url, {
		action: 'start-booking',
		booking_id: _pause_booking_id,
		service_date: _pause_booking_date,
		booking_type: _pause_booking_type
	}, function(response) {
		if (response == 'error') {
			alert('Unexpected error!');
		} else if (response == 'locked') {
			alert('Booking Locked by another user!');
		} else {
			$('#btn-search-booking').click();
      closeFancy();
		}
	});
}

$('body').on('change', '#statementtype', function () {
	var statementtype = $("#statementtype").val();
	if (statementtype == 2) {
		$(".dateview").show();
	} else {
		$(".dateview").hide();
	}
});
$('body').on('click', '#view-statement', function () {
    var statementtype = $("#statementtype").val();
	if(statementtype == "")
	{
		alert("Select any option");
		return false;
	}
	
	if (statementtype == 2) 
	{
        var startdate = $("#startdate").val();
        if (startdate == "") {
            alert("Select start date");
            return false;
        }
		var enddate = $("#enddate").val();
        if (enddate == "") {
            alert("Select end date");
            return false;
        }
    }
	
	
	
	
	
    // var starttdate = $("#cust_state_frmdt").val();
    // var enddate = $("#cust_state_todt").val();

    // if (statementtype == "" || !$.isNumeric(statementtype)) {
        // alert("Select one type");
        // return false;
    // }

    // if (statementtype == 2) {
        // var startdate = $("#startdate").val();
        // if (startdate == "") {
            // alert("Select start date");
            // return false;
        // }
    // }
    // else {
        // var startdate = "";
    // }

    // if (starttdate == "") {
        // alert("Select start date");
        // return false;
    // }

    // if (enddate == "") {
        // alert("Select end date");
        // return false;
    // }

    // var sdt = new Date(starttdate.split("/").reverse().join("-")); var tdt = new Date(enddate.split("/").reverse().join("-"));

    // if (sdt > tdt) {
        // alert("Start date cannot be greater than end date");
        // return false;
    // }
    // sdt = starttdate.split("/").reverse().join("-"); tdt = enddate.split("/").reverse().join("-");
    var custid = $("#cust-stat-id").val();
    // window.open(_base_url + 'customer/view_customer_statement_new/' + custid + '/' + statementtype + '/' + sdt + '/' + tdt, '_blank');
	if(statementtype == 2)
	{
		window.open(_base_url + 'customer/view_customer_statement_new/' + custid + '/' + statementtype + '/' + startdate + '/' + enddate, '_blank');
	} else {
		window.open(_base_url + 'customer/view_customer_statement_new/' + custid + '/' + statementtype, '_blank');
	}




    // $('.mm-loader').css('display','block');
    // $.ajax({
    // type: "POST",
    // url: _base_url + "customer/view_customer_statement",
    // data: {statementtype: statementtype,startdate: startdate,custid: custid},
    // dataType: "text",
    // cache: false,
    // success: function (result) {
    // $('.mm-loader').css('display','none');
    // alert(result);
    // return false;
    // var _alert_html = "";
    // if(result == "success")
    // {
    // $("#statementtype").val('');
    // $("#startdate").val('');
    // _alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Email has been sent successfully.</div><div class="bottom"><input type="button" value="OK" class="pop_close alert-popup-close" style="float:none;" /></div></div>';
    // } else {
    // _alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close"></span></div><div class="content padd20">No email found. Try again...</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close alert-popup-close" style="float:none;"  /></div></div>';
    // }
    // if(_alert_html  != '')
    // {
    // $.fancybox.open({
    // autoCenter : true,
    // fitToView : false,
    // scrolling : false,
    // openEffect : 'fade',
    // openSpeed : 100,
    // helpers : {
    // overlay : {
    // css : {
    // 'background' : 'rgba(0, 0, 0, 0.3)'
    // },
    // closeClick: false
    // }
    // },
    // padding : 0,
    // closeBtn : false,
    // content: _alert_html
    // });
    // }
    // }
    // });


});
</script>