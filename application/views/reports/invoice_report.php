<script src="https://unpkg.com/xlsx/dist/shim.min.js"></script>
<script src="https://unpkg.com/xlsx/dist/xlsx.full.min.js"></script>
<script src="https://unpkg.com/blob.js@1.0.1/Blob.js"></script>
<script src="https://unpkg.com/file-saver@1.3.3/FileSaver.js"></script>
<style type="text/css">
  .widget .widget-header {
    margin-bottom: 0px;
  }


  table.da-table tr td {
    padding: 0px 6px;
  }

  .datepicker-days table .disabled-date.day {
  background: #ffe1e1;
    color: #9b9b9b;
}

.datepicker table tr td.disabled,
.datepicker table tr td.disabled:hover {
  background: #ffe1e1;
    color: #9b9b9b;
}
</style>
<div class="row m-0">
  <div class="col-md-12">
    <div class="widget widget-table action-table">
      <div class="widget-header">
        <form class="form-horizontal" method="POST" action="<?=base_url('reports/invoice_report');?>">
      <ul>
      <li><i class="icon-th-list"></i>
        <h3>Invoice Report</h3></li>
        <li>
          <input type="text" name="filter_from_date" id="filter_from_date" autocomplete="off" placeholder="dd/mm/yyyy" value="<?=$filter_from_date;?>" readonly>
        </li>
        <li>
          <input type="text" name="filter_to_date" id="filter_to_date" autocomplete="off" placeholder="dd/mm/yyyy" value="<?=$filter_to_date;?>" readonly>
        </li>
        <li>
          <input type="submit" class="n-btn" id="customer-statement-search" value="Search">
        </li>
        <li class="mr-0 float-right">
        <div class="topiconnew border-0 green-btn">
        	<a data-action="excel-export" title=""><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
        </div>
        </li>
        </ul>
        </form>
      </div>
      <!-- /widget-header -->
      <div class="widget-content">
        <table id="holiday-list-table" class="table table-hover da-table" width="100%">
          <thead>
            <tr>
              <th>
                Date
              </th>
              <th>
                Invoice No.
              </th>
              <th>
                Customer
              </th>
              <th>
                Amount
              </th>
              <th>
                Status
              </th>
            </tr>
          </thead>
          <tbody>
          <?php 
          if (count($invoices) > 0) :
          foreach ($invoices as $key => $invoice): 
          ?>
                <tr>
                  <td><?=DateTime::createFromFormat('Y-m-d', $invoice['invoice_date'])->format('d-m-Y') ?></td>
                  <td><?=$invoice['invoice_num']?></td>
                  <td><?=$invoice['customer_name']?></td>
                  <td><?=$invoice['invoice_net_amount']?></td>
                  <td>
                  <?=$invoice['invoice_status'] == 3 ? 'Paid' : 'Open'?>
                  </td>
                </tr>
          <?php
          endforeach;
          endif;
          ?>
          </tbody>
        </table>
        <table id="excel-table" style="display:none" width="100%">
          <thead>
            <tr>
              <th style="width:150px;">
              Invoice Date
              </th>
              <th>
                Invoice No.
              </th>
              <th>
                Customer Name
              </th>
              <th>
                Amount (AED)
              </th>
              <th>
                Status
              </th>
            </tr>
          </thead>
          <tbody>
          <?php 
          if (count($invoices) > 0) :
          foreach ($invoices as $key => $invoice): 
          ?>
                <tr>
                  <td><?=DateTime::createFromFormat('Y-m-d', $invoice['invoice_date'])->format('d-m-Y') ?></td>
                  <td><?=$invoice['invoice_num']?></td>
                  <td><?=$invoice['customer_name']?></td>
                  <td><?=$invoice['invoice_net_amount']?></td>
                  <td>
                  <?=$invoice['invoice_status'] == 3 ? 'Paid' : 'Open'?>
                  </td>
                </tr>
          <?php
          endforeach;
          endif;
          ?>
          </tbody>
        </table>
      </div>
      <!-- /widget-content -->
    </div>
    <!-- /widget -->
  </div>
  <!-- /span6 -->
</div>
<!-- Modal -->
<script>
(function(a) {
	a(document).ready(function(b) {
		if (a('#holiday-list-table').length > 0) {
			a("table#holiday-list-table").dataTable({
				'sPaginationType': "full_numbers",
				"bSort": true,
				"iDisplayLength": 100,
				"scrollY": true,
				"orderMulti": false,
				"scrollX": true,
				"aaSorting": []
			});
		}
	});
})(jQuery);
$(function() {
    $('input[name="filter_from_date"]').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    });
    $('input[name="filter_to_date"]').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    });
});
$('[data-action="excel-export"]').click(function(){
  var fileName = "Invoice Report "+ "<?= str_replace("/", "_", $filter_from_date) ?>" + " - <?= str_replace("/", "_", $filter_to_date) ?>";
  var fileType = "xlsx";
  var table = document.getElementById("excel-table");
  var wb = XLSX.utils.table_to_book(table, {sheet: "Report",dateNF:'dd/mm/yyyy;@',cellDates:true, raw: true, rawNumbers: false});
  const ws = wb.Sheets['Report'];
  var wscols = [
    {wch:15},
    {wch:25},
    {wch:30},
    {wch:25},
    {wch:20}
  ];
  ws['!cols'] = wscols;
    return XLSX.writeFile(wb, null || fileName + "." + (fileType || "xlsx"));
});

$('[data-action="excel-export-"]').click(function(){
	$('[data-action="excel-export"]').html('<i class="fa fa-spinner fa-spin"></i>');
	var fromdate = $('#filter_from_date').val();
	var todate = $('#filter_to_date').val();

	$.ajax({
		type: "POST",
		url: _base_url + 'customerexcel/invoice_report_excel',
		data: { filter_from_date: fromdate, filter_to_date: todate },
		cache: false,
		success: function (response) {
			// $('[data-action="excel-export"]').html('<i class="fa fa-file-excel-o"></i>');
			// window.location = response;
			alert(response);
		}
	});
});
	
</script>