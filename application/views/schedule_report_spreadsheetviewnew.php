<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=ScheduleReport.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border="1">
    <thead>
        <tr>
            <th style="line-height: 18px; padding: 10px;">Sl No</th>
			<th style="line-height: 18px; padding: 10px;">Name</th>
			<th style="line-height: 18px; padding: 10px;">Phone</th>
			<th style="line-height: 18px; padding: 10px;">Date of Service</th>
			<th style="line-height: 18px; padding: 10px;">Booking Type</th>
			<th style="line-height: 18px; padding: 10px;">Cleaner</th>
			<th style="line-height: 18px; padding: 10px;">Amount</th>
			<th style="line-height: 18px; padding: 10px;">Status</th>
			<th style="line-height: 18px; padding: 10px;">Zone</th>
			<th style="line-height: 18px; padding: 10px;">Driver</th>
        </tr>
    </thead>
    <tbody>
		<?php
		if(!empty($reports))
		{
			$i = 0;
			$totamount = 0;
			foreach ($reports as $report)
			{
				$totamount += $report->total_amount;
		?>
			<tr>
				<td style="line-height: 18px;"><?php echo ++$i; ?></td>
				<td style="line-height: 18px;"><?php echo $report->customer_name; ?></td>
				<td style="line-height: 18px;"><?php echo $report->mobile_number_1; ?></td>
				<td style="line-height: 18px;"><?php echo $payment_date ?></td>
				<td style="line-height: 18px;">
					<?php
					if($report->booking_type == "OD")
					{
						echo "One Day";
					} else if($report->booking_type == "WE"){
						echo "Weekly";
					} else if($report->booking_type == "BW"){
						echo "Bi-Weekly";
					}
					?>
				</td>
				<td style="line-height: 18px;"><?php echo $report->maid_name; ?></td>
				<td style="line-height: 18px;"><?php echo $report->total_amount; ?></td>
				<td style="line-height: 18px;">
				<?php
				if($report->service_status == 1)
				{
					echo "In Progress";
				} else if($report->service_status == 2){
					echo "Finished";
				} else if($report->service_status == 3){
					echo "Cancelled";
				} else {
					echo "Not Started";
				}
				?>
				</td>
				<td style="line-height: 18px; text-align: center;">
					<?php
					echo $report->zone.' ('.$report->driver.')'; 
					?>
				</td>
				<td style="line-height: 18px; text-align: center;">
					<?php
					if($report->serviceadd == "T")
					{
						echo $report->serviceaddby;
					}
					?>
				</td>
			</tr>
		<?php
			}
		}
		
		?>
	</tbody>
	<tfoot>
		<tr>
			<td style="line-height: 18px;"></td>
			<td style="line-height: 18px;"></td>
			<td style="line-height: 18px;"></td>
			<td style="line-height: 18px;"></td>
			<td style="line-height: 18px;"></td>
			<td style="line-height: 18px;">Total</td>
			<td style="line-height: 18px;"><?php echo $totamount; ?></td>
			<td style="line-height: 18px;"></td>
			<td style="line-height: 18px;"></td>
		</tr>
	</tfoot>
</table>