<html>
<head>
	<!-- mithumb w=768 h=1024 t_w=150 t_h=200 -->
    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
  <![endif]-->
    <!--[if mso]>
        <style>
            * {
                font-family: sans-serif !important;
            }
        </style>
    <![endif]-->
    <style type="text/css">
/*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */

        /* Document
    ========================================================================== */

        /**
    * 1. Correct the line height in all browsers.
    * 2. Prevent adjustments of font size after orientation changes in iOS.
    */


html {
line-height: 1.15;
/* 1 */
-webkit-text-size-adjust: 100%;/* 2 */
}
/* Sections
    ========================================================================== */

        /**
    * Remove the margin in all browsers.
    */

body {
margin: 0px;
}
/**
    * Render the `main` element consistently in IE.
    */

main {
display: block;
}
/**
    * Correct the font size and margin on `h1` elements within `section` and
    * `article` contexts in Chrome, Firefox, and Safari.
    */

h1 {
font-size: 2em;
margin: 0.67em 0px;
}
/* Grouping content
    ========================================================================== */

        /**
    * 1. Add the correct box sizing in Firefox.
    * 2. Show the overflow in Edge and IE.
    */

hr {
box-sizing: content-box;
/* 1 */
height: 0px;
/* 1 */
overflow: visible;/* 2 */
}
/**
    * 1. Correct the inheritance and scaling of font size in all browsers.
    * 2. Correct the odd `em` font sizing in all browsers.
    */

pre {
font-family: monospace, monospace;
/* 1 */
font-size: 1em;/* 2 */
}
/* Text-level semantics
    ========================================================================== */

        /**
    * Remove the gray background on active links in IE 10.
    */

a {
background-color: transparent;
}
/**
    * 1. Remove the bottom border in Chrome 57-
    * 2. Add the correct text decoration in Chrome, Edge, IE, Opera, and Safari.
    */

abbr[title] {
border-bottom: none;
/* 1 */
text-decoration: underline;
/* 2 */
text-decoration: underline dotted;/* 2 */
}
/**
    * Add the correct font weight in Chrome, Edge, and Safari.
    */

b,  strong {
font-weight: bolder;
}
/**
    * 1. Correct the inheritance and scaling of font size in all browsers.
    * 2. Correct the odd `em` font sizing in all browsers.
    */

code,  kbd,  samp {
font-family: monospace, monospace;
/* 1 */
font-size: 1em;/* 2 */
}
/**
    * Add the correct font size in all browsers.
    */

small {
font-size: 80%;
}
/**
    * Prevent `sub` and `sup` elements from affecting the line height in
    * all browsers.
    */

sub,  sup {
font-size: 75%;
line-height: 0px;
position: relative;
}
/* Embedded content
    ========================================================================== */

        /**
    * Remove the border on images inside links in IE 10.
    */

img {
border-style: none;
}
/* Forms
    ========================================================================== */

        /**
    * 1. Change the font styles in all browsers.
    * 2. Remove the margin in Firefox and Safari.
    */

button,  input,  optgroup,  select,  textarea {
font-family: inherit;
/* 1 */
font-size: 100%;
/* 1 */
line-height: 1.15;
/* 1 */
margin: 0px;/* 2 */
}
/**
    * Show the overflow in IE.
    * 1. Show the overflow in Edge.
    */

button,  input {
/* 1 */
overflow: visible;
}
/**
    * Remove the inheritance of text transform in Edge, Firefox, and IE.
    * 1. Remove the inheritance of text transform in Firefox.
    */

button,  select {
/* 1 */
text-transform: none;
}

        /**
    * Correct the inability to style clickable types in iOS and Safari.
    */

        button,  [type="button"],  [type="reset"],  [type="submit"] {
 -webkit-appearance: button;
}

        /**
    * Remove the inner border and padding in Firefox.
    */

        button::-moz-focus-inner,  [type="button"]::-moz-focus-inner,  [type="reset"]::-moz-focus-inner,  [type="submit"]::-moz-focus-inner {
 border-style: none;
 padding: 0px;
}

        /**
    * Restore the focus styles unset by the previous rule.
    */

        button:-moz-focusring,  [type="button"]:-moz-focusring,  [type="reset"]:-moz-focusring,  [type="submit"]:-moz-focusring {
 outline: 1px dotted ButtonText;
}
/**
    * Correct the padding in Firefox.
    */

fieldset {
padding: 0.35em 0.75em 0.625em;
}
/**
    * 1. Correct the text wrapping in Edge and IE.
    * 2. Correct the color inheritance from `fieldset` elements in IE.
    * 3. Remove the padding so developers are not caught out when they zero out
    *    `fieldset` elements in all browsers.
    */

legend {
box-sizing: border-box;
/* 1 */
color: inherit;
/* 2 */
display: table;
/* 1 */
max-width: 100%;
/* 1 */
padding: 0px;
/* 3 */
white-space: normal;/* 1 */
}
/**
    * Add the correct vertical alignment in Chrome, Firefox, and Opera.
    */

progress {
vertical-align: baseline;
}
/**
    * Remove the default vertical scrollbar in IE 10+.
    */

textarea {
overflow: auto;
}

        /**
    * 1. Add the correct box sizing in IE 10.
    * 2. Remove the padding in IE 10.
    */

        [type="checkbox"],  [type="radio"] {
 box-sizing: border-box;
            /* 1 */
            padding: 0px;
/* 2 */
}

        /**
    * Correct the cursor style of increment and decrement buttons in Chrome.
    */

        [type="number"]::-webkit-inner-spin-button,  [type="number"]::-webkit-outer-spin-button {
 height: auto;
}

        /**
    * 1. Correct the odd appearance in Chrome and Safari.
    * 2. Correct the outline style in Safari.
    */

        [type="search"] {
 -webkit-appearance: textfield;
            /* 1 */
            outline-offset: -2px;
/* 2 */
}

        /**
    * Remove the inner padding in Chrome and Safari on macOS.
    */

        [type="search"]::-webkit-search-decoration {
 -webkit-appearance: none;
}

        /**
    * 1. Correct the inability to style clickable types in iOS and Safari.
    * 2. Change font properties to `inherit` in Safari.
    */

        ::-webkit-file-upload-button {
 -webkit-appearance: button;
            /* 1 */
            font: inherit;
/* 2 */
}
/* Interactive
    ========================================================================== */

        /*
    * Add the correct display in Edge, IE 10+, and Firefox.
    */

details {
display: block;
}
/*
    * Add the correct display in all browsers.
    */

summary {
display: list-item;
}
/* Misc
    ========================================================================== */

        /**
    * Add the correct display in IE 10+.
    */

template {
display: none;
}

        /**
    * Add the correct display in IE 10.
    */

        [hidden] {
 display: none;
}
/* What it does: Remove spaces around the email design added by some email clients. */
        /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
html,  body {
margin: 0 auto !important;
padding: 0 !important;
height: 100% !important;
width: 100% !important;
}
/* What it does: Stops email clients resizing small text. */
* {
-ms-text-size-adjust: 100%;
-webkit-text-size-adjust: 100%;
}
/* What it does: Centers email on Android 4.4 */
div[style*="margin: 16px 0"] {
margin: 0 !important;
}
/* What it does: forces Samsung Android mail clients to use the entire viewport */
#MessageViewBody,  #MessageWebViewDiv {
width: 100% !important;
}
/* What it does: Stops Outlook from adding extra spacing to tables. */
table,  td {
mso-table-lspace: 0pt !important;
mso-table-rspace: 0pt !important;
}
/* What it does: Replaces default bold style. */
th {
font-weight: normal;
}
/* What it does: Fixes webkit padding issue. */
table {
border-spacing: 0 !important;
border-collapse: collapse !important;
table-layout: fixed !important;
margin: 0 auto !important;
}
/* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
a {
text-decoration: none;
}
/* What it does: Uses a better rendering method when resizing images in IE. */
img {
-ms-interpolation-mode: bicubic;
}
/* What it does: A work-around for email clients meddling in triggered links. */
a[x-apple-data-detectors],         /* iOS */
        .unstyle-auto-detected-links a,  .aBn {
border-bottom: 0 !important;
cursor: default !important;
color: inherit !important;
text-decoration: none !important;
font-size: inherit !important;
font-family: inherit !important;
font-weight: inherit !important;
line-height: inherit !important;
}
/* What it does: Prevents Gmail from changing the text color in conversation threads. */
.im {
color: inherit !important;
}
/* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
.a6S {
display: none !important;
opacity: 0.01 !important;
}
/* If the above doesn't work, add a .g-img class to any image in question. */
img.g-img+div {
display: none !important;
}

        /* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
        /* Create one of these media queries for each additional viewport size you'd like to fix */

        /* iPhone 4, 4S, 5, 5S, 5C, and 5SE */
        @media only screen and (min-device-width: 320px) and (max-device-width: 374px) {
u~div .email-container {
min-width: 320px !important;
}
}

        /* iPhone 6, 6S, 7, 8, and X */
        @media only screen and (min-device-width: 375px) and (max-device-width: 413px) {
u~div .email-container {
min-width: 375px !important;
}
}

        /* iPhone 6+, 7+, and 8+ */
        @media only screen and (min-device-width: 414px) {
u~div .email-container {
min-width: 414px !important;
}
}
/* PYKSYS */

u+#body a {
color: inherit;
text-decoration: none;
font-size: inherit;
font-family: inherit;
font-weight: inherit;
line-height: inherit;
}
body {
margin: 0px;
padding: 0px;
-webkit-text-size-adjust: 100%;
-ms-text-size-adjust: 100%;
}
table,  th,  td {
border-collapse: collapse;
mso-table-lspace: 0pt;
mso-table-rspace: 0pt;
}
img {
border: 0px;
height: auto;
line-height: 100%;
outline: none;
text-decoration: none;
-ms-interpolation-mode: bicubic;
}
 @media print {
* {
-webkit-print-color-adjust: exact
}
}
.hide {
width: 0px !important;
height: 0px !important;
color: transparent !important;
line-height: 0 !important;
overflow: hidden !important;
font-size: 0px !important;
display: none !important;
visibility: hidden !important;
opacity: 0 !important;
mso-hide: all !important;
}
 @media screen and (max-width: 639px) {
.email-container {
width: 100% !important;
margin: auto !important
}
th.stack-column,  th.stack-column-center {
display: block !important;
width: 100% !important;
max-width: 100% !important;
direction: ltr !important;
padding-left: 0px !important;
padding-right: 0px !important
}
th.stack-column-center,  th.stack-column-center th,  th.stack-column-center td {
text-align: center !important
}
td.stack-column,  td.stack-column-center {
display: block !important;
width: 100% !important;
max-width: 100% !important;
direction: ltr !important;
padding-left: 0px !important;
padding-right: 0px !important
}
td.stack-column-center,  td.stack-column-center th,  td.stack-column-center td {
text-align: center !important
}
.padding-0-on-mobile {
padding: 0px !important;
}
.hide-on-mobile {
width: 0px !important;
height: 0px !important;
color: transparent !important;
line-height: 0 !important;
overflow: hidden !important;
font-size: 0px !important;
display: none !important;
visibility: hidden !important;
opacity: 0 !important;
mso-hide: all !important;
}
.mb-10 {
margin-bottom: 10px !important;
}
.mt-20 {
margin-top: 20px !important;
}
.mobile-width {
width: 80% !important;
}
}
:root {
color-scheme: light only;
}
</style>
    <!--[if mso]>
    <style>
      sup {
        font-size: 100% !important;
      }
    </style>
  <![endif]-->
</head>
<body>
<div class="main-wrapper" style="width: 800px; margin:0 auto; background:#ddf8ed;">
	<div style="width: 800px;"><img src="<?php echo base_url(); ?>images/email-banner.jpg?v=1" width="800" height="303" alt="" /></div>
	<div style="width: 798px; background:#ddf8ed;">
		<div style="width: 600px; height:auto; margin: 0 auto; padding: 0px 30px;">
			<div style="width: 600px; height:auto; padding: 0px 0px 30px 0px; margin-bottom: 30px;">
				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding:20px 0px 0px 0px; font-weight: bold; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Dear <?php echo $customer_name  ?>,</p>
				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Welcome to Dubaihousekeeping Cleaning Services!...</p>
				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Thanks for using our service. It seems like you have some payments due. If you wish to make the payment, click the below button.</p>
				<br>
				<a href="https://booking.dubaihousekeeping.com/payment?id=<?php echo $customer_id; ?>&amount=<?php echo $balance; ?>&message=<?php echo $description;?>" style="background:#0054a6; color:#fff; padding:15px 20px 10px 15px; border-radius:5px; text-decoration:none; font-weight:bold; font-size:16px;">
                                                                                                        <img src="<?php echo base_url(); ?>images/payicon.png" width="32" height="24">&nbsp;&nbsp;Make Payment
                                                                                                    </a>
																									<br>
																									<br>
                <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Thank you for choosing Company Name. We look forward to hearing from you!</p>
			</div>
			
			<div style="width: 600px; height:auto; padding: 20px 0px 0px 0px; margin-bottom: 30px; text-align: center;">
				<div style="width: 100%; text-align: center;"><img src="<?php echo base_url(); ?>images/dhk-email-logo.png?v=1" width="300" height="33" alt="" style="margin:0 auto;" /></div>
				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 5px 0px 15px 0px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Grace Quest Cleaning Services<br />
					Office 4201B, ASPiN Commercial Tower,<br />
					Sheikh Zayed Road, Dubai, U.A.E<br />
					Phone : +971 4 263 1976<br />
					Email : office@dubaihousekeeping.com</p>
			</div>
			
		</div>
		
		<div style="width: 740px; height:auto; padding: 0px 30px; background: #1cac51; text-align: center;">
			<p style="font-family: Tahoma, Geneva, sans-serif; font-size:12px; color: #FFF; line-height:20px; padding: 12px 0px 10px 0px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">© <?php echo date('Y'); ?> Company Name All Rights Reserved.</p>
		</div>
	</div>
</div>
</body>
</html>
