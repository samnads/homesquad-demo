<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
	.container.booking-fl-box{width: 100% !important;}
</style>
<div class="row">   
    <div class="span12" style="width: 97% !important;">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form id="edit-profile" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <i class="icon-th-list"></i>
                    <h3>Online Payments </h3>                   
                    <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="vehicle_date" name="vehicle_date" placeholder="From Date" value="<?php if($search_date_from != NULL){ echo $search_date_from; } ?>">
					<input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="vehicle_date_to" name="vehicle_date_to" placeholder="To Date" value="<?php if($search_date_to != NULL){ echo $search_date_to; } ?>">
					
                    <!--<select style="margin-top: 5px; width:160px; margin-bottom: 9px;" id="customers_vh_rep_new" name="customers_vh_rep">
						<option value="0">-- Select Customer --</option>
						<?php
						// foreach($customerlist as $c_val)
						// {
							// if($c_val->customer_id == $customer_id)
							// {
								// $selected = 'selected="selected"';
							// } else {
								// $selected = '';
							// }
						?>
						<option value="<?php// echo $c_val->customer_id; ?>" <?php// echo $selected; ?>><?php// echo $c_val->customer_name; ?></option>
						<?php
						//}
						?>  
                    </select>-->
                    <input type="submit" class="btn" value="Go" name="vehicle_report" style="margin-bottom: 4px;">
					<!--<a style="float:right ; margin-right:15px; cursor:pointer;" href="<?php// echo base_url(); ?>customer/add_payment">
						<img src="<?php// echo base_url(); ?>img/add.png" title="Add Payment">
					</a>-->
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Sl.No</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Customer</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Transaction Id</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Reference No</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Amount</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Received Amount</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Convenience Fee</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Balance Amount</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Description</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Status</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Paid Date Time</th>
                        </tr>
                    </thead>
                    <tbody>
					<?php
					$rec_amt = 0;
					$ser_amt = 0;
					$bal_amt = 0;
					$i = 1;
                    foreach ($payment_report as $pay){
						$new_received_amt = ($pay->amount + $pay->transaction_charge);
						$new_tot_received_amt += ($pay->amount + $pay->transaction_charge);
						$rec_amt += round($pay->amount,2);
						$amount = round($pay->amount,2);
						// $service_charge = ((($amount * 0.03) + 1) * 1.05);
						$service_charge = $pay->transaction_charge;
						$ser_amt += round($service_charge,2);
						$balance_amount = ($new_received_amt - $service_charge);
						$bal_amt += round($balance_amount,2);
					?>
						<tr>
							<td style="line-height: 18px;"><?php echo $i; ?> </td>
							<td style="line-height: 18px;"><?php echo $pay->customer_name; ?></td>
							<td style="line-height: 18px;"><?php echo $pay->transaction_id; ?></td>
							<td style="line-height: 18px;"><?php echo $pay->reference_id; ?></td>
							<td style="line-height: 18px;"><?php echo $amount; ?></td>
							<td style="line-height: 18px;"><?php echo $new_received_amt; ?></td>
							<td style="line-height: 18px;"><?php echo round($service_charge,2); ?></td>
							<td style="line-height: 18px;"><?php echo round($balance_amount,2); ?></td>
							<td style="line-height: 18px;"><?php echo $pay->description; ?></td>
							<td style="line-height: 18px;"><?php echo $pay->payment_status; ?></td>
							<td style="line-height: 18px;"><?php echo date('d/m/Y h:i A',strtotime($pay->payment_datetime)); ?> </td>
						</tr>
					<?php
					$i++;	
					}
					?>
                    </tbody>
					<tfoot>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td>Total</td>
							<td><?php echo $rec_amt; ?></td>
							<td><?php echo $new_tot_received_amt; ?></td>
							<td><?php echo $ser_amt; ?></td>
							<td><?php echo $bal_amt; ?></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</tfoot>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>