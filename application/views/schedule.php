<script>
  var service_vat_percentage = <?= $settings->service_vat_percentage; ?>;
</script>
<style type="text/css">
  .bg-od,
  .schedule_bubble.od {
    background-color: <?= $settings->color_bg_booking_od; ?> !important;
    color: <?= $settings->color_bg_text_booking_od; ?> !important;
  }

  .bg-we,
  .schedule_bubble.we {
    background-color: <?= $settings->color_bg_booking_we; ?> !important;
    color: <?= $settings->color_bg_text_booking_we; ?> !important;
  }

  .bg-bw,
  .schedule_bubble.bw {
    background-color: <?= $settings->color_bg_booking_bw; ?> !important;
    color: <?= $settings->color_bg_text_booking_bw; ?> !important;
  }

  .schedule_bubble.od:after {
    border-color: transparent <?= $settings->color_bg_booking_od; ?>;
  }

  .schedule_bubble.we:after {
    border-color: transparent <?= $settings->color_bg_booking_we; ?>;
  }

  .schedule_bubble.bw:after {
    border-color: transparent <?= $settings->color_bg_booking_bw; ?>;
  }
</style>
<style type="text/css">
  .field-error.text-danger {
    color: #f00;
  }

  .pac-container {
    z-index: 9999999999 !important;
  }

  .select2-arrow {
    visibility: hidden;
  }

  .select2-container .select2-choice {
    -moz-appearance: none;
    background: #fff url("../images/ash-arrow.png") no-repeat scroll right 7px top 11px;
    border: 1px solid #ccc;
    border-radius: 3px;
    cursor: pointer;
    font-size: 12px;
    height: 30px;
    line-height: 24px;
    padding: 3px 0 3px 10px;
    text-indent: 0.01px;
  }

  .popup-main-box .booking_form .row .cell1 {
    width: 160px !important;
  }

  .container.booking-fl-box {
    width: 100% !important;
  }

  #maids-panel {
    z-index: 99999999 !important;
  }

  #service-panel {
    z-index: 99999999 !important;
  }

  .select2-dropdown {
    z-index: 99999999 !important;
  }

  #customer-address-panel {
    z-index: 99999999 !important;
  }

  .fancybox-inner {
    height: auto !important;
    width: 100%;
    margin: 0px auto;
  }

  .n-field-box input {
    width: 100% !important;
    height: 32px;
  }

  .n-booking-popup-wrapper {}

  #customer-info {
    width: 100%;
  }

  #customer-info .select2 {
    width: 100% !important;
  }

  .popup-note-fld,
  .pop-main-button {
    width: 100% !important;
  }

  .n-form-set-main {
    padding-top: 20px;
  }

  .n-field-main {
    /*background: #F00;*/
    padding-bottom: 15px;
  }

  .n-field-box {
    /*background: #0CF;*/
  }

  .n-field-main p {
    font-size: 12px;
    color: #333;
    line-height: 18px;
    padding-bottom: 3px;
  }

  .n-btn {
    margin-bottom: 20px;
    margin-top: 0px;
  }

  .n-field-box label {
    margin-bottom: 0px;
  }

  .popup-main-box .booking_form .row {
    display: block;
  }

  label input,
  label select {
    width: 100%;
    height: 32px;
  }

  .switch-main {
    user-select: none;
  }

  .switch {
    position: relative;
    display: inline-block;
    width: 34px;
    height: 19px;
  }

  .switch input {
    opacity: 0;
    width: 0;
    height: 0;
    box-shadow: none !important;
  }

  .switch input:focus {
    box-shadow: none !important;
  }

  .slider {
    position: absolute;
    cursor: pointer;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #dedede;
    -webkit-transition: .4s;
    transition: .4s;
  }

  .slider:before {
    position: absolute;
    content: "";
    height: 14px;
    width: 14px;
    left: 3px;
    bottom: 3px;
    background-color: white;
    -webkit-transition: .1s;
    transition: .1s;
  }

  input:checked+.slider {
    background-color: #6cd038;
  }

  /*input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}*/
  input:checked+.slider:before {
    -webkit-transform: translateX(14px);
    -ms-transform: translateX(14px);
    transform: translateX(14px);
  }

  /* Rounded sliders */
  .slider.round {
    border-radius: 34px;
  }

  .slider.round:before {
    border-radius: 50%;
  }

  .n-left-position {
    height: 100%;
    font-size: 11px;
    position: absolute;
    left: 0px;
    top: 0px;
    z-index: 9;
    background: #BFEBFF;
    flex-direction: column;
    justify-content: center !important;
    display: flex !important;
    padding: 0px 5px;
    -webkit-border-top-left-radius: 3px;
    -webkit-border-bottom-left-radius: 3px;
    -moz-border-radius-topleft: 3px;
    -moz-border-radius-bottomleft: 3px;
    border-top-left-radius: 3px;
    border-bottom-left-radius: 3px;
    border: 1px solid #3CC;
  }

  .n-right-position {
    height: 100%;
    font-size: 11px;
    position: absolute;
    right: 0px;
    top: 0px;
    z-index: 9;
    background: #BFEBFF;
    flex-direction: column;
    justify-content: center !important;
    display: flex !important;
    padding: 0px 5px;
    -webkit-border-top-right-radius: 3px;
    -webkit-border-bottom-right-radius: 3px;
    -moz-border-radius-topright: 3px;
    -moz-border-radius-bottomright: 3px;
    border-top-right-radius: 3px;
    border-bottom-right-radius: 3px;
    border: 1px solid #3CC;
  }

  .position-relative {
    position: relative;
  }

  .n-popup-btn-main input {
    width: auto !important;
  }

  .n-details-box-section p {
    color: #888;
    padding-bottom: 2px;
  }

  .n-details-box-section .n-field-box {
    font-weight: 600;
    font-family: Arial, Helvetica, sans-serif;
  }

  .driver-note textarea {
    font-size: 15px;
    line-height: 20px;
    color: #333;
  }

  .n-pick-maids-set label {
    display: inline-block;
    width: 200px;
  }

  .n-pick-maids-set span {
    display: inline-block !important;
  }

  .n-pick-maids-set p {
    display: inline-block !important;
    bottom: -4px;
    position: relative;
    padding-left: 5px;
  }

  body {
    over-flow-x: hidden;
  }

  #schedule {
    margin-top: 0px;
  }
</style>
<!--<script src="http://maps.googleapis.com/maps/api/js?key=<?= $settings->google_map_api_key; ?>&amp;libraries=places"></script>-->
<!-- this is replaced from PM -->
<script type="text/javascript">
  $(document).ready(function() {
    $(".n-delete-set-right").click(function() {
      $('.n-delete-set-right').removeClass('de-select');
      $('.n-delete-set-left').addClass('de-select');
    });
    $(".n-delete-set-left").click(function() {
      $('.n-delete-set-left').removeClass('de-select');
      $('.n-delete-set-right').addClass('de-select');
    });
  });
</script>
<div id="move-booking-modal" style="background:none;display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span class="title">Move Booking</span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <div class="controls">
        <div id="maids-move-panel">
          <div class="inner p-2" style="height: 260px;overflow-x:hidden;overflow-y:scroll;">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="alert-popup" style="background:none;display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="alert-title"></span>
    </div>
    <div class="modal-body">
      <h3 id="alert-message"></h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0 pop_close">OK</button>
    </div>
  </div>
</div>
<div class="content-wrapper" style="width: 100%; margin-left: 0px;">
  <div id="schedule-wrapper">
    <div id="schedule" class="mt-0">
      <div class="scroll-top-fix" style="position: relative;">
        <div id="schedule-top" class="pt-0">
          <div class="row">
            <div class="container" style="width:100%;">
              <div class="book-nav-top">
                <ul>
                  <li>
                    <div class="mm-color-box bg-od"><?= $settings->od_booking_text; ?></div>
                    <div class="mm-color-box bg-we"><?= $settings->we_booking_text; ?></div>
                    <div class="mm-color-box bg-bw"><?= $settings->bw_booking_text; ?></div>
                  </li>
                  <li>
                    <div class="hed-main-date">
                      <div class="row">
                        <div class="prev_day"><a class="hed-date-left-arrow" href="<?php echo base_url() . 'booking/' . $prev_day; ?>"></a></div>
                        <div class="hed-date-main-box"><?php echo html_escape($schedule_day); ?></div>
                        <div class="next_day"><a class="hed-date-right-arrow" href="<?php echo base_url() . 'booking/' . $next_day; ?>"></a></div>
                        <div class="hed-date-calender-icon"><span class="datepicker" data-date="<?php echo html_escape($schedule_day_c); ?>" data-date-format="dd-mm-yyyy"></span></div>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="mm-drop">
                      <input type="hidden" value="<?php echo $services_date; ?>" id="servicesdate" />
                      <select id="freemaids" name="freemaids" class="" style="">
                        <option value="0" <?php if ($freemaid_ids == '0') {
                                            echo ' selected="selected"';
                                          } ?>>Select All</option>
                        <option value="1" <?php if ($freemaid_ids == '1') {
                                            echo ' selected="selected"';
                                          } ?>>Free Maids</option>
                        <option value="2" <?php if ($morning_avail == '2') {
                                            echo ' selected="selected"';
                                          } ?>>Morning Availability</option>
                        <option value="3" <?php if ($eve_avail == '3') {
                                            echo ' selected="selected"';
                                          } ?>>Evening Availability</option>
                        <option value="4" <?php if ($tot_avail == '4') {
                                            echo ' selected="selected"';
                                          } ?>>On Duty Maids</option>
                      </select>
                    </div>
                  </li>
                  <li class="no-right-margin" style="float: right;">
                    <div class="n-btn" style="cursor: pointer; margin-top: -3px; margin-bottom: 6px !important;">
                      <span id="print-schedule-report"><i class="fa fa-print"></i> &nbsp; Print Schedule</span>
                    </div>
                  </li>
                  <div class="clear"></div>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="booking-position"></div>
        <div class="booking-calender-section">
          <div class="head">Staff Name</div>
          <div id="tb-slide-left" class="time-nav previous-time" title="Previous Time">&nbsp;</div>
          <div class="time_line">
            <div class="time_slider">
              <div style="width: 4820px;">
                <?php
                foreach ($times as $time_index => $time) {
                  echo '<div><span>' . $time->display . '</span></div>';
                }
                ?>
              </div>
            </div>
          </div>
          <div id="tb-slide-right" class="time-nav next-time" title="Next Time">&nbsp;</div>
          <div class="next-time-bg">&nbsp;</div>
          <div class="clear"></div>
        </div>
      </div>
      <div class="book-mid-det-lt-box">
        <div class="maids">
          <?php
          $i = 1;
          foreach ($maids as $maid) {
          ?>
            <div class="maid" data-id="<?= $maid->maid_id; ?>"><?php echo $i; ?>. <?php echo html_escape($maid->maid_name); ?></div>
          <?php
            $i++;
          }
          ?>
        </div>
        <div class="time_grid" style="background: #f9f6f1; height: <?php echo (count($maids) * 71);  ?>px;">
          <div class="grids">
            <div id="schedule-grid-rows"><?php echo $schedule_grid; ?></div>
          </div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
  </div>
</div>
<input type="hidden" id="all-maids" value='<?php echo $all_maids; ?>' />
<input type="hidden" id="time-slots" value='<?php echo json_encode($times); ?>' />
<input type="hidden" id="repeate-end-start" value="<?php echo html_escape($repeate_end_start_c); ?>" />
<div class="n-booking-popup-wrapper" id="booking-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head"><span id="b-maid-name"></span> <span id="b-time-slot"></span><span id="b-booking-id"></span> <span class="pop_close n-close-btn">&nbsp;</span> </div>
    <form id="booking-form">
    <div id="popup-booking" class="col-md-12 col-sm-12">
      <div class="booking_form" id="customer-info">
        <div class="row n-form-set-main m-0">
          <div class="col-sm-6 n-form-set-left">
            <div class="row m-0 n-field-main">
              <p>Customer</p>
              <div class="n-field-box" id="b-customer-id-cell">
                <select name="customer_id" id="b-customer-id" data-placeholder="Select customer" class="">
                  <option></option>
                  <?php
                  ?>
                </select>
                <div id="customer-picked-address"></div>
              </div>
            </div>
            <div id="customer-address-panel">
              <div class="head">Pick One Address <span class="close">
                  <div class="n-close-btn">&nbsp;</div>
                </span></div>
              <div class="inner"> Loading<span class="dots_loader"></span> </div>
            </div>
            <div id="maids-panel">
              <div class="head">Pick Maids <span class="close">
                  <div class="n-close-btn">&nbsp;</div>
                </span></div>
              <div class="controls">
                <div class="row m-0 n-field-main pt-2" style="border-bottom: 1px solid #ddd;">
                  <div class="col-sm-3 n-field-box">
                    <div class="n-end n-pick-maids-set">
                      <input id="pick-maids-all" type="radio" value="0" name="same_zone" class="" checked="">
                      <label for="pick-maids-all"> <span class="border-radius-3"></span>
                        <p>All</p>
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-3 n-field-box">
                    <div class="n-end n-pick-maids-set">
                      <input id="pick-maids-zone" type="radio" value="1" name="same_zone" class="">
                      <label for="pick-maids-zone"> <span class="border-radius-3"></span>
                        <p>Same Zone</p>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="inner">
                Loading<span class="dots_loader"></span> </div>
            </div>
            <div id="drivers-panel">
              <div class="head">Select Driver <span class="close">
                  <div class="n-close-btn">&nbsp;</div>
                </span></div>
              <div class="controls"> </div>
              <div class="inner"> Loading<span class="dots_loader"></span> </div>
            </div>
            <div class="row m-0 n-field-main">
              <p>Service Type</p>
              <div class="n-field-box sertype_sectn">
                <select name="service_type_id" id="b-service-type-id" data-placeholder="Select service type" class="sel2">
                  <option></option>
                  <?php
                  foreach ($service_types as $service_type) {
                    $selected = $service_type->service_type_id == 1 ? 'selected="selected"' : '';
                    echo '<option data-cost="' . $service_type->service_rate . '" data-pricetype="' . $service_type->price_condition . '" data-type="' . $service_type->service_type . '" value="' . $service_type->service_type_id . '" ' . $selected . '>' . html_escape($service_type->service_type_name) . '</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
            <div id="service-panel">
              <div class="head">Pick Service Type <span class="close">Close</span></div>
              <div class="inner">
                Loading<span class="dots_loader"></span> </div>
            </div>
            <div class="row m-0 n-field-main" id="sqrbtnshow" style="display: none;">
              <p>Enter Square ft</p>
              <div class="n-field-box">
                <input type="text" name="squarefeetval" id="squarefeetval" value="">
              </div>
            </div>
            <div class="row m-0 n-field-main">
              <div class="col-sm-6 pl-0 n-field-box">
                <p>From Time</p>
                <select name="time_from" id="b-from-time" data-placeholder="Select" class="sel2 small mr15">
                  <option></option>
                  <?php
                  foreach ($times as $time_index => $time) {
                    echo '<option value="' . $time->stamp . '">' . $time->display . '</option>';
                  }
                  ?>
                </select>
              </div>
              <div class="col-sm-6 pr-0 n-field-box">
                <p>To Time</p>
                <select name="time_to" id="b-to-time" data-placeholder="Select" class="sel2 small">
                  <option></option>
                  <?php
                  foreach ($times as $time_index => $time) {
                    if ($time_index == 't-0') {
                      continue;
                    }
                    echo '<option value="' . $time->stamp . '">' . $time->display . '</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="row m-0 n-field-main">
              <p>Repeats</p>
              <div class="n-field-box">
                <select name="booking_type" id="b-booking-type" data-placeholder="Select repeat type" class="sel2">
                  <option></option>
                  <option value="OD"><?= $settings->od_booking_text; ?></option>
                  <option value="WE"><?= $settings->we_booking_text; ?></option>
                  <option value="BW"><?= $settings->bw_booking_text; ?></option>
                </select>
              </div>
            </div>
            <div class="row m-0 n-field-main" id="repeat-days">
              <p>Repeat On</p>
              <div class="col-sm-12 pl-0 pr-0 n-field-box n-field-day">
                <div class="n-days">
                  <input id="repeat-on-0" type="checkbox" value="0" name="w_day[]" class="w_day">
                  <label for="repeat-on-0"> <span class="border-radius-3"></span>
                    <p>Sun</p>
                  </label>
                </div>
                <div class="n-days">
                  <input id="repeat-on-1" type="checkbox" value="1" name="w_day[]" class="w_day">
                  <label for="repeat-on-1"> <span class="border-radius-3"></span>
                    <p>Mon</p>
                  </label>
                </div>
                <div class="n-days">
                  <input id="repeat-on-2" type="checkbox" value="2" name="w_day[]" class="w_day">
                  <label for="repeat-on-2"> <span class="border-radius-3"></span>
                    <p>Tue</p>
                  </label>
                </div>
                <div class="n-days">
                  <input id="repeat-on-3" type="checkbox" value="3" name="w_day[]" class="w_day">
                  <label for="repeat-on-3"> <span class="border-radius-3"></span>
                    <p>Wed</p>
                  </label>
                </div>
                <div class="n-days">
                  <input id="repeat-on-4" type="checkbox" value="4" name="w_day[]" class="w_day">
                  <label for="repeat-on-4"> <span class="border-radius-3"></span>
                    <p>Thu</p>
                  </label>
                </div>
                <div class="n-days">
                  <input id="repeat-on-5" type="checkbox" value="5" name="w_day[]" class="w_day">
                  <label for="repeat-on-5"> <span class="border-radius-3"></span>
                    <p>Fri</p>
                  </label>
                </div>
                <div class="n-days pr-0">
                  <input id="repeat-on-6" type="checkbox" value="6" name="w_day[]" class="w_day">
                  <label for="repeat-on-6"> <span class="border-radius-3"></span>
                    <p>Sat</p>
                  </label>
                </div>
                <div class="clear"></div>
              </div>
            </div>
            <div class="row m-0 n-field-main" id="repeat-ends">
              <div class="col-sm-6 pl-0 n-field-box n-end-main">
                <p>Never Ends</p>
                <div class="n-end">
                  <input id="repeat-end-never" type="radio" value="never" name="repeat_end" class="" checked>
                  <label for="repeat-end-never"> <span class="border-radius-3"></span></label>
                </div>
              </div>
              <div class="col-sm-6 pr-0 n-field-box">
                <p>Ends</p>
                <div class="n-end">
                  <input id="repeat-end-ondate" type="radio" value="ondate" name="repeat_end" class="">
                  <label for="repeat-end-ondate"> <span class="border-radius-3"></span></label>
                </div>
                <div class="n-end-field">
                  <input type="text" class="end_datepicker" id="repeat-end-date" data-date-format="dd/mm/yyyy" readonly="readonly" disabled="disabled" />
                </div>
              </div>
            </div>
            <!------------- Cleaning materials Div --------------->
            <div class="row m-0 n-field-main">
              <div class="col-sm-6 n-field-box pl-0">
                <p>Cleaning Materials</p>
                <div class="switch-main">
                  <label class="switch">
                    <input type="checkbox" name="cleaning_materials" id="b-cleaning-materials" value="Y">
                    <span class="slider round"></span>
                  </label>
                </div>
              </div>
            </div>
            <div class="row m-0 n-field-main" id="tools-list">
              <p>Add tools?</p>
              <?php
                // Group the $cleaning_supplies array by 'type'
                $groupedSupplies = [];
                foreach ($cleaning_supplies as $supplies) {
                    $type = $supplies->type;
                    if (!isset($groupedSupplies[$type])) {
                        $groupedSupplies[$type] = [];
                    }
                    $groupedSupplies[$type][] = $supplies;
                }
              ?>
              <div class="col-sm-12 pl-0 pr-0 n-field-box n-field-day" id="cleanPlanType" 
              style="display:none;">
              <?php foreach ($groupedSupplies['PlanBased'] as $cleanType =>$groupedSupply) {
                      $checked = ($groupedSupply->id == 1) ? 'checked' : '';?>
                      <div class="n-days n-tools">
                          <input id="tools-<?php echo str_replace(' ', '-', strtolower($groupedSupply->name)); ?>" 
                          type="radio" value="<?php echo $groupedSupply->id; ?>" 
                          name="w_tooltype" class="w_tooltype" <?php echo $checked ?> 
                          data-amount ="<?php echo $groupedSupply->amount; ?>" 
                          data-type ="<?php echo $groupedSupply->type; ?>">
                          <label class="clearfix" for="tools-<?php echo str_replace(' ', '-', strtolower($groupedSupply->name)); ?>"> 
                            <span class="border-radius-3"></span>
                            <p><?php echo $groupedSupply->name; ?></p>
                          </label>
                      </div>
                  <?php } ?>
                </div>
                <div class="col-sm-12 pl-0 pr-0 n-field-box n-field-day" id="customtools">
                    <?php foreach ($groupedSupplies['Custom'] as $cleanType =>$groupedSupply) {?>
                            <div class="n-days n-tools">
                              <input id="tools-<?php echo str_replace(' ', '-', strtolower($groupedSupply->name)); ?>" 
                              type="checkbox" value="<?php echo $groupedSupply->id; ?>" 
                              name="w_tool[]" class="w_tool" data-amount ="<?php echo $groupedSupply->amount; ?>" 
                              data-type ="<?php echo $groupedSupply->type; ?>">
                              <label class="clearfix" for="tools-<?php echo str_replace(' ', '-', strtolower($groupedSupply->name)); ?>"> 
                              <span class="border-radius-3"></span>
                                <p><?php echo $groupedSupply->name; ?></p>
                              </label>
                            </div>                            
                    <?php }?>
              </div>
          </div>
              <!--<div class="col-sm-12 pl-0 pr-0 n-field-box n-field-day" id="cleanPlanType" style="display:none;">              
                  <div class="n-end n-pick-maids-set">
                      <input id="tools-standard" type="radio" value="_standardCleaningSuppliesCostPerHr" name="w_tooltype" class="w_tooltype" checked>
                      <label for="tools-standard"> <span class="border-radius-3"></span>
                        <p>Standard Cleaning Supplies</p>
                      </label>
                  </div>
                  <div class="n-end n-pick-maids-set">
                      <input id="tools-eco" type="radio" value="_EcologicalGreenCleanSuppliesCostPerHr" name="w_tooltype" class="w_tooltype">
                      <label for="tools-eco"> <span class="border-radius-3"></span>
                        <p>Ecological Green Clean Supplies</p>
                      </label>
                  </div>
              </div>
              <div class="col-sm-12 pl-0 pr-0 n-field-box n-field-day" id="customtools">
                <div class="n-days n-tools">
                  <input id="tools-vaccum" type="checkbox" value="3" name="w_tool[]" class="w_tool">
                  <label for="tools-vaccum"> <span class="border-radius-3"></span>
                    <p>Vacuum Cleaner</p>
                  </label>
                </div>
                <div class="n-days n-tools">
                  <input id="tools-ladder" type="checkbox" value="4" name="w_tool[]" class="w_tool">
                  <label for="tools-ladder"> <span class="border-radius-3"></span>
                    <p>Ladder</p>
                  </label>
                </div>
                <div class="n-days n-tools">
                  <input id="tools-mop" type="checkbox" value="5" name="w_tool[]" class="w_tool">
                  <label for="tools-mop"> <span class="border-radius-3"></span>
                    <p>Mop Bucket</p>
                  </label>
                </div>
                <div class="n-days n-tools">
                  <input id="tools-iron" type="checkbox" value="6" name="w_tool[]" class="w_tool">
                  <label for="tools-iron"> <span class="border-radius-3"></span>
                    <p>Electric Iron</p>
                  </label>
                </div>
              </div>
            </div>-->

            <!----------------------------------------------------->
            <div class="row m-0 n-field-main">
            <div class="col-sm-6 n-field-box pl-0">
                <p>Lock Booking</p>
                <div class="switch-main">
                  <label class="switch">
                    <input type="checkbox" name="lock_booking" id="lock-booking">
                    <span class="slider round"></span>
                  </label>
                </div>
              </div>
            </div>
            <!----------------------------------------------------->
            <div class="row m-0 n-field-main">
              <div class="col-sm-6 n-field-box pl-0">
                <p>Email Notification</p>
                <div class="switch-main">
                  <label class="switch">
                    <input type="checkbox" name="notification_email_booking" id="notification-email-booking">
                    <span class="slider round"></span>
                  </label>
                </div>
              </div>
              <div class="col-sm-6 n-field-box pr-0">
                <p>SMS Notification</p>
                <div class="switch-main">
                  <label class="switch">
                    <input type="checkbox" name="notification_sms_booking" id="notification-sms-booking">
                    <span class="slider round"></span>
                  </label>
                </div>
              </div>
            </div>
            <div class="row m-0 n-field-main">
              <div class="col-sm-6 n-field-box pl-0">
                <p>Free Service</p>
                <div class="switch-main">
                  <label class="switch">
                    <input type="checkbox" name="free_service" id="free-service">
                    <span class="slider round"></span>
                  </label>
                </div>
              </div>
              <div class="col-sm-6 n-field-box">
                <p>Supervisor</p>
                <div class="switch-main">
                  <label class="switch">
                    <input type="checkbox" name="supervisor_selected" id="supervisor_selected">
                    <span class="slider round"></span>
                  </label>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6 n-form-set-right">
            <div class="row m-0 n-field-main payment_mode_customer" style="display:none;">
              <p>Payment mode</p>
              <div class="pl-0 n-field-box">
                <input type="text" name="payment_mode" id="b-customer-paymode-cell">
              </div>
            </div>
            <div class="row pl-3 n-field-main">
              <div class="col-sm-6 n-field-box">
                <p>Rate</p>
                <label class=" position-relative">
                  <div class="n-left-position">AED</div>
                  <div class="n-right-position">/hr</div>
                  <input name="rate_per_hr" id="b-rate_per_hr" type="number" step="any" class="popup-disc-fld text-center no-arrows" autocomplete="off" />
                </label>
              </div>
              <div class="col-sm-6 n-field-box">
                <p>Discount Rate</p>
                <label class="position-relative">
                  <div class="n-left-position">AED</div>
                  <div class="n-right-position">/hr</div>
                  <input name="discount_rate_perhr" id="discount_rate_perhr" value="0" type="number" step="any" autocomplete="off" class="popup-disc-fld text-center no-arrows" />
                </label>
              </div>
            </div>
            <div class="row pl-3 n-field-main">
              <div class="col-sm-6 n-field-box">
                <p>Amount</p>
                <label>
                  <input name="serviceamountcost" id="serviceamountcost" readonly="" value="" type="number" class="popup-disc-fld" />
                </label>
              </div>
              <div class="col-sm-6  n-field-box">
                <p>Service Discount</p>
                <label>
                  <input name="discount" id="b-discount" type="number" step="any" class="popup-disc-fld no-arrows" autocomplete="off" />
                </label>
              </div>
            </div>
            <div class="row m-0 n-field-main">
              <div class="col-sm-6 pl-0 n-field-box">
                <p>Material Cost</p>
                <label>
                  <input name="cleaning_materials_charge" id="cleaning-materials-charge" readonly type="number" step="any" class="popup-disc-fld" />
                </label>
              </div>
			  <div class="col-sm-6 pr-0 n-field-box">
                <p>Supervisor Fee</p>
                <label>
                  <input name="supervisor-charge" id="supervisor-charge" readonly type="number" step="any" class="popup-disc-fld" />
                </label>
              </div>
            </div>
            <div class="row m-0 n-field-main">
			<div class="col-sm-4 pl-0 n-field-box">
                <p>Service Amount</p>
                <label>
                  <input type="hidden" id="total_prebook_hrs" value="0">
                  <input type="hidden" id="total_week_days" value="1">
                  <input type="hidden" id="flatrateval" value="">
                  <input type="hidden" id="categoryidval" value="">
                  <input type="hidden" id="subcategoryidval" value="">
                  <input type="hidden" id="furnishid" value="">
                  <input type="hidden" id="scrubpolishid" value="">
                  <input type="hidden" id="valname" value="">
                  <input type="hidden" id="servicecostval" value="35">
                  <input type="hidden" id="servicetypeval" value="H">
                  <input type="hidden" id="service_pricetype" value="CP">
                  <input type="hidden" id="sqftcount" value="">
                  <input type="hidden" id="b-rate_per_hr_cust" value="35">
                  <input type="hidden" id="customer_flag_val" value="">
                  <input type="hidden" id="customer_flag_reason" value="">
                  <input name="tot_amout" id="tot_amout" value="" readonly="" type="number" class="popup-disc-fld" />
                </label>
              </div>
              <div class="col-sm-3 pl-0 pr-0 n-field-box">
                <p>VAT <?= $settings->service_vat_percentage; ?>%</p>
                <label>
                  <input name="service_vat_amount" id="service_vat_amount" readonly="" value="" type="number" class="popup-disc-fld" />
                </label>
              </div>
              <div class="col-sm-5 pr-0 n-field-box">
                <p>Total Amount</p>
                <label class="position-relative">
                  <div class="n-left-position">AED</div>
                  <input name="service_taxed_total" id="service_taxed_total" type="number" class="popup-disc-fld text-center no-arrows" />
                </label>
              </div>
            </div>
            <div class="row m-0 n-field-main normaldriversec">
              <p>Select Driver</p>
              <div class="n-field-box" id="driver">
                <div class="inner" style="width:100%">
                  <select name="driver_select_id" id="driver_select_id" data-placeholder="Select Driver" class="sel2" style="width:100%">
                    <option></option>
                    <?php
                    foreach ($tablets as $tabletval) {
                      echo '<option value="' . $tabletval->tablet_id . '">' . $tabletval->tablet_driver_name . '</option>';
                    }
                    ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="row m-0 n-field-main" id="updatedriversec" style="display: none;">
              <p>Select Driver</p>
              <div class="n-field-box">
                <select name="driver_select_id_update" id="driver_select_id_update" data-placeholder="Select Driver" class="sel2" style="width:100%">
                  <option></option>
                  <?php
                  foreach ($tablets as $tabletval) {
                    echo '<option value="' . $tabletval->tablet_id . '">' . $tabletval->tablet_driver_name . '</option>';
                  }
                  ?>
                </select>
                <input type="button" class="copy-but n-btn purple-btn mb-0 mt-2" id="updatedrivertobooking" value="Update Driver" style=" width: 135px !important; float: right; margin-right: 0px;" />
              </div>
            </div>
            <div class="row m-0 n-field-main" style="display: none;">
              <p>Transferred To</p>
              <div class="n-field-box" id="transfeered_to_show">
              </div>
            </div>
            <div class="row m-0 n-field-main" id="delete_replace_section_new" style="display: none;">
              <p>Delete Request</p>
              <div class="n-field-box" id="">
                <input type="text" readonly name="payment_mode" id="approvetextlabelDelete">
                <input type="button" class="red-btn" id="delete-cancelbtn" style="display:none;" value="Delete Request" />
              </div>
            </div>
            <div class="booking_form">
            </div>
            <div style="clear:both;"></div>
            <div class="row m-0 n-field-main driver-note">
              <p>Note to Driver</p>
              <div class="n-field-box pop-main-cont-fld-box">
                <textarea name="booking_note" id="booking-note" class="popup-note-fld"></textarea>
              </div>
            </div>
          </div>
          <div class="col-sm-12 pop-main-button">
            <div id="b-error"></div>
            <div class="col-sm-12 n-popup-btn-main">
              <input type="hidden" class="n-btn" id="booking-id" />
              <input type="hidden" class="n-btn" id="hiddentabletid" name="hiddentabletid" />
              <input type="hidden" class="n-btn" id="customer-address-id" />
              <input type="hidden" class="n-btn" id="maid-id" />
              <input type="button" class="n-btn" id="save-booking" value="Save" />
              <input type="button" class="n-btn" id="copy-booking" value="Copy" />
              <input type="button" class="n-btn ml-3 mr-3" id="transfer-driver" value="Transfer Driver" />
              <input type="button" class="n-btn red-btn" id="delete-booking" value="Delete" />
              <?php if ($_SERVER['HTTP_HOST'] == 'localhost' || stripos($_SERVER['REQUEST_URI'], 'elite-demo') !== false) : ?>
                <button type="button" class="n-btn ml-2" id="move-booking-btn"><i class="fa fa-arrows" aria-hidden="true"></i> Move</button>
              <?php endif; ?>
              <div class="clear"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </form>
    <div id="customer-add-popup" class="col-md-4 col-sm-5" style="display:none;">
      <div class="booking_form" style="display: block !important; padding-right: 15px;">
        <form name="customer-popup" id="customer-popup" method="POST">
          <div class="row m-0 n-field-main pt-4">
            <p>Customer</p>
            <div class="n-field-box">
              <input type="text" name="customer_name" id="customer_name">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Email</p>
            <div class="n-field-box">
              <input type="text" name="customer_email" id="customer_email">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Area</p>
            <div class="n-field-box">
              <select name="area" id="area" class="sel2" style="width: 100% !important;">
                <option value="" selected>Select Area</option>
                <?php
                if (count($areas) > 0) {
                  foreach ($areas as $areasVal) {
                ?>
                    <option value="<?php echo $areasVal['area_id'] ?>"><?php echo $areasVal['area_name'] ?></option>
                <?php
                  }
                }
                ?>
              </select>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Apartment No</p>
            <div class="n-field-box">
              <input type="text" name="apartmentnos" id="apartmentnos" />
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Address</p>
            <div class="n-field-box">
              <input id="address" type="text" name="address" onkeyup="loadLocationField('address');" />
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Mobile Number</p>
            <div class="n-field-box">
              <input autocomplete="off" type="number" maxlength="12" id="mobile_number1" name="mobile_number1" class="tel" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" />
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <div class="col-sm-6 pl-0 pr-0 n-field-box">
              <input type="button" id="popup-add-customer" value="Add" class="save-but">
            </div>
          </div>
        </form>
      </div>
    </div>
    <div id="customer-detail-popup" class="col-md-4 col-sm-5" style="float:right;display:none;">
      <div class="booking_form n-details-box-section">
        <div class="row m-0 n-field-main pt-4">
          <p>Name</p>
          <div class="n-field-box" id="b-customer-ids-cell">
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Mobile</p>
          <div class="n-field-box" id="b-customer-mobile-cell">
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Email</p>
          <div class="n-field-box" id="b-customer-email-cell">
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Area</p>
          <div class="n-field-box" id="b-customer-area-cell">
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Zone</p>
          <div class="n-field-box" id="b-customer-zone-cell">
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Address</p>
          <div class="n-field-box" id="b-customer-address-cell">
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Apartment No</p>
          <div class="n-field-box" id="b-customer-apartment-no">
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Customer Book Type</p>
          <div class="n-field-box" id="b-customer-booktype-cell">
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Customer Notes</p>
          <div class="n-field-box" id="b-customer-notes-cell">
          </div>
        </div>
        <div class="row m-0 n-field-main start_status position-relative" style="display:none;">
          <p>Status <i class="fa fa-gear fa-spin spin-icon" title="In progress"></i></p>
          <div class="n-field-box" id="progress_time"></div>
        </div>
        <div class="row m-0 n-field-main service_started_at" style="display:none;">
          <p>Started at</p>
          <div class="n-field-box" id="started_time"> </div>
        </div>
        <div class="row m-0 n-field-main finish_status" style="display:none;">
          <p>Status</p>
          <div class="n-field-box"><b>Finished</b>&nbsp;<i class="fa fa-check" aria-hidden="true"></i></div>
        </div>
        <div class="row m-0 n-field-main  finish_status" style="display:none;">
          <div class="col-sm-6 pl-0">
            <p>Started at</p>
            <div class="n-field-box" id="time_started"> </div>
          </div>
          <div class="col-sm-6 pr-0">
            <p>Finished at</p>
            <div class="n-field-box" id="time_finished"></div>
          </div>
        </div>
        <div class="row m-0 n-field-main finish_status" style="display:none;">
          <p></p>
          <div class="n-field-box" id="worked_hrs"> </div>
        </div>
		<div class="row m-0 n-field-main" id="extra_services_show" style="display:none;">
          <div class="col-sm-12 pl-0">
            <p>Extra Services</p>
            <div class="n-field-box" id="extra-services"></div>
          </div>
        </div>
      </div>
    </div>
    <style>
      .n-popup-alert-main {
        position: absolute;
        left: 0px;
        top: 100px;
        z-index: 999;
        background: #0cf;
        padding: 20px;
        width: 100%;
        -webkit-border-bottom-right-radius: 10px;
        -webkit-border-bottom-left-radius: 10px;
        -moz-border-radius-bottomright: 10px;
        -moz-border-radius-bottomleft: 10px;
        border-bottom-right-radius: 10px;
        border-bottom-left-radius: 10px;
        box-shadow: 0 -2px 5px 0 rgba(0, 0, 0, 0.5);
      }

      .n-popup-alert-main .n-close-btn {
        position: absolute;
        right: 10px;
        top: 10px;
        cursor: pointer;
        user-select: none;
      }

      #booking-action-confirm-panel {
        border: 1px solid #CCC;
        width: 96%;
        padding: 15px;
        background: #fafafa;
        position: absolute;
        z-index: 100;
        border-radius: 5px;
        margin: -10px 0px 0px 0px;
        text-align: left;
        box-shadow: 0px 0px 0px #CCC;
        bottom: 15px;
        left: 15px;
      }
    </style>
  </div><!--popup-main-box end-->
</div>
<div id="schedule-report" style="display: none !important;"></div>
