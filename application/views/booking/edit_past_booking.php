<div class="row">
    <div class="span12" style="width: 97% !important;">      		
        <div class="widget">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Edit Booking</h3>
            </div> <!-- /widget-header -->
            <div class="widget-content">
                <div class="col-md-12 col-sm-12 confirm-det-cont-box  borderbox pl-3 pb-3 pr-3">
					<form class="form-horizontal" id="invoice-ma-form" method="post" enctype="multipart/form-data"> 
						<div class="col-sm-3">
							<div class="row m-0 n-field-main">
								<div class="col-sm-12 p-0 n-field-box">                                         
									<label class="control-label" for="booking_date">Service Date &nbsp;<font style="color: #C00">*</font></label>
									<div class="controls">
										<input type="text" class="span7" id="booking_date" name="booking_date" value="<?php echo date('Y-m-d'); ?>" required>
									</div> <!-- /controls --> 
								</div>									
							</div> <!-- /control-group -->
						</div>
						<div class="col-sm-3">
							<div class="row m-0 n-field-main">
								<div class="col-sm-12 p-0 n-field-box">
									<label class="control-label" for="basicinput">Customer&nbsp;<font style="color: #C00">*</font></label>
									<div class="controls">
										<select name="customer" id="customers_vh_rep_new" class="sel2 span7" required>
											<option value="">Select customer</option>
											<?php
											foreach($customerlist as $c_val)
											{
												if($c_val->customer_id == $customerId)
												{
													$selected = 'selected="selected"';
												} else {
													$selected = '';
												}
											?>
											<option value="<?php echo $c_val->customer_id; ?>" <?php echo $selected; ?>><?php echo $c_val->customer_name; ?></option>
											<?php
											}
											?>                                                                  
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="row m-0 n-field-main">
								<div class="col-sm-12 p-0 n-field-box">
									<label class="control-label" for="basicinput">Staff&nbsp;</label>
									<div class="controls">
										<select name="staff" id="edit_booking_staff" class="sel2 span7" required>
											<option value="">Select Staff</option>
											<?php
											foreach($stafflist as $m_val)
											{
											?>
											<option value="<?php echo $m_val->maid_id; ?>"><?php echo $m_val->maid_name; ?></option>
											<?php
											}
											?>                                                                  
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="row m-0 n-field-main">
								<div class="col-sm-6 p-0 n-field-box">
								</div>
								<div class="col-sm-6 p-0 n-field-box">
									<input type="button" class="btn mm-btn" value="Search" id="editbookingdetail">
									<span style="color: #C00" id="editerror"></span>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="col-md-12 col-sm-12 no-left-right-padding" id="activebookingssection" style="display: none;">
					<div class="widget-header" style="margin-bottom: 0; margin-top: 10px;">
						<i class="icon-th-list"></i>
						<h3>Active Bookings</h3>
					</div>
					<div class="widget-content borderbox" style="margin-bottom:30px">
						<table id="bookingtable" class="table da-table" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Sl.No</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Booking Id</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Service date</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Customer</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Maid</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Time</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Amount</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Vat</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Net Amount</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;">Action</th>
								</tr>
							</thead>
							<tbody>
							
							</tbody>
						</table>
						
						
					</div><!-- /widget-content --> 
				</div>
				
				<div class="col-md-12 col-sm-12 no-left-right-padding" id="activeservicessection" style="display: none;">
					<div class="widget-header" style="margin-bottom: 0; margin-top: 10px;">
						<i class="icon-th-list"></i>
						<h3>Active Services</h3>
					</div>
					<div class="widget-content borderbox" style="margin-bottom:30px">
						<table id="servicestable" class="table da-table" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Sl.No</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Booking Id</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Service Id</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Service date</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Customer</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Maid</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Time</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Amount</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Status</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;">Action</th>
								</tr>
							</thead>
							<tbody>
							
							</tbody>
						</table>
						
						
					</div><!-- /widget-content --> 
				</div>
				
				<div class="col-md-12 col-sm-12 no-left-right-padding" id="activeinvoicessection" style="display: none;">
					<div class="widget-header" style="margin-bottom: 0; margin-top: 10px;">
						<i class="icon-th-list"></i>
						<h3>Active Invoices</h3>
					</div>
					<div class="widget-content borderbox" style="margin-bottom:30px">
						<table id="invoicestable" class="table da-table" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Sl.No</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Invoice No:</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Customer</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Invoice date</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Invoice due date</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Amount</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Vat</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Total Amount</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Paid Amount</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Balance Amount</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Status</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Action</th>
								</tr>
							</thead>
							<tbody>
							
							</tbody>
						</table>
						
						
					</div><!-- /widget-content --> 
				</div>
				
				<div class="col-md-12 col-sm-12 no-left-right-padding" style="display: none;" id="invoicesection">
					<div class="widget-header" style="margin-bottom: 0; margin-top: 10px;">
						<i class="icon-th-list"></i>
						<h3>Invoice Detail</h3>
					</div>
					<div class="widget-content borderbox" style="margin-bottom:30px" id="invoicedetailsection">
						
					</div><!-- /widget-content --> 
				</div>
				
				<div class="col-md-12 col-sm-12 no-left-right-padding" id="allocatedpaymentssection" style="display: none;">
					<div class="widget-header" style="margin-bottom: 0; margin-top: 10px;">
						<i class="icon-th-list"></i>
						<h3>Allocated Payments</h3>
					</div>
					<div class="widget-content borderbox" style="margin-bottom:30px">
						<table id="paymentstable" class="table da-table" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Sl.No</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Invoice No:</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Customer</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Payment date</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Allocated Amount</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Action</th>
								</tr>
							</thead>
							<tbody>
							
							</tbody>
						</table>
						
						
					</div><!-- /widget-content --> 
				</div>
            </div> <!-- /widget-content -->					
        </div> <!-- /widget -->	      		
    </div> <!-- /span8 -->	      		      		      		      	
</div> <!-- /row -->

<div id="new-popup" style="display:none;">
	<div class="popup-main-box">
		<div class="col-md-12 col-sm-12 green-popup-head">
			<span id="b-maid-name">New Area</span>
			<span id="b-time-slot"></span>
			<span class="pop_close n-close-btn">&nbsp;</span>
		</div>
		<form class="form-horizontal" method="post" id="edit-booking-form">
			<div id="popup-booking" class="col-12 p-0">
				<div class="modal-body">
					<div class="row m-0 n-field-main">
						<p>Customer</p>
						<div class="col-sm-12 p-0 n-field-box" id="customersec">
							
						</div>
					</div>
					<div class="row m-0 n-field-main">
						<div class="col-sm-6 pl-0 n-field-box">
							<p>From Time</p>
							<select name="time_from" id="fromtimesec" data-placeholder="Select" class="sel2 small mr15" style="width: 150px !important;">
								<option></option>
								<?php
								foreach ($times as $time_index => $time) {
									echo '<option value="' . $time->stamp . '">' . $time->display . '</option>';
								}
								?>
							</select>
						</div>
						<div class="col-sm-6 pr-0 n-field-box">
							<p>To Time</p>
							<select name="time_to" id="totimesec" data-placeholder="Select" class="sel2 small" style="width: 150px !important;">
								<option></option>
								<?php
								foreach ($times as $time_index => $time) {
									if ($time_index == 't-0') 
									{
										continue;
									}
									echo '<option value="' . $time->stamp . '">' . $time->display . '</option>';
								}
								?>
							</select>
						</div>
					</div>
					<div class="row m-0 n-field-main">
						<div class="col-sm-6 n-field-box pl-0">
							<p>Cleaning Materials</p>
							<div class="switch-main">
								<label class="switch">
									<input type="checkbox" name="cleaning_materials" id="b-cleaning-materials" value="Y">
									<span class="slider round"></span>
								</label>
							</div>
						</div>
						<div class="col-sm-6 n-field-box pr-0">
							<p>Supervisor</p>
							<div class="switch-main">
								<label class="switch">
									<input type="checkbox" name="supervisor_selected" id="supervisor_selected" value="Y">
									<span class="slider round"></span>
								</label>
							</div>
						</div>
					</div>
					<div class="row m-0 n-field-main" id="tools-list">
						<div class="col-sm-6 pl-0 n-field-box">
							<p>Add tools?</p>
							<?php
							// Group the $cleaning_supplies array by 'type'
							$groupedSupplies = [];
							foreach ($cleaning_supplies as $supplies) {
								$type = $supplies->type;
								if (!isset($groupedSupplies[$type])) {
									$groupedSupplies[$type] = [];
								}
								$groupedSupplies[$type][] = $supplies;
							}
							?>
							<div class="col-sm-12 pl-0 pr-0 n-field-box n-field-day" id="cleanPlanType" style="display:none;">
								<?php 
								foreach ($groupedSupplies['PlanBased'] as $cleanType =>$groupedSupply)
								{
									$checked = ($groupedSupply->id == 1) ? 'checked' : '';
								?>
									<div class="n-days n-tools">
										<input id="tools-<?php echo str_replace(' ', '-', strtolower($groupedSupply->name)); ?>" type="radio" value="<?php echo $groupedSupply->id; ?>" 
									  name="w_tooltype" class="w_tooltype" <?php echo $checked ?> 
									  data-amount ="<?php echo $groupedSupply->amount; ?>" 
									  data-type ="<?php echo $groupedSupply->type; ?>">
									  <label class="clearfix" for="tools-<?php echo str_replace(' ', '-', strtolower($groupedSupply->name)); ?>"> 
										<span class="border-radius-3"></span>
										<p><?php echo $groupedSupply->name; ?></p>
									  </label>
									</div>
								<?php } ?>
							</div>
							<div class="col-sm-12 pl-0 pr-0 n-field-box n-field-day" id="customtools">
								<?php foreach ($groupedSupplies['Custom'] as $cleanType =>$groupedSupply) {?>
										<div class="n-days n-tools">
										  <input id="tools-<?php echo str_replace(' ', '-', strtolower($groupedSupply->name)); ?>" 
										  type="checkbox" value="<?php echo $groupedSupply->id; ?>" 
										  name="w_tool[]" class="w_tool" data-amount ="<?php echo $groupedSupply->amount; ?>" 
										  data-type ="<?php echo $groupedSupply->type; ?>">
										  <label class="clearfix" for="tools-<?php echo str_replace(' ', '-', strtolower($groupedSupply->name)); ?>"> 
										  <span class="border-radius-3"></span>
											<p><?php echo $groupedSupply->name; ?></p>
										  </label>
										</div>                            
								<?php }?>
							</div>
						</div>
						<div class="col-sm-6 n-field-box">
							<p>Free Service</p>
							<div class="switch-main">
								<label class="switch">
									<input type="checkbox" name="free_service" id="free-service" value="Y">
									<span class="slider round"></span>
								</label>
							</div>
						</div>
					</div>
					<div class="row pl-3 n-field-main">
						<div class="col-sm-4 n-field-box">
							<p>Rate</p>
							<label class=" position-relative">
								<div class="n-left-position">AED</div>
								<div class="n-right-position">/hr</div>
								<input name="rate_per_hr" id="rate_per_hr_sec" type="number" step="any" class="popup-disc-fld text-center no-arrows" autocomplete="off" />
							</label>
						</div>
						<div class="col-sm-4 n-field-box">
							<p>Discount Rate</p>
							<label class="position-relative">
								<div class="n-left-position">AED</div>
								<div class="n-right-position">/hr</div>
								<input name="discount_rate_perhr" id="discount_rate_perhrsec" value="0" type="number" step="any" autocomplete="off" class="popup-disc-fld text-center no-arrows" />
							</label>
						</div>
						<div class="col-sm-4 n-field-box">
							<p>Amount</p>
							<label>
								<input name="serviceamountcost" id="serviceamountcostsec" readonly="" value="" type="number" class="popup-disc-fld" />
							</label>
						</div>
					</div>
					<div class="row pl-3 n-field-main">
						<div class="col-sm-4  n-field-box">
							<p>Service Discount</p>
							<label>
								<input name="discount" id="b_discount_sec" type="number" step="any" class="popup-disc-fld no-arrows" autocomplete="off" />
							</label>
						</div>
						<div class="col-sm-4 n-field-box">
							<p>Material Cost</p>
							<label>
								<input name="cleaning_materials_charge" id="cleaning_materials_charge_sec" readonly type="number" step="any" class="popup-disc-fld" />
								<input name="cleaning_materials_section" id="cleaning_materials_section" readonly type="hidden" />
							</label>
						</div>
						<div class="col-sm-4 n-field-box">
							<p>Supervisor Fee</p>
							<label>
								<input name="supervisor-charge" id="supervisor_charge_sec" readonly type="number" step="any" class="popup-disc-fld" />
							</label>
						</div>
					</div>
					<div class="row m-0 n-field-main">
						<div class="col-sm-4 pl-0 n-field-box">
							<p>Service Amount</p>
							<label>
								<input name="tot_amout" id="tot_amout_sec" value="" readonly="" type="number" class="popup-disc-fld" />
							</label>
						</div>
						<div class="col-sm-4 n-field-box">
							<p>VAT 5%</p>
							<label>
								<input name="service_vat_amount" id="service_vat_amount_sec" readonly="" value="" type="number" class="popup-disc-fld" />
							</label>
						</div>
						<div class="col-sm-4 n-field-box">
							<p>Total Amount</p>
							<label class="position-relative">
								<div class="n-left-position">AED</div>
								<input name="service_taxed_total" id="service_taxed_total_sec" type="number" class="popup-disc-fld text-center no-arrows" />
							</label>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<input name="edit_booking_type" id="edit_booking_type" type="hidden" />
				<input name="edit_booking_id" id="edit_booking_id" type="hidden" />
				<input name="edit_booking_lock" id="edit_booking_lock" type="hidden" />
				<input name="edit_booked_by" id="edit_booked_by" type="hidden" />
				<input name="edit_maid_id" id="edit_maid_id" type="hidden" />
				<input name="edit_timefrom" id="edit_timefrom" type="hidden" />
				<input name="edit_timeto" id="edit_timeto" type="hidden" />
				<input name="edit_book_date" id="edit_book_date" type="hidden" />
				<button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
				<input type="button" class="n-btn m-0" value="Save" id="edit-booking-form-btn" />
			</div>
		</form>
	</div>
</div>

<div id="alert-popup" style="background:none;display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="alert-title"></span>
    </div>
    <div class="modal-body">
      <h3 id="alert-message"></h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0 pop_close">OK</button>
    </div>
  </div>
</div>

<script>
/*********************************************************************************** */
// function newPopup() {
	// $.fancybox.open({
		// autoCenter: true,
		// fitToView: false,
		// scrolling: false,
		// openEffect: 'none',
		// openSpeed: 1,
		// autoSize: false,
		// width: 450,
		// height: 'auto',
		// helpers: {
			// overlay: {
				// css: {
					// 'background': 'rgba(0, 0, 0, 0.3)'
				// },
				// closeClick: false
			// }
		// },
		// padding: 0,
		// closeBtn: false,
		// content: $('#new-popup'),
	// });
// }
/*********************************************************************************** */
</script>