<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php echo isset($page_title) ? html_escape($page_title) : $settings->site_name; ?> - <?=$settings->site_name;?></title>
        <link rel="manifest" href="img/fav/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="img/fav/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <link rel="icon" type="image/x-icon" href="<?= check_and_get_img_url('./uploads/images/settings/'.$settings->site_favicon, 'favicon.ico'); ?>">
        <link href="<?php echo base_url(); ?>css/newstyle.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/bootstrap-responsive.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.flexdatalist.css" type="text/css"/>
        <!--<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">-->
        <link href="<?php echo base_url(); ?>css/font-awesome.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/slider.css" type="text/css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/animation.css" type="text/css">
        <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/pages/dashboard.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/hm.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/jquery-ui.min.css" rel="stylesheet" type="text/css" />
		
                <link href="<?php echo base_url(); ?>css/dataTables.checkboxes.css" rel="stylesheet" type="text/css" />
				<link href="<?php echo base_url(); ?>css/fullcalendar.css" rel="stylesheet" type="text/css" />
				<link href="<?php echo base_url(); ?>css/select2.min.css" rel="stylesheet" type="text/css" />
        <?php
        if (isset($css_files) && is_array($css_files)) {
            foreach ($css_files as $css_file) {
                echo '<link rel="stylesheet" href="' . base_url() . 'css/' . $css_file . '" />';
            }
        }
        $extra_navbar_inner_style = '';
        $extra_subnavbar_inner_style = '';
        $body_background = '';
        if (strtolower($page_title) == 'bookings' || strtolower($page_title) == 'schedule') {
            $extra_navbar_inner_style = 'style="width: 104%;margin-left: -20px;"';
            $extra_subnavbar_inner_style = 'style="margin-top:54px;"';
            $body_background = 'style="background-color:#FFF;"';
        }
        ?>

<?php $this->load->view('includes/settings_js_css'); ?>

</head>
    <body <?php echo $body_background; ?>>
        <?php
        if (is_user_loggedin()) {
            ?>
            <header>
  <div class="row header-wrapper no-left-right-margin">
  
  <div class="col-md-12 col-sm-12 search-wrapper">
  
  
   <div class="col-md-3 col-sm-12 no-left-right-padding logo-section">
       <div class="logo"><a href="<?php echo base_url(); ?>"><img src="<?= check_and_get_img_url('./uploads/images/settings/'.$settings->website_logo, 'website-logo.png'); ?>" alt=""></a></div>
       <form class="navbar-search mob-menu-view-only">
        <!--<input class="search-query" id="head-search-f" placeholder="Search Customer" autocomplete="off" type="text">-->
		<select style="margin-top: 6px; width:220px; margin-bottom: 9px;" class="search-query customers_vh_rep_new" id="" name="customers_vh_rep_new">
			<option value="0">-- Select Customer --</option>
		</select>
     </form>
       <div class="clear"></div>
   </div>
   
   <div class="col-md-9 col-sm-6 no-left-right-padding search-section">
   
     <div class="admin-drp-main">
   		 <nav id="primary_nav_wrap"> 
        	<ul>
                   <li><a href="#" class="e-hed-icon"><i class="fa fa-user"></i> <?php echo user_authenticate_name()?></a>
                	
                    <ul class="last-ul">
                        <li><a href="<?php echo base_url() . 'dashboard/changepassword' ?>">Change Password</a></li>
                        <li><a href="<?php echo base_url() . 'logout' ?>">Logout</a></li>
                    </ul>
                    
                </li>
                
                
                
                   <?php if(user_authenticate() == 1) {?>
                    <li><a href="#" class="e-hed-icon"><i class="fa fa-cogs"></i></a>
                	
                    <ul>
                        <!--<li><a href="<?php// echo base_url() . 'sms-settings' ?>">SMS Configuration</a></li>
                        <li><a href="<?php// echo base_url() . 'email-settings' ?>">Email Configuration</a></li>
                        <li><a href="<?php// echo base_url() . 'tax-settings' ?>">Tax Configuration</a></li>-->
                        
                        <?php if(user_permission(user_authenticate(), 22)) {?><li><a href="<?php echo base_url() . 'users'; ?>">Users</a></li><?php } ?>
                    	<!--<li><a href="javascript:;">Settings</a></li>
                        <li><a href="javascript:;">Help</a></li>-->
                      
                    </ul>
                    
                </li>
                <?php } ?>
                <li style="width: 25px; padding: 0px;"><div class="mob-menu-icon mob-menu-view-only"><img src="<?php echo base_url(); ?>images/menu.png"></div><!--logo end--></li>
            </ul>
    </nav>
    </div>
  
     <form class="navbar-search pc-menu-view-only">
        <!--<input class="search-query" id="head-search-m" placeholder="Search Customer" autocomplete="off" type="text">-->
		<select style="margin-top: 6px; width:220px; margin-bottom: 9px;" class="search-query customers_vh_rep_new" id="" name="customers_vh_rep_new">
			<option value="0">-- Select Customer --</option>
		</select>
     </form>
    
    <div class="clear"></div>
    </div>
    
  </div>
  
  
  

        <div class="col-md-12 col-sm-12 no-left-right-padding menu">
           <?php $this->load->view('layouts/primary_navigation'); ?>
        </div>

  </div><!--row header-wrapper end-->
</header><!--header section end-->

            <div class="main" style="min-height: 572px;">
                <div class="main-inner">
                    <div class="container" style="width: 100%;">

    <?php
    echo $content_body;
    ?>
                    </div>

                </div>

            </div>
    <?php
} else {
    echo $content_body;
}
?>
        <?php
        if (is_user_loggedin()) {
            ?>
            <div class="footer">
                <div class="footer-inner">
                    <div class="container">
                        <div class="row">
                            <div class="span12"><?=$settings->site_footer_copyright_line_html;?></div>
                        </div>
                    </div>
                </div>
            </div>
<?php } ?>


        <!-- Le javascript
        ================================================== --> 
        <!-- Placed at the end of the document so the pages load faster --> 
        <script language="javascript">
            var _base_url = '<?php echo base_url(); ?>';
            var _page_url = '<?php echo current_url(); ?>';
        </script>
        <script src="<?php echo base_url(); ?>js/jquery-1.7.2.min.js"></script> 
        <script src="<?php echo base_url(); ?>js/bootstrap.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-2.1.0.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.min.js"></script>  
		<script type="text/javascript" src="<?php echo base_url(); ?>js/select2.min.js"></script>
        <script src="<?php echo base_url(); ?>js/owl.carousel.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/hm.js"></script>
        <script type="text/javascript" src="https://momentjs.com/downloads/moment.js"></script>
        <!--<script type="text/javascript" src="<?php// echo base_url(); ?>js/select2.js"></script>-->
        <script>
$(document).ready(function() {
	
	var owl = $("#owl-demo-client");
	owl.owlCarousel({
	items : 6, //10 items above 1000px browser width
	itemsDesktop : [1000,6], //5 items between 1000px and 901px
	itemsDesktopSmall : [900,2], // 3 items betweem 900px and 601px
	itemsTablet: [600,1], //2 items between 600 and 0;
	itemsMobile : [320,1], // itemsMobile disabled - inherit from itemsTablet option
  });
  
  
	var owl = $("#owl-demo");
	owl.owlCarousel({
	items : 2, //10 items above 1000px browser width
	itemsDesktop : [1000,2], //5 items between 1000px and 901px
	itemsDesktopSmall : [900,2], // 3 items betweem 900px and 601px
	itemsTablet: [600,1], //2 items between 600 and 0;
	itemsMobile : [320,1], // itemsMobile disabled - inherit from itemsTablet option
  });
  
  $(".next").click(function(){
    owl.trigger('owl.next');
  });
  
  $(".prev").click(function(){
    owl.trigger('owl.prev');
  });
  
  
  $('.mob-menu-icon') .click(function(){
  $('.menu').toggle(1000);
  });
  
  $('.mob-menu-hide') .click(function(){
  $('.menu').hide(1000);
  });
  
  setTimeout(function(){
	  $('.social-fixed').show();
	  }, 1000);
	  

$('#button_file').click(function () {
   	$("input[type='file']").trigger('click');
})

$("input[type='file']").change(function () {
   	$('#val').text(this.value.replace(/C:\\fakepath\\/i, ''))
})


  
});
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/animation.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.flexslider-min.js"></script>
<script type="text/javascript" charset="utf-8">
    var $ = jQuery.noConflict();
    $(window).load(function() {
    $('.flexslider').flexslider({
          animation: "slide"
    });
 });
</script>


<script type="text/javascript" src="<?php echo base_url(); ?>js/classie.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.flexdatalist.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/fullcalendar.min.js"></script>
<script type="text/javascript" charset="utf-8">
    function init() {
        window.addEventListener('scroll', function(e){
            var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                shrinkOn = 300,
                header = document.querySelector("header");
            if (distanceY > shrinkOn) {
                classie.add(header,"smaller");
            } else {
                if (classie.has(header,"smaller")) {
                    classie.remove(header,"smaller");
                }
            }
        });
    }
    window.onload = init();
</script>

<script>
	$(document).ready(function() {
		var urls = '<?php echo base_url(); ?>';
		//var urls = $('#baseurls').val();
		var d = new Date();
		var month = d.getMonth()+1;
		var day = d.getDate();
		var output = d.getFullYear() + '-' + ((''+month).length<2 ? '0' : '') + month + '-' + ((''+day).length<2 ? '0' : '') + day;
		/*Maint*/
		$('#calendar_cln').fullCalendar({
			header: {
				//left: 'prev,next today',
				left: '',
				center: 'title',
				//right: 'month,basicWeek,basicDay'
				//right: 'month,agendaWeek,agendaDay'
				right: 'prev,next'
			},
            titleFormat: 'MMMM D, YYYY',
            columnFormat: {
            month: 'ddd',
            week: 'ddd - MMM D',
            day: 'dddd d/M'
        },
            height: 'auto',
			defaultDate: output,//'2016-01-12'
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			selectable: true,
			selectHelper: true,
                        minTime: '08:00:00',
                        maxTime: '20:00:00',
                        defaultView: 'agendaWeek',
                        //hiddenDays: [ 0 ],
                        displayEventTime: false,
                        eventDurationEditable: false,
                        eventStartEditable: false,
                        allDaySlot: false,
			select: function(start, end) {
                /********************************** */
                var startDate = moment(start),
    endDate = moment(end),
    date = startDate.clone(),
    isWeekend = false;

    while (date.isBefore(endDate)) {
        if (date.isoWeekday() == 7) {
            isWeekend = true;
        }    
        date.add(1, 'day');
    }

    if (isWeekend) {
        alert('You can\'t add on weekends !');

        return false;
    }
    /********************************** */
				//alert(moment(start).format('YYYY-MM-DD HH:mm:ss'));
                                var select_date = moment(start).format('YYYY-MM-DD');
                                var select_start_time = moment(start).format('HH:mm:ss');
                                var select_end_time = moment(end).format('HH:mm:ss');
                                $('.mm-loader').show();
                                $.ajax({
                                    url: urls + 'booking/addscheduletime',
                                    type: "POST",
                                    data: {select_date:select_date,start:select_start_time,end:select_end_time},
                                    success: function(rep) {
                                        if(rep == 'success'){
                                            $('.mm-loader').hide();
                                            location.reload();
                                        }else{
                                            $('.mm-loader').hide();
                                            alert('Could not be saved. try again.'); 
                                        }
                                    }
                                });
			},
			eventRender: function(event, element) {
				element.bind('click', function() {
                                    
                                    $('.mm-loader').show();
                                    $.ajax({
                                        url: urls + 'booking/deletescheduletime',
                                        type: "POST",
                                        data: {timeid:event.id},
                                        success: function(rep) {
                                         
                                            if(rep == 'success'){
                                                $('.mm-loader').hide();
                                                location.reload();
                                            }else{
                                                $('.mm-loader').hide();
                                                alert('Could not be saved. try again.'); 
                                            }
                                        }
                                    });
				});
			},
			events: [
			<?php foreach($availabletimings_maint as $event): ?>
				{
					id: '<?php echo $event->id; ?>',
					title: '',
					start: '<?php echo $event->date; ?> <?php echo $event->from_time; ?>',
					end: '<?php echo $event->date; ?> <?php echo $event->to_time; ?>',
					color: '#008000',
                    className: 'selected-event'
                    //allDay : false
				},
			<?php endforeach; ?>
			]
		});
		/*Ends*/
	});

</script>


<script type="text/javascript">
    jQuery(document).ready(function () {
        var pxShow = 300;//height on which the button will show
        var fadeInTime = 1000;//how slow/fast you want the button to show
        var fadeOutTime = 1000;//how slow/fast you want the button to hide
        var scrollSpeed = 1000;//how slow/fast you want the button to scroll to top. can be a value, 'slow', 'normal' or 'fast'
        jQuery(window).scroll(function () {
            if (jQuery(window).scrollTop() >= pxShow) {
                jQuery(".backtotop").fadeIn(fadeInTime);
            } else {
                jQuery(".backtotop").fadeOut(fadeOutTime);
            }
        });

        jQuery('.backtotop a').click(function () {
            jQuery('html, body').animate({scrollTop: 0}, scrollSpeed);
            return false;
        });
    });
</script>
    
<script type="text/javascript">
$(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});
</script>
    <style>
        #primary_nav_wrap li:has(ul li a.active) > a {
            background: #fff;
            color: #333 !important;
        }
        #primary_nav_wrap ul li ul li .active {
            background: #4c656e;
            color: #fff !important;
        }
.fc-sun { 
color:red;  
border-color: black;  
background-color: #ffc0c0; }
    </style>
    <!----------------------------------------------------------------------------- -->
<?php
if (isset($external_js_files) && is_array($external_js_files)) {
    foreach ($external_js_files as $external_js_file) {
        echo '<script src="' . $external_js_file . '"></script>' . "\n";
    }
}
if (isset($js_files) && is_array($js_files)) {
    foreach ($js_files as $js_file) {
        echo '<script type="text/javascript" src="' . base_url() . 'js/' . $js_file . '"></script>' . "\n";
    }
}
?>
<div class="mm-loader"></div>
<script type="text/javascript" src="<?= base_url('js/main.js?v=') .JS_VERSION; ?>"></script>
    </body>
</html>