<nav id="primary_nav_wrap">
    <ul>
        <li><a href="<?php echo base_url('dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <?php if (user_permission(user_authenticate(), 2)) { ?>
            <li>
                <a href="<?php echo base_url() . 'schedule' ?>" onclick="return false;">
<i class="fa fa-calendar"></i>&nbsp;&nbsp;Schedule&nbsp;&nbsp;<i class="fa fa-caret-down"></i>
                </a>
                <ul>
                    <?php if (user_permission(user_authenticate(), 2)) { ?><li><a href="<?php echo base_url() . 'booking' ?>"><i class="fa fa-calendar"></i>&nbsp;&nbsp;Booking</a></li><?php } ?>
                    <?php if (user_permission(user_authenticate(), 2)) { ?><li><a href="<?php echo base_url() . 'edit-past-booking' ?>"><i class="fa fa-calendar"></i>&nbsp;&nbsp;Edit Booking</a></li><?php } ?>
                    <?php if (user_permission(user_authenticate(), 2)) { ?><li><a href="<?php echo base_url() . 'booking/approvallist' ?>"><i class="icon-list-alt"></i>&nbsp;&nbsp;Web Bookings</a></li><?php } ?>
                    <?php /*if (user_permission(user_authenticate(), 2)) { ?><li><a href="<?php echo base_url() . 'justmop' ?>"><i class="fa fa-calendar"></i> Upload From Justmop</a></li><?php }*/ ?>
                    <?php /*if (user_permission(user_authenticate(), 2)) { ?><li><a href="<?php echo base_url() . 'justmop/approvallist' ?>"><i class="fa fa-calendar"></i> Justmop Bookings</a></li><?php }*/ ?>
                    <?php if (user_permission(user_authenticate(), 4)) { ?><li><a href="<?php echo base_url() . 'maid/schedule' ?>"><i class="icon-calendar-empty"></i>&nbsp;&nbsp;Maid Schedule</a></li><?php } ?>
                    <?php if (user_permission(user_authenticate(), 4)) { ?><li><a href="<?php echo base_url() . 'booking/new_booking' ?>"><i class="icon-plus-sign-alt"></i>&nbsp;&nbsp;New Booking</a></li><?php } ?>
                    <?php if (user_permission(user_authenticate(), 4)) { ?><li><a href="<?php echo base_url() . 'booking/block_timing' ?>"><i class="icon-th"></i>&nbsp;&nbsp;Block Timing</a></li><?php } ?>

                </ul>
            <?php } ?>
            <!--<li><a <?php echo isset($maps_active) ? 'class="active"' : ''; ?> href="<?php echo base_url() . 'booking/locations' ?>"><i class="fa fa-map-marker"></i> Maps</a></li>-->
            <li><a onclick="return false;" href="<?php echo base_url('location') ?>"><i class="fa fa-map-marker"></i>&nbsp;&nbsp;Maps&nbsp;&nbsp;<i class="fa fa-caret-down"></i></a>
                    <ul>
                        <li><a href="<?php echo base_url('location/booking'); ?>"><i class="fa fa-calendar"></i>&nbsp;&nbsp;Bookings</a></li>
                        <li><a href="<?php echo base_url('location/driver'); ?>"><i class="fa fa-car"></i>&nbsp;&nbsp;Drivers</a></li>
                    </ul>
                </li>
            <li>
            
            
            
            
            
            
            
            <?php if (user_permission(user_authenticate(), 5)) { ?><li><a href="<?= base_url('activity/jobs') ?>"><i class="fa fa-tasks"></i>&nbsp;&nbsp;Jobs</a></li><?php } ?>
            <?php if (user_permission(user_authenticate(), 6)) { ?>
                <li>
                    <a href="<?php echo base_url() . 'customers' ?>"><i class="fa fa-user"></i>&nbsp;&nbsp;Customers</a>
					<ul>
                        <!--<li><a href="<?php echo base_url('customer/outstanding'); ?>"><i class="fa fa-bookmark"></i>&nbsp;&nbsp;Outstanding Report</a></li>-->
                        <li><a href="<?php echo base_url('customer/outstanding-report'); ?>"><i class="fa fa-bookmark"></i>&nbsp;&nbsp;Outstanding Report</a></li>
                    </ul>
                </li>
            <?php } ?>
            <li><a href="<?php echo base_url() . 'maids' ?>"><i class="fa fa-group"></i>&nbsp;&nbsp;Staffs</a></li>
            <li><a href="<?php echo base_url('package?status=1'); ?>"><i class="fa fa-inbox"></i>&nbsp;&nbsp;Packages&nbsp;<i class="fa fa-caret-down"></i></a>
                    <ul>
                        <li><a href="<?php echo base_url('package_subscription?status=1'); ?>"><i class="fa fa-bookmark"></i>&nbsp;&nbsp;Subscriptions</a></li>
                    </ul>
                </li>
            <li><a href="#"><i class="fa fa-cogs"></i>&nbsp;&nbsp;Settings&nbsp;&nbsp;<i class="fa fa-caret-down"></i></a>
                <ul>
					<?php if (user_permission(user_authenticate(), 7)) {?><li><a href="<?php echo base_url('holidays'); ?>">Holidays</a></li><?php }?>
                    <?php if (user_permission(user_authenticate(), 7)) { ?><li><a href="<?php echo base_url() . 'zones' ?>">Zones</a></li><?php } ?>
                    <?php if (user_permission(user_authenticate(), 8)) { ?><li><a href="<?php echo base_url() . 'areas' ?>">Areas</a></li><?php } ?>
                    <?php if (user_permission(user_authenticate(), 9)) { ?><li><a href="<?php echo base_url() . 'flats' ?>">Flats</a></li><?php } ?>
                    <?php if (user_permission(user_authenticate(), 10)) { ?><li><a href="<?php echo base_url() . 'tablets' ?>">Tablets</a></li><?php } ?>
                    <?php if (user_permission(user_authenticate(), 12)) { ?><li><a href="<?php echo base_url() . 'offers' ?>">Offers</a></li><?php } ?>
                    <!--<li><a href="<?php // echo base_url() . 'teams' 
                                        ?>">Teams</a></li>
                        <li><a href="<?php // echo base_url() . 'payment-settings' 
                                        ?>">Payment Settings</a></li>-->
                    <?php if (user_permission(user_authenticate(), 11)) { ?><li><a href="<?php echo base_url() . 'services' ?>">Services</a></li><?php } ?>
                    <?php
                    if (user_authenticate() == 1) {
                    ?>
                        <li><a href="<?php echo base_url() . 'users'; ?>"><span>User Management</span></a></li>
                    <?php } ?>
                    <li><a href="<?php echo base_url() . 'sms-list-upload'; ?>"><span>SMS List Upload</span></a></li>
                    <?php
                    if (user_authenticate() == 1) {
                    ?>
                        <li><a href="<?php echo base_url() . 'bulk-sms'; ?>"><span>Bulk SMS</span></a></li>
                    <?php
                    }
                    ?>
                    <li><a href="<?php echo base_url() . 'email-list-upload'; ?>"><span>Email List Upload</span></a></li>
                    <?php
                    if (user_authenticate() == 1) {
                    ?>
                        <li><a href="<?php echo base_url() . 'customer/customer_bulk_email' ?>"><span>Bulk Email</span></a></li>
                    <?php
                    }
                    ?>
                    <li><a href="<?php echo base_url() . 'coupons'; ?>"><span>Coupon Management</span></a></li>
                    <!--<li><a href="#" target="_blank"><span>Offer Popup</span></a></li>-->
                    <li>
                        <a href="javascript:{}" onclick="document.getElementById('fcm_act').submit();">Notification Activate</a>
                        <form action="#" target="_blank" id="fcm_act" method="POST">
                            <input type="hidden" name="ws" value="ef3ba09609d17359d1ac3d2516d0b7f2" />
                            <input type="hidden" name="uid" value="<?php echo user_authenticate(); ?>" />
                        </form>
                    </li>
                </ul>
            </li>
            <li><a href="<?php echo base_url() . 'booking/delayed_report' ?>"><i class="fa fa-file-text-o"></i> Delay Report &nbsp; </a>
            </li>
            <li><a><i class="fa fa-book" aria-hidden="true"></i>&nbsp;&nbsp;Accounting&nbsp;<i class="fa fa-caret-down"></i></a>
				<ul>
					<?php
					if($settings->enableQuickBook == 1)
					{
					?>
					<li><a href="<?php echo base_url('invoice/list_customer_invoices'); ?>"><i class="fa fa-bookmark-o" aria-hidden="true"></i>&nbsp;&nbsp;QuickBooks</a>
						<ul>
							<li><a href="<?php echo base_url('quickbook'); ?>"><i class="fa fa-bookmark-o" aria-hidden="true"></i>&nbsp;&nbsp;QuickBook Connection</a></li>
							<li><a href="<?php echo base_url('quickbook/sync_customers'); ?>"><i class="fa fa-bookmark-o" aria-hidden="true"></i>&nbsp;&nbsp;Sync Customers</a></li>
							<li><a href="<?php echo base_url('quickbook/sync_customer_payments'); ?>"><i class="fa fa-bookmark-o" aria-hidden="true"></i>&nbsp;&nbsp;Sync Customer Payments</a></li>
							<li><a href="<?php echo base_url('quickbook/sync_invoices'); ?>"><i class="fa fa-bookmark-o" aria-hidden="true"></i>&nbsp;&nbsp;Sync Invoices</a></li>
							<li><a href="<?php echo base_url('quickbook/sync_invoice_payments'); ?>"><i class="fa fa-bookmark-o" aria-hidden="true"></i>&nbsp;&nbsp;Sync Invoice Payments</a></li>
						</ul>
					</li>
					<?php
					}
					?>
                    <li><a href="<?php echo base_url('invoice/list_customer_invoices'); ?>"><i class="fa fa-bookmark-o" aria-hidden="true"></i>&nbsp;&nbsp;Customer Invoices</a></li>
                    <li><a href="<?php echo base_url('add-monthly-invoice'); ?>"><i class="fa fa-bookmark-o" aria-hidden="true"></i>&nbsp;&nbsp;Customer Monthly Invoices New</a></li>
                    <li><a href="<?php echo base_url('invoice/list_customer_monthly_invoices'); ?>"><i class="fa fa-bookmark-o" aria-hidden="true"></i>&nbsp;&nbsp;Customer Monthly Invoices</a></li>
                    <li><a href="<?php echo base_url('invoice/list/monthly-advanced'); ?>" style=""><i class="fa fa-bookmark-o" aria-hidden="true"></i>&nbsp;&nbsp;Customer Monthly Invoices Advanced</a></li>
                    <li><a href="<?php echo base_url('customer/list_customer_payments'); ?>"><i class="fa fa-bookmark-o" aria-hidden="true"></i>&nbsp;&nbsp;Customer Payments</a></li>
                    <li><a href="<?php echo base_url('customer/online_payments'); ?>"><i class="fa fa-bookmark-o" aria-hidden="true"></i>&nbsp;&nbsp;Online Payments</a></li>
                    <!--<li><a href="<?php// echo base_url('invoice/create_invoice'); ?>"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;New Invoice</a></li>-->
                </ul>
            </li>
            <li><a href="<?php echo base_url() . 'activity' ?>"><i class="fa fa-file-text-o"></i> Plan</a></li>
            <li><a href="#"><i class="fa fa-copy"></i>&nbsp;&nbsp;Reports&nbsp;&nbsp;<i class="fa fa-caret-down"></i></a>
                <ul class="last-ul">
				<li><a href="<?= base_url('reports/schedule_report_service_wise'); ?>">Service Wise Report</a></li>
				<li><a href="<?= base_url('reports/customer_sales_report'); ?>">Customer Sales Report</a></li>
				<li><a href="<?= base_url('reports/invoice_report'); ?>">Invoice Report</a></li>
                    <li><a href="<?= base_url('reports/customerbookinghours'); ?>">Customer Hours Report</a></li>
                    <li><a href="<?= base_url('reports/schedule-report' ); ?>">Schedule Report</a></li>
                    <?php if (user_permission(user_authenticate(), 13)) {?><li><a href="<?php echo base_url() . 'reports/zone' ?>">Zone Reports</a></li><?php }?>
                    <?php if (user_permission(user_authenticate(), 14)) { ?><li><a href="<?php echo base_url() . 'reports/vehicle' ?>">Vehicle Reports</a></li><?php } ?>
                    <!--<?php if (user_permission(user_authenticate(), 14)) { ?><li><a href="<?php echo base_url() . 'reports/newreport' ?>">Driver Reports</a></li><?php } ?>-->
                    <?php if (user_permission(user_authenticate(), 13)) { ?><li><a href="<?php echo base_url() . 'booking/booking_reports' ?>">Maid Performance Report</a></li><?php } ?>
                    <!--<?php // if(user_permission(user_authenticate(), 23)) {
                        ?><li><a href="<?php // echo base_url() . 'reports/call-report' 
                                                            ?>">Call Report</a></li><?php // } 
                                                                                    ?>-->
                    <?php if (user_permission(user_authenticate(), 16)) { ?><li><a href="<?php echo base_url() . 'reports/oneday' ?>">One Day Cancel Report</a></li><?php } ?>
                    <?php if (user_permission(user_authenticate(), 16)) { ?><li><a href="<?php echo base_url() . 'reports/booking/cancel' ?>">Booking Cancel Report</a></li><?php } ?>
                    <?php if (user_permission(user_authenticate(), 17)) { ?><li><a href="<?php echo base_url() . 'reports/work' ?>">Work Report of an Employee</a></li><?php } ?>
                    <?php if (user_permission(user_authenticate(), 17)) { ?><li><a href="<?php echo base_url() . 'reports/work/all' ?>">Employees Work Report</a></li><?php } ?>
                    <?php if (user_permission(user_authenticate(), 18)) { ?><li><a href="<?php echo base_url() . 'reports/maidattendance' ?>">Staff Attendance Report</a></li><?php } ?>
                    <!--<?php if (user_permission(user_authenticate(), 19)) { ?><li><a href="<?php echo base_url() . 'reports/schedule-report' ?>">Driver Activity Report</a></li><?php } ?>-->
                    <?php if (user_permission(user_authenticate(), 19)) { ?><li><a href="<?php echo base_url() . 'reports/useractivity' ?>">User Activity Report</a></li><?php } ?>
                    <?php if (user_permission(user_authenticate(), 20)) { ?><li><a href="<?php echo base_url() . 'reports/activity' ?>">Activity Summary Report</a></li><?php } ?>
                    <?php if (user_permission(user_authenticate(), 21)) { ?><li><a href="<?php echo base_url() . 'reports/vehicleattendance' ?>">Staff Vehicle Report</a></li><?php } ?>
                    <?php if (user_permission(user_authenticate(), 21)) { ?><li><a href="<?php echo base_url() . 'reports/zone-wise-booking-report' ?>">Zone Wise Booking Report</a></li><?php } ?>
                    <?php if (user_permission(user_authenticate(), 21)) { ?><li><a href="<?php echo base_url() . 'reports/maid-leave-report' ?>">Staff Leave Report</a></li><?php } ?>
                    <li><a href="<?php echo base_url() . 'reports/maidhours' ?>">Staff Hours Report</a></li>
                    <!--<li><a href="<?php// echo base_url() . 'backpayment' ?>">Back Payments</a></li>-->
                    <!--<li><a href="<?php echo base_url('reports/customer-booking-hours'); ?>"> Customer Hours Report</a></li>-->
                    <li><a href="<?php echo base_url() . 'rating-review' ?>">Rating & Review</a></li>
                    <li><a href="<?php echo base_url() . 'complaint-report' ?>">Complaints Report</a></li>
                </ul>
            </li>
    </ul>
</nav>