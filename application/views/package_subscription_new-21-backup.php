<style type="text/css">
    .select2-arrow {
        visibility: hidden;
    }

    .select2-container .select2-choice {
        -moz-appearance: none;
        background: #fff url("../images/ash-arrow.png") no-repeat scroll right 7px top 11px;
        border: 1px solid #ccc;
        border-radius: 3px;
        cursor: pointer;
        font-size: 12px;
        height: 30px;
        line-height: 24px;
        padding: 3px 0 3px 10px;

        text-indent: 0.01px;
    }

    #user-repeat-days label {
        display: inline;
    }

    .in-bookingform-field-droop-main label {
        display: inline;
    }

    #user-repeat-end-date {
        width: 73%;
    }

    .select2-results__options li {
        margin-left: 0px;
    }
	
	.in-bookingform-field-droop-main { width: 100%; float: none;}
</style>

<?php
if ($this->uri->segment(2) == 'view') {
    $readonly = 'disabled';
}
?>
<section>
    <div class="">
        <div class="col-md-12 col-sm-12">
            <!--<div class="widget widget-table action-table" style="margin-bottom:30px">-->
            <div class="widget-header" style="margin-bottom: 0;">
                <i class="icon-th-list"></i>
                <h3><?php echo $this->uri->segment(2) == 'edit' ? 'Update Subscription' : ($this->uri->segment(2) == 'new' ? 'New Subscription' : 'Subscription Details'); ?></h3>
            </div>

            <div class="col-md-12 col-sm-12 confirm-det-cont-box borderbox" style="background: #FFF;">
                <?php echo form_open('package_subscription/save', array('id' => 'add-user-booking-form')); ?>
                <?php if ($this->uri->segment(2) == 'edit') : ?>
                    <input name="id" id="id" type="hidden" value="<?php echo $data['id']; ?>" required>
                <?php endif; ?>
                
                <div class="col-md-6 col-sm-6 confi-det-cont-det new-booking-box-main no-left-padding">
                
                    <div class="col-md-12 col-sm-12 text-field-main">
                        <div id="u-error" style="color:red;"></div>
                    </div>
                    
                    
                    
<div class="col-sm-12">
                    
<div class="row m-0 n-field-main">
    <p><?php echo form_label('Customer <span>:</span>'); ?></p>
    <div class="n-field-box">
         <div class="in-bookingform-field-droop-main">
              <select class="customer-name sel2" name="customer_id" id="customer_id" required <?php echo $readonly; ?> style="width:100%">
                  <option value="<?php echo $data['customer_id']; ?>"><?php echo $data['customer_name']; ?></option>
              </select>
          </div>
    </div>
</div>

</div>   
<div class="col-sm-12">              

<div class="row m-0 n-field-main">
    <p>Package</p>
    <div class="n-field-box">
        <select class="package-name sel2" name="package_id" id="package_id" required <?php echo $readonly; ?> style="width:100%">
            <option value="<?php echo $data['package_id']; ?>"><?php echo $data['package_name']; ?></option>
        </select>
    </div>
</div>
                    
                    
</div>   
<div class="col-sm-12">                  
                    

<div class="row m-0 n-field-main">
    <p><?php echo form_label('Amount <span>:</span>', 'user_total_amt',  array('class' => '')); ?></p>
    <div class="n-field-box">
        <div class="in-bookingform-field-droop-main">
             <input name="price" id="price" type="number" class="popup-disc-fld" value="<?php echo $data['price']; ?>" required <?php echo $readonly; ?>>
        </div>
    </div>
</div>

</div>   
<div class="col-sm-12">                
                    

<div class="row m-0 n-field-main">
    <p><?php echo form_label('Notes <span>:</span>', 'user_notes',  array('class' => '')); ?></p>
    <div class="n-field-box">
        <div class="in-bookingform-field-droop-main mr-0">
            <textarea name="notes" id="notes" class="full-width" <?php echo $readonly; ?>><?php echo $data['notes']; ?></textarea>
        </div>
    </div>
</div>

                   
</div>   
<div class="col-sm-12 p-0">               
                    

<div class="row m-0 n-field-main">
    <p> <input type="hidden" id="customer-address-id-user" name="customer-address-id-user" disabled>
        <input type="hidden" id="booking-id-user" disabled></p>
    <div class="col-sm-6 n-field-box">
         <?php if ($this->uri->segment(2) != 'view') : ?>
                <!--<div class="col-md-8 col-sm-8 text-field-det">
                    <div class="in-bookingform-field-droop-main">-->
                        <input type="submit" class="save-but full-width" id="add-user-booking-nform" value="<?php echo $this->uri->segment(2) == 'edit' ? 'Update' : 'Save'; ?>" />
                    <!--</div>
                </div>-->
         <?php endif; ?>
    </div>
</div>

                    
</div>                 
                    
                    
                </div>
                
                <?php echo form_close(); ?>
                <div id="maid_search" style="border: medium none !important;"></div>
            </div>
        </div>
        <!--welcome-text-main end-->

    </div>
    <!--row content-wrapper end-->
</section>
<!--welcome-text end-->

<script>
    $(document).ready(function() {
        $('.customer-name').select2({
            ajax: {
                url: '<?php echo base_url('package_subscription/customer_search'); ?>',
                dataType: 'json',
                data: function(params) {
                    var query = {
                        query: params.term,
                    }
                    return query;
                },
                processResults: function(data) {
                    // Transforms the top-level key of the response object from 'items' to 'results'
                    return {
                        results: data.results
                    };
                }
            }
        });
        $('.package-name').select2({
            ajax: {
                url: '<?php echo base_url('package_subscription/package_search'); ?>',
                dataType: 'json',
                data: function(params) {
                    var query = {
                        query: params.term,
                    }
                    return query;
                },
                processResults: function(data) {
                    // Transforms the top-level key of the response object from 'items' to 'results'
                    return {
                        results: data.results
                    };
                }
            }
        });
    });
</script>