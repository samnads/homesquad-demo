<div class="row m-0"> 
  <!-- /span6 -->
  <div class="col-sm-12" id="add_user">
    <div class="widget ">
      <div class="widget-header" style="margin-bottom: 0;">
        <ul>
          <li> <i class="icon-globe"></i>
            <h3>Edit Coupon</h3>
          </li>
          <li class="mr-0 float-right">
            <div class="topiconnew border-0 green-btn"> <a href="<?php echo base_url(); ?>coupons" title=""> <i class="fa fa-users"></i></a> </div>
          </li>
        </ul>
      </div>
      <!-- /widget-header -->
      
      <div class="widget-content">
        <div class="tabbable">
          <div class="tab-content">
            <form id="useraadd" action="<?php echo base_url(); ?>coupons/edit/<?php echo $coupon_id; ?>" class="form-horizontal" method="post">
              <fieldset>
              
              
                <div class="control-group mb-0">
                  <div class="error" style="text-align: center;"><?php echo isset($error) && strlen(trim($error)) > 0 ? $error : '&nbsp;'; ?></div>
                </div>
                
                
                <div class="row m-0">
                <div class="col-sm-4">
                
                <div class="row m-0 n-field-main">
                    <p>Coupon Name</p>
                    <div class="col-sm-12 p-0 n-field-box">
                         <input type="text" class="" id="couponname" name="couponname" value="<?php echo $coupon_info->coupon_name; ?>">
                    </div>
                </div>


      
                
                
                
                
                <div class="row m-0 n-field-main">
                    <p>Expiry Date</p>
                    <div class="col-sm-12 p-0 n-field-box">
                         <input type="text" id="expirydate" readonly="readonly" name="expirydate" value="<?php echo $coupon_info->expiry_date; ?>">
                    </div>
                </div>
                
                
 
 
               
               
                <div class="row m-0 n-field-main">
                    <p>Service</p>
                    <div class="col-sm-12 p-0 n-field-box">
                         <select class="form-control select2" name="service_type" id="service_type">
                      <option value="">Select Option</option>
                      <?php foreach ($services as $service){ ?>
                      <option value="<?php echo $service['service_type_id'];?>" <?php if($service['service_type_id']==$coupon_info->service_id){echo 'selected';}?>><?php echo $service['service_type_name'];?></option>
                      <?php } ?>
                    </select>
                    </div>
                </div>
                
                
             
             
             
                
                <div class="row m-0 n-field-main">
                    <?php  
					  if($coupon_info->offer_type == "F")
					  {
						  if($coupon->discount_type == 0)
						  {
							  $offertype = "Flat Percentage";
						  }
						  else
						  {
							  $offertype = "Flat Rate";
						  }
						  
					  } else {
						  $offertype = "Rate Per Hour";
					  }
					  ?>
                    <p>Discount <?php echo $offertype;?></p>
                    <div class="col-sm-12 p-0 n-field-box">
                         <input type="text" id="discountprice" name="discountprice" value="<?php echo $coupon_info->percentage; ?>">
                    </div>
                </div>
                
                
                
             
             
            
            
                
                <div class="row m-0 n-field-main">
                    <p>Minimum Hour</p>
                    <div class="col-sm-12 p-0 n-field-box">
                         <select class="form-control" name="minhour" id="" >
                      <option value="">Select Hour</option>
                      <option value="2" <?php if($coupon_info->min_hrs == 2) { echo 'selected="selected"'; } ?>>2</option>
                      <option value="3" <?php if($coupon_info->min_hrs == 3) { echo 'selected="selected"'; } ?>>3</option>
                      <option value="4" <?php if($coupon_info->min_hrs == 4) { echo 'selected="selected"'; } ?>>4</option>
                      <option value="5" <?php if($coupon_info->min_hrs == 5) { echo 'selected="selected"'; } ?>>5</option>
                      <option value="6" <?php if($coupon_info->min_hrs == 6) { echo 'selected="selected"'; } ?>>6</option>
                      <option value="7" <?php if($coupon_info->min_hrs == 7) { echo 'selected="selected"'; } ?>>7</option>
                    </select>
                    </div>
                </div>
                
                
                
                
                <?php
				$weeekdays = $coupon_info->valid_week_day;
				$arr = explode(",",$weeekdays);
				?>
                
<div class="row m-0 n-field-main">
    <p>Week Days</p>
    <div class="col-sm-12 pl-0 pr-0 n-field-box n-field-day">

<div class="n-days">
    <input id="repeat-on-0" type="checkbox" value="0" name="w_day[]" class="w_day" <?php echo (in_array('0', $arr)) ? "checked='checked'" : ''; ?>>
    <label for="repeat-on-0"> <span class="border-radius-3"></span><p>Sun</p></label>
</div>

<div class="n-days">
    <input id="repeat-on-1" type="checkbox" value="1" name="w_day[]" class="w_day" <?php echo (in_array('1', $arr)) ? "checked='checked'" : ''; ?>>
    <label for="repeat-on-1"> <span class="border-radius-3"></span><p>Mon</p></label>
</div>

<div class="n-days">
    <input id="repeat-on-2" type="checkbox" value="2" name="w_day[]" class="w_day" <?php echo (in_array('2', $arr)) ? "checked='checked'" : ''; ?>>
    <label for="repeat-on-2"> <span class="border-radius-3"></span><p>Tue</p></label>
</div>

<div class="n-days">
    <input id="repeat-on-3" type="checkbox" value="3" name="w_day[]" class="w_day" <?php echo (in_array('3', $arr)) ? "checked='checked'" : ''; ?>>
    <label for="repeat-on-3"> <span class="border-radius-3"></span><p>Wed</p></label>
</div>

<div class="n-days">
    <input id="repeat-on-4" type="checkbox" value="4" name="w_day[]" class="w_day" <?php echo (in_array('4', $arr)) ? "checked='checked'" : ''; ?>>
    <label for="repeat-on-4"> <span class="border-radius-3"></span><p>Thu</p></label>
</div>

<div class="n-days">
  <input id="repeat-on-5" type="checkbox" value="5" name="w_day[]" class="w_day" <?php echo (in_array('5', $arr)) ? "checked='checked'" : ''; ?>>
    <label for="repeat-on-5"> <span class="border-radius-3"></span><p>Fri</p></label>
</div>

<div class="n-days pr-0">
  <input id="repeat-on-6" type="checkbox" value="6" name="w_day[]" class="w_day" <?php echo (in_array('6', $arr)) ? "checked='checked'" : ''; ?>>
  <label for="repeat-on-6"> <span class="border-radius-3"></span><p>Sat</p></label>
</div>
<div class="clear"></div>
</div>
</div>
                
                
                

	            
                
               
               
                
                
                
         <div class="row m-0 n-field-main" >
             <div class="col-sm-6 pl-0 n-field-box n-end-main">
                   <p>For App Only</p>
                   <div class="n-end">
                        <input type="radio" id="forapp" value="1" name="couponforapp" <?php if($coupon_info->forApp == 1) { echo 'checked="checked"'; } ?>>
                        <label for="forapp"> <span class="border-radius-3"></span><p class="pt-2">Yes</p></label>
                   </div>
              </div>
              
              <div class="col-sm-6 pr-0 n-field-box">
					<p>&nbsp;</p>
                  <div class="n-end">
                       <input type="radio" id="forapp1" value="0" name="couponforapp" <?php if($coupon_info->forApp == 0) { echo 'checked="checked"'; } ?>>
                       <label for="forapp1"> <span class="border-radius-3"></span><p class="pt-2">No</p></label>
                  </div>
              </div>
            </div>        
                
         <div class="row m-0 n-field-main" >
             <div class="col-sm-6 pl-0 n-field-box n-end-main">
                   <p>Status Active</p>
                   <div class="n-end">
                        <input id="status" type="radio" value="1" name="status" <?php if($coupon_info->status == 1) { echo 'checked="checked"'; } ?>>
                        <label for="status"> <span class="border-radius-3"></span></label>
                   </div>
              </div>
              
              <div class="col-sm-6 pr-0 n-field-box"> 
                  <p>Status Inactive</p> 
                  <div class="n-end">
                       <input id="status1" type="radio" value="0" name="status" <?php if($coupon_info->status == 0) { echo 'checked="checked"'; } ?>>
                       <label for="status1"> <span class="border-radius-3"></span></label>
                  </div>
              </div>
            </div>
  
  
        
        
                
                
                
                
                
              
              
                <div class="row m-0 pt-4 n-field-main">
                    <div class="col-sm-12 p-0">
                         <input type="submit" class="n-btn" value="Submit" name="coupon_sub">
                    </div>
                </div>
                
                
                </div>
                </div>
                
                
              </fieldset>
            </form>
          </div>
        </div>
      </div>
      <!-- /widget-content --> 
    </div>
    <!-- /widget --> 
  </div>
  <!-- /span6 --> 
  
</div>
