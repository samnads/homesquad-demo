<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
</style>
<div class="row">   
    <div class="span12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form id="edit-profile" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <ul>
                    <li>
                    <i class="icon-th-list"></i>
                    <h3>Payment Reports</h3> 
                    </li>
                    <li>                  
                    <?php
                    if ($search['search_date'] == "") {
                        ?>
                        <input type="text" style="width: 160px;" id="payment_date" name="payment_date" value="<?php echo date('d/m/Y') ?>">
                        
                      </li>
                        <li>

                        <?Php
                    } else {
                        ?>
                        <input type="text" style="width: 160px;" id="payment_date" name="payment_date" value="<?php echo $search['search_date'] ?>">
                        <?Php
                    }
                    ?>
                    </li>
                    <li>

                    <span style="margin-left:23px;">Zone :</span>

                    <?php
                    if ($search['search_zone'] == "") {
                        ?>

                        <select style="width:160px;" id="zones" name="zones">
                            <option value="">-- Select Zone --</option>
                            <?php
                            if (count($zones) > 0) {
                                foreach ($zones as $zones_val) {
                                    ?>
                                    <option value="<?php echo $zones_val['zone_id']; ?>"><?php echo $zones_val['zone_name']; ?></option>

                                    <?php
                                }
                            }
                            ?>
                        </select>

                        <?Php
                    } else {
                        ?>
                        <select style=" width:160px;" id="zones" name="zones">
                            <option value="">-- Select Zone --</option>
                            <?php
                            if (count($zones) > 0) {
                                foreach ($zones as $zones_val) {
                                    ?>
                                    <option value="<?php echo $zones_val['zone_id']; ?>" <?php echo isset($search['search_zone']) ? ($search['search_zone'] == $zones_val['zone_id'] ? 'selected="selected"' : '') : '' ?> ><?php echo $zones_val['zone_name']; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                        <?Php
                    }
                    ?>
                    </li>
                    
                    <li class="mr-2">


                    <span style="margin-left:15px;"></span>
                    <input type="hidden" name="day" id="day" value="<?php echo $search['search_day'] ?>">
                    <input type="hidden" name="zone_name" id="zone_name" value="<?php echo $search['search_zone_name'] ?>">
                    </li>
                    <li>

                    <input type="submit" class="n-btn" value="Go" name="payment_report">
                    
                    </li>
                    
                   <li class="mr-0 float-right">
                    
                    <div class="topiconnew border-0 green-btn">
                       <a id="printButn" title="Print"> <i class="fa fa-download"></i></a>
                    </div> 
                    
                    
                    </li>
                    </ul>
                    
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px;"> Sl.NO</th>
                            <th style="line-height: 18px;"> Zone Name</th>
                            <th style="line-height: 18px;"> Customer Name</th>
                            <th style="line-height: 18px;">Amount</th>
                            <th style="line-height: 18px;"> Paid Date Time</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                       <?php 
                        if ($payment_report != NULL) {
                        $i = 0;   $total =0; 
                            foreach ($payment_report as $payment){
                                //Payment Type
                                if($payment['payment_type'] == "D")
                                {
                                    $paytype = "(D)";
                                } else if($payment['payment_type'] == "W")
                                {
                                    $paytype = "(W)";
                                } else if($payment['payment_type'] == "M")
                                {
                                    $paytype = "(M)";
                                } else
                                {
                                    $paytype = "";
                                }
                            $total += $payment['paid_amount'];
                           ?>
                        
                        <tr>
                            <td style="line-height: 18px;"><?php echo ++$i; ?></td>
                            <td style="line-height: 18px;"><?php echo $payment['zone_name']; ?></td>
                            <td style="line-height: 18px;"><?php echo $payment['customer']; ?> <?php echo $paytype; ?></td>
                            <td style="line-height: 18px;"><?php echo number_format($payment['paid_amount'],2); ?></td>
                            <td style="line-height: 18px;"><?php echo $payment['paid_datetime']; ?></td>

                            </tr>
                            
                            <?php
                            }?>
                            <tr>
                            <td style="line-height: 18px;"></td>
                            <td style="line-height: 18px;"></td>
                            <td style="line-height: 18px;">Total :</td>
                            <td style="line-height: 18px;"><?php echo number_format($total,2); ?></td>
                            <td style="line-height: 18px;"></td>
                            </tr>
                            <?php
                        }
                            ?>
                         
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>


<div id="divForPrint" style="display: none">
    <div class="widget-content" style="margin-bottom:30px">
        <table border="1" width="100%" cellspacing="0" cellpadding = "0">
              <thead>
                        <tr>
                            <th style="line-height: 18px;"> Sl.NO</th>
                            <th style="line-height: 18px;"> Zone Name</th>
                            <th style="line-height: 18px;"> Customer Name</th>
                            <th style="line-height: 18px;">Amount</th>
                            <th style="line-height: 18px;"> Paid Date Time</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                       <?php 
                        if ($payment_report != NULL) {
                        $i = 0;   $total =0; 
                            foreach ($payment_report as $payment){
                                //Payment Type
                                if($payment['payment_type'] == "D")
                                {
                                    $paytype = "(D)";
                                } else if($payment['payment_type'] == "W")
                                {
                                    $paytype = "(W)";
                                } else if($payment['payment_type'] == "M")
                                {
                                    $paytype = "(M)";
                                } else
                                {
                                    $paytype = "";
                                }
                            $total += $payment['paid_amount'];
                           ?>
                        
                        <tr>
                            <td style="line-height: 18px;"><?php echo ++$i; ?></td>
                            <td style="line-height: 18px;"></td>
                            <td style="line-height: 18px;"><?php echo $payment['customer']; ?> <?php echo $paytype; ?></td>
                            <td style="line-height: 18px;"><?php echo number_format($payment['paid_amount'],2); ?></td>
                            <td style="line-height: 18px;"><?php echo $payment['paid_datetime']; ?></td>
                            </tr>
                            
                            <?php
                            }?>
                            <tr>
                            <td style="line-height: 18px;"></td>
                            <td style="line-height: 18px;"></td>
                            <td style="line-height: 18px;">Total :</td>
                            <td style="line-height: 18px;"><?php echo number_format($total,2); ?></td>
                            <td style="line-height: 18px;"></td>
                            </tr>
                            <?php
                        }
                            ?>
                         
                    </tbody>
                    
        </table>
    </div><!-- /widget-content --> 
</div>