<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
	.container.booking-fl-box{width: 100% !important;}
</style>
<div id="delete-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Delete ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to delete this payment ?</h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0" data-dismiss="modal" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" data-dismiss="modal" onclick="confirm_delete()">Delete</button>
    </div>
  </div>
</div>
<div class="main-inner">   
    <div class="span12" style="width: 97% !important;">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form id="edit-profile" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <i class="icon-th-list"></i>
                    <h3>Customer Payments </h3>                   
                    <input autocomplete="off" type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="vehicle_date" name="vehicle_date" placeholder="From Date" value="<?php if($search_date_from != NULL){ echo $search_date_from; } ?>">
					<input autocomplete="off" type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="vehicle_date_to" name="vehicle_date_to" placeholder="To Date" value="<?php if($search_date_to != NULL){ echo $search_date_to; } ?>">
					
                    <select class="sel2" style="margin-top: 5px; width:160px; margin-bottom: 9px;" id="customers_vh_rep_new" name="customers_vh_rep">
						<option value="0">-- Select Customer --</option>
						<?php
						foreach($customerlist as $c_val)
						{
							if($c_val->customer_id == $customer_id)
							{
								$selected = 'selected="selected"';
							} else {
								$selected = '';
							}
						?>
						<option value="<?php echo $c_val->customer_id; ?>" <?php echo $selected; ?>><?php echo $c_val->customer_name; ?></option>
						<?php
						}
						?>  
                    </select>
					<!--<select class="sel2" style="margin-top: 5px; width:160px; margin-bottom: 9px;" id="company_vh_rep_new" name="company_vh_rep">
						<option value="">-- Select Company --</option>
						<?php
						// $company_list=['Justmop','Matic','ServiceMarket','Helpling','Rizek','Ifsg','Urban Clap'];
						// foreach($company_list as $c_val)
						// {
							// if($c_val == $company_vh_rep)
							// {
								// $selected = 'selected="selected"';
							// } else {
								// $selected = '';
							// }
						?>
						<option value="<?php// echo $c_val; ?>" <?php// echo $selected; ?>><?php// echo $c_val ?></option>
						<?php
						//}
						?>  
                    </select>-->
					<select class="sel2" style="margin-top: 5px; width:160px; margin-bottom: 9px;" id="company_vh_rep_new" name="company_vh_rep">
						<option value="">-- Select Status --</option>
						<option value="D" <?php if($company_vh_rep == 'D'){ echo 'selected'; } ?>>Draft</option>
						<option value="P" <?php if($company_vh_rep == 'P'){ echo 'selected'; } ?>>Posted</option>
                    </select>
					<select style="margin-top: 5px; width:160px; margin-bottom: 9px;" id="type_vh_rep" name="type_vh_rep">
						<option value="">-- Select Type --</option>
						<?php
						$type=[['id'=>'22','label'=>'Cash'],['id'=>'1','label'=>'Card'],['id'=>'2','label'=>'Cheque'],['id'=>'3','label'=>'Bank'],['id'=>'4','label'=>'Credit Card']];
						foreach($type as $c_val)
						{
							if($c_val['id'] === $type_vh_rep)
							{
								$selected = 'selected="selected"';
							} else {
								$selected = '';
							}
						?>
						<option value="<?php echo $c_val['id']; ?>" <?php echo $selected; ?>><?php echo $c_val['label']; ?></option>
						<?php
						}
						?>  
                    </select>
                    <input type="submit" class="btn" value="Go" name="vehicle_report" style="margin-bottom: 4px;">
					<a style="float:right ; margin-right:15px; cursor:pointer;" href="<?php echo base_url(); ?>customer/add_payment">
						<img src="<?php echo base_url(); ?>img/add.png" title="Add Payment">
					</a>
					<div class="topiconnew">
					<a href="<?php echo base_url(); ?>reports/customerpaymentreporttoExcel/<?=$search_date_from!=''?implode('-',explode('/',$search_date_from)):'nil'?>/<?=$search_date_to!=''?implode('-',explode('/',$search_date_to)):'nil'?>/<?=$customer_id!=''?$customer_id:'nil'?>/<?=$company_vh_rep!=''?$company_vh_rep:'nil'?>/<?=$type_vh_rep!=''?$type_vh_rep:'nil'?>">
						<img src="<?php echo base_url(); ?>images/excel-icon.png" title="Download to Excel">
					</a>
				</div>
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table table-hover" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Sl. No</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Date</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Customer</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Name</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Memo</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Type</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Amount</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Paid Amount</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Balance Amount</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Payment From</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Status</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Action</th>
                        </tr>
                    </thead>
                    <tbody>
					<?php
					$sub_t = 0;
					$i = 1;
                    foreach ($payment_report as $pay){
						$sub_t += round($pay->paid_amount,2);
					?>
						<tr>
							<td style="line-height: 18px;"><?php echo $i; ?> </td>
							<td style="line-height: 18px;"><?php echo date('d/m/Y',strtotime($pay->paid_datetime)); ?> </td>
							<td style="line-height: 18px;"><?php echo $pay->customer_name; ?></td>
							<td style="line-height: 18px;">
								<?php
								if($pay->payment_status == 0)
								{
									echo "Draft Payment";
								} else {
									echo $pay->payment_reference;
								}
								if($pay->payment_type == "CM")
								{
									echo "<br/>(Credit Memo)";
								}
								?>
							</td>
							<td style="line-height: 18px;"><?php echo $pay->ps_no; ?></td>
							<td style="line-height: 18px;">
								<?php
								if($pay->payment_method == 0)
								{
									echo "Cash";
								} else if($pay->payment_method == 1){
									echo "Card";
								}
								else if($pay->payment_method == 2){
									echo "Cheque";
								}
								else if($pay->payment_method == 3){
									echo "Bank";
								}
								else if($pay->payment_method == 4){
									echo "Credit Card";
								}
								?>
							</td>
							<td style="line-height: 18px;"><?php echo $pay->paid_amount; ?></td>
							<td style="line-height: 18px;"><?php echo $pay->allocated_amount; ?></td>
							<td style="line-height: 18px;"><?php echo $pay->balance_amount; ?></td>
							<td style="line-height: 18px;">
								<?php
								if($pay->paid_at == 'O')
								{
									echo "Online";
								} else {
									echo "Admin";
								}
								?>
							</td>
							<td style="line-height: 18px;">
								<?php
								if($pay->payment_status == 0)
								{
									echo "Draft";
								} else {
									echo "Posted";
								}
								?>
							</td>
							<td style="line-height: 18px">
								<?php if($pay->verified_status == 1){?>
								<a class="btn btn-small btn-info" href="<?php echo base_url('customer/view_payment/'.$pay->payment_id); ?>" title="View Payment">
									<i class="btn-icon-only fa fa-eye "> </i>
								</a>
								<?php }else{?>
									Payment not<br/>verified
								<?php }?>
								<?php if($pay->paid_at == 'P' && $pay->payment_status == 0 && $pay->payment_type != 'CM'): ?>
								<a class="btn btn-small btn-info" href="<?php echo base_url('customer/edit_payment/'.$pay->payment_id); ?>" title="Edit Payment">
									<i class="btn-icon-only fa fa-pencil "> </i>
								</a>
								<?php endif; ?>
								<?php if($pay->paid_at == 'P' && $pay->payment_status == 0 && $pay->payment_type != 'CM'): ?>
								<a class="btn btn-small btn-info delete-payment" data-id="<?= $pay->payment_id ?>" title="Delete Payment">
									<i class="btn-icon-only fa fa-trash "> </i>
								</a>
								<?php endif; ?>
							</td>
						</tr>
					<?php
					$i++;	
					}
					?>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>Total</td>
							<td><?php echo $sub_t; ?></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>
<script>
var delete_payment_id;
function closeFancy() {
	$.fancybox.close();
}
[...document.querySelectorAll('.delete-payment')].forEach(function(item) {
	item.addEventListener('click', function() {
		delete_payment_id = this.getAttribute('data-id');
		fancybox_show('delete-popup');
	});
});
function confirm_delete() {
	$.ajax({
		type: "POST",
		url: _base_url + "customer/payment/update",
		data: {
			action: 'delete-customer-payment',
            id: delete_payment_id
		},
		dataType: "json",
		cache: false,
		success: function(data) {
            if(data.status == true){
                toast(data.type,data.message);
				setTimeout(function(){location.reload()},1500);
            }
            else{
                toast(data.type,data.message);
            }
		},
		error: function() {
             toast('error',"An error occured !");
		},
        complete: function() {
            closeFancy();
		}
	});
}
</script>
