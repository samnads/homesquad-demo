<style>
ul li{margin-left: 0;}
input.span3, textarea.span3, .uneditable-input.span3{width: 270px;}
</style>
<div class="form-row">
    <div class="span7">      		
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-money"></i>
                <h3>Add Payment</h3>
                <a style="float:right ; margin-right:20px; cursor:pointer; text-decoration:none;" href="<?php echo base_url(); ?>customer/list_customer_payments"><i class="icon-th-list"></i></a>
            </div> <!-- /widget-header -->

            <div class="widget-content">



                <div class="tabbable">


                    <div class="tab-content">

                        <form id="edit-profile" class="form-horizontal" method="post">
                            <div class="alert alert-<?php echo $errors['class']; ?>" style="display: <?php echo !empty($errors) ? 'block' : 'none'?>">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <?php echo $errors['message']; ?>
                            </div>

                           <div class="control-group">											
                                <label class="control-label" for="customer">Customer</label>
                                <div class="controls">                                    
                                    <select class="span3 sel2" name="customer_id" id="pcustomer-id" data-placeholder="Select customer" required="required">
                                        <option></option>
                                        <?php
                                        foreach ($customers as $customer)
                                        {
                                            echo '<option value="' . $customer->customer_id . '">' . $customer->customer_name . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div> <!-- /controls -->				
                            </div> <!-- /control-group -->
							
							<div class="control-group" id="req-balance-amount" style="display: none;">
                                <label class="control-label" for="requested-amount">Outstanding Amount</label>
                                <div class="controls">
                                    <input type="text" class="span3 disabled" id="requested-amount"
                                        name="requested_amount" value="" readonly="readonly">
                                </div> <!-- /controls -->
                            </div>
							
							<div class="control-group">											
                                <label class="control-label" for="paymentcategory">Category</label>
                                <div class="controls">                                    
                                    <select class="span3 sel2" name="paymentcategory" id="paymentcategory" data-placeholder="Select Category" required="required">
                                        <option></option>
                                        <option value="P">Payment</option>
                                        <option value="CM">Credit Memo</option>
                                    </select>
                                </div> <!-- /controls -->				
                            </div>
                            
                            <div class="control-group">											
                                <label class="control-label" for="collected-amount">Collected Amount</label>
                                <div class="controls">
                                    <input type="text" class="span3" id="collected-amount" name="collected_amount" required="required">
                                    <input type="hidden" id="quickbook_id" name="quickbook_id" value="" />
                                </div> <!-- /controls -->				
                            </div> <!-- /control-group -->
                             <div class="control-group">											
                                <label class="control-label" for="collected-amount">Collection Date</label>
                                <div class="controls">
                                    <input type="text" class="span3" id="collect_date" readonly="readonly" name="collect_date" value="<?php echo $collected_date; ?>" required="required">
                                </div> <!-- /controls -->				
                            </div> <!-- /control-group -->
							
							<div class="control-group">											
                                <label class="control-label" for="paymenttype">Payment Method</label>
                                <div class="controls">                                    
                                    <select class="span3 sel2" name="paymenttype" id="paymenttype" data-placeholder="Select payment type" required="required">
                                        <option></option>
                                        <option value="0">Cash</option>
                                        <option value="1">Card</option>
                                        <option value="2">Cheque</option>
                                        <option value="3">Bank</option>
                                        <option value="4">Credit Card</option>
                                    </select>
                                </div> <!-- /controls -->				
                            </div>
							
							<div class="control-group">											
                                <label class="control-label" for="memo">Memo</label>
                                <div class="controls">
                                    <input type="text" class="span3" id="memo" name="memo">
                                </div> <!-- /controls -->				
                            </div>
							
							<div class="control-group">											
                                <label class="control-label" for="description">Description</label>
                                <div class="controls">
                                    <textarea class="span3" id="description" name="description"></textarea>
                                </div> <!-- /controls -->				
                            </div>
                            
                            <br />


                            <div class="form-actions">
                                <button type="submit" name="add_payment" value="1" class="btn mm-btn">Save</button> 
                                <a class="btn" href="<?php echo base_url(); ?>customer/list_customer_payments">Cancel</a>
                            </div> <!-- /form-actions -->

                        </form>




                    </div>


                </div>





            </div>				
        </div> <!-- /widget -->	      		
    </div> <!-- /span8 -->	      		      		      		      	
</div> <!-- /row -->
<script>
var status = '<?php echo $quickbookmessage["status"]; ?>';
var messagess = '<?php echo $quickbookmessage["message"]; ?>';
if(status !="")
{
	if(status =="success")
	{
		toastr.success(messagess)
	} else {
		toastr.error(messagess)
	}
}
</script>