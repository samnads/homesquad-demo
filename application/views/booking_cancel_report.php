<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
	td {
		padding: 10px;
	}
</style>
<div class="row m-0">   
    <div class="col-md-12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form id="edit-profile" class="form-horizontal" method="post" >
                    
                    <div class="book-nav-top">
            <ul>
              <li><i class="icon-th-list"></i><h3>Booking Cancel Report</h3> </li>
              
              <li>
                  <input type="text" readonly="readonly" style="width: 160px;" id="OneDayDate" name="search_date" value="<?php echo isset($search_date) ? $search_date : date('d/m/Y'); ?>">
              </li>
              <li class="mr-2">
                  <input type="text" readonly="readonly" style="width: 160px;" id="search_date_to" name="search_date_to" value="<?php echo isset($search_date_to) ? $search_date_to : date('d/m/Y'); ?>">
              </li>
      
              <li>
                  <input type="submit" class="n-btn" value="Go" name="ondeday_report">
              </li>
    
              <li class="mr-0 float-right">
                  <div class="topiconnew border-0 green-btn">
                    	<a href="#" id="OneDayPrint" title="Print"> <i class="fa fa-print"></i></a>
                    </div>
                    
                    <!--<div class="topiconnew border-0 green-btn">
                    	<a href="<?php echo base_url(); ?>reports/booking_cancel_excel/<?php echo $search_from_date; ?>/<?php echo $search_to_date; ?>" title="Download to Excel"> <i class="fa fa-download"></i></a>
                    </div>-->
              </li>
              <li class="mr-0 float-right">
                    <div class="topiconnew border-0 green-btn">
                        <a data-action="excel-export" title="Download as Excel"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
                    </div>
                    </li>     
              <div class="clear"></div>
            </ul>
     </div>
                </form>   
            </div>
            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px;">Sl No</th>
                            <th style="line-height: 18px">Customer Name</th>
                            <th style="line-height: 18px">Maid Name</th>
                            <th style="line-height: 18px">Shift</th>
                            <th style="line-height: 18px">Booking Type</th>
                            <th style="line-height: 18px">Canceled User</th>
                            <th style="line-height: 18px">Canceled Date</th>
                            <th style="line-height: 18px">Remarks</th>
                            <th style="line-height: 18px">Booking Date</th>
                            <th style="line-height: 18px">Status</th>
                           
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($reports)) {
                            $i = 0;
                            foreach ($reports as $report) {
                                //Payment Type
                                if($report->payment_type == "D")
                                {
                                    $paytype = "(D)";
                                } else if($report->payment_type == "W")
                                {
                                    $paytype = "(W)";
                                } else if($report->payment_type == "M")
                                {
                                    $paytype = "(M)";
                                } else
                                {
                                    $paytype = "";
                                }
                                $i++;
                                ?>
                                <tr>
                                    <td style="line-height: 18px;">
                                        <?php echo $i; ?>
                                    </td>
                                    <td style="line-height: 18px">
                                        <?php echo $report->customer_name; ?> <?php echo $paytype; ?>
                                    </td>
                                    <td style="line-height: 18px">
                                        <?php echo $report->maid_name; ?>
                                    </td>
                                    <td style="line-height: 18px">
                                        <?php echo $report->shift; ?>
                                    </td>
									<td style="line-height: 18px">
                                        <?php
										if($report->booking_type == "OD")
										{
											$type = "One Day";
										} else if($report->booking_type == "WE")
										{
											$type = "Weekly";
										} else if($report->booking_type == "BW")
										{
											$type = "Bi-Weekly";
										}
										echo $type;
										?>
                                    </td>
                                    <td style="line-height: 18px">
                                        <?php echo $report->user_fullname; ?>
                                    </td>
									<td style="line-height: 18px">
                                        <?php echo date('d-m-Y h:i A',strtotime($report->added)); ?>
                                    </td>
                                    <td style="line-height: 18px">
                                        <?php echo $report->remarks; ?>
                                    </td>
                                    <td style="line-height: 18px">
                                        <?php echo date('d-m-Y',strtotime($report->booked_datetime)); ?>
                                    </td>
									<td style="line-height: 18px">
										<?php
										$getstatus = $this->reports_model->getservicestatus($report->booking_id,$report->service_date);
										if(count($getstatus) == 0)
										{
											echo "Not Started";
										} else {
											if($getstatus->service_status == 1)
											{
												echo "Started";
											} else if($getstatus->service_status == 2){
												echo "Finished";
											} else if($getstatus->service_status == 3){
												echo "Cancelled";
											}
										}
                                        
										?>
                                    
                                    </td>
                                    
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>
<div style="display: none;" id="OneDayReportPrint"> 
    <table border="1" cellpadding="0" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th style="line-height: 18px;">Sl No</th>
                <th style="line-height: 18px">Customer Name</th>
                <th style="line-height: 18px">Maid Name</th>
                <th style="line-height: 18px">Shift</th>
                <th style="line-height: 18px">Booking Type</th>
                <th style="line-height: 18px">Canceled User</th>
				<th style="line-height: 18px">Canceled Date</th>
				<th style="line-height: 18px">Remarks</th>
                <th style="line-height: 18px">Booking Date</th>
				<th style="line-height: 18px">Status</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (!empty($reports)) {
                $i = 0;
                foreach ($reports as $report) {
                    //Payment Type
                    if($report->payment_type == "D")
                    {
                        $paytype = "(D)";
                    } else if($report->payment_type == "W")
                    {
                        $paytype = "(W)";
                    } else if($report->payment_type == "M")
                    {
                        $paytype = "(M)";
                    } else
                    {
                        $paytype = "";
                    }
                    $i++;
                    ?>
                    <tr>
                        <td style="line-height: 18px; padding: 5px 10px;">
                            <?php echo $i; ?>
                        </td>
                        <td style="line-height: 18px; padding: 5px 10px;">
                            <?php echo $report->customer_name; ?> <?php echo $paytype; ?>
                        </td>
                        <td style="line-height: 18px; padding: 5px 10px;">
                            <?php echo $report->maid_name; ?>
                        </td>
                        <td style="line-height: 18px; padding: 5px 10px;">
                            <?php echo $report->shift; ?>
                        </td>
						<td style="line-height: 18px; padding: 5px 10px;">
							<?php
							if($report->booking_type == "OD")
							{
								$type = "One Day";
							} else if($report->booking_type == "WE")
							{
								$type = "Weekly";
							} else if($report->booking_type == "BW")
							{
								$type = "Bi-Weekly";
							}
							echo $type;
							?>
						</td>
                        <td style="line-height: 18px; padding: 5px 10px;">
                            <?php echo $report->user_fullname; ?>
                        </td>
						<td style="line-height: 18px; padding: 5px 10px;">
							<?php echo date('d-m-Y h:i A',strtotime($report->added)); ?>
						</td>
						<td style="line-height: 18px; padding: 5px 10px;">
							<?php echo $report->remarks; ?>
						</td>
                        <td style="line-height: 18px">
                                        <?php echo date('d-m-Y h:i A',strtotime($report->booked_datetime)); ?>
                                    </td>
						<td style="line-height: 18px">
							<?php
							$getstatus = $this->reports_model->getservicestatus($report->booking_id,$report->service_date);
							if(count($getstatus) == 0)
							{
								echo "Not Started";
							} else {
								if($getstatus->service_status == 1)
								{
									echo "Started";
								} else if($getstatus->service_status == 2){
									echo "Finished";
								} else if($getstatus->service_status == 3){
									echo "Cancelled";
								}
							}
							?>
						</td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
</div>
<script>
    var report_file_name = "Booking Cancel Report";
</script>


