<style>
    .book-nav-top li{
        margin: 0 10px 0 0 !important;
    }
    .select2-arrow{visibility : hidden;}
    .select2-container .select2-choice{
	-moz-appearance: none;
        background: #fff url("../images/ash-arrow.png") no-repeat scroll right 7px top 11px;
        border: 1px solid #ccc;
        border-radius: 3px;
        cursor: pointer;
        font-size: 12px;
        height: 30px;
        line-height: 24px;
        padding: 3px 0 3px 10px;
        text-indent: 0.01px;
    }
    .select2-results li{margin-left: 0px !important;}
    
    table thead tr th table th{
        text-transform: none;
        padding: 5px;
    }
    table td{
        border-right: 1px solid #000 !important;
        border-bottom: 1px solid #000 !important;
        padding: 5px;
    }
    .main-inner .container .dash-top-wrapper table colgroup + thead tr:first-child th, table colgroup + thead tr:first-child td, table thead:first-child tr:first-child th, table thead:first-child tr:first-child td{
        border-top: 1px solid #000 !important;
    }
    
    table tbody tr:first-child{
        border-left: 1px solid #000 !important;
    }
    
    table{font-size: 9px;}
    
    /*source color*/
.helpling {
	background:rgb(255, 192, 0);
}
.justmop {
	background:rgb(177, 160, 199);
}
.justmopod {
	background:rgb(230, 184, 183);
}
.helpersquad {
	background:rgb(255, 51, 153);
}
.bookservice {
	background:rgb(196, 189, 151);
}
.helpbit {
	background:rgb(0, 176, 240);
}
.mrusta {
	background:rgb(218, 150, 148);
}
.servicemarket {
	background:rgb(53, 95, 172);
}

.colorod{ background: #1ca99e;}
<!--.colorwe{ background: #33cce8;}-->
.colorwe{ background: #42dbe0;}
<!--.colorbw{ background: #604881;}-->
.colorbw{ background: #b39bd4;}

.specjohn {
	background:rgb(255,69,0);
}
.specian {
	background:rgb(222, 234, 26);
}
.specruth {
	background:rgb(199, 0, 57);
}
.specdom {
	background:rgb(105, 105, 105);
}
.speckc {
	background:rgb(237, 127, 34);
}
.specmark {
	background:rgb(103, 64, 140);
}.specayen {
	background:rgb(185, 35, 61);
}
.mildred {
	background:rgb(185, 119, 14);
}
.glory {
	background:rgb(65, 65, 65);
}

.rose {
	background:rgb(237, 127, 34);
}

.jane {
	background:rgb(103, 64, 140);
}



.spectrum {
	background:rgb(146, 208, 80);
}
.other {
    background:white;
}
/*    #invoice-exTab2 ul li { margin-left: 0px !important; }
    #invoice-exTab2 ul li a { color: black; }
    #exTab2 .dataTables_filter {
display: none;
} 
.book-nav-top li{
    margin-right: 10px;
}

.table-bordered thead:first-child tr:first-child th:first-child, .table-bordered tbody:first-child tr:first-child td:first-child {
    border-radius: 0;
}
.table-bordered thead:first-child tr:first-child th:last-child, .table-bordered tbody:first-child tr:first-child td:last-child {
    border-radius: 0;
}
.no-left-border { border-left: 0px  !important;}

.no-right-border { border-right: 0px  !important;}

.table-bordered { border-radius: 0; }*/
</style>
<section>
    <div class="row dash-top-wrapper m-0">
        <div class="col-md-12 col-sm-12 no-left-right-padding">
            <!--<div class="widget widget-table action-table" style="margin-bottom:30px">-->
            <div class="widget-header"> 
                <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="<?php echo base_url(); ?>reports/newreport">
                    <div class="book-nav-top">
                        <ul>
                            <li>
                                <i class="icon-th-list"></i>
                                <h3>Customer Statement</h3>
                            </li>
                            <li>
                                <input type="text" id="cstatemnet-date-from" name="cstatemnet-date-from" style="width: 160px;" data-date="<?php echo $search_date_from_statement; ?>" readonly value="<?php echo $search_date_from_statement; ?>" data-date-format="dd/mm/yyyy"/> 
                            </li>
							<li>
								<select style="width:160px;" id="zones" name="zones">
									<option value="">-- Select Zone --</option>
									<?php
									if (count($zones) > 0) {
										foreach ($zones as $zones_val) {
											?>
											
											<option value="<?php echo $zones_val['zone_id']; ?>" <?php echo isset($zone_id) ? ($zone_id == $zones_val['zone_id'] ? 'selected="selected"' : '') : '' ?> ><?php echo $zones_val['zone_name']; ?>(<?php echo $zones_val['driver_name']; ?>)</option>
											<?php
										}
									}
									?>
								</select>
							</li>
							<li>
								<select id="freemaids" name="freemaids" class="" style="width:160px;">
									<option value="0"<?php if ($freemaid_ids == '0') { echo ' selected="selected"'; } ?>>Select All</option>
									<option value="1"<?php if ($freemaid_ids == '1') { echo ' selected="selected"'; } ?>>Free Maids</option>
									<option value="2"<?php if ($freemaid_ids == '2') { echo ' selected="selected"'; } ?>>Morning Availability</option>
									<option value="3"<?php if ($freemaid_ids == '3') { echo ' selected="selected"'; } ?>>Evening Availability</option>
									<option value="4"<?php if ($freemaid_ids == '4') { echo ' selected="selected"'; } ?>>On Duty Maids</option>
									<option value="5"<?php if ($freemaid_ids == '5') { echo ' selected="selected"'; } ?>>Leave Maids</option>
									<option value="6"<?php if ($freemaid_ids == '6') { echo ' selected="selected"'; } ?>>Inactive Maids</option>
								</select>
							</li>
                            <li>
                                <input type="submit" id="customer-statement-search" value="Search" style="width:160px;" />
                            </li>

                            
                            
                            <li class="mr-0 float-right">
                                
                                <div class="topiconnew border-0 green-btn">
                                    <a href="<?php echo base_url(); ?>reports/newreporttoExcel/<?php echo $date_from_job; ?>/<?php echo $freemaid_ids; ?>/<?php echo $zone_id; ?>" title="Download to Excel"> <i class="fa fa-download"></i></a>
                                </div>
                                
                            </li>
                            <div class="clear"></div>
                            <!-- Statement ends -->
                        </ul>
                    </div>
                               
                </form>
            </div>
                    <!--<div class="widget-content" style="margin-bottom:30px">-->
            <div id="statement_content" class="" style=" height: 60vh; background: #FFF; overflow: scroll;">
                <table id="" class="" cellspacing="0" width="100%">
                    <thead style="font-family: Arial, Helvetica, sans-serif; font-size: 13px;">
                        <tr>
                            <td> <center>Sl.No</center></td>
                            <td> <center><b>Cleaner Name</b></center></td>
							<td><center><b>Work Hours</b></center></td>
                            <?php 
                            $i = 0;
                            for ($i=0; $i < 3; $i++)
                            {
                            ?>
									
                                    <td><b>Total Timings</b></td>
                                    <td><b>Client</b></td>
									<td><b>Reference</b></td>
                                    <td><b>Timing</b></td>
                                    <td><b>Location</b></td>
                                    <td><b>Mat</b></td>
                                    <td><b>Payment</b></td>
                                    <td><b>Payment mode</b></td>
                                    <td><b>Driver</b></td>
									<td><b>Notes</b></td>
                                    
                            
                            <?php
                            }
                            ?>
                        </tr>
                    </thead>
                    
                    <tbody  style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: 400 !important;">
                        <?php
                        $j = 1;
						if($zone_id == "")
						{
							foreach ($maids as $maids_val) {
							?>
							<tr>
								<td><?php echo $j; ?></td>
								<td><b><?php echo $maids_val->maid_name; ?></b></td>
								<?php
								$veh1 = $this->reports_model->getbookingsbymaid_id_new($date_from_job,$maids_val->maid_id,$zone_id);
								?>
								<td><b><?php echo $veh1[0]->total_hrs; ?></b></td>
								<?php
								//$getbookingsbymaid_id = $this->reports_model->getbookingsbymaid_id(date('Y-m-d'),$maids_val['maid_id']);
								$veh = $this->reports_model->getbookingsbymaid_id($date_from_job,$maids_val->maid_id,$zone_id);
								if(count($veh) == 1)
								{
									if($veh[0]['booking_type'] == "OD")
									{
										$zonecolor = "colorod";
									} else if($veh[0]['booking_type'] == "WE"){
										$zonecolor = "colorwe";
									} else {
										$zonecolor = "colorbw";
									}
									
									//Payment Type
									if($veh[0]['payment_type'] == "D")
									{
										$paytype = "(D)";
									} else if($veh[0]['payment_type'] == "W")
									{
										$paytype = "(W)";
									} else if($veh[0]['payment_type'] == "M")
									{
										$paytype = "(M)";
									} else
									{
										$paytype = "";
									}
									$t_shrt = $veh[0]['time_to']; 
									$cur_shrt = $veh[0]['time_from'];
									$hours = ((strtotime($t_shrt) - strtotime($cur_shrt))/3600);
									
									if($veh[0]['cleaning_material'] == 'Y')
									{
										$mat = "Yes";
									} else {
										$mat = "No";
									}
									
									$check_buk_exist = $this->bookings_model->get_booking_exist($veh[0]['booking_id'],$date_from_job);
									if(!empty($check_buk_exist))
									{
										$mop = $check_buk_exist->mop;
										$ref = $check_buk_exist->just_mop_ref;
									} else {
										if($veh[0]['pay_by'] != "")
										{
											$mop = $veh[0]['pay_by'];
											$ref = $veh[0]['justmop_reference'];
										} else {
											$mop = $veh[0]['payment_mode'];
											$ref = $veh[0]['justmop_reference'];
										}
									}
									
									//if(($veh[0]['time_from'] >= '05:00:00') && ($veh[0]['time_from'] <= '12:00:00'))
									if($veh[0]['time_from'] >= '12:00:00')
									{
										?>
										<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								
								<td class="<?php echo $zonecolor; ?>"><?php echo $hours; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[0]['customer_name']; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $ref; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo date("g:i", strtotime($veh[0]['time_from']));?>-
										<?php echo date("g:i", strtotime($veh[0]['time_to']));?> </td>

								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[0]['area_name']; ?>/<?php echo '<br/>'; ?><?php echo $veh[0]['customer_address'] ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $mat; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[0]['total_amount']; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $mop; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[0]['driver_name'] ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[0]['booking_note'] ?></td>
										
								
										
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
										<?php
									} else {
										$i = 0;
										for ($i=0; $i < 3; $i++)
										{
											if(!empty($veh[$i]))
											{
												if($veh[$i]['booking_type'] == "OD")
												{
													$zonecolor = "colorod";
												} else if($veh[$i]['booking_type'] == "WE"){
													$zonecolor = "colorwe";
												} else {
													$zonecolor = "colorbw";
												}
												
												//Payment Type
												if($veh[$i]['payment_type'] == "D")
												{
													$paytype = "(D)";
												} else if($veh[$i]['payment_type'] == "W")
												{
													$paytype = "(W)";
												} else if($veh[$i]['payment_type'] == "M")
												{
													$paytype = "(M)";
												} else
												{
													$paytype = "";
												}
												
												$t_shrt = $veh[$i]['time_to']; 
												$cur_shrt = $veh[$i]['time_from'];
												$hours = ((strtotime($t_shrt) - strtotime($cur_shrt))/3600);
												
												if($veh[$i]['cleaning_material'] == 'Y')
												{
													$mat = "Yes";
												} else {
													$mat = "No";
												}
												
												$check_buk_exist = $this->bookings_model->get_booking_exist($veh[$i]['booking_id'],$date_from_job);
												if(!empty($check_buk_exist))
												{
													$mop = $check_buk_exist->mop;
													$ref = $check_buk_exist->just_mop_ref;
												} else {
													if($veh[$i]['pay_by'] != "")
													{
														$mop = $veh[$i]['pay_by'];
														$ref = $veh[$i]['justmop_reference'];
													} else {
														$mop = $veh[$i]['payment_mode'];
														$ref = $veh[$i]['justmop_reference'];
													}
												}
									
												?>
								<td class="<?php echo $zonecolor; ?>"><?php echo $hours; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['customer_name']; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $ref; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo date("g:i", strtotime($veh[$i]['time_from']));?>-
										<?php echo date("g:i", strtotime($veh[$i]['time_to']));?> </td>

								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['area_name']; ?>/<?php echo '<br/>'; ?><?php echo $veh[$i]['customer_address'] ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $mat; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['total_amount']; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $mop; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['driver_name'] ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['booking_note'] ?></td>
												<?php
											} else { ?>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td> 
								<td class="other"></td> 
										<?php }
										}
									}
								} else {
									$i = 0;
									for ($i=0; $i < 3; $i++)
									{
										if(!empty($veh[$i]))
										{
											if($veh[$i]['booking_type'] == "OD")
										{
											$zonecolor = "colorod";
										} else if($veh[$i]['booking_type'] == "WE"){
											$zonecolor = "colorwe";
										} else {
											$zonecolor = "colorbw";
										}
											//Payment Type
											if($veh[$i]['payment_type'] == "D")
											{
												$paytype = "(D)";
											} else if($veh[$i]['payment_type'] == "W")
											{
												$paytype = "(W)";
											} else if($veh[$i]['payment_type'] == "M")
											{
												$paytype = "(M)";
											} else
											{
												$paytype = "";
											}
												
											$t_shrt = $veh[$i]['time_to']; 
											$cur_shrt = $veh[$i]['time_from'];
											$hours = ((strtotime($t_shrt) - strtotime($cur_shrt))/3600);
											
											if($veh[$i]['cleaning_material'] == 'Y')
											{
												$mat = "Yes";
											} else {
												$mat = "No";
											}
											
											$check_buk_exist = $this->bookings_model->get_booking_exist($veh[$i]['booking_id'],$date_from_job);
											if(!empty($check_buk_exist))
											{
												$mop = $check_buk_exist->mop;
												$ref = $check_buk_exist->just_mop_ref;
											} else {
												if($veh[$i]['pay_by'] != "")
												{
													$mop = $veh[$i]['pay_by'];
													$ref = $veh[$i]['justmop_reference'];
												} else {
													$mop = $veh[$i]['payment_mode'];
													$ref = $veh[$i]['justmop_reference'];
												}
											}
										?>
								<td class="<?php echo $zonecolor; ?>"><?php echo $hours; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['customer_name']; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $ref; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo date("g:i", strtotime($veh[$i]['time_from']));?>-
										<?php echo date("g:i", strtotime($veh[$i]['time_to']));?> </td>

								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['area_name']; ?>/<?php echo '<br/>'; ?><?php echo $veh[$i]['customer_address'] ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $mat; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['total_amount']; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $mop; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['driver_name'] ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['booking_note'] ?></td>
												<?php
										} else { ?>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td> 
								<td class="other"></td>
										<?php
										}
									}
								}
							$j++;		
							}
							?>
							</tr>
							<?php
							
						} else {
						foreach ($maids as $maids_val) {
								$veh = $this->reports_model->getbookingsbymaid_id($date_from_job,$maids_val->maid_id,$zone_id);
								if(count($veh) > 0)
								{
							?>
						<tr>
                            <td><?php echo $j; ?></td>
                            <td><b><?php echo $maids_val->maid_name; ?></b></td>
							<?php
								$veh1 = $this->reports_model->getbookingsbymaid_id_new($date_from_job,$maids_val->maid_id,$zone_id);
								?>
								<td><b><?php echo $veh1[0]->total_hrs; ?></b></td>
                            <?php
                            if(count($veh) == 1)
								{
									if($veh[0]['booking_type'] == "OD")
									{
										$zonecolor = "colorod";
									} else if($veh[0]['booking_type'] == "WE"){
										$zonecolor = "colorwe";
									} else {
										$zonecolor = "colorbw";
									}
									//Payment Type
									if($veh[0]['payment_type'] == "D")
									{
										$paytype = "(D)";
									} else if($veh[0]['payment_type'] == "W")
									{
										$paytype = "(W)";
									} else if($veh[0]['payment_type'] == "M")
									{
										$paytype = "(M)";
									} else
									{
										$paytype = "";
									}
									$t_shrt = $veh[0]['time_to']; 
									$cur_shrt = $veh[0]['time_from'];
									$hours = ((strtotime($t_shrt) - strtotime($cur_shrt))/3600);
									
									if($veh[0]['cleaning_material'] == 'Y')
									{
										$mat = "Yes";
									} else {
										$mat = "No";
									}
									$check_buk_exist = $this->bookings_model->get_booking_exist($veh[0]['booking_id'],$date_from_job);
									if(!empty($check_buk_exist))
									{
										$mop = $check_buk_exist->mop;
										$ref = $check_buk_exist->just_mop_ref;
									} else {
										if($veh[0]['pay_by'] != "")
										{
											$mop = $veh[0]['pay_by'];
											$ref = $veh[0]['justmop_reference'];
										} else {
											$mop = $veh[0]['payment_mode'];
											$ref = $veh[0]['justmop_reference'];
										}
									}
									//if(($veh[0]['time_from'] >= '05:00:00') && ($veh[0]['time_from'] <= '12:00:00'))
									if($veh[0]['time_from'] >= '12:00:00')
									{
										?>
										<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								
								<td class="<?php echo $zonecolor; ?>"><?php echo $hours; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[0]['customer_name']; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $ref; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo date("g:i", strtotime($veh[0]['time_from']));?>-
										<?php echo date("g:i", strtotime($veh[0]['time_to']));?> </td>

								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[0]['area_name']; ?>/<?php echo '<br/>'; ?><?php echo $veh[0]['customer_address'] ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $mat; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[0]['total_amount']; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $mop; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[0]['driver_name'] ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['booking_note'] ?></td>
										
								
										
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
										<?php
									} else {
										$i = 0;
										for ($i=0; $i < 3; $i++)
										{
											if(!empty($veh[$i]))
											{
												if($veh[$i]['booking_type'] == "OD")
												{
													$zonecolor = "colorod";
												} else if($veh[$i]['booking_type'] == "WE"){
													$zonecolor = "colorwe";
												} else {
													$zonecolor = "colorbw";
												}
												//Payment Type
												if($veh[$i]['payment_type'] == "D")
												{
													$paytype = "(D)";
												} else if($veh[$i]['payment_type'] == "W")
												{
													$paytype = "(W)";
												} else if($veh[$i]['payment_type'] == "M")
												{
													$paytype = "(M)";
												} else
												{
													$paytype = "";
												}
												
												$t_shrt = $veh[$i]['time_to']; 
												$cur_shrt = $veh[$i]['time_from'];
												$hours = ((strtotime($t_shrt) - strtotime($cur_shrt))/3600);
												
												if($veh[$i]['cleaning_material'] == 'Y')
												{
													$mat = "Yes";
												} else {
													$mat = "No";
												}
												
												$check_buk_exist = $this->bookings_model->get_booking_exist($veh[$i]['booking_id'],$date_from_job);
												if(!empty($check_buk_exist))
												{
													$mop = $check_buk_exist->mop;
													$ref = $check_buk_exist->just_mop_ref;
												} else {
													if($veh[$i]['pay_by'] != "")
													{
														$mop = $veh[$i]['pay_by'];
														$ref = $veh[$i]['justmop_reference'];
													} else {
														$mop = $veh[$i]['payment_mode'];
														$ref = $veh[$i]['justmop_reference'];
													}
												}
												?>
								<td class="<?php echo $zonecolor; ?>"><?php echo $hours; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['customer_name']; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $ref; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo date("g:i", strtotime($veh[$i]['time_from']));?>-
										<?php echo date("g:i", strtotime($veh[$i]['time_to']));?> </td>

								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['area_name']; ?>/<?php echo '<br/>'; ?><?php echo $veh[$i]['customer_address'] ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $mat; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['total_amount']; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $mop; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['driver_name'] ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['booking_note'] ?></td>
												<?php
											} else { ?>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td> 
								<td class="other"></td> 
										<?php }
										}
									}
								} else {
									$i = 0;
									for ($i=0; $i < 3; $i++)
									{
										if(!empty($veh[$i]))
										{
											if($veh[$i]['booking_type'] == "OD")
											{
												$zonecolor = "colorod";
											} else if($veh[$i]['booking_type'] == "WE"){
												$zonecolor = "colorwe";
											} else {
												$zonecolor = "colorbw";
											}
											//Payment Type
											if($veh[$i]['payment_type'] == "D")
											{
												$paytype = "(D)";
											} else if($veh[$i]['payment_type'] == "W")
											{
												$paytype = "(W)";
											} else if($veh[$i]['payment_type'] == "M")
											{
												$paytype = "(M)";
											} else
											{
												$paytype = "";
											}
												
											$t_shrt = $veh[$i]['time_to']; 
											$cur_shrt = $veh[$i]['time_from'];
											$hours = ((strtotime($t_shrt) - strtotime($cur_shrt))/3600);
											
											if($veh[$i]['cleaning_material'] == 'Y')
											{
												$mat = "Yes";
											} else {
												$mat = "No";
											}
											
											$check_buk_exist = $this->bookings_model->get_booking_exist($veh[$i]['booking_id'],$date_from_job);
											if(!empty($check_buk_exist))
											{
												$mop = $check_buk_exist->mop;
												$ref = $check_buk_exist->just_mop_ref;
											} else {
												if($veh[$i]['pay_by'] != "")
												{
													$mop = $veh[$i]['pay_by'];
													$ref = $veh[$i]['justmop_reference'];
												} else {
													$mop = $veh[$i]['payment_mode'];
													$ref = $veh[$i]['justmop_reference'];
												}
											}
										?>
								<td class="<?php echo $zonecolor; ?>"><?php echo $hours; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['customer_name']; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $ref; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo date("g:i", strtotime($veh[$i]['time_from']));?>-
										<?php echo date("g:i", strtotime($veh[$i]['time_to']));?> </td>

								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['area_name']; ?>/<?php echo '<br/>'; ?><?php echo $veh[$i]['customer_address'] ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $mat; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['total_amount']; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $mop; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['driver_name'] ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['booking_note'] ?></td>
												<?php
										} else { ?>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td> 
								<td class="other"></td> 
										<?php
										}
									}
								}
							
							?>
                        </tr>
                        <?php 
						 }
							$j++;
						 } } ?>
                        
                    </tbody>
                </table> 
            </div>
        </div><!--welcome-text-main end-->
    </div><!--row content-wrapper end--> 
</section><!--welcome-text end-->