<div class="row">
    <!-- /span6 --> 
    <div class="span6" id="add_user" style="float: none; margin: 0 auto;">      		
	<div class="widget ">
	    
         <!-- /widget-header -->
            
            
            
            <div class="widget-header">
     <div class="book-nav-top">
            <ul>
              <li><i class="icon-th-list"></i><h3>Add Coupon</h3></li>
              




      
              <li class="mr-0 float-right">
                  <div class="topiconnew border-0 green-btn">
                    	<a href="<?php echo base_url(); ?>coupons" title=""> <i class="fa fa-users"></i></a>
                    </div>
              </li>
      
      
      
              <div class="clear"></div>
            </ul>
     </div>
</div>


	
            <div class="widget-content">
		<div class="tabbable">
                    <div class="tab-content">
                        <form id="useraadd" action="<?php echo base_url(); ?>coupons/add" class="form-horizontal" method="post">
                            <fieldset>
                                    <div class="control-group">
                                        <div class="error" style="text-align: center;"><?php echo isset($error) && strlen(trim($error)) > 0 ? $error : '&nbsp;'; ?></div>
                                    </div>
                                    <div class="control-group">											
					<label class="control-label" for="couponname">Coupon Name</label>
                                            <div class="controls">
                                                <input type="text" class="span6" style="width: 300px;" id="couponname" name="couponname" value="">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
										
                                    <div class="control-group">											
					<label class="control-label" for="expirydate">Expiry Date</label>
                                            <div class="controls">
                                                <input type="text" class="span6" style="width: 300px;" id="expirydate" readonly="readonly" name="expirydate" value="">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
									<!--<div class="control-group">											
											<label class="control-label" for="coupontype">Service</label>
                                            <div class="controls">
                                                <select class="form-control select2" name="service_type" id="service_type" style="width: 300px;">
													  <option value="">Select Option</option>
													  <?php foreach ($services as $service){ ?>
													  <option value="<?php echo $service['service_type_id'];?>"><?php echo $service['service_type_name'];?></option>
													  <?php } ?>
												</select>
                                            </div>
                                    </div>-->
                                    <div class="control-group">											
											<label class="control-label" for="coupontype">Service</label>
                                            <div class="controls">
													  <?php foreach ($service_types as $service_type): ?>
                                                        <label class="checkbox inline"><input type="checkbox" name="service_types[]" value="<?= $service_type['service_type_id']; ?>"> <?= $service_type['service_type_name']; ?></label>
													  <?php endforeach; ?>
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
									<div class="control-group">											
											<label class="control-label" for="coupontype">Coupon Type</label>
                                            <div class="controls">
                                                <select class="form-control select2" name="coupontype" id="coupontype" style="width: 300px;">
													  <option value="">Select Option</option>
													  <option value="FT">First time booking</option>
													  <option value="ET">Everybooking</option>
													  <option value="OT">One Time</option>
												</select>
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
                                    
                                    <div class="control-group">											
											<label class="control-label" for="offertype">Offer Type</label>
                                            <div class="controls">
                                                <select class="form-control select2" name="offertype" id="offertype" style="width: 300px;">
													  <option value="">Select Option</option>
													  <option value="F">Flat</option>
													  <option value="P">Per Hour</option>
												</select>
                                            </div>			
                                    </div>
                                    
									
										<!--<div id="discount_percentage">
										<div class="control-group" id="">											
											<label class="control-label" for="discountprice">Discount Price</label>
											<div class="controls">
												<input type="text" class="span6" style="width: 300px;" id="discountprice" name="discountprice" value="">
											</div>		
										</div>
										</div>-->
										<div class="control-group" id="discount_percentage" style="display:none;">											
												<label class="control-label" for="discounttype">Discount Type</label>
												<div class="controls">
													<select class="form-control select2" name="discounttype" id="discounttype" style="width: 300px;">
														<!-- <option value="">Select Option</option> -->
														  <option value="0">Percentage</option>
														  <option value="1">Price</option>
													</select>
												</div>				
										</div>
										
									
									
									
										<div class="control-group" id="discount_price">											
											<label class="control-label" for="discount_price">Discount <span id="discpric_txt">Price</span></label>
											<div class="controls">
												<input type="number" class="span6" style="width: 300px;" id="" name="discount_price" value="">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
												<label class="control-label" for="minhour">Minimum Hour</label>
												<div class="controls">
													<select class="form-control" name="minhour" id="minhour" style="width: 300px;">
														  <option value="">Select Hour</option>
														  <option value="2">2</option>
														  <option value="3">3</option>
														  <option value="4">4</option>
														  <option value="5">5</option>
														  <option value="6">6</option>
														  <option value="7">7</option>
													</select>
												</div>			
										</div>
									
                                    
                                    <div class="control-group">											
                                        <label class="control-label">Week Days</label>
                                            <div class="controls">
                                                <label class="checkbox inline"><input type="checkbox"  name="w_day[]" value="0" /> SUN</label>
                                                <label class="checkbox inline"><input type="checkbox" name="w_day[]" value="1" /> MON</label>
                                                <label class="checkbox inline"><input type="checkbox" name="w_day[]" value="2" /> TUE</label>
                                                <label class="checkbox inline"><input type="checkbox" name="w_day[]" value="3" /> WED</label>
                                                <label class="checkbox inline"><input type="checkbox" name="w_day[]" value="4" /> THU</label>
                                                <label class="checkbox inline"><input type="checkbox" name="w_day[]" value="5" /> FRI</label>
                                                <label class="checkbox inline"><input type="checkbox" name="w_day[]" value="6" /> SAT</label>
                                            </div>		
                                    </div>
									
									<div class="control-group">											
                                        <label class="control-label">For App Only</label>
                                            <div class="controls">
                                                <label class="radio inline"><input type="radio"  name="couponforapp" value="1"> Yes</label>
                                                <label class="radio inline"><input type="radio" name="couponforapp" value="0" checked="checked"> No</label>
                                            </div>		
                                    </div>
									
									<div class="control-group">											
                                        <label class="control-label">Status</label>
                                            <div class="controls">
                                                <label class="radio inline"><input type="radio"  name="status" value="1"> Active</label>
                                                <label class="radio inline"><input type="radio" name="status" value="0"> Inactive</label>
                                            </div>	<!-- /controls -->			
                                    </div>
										
                                    <br />
										
                                    <div class="form-actions">
                                        <input type="submit" class="btn btn-primary pull-right" value="Submit" name="coupon_sub">
<!--					<button class="btn">Cancel</button>-->
                                    </div> <!-- /form-actions -->
                            </fieldset>
                        </form>
			
                    </div>
		</div>
            </div> <!-- /widget-content -->
	</div> <!-- /widget -->
    </div> <!-- /span6 -->
    
</div>