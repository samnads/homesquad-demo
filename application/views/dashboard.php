<script src="//maps.googleapis.com/maps/api/js?key=<?= $settings->google_map_api_key; ?>&libraries=places&callback=Function.prototype" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/gh/geocodezip/v3-utility-library@master/archive/maplabel/src/maplabel.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<link rel="stylesheet" type="text/css" href="css/bootstrap4.css" >
<style>
.container, .container-sm, .container-md, .container-lg, .container-xl { max-width: 100%; }
</style>
<div class="common-popup-wrapper">
     <div class="common-popup-section d-flex">
          <div class="common-popup-main">
               <div class="close-btn"><img src="images/close-big-w.webp" alt=""></div>

               <div class="row common-popup-title m-0">
                    <h5>Lorem ipsum</h5>
               </div>

              <div class=" cm-field-content-main m-0">
                   <div class="row cm-field-main m-0">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                   </div>
              </div>

          </div>
     </div>
<script type="text/javascript">
$(document).ready(function() {
	$(".close-btn").click(function(){
		$(".common-popup-wrapper").hide(500);
	});
});
</script>
</div><!--common-popup-wrapper end-->
<section>
  <div class="row dash-top-wrapper no-left-right-margin">
      <div class="col-md-3 col-sm-3 colour-box-main">
        <div class="colour-box logo-color1">
        	<div class="n-color-cont-box">
                <div class="n-icon"><img src="images/jobs-b.webp"  /></div>
                <h2 class="text-center">Today's Booking</h2>
                <div class="row m-0">
                  <div class="col-sm-6 text-right pl-0">
                    <div class="total-booking-text">Total Count</div>
                    <div class="total-booking-num" id="bookings_today_total">0</div>
                  </div>
                  <div class="col-sm-6 pr-0">
                    <div class="total-booking-text">Total Hours</div>
                    <div class="total-booking-num" id="bookings_today_total_hours">0.00</div>
                  </div>
                </div>
          	</div>
            <div class="view-all-booking"> <a href="<?=base_url('booking')?>" title="View all today's bookings" class="full-width text-center"> View <span class="colour-rit-icon no-right-padding"><i class="fa fa-arrow-circle-o-right"></i></span>
                  <div class="clear"></div>
                  </a>
                  <div class="clear"></div>
                </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-3 colour-box-main">
        <div class="colour-box logo-color2">
        	<div class="n-color-cont-box">
                <div class="n-icon"><img src="images/scheduled.webp"  /></div>
                <h2 class="text-center">Monthly Booking</h2>
                <div class="row m-0">
                  <div class="col-sm-6 text-right pl-0">
                    <div class="total-booking-text">Total Count</div>
                    <div class="total-booking-num" id="bookings_monthly_total">0</div>
                  </div>
                  <div class="col-sm-6 pr-0">
                    <div class="total-booking-text">Total Hours</div>
                    <div class="total-booking-num" id="bookings_monthly_total_hours">0.00</div>
                  </div>
                </div>
          	</div>
            <div class="view-all-booking"> <a href="<?=base_url('booking/booking_reports')?>" title="View all Monthly bookings" class="full-width text-center"> View <span class="colour-rit-icon no-right-padding"><i class="fa fa-arrow-circle-o-right"></i></span>
                  <div class="clear"></div>
                  </a>
                  <div class="clear"></div>
                </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-3 colour-box-main">
        <div class="colour-box logo-color3">
        	<div class="n-color-cont-box">
                <div class="n-icon"><img src="images/cancel-booking.webp"  /></div>
                <h2 class="text-center">Today Cancel Booking</h2>
                <div class="row m-0">
                  <div class="col-sm-6 text-right pl-0">
                    <div class="total-booking-text">Total Count</div>
                    <div class="total-booking-num" id="booking_cancel_today_total">0</div>
                  </div>
                  <div class="col-sm-6 pr-0">
                    <div class="total-booking-text">Total Hours</div>
                    <div class="total-booking-num" id="booking_cancel_today_hours">0.00</div>
                  </div>
                </div>

          	</div>

            <div class="view-all-booking"> <a href="<?=base_url('reports/booking/cancel')?>" title="View all cancel bookings" class="full-width text-center"> View <span class="colour-rit-icon no-right-padding"><i class="fa fa-arrow-circle-o-right"></i></span>
                  <div class="clear"></div>
                  </a>
                  <div class="clear"></div>
                </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-3 colour-box-main">
        <div class="colour-box logo-color4">
        	<div class="n-color-cont-box">
                <div class="n-icon"><img src="images/customers.webp"  /></div>
                <h2 class="text-center">Customers</h2>
                <div class="row m-0">
                  <div class="col-sm-6 text-right pl-0">
                    <div class="total-booking-text">Total Count</div>
                    <div class="total-booking-num" id="customers_total">0</div>
                  </div>
                  <div class="col-sm-6 pr-0">
                    <div class="total-booking-text">Total Hours</div>
                    <div class="total-booking-num" id="bookings_total_hours">0.00</div>
                  </div>
                </div>
          	</div>
            <div class="view-all-booking"> <a href="<?=base_url('customer/add')?>" title="Create new customer"><span class="colour-rit-icon no-left-padding"> <i class="fa fa-pencil-square"></i></span> Create
            <div class="clear"></div>
            </a> <a class="text-right" href="<?=base_url('customers')?>" title="View all customers"> View <span class="colour-rit-icon no-right-padding"><i class="fa fa-arrow-circle-o-right"></i></span>
            <div class="clear"></div>
            </a>
            <div class="clear"></div>
          </div>
        </div>
      </div>
  </div>
  <!--row content-wrapper end-->
</section>
<section>
  <div class="row content-wrapper m-0">
    <div class="col-sm-12">
      <div class="row n-box-summary-main m-0">
        <div class="col-sm-7 n-box-summary-left">
          <div class="row colour-box m-0">
            <div class="col-sm-6 colour-box-main n-booking-summary pb-0">
              <div class="colour-box p-0">
                <h2 class="text-black pl-3 strong">Our Side Booking</h2>
                <div class="row m-0">
                  <div class="col-sm-6">
                    <div class="total-booking-text text-black">Total Count</div>
                    <div class="total-booking-num text-black" id="bookings_company_total">0</div>
                  </div>
                  <div class="col-sm-6">
                    <div class="total-booking-text text-black">Total Hours</div>
                    <div class="total-booking-num text-black" id="bookings_company_total_hours">0.00</div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 colour-box-main n-booking-summary pb-0">
              <div class="colour-box p-0">
                <h2 class="text-black pl-3 strong">Partners Booking</h2>
                <div class="row m-0">
                  <div class="col-sm-6">
                    <div class="total-booking-text text-black">Total Count</div>
                    <div class="total-booking-num text-black" id="bookings_partner_total">0</div>
                  </div>
                  <div class="col-sm-6">
                    <div class="total-booking-text text-black">Total Hours</div>
                    <div class="total-booking-num text-black" id="bookings_partner_total_hours">0.00</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row  m-0">
            <div class="dash-suppurate-box">&nbsp;</div>
          </div>
          <div class="row colour-box m-0">
            <div class="col-sm-3">
              <div class="n-icon d-block"><img src="images/complaints-r.webp"  /></div>
            </div>
            <div class="col-sm-9 colour-box-main n-booking-summary pb-0">
              <div class="colour-box p-0">
                <h2 class="text-red pl-3 strong">Complaints</h2>
                <div class="row m-0">
                  <div class="col-sm-4">
                    <div class="total-booking-text text-red">Today</div>
                    <div class="total-booking-num text-red" id="complaints_today_total">0</div>
                  </div>
                  <div class="col-sm-4">
                    <div class="total-booking-text text-red">Last Week</div>
                    <div class="total-booking-num text-red" id="complaints_last_week_total">0</div>
                  </div>
                  <div class="col-sm-4">
                    <div class="total-booking-text text-red">This Month</div>
                    <div class="total-booking-num text-red" id="complaints_this_month_total">0</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-5 n-box-summary-right" style="background:#3d285d;">
          <!--<div class="n-payment-icon">
            <div class="cm-sale-con-graph"><img src="images/money.gif" alt=""></div>
          </div>

          <h2 class="text-center text-white pb-3 strong">Payments</h2>-->

          <style>
		  .n-box-summary-right .total-booking-num { font-size: 25px; }
		  .n-weekly-payment-det { border-top: 2px dotted #777; border-bottom: 2px dotted #777; margin: 30px 0px !important; padding: 30px 0px;}
		  </style>
          <div class="row n-today-payment-det m-0">
               <!--<div class="col-sm-12 pr-0">
                   <div class="total-booking-num" style="color:#FFF;"><span>Today Payments</span></div>
               </div>-->
               <div class="col-sm-4 pr-0">
                   <div class="total-booking-text" style="color:#00ffff;"><strong style="color:#FFF">Today Value</strong></div>
                   <div class="total-booking-num" style="color:#00ffff;"><span>AED</span> <span id="payments_today_total_amount" style="font-size:25px;">0.00</span></div>
               </div>
               <div class="col-sm-4 pr-0">
                   <div class="total-booking-text" style="color:#0de01b;">Collected</div>
                   <div class="total-booking-num" style="color:#0de01b;"><span>AED</span> <span id="payments_today_collected_amount" style="font-size:25px;">0.00</span></div>
               </div>
               <div class="col-sm-4 pr-0">
                   <div class="total-booking-text" style="color:#ff5151;">Balance</div>
                   <div class="total-booking-num" style="color:#ff5151;"><span>AED</span> <span id="payments_today_pending_amount" style="font-size:25px;">0.00</span></div>
               </div>
          </div>
          <div class="row n-weekly-payment-det ml-0 mr-0">
               <!--<div class="col-sm-12 pr-0">
                   <div class="total-booking-num" style="color:#FFF;"><span>Weekly Payments</span></div>
               </div>-->
               <div class="col-sm-4 pr-0">
                   <div class="total-booking-text" style="color:#00ffff;"><strong style="color:#FFF">Weekly Value</strong></div>
                   <div class="total-booking-num" style="color:#00ffff;"><span>AED</span> <span id="payments_weekly_total_amount" style="font-size:25px;">0.00</span></div>
               </div>
               <div class="col-sm-4 pr-0">
                   <div class="total-booking-text" style="color:#0de01b;">Collected</div>
                   <div class="total-booking-num" style="color:#0de01b;"><span>AED</span> <span id="payments_weekly_collected_amount" style="font-size:25px;">0.00</span></div>
               </div>
               <div class="col-sm-4 pr-0">
                   <div class="total-booking-text" style="color:#ff5151;">Balance</div>
                   <div class="total-booking-num" style="color:#ff5151;"><span>AED</span> <span id="payments_weekly_pending_amount" style="font-size:25px;">0.00</span></div>
               </div>
          </div>
          <div class="row n-monthly-payment-det m-0">
               <!--<div class="col-sm-12 pr-0">
                   <div class="total-booking-num" style="color:#FFF;"><span>Monthly Payments</span></div>
               </div>-->
               <div class="col-sm-4 pr-0">
                   <div class="total-booking-text" style="color:#00ffff;"><strong style="color:#FFF">Monthly Value</strong></div>
                   <div class="total-booking-num" style="color:#00ffff;"><span>AED</span> <span id="payments_monthly_total_amount" style="font-size:25px;">0.00</span></div>
               </div>
               <div class="col-sm-4 pr-0">
                   <div class="total-booking-text" style="color:#0de01b;">Collected</div>
                   <div class="total-booking-num" style="color:#0de01b;"><span>AED</span> <span id="payments_monthly_collected_amount" style="font-size:25px;">0.00</span></div>
               </div>
               <div class="col-sm-4 pr-0">
                   <div class="total-booking-text" style="color:#ff5151;">Balance</div>
                   <div class="total-booking-num" style="color:#ff5151;"><span>AED</span> <span id="payments_monthly_pending_amount" style="font-size:25px;">0.00</span></div>
               </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="mt-4">
  <div class="row content-wrapper m-0">
       <div class="col-sm-4">
            <div class="col-sm-12 n-bot-box-main p-0">
                 <h2><i class="fa fa-map-marker">&nbsp;</i> Driver Position</h2>
                 <div id="driver-map" style="height:340px"></div>
            </div>
       </div>
       <div class="col-sm-4">
            <div class="col-sm-12 n-bot-box-main p-0">
                 <h2 class=""><i class="fa fa-calendar">&nbsp;</i> Bookings</h2>

                 <div id="chart-3" class="pt-4"></div>
            </div>
       </div>
       <div class="col-sm-4">
                 <div class="col-md-12 n-main-box p-0">
          <div class="col-md-12 n-cont-main-box p-0">
            <div class="col-md-12 col-sm-12 tab-hed no-left-right-padding">
              <h2><i class="fa fa-clock-o">&nbsp;</i> Recent Activity <span class="pull-right"> <a href="<?php echo base_url('reports/useractivity') ?>" title="View All Recent Activities"> <img src="images/view-all.webp" class="n-n" /> <img src="images/view-all1.webp" class="n-h" /> </a> </span> </h2>
            </div>
            <!--tab-hed end-->
            <div class="col-md-12 col-sm-12 tab-cont no-padding rcnt-actvty-box" >
              <div class="report-cont-det">
                <ul id="recent_activities">
                </ul>
              </div>
              <!--report-cont-det end-->
            </div>
            <!--tab-hed end-->
          </div>
        </div>
       </div>
  </div>
</section>
<script>
    google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
     var data = new google.visualization.DataTable();
        data.addColumn('date', 'Date');
        data.addColumn('number', 'Schedules');
        data.addColumn({type: 'string', role: 'tooltip'});
        //data.addColumn('number', 'persone2');
        //data.addColumn('number', 'persone3');
        data.addRows([
            <?php
foreach ($grapharray as $graphval) {
    ?>
    [<?php echo $graphval; ?>],
            <?php }?>
])

        var options = {
          legend: 'none',
          //hAxis: { minValue: 0, maxValue: 9 },
          //curveType: 'function',
          pointSize: 10,
      };

      var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    //     var chart = new google.visualization.AnnotatedTimeLine(document.getElementById('chart_div'));
    //  chart.draw(data, {displayAnnotations: true});
      }
    </script>