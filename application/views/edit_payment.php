<style>
ul li{margin-left: 0;}
input.span3, textarea.span3, .uneditable-input.span3{width: 270px;}
</style>
<div class="">
    <div class="span7">      		
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-money"></i>
                <h3>Update Payment</h3>
                <a style="float:right ; margin-right:20px; cursor:pointer; text-decoration:none;" href="<?php echo base_url(); ?>customer/list_customer_payments"><i class="icon-th-list"></i></a>
            </div> <!-- /widget-header -->

            <div class="widget-content">



                <div class="tabbable">


                    <div class="tab-content">

                        <form id="edit-profile" class="form-horizontal" method="post">
                            <div class="alert alert-<?php echo $errors['class']; ?>" style="display: <?php echo !empty($errors) ? 'block' : 'none'?>">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <?php echo $errors['message']; ?>
                            </div>

                           <div class="control-group">											
                                <label class="control-label" for="customer">Customer</label>
                                <div class="controls">                                    
                                    <?php
                                        echo $customer->customer_name;
                                        ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /control-group -->
							
							<div class="control-group" id="req-balance-amount" style="display: none;">
                                <label class="control-label" for="requested-amount">Outstanding Amount</label>
                                <div class="controls">
                                    <input type="text" class="span3 disabled" id="requested-amount"
                                        name="requested_amount" value="" readonly="readonly">
                                </div> <!-- /controls -->
                            </div>
                            
                            <div class="control-group">											
                                <label class="control-label" for="collected-amount">Collected Amount</label>
                                <div class="controls">
                                    <input type="text" class="span3" id="collected-amount" name="collected_amount" value="<?= $payment->paid_amount ?>" required>
                                </div> <!-- /controls -->				
                            </div> <!-- /control-group -->
                             <div class="control-group">											
                                <label class="control-label" for="collected-amount">Collection Date</label>
                                <div class="controls">
                                    <input type="text" class="span3" id="collect_date" readonly="readonly" name="collect_date" value="<?= DateTime::createFromFormat('Y-m-d', $payment->paid_date)->format('d/m/Y'); ?>" required="required">
									<input type="hidden" id="quickbook_id" name="quickbook_id" value="<?= $payment->quickbook_id ?>" />
									<input type="hidden" id="quickbook_paysync_id" name="quickbook_paysync_id" value="<?= $payment->quickbookPaymentTokenId ?>" />
									<input type="hidden" id="quickbook_pay_id" name="quickbook_pay_id" value="<?= $payment->quickbook_payment_id ?>" />
								</div> <!-- /controls -->				
                            </div> <!-- /control-group -->
							
							<div class="control-group">											
                                <label class="control-label" for="paymenttype">Payment Type</label>
                                <div class="controls">                                    
                                    <select class="span3 sel2" name="paymenttype" id="paymenttype" data-placeholder="Select payment type" required="required">
                                        <option></option>
                                        <option value="0" <?= $payment->payment_method == '0' ? 'selected' : '' ?>>Cash</option>
                                        <option value="1" <?= $payment->payment_method == '1' ? 'selected' : '' ?>>Card</option>
                                        <option value="2" <?= $payment->payment_method == '2' ? 'selected' : '' ?>>Cheque</option>
                                        <option value="3" <?= $payment->payment_method == '3' ? 'selected' : '' ?>>Bank</option>
                                        <option value="4" <?= $payment->payment_method == '4' ? 'selected' : '' ?>>Credit Card</option>
                                    </select>
                                </div> <!-- /controls -->				
                            </div>
							
							<div class="control-group">											
                                <label class="control-label" for="memo">Memo</label>
                                <div class="controls">
                                    <input type="text" class="span3" id="memo" name="memo" value="<?= $payment->receipt_no ?>">
                                </div> <!-- /controls -->				
                            </div>
							
							<div class="control-group">											
                                <label class="control-label" for="description">Description</label>
                                <div class="controls">
                                    <textarea class="span3" id="description" name="description" required><?= $payment->pay_description ?></textarea>
                                </div> <!-- /controls -->				
                            </div>
                            
                            <br />


                            <div class="form-actions">
                                <button type="submit" name="update_payment" value="1" class="btn mm-btn">Update</button> 
                                <a class="btn" href="<?php echo base_url('customer/list_customer_payments'); ?>">Cancel</a>
                            </div> <!-- /form-actions -->

                        </form>




                    </div>


                </div>





            </div>				
        </div> <!-- /widget -->	      		
    </div> <!-- /span8 -->	      		      		      		      	
</div> <!-- /row -->
<script>
var status = '<?php echo $quickbookmessage["status"]; ?>';
var messagess = '<?php echo $quickbookmessage["message"]; ?>';
if(status !="")
{
	if(status =="success")
	{
		toastr.success(messagess)
	} else {
		toastr.error(messagess)
	}
}
</script>