<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=customer_payment_report_".date('d-m-Y').".xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<table border="1">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Sl.No</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Date</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Customer</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Name</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Memo</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Type</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Amount</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Paid Amount</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Balance Amount</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Status</th>
                        </tr>
                    </thead>
                    <tbody>
					<?php
					$sub_t = 0;
					$i = 1;
                    foreach ($payment_report as $pay){
						$sub_t += round($pay->paid_amount,2);
					?>
						<tr>
							<td style="line-height: 18px;"><?php echo $i; ?> </td>
							<td style="line-height: 18px;"><?php echo date('d/m/Y',strtotime($pay->paid_datetime)); ?> </td>
							<td style="line-height: 18px;"><?php echo $pay->customer_name; ?></td>
							<td style="line-height: 18px;">
								<?php
								if($pay->payment_status == 0)
								{
									echo "Draft Payment";
								} else {
									echo $pay->payment_reference;
								}
								?>
							</td>
							<td style="line-height: 18px;"><?php echo $pay->ps_no; ?></td>
							<td style="line-height: 18px;">
								<?php
								if($pay->payment_method == 0)
								{
									echo "Cash";
								} else if($pay->payment_method == 1){
									echo "Card";
								}
								else if($pay->payment_method == 2){
									echo "Cheque";
								}
								else if($pay->payment_method == 3){
									echo "Bank";
								}
								?>
							</td>
							<td style="line-height: 18px;"><?php echo $pay->paid_amount; ?></td>
							<td style="line-height: 18px;"><?php echo $pay->allocated_amount; ?></td>
							<td style="line-height: 18px;"><?php echo $pay->balance_amount; ?></td>
							<td style="line-height: 18px;">
								<?php
								if($pay->payment_status == 0)
								{
									echo "Draft";
								} else {
									echo "Posted";
								}
								?>
							</td>
							
						</tr>
					<?php
					$i++;	
					}
					?>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>Total</td>
							<td><?php echo $sub_t; ?></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
                    </tbody>