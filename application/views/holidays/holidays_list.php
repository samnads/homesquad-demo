<script src="https://unpkg.com/xlsx/dist/shim.min.js"></script>
<script src="https://unpkg.com/xlsx/dist/xlsx.full.min.js"></script>
<script src="https://unpkg.com/blob.js@1.0.1/Blob.js"></script>
<script src="https://unpkg.com/file-saver@1.3.3/FileSaver.js"></script>
<style type="text/css">
  .widget .widget-header {
    margin-bottom: 0px;
  }


  table.da-table tr td {
    padding: 0px 6px;
  }

  .datepicker-days table .disabled-date.day {
  background: #ffe1e1;
    color: #9b9b9b;
}

.datepicker table tr td.disabled,
.datepicker table tr td.disabled:hover {
  background: #ffe1e1;
    color: #9b9b9b;
}
</style>
<div id="new-holiday-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">New Holiday</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form class="form-horizontal" method="post" id="add-new-holiday-form">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <p>Holiday Date</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="date" autocomplete="off" placeholder="dd/mm/yyyy" value="<?=date('d/m/Y');?>" readonly required>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Holiday Name</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="holiday_name" autocomplete="off" required>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
          <button type="submit" class="n-btn mb-0" value="Submit" name="save_holiday">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div id="disable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Delete ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to delete this holiday ?</h3>
      <input type="hidden" id="holiday_id">
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_disable()">Delete</button>
    </div>
  </div>
</div>
<div class="row m-0">
  <div class="col-md-12">
    <div class="widget widget-table action-table">
      <div class="widget-header">
        <form class="form-horizontal" method="POST" action="holidays">
      <ul>
      <li><i class="icon-th-list"></i>
        <h3>Holidays</h3></li>
        <li>
          <input type="text" name="filter_from_date" autocomplete="off" placeholder="dd/mm/yyyy" value="<?=$filter_from_date;?>" readonly>
        </li>
        <li>
          <input type="text" name="filter_to_date" autocomplete="off" placeholder="dd/mm/yyyy" value="<?=$filter_to_date;?>" readonly>
        </li>
        <li>
          <input type="submit" class="n-btn" id="customer-statement-search" value="Search">
        </li>
        <?php if(count($holidays_list) > 0): ?>
        <li class="mr-0 float-right">
        <div class="topiconnew border-0 green-btn">
        	<a data-action="excel-export" title="Add Holiday"><i class="fa fa-file-excel-o"></i></a>
        </div>
        </li>
        <?php endif; ?>
        <li class="mr-0 float-right">
        <div class="topiconnew border-0 green-btn">
        	<a data-action="add-holiday" title="Add Holiday"><i class="fa fa-plus"></i></a>
        </div>
        </li>
        </ul>
        </form>
      </div>
      <!-- /widget-header -->
      <div class="widget-content">
        <table id="holiday-list-table" class="table table-hover da-table" width="100%">
          <thead>
            <tr>
              <th>
                No.
              </th>
              <th>
                Holiday
              </th>
              <th>
                Holiday Name
              </th>
              <th>
                Date As Text
              </th>
              <th>
                Added By User
              </th>
              <th>
                Added On
              </th>
              <th class="td-actions">
                Action
              </th>
            </tr>
          </thead>
          <tbody>
            <?php
if (count($holidays_list) > 0) {
    foreach ($holidays_list as $key => $holiday) {
        ?>
                <tr>
                  <td style="line-height: 18px; width: 20px;  text-align:center;">
                    <center><?php echo $key + 1; ?></center>
                  </td>
                  <td><?php echo DateTime::createFromFormat('Y-m-d', $holiday['date'])->format('d/m/Y') ?></td>
                  <td><?php echo $holiday['holiday_name'] ?></td>
                  <td><?php echo DateTime::createFromFormat('Y-m-d', $holiday['date'])->format('d F Y') ?></td>
                  <td><?php echo $holiday['created_by_user'] ?: '-' ?></td>
                  <td><?php echo DateTime::createFromFormat('Y-m-d H:i:s', $holiday['created_at'])->format('d/m/Y h:m A') ?></td>
                  <td style="line-height: 18px; width: 158px" class="td-actions">
                  	<center>

<a href="javascript:void(0)" class="n-btn-icon red-btn" title="Delete" onclick="confirm_disable_enable_modal(<?=$holiday['id']?>);"><i class="btn-icon-only fa fa-trash"> </i></a>
                    </center>
                  </td>
                </tr>
              <?php
}
}?>
          </tbody>
        </table>
        <table id="holiday-list-excel" class="table table-hover da-table" width="100%" style="display:none">
          <thead>
            <tr>
              <th>
                No.
              </th>
              <th>
                Holiday
              </th>
              <th>
                Holiday Name
              </th>
              <th>
                Added By User
              </th>
              <th>
                Added On
              </th>
              <th class="td-actions">
                Action
              </th>
            </tr>
          </thead>
          <tbody>
            <?php
if (count($holidays_list) > 0) {
    foreach ($holidays_list as $key => $holiday) {
        ?>
                <tr>
                  <td style="line-height: 18px; width: 20px;  text-align:center;">
                    <center><?php echo $key + 1; ?></center>
                  </td>
                  <td><?php echo DateTime::createFromFormat('Y-m-d', $holiday['date'])->format('d/m/Y') ?></td>
                  <td><?php echo $holiday['holiday_name'] ?></td>
                  <td><?php echo $holiday['created_by_user'] ?: '-' ?></td>
                  <td><?php echo DateTime::createFromFormat('Y-m-d H:i:s', $holiday['created_at'])->format('d/m/Y h:m A') ?></td>
                  <td style="line-height: 18px; width: 158px" class="td-actions">
                  	<center>

<a href="javascript:void(0)" class="n-btn-icon red-btn" title="Delete" onclick="confirm_disable_enable_modal(<?=$holiday['id']?>);"><i class="btn-icon-only fa fa-trash"> </i></a>
                    </center>
                  </td>
                </tr>
              <?php
}
}?>
          </tbody>
        </table>
      </div>
      <!-- /widget-content -->
    </div>
    <!-- /widget -->
  </div>
  <!-- /span6 -->
</div>
<!-- Modal -->
<script>
function confirm_disable_enable_modal(id) {
  $('#holiday_id').val(id);
  fancybox_show('disable-popup');
}
function confirm_disable() {
	$('body').append($('<form/>').attr({
		'action': _base_url + "holidays/delete",
		'method': 'post',
		'id': 'form_sdsds'
	}).append($('<input/>').attr({
		'type': 'hidden',
		'name': 'holiday_id',
		'value': $('#holiday_id').val()
	}))).find('#form_sdsds').submit();
}
function closeFancy() {
	$.fancybox.close();
}
(function(a) {
	a(document).ready(function(b) {
		if (a('#holiday-list-table').length > 0) {
			a("table#holiday-list-table").dataTable({
				'sPaginationType': "full_numbers",
				"bSort": true,
				"iDisplayLength": 100,
				"scrollY": true,
				"orderMulti": false,
				"scrollX": true,
        "aaSorting": [],
        "language": {
                "search": '<i class="fa fa-search"></i>',
                "searchPlaceholder": "Type month name or anything",
        },
				'columnDefs': [{
					'targets': [0],
					'orderable': false
				},
      {
					'targets': [3],
					'orderable': false,
          'visible' : false 
				}, ],
			});
		}
	});
})(jQuery);
/******************************************************** */
var datesForDisable = [<?=sizeof($holiday_dates) ? ("'" . implode('\',\'', $holiday_dates) . "'") : null?>];
$(function() {
    $('#add-new-holiday-form input[name="date"]').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        startDate: "today",
        daysOfWeekDisabled: [<?=implode(',', $weekends)?>],
        datesDisabled: datesForDisable,
        beforeShowDay: function (currentDate) {
                var dayNr = currentDate.getDay();
                var dateNr = moment(currentDate.getDate()).format("DD/MM/YYYY");
                    if (datesForDisable.length > 0) {
                        for (var i = 0; i < datesForDisable.length; i++) {
                            if (moment(currentDate).unix()==moment(datesForDisable[i],'YYYY-MM-DD').unix()){
                                return false;
                           }
                        }
                    }
                    return true;
                }
    });
    $('input[name="filter_from_date"]').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    });
    $('input[name="filter_to_date"]').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    });
})
/******************************************************** */
$('[data-action="add-holiday"]').click(function(){
    fancybox_show('new-holiday-popup');
});
/******************************************************** */
$('[data-action="excel-export"]').click(function(){
  var fileName = "Holiday List "+ "<?= str_replace("/", "_", $filter_from_date) ?>" + " - <?= str_replace("/", "_", $filter_to_date) ?>";
  var fileType = "xlsx";
  var table = document.getElementById("holiday-list-excel");
  var wb = XLSX.utils.table_to_book(table, {sheet: "Holidays"});
  const ws = wb.Sheets['Holidays'];
  var wscols = [
    {wch:15},
    {wch:25},
    {wch:30},
    {wch:25},
    {wch:20},
    {wch:20}
  ];
  ws['!cols'] = wscols;
    return XLSX.writeFile(wb, null || fileName + "." + (fileType || "xlsx"));
});
/******************************************************** */
</script>