<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title><?= $settings->site_name; ?> - Statement of <?= $customername; ?></title>
   </head>
   <style>
      @page {
      margin: 0px 0px 0px 0px !important;
      padding: 0px 0px 0px 0px !important;
      }
   </style>
   <body style="padding: 0px; margin: 0px;">
      <div class="main" style="width:793px; height:auto; padding: 0px; margin: 0px auto;">
         <header style="width:100%; height: 200px; overflow: hidden;  position: fixed; left:30px; top: 26px; z-index: 999;">
            <div style="width: 150px; height:auto; float: left; margin: 0px; padding: 3px 20px 0px 0px;">
                 <img src="<?= base_url('uploads/images/settings/invoice-pdf-logo.png'); ?>" width="150" height="150" />
            </div>
            <div style="width: auto; height:auto; float: right;">
               <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 24px; line-height: 16px; color: #333; padding: 15px 25px 0px 0px; margin: 0px;">
                  <?= $settings->site_name; ?>
               </p>
               <p style="font-family: Arial, Helvetica, sans-serif; font-size: 15.5px; line-height: 20px; color: #333; padding: 10px 25px 0px 0px; margin: 0px;">
                  <?php $this->load->view('includes/company_invoice_address'); ?>
               </p>
            </div>
            <div style="clear:both;"></div>
         </header>
         <section style="width:100%; height: 700px;  padding:150px 30px 0px 30px;">
            <div class="main-content" style="width: 90%; height:auto; padding: 0px 0px 0px 0px; margin: 0px auto;">
               <div class="tax-head" style="width:100%; height:auto; padding: 50px 0px 20px 0px; margin: 0px;">
                  <p style="font-family: Roboto, sans-serif; font-size: 30px; color: #555; line-height: 30px; padding: 0px; margin: 0px; text-align: center;">Statement of Account</p>
               </div>
               <div class="to-job" style="width:100%; height:auto; padding: 0px 0px 0px 0px; margin: 0px 0px 20px 0px;">
                  <div class="to-address" style="width:100%; height:auto; background: #e5f6fc; padding: 20px 0px 25px; margin: 0px;">
                     <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                           <td valign="middle">
                              <p style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; padding: 0px 25px 0px; margin: 0px;"><strong>Kind Attn: </strong></p>
                              <p style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; padding: 0px 25px 0px 45px; margin: 0px;">
                                 <strong> <?php echo $customername; ?></strong><br />
                                 <?php echo $address; ?>
                              </p>
                              <!--<p style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; padding: 10px 25px 0px 45px; margin: 0px; font-weight: bold;">Customer TRN &nbsp;&nbsp;&nbsp; :&nbsp; 0087267213283283</p>-->
                           </td>
                           <td align="right" valign="middle">
                              <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; margin: 0px;">
                                 <tr>
                                    <td style="padding: 0px 0px 0px 0px"><strong>Date of Issue</strong></td>
                                    <td>:</td>
                                    <td align="right" valign="middle" style="padding: 0px 25px 0px 0px"><strong><?php echo date('M d, Y') ?> </strong></td>
                                 </tr>
                              </table>
                           </td>
                        </tr>
                     </table>
                  </div>
               </div>
               <?php if ($this->uri->segment(6)): ?>
                <div style="margin-bottom:15px;text-align:center;font-family: Roboto, sans-serif;color:#555">Statement for the period <?=strtoupper(DateTime::createFromFormat('Y-m-d', $this->uri->segment(5))->format('d-M-Y'));?> to <?=strtoupper(DateTime::createFromFormat('Y-m-d', $this->uri->segment(6))->format('d-M-Y'));?></div>
                <?php endif;?>
               <div class="table-main" style="width:100%; height:auto; padding: 0px 0px 0px 0px; margin: 0px;">
                  <table width="100%" border="0" bordercolor="#FFFFFF" cellspacing="0" cellpadding="0" style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 14px; padding: 0px; margin: 0px;">
                     <tr class="table-head" bgcolor="#62A6C9" style=" font-size: 15px; line-height: 20px; font-weight: bold; color: #FFF;">
                        <td width="12%" valign="middle" style="padding: 10px 10px; border-right:0.5px solid #FFF; font-family:Arial, Helvetica, sans-serif; font-weight: bold;">Date</td>
                        <td width="32%" valign="middle" style="padding: 10px 10px; border-right:0.5px solid #FFF;">Reference</td>
                        <td width="18%" valign="middle" style="padding: 10px 10px; border-right:0.5px solid #FFF;text-align:right;">Debit</td>
                        <td width="18%" valign="middle" style="padding: 10px 10px; border-right:0.5px solid #FFF;text-align:right;">Credit</td>
                        <td width="20%" valign="middle" style="padding: 10px 10px; border-right:0.5px solid #FFF;text-align:right;">Balance</td>
                     </tr>
                     <?php if ($this->uri->segment(6)): ?>
					 <?php
					 $nbal = ($total_debit_before_start_date - $total_credit_before_start_date);
					 if($nbal != 0)
					 {
					 ?>
                     <tr bgcolor="#e5f6fc">
                        <!--<td style="padding: 10px 10px; border-left:0.5px solid #CCC;  border-bottom:0.5px solid #CCC;  border-right:0.5px solid #CCC;"><?//=strtoupper(DateTime::createFromFormat('Y-m-d', $this->uri->segment(5))->modify("-1 day")->format('d/m/Y'));?></td>-->
                        <td style="padding: 10px 10px; border-left:0.5px solid #CCC;  border-bottom:0.5px solid #CCC;  border-right:0.5px solid #CCC;"></td>
                        <td style="padding: 10px 10px; border-left:0.5px solid #CCC;  border-bottom:0.5px solid #CCC;  border-right:0.5px solid #CCC;">Opening Balance</td>
                        <!--<td style="padding: 10px 10px; border-left:0.5px solid #CCC;  border-bottom:0.5px solid #CCC;  border-right:0.5px solid #CCC;text-align:right;"><span style=" font-size: 11px;"></span> <?//= number_format($total_debit_before_start_date,2); ?></td>
                        <td style="padding: 10px 10px; border-left:0.5px solid #CCC;  border-bottom:0.5px solid #CCC;  border-right:0.5px solid #CCC;text-align:right;"><span style=" font-size: 11px;"></span> <?//= number_format($total_credit_before_start_date,2); ?></td>-->
						<td style="padding: 10px 10px; border-left:0.5px solid #CCC;  border-bottom:0.5px solid #CCC;  border-right:0.5px solid #CCC;text-align:right;"><span style=" font-size: 11px;"></span> </td>
                        <td style="padding: 10px 10px; border-left:0.5px solid #CCC;  border-bottom:0.5px solid #CCC;  border-right:0.5px solid #CCC;text-align:right;"><span style=" font-size: 11px;"></span> </td>
                        <td style="padding: 10px 10px; border-left:0.5px solid #CCC;  border-bottom:0.5px solid #CCC;  border-right:0.5px solid #CCC;text-align:right;"><span style=" font-size: 11px;"></span> <?= number_format($total_debit_before_start_date - $total_credit_before_start_date,2); ?></td>
                     </tr>
					 <?php
					 }
					 ?>
                     <?php endif;?>         
                     <?php
                        if($initial_bal_sign == 'Dr')
                        {
                          $credit = "0.00";
                          // $debit = $initial_balance;
                          $debit = "0.00";
                          $balance = number_format(($credit + $debit),2);
                          $newbal = ($credit + $debit);
                          //$newamount = ($credit + $debit);
                        } else {
                          // $credit = $initial_balance;
                          $credit = "0.00";
                          $debit = "0.00";
                          $balance = number_format(($credit + $debit),2);
                          // $newbal = '-'.$balance;
                          $newbal = 0;
                          //$newamount = 0;
                        }
                        ?>
                     <?php
                     if ($this->uri->segment(6)){
                        $calcdebitnew = $total_debit_before_start_date;
                        $calccreditnew = $total_credit_before_start_date;
                        $newbal = $total_debit_before_start_date - $total_credit_before_start_date;
                     }
                     ?>
                     <?php
                        //$i = 2;
                        $i = 1;
                        foreach ($customer_statement as $statval)
                        {
                        ?>
                     <tr bgcolor="<?php echo ($i%2 ? "#FFF" : "#e5f6fc"); ?>">
                        <td style="padding: 10px 10px; border-left:0.5px solid #CCC;  border-bottom:0.5px solid #CCC;  border-right:0.5px solid #CCC;"><?php echo date('d/m/Y',strtotime($statval->dateval)); ?></td>
                        <td style="padding: 10px 10px; border-bottom:0.5px solid #CCC;  border-right:0.5px solid #CCC; font-size: 11px;">
                           <?php
							if($statval->stattype=='B')
							{
								if($statval->paytypes=='CM')
								{
									$paymntval = 'Credit Memo';
                              	} else {
									if($statval->paymethval == 0)
									{
										$paymntval = 'Cash Payment';
									} else if($statval->paymethval == 1){
										$paymntval = 'Card Payment';
									} else if($statval->paymethval == 2){
										$paymntval = 'Cheque Payment';
									} else if($statval->paymethval == 3){
										$paymntval = 'Bank Payment';
									} else if($statval->paymethval == 4){
										$paymntval = 'Credit Card Payment';
									}
								}
                              	echo $statval->invoiceval." (".$paymntval.")";
							} else {
                              	echo $statval->invoiceval;
							}
                                     
                                     // echo payment_type($statval->invoiceval);
                                     // echo $statval->invoiceval;
                              ?>
                           <!--<?php// echo $statval->invoiceval; ?><?php// if($statval->stattype=='B'){ ?> (<?php// echo $statval->payinvref; ?>) <?php// } ?><?php// if($statval->invnotes !=''){ ?> <br>(<?php// echo $statval->invnotes; ?>) <?php// } ?><?php// if($statval->invoice_type =='2'){ ?><br>(Monthly Invoice) <?php// } ?>-->
                        </td>
                        <td style="padding: 10px 10px; border-bottom:0.5px solid #CCC; border-right:0.5px solid #CCC;  font-size: 11px; line-height: 14px;text-align:right;">
                           <?php
                              if($statval->stattype == 'A')
                              {
                                $calcdebit = $statval->amount;
                                //$newamount = $statval->amount;
                                echo number_format($statval->amount,2);
                              } else {
                                $calccredit = 0;
                                echo '0.00';
                              }
                              ?>
                        </td>
                        <td style="padding: 10px 10px; border-bottom:0.5px solid #CCC; border-right:0.5px solid #CCC; font-size: 12px;text-align:right;">
                            <?php
                              if($statval->stattype == 'B')
                              {
                                echo number_format($statval->amount,2);
                                $calcdebit = 0;
                                $calccredit = $statval->amount;
                                //$newamount = '-'.$statval->amount;
                              } else {
                                $calccredit = 0;
                                echo '0.00';
                                $calc = 0;
                              }
                              ?>
                        </td>
                        <td style="padding: 10px 10px; border-bottom:0.5px solid #CCC; border-right:0.5px solid #CCC; font-size: 12px;text-align:right;">
                           <?php
                              if($statval->stattype == 'A')
                              {
                                $newamount = $statval->amount;
                              } else {
                                $newamount = '-'.$statval->amount;
                              }
                              // echo $newamount;
                              // echo '<br>';
                              $newbal += $newamount;
                              $calcdebitnew += ($calcdebit);
                              $calccreditnew += ($calccredit);
                              ?>
                              <?= number_format($newbal,2); ?>
                        </td>
                        <!--<td align="center" style="padding: 10px 10px; border-bottom:0.5px solid #CCC; border-right:0.5px solid #CCC; font-size: 12px;"><?php// echo $jobs->line_vat_amount; ?></td>
                           <td align="center" style="padding: 10px 10px; border-bottom:0.5px solid #CCC; border-right:0.5px solid #CCC; font-size: 12px;"><?php// echo $jobs->line_net_amount; ?></td>-->
                     </tr>
                     <?php
                        $i++; } ?> 
                     <?php
                        $samount= substr($newbal, 0, 1);
                        if($samount == '-')
                        {
                        	$signvals = "(Cr)";
                        	$newbal = substr($newbal, 1);
                        } else {
                        	$signvals = "(Dr)";
                        }
                        ?>
						<?php
                     if ($this->uri->segment(6)){
						 ?>
                     <tr bgcolor="#FFF" style="">
                        <td style="padding: 20px 10px; border-right:0.5px solid #CCC; font-size: 16px; font-weight: bold;">&nbsp;</td>
                        <td style="padding: 20px 10px; border-bottom:0.5px solid #CCC;  border-right:0.5px solid #CCC; font-size: 15px; font-weight: bold;">TOTAL</td>
                        <td style="padding: 20px 10px; border-bottom:0.5px solid #CCC; border-right:0.5px solid #CCC;  font-size: 15px; font-weight: bold; line-height: 14px;text-align:right;"><span style=" font-size: 11px;color:#777;">AED</span> <?php echo number_format(($calcdebitnew + $debit - $total_debit_before_start_date),2); ?></td>
                        <td style="padding: 20px 10px; border-bottom:0.5px solid #CCC; border-right:0.5px solid #CCC; font-size: 15px; font-weight: bold;text-align:right;"><span style=" font-size: 11px;color:#777;">AED</span> <?php if($calccreditnew != ""){ echo number_format(($calccreditnew - $total_credit_before_start_date),2); } else { echo '0.00'; } ?></td>
                        <td style="padding: 20px 10px; border-bottom:0.5px solid #CCC; border-right:0.5px solid #CCC; font-size: 15px; font-weight: bold;text-align:right;"><span style=" font-size: 11px;color:#777;">AED</span> <?php echo number_format($newbal,2); ?> <?php echo $signvals; ?></td>
                     </tr>
					 <?php
					 } else {
					 ?>
					 <tr bgcolor="#FFF" style="">
                        <td style="padding: 20px 10px; border-right:0.5px solid #CCC; font-size: 16px; font-weight: bold;">&nbsp;</td>
                        <td style="padding: 20px 10px; border-bottom:0.5px solid #CCC;  border-right:0.5px solid #CCC; font-size: 15px; font-weight: bold;">TOTAL</td>
                        <td style="padding: 20px 10px; border-bottom:0.5px solid #CCC; border-right:0.5px solid #CCC;  font-size: 15px; font-weight: bold; line-height: 14px;text-align:right;"><span style=" font-size: 11px;color:#777;">AED</span> <?php echo number_format(($calcdebitnew + $debit),2); ?></td>
                        <td style="padding: 20px 10px; border-bottom:0.5px solid #CCC; border-right:0.5px solid #CCC; font-size: 15px; font-weight: bold;text-align:right;"><span style=" font-size: 11px;color:#777;">AED</span> <?php if($calccreditnew != ""){ echo number_format($calccreditnew,2); } else { echo '0.00'; } ?></td>
                        <td style="padding: 20px 10px; border-bottom:0.5px solid #CCC; border-right:0.5px solid #CCC; font-size: 15px; font-weight: bold;text-align:right;"><span style=" font-size: 11px;color:#777;">AED</span> <?php echo number_format($newbal,2); ?> <?php echo $signvals; ?></td>
                     </tr>
					 <?php
					 }
					 ?>
                  </table>
               </div>
            </div>
            <table width="100%"  border="0" cellspacing="0" cellpadding="0" style="margin-top: 50px;" >
               <tr>
                  <td width="22%">
                     <p style="font-family: Arial, Helvetica, sans-serif; font-size: 16px; font-weight: bold; line-height: 18px; color: #333; padding: 5px 15px 5px 0px; margin: 0px;"><strong>Bank Details</strong></p>
                  </td>
                  <td width="4%">&nbsp;</td>
                  <td width="74%">&nbsp;</td>
               </tr>
               <tr>
                  <td>
                     <p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; color: #333; padding: 5px 15px 5px 0px; margin: 0px;">BANK NAME</p>
                  </td>
                  <td>&nbsp;</td>
                  <td>
                     <p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; color: #333; padding: 5px 15px 5px 0px; margin: 0px;">Example Bank Name</p>
                  </td>
               </tr>
               <tr>
                  <td>
                     <p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; color: #333; padding: 5px 15px 5px 0px; margin: 0px;">ACCOUNT NAME</p>
                  </td>
                  <td>&nbsp;</td>
                  <td>
                     <p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; color: #333; padding: 5px 15px 5px 0px; margin: 0px;">Example Account Name</p>
                  </td>
               </tr>
               <tr>
                  <td>
                     <p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; color: #333; padding: 5px 15px 5px 0px; margin: 0px;">ACCOUNT NUMBER</p>
                  </td>
                  <td>&nbsp;</td>
                  <td>
                     <p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; color: #333; padding: 5px 15px 5px 0px; margin: 0px;">0000000000</p>
                  </td>
               </tr>
               <tr>
                  <td>
                     <p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; color: #333; padding: 5px 15px 5px 0px; margin: 0px;">IBAN NUMBER</p>
                  </td>
                  <td>&nbsp;</td>
                  <td>
                     <p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; color: #333; padding: 5px 15px 5px 0px; margin: 0px;">0000000000</p>
                  </td>
               </tr>
            </table>
         </section>
         <footer style="height: 120px; position: fixed; left:0; bottom:0; z-index: 999;">
            <div style="">
               <table width="100%" border="0" style="padding-bottom: 10px;">
               <tr>
                  <td width="23.3333%" align="center">
                     <?php
                        if($invoice_detail[0]->signature_status == 1)
                        {
                         $imagepath = FCPATH."/invoice_customer_signature/".$invoice_detail[0]->signature_image;
                           //$imagepath = FCPATH."/invoice_customer_signature/customer-signature.jpg";
                        ?>
                     <img src="<?php echo $imagepath; ?>" width="60" height="34" style="padding-left: 50px;" />
                     <?php
                        }
                        ?>
                  </td>
                  <td width="53.3333%">&nbsp;</td>
                  <td width="23.3333%">
                     <!--<img src="<?= FCPATH ?>invoice_customer_signature/customer-signature.jpg" width="60" height="34" />-->
                  </td>
               </tr>
            </div>
            <table width="100%" border="0" style="border-top: 1px solid #888; margin-top: 10px;">
               <!--<tr>
                  <td width="23.3333%"><p style="font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #333; line-height: 16px; padding: 0px 0px 0px 30px; text-align:right; margin: 0px; font-weight: 600;">Receiver's Signature</p></td>
                  <td width="53.3333%">&nbsp;</td>
                  <td width="23.3333%"><p style="font-family: Verdana, Geneva, sans-serif; font-size: 12px;color: #333; line-height: 16px; padding: 0px; margin: 0px; font-weight: 600;">Signature</p></td>
                  </tr>-->
               <tr>
                  <td>&nbsp;</td>
               </tr>
            </table>
            <div style=" width: 100%; margin-top: 25px;">
               <p style="font-family: Verdana, Geneva, sans-serif; font-size: 11px;color: #333; line-height: 16px; text-align: center; padding: 0px; margin: 0 auto; font-weight: 600; ">This is computer generated statement and does not require a signature.</p>
            </div>
         </footer>
      </div>
   </body>
</html>
<?php
   function numberTowords($num)
   {
     $ones = array(
       0 =>"Zero",
       1 => "One",
       2 => "Two",
       3 => "Three",
       4 => "Four",
       5 => "Five",
       6 => "Six",
       7 => "Seven",
       8 => "Eight",
       9 => "Nine",
       10 => "Ten",
       11 => "Eleven",
       12 => "Twelve",
       13 => "Thirteen",
       14 => "Fourteen",
       15 => "Fifteen",
       16 => "Sixteen",
       17 => "Seventeen",
       18 => "Eighteen",
       19 => "Nineteen",
       "014" => "Fourteen"
     );
     $tens = array( 
       0 => "Zero",
       1 => "Ten",
       2 => "Twenty",
       3 => "Thirty", 
       4 => "Forty", 
       5 => "Fifty", 
       6 => "Sixty", 
       7 => "Seventy", 
       8 => "Eighty", 
       9 => "Ninety" 
     ); 
     $hundreds = array( 
       "Hundred", 
       "Thousand", 
       "Million", 
       "Billion", 
       "Trillion", 
       "Quardrillion" 
     ); /*limit t quadrillion */
     $num = number_format($num,2,".",","); 
     $num_arr = explode(".",$num); 
     $wholenum = $num_arr[0]; 
     $decnum = $num_arr[1]; 
     $whole_arr = array_reverse(explode(",",$wholenum)); 
     krsort($whole_arr,1); 
     $rettxt = ""; 
     foreach($whole_arr as $key => $i)
     {
       while(substr($i,0,1)=="0")
         $i=substr($i,1,5);
       if($i < 20){ 
       /* echo "getting:".$i; */
       $rettxt .= $ones[$i]; 
       }elseif($i < 100){ 
       if(substr($i,0,1)!="0")  $rettxt .= $tens[substr($i,0,1)]; 
       if(substr($i,1,1)!="0") $rettxt .= " ".$ones[substr($i,1,1)]; 
       }else{ 
       if(substr($i,0,1)!="0") $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0]; 
       if(substr($i,1,1)!="0")$rettxt .= " ".$tens[substr($i,1,1)]; 
       if(substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)]; 
       } 
       if($key > 0){ 
       $rettxt .= " ".$hundreds[$key]." "; 
       }
     } 
   if($decnum > 0){
   $rettxt .= " and ";
   if($decnum < 20){
   $rettxt .= $ones[$decnum];
   $rettxt .= " Phills";
   }elseif($decnum < 100){
   $rettxt .= $tens[substr($decnum,0,1)];
   if(substr($decnum,1,1) > 0)
   {
   $rettxt .= " ".$ones[substr($decnum,1,1)];
   }
   $rettxt .= " Phills";
   }
   }
   return $rettxt;
   }
   ?>