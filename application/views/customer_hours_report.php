<script src="https://unpkg.com/xlsx/dist/shim.min.js"></script>
<script src="https://unpkg.com/xlsx/dist/xlsx.full.min.js"></script>
<script src="https://unpkg.com/blob.js@1.0.1/Blob.js"></script>
<script src="https://unpkg.com/file-saver@1.3.3/FileSaver.js"></script>
<style type="text/css">
    .widget .widget-header {
        margin-bottom: 0px;
    }

    .topiconnew {
        cursor: pointer;
    }
</style>
<div class="row m-0">
    <div class="col-sm-12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header">
                <form class="form-horizontal" method="post">







                    <ul>
                        <li><i class="icon-th-list"></i>
                            <h3>Customer Hours Report</h3>
                        </li>
                        <li><input type="text" readonly="readonly" id="vehicle_date" name="from_date" value="<?php echo $activity_date ?>"></li>
                        <li><input type="text" readonly="readonly" id="schedule_date" name="to_date" value="<?php echo $activity_date_to ?>"></li>
                        <li><input type="submit" class="n-btn" value="Go" name="vehicle_report"></li>
                        <li class="mr-0 float-right">
                            <div class="topiconnew border-0 green-btn"><a data-action="excel-export"><i class="fa fa-download"></i></a></div>
                        </li>
                    </ul>

                </form>
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table table-hover da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 40px; text-align:center"> Sl No.</th>
                            <th style="line-height: 18px;"> Customer</th>
                            <th style="line-height: 18px;"> Source</th>
                            <th style="line-height: 18px;"> Service</th>
                            <th style="line-height: 18px;"> Hours</th>
                            <th style="line-height: 18px;"> Amount</th>
                            <th style="line-height: 18px;"> Count</th>
                            <th style="line-height: 18px;"> Added Date</th>
                            <th style="line-height: 18px;"> Is Flagged</th>
                            <th style="line-height: 18px;">Date of last service</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($maid_hours_report)) {
                            $i = 0;
                            $total_hrs = 0;
                            foreach ($maid_hours_report as $report) {
                                $maid_time = explode(":", $report->hours);
                                $maid_time = $maid_time[0] + ($maid_time[1] / 60);
                                $total_hrs += $maid_time;
                                echo '<tr>'
                                    . '<td style="line-height: 18px; text-align:center">' . ++$i . '</td>'
                                    . '<td style="line-height: 18px;">' . $report->customer_name . '</td>'
                                    . '<td style="line-height: 18px;">' . $report->customer_source . '</td>'
                                    . '<td style="line-height: 18px;">' . $report->service_type_name . '</td>'
                                    . '<td style="line-height: 18px;">' . $maid_time . '</td>'
                                    . '<td style="line-height: 18px;">' . $report->netamount . '</td>'
                                    . '<td style="line-height: 18px;">' . $report->netcount . '</td>'
                                    . '<td style="line-height: 18px;">' . date('d-m-Y', strtotime($report->customer_added_datetime)) . '</td>'
                                    . '<td style="line-height: 18px;">' . ($report->is_flag == "N" ? 'No' :  'Yes') . '</td>'
                                    . '<td style="line-height: 18px;">' . date('d-m-y', strtotime($report->service_date)) . '</td>'
                                    . '</tr>';
                            }
                            //echo '<tr>'. '<td style="line-height: 18px;"></td>'. '<td style="line-height: 18px;font-weight:bold;">Total</td>'. '<td style="line-height: 18px;">' . $total_hrs . '</td>'.'</tr>';
                        } else {
                            //echo '<tr><td colspan="5">No Results!</td></tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div><!-- /widget-content -->

        </div><!-- /widget -->
    </div><!-- /span12 -->
</div>


<div style="display: none">
    <div class="widget-content" style="margin-bottom:30px">
        <table border="1" width="100%" cellspacing="0" cellpadding="10" style="font-size:11px; border-color: #ccc;" class="ptable" id="divToPrint">
            <thead>
                <tr>
                    <th style="line-height: 18px;"> Sl No.</th>
                    <th style="line-height: 18px;"> Customer</th>
                    <th style="line-height: 18px;"> Source</th>
                    <th style="line-height: 18px;"> Service</th>
                    <th style="line-height: 18px;"> Hours</th>
                    <th style="line-height: 18px;"> Amount</th>
                    <th style="line-height: 18px;"> Count</th>
                    <th style="line-height: 18px;"> Added Date</th>
                    <th style="line-height: 18px;"> Is Flagged</th>
                    <th style="line-height: 18px;">Date of last service</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (!empty($maid_hours_report)) {
                    $i = 0;
                    $total_hrs = 0;
                    foreach ($maid_hours_report as $report) {
                        $maid_time = explode(":", $report->hours);
                        $maid_time = $maid_time[0] + ($maid_time[1] / 60);
                        $total_hrs += $maid_time;
                        echo '<tr>'
                            . '<td style="line-height: 18px;">' . ++$i . '</td>'
                            . '<td style="line-height: 18px;">' . $report->customer_name . '</td>'
                            . '<td style="line-height: 18px;">' . $report->customer_source . '</td>'
                            . '<td style="line-height: 18px;">' . $report->service_type_name . '</td>'
                            . '<td style="line-height: 18px;">' . $maid_time . '</td>'
                            . '<td style="line-height: 18px;">' . $report->netamount . '</td>'
                            . '<td style="line-height: 18px;">' . $report->netcount . '</td>'
                            . '<td style="line-height: 18px;">' . date('d-m-Y', strtotime($report->customer_added_datetime)) . '</td>'
                            . '<td style="line-height: 18px;">' . ($report->is_flag == "N" ? 'No' :  'Yes') . '</td>'
                            . '<td style="line-height: 18px;">' . date('d-m-y', strtotime($report->service_date)) . '</td>'
                            . '</tr>';
                    }
                    //echo '<tr>'. '<td style="line-height: 18px;"></td>'. '<td style="line-height: 18px;font-weight:bold;">Total</td>'. '<td style="line-height: 18px;">' . $total_hrs . '</td>'.'</tr>';
                } else {
                    //echo '<tr><td colspan="5">No Results!</td></tr>';
                }
                ?>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
    function exportF(elem) {
        var table = document.getElementById("divToPrint");
        var html = table.outerHTML;
        var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url 
        elem.setAttribute("href", url);
        elem.setAttribute("download", "Customer Hours Report <?php echo $activity_date ?> - <?php echo $activity_date_to ?>.xls"); // Choose the file name
        return false;
    }
    /******************************************************** */
    $('[data-action="excel-export"]').click(function () {
    var fileName = "Customer Hours Report " + "<?= $activity_date ?>" + " - <?= $activity_date_to ?>";
    var fileType = "xlsx";
    var table = document.getElementById("divToPrint");

    // Get the raw workbook without formatting
    var wb = XLSX.utils.table_to_book(table, { sheet: "Customer Hours Report",dateNF:'dd/mm/yyyy;@',cellDates:true, raw: true });

    // Access the worksheet
    var ws = wb.Sheets['Customer Hours Report'];

    // Set column widths
    var wscols = [
        { wch: 15 },
        { wch: 25 },
        { wch: 30 },
        { wch: 25 },
        { wch: 20 },
        { wch: 20 },
        { wch: 20 },
        { wch: 15 },
        { wch: 20 },
        { wch: 15 },
    ];
    ws['!cols'] = wscols;

    // Export the modified workbook
    return XLSX.writeFile(wb, null || fileName + "." + (fileType || "xlsx"));
});

    /******************************************************** */
</script>
