<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?= $settings->site_name; ?> Invoice - <?= $invoice_detail[0]->invoice_num; ?></title>
</head>
<style>
@page {
	margin: 0px 0px 0px 0px !important;
	padding: 0px 0px 0px 0px !important;
}


</style>
<body style="padding: 0px; margin: 0px;">

<div class="main" style="width:793px; height:auto; padding: 0px; margin: 0px auto;">

    <header style="width:100%; height: 200px; overflow: hidden;  position: fixed; left:30px; top: 26px; z-index: 999;">
            
            <div style="width: 150px; height:auto; float: left; margin: 0px; padding: 3px 20px 0px 0px;">
                 <img src="<?= base_url('uploads/images/settings/home-squad-logo.png'); ?>" width="200" height="50" />
            </div>
            
            <div style="width: auto; height:auto; float: right;">
                 <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 24px; line-height: 16px; color: #333; padding: 15px 25px 0px 0px; margin: 0px;">
                 <?= $settings->site_name; ?>
                 </p>
                 
                 <p style="font-family: Arial, Helvetica, sans-serif; font-size: 15.5px; line-height: 20px; color: #333; padding: 10px 25px 0px 0px; margin: 0px;">
                    <?php $this->load->view('includes/company_invoice_address'); ?>
                 </p>
            </div>
            
            <div style="clear:both;"></div>
            
    </header>
    
  <section style="width:100%; height: 700px;  padding:200px 30px 0px 30px;">
             <div style="width: 100%; height:auto; margin: 0px; padding: 0px;">
                 <div style="width: 50%; height:auto; float: left; margin: 0px; padding: 0px;">
                      
                      <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 14px; line-height: 16px; color: #333; padding: 0px 0px 10px 0px; margin: 0px; text-transform: uppercase;">
                         Recipient :
                     </p>
                     
                     <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 15px; line-height: 20px; color: #333; padding: 0px 0px 0px 20px; margin: 0px;">
                         <b><?php echo $invoice_detail[0]->customer_name; ?></b>,
                     </p>
                     
                     <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; color: #333; padding: 0px 0px 0px 20px; margin: 0px;">
                        <?php echo $invoice_detail[0]->bill_address; ?>
                     </p>
					 <?php
					  if($invoice_detail[0]->trnnumber != "")
					  {
					  ?>
					 <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; color: #333; padding: 0px 0px 0px 20px; margin: 0px;">
                        TRN: <?php echo $invoice_detail[0]->trnnumber; ?>
                     </p>
					  <?php } ?>
                     
                     
                     
                     <!--<p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 14px; line-height: 16px; color: #333; padding: 30px 0px 10px 0px; margin: 0px; text-transform: uppercase;">
                         Service Address :
                     </p>
                     
                     <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; color: #333; padding: 0px 0px 0px 20px; margin: 0px;">
                        <?php //echo $invoice_detail[0]->bill_address; ?>
                     </p>-->
                     
                 </div>
                 
                 <div style="width: 50%; height:auto; float: left; margin: 0px; padding: 0px;">
                   <div style="width: 100%; height:auto; margin: 0px; padding: 0px; background: #62A6C9;">
                           <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 16px; line-height: 16px; color: #FFF; padding: 10px 20px; margin: 0px;">
                         Tax Invoice :	 <?php echo $invoice_detail[0]->invoice_num; ?>
                     </p>
                      </div>
                      
                      
                      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 25px; color: #333; padding: 5px 0px 0px; margin: 0px; background: #eee;">
                          <tr>
                            <td width="50%" style="padding-left: 20px;">Issued</td>
                            <td width="50%" align="right" style="padding-right: 20px;"><?php echo date('d F - Y',strtotime($invoice_detail[0]->invoice_date)) ?></td>
                          </tr>
                          <tr>
                            <td style="padding-left: 20px;">Due</td>
                            <td align="right" style="padding-right: 20px;"><?php echo date('d F - Y',strtotime($invoice_detail[0]->invoice_due_date)) ?></td>
                          </tr>
                          <tr>
                            <td style="padding-left: 20px;padding-bottom: 10px;">Attn</td>
                            <td align="right" style="padding-right: 20px; line-height:15px; padding-bottom: 5px;">Finance and Accounts Department</td>
                          </tr>
                          <?php
                          if($invoice_detail[0]->customer_trn != "")
                          {
                          ?>
                          <tr>
                            <td style="padding-left: 20px; padding-bottom: 10px;">TRN</td>
                            <td align="right" style="padding-right: 20px; padding-bottom: 5px;"><?php echo $invoice_detail[0]->customer_trn; ?></td>
                          </tr>
                          <?php
                          }
                          ?>
                          <tr style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 16px; line-height: 16px; color: #FFF; padding: 0px; margin: 0px;">
                            <td style="padding: 15px 20px; background: #6098b7; ">Total</td>
                            <td align="right" style="padding: 15px 20px; background: #6098b7; "><span style="font-size: 14px;">AED</span> <?php echo number_format($invoice_detail[0]->invoice_net_amount,2); ?></td>
                          </tr>
                        </table>
               </div>
             <div style="clear:both;"></div>
             </div>
             
             
             
             
             <div style="width: 100%; height:auto; margin: 0px; padding: 50px 0px 0px 0px;">
                  <div style="width: 50%; height:auto; float: left; margin: 0px; padding: 0px;">
                       <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 14px; line-height: 16px; color: #333; padding: 0px; margin: 0px;">
                           <!-- For Services Rendered : -->
                       </p>
                  </div>
                  <div style="width: 50%; height:auto; float: left; margin: 0px; padding: 0px;">
                    <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 14px; line-height: 16px; color: #333; text-align: right; padding: 0px; margin: 0px;">
                      <!-- <span style="color: #777;">For the Month of</span> July 2020 -->
                    </p>
                  </div>
                  <div style="clear:both;"></div>
             </div>
             
             
             
             
             <div style="width: 100%; height:auto; margin: 0px; padding: 0px 0px 0px 0px;">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 16px; color: #333; padding: 0px; margin: 0px;">
                    <tr style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 13px; line-height: 20px; color: #FFF; background: #62A6C9; ">
                      <td width="26%" style="padding: 7px 0px 7px 10px; border: 1px solid; border-color: #CCC;">SERVICES</td>
                      <td width="45%" style="padding: 7px 0px 7px 10px; border: 1px solid; border-color: #CCC;">DESCRIPTION</td>
                      <td width="5%" align="center" style="border: 1px solid; border-color: #CCC;">HRS</td>
                      <!-- <td width="12%" align="center" style="border-right: 1px solid; border-color: #CCC;">Unit Cost</td> -->
                      <td width="12%" align="center" style="border: 1px solid; border-color: #CCC;">TOTAL</td>
                    </tr>
                    
                    



                    <?php
                    $i = 1;
                    foreach ($invoice_detail as $jobs)
                    {
                      $tot_hrs = ((strtotime($jobs->service_to_time) - strtotime($jobs->service_from_time))/ 3600);
                    ?>
                    <tr>
                      <td style="padding: 0px 0px 0px 10px;border: 1px solid; border-left: 1px solid; border-color: #CCC;">
                          <!-- <span style="font-size: 13px; font-weight: bold;"><?php echo date('M d,Y', strtotime($jobs->service_date)); ?></span><br /> -->
                          <span><?php echo $jobs->service_type_name ?: '-'; ?>
                          </span>
                      </td>
                      <td style="padding: 10px; border-bottom: 1px solid; border-right: 1px solid; border-color: #CCC;"><?php echo $jobs->description ?: '-'; ?></td>
                      <td align="center" style="border-bottom: 1px solid; border-right: 1px solid; border-color: #CCC;"><?php echo $tot_hrs; ?></td>
                      <!-- <td align="center" style="border-bottom: 1px solid; border-right: 1px solid; border-color: #CCC;"><span style="font-size: 11px;">AED</span> 25:00</td> -->
                      <td align="center" style="border-bottom: 1px solid; border-right: 1px solid; border-color: #CCC;"><span style="font-size: 11px;">AED</span> <?php echo number_format($jobs->line_amount,2); ?></td>
                    </tr>
                     <?php
                      $i++; } 
                    ?>
                    
                    
                  </table>
                  
                  
                  
                  
                  
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 20px; color: #333; font-weight: bold; padding: .5px 0px 5px -5px; margin: 0px;">
                    <tr style="">
                      <td width="30%">&nbsp;</td>
                      <td width="35%" style="padding:7px 15px; background: #eee; border-left: 1px solid; border-color: #CCC;">Subtotal</td>
                      <td width="35%" align="right" style="padding: 7px 15px; background: #eee; border-right: 1px solid; border-color: #CCC;"><span style="font-size:11px;">AED</span> <?php echo number_format($invoice_detail[0]->invoice_total_amount,2); ?></td>
                    </tr>
                    
                    
                    <tr>
                      <td>&nbsp;</td> 
                      <?php
                      $div = $invoice_detail[0]->invoice_tax_amount/$invoice_detail[0]->invoice_total_amount;
                      $div = is_nan($div) ? 0 : $div;
                      $service_vat_percentage = number_format(round($div*100),2);
                      ?>
                      <td style="padding: 7px 15px; border-left: 1px solid; border-color: #CCC;">VAT (<?= $service_vat_percentage; ?>%)</td>
                      <td align="right" style="padding: 7px 15px; border-right: 1px solid; border-color: #CCC;"><span style="font-size:11px;">AED</span> <?php echo number_format($invoice_detail[0]->invoice_tax_amount,2); ?></td>
                    </tr>
                    
                    
                    <tr style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 16px; line-height: 16px; color: #FFF; padding: 0px; margin: 0px;">
                        <td>&nbsp;</td> 
                        <td style="padding: 15px 15px; background: #6098b7; ">Total</td>
                        <td align="right" style="padding: 15px 15px; background: #6098b7; "><span style="font-size: 14px;">AED</span> <?php echo number_format($invoice_detail[0]->invoice_net_amount,2); ?></td>
                    </tr>
                    </table>
                  
                  

             </div>
			 
			 
<table width="100%"  border="0" cellspacing="0" cellpadding="0" style="margin-top: 50px;" >
	<tr>
		<td width="22%"><p style="font-family: Arial, Helvetica, sans-serif; font-size: 16px; font-weight: bold; line-height: 18px; color: #333; padding: 5px 15px 5px 0px; margin: 0px;"><strong>Bank Details</strong></p></td>
		<td width="4%">&nbsp;</td>
		<td width="74%">&nbsp;</td>
	</tr>
	
	<tr>
		<td><p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; color: #333; padding: 5px 15px 5px 0px; margin: 0px;">BANK NAME</p></td>
		<td>&nbsp;</td>
		<td><p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; color: #333; padding: 5px 15px 5px 0px; margin: 0px;">Example Bank Name</p></td>
	</tr>
	
	<tr>
		<td><p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; color: #333; padding: 5px 15px 5px 0px; margin: 0px;">ACCOUNT NAME</p></td>
		<td>&nbsp;</td>
		<td><p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; color: #333; padding: 5px 15px 5px 0px; margin: 0px;">Example Account Name</p></td>
	</tr>
	
	<tr>
		<td><p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; color: #333; padding: 5px 15px 5px 0px; margin: 0px;">ACCOUNT NUMBER</p></td>
		<td>&nbsp;</td>
		<td><p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; color: #333; padding: 5px 15px 5px 0px; margin: 0px;">0000000000</p></td>
	</tr>
	
	<tr>
		<td><p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; color: #333; padding: 5px 15px 5px 0px; margin: 0px;">IBAN NUMBER</p></td>
		<td>&nbsp;</td>
		<td><p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; color: #333; padding: 5px 15px 5px 0px; margin: 0px;">0000000000</p></td>
	</tr>
</table>

			 
			 
			 
			 
             
      
  
           
    
    
    <?php if($invoice_detail[0]->invoice_status == 2): ?>
    <div style="height:auto; float: left;padding: 50px 20px 0px 300px;">
                 <img src="<?= base_url('images/invoice-cancelled-icon.png'); ?>" width="150"/>
            </div>
    <?php endif; ?>
    
    
    
    
             
             
    </section>
    
    <footer style="height: 120px; position: fixed; left:0; bottom:0; z-index: 999;">
    
 <div style="">
<table width="100%" border="0" style="padding-bottom: 10px;">
<tr>
  <td width="23.3333%" align="center">
		<?php
	  if($invoice_detail[0]->signature_status == 1)
	  {
		  $imagepath = FCPATH."/invoice_customer_signature/".$invoice_detail[0]->signature_image;
      //$imagepath = FCPATH."/invoice_customer_signature/customer-signature.jpg";
	  ?>
		<img src="<?php echo $imagepath; ?>" width="60" height="34" style="padding-left: 50px;" />
		<?php
	  }
		?>
	</td>
  <td width="53.3333%">&nbsp;</td>
  <td width="23.3333%">
  <!--<img src="<?= FCPATH ?>invoice_customer_signature/customer-signature.jpg" width="60" height="34" />-->
  </td>
</tr>
</div>


<table width="100%" border="0" style="border-top: 1px solid #888; margin-top: 10px;">
  <!--<tr>
    <td width="23.3333%"><p style="font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #333; line-height: 16px; padding: 0px 0px 0px 30px; text-align:right; margin: 0px; font-weight: 600;">Receiver's Signature</p></td>
    <td width="53.3333%">&nbsp;</td>
    <td width="23.3333%"><p style="font-family: Verdana, Geneva, sans-serif; font-size: 12px;color: #333; line-height: 16px; padding: 0px; margin: 0px; font-weight: 600;">Signature</p></td>
  </tr>-->
  
  
  <tr>
    <td>&nbsp;</td>
	</tr>
  
  
</table>


<div style=" width: 100%; margin-top: 25px;">
  <p style="font-family: Verdana, Geneva, sans-serif; font-size: 11px;color: #333; line-height: 16px; text-align: center; padding: 0px; margin: 0 auto; font-weight: 600; ">This is computer generated invoice and does not require a signature.</p>
</div>   
   
    
    </footer>
    
</div>

</body>
</html>
