<section>
    <div class="row invoice-field-wrapper no-left-right-margin">
        <div class="widget-header">
            <i class="icon-th-list"></i>
            <h3>Create Invoice</h3>
			<a href="<?php echo base_url('invoice/list_customer_invoices'); ?>" class="btn" style="float: right; margin: 5px 15px 5px 0px;">Back</a>
			<!--<a class="btn" style="margin: 5px 15px 5px 5px; float:right;" href="<?php// echo base_url(); ?>invoice/generateinvoice/<?php// echo $invoice_detail[0]->invoice_id; ?>" target="_blank" title="Download Invoice">
				<i class="btn-icon-only fa fa-download "> </i>
			</a>-->
        </div>
        <div class="col-md-12 col-sm-12 invoice-box-main  box-center no-left-right-padding">
            
            <!--<h2 class="text-center">Invoice Details</h2>-->
            <div class="col-md-12 col-sm-12 invoice-box-right-main no-left-padding">
                <div class="col-md-12 col-sm-12 invoice-box-right">
                    <div class="col-md-9 col-sm-9 invoice-logo-box">
                        <div class="invoice-logo"><img src="<?= check_and_get_img_url('./uploads/images/settings/'.$settings->invoice_logo, 'invoice-logo.png'); ?>"></div>
                    </div>
					<div class="col-md-3 col-sm-3 invoice-logo-box">
                        
                    </div>
                    <form name="invoiceaddform" action="<?php echo base_url(); ?>invoice/add_new_invoice" id="invoiceaddform" method="POST">
                        <!--<input type="hidden" value="<?php// echo $jobdetail[$bookings_id]->customer_id; ?>" name="jobhiddencustomerid" id="jobhiddencustomerid" />    -->
                        <div class="col-md-12 col-sm-12 invoice-address-box">
                            <div class="col-md-6 col-sm-12 invoice-address no-left-right-padding">
                                <p><strong><?= $settings->company_name ?></strong><br>
                                     <?= $settings->company_address_lines_html ?>
                                     <br><br>
                                    Tel&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<strong> <?= $settings->company_tel_number ?></strong><br>
                                    Email&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<strong><?= $settings->company_email ?></strong>
                                </p>
                            </div>
                            <div class="col-md-6 col-sm-12 invoice-date no-left-right-padding">
                                <div class="col-md-6 col-sm-12 no-left-padding">  
                                    <div class="text-field-main no-bottom-padding">
                                        <p>Issue date :</p>
                                        <input name="invoiceissuedate" id="invoiceissuedate" class="text-field" type="text" autocomplete="off">
                                    </div><!--text-field-main end-->
                                </div>
                                <div class="col-md-6 col-sm-12 no-right-padding">  
                                    <div class="text-field-main no-bottom-padding">
                                        <p>Due date :</p>
                                        <input name="invoiceduedate" id="invoiceduedate" class="text-field" type="text" autocomplete="off">
                                    </div><!--text-field-main end-->
                                </div>
                                <!--<div class="col-md-12 col-sm-12 no-right-padding">  
                                    <div class="text-field-main">
                                        <p class="text-right">Amount Due : <strong>$0.00</strong></p>
                                    </div><!--text-field-main end-->
                                <!--</div>-->
                            </div>
                            <div class="col-md-12 col-sm-12 invoice-to-address no-left-right-padding">
                                <p><strong>To,</strong></p><br>
								<select style="margin-top: 5px; width:250px; margin-bottom: 9px;" id="customers_vh_rep_new_inv" name="customers_vh_rep_new_inv">
									<option value="">-- Select Customer --</option>
								</select>
                                <p><span id="inv_addrsss"></span><br></p>
                            </div>
                            <div class="col-md-12 col-sm-12 invoice-job-det no-left-right-padding">
                                <p><strong>Job</strong><br>
                                <?php
                                ?>
                                <span></span></p>
                            </div>
                            <div class="col-md-12 col-sm-12 invoice-det-table no-left-right-padding">
                                <div class="Table table-top-style-box no-top-border">
                                    <div class="Heading table-head">
										<div class="Cell">
                                            <p><strong>Sl. No.</strong></p>
                                        </div>
                                        <div class="Cell">
                                            <p><strong>Description</strong></p>
                                        </div>
                                        <!--<div class="Cell">
                                            <p><strong>Amount</strong></p>
                                        </div>
                                        <div class="Cell">
                                            <p><strong>VAT</strong></p>
                                        </div>-->
                                        <div class="Cell">
                                            <p><strong>Net Amount (Includes VAT <?= $settings->service_vat_percentage; ?>%)</strong></p>
                                        </div>
                                    </div>
                                    <?php
									// $i = 1;
                                    // foreach ($invoice_detail as $jobs)
                                    // {
										// $tot_hrs = ((strtotime($jobs->service_to_time) - strtotime($jobs->service_from_time))/ 3600);
                                        
                                    ?>
                                    <div class="Row">
										<div class="Cell">
                                            <p>1</p>
                                        </div>
										<div class="Cell">
                                            <p><input name="invoicedescription" id="invoicedescription" class="text-field" type="text" autocomplete="off"></p>
                                        </div>
                                        <!--<div class="Cell">
                                            <p><input name="invoiceamount" id="invoiceamount" class="text-field" type="number"></p>
                                        </div>
										<div class="Cell">
                                            <p><input name="invoicevatamount" id="invoicevatamount" class="text-field invoicevatamount" type="number"></p>
                                        </div>-->
										<div class="Cell">
                                            <p><input name="invoicenetamount" id="invoicenetamount" class="text-field invoicenetamount" type="number" autocomplete="off"></p>
                                        </div>
                                    </div>
                                    <?php
									//$i++; } ?>
                                    <div class="Row">
										<div class="Cell total-box no-right-border">
                                          &nbsp;
                                        </div>
                                        <div class="Cell total-box no-left-border">
                                          &nbsp;
                                          <span class="total-text text-right"><b class="pull-right">Total &nbsp;&nbsp; </b></span>
                                        </div>
                                        <!--<div class="Cell total-box light-green">
                                           <span class="total-text text-right"><b>AED </b><b class="invoiceamount"></b></span>
                                        </div>
                                        <div class="Cell total-box light-green">
                                            <span class="total-text text-right"><b>AED </b><b class="invoicevatamount"></b></span>
                                        </div>-->
                                        <div class="Cell total-box light-green">
                                            <span class="total-text text-right"><b>AED </b><b class="invoicenetamount"></b></span>
                                        </div>
                                    </div>
                                </div><!--Table table-top-style-box end--> 
                            </div>
                            <div class="col-md-12 col-sm-12 invoice-det-note no-left-right-padding">
                                <p><!--<strong>Note</strong>--><br></p>
                                <!--<span><?php// echo $notes; ?></span>-->
                            </div>
                            <div class="col-md-12 col-sm-12 invoice-button-main no-left-right-padding">
                                <div class="col-md-6 col-sm-12 invoice-button">
                                   &nbsp;
                                </div>
                                <div class="col-md-3 col-sm-6 invoice-button no-left-padding">
                                    <!--<input value="CANCEL" class="text-field-but dark" type="button">-->
                                </div>
                                <div class="col-md-3 col-sm-6 invoice-button no-right-padding">
                                    <input value="SAVE" name="jobinvoicesave" id="jobinvoicesave" class="text-field-but" type="submit">
                                </div>
                            </div>
                        </div>
                    </form>
                </div><!--invoice-box-right end-->
            </div><!--invoice-box-right-main end-->
        </div>
    </div>
</section>