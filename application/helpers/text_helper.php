<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
function color($type)
{
    $colors = array();
    $colors['OD'] = '#8793d6';
    $colors['WE'] = '#7ac255';
    $colors['BW'] = '#fd8e41';
    return $colors[strtoupper($type)] ?: 'red';
}
function booking_type($type)
{
    $CI = get_instance();
    $CI->load->model('settings_model');
    $settings = $CI->settings_model->get_settings();
    $types = array();
    $types['OD'] = $settings->od_booking_text;
    $types['WE'] = $settings->we_booking_text;
    $types['BW'] = $settings->bw_booking_text;
    return $types[strtoupper($type)] ?: 'Invalid Booking Type !';
}
function payment_type($type)
{
    $types      = array();
    $types[0]   = 'Cash';
    $types[1]   = 'Card';
    $types[2]   = 'Cheque';
    $types[3]   = 'Bank';
    $types[4]   = 'Credit Card';
    return $types[$type] ?: 'Unknown !';
}
