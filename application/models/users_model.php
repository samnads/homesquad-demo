<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 /** 
  * Users_model Class 
  * 
  * @package	Emaid
  * @author		Habeeb Rahman 
  * @since		Version 1.0
  */
class Users_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	/** 
	 * Add user
	 * 
	 * @author	Habeeb Rahman
	 * @acces	public 
	 * @param	bool
	 * @return	array
	 */
	function add_user($fields = array())
	{
		$fields['is_admin'] = isset($fields['is_admin']) ? $fields['is_admin'] : 'N';
		$fields['user_status'] = isset($fields['user_status']) ? $fields['user_status'] : 1;
		$fields['added_datetime'] = isset($fields['added_datetime']) ? $fields['added_datetime'] : date('Y-m-d H:i:s');

		$this->db->set($fields);
		$this->db->insert('users'); 
		return $this->db->insert_id();
	}
        
        function add_user_login_detail($fields = array())
	{
            $this->db->set($fields);
            $this->db->insert('user_login_details'); 
            return $this->db->insert_id();
	}
        
	function get_all_modules($active_only = TRUE)
	{
		$this->db->select('sm.module_id, sm.parent_id, sm.module_name AS sub_module_name, m.module_name AS module_name', FALSE)
				->from('modules sm')
                                ->join('modules m', 'm.module_id = sm.parent_id', 'left')
				->order_by('sm.parent_id');
		
		if($active_only)
		{
			$this->db->where('m.module_status', 1);
		}
               
		
		$get_modules_qry = $this->db->get(); 
		
		return $get_modules_qry->result();
	}
	/** 
	 * Get users
	 * 
	 * @author	Habeeb Rahman
	 * @acces	public 
	 * @param	bool
	 * @return	array
	 */
	function get_users($active_only = TRUE)
	{
		$this->db->select('user_id, username, password, user_fullname, permissions, is_admin, delete_permission, user_status, last_loggedin_datetime, added_datetime, last_loggedin_ip')
				->from('users')
				->order_by('added_datetime', 'DESC');
		
		if($active_only)
		{
			$this->db->where('user_status', '1');
		}
		
		$get_users_qry = $this->db->get();
		
		return $get_users_qry->result();
	}
    function get_users_by_status($user_status = 0)
	{
		$this->db->select('user_id, username, password, user_fullname, permissions, is_admin, delete_permission, user_status, last_loggedin_datetime, added_datetime, last_loggedin_ip')
				->from('users')
				->order_by('added_datetime', 'DESC');
		
		if($user_status > -1)
		{
			$this->db->where('user_status', $user_status);
		}
		
		$get_users_qry = $this->db->get();
		
		return $get_users_qry->result();
	}
        
        function get_user_by_id($user_id)
	{
		$this->db->select('user_id, username, password, user_fullname, permissions, is_admin, delete_permission, user_status, login_type')
				 ->from('users')
				 ->where('user_id', $user_id)
				 ->limit(1);
		
		$get_user_by_id_qry = $this->db->get();
		
		return $get_user_by_id_qry->row();
	}
        
        function get_user_login_info($user_id)
	{
            $this->db->select('ud.user_id, u.username,u.user_fullname, ud.login_ip, ud.user_agent, ud.login_date_time')
                    ->from('user_login_details ud')
                    ->join('users u','u.user_id = ud.user_id')
                    ->where('u.user_id', $user_id)
                    ->order_by('ud.login_date_time','desc');

            $get_user_by_id_qry = $this->db->get();

            return $get_user_by_id_qry->result();
	}
        
	function get_permission_by_user($user_id)
        {
//                $this->db->select('m.module_id, m.module_name')
//                         ->from('user_permissions up')
//                         ->join('modules m', 'up.module_id = m.module_id')
//                         ->where('up.user_id', $user_id);
//                
//                $get_permission_by_user_qry = $this->db->get();
//                
//                return $get_permission_by_user_qry->result();
            $this->db->select('up.module_id')
                    ->from('user_permissions up')
                    ->join('modules m', 'up.module_id = m.module_id')
                    ->where('up.user_id', $user_id)
                    ->where('up.permission', 'Y');        

            $get_permissions_qry = $this->db->get();       

            return $get_permissions_qry->result();
        }
	/** 
	 * Get user by username and password
	 * 
	 * @author	Habeeb Rahman
	 * @acces	public 
	 * @param	str, str
	 * @return	array
	 */
	function get_user_by_username_and_password($username, $password)
	{
		$this->db->select('user_id, username, password, user_fullname, permissions, is_admin, user_status, login_type')
				 ->from('users')
				 ->where('username', $username)
				 ->where('password', $password)
				 ->limit(1);
		
		$get_user_by_username_and_password_qry = $this->db->get();
		
		return $get_user_by_username_and_password_qry->row();
	}
		
	/** 
	 * Update user
	 * 
	 * @author	Habeeb Rahman
	 * @acces	public 
	 * @param	int, array
	 * @return	int
	 */
	function update_user($user_id, $fields = array())
	{
		$this->db->where('user_id', $user_id);
		$this->db->update('users', $fields); 

		return $this->db->affected_rows();
	}
	
	/** 
	 * Delete user
	 * 
	 * @author   Habeeb Rahman
	 * @acces    public 
	 * @param    int
	 * @return   int
	 */
	function delete_user($user_id)
	{
		$this->db->where('user_id', $user_id);
		$this->db->delete('users'); 

		return $this->db->affected_rows();
	}
        function delete_permissions_by_user_id($user_id)
        {
            $this->db->where('user_id', $user_id);
            $this->db->delete('user_permissions'); 

            return $this->db->affected_rows();
        }
        function check_password($user_id, $password)
        {
            $this->db->select('user_id')
                ->from('users')
                ->where('user_id', $user_id)
                ->where('password', $password)
                ->limit(1);

            $check_password_qry = $this->db->get();

            if ($check_password_qry->num_rows() > 0) 
            {
                return TRUE;
            } else {
                return FALSE;
            }
    }
    function get_modules($active = TRUE)
    {
        $this->db->select('module_id, module_name, parent_id')
                ->from('modules')
                ->order_by('module_id,parent_id');
        if($active)
        {
            $this->db->where('module_status', 1);
        }
        
        $get_modules_qry = $this->db->get();
        
        return $get_modules_qry->result();
    }
    function get_module_id_by_name($module_name, $parent_id = 0)
    {
        $this->db->select('module_id')
                ->from('modules')
                ->where('module_name', $module_name)
                ->where('parent_id', $parent_id)
                ->where('module_status', 1);
        
        $get_module_id_by_name_qry = $this->db->get();
        
        return $get_module_id_by_name_qry->row();
    }
    function get_permissions($module_id, $user_id, $method_id)
    {
        $this->db->select('up.permission')
                ->from('user_permissions up')
                ->join('modules m', 'up.module_id = m.module_id')
                ->where('up.user_id', $user_id);
        if($method_id == 0){
            $this->db->where('up.module_id', $module_id)
                    ->where('m.parent_id', $method_id);  
        }else{
               $this->db->where('m.parent_id', $module_id)
                       ->where('up.module_id', $method_id);        
        }
        $get_permissions_qry = $this->db->get(); 
        //echo $this->db->last_query();
        return $get_permissions_qry->row();
        
    }
    function get_module_by_user_id($user_id)
    {
        $this->db->select('up.module_id')
                ->from('user_permissions up')
                ->join('modules m', 'up.module_id = m.module_id')
                ->where('up.user_id', $user_id)
                ->where('up.permission', 'Y');        
        
        $get_permissions_qry = $this->db->get();       
        
        return $get_permissions_qry->result();
        
    }
    
    function add_permission( $fields, $user_id)
    {
        
        $this->db->where('user_id', $user_id);
        $this->db->delete('user_permissions');
        
        $this->db->insert_batch('user_permissions', $fields); 
        
        return $this->db->affected_rows();
    }
    function check_user_permission($user_id, $module_id)
    {
        $this->db->select('user_permission_id')
                ->from('user_permissions')
                ->where('user_id', $user_id)
                ->where('module_id', $module_id)
                ->limit(1);
        
        $check_user_permission_qry = $this->db->get();
        
        return $check_user_permission_qry->row();
    }
    function get_ci_session($session_id)
    {
        $this->db->select('session_id')
                ->from('ci_sessions')
                ->where('session_id', $session_id);

        $get_ci_session_qry = $this->db->get();

        return $get_ci_session_qry->num_rows();
    }
    function add_sessions($fields = array())
    {
            $session_id = $fields['session_id'];
            if($this->get_ci_session($session_id) > 0)
            {
                $update_fields = array();
                $update_fields['last_activity'] = $fields['last_activity'];
                $update_fields['user_data'] = $fields['user_data'];

                $this->update_sessions($session_id, $update_fields);
            }
            else
            {
                $this->db->set($fields);
                $this->db->insert('ci_sessions'); 
            }


    }
    function update_sessions($session_id, $fields = array())
    {
            $this->db->where('session_id', $session_id);
            $this->db->update('ci_sessions', $fields); 

            return $this->db->affected_rows();
    }
    function get_user_activity_by_date($date, $date_to = NULL)
    {
        
        $this->db->select('u.user_fullname, a.action_type, a.booking_type, a.shift, a.action_content, DATE_FORMAT(a.addeddate, "%d/%m/%Y %H:%i:%s") AS added', FALSE)
                ->from('user_activity a')
                ->join('users u', 'a.added_user = u.user_id')                
                ->order_by('a.addeddate', 'desc');
        if($date_to != NULL)
        {
            $this->db->where("(DATE(a.addeddate) BETWEEN  '$date' AND '$date_to')", NULL, FALSE);
        }
        else
        {
            $this->db->where('DATE(a.addeddate)', $date);
        }
        $get_user_activity_qry = $this->db->get();
        
        return $get_user_activity_qry->result();
    }
	
	public function count_all_activity()
	{
		$this->db->select('activity_id')
                ->from('user_activity');
        $num_results = $this->db->count_all_results();
        return $num_results;
	}
	
	public function get_all_activitynew($regdate=NULL,$regdateto=NULL,$keywordsearch=NULL)
    {
        $this->db->select('activity_id')
                ->from('user_activity');
                if($keywordsearch != "")
                {
                    $this->db->where('user_activity.action_content like"%'.$keywordsearch.'%" OR user_activity.action_type like"%'.$keywordsearch.'%" OR user_activity.booking_type like"%'.$keywordsearch.'%"');
                }
       
		if ($regdate && $regdateto) {
			if($regdate != "")
			{
				$regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
			}
			if($regdateto != "")
			{
				$regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
			}
            $this->db->where("DATE(`addeddate`) BETWEEN '$regdate' AND '$regdateto'");
        } 
		if(($regdate!="" && $regdateto=="" ) || ($regdate=="" && $regdateto!="" ) ) {
			if($regdate != "")
			{
				$regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
				$this->db->where("DATE(`addeddate`) = '$regdate'");
			}
			if($regdateto != "")
			{
				$regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
				$this->db->where("DATE(`addeddate`) = '$regdateto'");
			}
        }
        $num_results = $this->db->count_all_results();
        return $num_results;
    }
	
	public function get_all_newactivity($columnName=NULL, $columnSortOrder=NULL, $rowperpage=NULL, $start=NULL, $draw=NULL, $recordsTotal=NULL, $recordsTotalFilter=NULL,$regdate=NULL,$regdateto=NULL,$keywordsearch=NULL)
    {
        $response = array();
        ## Fetch records
		$this->db->select('u.user_fullname, a.action_type, a.booking_type, a.shift, a.action_content, DATE_FORMAT(a.addeddate, "%d/%m/%Y %H:%i:%s") AS added', FALSE)
                ->from('user_activity a')
				->join('users u', 'a.added_user = u.user_id');
        $this->db->order_by('a.addeddate', 'DESC');
        if($keywordsearch != "")
        {
            $this->db->where('(a.action_content like"%'.$keywordsearch.'%" OR a.action_type like"%'.$keywordsearch.'%" OR a.booking_type like"%'.$keywordsearch.'%")');
        }
		if ($regdate && $regdateto) {
			if($regdate != "")
			{
				$regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
			}
			if($regdateto != "")
			{
				$regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
			}
            $this->db->where("DATE(`addeddate`) BETWEEN '$regdate' AND '$regdateto'");
        } if(($regdate!="" && $regdateto=="" ) || ($regdate=="" && $regdateto!="" ) ) {
			if($regdate != "")
			{
				$regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
				$this->db->where("DATE(`addeddate`) >= '$regdate'");
			}
			if($regdateto != "")
			{
				$regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
				$this->db->where("DATE(`addeddate`) <= '$regdateto'");
			}
        }
		if($columnName != '' && $columnSortOrder !="")
        {
			$this->db->order_by($columnName, $columnSortOrder);
		}
        $this->db->limit($rowperpage, $start);
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $records = $query->result();
        $data = array();
        $i = 1;
        foreach($records as $record )
        {
			$action_type = $record->action_type;
			switch ($record->action_type)
			{
				case 'Add' : $action_type = 'New Booking';
					break;
				case 'Update' : $action_type = 'Booking Update';
					break;
				case 'Delete' : $action_type = 'Booking Delete';
					break;
			}
			
			$data[] = array( 
				'slno'=>($i + $start),
				'type'=>str_replace("_",' ',$action_type),
				'bookingtype'=>$record->booking_type,
				'shift'=>$record->shift,
				'action'=>$record->action_content,
				'doneby'=>$record->user_fullname,
				'time'=>$record->added,
            ); 
            $i++;
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $recordsTotal,
            "lastcall" => $records[0],
            "iTotalDisplayRecords" => $recordsTotalFilter,
            "aaData" => $data
        );

        return $response; 
    }
	
    function get_user_activity_old()
    {
        
        $this->db->select('user_activity.*,users.user_fullname', FALSE)
                ->from('user_activity')
                ->join('users','user_activity.added_user = users.user_id','left')
                ->order_by('user_activity.addeddate', 'desc')
                ->limit(25);
        $get_user_activity_qry = $this->db->get();
        
        return $get_user_activity_qry->result();
    }
	function get_user_activity()
    {
        
        $this->db->select('user_activity.action_type,user_activity.action_content,user_activity.addeddate,users.user_fullname', FALSE)
                ->from('user_activity')
                ->join('users','user_activity.added_user = users.user_id','left')
                ->order_by('user_activity.addeddate', 'desc')
                ->limit(25);
        $get_user_activity_qry = $this->db->get();
        
        return $get_user_activity_qry->result();
    }
    function delete_module($module_id)
    {
            $this->db->where('module_id', $module_id);
            $this->db->delete('modules'); 

            $affected = $this->db->affected_rows();
            $this->db->where('module_id', $module_id);
            $this->db->delete('user_permissions');

            return $affected;
    }
    
    function check_passwords($user_id, $password)
    {
        $this->db->select('user_id')
                ->from('users')
            ->where('user_id', $user_id)
            ->where('password', $password)
            ->limit(1);

        $check_password_qry = $this->db->get();

        if ($check_password_qry->num_rows() > 0) 
        {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    function update_users($user_id, $fields = array())
    {
        $this->db->where('user_id', $user_id);
        $this->db->update('users', $fields); 

        return $this->db->affected_rows();
    }
    
    function get_user_by_user_name($username)
    {
        $this->db->select('user_id, username, password, user_fullname, permissions, is_admin, delete_permission, user_status')
                ->from('users')
                ->where('username', $username)				 
                ->limit(1);

        $get_user_by_username_and_password_qry = $this->db->get();

        return $get_user_by_username_and_password_qry->row();
    }

    public function get_all_activitynew_new($regdate=NULL,$regdateto=NULL)
    {
        $this->db->select('activity_id')
                ->from('user_activity');
       
		if ($regdate && $regdateto) {
			if($regdate != "")
			{
				$regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
			}
			if($regdateto != "")
			{
				$regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
			}
            $this->db->where("DATE(`addeddate`) BETWEEN '$regdate' AND '$regdateto'");
        } 
		if(($regdate!="" && $regdateto=="" ) || ($regdate=="" && $regdateto!="" ) ) {
			if($regdate != "")
			{
				$regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
				$this->db->where("DATE(`addeddate`) = '$regdate'");
			}
			if($regdateto != "")
			{
				$regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
				$this->db->where("DATE(`addeddate`) = '$regdateto'");
			}
        }
        $num_results = $this->db->count_all_results();
        return $num_results;
    }
	
	function check_request_exist($userid,$booking_id,$service_date,$reqtype)
	{
		$this->db->select('*')
                ->from('replace_delete_requests')
                ->where('booking_id', $booking_id)				 
                ->where('service_date', $service_date)				 
                ->where('requested_by', $userid)				 
                ->where('request_type', $reqtype)				 
                ->limit(1);

        $get_user_by_username_and_password_qry = $this->db->get();

        return $get_user_by_username_and_password_qry->row();
	}
	
	function add_replace_request($fields = array())
	{
		$this->db->set($fields);
		$this->db->insert('replace_delete_requests'); 
		return $this->db->insert_id();
	}
	
	function check_request_exist_new($userid,$booking_id,$service_date)
	{
		$this->db->select('*')
                ->from('replace_delete_requests')
                ->where('booking_id', $booking_id)				 
                ->where('service_date', $service_date)				 
                ->where('requested_by', $userid)
				->order_by('request_id', 'desc');

        $get_user_by_username_and_password_qry = $this->db->get();

        return $get_user_by_username_and_password_qry->result();
	}
	
	function get_all_replace_delete_requests($date,$date_to)
	{
		$this->db->select('rdr.request_id,rdr.service_date,rdr.request_type,rdr.request_reason,rdr.request_status,m.maid_name,c.customer_name,u.user_fullname,DATE_FORMAT(b.time_from, "%h:%i %p") AS time_from, DATE_FORMAT(b.time_to, "%h:%i %p") AS time_to', FALSE)
			->from('replace_delete_requests rdr')
			->join('bookings b', 'rdr.booking_id = b.booking_id')
			->join('maids m', 'b.maid_id = m.maid_id')
			->join('users u', 'rdr.requested_by = u.user_id')
			->join('customers c', 'b.customer_id = c.customer_id')
			->where("DATE(rdr.request_added_date) BETWEEN '$date' AND '$date_to'")
			->order_by('rdr.request_id', 'desc');

		$user_query = $this->db->get();

		return $user_query->result();
	}
	
	public function count_all_requestapproval()
	{
		$this->db->select('request_id')
                ->from('replace_delete_requests');
		$user_query = $this->db->get();
        return $user_query->result();
	}
	
	public function get_all_requestapproval($useractive=NULL,$regdate=NULL,$regdateto=NULL,$keywordsearch=NULL)
    {
        $this->db->select('rdr.request_id')
                ->from('replace_delete_requests rdr')
				->join('bookings b', 'rdr.booking_id = b.booking_id')
				->join('maids m', 'b.maid_id = m.maid_id')
				->join('users u', 'rdr.requested_by = u.user_id')
				->join('users up', 'rdr.approvedBy = up.user_id','LEFT')
				->join('customers c', 'b.customer_id = c.customer_id');
        if($useractive != '')
        {
            $this->db->where('rdr.request_status',$useractive);
        }
		if($keywordsearch != "")
		{
			$this->db->where('c.customer_name like"%'.$keywordsearch.'%" OR c.mobile_number_1 like"%'.$keywordsearch.'%" OR c.email_address like"%'.$keywordsearch.'%" OR m.maid_name like"%'.$keywordsearch.'%" OR u.user_fullname like"%'.$keywordsearch.'%" OR up.user_fullname like"%'.$keywordsearch.'%"');
		}
		if ($regdate && $regdateto) {
			if($regdate != "")
			{
				$regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
			}
			if($regdateto != "")
			{
				$regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
			}
            $this->db->where("DATE(`rdr.request_added_date`) BETWEEN '$regdate' AND '$regdateto'");
        } 
		if(($regdate!="" && $regdateto=="" ) || ($regdate=="" && $regdateto!="" ) ) {
			if($regdate != "")
			{
				$regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
				$this->db->where("DATE(`rdr.request_added_date`) = '$regdate'");
			}
			if($regdateto != "")
			{
				$regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
				$this->db->where("DATE(`rdr.request_added_date`) = '$regdateto'");
			}
        }
        $user_query = $this->db->get();
		// echo $this->db->last_query(); exit();
        return $user_query->result();
    }
	
	public function get_all_newrequestapproval($useractive=Null, $rowperpage=NULL, $start=NULL, $draw=NULL, $recordsTotal=NULL, $recordsTotalFilter=NULL,$regdate=NULL,$regdateto=NULL,$keywordsearch=NULL,$user_id=NULL)
    {
        $response = array();

        $this->db->select('rdr.request_id,rdr.service_date,rdr.request_type,rdr.request_reason,rdr.request_status,m.maid_name,c.customer_name,u.user_fullname,up.user_fullname as approvedBy,DATE_FORMAT(b.time_from, "%h:%i %p") AS time_from, DATE_FORMAT(b.time_to, "%h:%i %p") AS time_to', FALSE)
            ->from('replace_delete_requests rdr')
			->join('bookings b', 'rdr.booking_id = b.booking_id')
			->join('maids m', 'b.maid_id = m.maid_id')
			->join('users u', 'rdr.requested_by = u.user_id')
			->join('users up', 'rdr.approvedBy = up.user_id','LEFT')
			->join('customers c', 'b.customer_id = c.customer_id')
            ->order_by('rdr.request_id', 'DESC');

        if($useractive != '')
        {
            $this->db->where('rdr.request_status',$useractive);
        }
		if($keywordsearch != "")
		{
			$this->db->where('c.customer_name like"%'.$keywordsearch.'%" OR c.mobile_number_1 like"%'.$keywordsearch.'%" OR c.email_address like"%'.$keywordsearch.'%" OR m.maid_name like"%'.$keywordsearch.'%" OR u.user_fullname like"%'.$keywordsearch.'%" OR up.user_fullname like"%'.$keywordsearch.'%"');
		}
		if ($regdate && $regdateto) {
			if($regdate != "")
			{
				$regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
			}
			if($regdateto != "")
			{
				$regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
			}
            $this->db->where("DATE(`rdr.request_added_date`) BETWEEN '$regdate' AND '$regdateto'");
        } 
		if(($regdate!="" && $regdateto=="" ) || ($regdate=="" && $regdateto!="" ) ) {
			if($regdate != "")
			{
				$regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
				$this->db->where("DATE(`rdr.request_added_date`) = '$regdate'");
			}
			if($regdateto != "")
			{
				$regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
				$this->db->where("DATE(`rdr.request_added_date`) = '$regdateto'");
			}
        }
        $this->db->limit($rowperpage, $start);
        $query = $this->db->get();
        $records = $query->result();
        $data = array();
        $i = 1;

        foreach($records as $record )
        {
			if($record->request_status == 0)
			{
				$status = "Pending";
				$approve = '<a title="Approve" href="javascript:void(0)" data-bind="0" style="text-decoration:none; display:inline; float: left; padding-right: 5px;" onclick="approve_request('.$record->request_id.','.$user_id.');"><span class="finished"></span></a>';
				$reject = '<a title="Reject" href="javascript:void(0)" data-bind="0" style="text-decoration:none; display:inline; float: left;" onclick="reject_request('.$record->request_id.','.$user_id.');"><span class="cancelled"></span></a>';
			} else if($record->request_status == 1)
			{
				$status = "Approved";
				$approve = '';
				$reject = '';
			} else if($record->request_status == 2)
			{
				$status = "Rejected";
				$approve = '';
				$reject = '';
			} else if($record->request_status == 3)
			{
				$status = "Cancelled";
				$approve = '';
				$reject = '';
			}
			
			$data[] = array( 
				'slno'=>($i + $start),
				'name'=>$record->customer_name,
				'servicedate'=>($record->service_date) ? date("d/m/Y", strtotime($record->service_date)) : "",
				'shift'=>$record->time_from . '-' . $record->time_to,
				'maidname'=>$record->maid_name,
				'reason'=>$record->request_reason,
				'requestedby'=>$record->user_fullname,
				'approvedby'=>$record->approvedBy,
				'status'=>$status,
				'action'=>$approve.$reject,
            ); 
            $i++;
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $recordsTotal,
            "lastcall" => $records[0],
            "iTotalDisplayRecords" => $recordsTotalFilter,
            "aaData" => $data
        );

        return $response; 
    }
	
	public function get_all_newrequests_excel($regdate=NULL,$regdateto=NULL,$useractive=Null,$keywordsearch=NULL)
    {
        $response = array();

        $this->db->select('rdr.request_id,rdr.service_date,rdr.request_type,rdr.request_reason,rdr.request_status,m.maid_name,c.customer_name,u.user_fullname,up.user_fullname as approvedBy,DATE_FORMAT(b.time_from, "%h:%i %p") AS time_from, DATE_FORMAT(b.time_to, "%h:%i %p") AS time_to', FALSE)
            ->from('replace_delete_requests rdr')
			->join('bookings b', 'rdr.booking_id = b.booking_id')
			->join('maids m', 'b.maid_id = m.maid_id')
			->join('users u', 'rdr.requested_by = u.user_id')
			->join('users up', 'rdr.approvedBy = up.user_id','LEFT')
			->join('customers c', 'b.customer_id = c.customer_id')
            ->order_by('rdr.request_id', 'DESC');

        if($useractive != '')
        {
            $this->db->where('rdr.request_status',$useractive);
        }
		if($keywordsearch != "")
		{
			$this->db->where('c.customer_name like"%'.$keywordsearch.'%" OR c.mobile_number_1 like"%'.$keywordsearch.'%" OR c.email_address like"%'.$keywordsearch.'%" OR m.maid_name like"%'.$keywordsearch.'%" OR u.user_fullname like"%'.$keywordsearch.'%" OR up.user_fullname like"%'.$keywordsearch.'%"');
		}
		if ($regdate && $regdateto) {
			if($regdate != "")
			{
				$regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
			}
			if($regdateto != "")
			{
				$regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
			}
            $this->db->where("DATE(`rdr.request_added_date`) BETWEEN '$regdate' AND '$regdateto'");
        } 
		if(($regdate!="" && $regdateto=="" ) || ($regdate=="" && $regdateto!="" ) ) {
			if($regdate != "")
			{
				$regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
				$this->db->where("DATE(`rdr.request_added_date`) = '$regdate'");
			}
			if($regdateto != "")
			{
				$regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
				$this->db->where("DATE(`rdr.request_added_date`) = '$regdateto'");
			}
        }
        $query = $this->db->get();
        $records = $query->result();
        return $records;
    }
	
	function replace_request_update($request_id, $fields = array())
	{
		$this->db->where('request_id', $request_id);
		$this->db->update('replace_delete_requests', $fields);

		return $this->db->affected_rows();
	}
    
   
}