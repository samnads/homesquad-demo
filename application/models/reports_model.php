<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reports_model extends CI_Model {

    function get_all_maids() {

        $this->db->select('maids.*, flats.flat_name')
                ->from('maids')
                ->join('flats', 'maids.flat_id = flats.flat_id', 'left')
                ->where('maid_status', 1)
                ->order_by('maids.maid_name');

        $query = $this->db->get();
        return $query->result_array();
    }

    function get_zone_reports($service_date, $zone) {
        $service_week_day = date('w', strtotime($service_date));

        $deleted_bookings = array();
        $deletes = $this->get_booking_deletes_by_date($service_date);
        if (!empty($deletes)) {
            foreach ($deletes as $delete) {
                $deleted_bookings[] = $delete->booking_id;
            }
        }
//        $deletes = $this->get_booking_service_not_done_by_date($service_date);
//        if (!empty($deletes)) {
//            foreach ($deletes as $delete) {
//                $deleted_bookings[] = $delete->booking_id;
//            }
//        }





        $this->db->select('bookings.maid_id, bookings.booking_id, bookings.booking_type, customers.customer_id, customers.customer_name,customers.payment_type,customers.payment_type,customers.price_hourly, maids.maid_name, DAYNAME(bookings.service_start_date) as day, areas.area_name, zones.zone_name, 

bookings.time_from AS time_from, 
bookings.time_to AS time_to')
                ->from('bookings')
                ->join('customers', 'bookings.customer_id = customers.customer_id')
                //->join('customer_addresses', 'customers.customer_id = customer_addresses.customer_id')
                ->join('customer_addresses', 'bookings.customer_address_id = customer_addresses.customer_address_id')
                ->join('maids', 'maids.maid_id = bookings.maid_id')
                ->join('areas', 'customer_addresses.area_id = areas.area_id')
                ->join('zones', 'areas.zone_id = zones.zone_id')
				->where('bookings.booking_category', 'C')
                ->where('bookings.booking_status', 1);
                //->where('maids.maid_status', 1);

		if($service_date >= date('Y-m-d'))
		{
			$this->db->where('maids.maid_status', 1);
		}
		
        if (!empty($deleted_bookings)) {
            $this->db->where_not_in('bookings.booking_id', $deleted_bookings);
        }

        if ($zone != "") {
            $this->db->where('zones.zone_id', $zone);
        }
        if ($service_date != "") {
            //$this->db->where("((bookings.service_start_date = " . $this->db->escape($service_date) . " AND bookings.booking_type = 'OD') OR (bookings.service_start_date <= " . $this->db->escape($service_date) . " AND bookings.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1 AND service_week_day = " . $service_week_day . " AND bookings.booking_type = 'WE') OR (bookings.service_start_date <= " . $this->db->escape($service_date) . " AND service_end = 0 AND service_week_day = " . $service_week_day . " AND bookings.booking_type = 'WE'))", NULL, FALSE);
            $this->db->where("((bookings.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0 AND bookings.service_start_date <= " . $this->db->escape($service_date) . "))", NULL, FALSE);
            $this->db->where("((bookings.service_start_date = " . $this->db->escape($service_date) . " AND bookings.booking_type = 'OD') OR (bookings.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND bookings.booking_type = 'WE') OR (bookings.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND bookings.booking_type = 'BW'))", NULL, FALSE);
        }


        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        return $query->result();
    }
    
    
    function get_maidzone_reports($service_date, $starttime, $zone_val) {
        $service_week_day = date('w', strtotime($service_date));

        $deleted_bookings = array();
        $deletes = $this->get_booking_deletes_by_date($service_date);
        if (!empty($deletes)) {
            foreach ($deletes as $delete) {
                $deleted_bookings[] = $delete->booking_id;
            }
        }
        $this->db->select('bookings.maid_id,zones.zone_id')
                ->from('bookings')
                ->join('customers', 'bookings.customer_id = customers.customer_id')
                //->join('customer_addresses', 'customers.customer_id = customer_addresses.customer_id')
                ->join('customer_addresses', 'bookings.customer_address_id = customer_addresses.customer_address_id')
                ->join('maids', 'maids.maid_id = bookings.maid_id')
                ->join('areas', 'customer_addresses.area_id = areas.area_id')
                ->join('zones', 'areas.zone_id = zones.zone_id')
                ->where('bookings.time_from', $starttime)
                ->where('zones.zone_id', $zone_val)
				->where('bookings.booking_category', 'C')
                ->where('bookings.booking_status', 1)
                ->where('maids.maid_status', 1);
        if (!empty($deleted_bookings)) {
            $this->db->where_not_in('bookings.booking_id', $deleted_bookings);
        }
        if ($service_date != "") {
            //$this->db->where("((bookings.service_start_date = " . $this->db->escape($service_date) . " AND bookings.booking_type = 'OD') OR (bookings.service_start_date <= " . $this->db->escape($service_date) . " AND bookings.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1 AND service_week_day = " . $service_week_day . " AND bookings.booking_type = 'WE') OR (bookings.service_start_date <= " . $this->db->escape($service_date) . " AND service_end = 0 AND service_week_day = " . $service_week_day . " AND bookings.booking_type = 'WE'))", NULL, FALSE);
            $this->db->where("((bookings.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0 AND bookings.service_start_date <= " . $this->db->escape($service_date) . "))", NULL, FALSE);
            $this->db->where("((bookings.service_start_date = " . $this->db->escape($service_date) . " AND bookings.booking_type = 'OD') OR (bookings.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND bookings.booking_type = 'WE') OR (bookings.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND bookings.booking_type = 'BW'))", NULL, FALSE);
        }


        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        return $query->num_rows();
        //return $query->result();
    }

    function get_maids($service_date, $zone) {
        $service_week_day = date('w', strtotime($service_date));

        $deleted_bookings = array();
        $deletes = $this->get_booking_deletes_by_date($service_date);
        if (!empty($deletes)) {
            foreach ($deletes as $delete) {
                $deleted_bookings[] = $delete->booking_id;
            }
        }


        $this->db->select('bookings.maid_id, maids.maid_name')
                ->from('bookings')
                ->join('customers', 'bookings.customer_id = customers.customer_id')
                //->join('customer_addresses', 'customers.customer_id = customer_addresses.customer_id')
                ->join('customer_addresses', 'bookings.customer_address_id = customer_addresses.customer_address_id')
                ->join('maids', 'maids.maid_id = bookings.maid_id')
                ->join('areas', 'customer_addresses.area_id = areas.area_id')
                ->join('zones', 'areas.zone_id = zones.zone_id')
				->where('bookings.booking_category', 'C')
                ->where('bookings.booking_status', 1);
                //->where('maids.maid_status', 1);

		
        if (!empty($deleted_bookings)) {
            $this->db->where_not_in('bookings.booking_id', $deleted_bookings);
        }
		
		if($service_date >= date('Y-m-d'))
		{
			$this->db->where('maids.maid_status', 1);
		}

        if ($zone != "") {
            $this->db->where('zones.zone_id', $zone);
        }
        if ($service_date != "") {
            $this->db->where("((bookings.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE);
            $this->db->where("((bookings.service_start_date = " . $this->db->escape($service_date) . " AND bookings.booking_type = 'OD') OR (bookings.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND bookings.booking_type = 'WE') OR (bookings.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND bookings.booking_type = 'BW'))", NULL, FALSE);
        }
        $this->db->group_by('maids.maid_name');
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

    function get_maids_report($service_date, $zone) {
        $service_week_day = date('w', strtotime($service_date));

        $this->db->select('bookings.maid_id, maids.maid_name')
                ->from('bookings')
                ->join('day_services', 'day_services.booking_id = bookings.booking_id')
                ->join('customer_addresses', 'bookings.customer_address_id = customer_addresses.customer_address_id', 'left')
                ->join('maids', 'maids.maid_id = bookings.maid_id')
                ->join('areas', 'customer_addresses.area_id = areas.area_id', 'left')
                ->join('zones', 'areas.zone_id = zones.zone_id', 'left')
				->where('bookings.booking_category', 'C')
                ->where('day_services.service_status', 2);


        if ($zone != "") {

            $this->db->where('zones.zone_id', $zone);
        }



        if ($service_date != "") {

            $this->db->where('day_services.service_date', $service_date);
        }



        $this->db->group_by('maids.maid_name');

        $query = $this->db->get();

        return $query->result_array();
    }

    function get_booking_deletes_by_date($service_date) {
        $this->db->select('booking_id')
                ->from('booking_deletes')
                ->where('service_date', $service_date);

        $get_booking_deletes_by_date_qry = $this->db->get();

        return $get_booking_deletes_by_date_qry->result();
    }

    function get_booking_service_not_done_by_date($service_date) {
        $this->db->select('booking_id')
                ->from('day_services')
                ->where('service_date', $service_date)
                ->where('service_status', 3);

        $get_booking_service_not_done_by_date_qry = $this->db->get();

        return $get_booking_service_not_done_by_date_qry->result();
    }

    function get_zone_name($zone) {
        $this->db->select('zone_name')->from('zones')->where('zone_id', $zone);
        $query = $this->db->get();
        return $query->row()->zone_name;
    }

    function get_vehicle_reports($service_date, $zone) {

        $service_week_day = date('w', strtotime($service_date));

        $deleted_bookings = array();
        $deletes = $this->get_booking_deletes_by_date($service_date);
        if (!empty($deletes)) {
            foreach ($deletes as $delete) {
                $deleted_bookings[] = $delete->booking_id;
            }
        }



        $this->db->select('bookings.maid_id, bookings.pay_by, bookings.justmop_reference, bookings.cleaning_material, bookings.booking_note, bookings.booking_id, bookings.booking_type, bookings.total_amount, bookings.booked_from, bookings.price_per_hr, bookings.discount, customers.price_hourly, customers.customer_source, customers.price_extra, customers.price_weekend, customers.customer_id, customers.customer_name, customers.payment_mode, customers.payment_type, customer_addresses.customer_address, customer_addresses.building, customer_addresses.unit_no, customer_addresses.street, maids.maid_name, DAYNAME(bookings.service_start_date) as day, areas.area_name, zones.zone_name, zones.driver_name, 

bookings.time_from AS time_from, 
bookings.time_to AS time_to,service_types.service_type_name')
                ->from('bookings')
                ->join('customers', 'bookings.customer_id = customers.customer_id')
                ->join('customer_addresses', 'bookings.customer_address_id = customer_addresses.customer_address_id')
                ->join('maids', 'maids.maid_id = bookings.maid_id')
                ->join('areas', 'customer_addresses.area_id = areas.area_id')
                ->join('zones', 'areas.zone_id = zones.zone_id')
                ->join('service_types', 'bookings.service_type_id = service_types.service_type_id','left')
		->where('bookings.booking_category', 'C')
                ->where('bookings.booking_status', 1)
                ->where('maids.maid_status', 1);


        if (!empty($deleted_bookings)) {
            $this->db->where_not_in('bookings.booking_id', $deleted_bookings);
        }

        if ($zone != "") {
            $this->db->where('zones.zone_id', $zone);
        }
        if ($service_date != "") {
            $this->db->where("((bookings.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE);
            $this->db->where("((bookings.service_start_date = " . $this->db->escape($service_date) . " AND bookings.booking_type = 'OD') OR (bookings.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND bookings.booking_type = 'WE') OR (bookings.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND bookings.booking_type = 'BW'))", NULL, FALSE);
        }
        $this->db->order_by('maids.maid_name');

        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result();
    }
    
    
    function get_vehicle_reports_new($service_date, $zone,$customer_id,$company_name,$filter_maid_id) { 

        $service_week_day = date('w', strtotime($service_date));

        $deleted_bookings = array();
        $deletes = $this->get_booking_deletes_by_date($service_date);
        if (!empty($deletes)) {
            foreach ($deletes as $delete) {
                $deleted_bookings[] = $delete->booking_id;
            }
        }
        $s_date=date("d/m/Y", strtotime($service_date));


        $this->db->select("bookings.maid_id, bookings.pay_by, bookings.justmop_reference, bookings.cleaning_material, bookings.booking_note, bookings.booking_id, bookings.booking_type, bookings.total_amount, bookings.booked_from, bookings.price_per_hr, bookings.discount, customers.mobile_number_1,customers.price_hourly, customers.customer_source, customers.price_extra, customers.price_weekend, customers.customer_id, customers.customer_name, customers.payment_mode, customers.payment_type, customer_addresses.customer_address, customer_addresses.building, customer_addresses.unit_no, customer_addresses.street, maids.maid_name, DAYNAME(bookings.service_start_date) as day, areas.area_name, zones.zone_name, zones.driver_name, 

bookings.time_from AS time_from, 
bookings.time_to AS time_to,service_types.service_type_name,'$s_date' as start_date", FALSE)
                ->from('bookings')
                ->join('customers', 'bookings.customer_id = customers.customer_id')
                ->join('customer_addresses', 'bookings.customer_address_id = customer_addresses.customer_address_id')
                ->join('maids', 'maids.maid_id = bookings.maid_id')
                ->join('areas', 'customer_addresses.area_id = areas.area_id')
                ->join('zones', 'areas.zone_id = zones.zone_id')
                ->join('service_types', 'bookings.service_type_id = service_types.service_type_id','left')
		->where('bookings.booking_category', 'C')
                ->where('bookings.booking_status', 1)
                ->where('maids.maid_status', 1);


        if (!empty($deleted_bookings)) {
            $this->db->where_not_in('bookings.booking_id', $deleted_bookings);
        }

        if ($zone != "") {
            $this->db->where('zones.zone_id', $zone);
        }
        if ($customer_id>0) {
            $this->db->where('bookings.customer_id', $customer_id);
        }
        
        if ($service_date != "") {
            $this->db->where("((bookings.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE);
            $this->db->where("((bookings.service_start_date = " . $this->db->escape($service_date) . " AND bookings.booking_type = 'OD') OR (bookings.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND bookings.booking_type = 'WE') OR (bookings.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND bookings.booking_type = 'BW'))", NULL, FALSE);
        }
		
		if ($company_name != "") {
            
			$this->db->where('customers.company_name', $company_name);
			$this->db->where('customers.is_company', 'Y');
			
        }
        if($filter_maid_id != NULL)
		{
			$this->db->where('maids.maid_id', $filter_maid_id);
		}
		
        $this->db->order_by('maids.maid_name');

        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result();
    }
	
	function get_vehicle_reports_new_test($service_date, $zone,$customer_id,$company_name,$filter_maid_id) {

        $service_week_day = date('w', strtotime($service_date));

        $deleted_bookings = array();
        $deletes = $this->get_booking_deletes_by_date($service_date);
        if (!empty($deletes)) {
            foreach ($deletes as $delete) {
                $deleted_bookings[] = $delete->booking_id;
            }
        }
        $s_date=date("d/m/Y", strtotime($service_date));


        $this->db->select("bookings.maid_id, bookings.pay_by, bookings.justmop_reference, bookings.cleaning_material, bookings.booking_note, bookings.booking_id, bookings.booking_type, bookings.total_amount, bookings.booked_from, bookings.price_per_hr, bookings.discount, customers.mobile_number_1,customers.price_hourly, customers.customer_source, customers.price_extra, customers.price_weekend, customers.customer_id, customers.customer_name, customers.payment_mode, customers.payment_type, customer_addresses.customer_address, customer_addresses.building, customer_addresses.unit_no, customer_addresses.street, maids.maid_name, DAYNAME(bookings.service_start_date) as day, areas.area_name, zones.zone_name, zones.driver_name, 

bookings.time_from AS time_from, 
bookings.time_to AS time_to,service_types.service_type_name,'$s_date' as start_date", FALSE)
                ->from('day_services')
				->join('bookings', 'day_services.booking_id = bookings.booking_id')
                ->join('customers', 'day_services.customer_id = customers.customer_id')
                ->join('customer_addresses', 'bookings.customer_address_id = customer_addresses.customer_address_id')
                ->join('maids', 'maids.maid_id = day_services.maid_id')
                ->join('areas', 'customer_addresses.area_id = areas.area_id')
                ->join('zones', 'areas.zone_id = zones.zone_id')
                ->join('service_types', 'bookings.service_type_id = service_types.service_type_id','left')
		->where('bookings.booking_category', 'C')
                ->where('day_services.service_status', 2)
                ->where('bookings.booking_status', 1);
                //->where('maids.maid_status', 1);


        if (!empty($deleted_bookings)) {
            $this->db->where_not_in('bookings.booking_id', $deleted_bookings);
        }

        if ($zone != "") {
            $this->db->where('zones.zone_id', $zone);
        }
        if ($customer_id>0) {
            $this->db->where('day_services.customer_id', $customer_id);
        }
        
        if ($service_date != "") {
            //$this->db->where("((bookings.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE);
            //$this->db->where("((bookings.service_start_date = " . $this->db->escape($service_date) . " AND bookings.booking_type = 'OD') OR (bookings.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND bookings.booking_type = 'WE') OR (bookings.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND bookings.booking_type = 'BW'))", NULL, FALSE);
			$this->db->where('day_services.service_date',$service_date);
		}
		
		if ($company_name != "") {
            $this->db->where('customers.company_name', $company_name);
        }
        if($filter_maid_id != NULL)
		{
			$this->db->where('maids.maid_id', $filter_maid_id);
		}
		
        $this->db->order_by('maids.maid_name');

        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result();
    }
    
    
    function get_payment_reports($payment_date, $zone) {

        $this->db->select('customers.customer_name, customers.payment_type, customer_payments.paid_amount, customer_payments.paid_at_id, customer_payments.paid_datetime, tablets.zone_id, zones.zone_name')
                ->from('customer_payments')
                ->join('customers', 'customer_payments.customer_id = customers.customer_id', 'left')
                ->join('tablets', 'tablets.tablet_id = customer_payments.paid_at_id', 'left')
                ->join('zones', 'zones.zone_id = tablets.zone_id', 'left');
        //->group_by("customer_payments.day_service_id");

        if ($zone != "") {
            $this->db->where('zones.zone_id', $zone);
        }
        if ($payment_date != "") {
            $this->db->where("DATE(customer_payments.paid_datetime) = '" . $payment_date . "'", NULL, FALSE);
        }

        $query = $this->db->get();
//        echo $this->db->last_query();exit;
        return $query->result();
    }

    // Edited By Aparna

    function get_onedaycancel_report($date = NULL) {
        if (!$date) {
            $date = date('Y-m-d');
        }

        $query = $this->db->query('SELECT bk_del.booking_delete_id,bk_del.booking_delete_id,bk_del.service_date, bk_del.added_datetime AS added,bk.service_start_date,m.maid_name,bk.booking_id,bk.customer_address_id,c.customer_name,c.payment_type,CONCAT(TIME_FORMAT(bk.time_from,"%h:%i%p")," - ",TIME_FORMAT(bk.time_to,"%h:%i%p")) AS shift,us.username,us.user_fullname

                            FROM booking_deletes bk_del 
                            JOIN bookings bk ON bk_del.booking_id=bk.booking_id
                            JOIN maids m ON m.maid_id=bk.maid_id
                            JOIN customer_addresses c_aadr ON c_aadr.customer_address_id=bk.customer_address_id 
                            JOIN customers c ON c.customer_id=bk.customer_id  AND bk.customer_address_id = c_aadr.customer_address_id
                            JOIN users us ON us.user_id=bk_del.deleted_by                             
                            WHERE bk.booking_category="C" AND bk.booking_status=1 AND bk_del.service_date="' . $date . '" AND DATE(bk_del.added_datetime)="' . $date . '" GROUP BY bk.booking_id');
        return $query->result();
    }
    
    function get_bookingremarks_report($date = NULL,$date_to = NULL) {
        if (!$date) {
            $date = date('Y-m-d');
        }
		if (!$date_to) {
            $date_to = date('Y-m-d');
        }

        $query = $this->db->query('SELECT bk_del.id,bk_del.remarks,bk_del.service_date, bk_del.deleted_date_time AS added,m.maid_name,bk.booking_id,bk.customer_address_id,bk.booking_type,c.customer_name,c.payment_type,CONCAT(TIME_FORMAT(bk.time_from,"%h:%i%p")," - ",TIME_FORMAT(bk.time_to,"%h:%i%p")) AS shift,us.username,us.user_fullname

                            FROM booking_delete_remarks bk_del 
                            JOIN bookings bk ON bk_del.booking_id=bk.booking_id
                            JOIN maids m ON m.maid_id=bk.maid_id
                            JOIN customer_addresses c_aadr ON c_aadr.customer_address_id=bk.customer_address_id 
                            JOIN customers c ON c.customer_id=bk.customer_id  AND bk.customer_address_id = c_aadr.customer_address_id
                            JOIN users us ON us.user_id=bk_del.deleted_by                             
                            WHERE DATE(bk_del.deleted_date_time) between "' . $date.'" AND "' . $date_to.'"');
        return $query->result();
    }
    
    function get_booking_cancel_report($service_date = NULL) {
        if (!$service_date) {
            $service_date = date('Y-m-d');
        }
        $service_week_day = date('w', strtotime($service_date));

        $deleted_bookings = array();
        $deletes = $this->get_booking_deletes_by_date($service_date);
        if (!empty($deletes)) {
            foreach ($deletes as $delete) {
                $deleted_bookings[] = $delete->booking_id;
            }
        }



        $this->db->select('bookings.maid_id, bookings.booking_id, bookings.booking_type, customers.customer_id, customers.customer_name, customers.payment_type, maids.maid_name, DAYNAME(bookings.service_start_date) as day, areas.area_name, zones.zone_name, 

bookings.time_from AS time_from, bookings.time_to AS time_to, CONCAT(TIME_FORMAT(bookings.time_from,"%h:%i%p")," - ",TIME_FORMAT(bookings.time_to,"%h:%i%p")) AS shift, us.username,us.user_fullname', FALSE)
                ->from('bookings')
                ->join('booking_cancel bc', 'bookings.booking_id = bc.booking_id')
                ->join('customers', 'bookings.customer_id = customers.customer_id')
                //->join('customer_addresses', 'customers.customer_id = customer_addresses.customer_id')
                ->join('customer_addresses', 'bookings.customer_address_id = customer_addresses.customer_address_id')
                ->join('maids', 'maids.maid_id = bookings.maid_id')
                ->join('areas', 'customer_addresses.area_id = areas.area_id')
                ->join('zones', 'areas.zone_id = zones.zone_id')
                ->join('users us', 'us.user_id=bc.deleted_by')
                ->where('bc.service_date', $service_date)
				->where('bookings.booking_category', 'C')
                ->where('bookings.booking_status', 1)
                ->where('maids.maid_status', 1);


        if (!empty($deleted_bookings)) {
            $this->db->where_not_in('bookings.booking_id', $deleted_bookings);
        }


        if ($service_date != "") {
            //$this->db->where("((bookings.service_start_date = " . $this->db->escape($service_date) . " AND bookings.booking_type = 'OD') OR (bookings.service_start_date <= " . $this->db->escape($service_date) . " AND bookings.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1 AND service_week_day = " . $service_week_day . " AND bookings.booking_type = 'WE') OR (bookings.service_start_date <= " . $this->db->escape($service_date) . " AND service_end = 0 AND service_week_day = " . $service_week_day . " AND bookings.booking_type = 'WE'))", NULL, FALSE);
            $this->db->where("((bookings.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0 AND bookings.service_start_date <= " . $this->db->escape($service_date) . "))", NULL, FALSE);
            $this->db->where("((bookings.service_start_date = " . $this->db->escape($service_date) . " AND bookings.booking_type = 'OD') OR (bookings.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND bookings.booking_type = 'WE') OR (bookings.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND bookings.booking_type = 'BW'))", NULL, FALSE);
        }


        $query = $this->db->get();

        /* $query = $this->db->query('SELECT bk_del.booking_cancel_id,bk_del.service_date,bk.service_start_date,m.maid_name,bk.booking_id,bk.customer_address_id,c.customer_name,CONCAT(TIME_FORMAT(bk.time_from,"%h:%i%p")," - ",TIME_FORMAT(bk.time_to,"%h:%i%p")) AS shift,us.username,us.user_fullname
          FROM booking_cancel bk_del
          JOIN bookings bk ON bk_del.booking_id=bk.booking_id
          JOIN maids m ON m.maid_id=bk.maid_id
          JOIN customer_addresses c_aadr ON c_aadr.customer_address_id=bk.customer_address_id
          JOIN customers c ON c.customer_id=bk.customer_id  AND bk.customer_address_id = c_aadr.customer_address_id
          JOIN users us ON us.user_id=bk_del.deleted_by
          WHERE bk_del.service_date="' . $date.'" GROUP BY bk.booking_id'); */
        return $query->result();
    }

    function delete_onedaycancel($id) {
        $query = $this->db->query('DELETE FROM booking_deletes WHERE booking_delete_id=' . $id);
        if ($this->db->affected_rows() > 0) {
            return "success";
        } else {
            return "Failed deteling the entry";
        }
    }

    function get_employee_work_report($maid_id, $month, $year) {

        $query = $this->db->query('SELECT day_s.service_date,bk.booking_type,bk.time_from,bk.time_to,day_s.maid_id,day_s.customer_id,c.customer_name, c.payment_type

                                    FROM day_services day_s
                                    JOIN bookings bk ON bk.booking_id=day_s.booking_id
                                    JOIN customers c ON c.customer_id=day_s.customer_id

                                    WHERE day_s.service_status=2 AND day_s.maid_id=' . $this->db->escape($maid_id) . ' AND YEAR(day_s.service_date)=' . $this->db->escape($year) . ' AND MONTH(day_s.service_date)=' . $this->db->escape($month) . ' AND bk.booking_status=1 ORDER BY day_s.service_date ASC');

        return $query->result();
    }

    function get_employee_work_all($month, $year) {
        $query = $this->db->query('SELECT round(SUM(TIME_TO_SEC(TIMEDIFF(bk.time_to, bk.time_from))/3600)) AS total_hours, m.maid_name, m.maid_id
                                    FROM day_services day_s
                                    JOIN bookings bk ON bk.booking_id=day_s.booking_id
                                    JOIN maids m ON m.maid_id=day_s.maid_id
                                    
                                    WHERE day_s.service_status=2 AND  YEAR(day_s.service_date)=' . $this->db->escape($year) . ' AND MONTH(day_s.service_date)=' . $this->db->escape($month) . ' AND bk.booking_status=1 GROUP BY day_s.maid_id  ORDER BY m.maid_name ASC');
        //echo $this->db->last_query();
        return $query->result();
    }

    //sandeep
    function get_employee_work_normal($month, $year, $maid_id) {
        $query = $this->db->query('SELECT round(SUM(TIME_TO_SEC(TIMEDIFF(bk.time_to, bk.time_from))/3600)) AS total_normal_hours, m.maid_name, m.maid_id
                                    FROM day_services day_s
                                    JOIN bookings bk ON bk.booking_id=day_s.booking_id
                                    JOIN maids m ON m.maid_id=day_s.maid_id
                                    
                                    WHERE day_s.service_status=2 AND day_s.maid_id=' . $this->db->escape($maid_id) . ' AND bk.time_from < "18.00.00" AND bk.service_week_day!=5 AND  YEAR(day_s.service_date)=' . $this->db->escape($year) . ' AND MONTH(day_s.service_date)=' . $this->db->escape($month) . ' AND bk.booking_status=1 GROUP BY day_s.maid_id');
        $rows = $query->row();
        if ($rows) {
            return $rows->total_normal_hours;
        }
    }

    //sandeep
    function get_employee_extra_work($month, $year, $maid_id) {
        $query_row = $this->db->query('SELECT round(SUM(TIME_TO_SEC(TIMEDIFF(bk.time_to, bk.time_from))/3600)) AS total_hrs_extra_work
                                    FROM day_services day_s
                                    JOIN bookings bk ON bk.booking_id=day_s.booking_id
                                    JOIN maids m ON m.maid_id=day_s.maid_id
                                    
                                    WHERE day_s.service_status=2 AND  YEAR(day_s.service_date)=' . $this->db->escape($year) . ' AND MONTH(day_s.service_date)=' . $this->db->escape($month) . ' AND bk.booking_status=1 
                                    AND day_s.maid_id=' . $this->db->escape($maid_id) . ' AND bk.time_from >="18.00.00" GROUP BY day_s.maid_id');
        //echo $this->db->last_query(); 
        $rows_extra_work = $query_row->row();
        if ($rows_extra_work) {

            return $rows_extra_work->total_hrs_extra_work;
        }
    }

    //sandeep
    function get_employee_extra_work_holiday($month, $year, $maid_id) {
        $query = $this->db->query('SELECT round(SUM(TIME_TO_SEC(TIMEDIFF(bk.time_to, bk.time_from))/3600)) AS total_hrs_extra
                                    FROM day_services day_s
                                    JOIN bookings bk ON bk.booking_id=day_s.booking_id
                                    JOIN maids m ON m.maid_id=day_s.maid_id
                                    
                                    WHERE day_s.service_status=2 AND  YEAR(day_s.service_date)=' . $this->db->escape($year) . ' AND MONTH(day_s.service_date)=' . $this->db->escape($month) . ' AND bk.booking_status=1 
                                    AND day_s.maid_id=' . $this->db->escape($maid_id) . ' AND bk.service_week_day=5 GROUP BY day_s.maid_id');
        //echo $this->db->last_query(); 
        $rows = $query->row();
        if ($rows) {
            return $rows->total_hrs_extra;
        }
    }

    function get_activity_zone_report($service_date, $zone) {

        $service_week_day = date('w', strtotime($service_date));



        $deleted_bookings = array();

        $deletes = $this->get_booking_deletes_by_date($service_date);

        if (!empty($deletes)) {

            foreach ($deletes as $delete) {

                $deleted_bookings[] = $delete->booking_id;
            }
        }





        $this->db->select('bookings.maid_id, bookings.booking_id, bookings.booking_type, customers.customer_id, customers.customer_name, maids.maid_name, DAYNAME(bookings.service_start_date) as day, areas.area_name, zones.zone_name,

bookings.time_from AS time_from, bookings.time_to AS time_to')
                ->from('day_services')
                ->join('bookings', 'day_services.booking_id = bookings.booking_id')
                ->join('customers', 'bookings.customer_id = customers.customer_id')

                //->join('customer_addresses', 'customers.customer_id = customer_addresses.customer_id')
                ->join('customer_addresses', 'bookings.customer_address_id = customer_addresses.customer_address_id', 'left')
                ->join('maids', 'maids.maid_id = bookings.maid_id')
                ->join('areas', 'customer_addresses.area_id = areas.area_id', 'left')
                ->join('zones', 'areas.zone_id = zones.zone_id', 'left')
                ->where('day_services.service_date', $service_date)
                ->where('day_services.service_status', 2)

                //->where('maids.maid_status', 1)
                //->where('bookings.booking_status', 1)
                ->group_by('day_services.booking_id')
                ->order_by('maids.maid_name');







        if (!empty($deleted_bookings)) {

            //$this->db->where_not_in('bookings.booking_id', $deleted_bookings);
        }

        if ($zone != "") {

            $this->db->where('zones.zone_id', $zone);
        }





        $query = $this->db->get();

        //echo $this->db->last_query();exit;

        return $query->result();
    }

    function get_activity_summary_report_by_date($service_date) {
        $service_week_day = date('w', strtotime($service_date));

        $deleted_bookings = array();
        $deletes = $this->get_booking_deletes_by_date($service_date);
        if (!empty($deletes)) {
            foreach ($deletes as $delete) {
                $deleted_bookings[] = $delete->booking_id;
            }
        }





        /* $this->db->select('day_services.service_date,day_services.booking_id, bookings.booking_type,round(TIME_TO_SEC(TIMEDIFF(bookings.time_to, bookings.time_from))/3600) AS Total_hours', FALSE)
          ->from('day_services')
          ->join('bookings', 'day_services.booking_id = bookings.booking_id')
          ->join('customers', 'bookings.customer_id = customers.customer_id')
          //->join('customer_addresses', 'customers.customer_id = customer_addresses.customer_id')
          ->join('customer_addresses', 'bookings.customer_address_id = customer_addresses.customer_address_id')
          ->join('maids', 'maids.maid_id = bookings.maid_id')
          ->join('areas', 'customer_addresses.area_id = areas.area_id')
          ->join('zones', 'areas.zone_id = zones.zone_id')
          ->where('day_services.service_date', $service_date)
          ->where('day_services.service_status', 2)
          //->where('maids.maid_status', 1)
          //->where('bookings.booking_status', 1)
          ->order_by('day_services.service_date')
          ->group_by('day_services.booking_id'); */

        $this->db->select('day_services.service_date,day_services.booking_id, bookings.booking_type,round(TIME_TO_SEC(TIMEDIFF(bookings.time_to, bookings.time_from))/3600) AS Total_hours', FALSE)
                ->from('day_services')
                ->join('bookings', 'day_services.booking_id = bookings.booking_id')
                ->join('customers', 'bookings.customer_id = customers.customer_id')
                ->join('customer_addresses', 'bookings.customer_address_id = customer_addresses.customer_address_id', 'left')
                ->join('maids', 'bookings.maid_id = maids.maid_id')
                ->join('areas', 'customer_addresses.area_id = areas.area_id', 'left')
                ->join('zones', 'areas.zone_id = zones.zone_id', 'left')
                ->where('day_services.service_date', $service_date)
                ->where('day_services.service_status', 2)
                ->order_by('day_services.service_date')
                ->group_by('day_services.booking_id');


        if (!empty($deleted_bookings)) {
            //$this->db->where_not_in('bookings.booking_id', $deleted_bookings);
        }


        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        return $query->result();
    }

    function get_activity_summary_report_by_date1($service_date) {


        $deleted_bookings = array();

        $deletes = $this->get_booking_deletes_by_date($service_date);
        if (!empty($deletes)) {
            foreach ($deletes as $delete) {
                $deleted_bookings[] = $delete->booking_id;
            }
        }


        //$deleted_bookings = array();
        $query = $this->db->query("SELECT day_s.service_date,COUNT(day_s.day_service_id) AS hour_count,bk.booking_type,round(SUM(TIME_TO_SEC(TIMEDIFF(bk.time_to, bk.time_from))/3600)) AS Total_hours
                                    
                                    
                                    FROM day_services day_s
                                    JOIN bookings bk ON bk.booking_id=day_s.booking_id
                                    JOIN maids m ON day_s.maid_id = m.maid_id  
                                    
                                    
                                    WHERE  bk.booking_status =1 AND m.maid_status =1 AND " . (!empty($deleted_bookings) ? ' day_s.booking_id NOT IN (' . join(",", $deleted_bookings) . ') AND ' : '') . " day_s.service_status=2 AND day_s.service_date = " . $this->db->escape($service_date) . " 
                                    
                                    GROUP BY day_s.service_date,bk.booking_type
                                    ORDER BY day_s.service_date ASC");
        /* $query = $this->db->query("SELECT day_s.service_date,COUNT(day_s.day_service_id) AS hour_count,bk.booking_type,round(SUM(TIME_TO_SEC(TIMEDIFF(bk.time_to, bk.time_from))/3600)) AS Total_hours,
          SUM(IF(day_s.payment_status=1, IF(cp.paid_amount > 0 , cp.paid_amount, day_s.total_fee),0)) AS Total_payment1,
          SUM(IF(day_s.payment_status=1, cp.paid_amount,0)) AS Total_payment

          FROM day_services day_s
          JOIN bookings bk ON bk.booking_id=day_s.booking_id
          JOIN maids m ON day_s.maid_id = m.maid_id

          LEFT JOIN customer_payments cp ON day_s.day_service_id = cp.day_service_id
          WHERE  bk.booking_status =1 AND m.maid_status =1 AND " . (!empty($deleted_bookings) ? ' day_s.booking_id NOT IN (' . join(",", $deleted_bookings) . ') AND ' : '') . " day_s.service_status=2 AND day_s.service_date BETWEEN " . $this->db->escape($start_date) . " AND " . $this->db->escape($end_date) . "

          GROUP BY day_s.service_date,bk.booking_type
          ORDER BY day_s.service_date ASC"); */

        //echo $this->db->last_query();exit;
        return $query->result();
    }

    function get_activity_summary_report($start_date, $end_date) {


        $deleted_bookings = array();

        $datetime1 = new DateTime($start_date);
        $datetime2 = new DateTime($end_date);
        $interval = $datetime1->diff($datetime2);
        $days = $interval->format('%a');

        $service_date = $start_date;
        for ($i = 0; $i <= $days; ++$i) {
            $deletes = $this->get_booking_deletes_by_date($service_date);
            if (!empty($deletes)) {
                foreach ($deletes as $delete) {
                    $deleted_bookings[] = $delete->booking_id;
                }
            }
            $service_date = date('Y-m-d', strtotime('+1 day', strtotime($service_date)));
        }

        //$deleted_bookings = array();
        $query = $this->db->query("SELECT day_s.service_date,COUNT(day_s.day_service_id) AS hour_count,bk.booking_type,round(SUM(TIME_TO_SEC(TIMEDIFF(bk.time_to, bk.time_from))/3600)) AS Total_hours
                                    
                                    
                                    FROM day_services day_s
                                    JOIN bookings bk ON bk.booking_id=day_s.booking_id
                                    JOIN maids m ON day_s.maid_id = m.maid_id                                    
                                    
                                    WHERE  bk.booking_status =1 AND m.maid_status =1 AND " . (!empty($deleted_bookings) ? ' day_s.booking_id NOT IN (' . join(",", $deleted_bookings) . ') AND ' : '') . " day_s.service_status=2 AND day_s.service_date BETWEEN " . $this->db->escape($start_date) . " AND " . $this->db->escape($end_date) . "
                                    
                                    GROUP BY day_s.service_date,bk.booking_type
                                    ORDER BY day_s.service_date ASC");
        /* $query = $this->db->query("SELECT day_s.service_date,COUNT(day_s.day_service_id) AS hour_count,bk.booking_type,round(SUM(TIME_TO_SEC(TIMEDIFF(bk.time_to, bk.time_from))/3600)) AS Total_hours,
          SUM(IF(day_s.payment_status=1, IF(cp.paid_amount > 0 , cp.paid_amount, day_s.total_fee),0)) AS Total_payment1,
          SUM(IF(day_s.payment_status=1, cp.paid_amount,0)) AS Total_payment

          FROM day_services day_s
          JOIN bookings bk ON bk.booking_id=day_s.booking_id
          JOIN maids m ON day_s.maid_id = m.maid_id

          LEFT JOIN customer_payments cp ON day_s.day_service_id = cp.day_service_id
          WHERE  bk.booking_status =1 AND m.maid_status =1 AND " . (!empty($deleted_bookings) ? ' day_s.booking_id NOT IN (' . join(",", $deleted_bookings) . ') AND ' : '') . " day_s.service_status=2 AND day_s.service_date BETWEEN " . $this->db->escape($start_date) . " AND " . $this->db->escape($end_date) . "

          GROUP BY day_s.service_date,bk.booking_type
          ORDER BY day_s.service_date ASC"); */

        //echo $this->db->last_query();exit;
        return $query->result();
    }

    function get_zone_activity_summary_report($start_date, $end_date) {
        $query = $this->db->query("SELECT z.zone_id,z.zone_name, COUNT(day_s.day_service_id) AS hour_count,bk.booking_type,round(SUM(TIME_TO_SEC(TIMEDIFF(bk.time_to, bk.time_from))/3600)) AS Total_hours,
                                    SUM(IF(day_s.payment_status=1,day_s.total_fee,0)) AS Total_payment
                                    FROM day_services day_s
                                    JOIN bookings bk ON bk.booking_id=day_s.booking_id
                                    JOIN customer_addresses ca ON bk.customer_address_id = ca.customer_address_id
                                    JOIN areas a ON ca.area_id = a.area_id
                                    JOIN zones z ON a.zone_id = z.zone_id
                                    WHERE  day_s.service_status=2 AND day_s.service_date BETWEEN " . $this->db->escape($start_date) . " AND " . $this->db->escape($end_date) . "
                                    GROUP BY z.zone_id,bk.booking_type
                                    ORDER BY z.zone_id ASC");

//        echo $this->db->last_query();exit;
        return $query->result();
    }

    function get_payment_by_date($service_date) {
        $this->db->select_sum("paid_amount")
                ->from('customer_payments')
                ->where("DATE(paid_datetime) = '" . $service_date . "'", NULL, FALSE)
                ->limit(1);

        $get_payment_by_date_qry = $this->db->get();


        if ($get_payment_by_date_qry->num_rows() > 0) {
            return $get_payment_by_date_qry->row()->paid_amount;
        } else {
            return 0;
        }
    }

    function get_customer_refereces() {
        $this->db->select("count(*) AS reference, customer_source_id", FALSE)
                ->from('customers')
                ->where('customer_source_id >', 0, FALSE)
                ->group_by('customer_source_id');

        $get_customer_refereces_qry = $this->db->get();

        return $get_customer_refereces_qry->result();
    }

    function get_total_booking_hours($date_from = NULL, $type = 1, $date_to = NULL) {
//            $db = new Database();
        $service_week_day = date('w', strtotime($date_from));

        $deleted_bookings = $this->get_booking_deletes_by_date($date_from);

        $total_booking_hours_qry = '';
        if ($type === 1) {

            $total_booking_hours_qry = "SELECT SUM(TIME_TO_SEC(TIMEDIFF(time_to,time_from))) AS duration FROM bookings JOIN maids m on bookings.maid_id = m.maid_id "
                    . " WHERE booking_status = 1 AND m.maid_status = 1 AND WHERE booking_category = 'C' AND"
                    . " ((service_actual_end_date >= '" . ($date_from) . "' AND service_end = 1) OR (service_end = 0)) AND ((service_start_date = '" . ($date_from) . "' AND booking_type = 'OD') OR (service_start_date <= '" . ($date_from) . "' AND service_week_day = " . $service_week_day . " AND booking_type = 'WE') OR (service_start_date <= '" . ($date_from) . "' AND MOD(DATEDIFF(DATE('" . ($date_from) . "'), DATE(service_start_date)), 14) = 0 AND booking_type = 'BW'))";

            if (count($deleted_bookings) > 0) {
                $deleted_bookings = join(",", $deleted_bookings);
                $total_booking_hours_qry .= ' AND booking_id NOT IN (' . $deleted_bookings . ')';
            }
        } else if ($type === 2) {

            $total_booking_hours_qry = "SELECT SUM(TIME_TO_SEC(TIMEDIFF(time_to,time_from))) AS duration FROM day_services ds JOIN bookings b ON ds.booking_id = b.booking_id WHERE ds.service_status = 2 ";

            if ($date_from && $date_to) {

                $total_booking_hours_qry .= ' AND ds.service_date BETWEEN "' . $date_from . '" and "' . $date_to . '"';
            } else if ($date_from || $date_to) {

                $date = $date_from ? $date_from : $date_to;
                $total_booking_hours_qry .= ' AND ds.service_date = "' . $date . '"';
            } else {

                $total_booking_hours_qry .= ' AND ds.service_date = "' . $date_from . '"';
            }
        }


        $total_booking_hours_qry .= ' LIMIT 1';
        $query = $this->db->query($total_booking_hours_qry);

        $booking = $query->row();

        return isset($booking->duration) ? floor($booking->duration / 3600) : 0;
    }

    function get_day_collection($date_from = NULL, $date_to = NULL) {

        $day_collection_qry = 'SELECT SUM(paid_amount) AS day_collection FROM customer_payments';

        if ($date_from && $date_to) {
            $day_collection_qry .= ' WHERE DATE(paid_datetime) BETWEEN "' . $date_from . '" and "' . $date_to . '"';
        } else {
            $date = $date_from ? $date_from : $date_to;

            $day_collection_qry .= ' WHERE DATE(paid_datetime) = "' . $date . '"';
        }

        $day_collection_qry .= ' LIMIT 1';

        $get_day_collection_qry = $this->db->query($day_collection_qry);
        $payments = $get_day_collection_qry->row();

//            $payments = mysql_fetch_object($get_day_collection_qry);

        return isset($payments->day_collection) ? $payments->day_collection : 0;
    }

    function get_total_bookings($date_from = NULL, $type = 1, $date_to = NULL) {


        $service_week_day = date('w', strtotime($date_from));

        $deleted_bookings = $this->get_booking_deletes_by_date($date_from)[0];

        $total_booking_hours_qry = '';

        if ($type === 1) {

            $total_booking_hours_qry = "SELECT booking_id FROM bookings WHERE booking_category = 'C' AND WHERE booking_status = 1 AND ((service_actual_end_date >= '" . ($date_from) . "' AND service_end = 1) OR (service_end = 0)) AND ((service_start_date = '" . ($date_from) . "' AND booking_type = 'OD') OR (service_start_date <= '" . ($date_from) . "' AND service_week_day = " . $service_week_day . " AND booking_type = 'WE') OR (service_start_date <= '" . ($date_from) . "' AND MOD(DATEDIFF(DATE('" . ($date_from) . "'), DATE(service_start_date)), 14) = 0 AND booking_type = 'BW'))";
        } else if ($type === 2) {

            $total_booking_hours_qry = "SELECT day_service_id FROM day_services WHERE service_status = 2";

            if ($date_from && $date_to) {

                $total_booking_hours_qry .= ' AND service_date BETWEEN "' . $date_from . '" and "' . $date_to . '"';
            } else if ($date_from || $date_to) {

                $date = $date_from ? $date_from : $date_to;
                $total_booking_hours_qry .= ' AND service_date = "' . $date . '"';
            } else {

                $total_booking_hours_qry .= ' AND service_date = "' . $date_from . '"';
            }
        }
//print_r($deleted_bookings);
        if (count($deleted_bookings) > 0 ) {
            
            $deleted_bookings = join(",", $deleted_bookings);
            if(!empty($deleted_bookings)){
                
            $total_booking_hours_qry .= " AND booking_id NOT IN (' ". $deleted_bookings . "')";
            }
        }

        $get_total_bookings_qry = $this->db->query($total_booking_hours_qry);

        return $get_total_bookings_qry->num_rows();
//            return mysql_num_rows($get_total_bookings_qry);
    }

    function get_one_day_bookings($date_from = NULL, $type = 1, $date_to = NULL) {


        $service_week_day = date('w', strtotime($date_from));

        $deleted_bookings = $this->get_booking_deletes_by_date($date_from)[0];

        $total_booking_hours_qry = '';

        if ($type === 1) {

            $total_booking_hours_qry = "SELECT booking_id FROM bookings WHERE booking_category = 'C' AND WHERE booking_status = 1 AND booking_type = 'OD' AND ((service_actual_end_date >= " . ($date_from) . " AND service_end = 1) OR (service_end = 0)) AND ((service_start_date = '" . ($date_from) . "' AND booking_type = 'OD') OR (service_start_date <= '" . ($date_from) . "' AND service_week_day = " . $service_week_day . " AND booking_type = 'WE') OR (service_start_date <= '" . ($date_from) . "' AND MOD(DATEDIFF(DATE('" . ($date_from) . "'), DATE(service_start_date)), 14) = 0 AND booking_type = 'BW'))";

            if (count($deleted_bookings) > 0) {
                $deleted_bookings = join(",", $deleted_bookings);
                $total_booking_hours_qry .= ' AND booking_id NOT IN (' . $deleted_bookings . ')';
            }
        } else if ($type === 2) {

            $total_booking_hours_qry = "SELECT d.day_service_id FROM day_services d JOIN bookings b ON d.booking_id=b.booking_id WHERE b.booking_type = 'OD' AND b.booking_status = 1 AND d.service_status = 2";

            if ($date_from && $date_to) {

                $total_booking_hours_qry .= ' AND d.service_date BETWEEN "' . $date_from . '" and "' . $date_to . '"';
            } else if ($date_from || $date_to) {

                $date = $date_from ? $date_from : $date_to;
                $total_booking_hours_qry .= ' AND d.service_date = "' . $date . '"';
            } else {

                $total_booking_hours_qry .= ' AND d.service_date = "' . $date_from . '"';
            }
            if (count($deleted_bookings) > 0) {
                $deleted_bookings = join(",", $deleted_bookings->booking_id);
//print_r($deleted_bookings);
                $total_booking_hours_qry .= " AND d.booking_id NOT IN ('" . $deleted_bookings . "')";
            }
        }


        $get_total_bookings_qry = $this->db->query($total_booking_hours_qry);

        return $get_total_bookings_qry->num_rows();
//            return mysql_num_rows($get_total_bookings_qry);
    }

    function get_week_day_bookings($date_from = NULL, $type = 1, $date_to = NULL) {


        $service_week_day = date('w', strtotime($date_from));

        $deleted_bookings = $this->get_booking_deletes_by_date($date_from)[0];

        $total_booking_hours_qry = '';

        if ($type === 1) {

            $total_booking_hours_qry = "SELECT booking_id FROM bookings WHERE booking_category = 'C' AND WHERE booking_status = 1 AND booking_type = 'WE' AND ((service_actual_end_date >= " . ($date_from) . " AND service_end = 1) OR (service_end = 0)) AND ((service_start_date = " . ($date_from) . " AND booking_type = 'OD') OR (service_start_date <= " . ($date_from) . " AND service_week_day = " . $service_week_day . " AND booking_type = 'WE') OR (service_start_date <= " . ($date_from) . " AND MOD(DATEDIFF(DATE(" . ($date_from) . "), DATE(service_start_date)), 14) = 0 AND booking_type = 'BW'))";
            if (count($deleted_bookings) > 0) {
                $deleted_bookings = join(",", $deleted_bookings);
                $total_booking_hours_qry .= ' AND booking_id NOT IN (' . $deleted_bookings . ')';
            }
        } else if ($type === 2) {

            $total_booking_hours_qry = "SELECT d.booking_id FROM day_services d JOIN bookings b ON d.booking_id = b.booking_id AND b.booking_type = 'WE' AND b.booking_status = 1 AND b.booking_category = 'C' AND d.service_status = 2";
            if ($date_from && $date_to) {
                $total_booking_hours_qry .= ' AND d.service_date BETWEEN "' . $date_from . '" and "' . $date_to . '"';
            } else if ($date_from || $date_to) {
                $date = $date_from ? $date_from : $date_to;
                $total_booking_hours_qry .= ' ANd d.service_date = "' . $date . '"';
            } else {
                $total_booking_hours_qry .= ' AND d.service_date = "' . $date_from . '"';
            }
            if (count($deleted_bookings) > 0) {
                $deleted_bookings = join(",", $deleted_bookings);
                if(!empty($deleted_bookings)){
                    
                $total_booking_hours_qry .= ' AND d.booking_id NOT IN (' . $deleted_bookings . ')';
                }
            }
        }


        $get_total_bookings_qry = $this->db->query($total_booking_hours_qry);

        return $get_total_bookings_qry->num_rows();
//            return mysql_num_rows($get_total_bookings_qry);
    }

    function get_total_cancellation($date_from = NULL, $date_to = NULL) {


        $total_cancellation_qry = 'SELECT booking_id FROM booking_deletes';

        if ($date_from && $date_to) {
            $total_cancellation_qry .= ' WHERE service_date BETWEEN "' . $date_from . '" AND "' . $date_to . '"';
        } else {
            $date = $date_from ? $date_from : $date_to;

            $total_cancellation_qry .= ' WHERE service_date = "' . $date . '"';
        }

        $get_total_cancellation_qry = $this->db->query($total_cancellation_qry);
        return $get_total_cancellation_qry->num_rows();

//            return mysql_num_rows($get_total_cancellation_qry);
    }

    function get_total_invoice_amount($date_from = NULL, $date_to = NULL) {


        $service_week_day = date('w', strtotime($date_from));

        $deleted_bookings = $this->get_booking_deletes_by_date($date_from);

        $total_booking_hours_qry = 'SELECT i.day_service_id FROM day_services d JOIN invoice i ON d.day_service_id=i.day_service_id WHERE d.service_status = 2';

        if ($date_from && $date_to) {
            $total_booking_hours_qry .= ' AND d.service_date BETWEEN "' . $date_from . '" and "' . $date_to . '"';
        } else if ($date_from || $date_to) {
            $date = $date_from ? $date_from : $date_to;
            $total_booking_hours_qry .= ' AND d.service_date = "' . $date . '"';
        } else {
            $total_booking_hours_qry .= ' AND d.service_date = "' . $date_from . '"';
        }
        if (count($deleted_bookings) > 0) {
            $deleted_bookings = join(",", $deleted_bookings);
            if(!empty($deleted_bookings)){
                
            $total_booking_hours_qry .= ' AND d.booking_id NOT IN (' . $deleted_bookings . ')';
            }
        }

        $get_total_invoice_amount_qry = $this->db->query($total_booking_hours_qry);

        return $get_total_invoice_amount_qry->num_rows();
//            return mysql_num_rows($get_total_invoice_amount_qry);
    }
 function get_total_pending_amount($date_from = NULL, $date_to = NULL) 
    {
        

            $service_week_day = date('w', strtotime($date_from));

            $deleted_bookings = $this->get_booking_deletes_by_date($date_from);
            
            $total_pending_amount_qry = "SELECT SUM(d.total_fee) AS pending_amount FROM day_services d JOIN invoice i ON d.day_service_id = i.day_service_id WHERE d.service_status = 2 AND i.invoice_status = 0";
            if($date_from && $date_to)
            {
                    $total_pending_amount_qry .= ' AND DATE(i.added) BETWEEN "'. $date_from. '" and "'. $date_to.'"';
            }
            else if($date_from || $date_to)
            {
                    $date = $date_from ? $date_from : $date_to;
                    $total_pending_amount_qry .= ' AND DATE(i.added) = "' . $date . '"';
            }
            else
            {
                    $total_pending_amount_qry .= ' AND DATE(i.added) = "' . $date_from . '"';
            }
            if(count($deleted_bookings) > 0)
            {      
                    $deleted_bookings = join(",", $deleted_bookings);
                    if(!empty($deleted_bookings))
                    $total_pending_amount_qry .= ' AND d.booking_id NOT IN (' . $deleted_bookings . ')';
            }

            $total_pending_amount_qry .= ' LIMIT 1';

            $get_total_pending_amount_qry = $this->db->query($total_pending_amount_qry);

             $payments = $get_total_pending_amount_qry->row();
//            $payments = mysql_fetch_object($get_total_pending_amount_qry);

            return isset($payments->pending_amount) ? $payments->pending_amount : 0;
            
    }
    
     function get_new_customer($date_from = NULL, $date_to = NULL)
    {
        
            
            $new_customer_qry = 'SELECT customer_id FROM customers';

            if($date_from && $date_to)
            {
                    $new_customer_qry .= ' WHERE DATE(customer_added_datetime) BETWEEN "'. $date_from. '" and "'. $date_to.'"';
            }
            else
            {
                    $date = $date_from ? $date_from : $date_to;

                    $new_customer_qry .= ' WHERE DATE(customer_added_datetime) = "' . $date . '"';
            }            

            $get_new_customer_qry = $this->db->query($new_customer_qry);             

return $get_new_customer_qry->num_rows();
//            return mysql_num_rows($get_new_customer_qry);            

    }
    
    function get_online_payments()
    {
       $this->db->select('op.*, c.customer_name,m.maid_name')
                ->from('online_payments op')
                ->join('customers c','op.customer_id = c.customer_id', 'left')
                ->join('bookings b','op.booking_id = b.booking_id','left')
                ->join('maids m','b.maid_id = m.maid_id','left');
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_maids_for_report()
    {
        $this->db->select('m.maid_id, m.odoo_maid_id, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, m.maid_status, m.odoo_synch_status')
                ->from('maids m')
                ->order_by('m.maid_name','asc');
        $this->db->where('m.maid_status', 1);
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }
    
    function getbookingsbymaid_id($service_date,$maid_id,$zone_id = NULL)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deleted_bookings = array();
        $deletes = $this->get_booking_deletes_by_date($service_date);
        if (!empty($deletes)) {
            foreach ($deletes as $delete) {
                $deleted_bookings[] = $delete->booking_id;
            }
        }


        $this->db->select('bookings.maid_id, bookings.pay_by, bookings.cleaning_material, bookings.booking_note,bookings.justmop_reference, bookings.booking_id, bookings.booking_type, bookings.total_amount, bookings.booked_from, bookings.price_per_hr, bookings.discount, customers.price_hourly, customers.customer_source, customers.price_extra, customers.price_weekend, customers.customer_id, customers.customer_name, customers.payment_mode, customers.payment_type, customer_addresses.customer_address, maids.maid_name, DAYNAME(bookings.service_start_date) as day, areas.area_name, zones.zone_name, zones.driver_name, bookings.time_from AS time_from, bookings.time_to AS time_to')
                ->from('bookings')
                ->join('customers', 'bookings.customer_id = customers.customer_id')
                //->join('customer_addresses', 'customers.customer_id = customer_addresses.customer_id')
                ->join('customer_addresses', 'bookings.customer_address_id = customer_addresses.customer_address_id')
                ->join('maids', 'maids.maid_id = bookings.maid_id')
                ->join('areas', 'customer_addresses.area_id = areas.area_id')
                ->join('zones', 'areas.zone_id = zones.zone_id')
				->where('bookings.booking_category', 'C')
                ->where('bookings.booking_status', 1)
                ->where('maids.maid_id', $maid_id);
                //->where('maids.maid_status', 1);


        if (!empty($deleted_bookings)) {
            $this->db->where_not_in('bookings.booking_id', $deleted_bookings);
        }

        
        if ($service_date != "") {
            $this->db->where("((bookings.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE);
            $this->db->where("((bookings.service_start_date = " . $this->db->escape($service_date) . " AND bookings.booking_type = 'OD') OR (bookings.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND bookings.booking_type = 'WE') OR (bookings.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND bookings.booking_type = 'BW'))", NULL, FALSE);
        }
		
		if($zone_id != "") {
			$this->db->where('zones.zone_id',$zone_id);
		}
        $this->db->order_by('bookings.time_from');
        $this->db->order_by('zones.driver_name');
        $query = $this->db->get();
        return $query->result_array();
    }
	
	function getbookingsbymaid_id_new($service_date,$maid_id,$zone_id = NULL)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deleted_bookings = array();
        $deletes = $this->get_booking_deletes_by_date($service_date);
        if (!empty($deletes)) {
            foreach ($deletes as $delete) {
                $deleted_bookings[] = $delete->booking_id;
            }
        }


        $this->db->select('round(SUM(TIME_TO_SEC(TIMEDIFF(bookings.time_to, bookings.time_from))/3600)) as total_hrs', FALSE)
                ->from('bookings')
                ->join('customers', 'bookings.customer_id = customers.customer_id')
                //->join('customer_addresses', 'customers.customer_id = customer_addresses.customer_id')
                ->join('customer_addresses', 'bookings.customer_address_id = customer_addresses.customer_address_id')
                ->join('maids', 'maids.maid_id = bookings.maid_id')
                ->join('areas', 'customer_addresses.area_id = areas.area_id')
                ->join('zones', 'areas.zone_id = zones.zone_id')
				->where('bookings.booking_category', 'C')
                ->where('bookings.booking_status', 1)
                ->where('maids.maid_id', $maid_id);
                //->where('maids.maid_status', 1);


        if (!empty($deleted_bookings)) {
            $this->db->where_not_in('bookings.booking_id', $deleted_bookings);
        }

        
        if ($service_date != "") {
            $this->db->where("((bookings.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE);
            $this->db->where("((bookings.service_start_date = " . $this->db->escape($service_date) . " AND bookings.booking_type = 'OD') OR (bookings.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND bookings.booking_type = 'WE') OR (bookings.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND bookings.booking_type = 'BW'))", NULL, FALSE);
        }
		
		if($zone_id != "") {
			$this->db->where('zones.zone_id',$zone_id);
		}
        $this->db->order_by('bookings.time_from');
        $this->db->order_by('zones.driver_name');
        $query = $this->db->get();
        return $query->result();
    }
	

    function get_rate_reports($from_date,$to_date) 
    {
            $this->db->select('ds.customer_name,m.maid_name,ds.service_date,ds.rating,ds.comments')
                    ->from('day_services ds')
                                    ->join('maids m', 'ds.maid_id = m.maid_id');
            if ($from_date != "") {
                            $this->db->where('ds.rate_added_date >=',$from_date);
            }
            
            if ($to_date != "") {
                            $this->db->where('ds.rate_added_date <=',$to_date);
            }
            
            
            $query = $this->db->get();
            //echo $this->db->last_query();
            return $query->result();
        }
		
		function get_call_report_new($date = NULL, $customer_id = NULL, $calltype)
    {
        $this->db->select('cl.mobile_no, cl.added_date_time, c.customer_name', FALSE)
                ->from('call_logger cl')
                ->join('customers c', 'cl.customer_id = c.customer_id','left');
		if($date != NULL)
		{
			$this->db->where('DATE(cl.added_date_time)', $date);
		}
		if($customer_id != NULL)
		{
			$this->db->where('cl.customer_id', $customer_id);
		}
		if($calltype == 1)
		{
			$this->db->where('cl.customer_id', 0);
		}
		if($calltype == 2)
		{
			$this->db->where('cl.customer_id !=', 0);
		}
		$this->db->order_by('cl.added_date_time','desc');
        
        $get_back_payment = $this->db->get();
        
        return $get_back_payment->result();
    }
	
	function get_call_report($date = NULL, $customer_id = NULL)
    {
        $this->db->select('cl.mobile_no, cl.added_date_time, c.customer_name', FALSE)
                ->from('call_logger cl')
                ->join('customers c', 'cl.customer_id = c.customer_id','left');
		if($date != NULL)
		{
			$this->db->where('DATE(cl.added_date_time)', $date);
		}
		if($customer_id != NULL)
		{
			$this->db->where('cl.customer_id', $customer_id);
		}
		$this->db->order_by('cl.added_date_time','desc');
        
        $get_back_payment = $this->db->get();
        
        return $get_back_payment->result();
    }
	
	function get_schedule_report($service_date)
	{
		$service_week_day = date('w', strtotime($service_date));
		$deletes = $this->get_booking_deletes_by_date($service_date);
		
		$deleted_bookings = array();
		foreach($deletes as $delete)
		{
			$deleted_bookings[] = $delete->booking_id;
		}
				
		$this->db->select("c.customer_name, c.mobile_number_1, c.email_address, b.booking_type, c.customer_source, m.maid_name, b.total_amount", FALSE)
				->from('bookings b')
				->join('customers c', 'b.customer_id = c.customer_id')
				->join('maids m', 'b.maid_id = m.maid_id')
				//->join('day_services ds', 'ds.booking_id = b.booking_id','LEFT')
				->where('b.booking_status', 1)
				->where('b.booking_category', 'C')
				->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
				->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
				->order_by('b.time_from');
		
		if(count($deleted_bookings) > 0)
		{
			$this->db->where_not_in('b.booking_id', $deleted_bookings);
		}
		
		$get_schedule_by_date_qry = $this->db->get();
		return $get_schedule_by_date_qry->result();
	}
	
	function get_schedule_report_lat($service_date)
	{
		$service_week_day = date('w', strtotime($service_date));
		$deletes = $this->get_booking_deletes_by_date($service_date);
		$deleted_bookings = array();
		foreach($deletes as $delete)
		{
			$deleted_bookings[] = $delete->booking_id;
		}		
		$this->db->select("b.booking_id,b.time_to,b.time_from, c.customer_name, c.mobile_number_1, c.email_address, b.booking_type, c.customer_source, m.maid_name, b.total_amount, NULL AS service_status", FALSE)
				->from('bookings b')
				//->join('day_services ds', 'ds.booking_id = b.booking_id','INNER')
				->join('customers c', 'b.customer_id = c.customer_id')
				->join('maids m', 'b.maid_id = m.maid_id')
				->where('b.booking_status', 1)
				->where('b.booking_category', 'C')
				->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
				->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
                ->where('m.maid_status', 1)
				->order_by('b.time_from');
		if(count($deleted_bookings) > 0)
		{
			$this->db->where_not_in('b.booking_id', $deleted_bookings);
		}
		$get_schedule_by_date_qry = $this->db->get();
		//echo $this->db->last_query();exit;
		$services = $get_schedule_by_date_qry->result();
		$schedule = array();
        $sch_date=date("d/m/Y",strtotime($service_date));
		foreach ($services as $serv_val)
		{
			$dayservice = $this->checkdayserviceval($serv_val->booking_id,$service_date);
			$serv_val->customer_name = $serv_val->customer_name;
			$serv_val->mobile_number_1 = $serv_val->mobile_number_1;
			$serv_val->email_address = $serv_val->email_address;
			$serv_val->booking_type = $serv_val->booking_type;
			$serv_val->customer_source = $serv_val->customer_source;
			$serv_val->maid_name = $serv_val->maid_name;
			$serv_val->total_amount = $serv_val->total_amount;
            $serv_val->sch_date = $sch_date;
			if(count($dayservice) > 0)
			{
				$serv_val->service_status = $dayservice->service_status;
				$serv_val->rating = $dayservice->rating;
				$serv_val->comments = $dayservice->comments;
			} else {
				$serv_val->service_status = NULL;
				$serv_val->rating = NULL;
				$serv_val->comments = NULL;
			}
			array_push($schedule, $serv_val);	
		}
		return $schedule;
	}
	
	function get_schedule_report_latest($service_date,$zone_id = NULL)
	{
		$service_week_day = date('w', strtotime($service_date));
		$deletes = $this->get_booking_deletes_by_date($service_date);
		$deleted_bookings = array();
		foreach($deletes as $delete)
		{
			$deleted_bookings[] = $delete->booking_id;
		}		
		$this->db->select("b.booking_id,b.time_to,b.time_from, c.customer_name,GROUP_CONCAT(m.maid_name SEPARATOR ', ') as allmaidss, c.customer_id, c.mobile_number_1, c.email_address,c.customer_added_datetime, b.booking_type, c.customer_source, m.maid_name, SUM(b.total_amount) as total_amount, SUM(b.discount) as discount, NULL AS service_status,st.service_type_name,z.zone_name,a.area_name,ca.building,ca.customer_address,c.balance,b.booking_note,b.pay_by,b.cleaning_material", FALSE)
				->from('bookings b')
				//->join('day_services ds', 'ds.booking_id = b.booking_id','INNER')
				->join('customers c', 'b.customer_id = c.customer_id')
				->join('maids m', 'b.maid_id = m.maid_id')
				->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
				->join('areas a', 'ca.area_id = a.area_id')
				->join('zones z', 'a.zone_id = z.zone_id')
                ->join('service_types st', 'b.service_type_id = st.service_type_id','left')
				->where('b.booking_status', 1)
				->where('b.booking_category', 'C')
				->where('a.area_status', 1)
				->where('z.zone_status', 1)
				->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
				->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
                //->where('m.maid_status', 1)
				->order_by('b.time_from');
		$this->db->group_by(array("b.time_from", "b.time_to", "c.customer_id"));
		if($zone_id != NULL)
		{
			$this->db->where('z.zone_id', $zone_id);
		}
		if($service_date >= date('Y-m-d'))
		{
			$this->db->where('m.maid_status', 1);
		}
		if(count($deleted_bookings) > 0)
		{
			$this->db->where_not_in('b.booking_id', $deleted_bookings);
		}
		$get_schedule_by_date_qry = $this->db->get();
		//echo $this->db->last_query();exit;
		$services = $get_schedule_by_date_qry->result();
		$schedule = array();
        $sch_date=date("d/m/Y",strtotime($service_date));
		foreach ($services as $serv_val)
		{
			$dayservice = $this->checkdayservicevalnew($serv_val->booking_id,$service_date);
			$serv_val->customer_name = $serv_val->customer_name;
			$serv_val->customer_id = $serv_val->customer_id;
			$serv_val->mobile_number_1 = $serv_val->mobile_number_1;
			$serv_val->email_address = $serv_val->email_address;
			$serv_val->booking_type = $serv_val->booking_type;
			$serv_val->customer_source = $serv_val->customer_source;
			$serv_val->maid_name = $serv_val->maid_name;
			$serv_val->total_amount = $serv_val->total_amount;
			$serv_val->discount = $serv_val->discount;
            $serv_val->service_type_name = $serv_val->service_type_name;
            $serv_val->customer_added_datetime = $serv_val->customer_added_datetime;
            $serv_val->building = $serv_val->building;
            $serv_val->customer_address = $serv_val->customer_address;
            $serv_val->balance = $serv_val->balance;
            $serv_val->booking_note = $serv_val->booking_note;
            $serv_val->cleaning_material = $serv_val->cleaning_material;
            $serv_val->pay_by = $serv_val->pay_by;
            $serv_val->sch_date = $sch_date;
            $serv_val->booking_time_from = date("h:i a",strtotime($serv_val->time_from));
            $serv_val->booking_time_to = date("h:i a",strtotime($serv_val->time_to));
            $serv_val->service_time_from = ($dayservice->start_time=='00:00:00'||$dayservice->start_time=='')?'':date("h:i a",strtotime($dayservice->start_time));
            $serv_val->service_time_to = ($dayservice->end_time=='00:00:00'||$dayservice->end_time=='')?'':date("h:i a",strtotime($dayservice->end_time));

			if(count($dayservice) > 0)
			{
				if($dayservice->service_added_by == 'T')
				{
					$serv_val->drivername = $dayservice->driver_name;
				} else {
					$serv_val->drivername = "From Admin";
				}
				$serv_val->service_status = $dayservice->service_status;
				$serv_val->rating = $dayservice->rating;
				$serv_val->comments = $dayservice->comments;
			} else {
				$serv_val->service_status = NULL;
				$serv_val->rating = NULL;
				$serv_val->comments = NULL;
				$serv_val->drivername = NULL;
			}
			array_push($schedule, $serv_val);	
		}
		return $schedule;
	}
	
	function get_schedule_report_latnew($service_date)
	{
		$service_week_day = date('w', strtotime($service_date));
		$deletes = $this->get_booking_deletes_by_date($service_date);
		$deleted_bookings = array();
		foreach($deletes as $delete)
		{
			$deleted_bookings[] = $delete->booking_id;
		}		
		$this->db->select("b.booking_id, c.customer_name, c.mobile_number_1, c.email_address, b.booking_type, c.customer_source, c.customer_added_datetime, m.maid_name, b.total_amount,z.zone_name,z.driver_name, NULL AS service_status,st.service_type_name", FALSE)
				->from('bookings b')
				//->join('day_services ds', 'ds.booking_id = b.booking_id','INNER')
				->join('customers c', 'b.customer_id = c.customer_id')
				->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
				->join('areas a', 'ca.area_id = a.area_id','left')
				->join('zones z', 'a.zone_id = z.zone_id','left')
				->join('maids m', 'b.maid_id = m.maid_id')
                ->join('service_types st', 'b.service_type_id = st.service_type_id','left')
				->where('b.booking_status', 1)
				->where('b.booking_category', 'C')
				->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
				->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
				->order_by('b.time_from');
		if(count($deleted_bookings) > 0)
		{
			$this->db->where_not_in('b.booking_id', $deleted_bookings);
		}
		$get_schedule_by_date_qry = $this->db->get();
		//echo $this->db->last_query();exit;
		$services = $get_schedule_by_date_qry->result();
		$schedule = array();
		foreach ($services as $serv_val)
		{
			$dayservice = $this->checkdayservicevalnew($serv_val->booking_id,$service_date);
			$serv_val->customer_name = $serv_val->customer_name;
			$serv_val->mobile_number_1 = $serv_val->mobile_number_1;
			$serv_val->email_address = $serv_val->email_address;
			$serv_val->booking_type = $serv_val->booking_type;
			$serv_val->customer_source = $serv_val->customer_source;
			$serv_val->maid_name = $serv_val->maid_name;
			$serv_val->total_amount = $serv_val->total_amount;
            $serv_val->service_type_name = $serv_val->service_type_name;
            $serv_val->customer_added_datetime = $serv_val->customer_added_datetime;
			$serv_val->zone = $serv_val->zone_name;
			$serv_val->driver = $serv_val->driver_name;
			$serv_val->total_amount = $serv_val->total_amount;
			if(count($dayservice) > 0)
			{
				$serv_val->service_status = $dayservice->service_status;
				$serv_val->rating = $dayservice->rating;
				$serv_val->comments = $dayservice->comments;
				$serv_val->serviceadd = $dayservice->service_added_by;
				$serv_val->serviceaddby = $dayservice->driver_name;
			} else {
				$serv_val->service_status = NULL;
				$serv_val->rating = NULL;
				$serv_val->comments = NULL;
				$serv_val->serviceadd = NULL;
				$serv_val->serviceaddby = NULL;
			}
			array_push($schedule, $serv_val);	
		}
		return $schedule;
	}
	
	function checkdayservicevalnew($booking_id, $service_date)
	{
		$this->db->select("ds.service_status,ds.rating,ds.comments,ds.service_added_by,ds.service_added_by_id,ds.start_time,ds.end_time,z.driver_name", FALSE)
					->from('day_services ds')
					->join('tablets t', 'ds.service_added_by_id = t.tablet_id','LEFT')
					->join('zones z', 'z.zone_id = t.zone_id','LEFT')
					->where('ds.service_date',$service_date)
					->where('ds.booking_id',$booking_id);
		$transfer_zone_qry = $this->db->get();
		//echo $this->db->last_query();exit;
		return $transfer_zone_qry->row(); 
	}
	
	function checkdayserviceval($booking_id, $service_date)
	{
		$this->db->select("ds.service_status,ds.rating,ds.comments,ds.service_added_by,ds.service_added_by_id", FALSE)
					->from('day_services ds')
					->where('ds.service_date',$service_date)
					->where('ds.booking_id',$booking_id);
		$transfer_zone_qry = $this->db->get();
		//echo $this->db->last_query();exit;
		return $transfer_zone_qry->row(); 
	}
	
	function get_schedule_report_new($service_date)
	{
		$service_week_day = date('w', strtotime($service_date));
		$deletes = $this->get_booking_deletes_by_date($service_date);
		$deleted_bookings = array();
		foreach($deletes as $delete)
		{
			$deleted_bookings[] = $delete->booking_id;
		}
				
		$this->db->select("c.customer_name, c.mobile_number_1, c.email_address, b.booking_type, c.customer_source, m.maid_name, b.total_amount, NULL AS service_status", FALSE)
				->from('bookings b')
				->join('customers c', 'b.customer_id = c.customer_id')
				->join('maids m', 'b.maid_id = m.maid_id')
				->where('b.booking_category', 'C')
				->where('b.booking_status', 1)
				->where('m.maid_status', 1)
				->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
				->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
				->order_by('b.time_from')
				->group_by('b.booking_id');
		
		if(count($deleted_bookings) > 0)
		{
			$this->db->where_not_in('b.booking_id', $deleted_bookings);
		}
		
		$get_schedule_by_date_qry = $this->db->get();
		
		$services = $get_schedule_by_date_qry->result();
                
		$this->db->select("ds.service_status", FALSE)
				->from('day_services ds')
				->where('ds.service_date', $service_date);
		$transfer_zone_qry = $this->db->get();
		$transfer_zone = $transfer_zone_qry->result(); 
		$schedule = array();
		foreach ($services as $service)
		{
			if(!empty($transfer_zone))
			{
				$service->customer_name = $service->customer_name;
				$service->mobile_number_1 = $service->mobile_number_1;
				$service->email_address = $service->email_address;
				$service->booking_type = $service->booking_type;
				$service->customer_source = $service->customer_source;
				$service->maid_name = $service->maid_name;
				$service->total_amount = $service->total_amount;
				$service->service_status = $transfer->service_status;
			}
			else
			{
				$service->customer_name = $service->customer_name;
				$service->mobile_number_1 = $service->mobile_number_1;
				$service->email_address = $service->email_address;
				$service->booking_type = $service->booking_type;
				$service->customer_source = $service->customer_source;
				$service->maid_name = $service->maid_name;
				$service->total_amount = $service->total_amount;
				$service->service_status = NULL;
			}
			array_push($schedule, $service);
		}
		return $schedule;
	}
	function schedule_report_service_wise($service_date,$zone_id = NULL,$filter_service_type_id,$filter_maid_id)
	{
		$service_week_day = date('w', strtotime($service_date));
		$deletes = $this->get_booking_deletes_by_date($service_date);
		$deleted_bookings = array();
		foreach($deletes as $delete)
		{
			$deleted_bookings[] = $delete->booking_id;
		}		
		$this->db->select("b.booking_id,b.time_to,b.time_from, c.customer_name,GROUP_CONCAT(m.maid_name SEPARATOR ', ') as allmaidss, b.maid_id, c.customer_id, c.mobile_number_1, c.email_address,c.customer_added_datetime, b.booking_type, c.customer_source, m.maid_name, SUM(b.total_amount) as total_amount, SUM(b.discount) as discount, NULL AS service_status,st.service_type_name,st.service_type_id,z.zone_name,a.area_name,ca.building,ca.customer_address,c.balance,b.booking_note,b.pay_by,b.cleaning_material", FALSE)
				->from('bookings b')
				//->join('day_services ds', 'ds.booking_id = b.booking_id','INNER')
				->join('customers c', 'b.customer_id = c.customer_id')
				->join('maids m', 'b.maid_id = m.maid_id')
				->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id','left')
				->join('areas a', 'ca.area_id = a.area_id','left')
				->join('zones z', 'a.zone_id = z.zone_id','left')
                ->join('service_types st', 'b.service_type_id = st.service_type_id','left')
				->where('b.booking_status', 1)
				->where('b.booking_category', 'C')
				// ->where('a.area_status', 1)
				// ->where('z.zone_status', 1)
				->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
				->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
                //->where('m.maid_status', 1)
				->order_by('b.time_from')
                //->group_by(array("b.time_from", "b.time_to", "c.customer_id"));
                ->group_by("b.booking_id");
		if($zone_id != NULL)
		{
			$this->db->where('z.zone_id', $zone_id);
		}
		if($service_date >= date('Y-m-d'))
		{
			$this->db->where('m.maid_status', 1);
		}
        if($filter_service_type_id != NULL)
		{
			$this->db->where('b.service_type_id', $filter_service_type_id);
		}
        if($filter_maid_id != NULL)
		{
			$this->db->where('b.maid_id', $filter_maid_id);
		}
		if($service_date >= date('Y-m-d'))
		{
			$this->db->where('m.maid_status', 1);
		}
		if(count($deleted_bookings) > 0)
		{
			$this->db->where_not_in('b.booking_id', $deleted_bookings);
		}
		$get_schedule_by_date_qry = $this->db->get();
		//echo $this->db->last_query();exit;
		$services = $get_schedule_by_date_qry->result();
		$schedule = array();
        $sch_date=date("d/m/Y",strtotime($service_date));
		foreach ($services as $serv_val)
		{
			$dayservice = $this->checkdayservicevalnew($serv_val->booking_id,$service_date);
			$serv_val->customer_name = $serv_val->customer_name;
			$serv_val->customer_id = $serv_val->customer_id;
			$serv_val->mobile_number_1 = $serv_val->mobile_number_1;
			$serv_val->email_address = $serv_val->email_address;
			$serv_val->booking_type = $serv_val->booking_type;
			$serv_val->customer_source = $serv_val->customer_source;
			$serv_val->maid_name = $serv_val->maid_name;
			$serv_val->total_amount = $serv_val->total_amount;
			$serv_val->discount = $serv_val->discount;
            $serv_val->service_type_name = $serv_val->service_type_name;
            $serv_val->customer_added_datetime = $serv_val->customer_added_datetime;
            $serv_val->building = $serv_val->building;
            $serv_val->customer_address = $serv_val->customer_address;
            $serv_val->balance = $serv_val->balance;
            $serv_val->booking_note = $serv_val->booking_note;
            $serv_val->cleaning_material = $serv_val->cleaning_material;
            $serv_val->pay_by = $serv_val->pay_by;
            $serv_val->sch_date = $sch_date;
            $serv_val->booking_time_from = date("h:i a",strtotime($serv_val->time_from));
            $serv_val->booking_time_to = date("h:i a",strtotime($serv_val->time_to));
            $serv_val->service_time_from = ($dayservice->start_time=='00:00:00'||$dayservice->start_time=='')?'':date("h:i a",strtotime($dayservice->start_time));
            $serv_val->service_time_to = ($dayservice->end_time=='00:00:00'||$dayservice->end_time=='')?'':date("h:i a",strtotime($dayservice->end_time));

			if(count($dayservice) > 0)
			{
				if($dayservice->service_added_by == 'T')
				{
					$serv_val->drivername = $dayservice->driver_name;
				} else {
					$serv_val->drivername = "From Admin";
				}
				$serv_val->service_status = $dayservice->service_status;
				$serv_val->rating = $dayservice->rating;
				$serv_val->comments = $dayservice->comments;
			} else {
				$serv_val->service_status = NULL;
				$serv_val->rating = NULL;
				$serv_val->comments = NULL;
				$serv_val->drivername = NULL;
			}
			array_push($schedule, $serv_val);	
		}
		return $schedule;
	}
	
	function getservicestatus($booking_id,$service_date)
	{
		$this->db->select("ds.service_status", FALSE)
				->from('day_services ds')
				->where('ds.booking_id', $booking_id)
				->where('ds.service_date', $service_date);
		$transfer_zone_qry = $this->db->get();
		$transfer_zone = $transfer_zone_qry->row(); 
		return $transfer_zone;
	}

    function get_bookingremarks_report_new($date = NULL,$date_to = NULL) {
        if (!$date) {
            $date = date('Y-m-d');
        }
		if (!$date_to) {
            $date_to = date('Y-m-d');
        }

        $query = $this->db->query('SELECT bk_del.id,bk_del.remarks,bk_del.service_date, bk_del.deleted_date_time AS added,m.maid_name,bk.booking_id,bk.customer_address_id,bk.booking_type,c.customer_name,c.payment_type,CONCAT(TIME_FORMAT(bk.time_from,"%h:%i%p")," - ",TIME_FORMAT(bk.time_to,"%h:%i%p")) AS shift,us.username,us.user_fullname,bk.booked_datetime

                            FROM booking_delete_remarks bk_del 
                            JOIN bookings bk ON bk_del.booking_id=bk.booking_id
                            JOIN maids m ON m.maid_id=bk.maid_id
                            JOIN customer_addresses c_aadr ON c_aadr.customer_address_id=bk.customer_address_id 
                            JOIN customers c ON c.customer_id=bk.customer_id  AND bk.customer_address_id = c_aadr.customer_address_id
                            JOIN users us ON us.user_id=bk_del.deleted_by                             
                            WHERE DATE(bk_del.deleted_date_time) between "' . $date.'" AND "' . $date_to.'"');
        return $query->result();
    }

    function get_complaints_reports($from_date,$to_date) 
    {
            // $this->db->select('ds.customer_name,m.maid_name,ds.service_date,ds.rating,ds.comments')
            //         ->from('day_services ds')
            //                         ->join('maids m', 'ds.maid_id = m.maid_id');
            $this->db->select('c.customer_name,ca.customer_address,b.booked_datetime,cn.complaint_type,cn.customer_comments,m.maid_name,cn.status,cn.complaint,cn.cleaner_comments,cn.action_taken,cn.service_date,zones.driver_name,cn.added_time,cn.image,cn.deduction_amount')
                    ->from('bookings b')
                    ->join('customers c', 'c.customer_id = b.customer_id')
                    ->join('complaints_new cn', 'cn.booking_id = b.booking_id')
                    ->join('customer_addresses ca', 'ca.customer_address_id = b.customer_address_id')
                    ->join('maids m', 'm.maid_id = b.maid_id')
                    ->join('areas', 'ca.area_id = areas.area_id')
                    ->join('zones', 'areas.zone_id = zones.zone_id');
            if ($from_date != "") {
                            $this->db->where('cn.created_date >=',$from_date);
            }
            
            if ($to_date != "") {
                            $this->db->where('cn.created_date <=',$to_date);
            }
            
            
            $query = $this->db->get();
            //echo $this->db->last_query();
            return $query->result();
        }
    
	
}
