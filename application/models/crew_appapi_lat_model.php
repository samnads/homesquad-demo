<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Crew_appapi_lat_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
	
	public function get_maid_login($username, $password) 
	{
        $this->db->select('*')
                ->from('maids')
                ->where('username', $username)
                ->where('password', $password)
                ->where('maid_status', 1)
                ->limit(1);
        $user = $this->db->get();
        return $user->row();
    }
	
	function update_maid($maid_id, $fields = array()) {
        $this->db->where('maid_id', $maid_id);
        $this->db->update('maids', $fields);

        return $this->db->affected_rows();
    }
	
	public function loginTabletNew($username, $password) 
    {
        $this->db->select("t.*,z.driver_name,z.zone_name,z.zone_name", FALSE)
                ->from('tablets t')
                ->join('zones z', 't.zone_id = z.zone_id', 'left')
                ->where('t.tablet_username', $username)
                ->where('t.tablet_password', $password)
                ->where('t.tablet_status', 1);

        $loginTabletNew_query = $this->db->get();

        return $loginTabletNew_query->row();
    }
	
	function update_tablet($tablet_id, $fields = array())
	{
        $this->db->where('tablet_id', $tablet_id);
        $this->db->update('tablets', $fields);

        return $this->db->affected_rows();
    }
	
	function token_tablet_check($user_id, $token)
    {
        $this->db->select('tablet_id')
                ->from('tablets')  
                ->where("tablet_id", $user_id)
                ->where('tablet_login_token', $token)
                ->where('tablet_status', 1);
        $get_qry = $this->db->get();
        return $get_qry->row();
    }
	
	function token_maid_check($user_id, $token)
    {
        $this->db->select('maid_id')
                ->from('maids')  
                ->where("maid_id", $user_id)
                ->where('maid_login_token', $token)
                ->where('maid_status', 1);
        $get_qry = $this->db->get();
        return $get_qry->row();
    }
	
	function get_schedule_by_date_for_tab_by_maid_id($service_date,$maid_id) 
	{
        $service_week_day = date('w', strtotime($service_date));
        $deletes = $this->get_booking_deletes_by_date($service_date);
        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }
		$this->db->select("b.booking_id,b.pay_by,b.vaccum_cleaning,b.total_amount,b.reference_id, b.cleaning_material, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.discount, b.booking_status, b.interior_window_clean, b.fridge_cleaning, b.ironing_services, b.oven_cleaning, c.customer_name, c.customer_nick_name, c.mobile_number_1,c.mobile_number_2, c.key_given, c.payment_type, ca.longitude, c.payment_mode, ca.latitude, c.price_hourly, c.price_extra, c.price_weekend, c.customer_source, c.balance, c.customer_notes, z.zone_id, z.zone_name, a.area_name, ca.customer_address, ca.building, ca.unit_no, ca.street, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, c.customer_booktype,u.user_fullname,z.driver_name,st.service_type_name,if(ds.payment_status is Null,0,ds.payment_status) as payment_status,cp.payment_method", FALSE)
                ->from('bookings b')
                ->join('customers c', 'b.customer_id = c.customer_id')
				->join('day_services ds', 'ds.booking_id = b.booking_id','left')
				->join('customer_payments cp', 'cp.day_service_id = ds.day_service_id','left')
				->join('maids m', 'b.maid_id = m.maid_id')
                ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id')
                ->join('users u', 'b.booked_by = u.user_id')
                ->join('service_types st','b.service_type_id = st.service_type_id','left')
                ->where('b.booking_status', 1)
                ->where('m.maid_status', 1)
                ->where('m.maid_id', $maid_id)
                ->where('a.area_status', 1)
                ->where('z.zone_status', 1)
                ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
                ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
                ->order_by('b.time_from')->group_by('b.booking_id');

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();
        return $get_schedule_by_date_qry->result();
    }
	
	function get_day_service_by_booking_id($date, $booking_id)
	{
		$this->db->select('ds.day_service_id, ds.service_date, ds.start_time, ds.end_time, ds.service_added_by_id, ds.invoice_status, ds.serv_invoice_id, z.zone_id, b.maid_id, ds.booking_id, ds.total_fee, ds.service_status,ds.payment_status, c.customer_id, c.payment_type, c.is_company, c.rating_mail, cp.paid_amount,cp.verified_status')
				->from('day_services ds')
				->join('bookings b', 'ds.booking_id = b.booking_id')
				->join('customers c', 'b.customer_id = c.customer_id')
                ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')				
				->join('areas a', 'ca.area_id = a.area_id')
				->join('zones z', 'a.zone_id = z.zone_id')
				->join('customer_payments cp', 'ds.day_service_id = cp.day_service_id','left')
				->where('ds.booking_id', $booking_id)
				->where('ds.service_date', $date)
				->limit(1);
		
		$get_day_service_by_booking_id_qry = $this->db->get();
		$res = $get_day_service_by_booking_id_qry->row();
		if($res)
		$res->payment_status=$this->payment_mode($res->day_service_id,$res->booking_id);
		return $res;
		}

		function payment_mode($ds_id,$b_id){
			if($this->db->from('day_services')->where('day_service_id',$ds_id)->get()->row()->payment_status == 0)
			return 1;
			else{
				$cp=$this->db->from('customer_payments')->where('day_service_id',$ds_id)->get()->row()->payment_method;
				if($cp)
				return 0;
				else 
				return 2;
			}
			// "0" - Paid [cash/cheque]
			// "1" - Not Paid
			// "2" - Paid(Online- prepaid) [online] 
		}
	function get_maid_attandence_by_date($maid_id, $date)
	{
		$this->db->select('attandence_id, maid_id, zone_id, tablet_id, date, maid_in_time, maid_out_time, attandence_status')
				->from('maid_attandence')
				->where('maid_id', $maid_id)
				->where('date', $date)
				->order_by('maid_in_time', 'desc')
				->limit(1);
		
		$get_maid_attandence_by_date_qry = $this->db->get();
		return $get_maid_attandence_by_date_qry->row();
	}
	
	function get_booking_exist($booking_id,$service_date)
	{
		$this->db->select("just_mop_ref,mop", FALSE)
				->from('justmop_ref')
				->where('service_date',$service_date)
				->where('booking_id',$booking_id);
		$get_schedule_by_date_qry = $this->db->get();
		return $get_schedule_by_date_qry->row();
	}
	
	public function get_related_bookings($ref_id, $booking_id,$service_date) 
    {
    	$this->db->select("b.booking_id,b.customer_id,c.customer_name,b.maid_id,b.reference_id,ma.attandence_status,m.maid_id,m.maid_name,m.maid_nationality,m.maid_photo_file", FALSE)
					->from('bookings b')
					->join('maid_attandence ma', "b.maid_id = ma.maid_id AND ma.date='$service_date'", 'left')
					->join('maids m', "b.maid_id = m.maid_id")
					->join('customers c', 'b.customer_id = c.customer_id')
					->where('b.booking_id !=', $booking_id)
					->where('b.reference_id', $ref_id);

	    $get_related_bookings_qry = $this->db->get();	
	    		
		return $get_related_bookings_qry->result();
    }
	
	function get_tablet_by_tabletid($tabid)
	{
        $this->db->select('tablets.tablet_id, tablets.zone_id, tablets.imei, tablets.google_reg_id, tablets.access_code, tablets.tablet_status,tablets.tablet_login_token, zones.driver_name')
                ->from('tablets')
                ->join('zones', 'tablets.zone_id = zones.zone_id', 'left') 
                ->where('tablet_id', $tabid)
                ->limit(1);

        $get_tab_by_imei_qry = $this->db->get();
        return $get_tab_by_imei_qry->row();
    }
	
	function get_schedule_by_date_for_tab($service_date)
	{
		$service_week_day = date('w', strtotime($service_date));
		$deletes = $this->get_booking_deletes_by_date($service_date);
		
		$deleted_bookings = array();
		foreach($deletes as $delete)
		{
			$deleted_bookings[] = $delete->booking_id;
		}
				
		$this->db->select("b.booking_id,b.pay_by,b.vaccum_cleaning,b.reference_id,b.total_amount, b.cleaning_material, b.interior_window_clean, b.fridge_cleaning, b.ironing_services, b.oven_cleaning, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.tabletid, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.discount, b.booking_status, c.customer_name, c.customer_nick_name, c.mobile_number_1,c.mobile_number_2, c.key_given, c.payment_type, c.payment_mode, c.price_hourly, c.price_extra, c.price_weekend, c.customer_source, c.balance, c.customer_notes, z.zone_id, z.zone_name, a.area_name, ca.customer_address, ca.building, ca.unit_no, ca.street, ca.latitude, ca.longitude, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, c.customer_booktype,u.user_fullname,m.driver_name as driver,st.service_type_name,if(ds.payment_status is Null,0,ds.payment_status) as payment_status,cp.payment_method", FALSE)
				->from('bookings b')
				->join('customers c', 'b.customer_id = c.customer_id')
				
				->join('day_services ds', 'ds.booking_id = b.booking_id','left')
				->join('customer_payments cp', 'cp.day_service_id = ds.day_service_id','left')->join('maids m', 'b.maid_id = m.maid_id')
				->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
				->join('areas a', 'ca.area_id = a.area_id')
				->join('zones z', 'a.zone_id = z.zone_id')
                ->join('users u','b.booked_by = u.user_id')
                ->join('service_types st','b.service_type_id = st.service_type_id','left')
				->where('b.booking_category', 'C')
				->where('b.booking_status', 1)
				->where('m.maid_status', 1)
				->where('a.area_status', 1)
				->where('z.zone_status', 1)
				->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
				->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
				->order_by('b.time_from')->group_by('b.booking_id')
				;
		
		if(count($deleted_bookings) > 0)
		{
			$this->db->where_not_in('b.booking_id', $deleted_bookings);
		}
		
		$get_schedule_by_date_qry = $this->db->get();
		return $get_schedule_by_date_qry->result();
	}
	
	function get_booking_deletes_by_date($service_date, $service_end_date = NULL)
	{
		$this->db->select('booking_id')
				->from('booking_deletes');
		if($service_end_date != NULL)
		{
			$this->db->where("service_date BETWEEN '$service_date' AND '$service_end_date'");
		}
		else
		{
			$this->db->where("service_date", $service_date);
		}		
		
		$get_booking_deletes_by_date_qry = $this->db->get();
		
		return $get_booking_deletes_by_date_qry->result();
	}
	
	function get_booking_transfers_by_date($service_date)
	{
		$this->db->select('booking_transfer_id, booking_id, zone_id')
				->from('booking_transfers')
				->where('service_date', $service_date)
				->group_by('booking_id')
				->order_by('transferred_time', 'desc');
		
		$get_booking_transfers_by_date_qry = $this->db->get();
		
		return $get_booking_transfers_by_date_qry->result();
	}
	
	function get_customer_address_by_id($customer_address_id)
	{
        $this->db->select('ca.customer_address_id, ca.customer_id, ca.area_id, ca.customer_address, ca.building, ca.unit_no, ca.street, a.area_name, z.zone_id, z.zone_name')
                ->from('customer_addresses ca')
                ->where('ca.customer_address_id', $customer_address_id)
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id')
                ->order_by('ca.customer_address_id');

        $get_customer_address_by_id_qry = $this->db->get();

        return $get_customer_address_by_id_qry->row();
    }
	
	function get_customer_by_id($customer_id)
	{
        $this->db->select("customer_id, odoo_customer_id, odoo_synch_status, customer_name,customer_username, customer_password, customer_nick_name, mobile_number_1, mobile_number_2, mobile_number_3, phone_number, fax_number, email_address, website_url, customer_photo_file, customer_booktype, customer_type, contact_person, payment_type, payment_mode, price_hourly, price_extra, price_weekend, latitude, longitude, key_given, customer_notes, DATE_FORMAT(customer_added_datetime, '%d / %M / %Y %h:%i %p') AS added_datetime, customer_status,balance, signed", FALSE)
                ->from('customers')
                ->where('customer_id', $customer_id)
                ->limit(1);

        $get_customer_by_id_qry = $this->db->get();
        return $get_customer_by_id_qry->row();
    }
	
	function get_area_by_id($area_id)
	{
		$this->db->select('a.area_id, a.zone_id, a.area_name, a.area_charge, a.area_status, z.zone_name')
				->from('areas a')
				->join('zones z', 'a.zone_id = z.zone_id')
				->where('a.area_id', $area_id);

		$get_area_by_id_qry = $this->db->get();
		
		return $get_area_by_id_qry->row();
	}
	
	function get_schedule_payments_by_date_for_tab_by_maid_id($service_date,$maid_id) 
	{
		$this->db->select("b.booking_id,b.total_amount,b.reference_id, b.cleaning_material, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.discount, b.booking_status, c.customer_name, c.customer_nick_name, c.mobile_number_1,c.mobile_number_2, c.key_given, c.payment_type, ca.longitude, c.payment_mode, ca.latitude, c.price_hourly, c.price_extra, c.price_weekend, c.customer_source, z.zone_id, z.zone_name, a.area_name, ca.customer_address, ca.building, ca.unit_no, ca.street, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, c.customer_booktype,u.user_fullname,z.driver_name,cp.paid_amount", FALSE)
				 ->from('day_services ds')
				 ->join('customer_payments cp', "ds.day_service_id = cp.day_service_id")
				 ->join('bookings b', "ds.booking_id = b.booking_id")
				 ->join('customers c', 'b.customer_id = c.customer_id')
                 ->join('maids m', 'b.maid_id = m.maid_id')
                 ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
                 ->join('areas a', 'ca.area_id = a.area_id')
                 ->join('zones z', 'a.zone_id = z.zone_id')
                 ->join('users u', 'b.booked_by = u.user_id')
				 ->where('ds.maid_id', $maid_id)
				 ->where('ds.payment_status', '1')
				 ->where('ds.service_date', $service_date);

	    $get_schedule_payments_qry = $this->db->get();		    		
		return $get_schedule_payments_qry->result();
	}
	
	function get_schedule_payments_by_date_for_tab($service_date)
	{
		$service_week_day = date('w', strtotime($service_date));
		$deletes = $this->get_booking_deletes_by_date($service_date);
		
		$deleted_bookings = array();
		foreach($deletes as $delete)
		{
			$deleted_bookings[] = $delete->booking_id;
		}		

		$this->db->select("b.booking_id,b.total_amount,b.reference_id, b.cleaning_material, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.discount, b.booking_status, c.customer_name, c.customer_nick_name, c.mobile_number_1,c.mobile_number_2, c.key_given, c.payment_type, ca.longitude, c.payment_mode, ca.latitude, c.price_hourly, c.price_extra, c.price_weekend, c.customer_source, z.zone_id, z.zone_name, a.area_name, ca.customer_address, ca.building, ca.unit_no, ca.street, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, c.customer_booktype,u.user_fullname,z.driver_name,cp.paid_amount,b.tabletid", FALSE)
				 ->from('day_services ds')
				 ->join('customer_payments cp', "ds.day_service_id = cp.day_service_id")
				 ->join('bookings b', "ds.booking_id = b.booking_id")
				 ->join('customers c', 'b.customer_id = c.customer_id')
                 ->join('maids m', 'b.maid_id = m.maid_id')
                 ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
                 ->join('areas a', 'ca.area_id = a.area_id')
                 ->join('zones z', 'a.zone_id = z.zone_id')
                 ->join('users u', 'b.booked_by = u.user_id','left')
				 ->where('ds.payment_status', '1')
				 ->where('b.booking_status !=', '2')
				 ->where('ds.service_date', $service_date);
		
		if(count($deleted_bookings) > 0)
		{
			$this->db->where_not_in('b.booking_id', $deleted_bookings);
		}
		
		$get_schedule_payments_by_date_for_tab_qry = $this->db->get();

		return $get_schedule_payments_by_date_for_tab_qry->result();
	}
	
	function get_booking_by_id($booking_id)
	{
		$this->db->select("b.time_from, b.time_to, z.zone_id, b.booking_id, b.reference_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.justmop_reference, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_actual_end_date, b.booking_status, b.booking_note, b.price_per_hr, b.discount, b.service_charge, b.vat_charge, b.total_amount, b.pending_amount, c.customer_name,c.mobile_number_1, c.email_address, c.payment_type, c.price_hourly, c.price_extra, c.price_weekend, ca.customer_address, ca.building, ca.unit_no, ca.street, a.area_name,ca.area_id, b.is_locked, b.booked_by, m.maid_name, b.service_end_date, b.cleaning_material,b.price_per_hr,b.booked_from,b.pay_by,u.is_admin,c.balance,c.signed,DATE_FORMAT(b.time_from, '%h:%i %p') AS newtime_from, DATE_FORMAT(b.time_to, '%h:%i %p') AS newtime_to", FALSE)
				->from('bookings b')
				->join('customers c', 'b.customer_id = c.customer_id')
				->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
                ->join('areas a', 'ca.area_id = a.area_id','left')
				->join('zones z', 'a.zone_id = z.zone_id','left')
				->join('maids m',  'b.maid_id = m.maid_id', 'left')
                ->join('users u', 'b.booked_by = u.user_id', 'left')
				->where('b.booking_id', $booking_id)
				->where('b.booking_category', 'C')
				->limit(1);
		
		$get_bookings_by_id_qry = $this->db->get();
		
		return $get_bookings_by_id_qry->row();
	}
	
	function update_maids($data,$maid_id)
	{
		$this->db->where('maid_id',$maid_id);
		$this->db->update('maids',$data);
	}
	
	function get_maid_details($maid_id)
    {
        $this->db->select('maids.*, flats.flat_name, teams.team_name')
                ->from('maids')
                ->join('flats','maids.flat_id = flats.flat_id','left')
                ->join('teams','maids.team_id = teams.team_id','left')
                ->where('maids.maid_id', $maid_id);
        $query = $this->db->get();
        return $query->row();   
    }
	
	function add_maid_attandence($fields = array())
	{
		$this->db->set($fields);
		$this->db->insert('maid_attandence'); 
		return $this->db->insert_id();
	}
	
	function update_maid_attandence($attandence_id, $fields = array())
	{
		$this->db->where('attandence_id', $attandence_id);
		$this->db->update('maid_attandence', $fields); 

		return $this->db->affected_rows();
	}
	
	function get_all_tablets($active_only = TRUE, $no_spare = TRUE)
	{
        $this->db->select('tablet_id, tablets.zone_id, zone_name,driver_name, zone_nick, imei, google_reg_id, access_code, tablet_status')
                ->from('tablets')
                ->join('zones', 'tablets.zone_id = zones.zone_id')
                ->where('tablet_status', 1);

        $get_tab_by_qry = $this->db->get();

        return $get_tab_by_qry->result();
    }
	
	function get_zone_by_id($zone_id)
	{
		$this->db->select('zone_id, zone_name, driver_name, spare_zone, zone_status')
				 ->from('zones')
				 ->where('zone_id', $zone_id)
				 ->limit(1);
		
		$get_zone_by_id_qry = $this->db->get();
		
		return $get_zone_by_id_qry->row();
	}
	
	function get_maid_bookings_on_zone_by_date($maid_id, $zone_id, $service_date)
	{
		$service_week_day = date('w', strtotime($service_date));
		
		$deletes = $this->get_booking_deletes_by_date($service_date);
		
		$deleted_bookings = array();
		foreach($deletes as $delete)
		{
			$deleted_bookings[] = $delete->booking_id;
		}
				
		$this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.booking_status, c.customer_name, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality", FALSE)
				->from('bookings b')
				->join('customers c', 'b.customer_id = c.customer_id')
				->join('maids m', 'b.maid_id = m.maid_id')
				->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
				->join('areas a', 'ca.area_id = a.area_id')
				->join('zones z', 'a.zone_id = z.zone_id')
				->where('b.booking_status', 1)
				->where('b.booking_category', 'C')
				->where('a.area_status', 1)
				->where('z.zone_status', 1)
				->where('b.maid_id', $maid_id)
				->where('a.zone_id', $zone_id)
				->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
				->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE);
		
		if(count($deleted_bookings) > 0)
		{
			$this->db->where_not_in('b.booking_id', $deleted_bookings);
		}
		
		$get_maid_bookings_on_zone_by_date_qry = $this->db->get();
		return $get_maid_bookings_on_zone_by_date_qry->result();
	}
	
	function add_booking_transfer($fields = array())
	{
		$this->db->set($fields);
		$this->db->insert('booking_transfers'); 
                
		return $this->db->insert_id();
	}
	
	function get_tablet_by_zone($zone_id) {
        $this->db->select('tablet_id, zone_id, imei, google_reg_id, access_code, tablet_status')
                ->from('tablets')
                ->where('zone_id', $zone_id)
                ->where('flat_id', 0)
                ->where('tablet_status', 1)
                ->limit(1);

        $get_tablet_by_zone_qry = $this->db->get();

        return $get_tablet_by_zone_qry->row();
    }
	
	function delete_booking_transfer($booking_transfer_id)
	{
		$this->db->where('booking_transfer_id', $booking_transfer_id);
		$this->db->delete('booking_transfers'); 
	}
	
	function get_booking_transfers_by_date_and_booking_id($service_date,$booking_id)
    {
        $this->db->select('booking_transfer_id, booking_id, zone_id')
                ->from('booking_transfers')
                ->where('service_date', $service_date)
                ->where('booking_id', $booking_id)
                ->group_by('booking_id')
                ->order_by('transferred_time', 'desc');
        
        $get_booking_transfers_by_date_qry = $this->db->get();
        
        return $get_booking_transfers_by_date_qry->result();
    }
	
	function add_day_service($fields = array())
	{
		$this->db->set($fields);
		$this->db->insert('day_services'); 
		return $this->db->insert_id();
	}
	
	function add_customer_payment($fields = array()) {
        $fields['paid_datetime'] = isset($fields['paid_datetime']) ? $fields['paid_datetime'] : date('Y-m-d H:i:s');

        $this->db->set($fields);
        $this->db->insert('customer_payments');
        return $this->db->insert_id();
    }
	
	function update_day_service($day_service_id, $fields = array())
	{
		$this->db->where('day_service_id', $day_service_id);
		$this->db->update('day_services', $fields); 

		return $this->db->affected_rows();
	}
	
	function update_customers($data, $customer_id) {
        $this->db->where('customer_id', $customer_id);
        $this->db->update('customers', $data);
           return $this->db->affected_rows();
    }
		
	function get_all_maids($active = 2)
	{
		$this->db->select('maids.*, flats.flat_name')
		 ->from('maids')
				 ->join('flats', 'maids.flat_id = flats.flat_id', 'left')
				 ->order_by('maids.maid_name');
				 //->order_by('maids.maid_added_datetime');
		if ($active == 2) {
			$this->db->where('maids.maid_status', 1);
		} else if ($active == 3) {
			$this->db->where('maids.maid_status', 0);
		
		} else if ($active == 4) {
			$this->db->where('maids.maid_status', 2);
		}
		$query = $this->db->get();
		return $query->result_array();    
	}
	
	function get_maid_notifications_by_date($maid_id,$notification_date) 
	{
        $this->db->select('*')
                ->from('push_notications');
        $this->db->where("maid_id", $maid_id);
        $this->db->where('DATE(addeddate)', $notification_date);
        $this->db->order_by('id','desc');


        $maid_notifications_qry = $this->db->get();

        return $maid_notifications_qry->result();
    }
	
	function get_driver_notifications_by_date($tablet_id,$notification_date) 
	{
        $this->db->select('*')
                ->from('push_notications');
        $this->db->where("tab_id", $tablet_id);
        $this->db->where('DATE(addeddate)', $notification_date);
        $this->db->order_by('id','desc');


        $driver_notifications_qry = $this->db->get();

        return $driver_notifications_qry->result();
    }
	
	function add_tablet_locations($fields = array()) {
        $this->db->set($fields);
        if ($this->db->insert('tablet_locations'))
            return $this->db->insert_id();
        else
            return FALSE;
    }
	
	function get_maid_by_id($maid_id)
	{
		$this->db->select("maid_id, maid_name, maid_gender, maid_nationality, maid_present_address, maid_permanent_address, maid_mobile_1, maid_mobile_2, flat_id, maid_photo_file, maid_passport_number, maid_passport_expiry_date, maid_passport_file, maid_visa_number, maid_visa_expiry_date, maid_visa_file, maid_labour_card_number, maid_labour_card_expiry_date, maid_labour_card_file, maid_notes,maid_login_token, odoo_maid_id, odoo_synch_status, odoo_new_maid_id, odoo_new_maid_status, maid_status, maid_added_datetime", FALSE)
				->from('maids')
				->where('maid_id', $maid_id)
				->limit(1);
		
		$get_maid_by_id_qry = $this->db->get();
		
		return $get_maid_by_id_qry->row();
	}
	
	function get_device_tokens() {
        $this->db->select('device_token')
                ->from('ipad_login');

        $get_device_tokens_qry = $this->db->get();

        return $get_device_tokens_qry->result();
    }
	
	function get_all_drivertablets()
	{
        $this->db->select('tablet_id,tablet_driver_name, tablet_username, tablet_password, tablet_status')
                ->from('tablets')
                ->where('tablet_status', 1);

        $get_tab_by_qry = $this->db->get();

        return $get_tab_by_qry->result();
    }
	
	public function check_booking_transferred_new($booking_id,$service_date)
	{
		 $this->db->select("bft.booking_transfer_tablet_id,t.tablet_driver_name",FALSE)
					->from('booking_transfers_tablet bft')
					->join('tablets t','bft.transfering_to_tablet = t.tablet_id');
		 $this->db->where('bft.booking_id', $booking_id);
		 $this->db->where('bft.service_date', $service_date);
		 $this->db->order_by('bft.booking_transfer_tablet_id','DESC');
		 $this->db->limit(1);
		 $get_results_query = $this->db->get();
		 return $get_results_query->row();
	}
	function insert_activity($insdata){
		$data='';
			$insdata =[
				'added_user'=>$data,
				'booking_type'=>$data,
				'user_type'=>$data,
				'transferred_bookid'=>$data,
				'action_type'=>$data,
				'action_content'=>$data,
				'addeddate'=>$data,
				'date_time_added'=>$data,
			];
			$this->db->set($insdata);
			$this->db->insert('user_activity');
	}
	
	function insert_driverchnage($data,$transferids,$user_type="A")
	{
		$this->db->set($data);
		$this->db->insert('booking_transfers_tablet');
		$insert_id = $this->db->insert_id();
		if($insert_id > 0)
		{
			if($transferids !=0)
			{
				$this->db->where('booking_transfer_tablet_id',$transferids);
				$this->db->delete('booking_transfers_tablet');
			}
			$activity_add = array();
			$activity_add['added_user'] = $data['transferred_by'];
			$activity_add['booking_type'] = '';
			$activity_add['user_type'] = $user_type;
			$activity_add['transferred_bookid'] = $data['booking_id'];
			$activity_add['action_type'] = "driver_change";
			$activity_add['action_content'] = "User changed driver for booking ".$data['booking_id']." dated ".$data['service_date'];
			$activity_add['addeddate'] = $data['transferred_date_time'];
			$activity_add['date_time_added'] = $data['transferred_date_time'];
			$this->db->set($activity_add);
			$this->db->insert('user_activity');
			return $insert_id = $this->db->insert_id();
			
		}
	}
	
	function get_tablet_by_zone_tabid($tabletidval) {
        $this->db->select('tablet_id, tablet_driver_name, zone_id, imei, google_reg_id, access_code, tablet_status')
                ->from('tablets')
                ->where('tablet_id', $tabletidval)
                //->where('flat_id', 0)
                ->where('tablet_status', 1)
                ->limit(1);

        $get_tablet_by_zone_qry = $this->db->get();

        return $get_tablet_by_zone_qry->row();
    }
	
	function add_push_notifications($fields = array())
	{
		
		$fields['addeddate'] = isset($fields['addeddate']) ? $fields['addeddate'] : date('Y-m-d H:i:s');		

		$this->db->set($fields);
		$this->db->insert('push_notications'); 
		return $this->db->insert_id();
	}
	
	function get_booking_transfers_by_date_new($service_date)
	{
		$this->db->select('booking_transfer_tablet_id, booking_id, transfering_to_tablet')
				->from('booking_transfers_tablet')
				->where('service_date', $service_date)
				->group_by('booking_id')
				->order_by('booking_transfer_tablet_id', 'desc');
		
		$get_booking_transfers_by_date_qry = $this->db->get();
		
		return $get_booking_transfers_by_date_qry->result();
	}
	
	function get_invoice_detailbyid($invoice_id)
	{
		$this->db->select('c.total_invoice_amount,c.balance,m.maid_name,c.total_paid_amount,c.customer_trn,c.email_address,c.quickbook_id,i.invoice_id,i.invoice_num,i.customer_name,i.customer_id,i.bill_address,i.invoice_status, i.invoice_paid_status, i.added, i.service_date, i.invoice_date, i.invoice_due_date, i.invoice_total_amount, i.invoice_tax_amount, i.invoice_net_amount, i.balance_amount, i.received_amount,i.invoice_qb_id,i.invoice_qb_sync_stat, l.maid_name, l.service_from_time, l.service_to_time, l.line_bill_address, l.description, l.line_amount, l.line_vat_amount, l.line_net_amount,l.invoice_line_id,l.service_hrs,l.monthly_product_service, l.maid_id, u.user_fullname,st.service_type_name,c.customer_id as custid,i.signature_status,i.signature_image,i.pdf_status,i.pdf_link')
                ->from('invoice i')
				->join('invoice_line_items l','i.invoice_id = l.invoice_id','INNER')
				->join('day_services ds','ds.day_service_id = l.day_service_id','LEFT')
				->join('bookings b','b.booking_id = ds.booking_id','LEFT')
				->join('maids m','l.maid_id = m.maid_id','LEFT')
				->join('service_types st','st.service_type_id = b.service_type_id','LEFT')
				->join('customers c','c.customer_id = i.customer_id')
				->join('users u','i.invoice_added_by = u.user_id','LEFT')
				->where('i.invoice_id',$invoice_id)
				->where('l.line_status',1);
		$get_invoice_qry = $this->db->get();
        return $get_invoice_qry->result();
	}
	
	function getservicedetails($service_id)
	{
		$this->db->select("ds.customer_id, ds.booking_id, ds.day_service_id as day_service_id, ds.customer_name, ds.customer_address, ds.service_date, ds.serviceamount,  ds.total_fee, ds.booking_service_id, ds.material_fee, ds.maid_id, m.maid_name, DATE_FORMAT(b.time_from, '%h:%i %p') AS time_from, DATE_FORMAT(b.time_to, '%h:%i %p') AS time_to, b.time_from as start_time, b.time_to as end_time, c.price_hourly", FALSE)/*ds.vatamount,*/
			   ->from('day_services ds')
			   ->join('bookings b','ds.booking_id = b.booking_id')
			   ->join('customers c', 'b.customer_id = c.customer_id')
			   ->join('maids m', 'ds.maid_id = m.maid_id')
			   ->where('ds.service_status',2)
			   ->where('ds.invoice_status',0)
			   ->where('ds.day_service_id',$service_id);
		$get_service_qry = $this->db->get();
        return $get_service_qry->row();
	}
	
	function get_related_bookings_for_invoice($customer_id,$service_date,$ds_id)
	{
		$this->db->select("ds.customer_id, ds.booking_id, ds.day_service_id, ds.customer_name, ds.customer_address, ds.service_date, ds.serviceamount,  ds.total_fee, ds.booking_service_id, ds.material_fee, ds.maid_id, m.maid_name, DATE_FORMAT(b.time_from, '%h:%i %p') AS time_from, DATE_FORMAT(b.time_to, '%h:%i %p') AS time_to, b.time_from as start_time, b.time_to as end_time, c.price_hourly", FALSE)/*ds.vatamount,*/
			   ->from('day_services ds')
			   ->join('bookings b','ds.booking_id = b.booking_id')
			   ->join('customers c', 'b.customer_id = c.customer_id')
			   ->join('maids m', 'ds.maid_id = m.maid_id')
               ->where('ds.day_service_id !=',$ds_id)
			   ->where('ds.customer_id',$customer_id)
			   ->where('ds.service_date',$service_date)
            ->where('ds.service_status',2)
            ->where('ds.invoice_status',0);
		$get_service_qry = $this->db->get();
        return $get_service_qry->result();
	}
	
	function add_main_invoice($fields = array())
	{
		$this->db->set($fields);
		$this->db->insert('invoice'); 
		return $this->db->insert_id();
	}
	
	function add_line_invoice($fields = array())
	{
		$this->db->set($fields);
		$this->db->insert('invoice_line_items'); 
		return $this->db->insert_id();
	}
	
	function update_service_invoice($invoice_id, $fields = array())
	{
		$this->db->where('invoice_id', $invoice_id);
		$this->db->update('invoice', $fields); 

		return $this->db->affected_rows();
	}
	
	function get_cancel_reasons()
	{
        $this->db->select('reason_id,reason_name')
                ->from('service_cancel_reasons')
                ->where('status', 1);

        $get_tab_by_qry = $this->db->get();

        return $get_tab_by_qry->result();
    }
	
	function update_notifications_status($pushid, $fields = array())
	{
		$this->db->where('id', $pushid);
		$this->db->update('push_notications', $fields); 

		return $this->db->affected_rows();
	}
	
	function istransferred($b_id)
	{
		return $this->db->from('booking_transfers_tablet')->where('booking_id', $b_id)->get()->num_rows() > 0 ? 1:0;
	}
	function add_remove_key($id,$type){
		if($type == 1)
		return $this->db->update('customers',['key_given' => 'Y'],['customer_id' => $id]) ? json_encode(['status' => 'success','message'=>'Key Added.']) : json_encode(['status' => 'error','message'=>'Try Again.']);
		else
		return $this->db->update('customers',['key_given' => 'N'],['customer_id' => $id]) ? json_encode(['status' => 'success','message'=>'Key Removed.']) : json_encode(['status' => 'error','message'=>'Try Again.']);
	}
}