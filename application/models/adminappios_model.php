<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Adminappios_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function check_ipad_user($user_name, $password) 
	{
        $this->db->select('user_id')
                ->from('ipad_users')
                ->where('user_name', $user_name)
                ->where('password', $password)
                ->where('status', 1)
                ->limit(1);

        $get_ipad_user_qry = $this->db->get();

        return $get_ipad_user_qry->row();
    }
	
	function get_total_booking_hours($date_from = NULL, $type = 1, $date_to = NULL) 
	{
		$service_week_day = date('w', strtotime($date_from));
		$deletes = $this->get_booking_deletes_by_date($date_from);
		$deleted_bookings = array();
		foreach($deletes as $delete)
		{
			$deleted_bookings[] = $delete->booking_id;
		}
		if($type === 1)
		{
			$this->db->select("SUM(TIME_TO_SEC(TIMEDIFF(time_to,time_from))) AS duration", FALSE)
						->from('bookings')   
						->where('booking_category', 'C')
						->where('booking_status', 1)                            
						->where("((service_actual_end_date >= " . $this->db->escape($date_from) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
						->where("((service_start_date = " . $this->db->escape($date_from) . " AND booking_type = 'OD') OR (service_start_date <= " . $this->db->escape($date_from) . " AND service_week_day = " . $service_week_day . " AND booking_type = 'WE') OR (service_start_date <= " . $this->db->escape($date_from) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($date_from) . "), DATE(service_start_date)), 14) = 0 AND booking_type = 'BW'))", NULL, FALSE)
						->limit(1);
		}
		else if($type === 2)
		{
			$this->db->select("SUM(TIME_TO_SEC(TIMEDIFF(end_time, start_time))) AS duration", FALSE)
					->from('day_services')
					->where('service_status', 2);
			if($date_from && $date_to)
			{
				$this->db->where('service_date BETWEEN "'. $date_from. '" and "'. $date_to.'"');
			}
			else if($date_from || $date_to)
			{
				$date = $date_from ? $date_from : $date_to;
				$this->db->where('service_date', $date);
			}
			else
			{
				$this->db->where('service_date', $date_from);
			}
			
			$this->db->limit(1);
		}
		if(count($deleted_bookings) > 0)
		{
				$this->db->where_not_in('booking_id', $deleted_bookings);
		}

		$get_total_booking_hours_qry = $this->db->get();
		$booking = $get_total_booking_hours_qry->row();
		return isset($booking->duration) ? floor($booking->duration/3600): 0;
	}
	
	function get_total_bookings($date_from = NULL, $type = 1, $date_to = NULL) 
	{
		$service_week_day = date('w', strtotime($date_from));
		$deletes = $this->get_booking_deletes_by_date($date_from);
		$deleted_bookings = array();
		foreach($deletes as $delete)
		{
			$deleted_bookings[] = $delete->booking_id;
		}
		if($type === 1)
		{
			$this->db->select("booking_id", FALSE)
						->from('bookings')      
						->where('booking_category', 'C')
						->where('booking_status', 1)                            
						->where("((service_actual_end_date >= " . $this->db->escape($date_from) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
						->where("((service_start_date = " . $this->db->escape($date_from) . " AND booking_type = 'OD') OR (service_start_date <= " . $this->db->escape($date_from) . " AND service_week_day = " . $service_week_day . " AND booking_type = 'WE') OR (service_start_date <= " . $this->db->escape($date_from) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($date_from) . "), DATE(service_start_date)), 14) = 0 AND booking_type = 'BW'))", NULL, FALSE);
		}
		else if($type === 2)
		{
			$this->db->select("day_service_id", FALSE)
					->from('day_services')
					->where('service_status', 2);
			if($date_from && $date_to)
			{
				$this->db->where('service_date BETWEEN "'. $date_from. '" and "'. $date_to.'"');
			}
			else if($date_from || $date_to)
			{
				$date = $date_from ? $date_from : $date_to;
				$this->db->where('service_date', $date);
			}
			else
			{
				$this->db->where('service_date', $date_from);
			}
		}
		if(count($deleted_bookings) > 0)
		{
			$this->db->where_not_in('booking_id', $deleted_bookings);
		}
		$get_total_bookings_qry = $this->db->get();
		return $get_total_bookings_qry->num_rows();
	}
	
	function get_one_day_bookings($date_from = NULL, $type = 1, $date_to = NULL) 
	{
		$service_week_day = date('w', strtotime($date_from));
		$deletes = $this->get_booking_deletes_by_date($date_from);

		$deleted_bookings = array();
		foreach($deletes as $delete)
		{
				$deleted_bookings[] = $delete->booking_id;
		}
		if($type === 1)
		{
			$this->db->select("booking_id", FALSE)
							->from('bookings')  
							->where('booking_category', 'C')
							->where('booking_status', 1) 
							->where('booking_type', 'OD')
							->where("((service_actual_end_date >= " . $this->db->escape($date_from) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
							->where("((service_start_date = " . $this->db->escape($date_from) . " AND booking_type = 'OD') OR (service_start_date <= " . $this->db->escape($date_from) . " AND service_week_day = " . $service_week_day . " AND booking_type = 'WE') OR (service_start_date <= " . $this->db->escape($date_from) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($date_from) . "), DATE(service_start_date)), 14) = 0 AND booking_type = 'BW'))", NULL, FALSE);
							
			if(count($deleted_bookings) > 0)
			{
				$this->db->where_not_in('booking_id', $deleted_bookings);
			}
		}
		else if($type === 2)
		{
			$this->db->select("d.day_service_id", FALSE)
					->from('day_services d')
					->join('bookings b', 'd.booking_id=b.booking_id')
					->where('b.booking_type', 'OD')
					->where('b.booking_status', 1) 
					->where('d.service_status', 2);
			if($date_from && $date_to)
			{
				$this->db->where('d.service_date BETWEEN "'. $date_from. '" and "'. $date_to.'"');
			}
			else if($date_from || $date_to)
			{
				$date = $date_from ? $date_from : $date_to;
				$this->db->where('d.service_date', $date);
			}
			else
			{
				$this->db->where('d.service_date', $date_from);
			}
			if(count($deleted_bookings) > 0)
			{
				$this->db->where_not_in('d.booking_id', $deleted_bookings);
			} 
		}
		$get_total_bookings_qry = $this->db->get();
		return $get_total_bookings_qry->num_rows();
	}
	
	function get_week_day_bookings($date_from = NULL, $type = 1, $date_to = NULL) 
	{
		$service_week_day = date('w', strtotime($date_from));
		$deletes = $this->get_booking_deletes_by_date($date_from);
		$deleted_bookings = array();
		foreach($deletes as $delete)
		{
			$deleted_bookings[] = $delete->booking_id;
		}
		if($type === 1)
		{
			$this->db->select("booking_id", FALSE)
							->from('bookings')
							->where('booking_category', 'C')
							->where('booking_status', 1) 
							->where('booking_type', 'WE')
							->where("((service_actual_end_date >= " . $this->db->escape($date_from) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
							->where("((service_start_date = " . $this->db->escape($date_from) . " AND booking_type = 'OD') OR (service_start_date <= " . $this->db->escape($date_from) . " AND service_week_day = " . $service_week_day . " AND booking_type = 'WE') OR (service_start_date <= " . $this->db->escape($date_from) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($date_from) . "), DATE(service_start_date)), 14) = 0 AND booking_type = 'BW'))", NULL, FALSE);
							
			if(count($deleted_bookings) > 0)
			{
				$this->db->where_not_in('booking_id', $deleted_bookings);
			}
		}
		else if($type === 2)
		{
			$this->db->select("d.booking_id", FALSE)
					->from('day_services d')
					->join('bookings b', 'd.booking_id=b.booking_id')
					->where('b.booking_category', 'C')
					->where('b.booking_type', 'WE')
					->where('b.booking_status', 1) 
					->where('d.service_status', 2);
			if($date_from && $date_to)
			{
				$this->db->where('d.service_date BETWEEN "'. $date_from. '" and "'. $date_to.'"');
			}
			else if($date_from || $date_to)
			{
				$date = $date_from ? $date_from : $date_to;
				$this->db->where('d.service_date', $date);
			}
			else
			{
				$this->db->where('d.service_date', $date_from);
			}
			if(count($deleted_bookings) > 0)
			{
				$this->db->where_not_in('d.booking_id', $deleted_bookings);
			}                
			
		}
		$get_total_bookings_qry = $this->db->get();
		return $get_total_bookings_qry->num_rows();
	}
	
	function get_total_cancellation($date_from = NULL, $date_to = NULL)
	{
		$this->db->select('booking_id')
			->from('booking_deletes');
			
		if($date_from && $date_to)
		{
			$this->db->where('service_date BETWEEN "'. $date_from. '" and "'. $date_to.'"');
		}
		else
		{
			$date = $date_from ? $date_from : $date_to;
			
			$this->db->where('service_date', $date);
		}
		$get_booking_deletes_by_date_qry = $this->db->get();             
		
		
		return $get_booking_deletes_by_date_qry->num_rows();
	}
	
	function get_total_invoice_amount($date_from = NULL, $date_to = NULL) 
	{
		$service_week_day = date('w', strtotime($date_from));
		$deletes = $this->get_booking_deletes_by_date($date_from);
		$deleted_bookings = array();
		foreach($deletes as $delete)
		{
			$deleted_bookings[] = $delete->booking_id;
		}
            
		$this->db->select("i.day_service_id", FALSE)
				->from('day_services d')
				->join('invoice i', 'd.day_service_id=i.day_service_id')                     
				->where('d.service_status', 2);
		if($date_from && $date_to)
		{
			$this->db->where('d.service_date BETWEEN "'. $date_from. '" and "'. $date_to.'"');
		}
		else if($date_from || $date_to)
		{
			$date = $date_from ? $date_from : $date_to;
			$this->db->where('d.service_date', $date);
		}
		else
		{
			$this->db->where('d.service_date', $date_from);
		}
		if(count($deleted_bookings) > 0)
		{
				$this->db->where_not_in('d.booking_id', $deleted_bookings);
		}

		$get_total_invoice_amount_qry = $this->db->get();
		return $get_total_invoice_amount_qry->num_rows();
	}
	
	function get_day_collection($date_from = NULL, $date_to = NULL)
	{
		$this->db->select('SUM(paid_amount) AS day_collection', FALSE)
			->from('customer_payments');
			
		if($date_from && $date_to)
		{
			$this->db->where('DATE(paid_datetime) BETWEEN "'. $date_from. '" and "'. $date_to.'"');
		}
		else
		{
			$date = $date_from ? $date_from : $date_to;
			
			$this->db->where('DATE(paid_datetime)', $date);
		}
		
		$this->db->limit(1);
		$get_day_collection_qry = $this->db->get(); 
		$payments = $get_day_collection_qry->row();
		return isset($payments->day_collection) ? $payments->day_collection : 0;
	}
	
	function get_total_pending_amount($date_from = NULL, $date_to = NULL) 
	{
		$service_week_day = date('w', strtotime($date_from));
		$deletes = $this->get_booking_deletes_by_date($date_from);

		$deleted_bookings = array();
		foreach($deletes as $delete)
		{
			$deleted_bookings[] = $delete->booking_id;
		}
		
		$this->db->select("SUM(d.total_fee) AS pending_amount", FALSE)
				->from('day_services d')
				->join('invoice i', 'd.day_service_id=i.day_service_id')                     
				->where('d.service_status', 2)
				->where('i.invoice_status', 0);
		if($date_from && $date_to)
		{
			$this->db->where('DATE(i.added) BETWEEN "'. $date_from. '" and "'. $date_to.'"');
		}
		else if($date_from || $date_to)
		{
			$date = $date_from ? $date_from : $date_to;
			$this->db->where('DATE(i.added)', $date);
		}
		else
		{
			$this->db->where('DATE(i.added)', $date_from);
		}
		if(count($deleted_bookings) > 0)
		{
			$this->db->where_not_in('d.booking_id', $deleted_bookings);
		}
		$this->db->limit(1);
		$get_total_pending_amount_qry = $this->db->get();
		$payments = $get_total_pending_amount_qry->row();
		return isset($payments->pending_amount) ? $payments->pending_amount : 0;
	}
	
	function get_booking_deletes_by_date($service_date, $service_end_date = NULL)
	{
		$this->db->select('booking_id')
				->from('booking_deletes');
		if($service_end_date != NULL)
		{
			$this->db->where("service_date BETWEEN '$service_date' AND '$service_end_date'");
		}
		else
		{
			$this->db->where("service_date", $service_date);
		}
		$get_booking_deletes_by_date_qry = $this->db->get();
		return $get_booking_deletes_by_date_qry->result();
	}
	
	function get_new_customer($date_from = NULL, $date_to = NULL)
	{
		$this->db->select('customer_id')
			->from('customers');
		if($date_from && $date_to)
		{
			$this->db->where('DATE(customer_added_datetime) BETWEEN "'. $date_from. '" and "'. $date_to.'"');
		}
		else
		{
			$date = $date_from ? $date_from : $date_to;
			
			$this->db->where('DATE(customer_added_datetime)', $date);
		}
		$get_new_customer_qry = $this->db->get();
		return $get_new_customer_qry->num_rows();
	}
	
	function change_password($old_password, $new_password) 
	{
        $fields['password'] = $new_password;
        $this->db->where('password', $old_password);
        $this->db->update('ipad_users', $fields);

        return $this->db->affected_rows();
    }
	
	function get_all_tablets($active_only = TRUE, $no_spare = TRUE) 
	{
        $this->db->select('tablet_id, tablets.zone_id, zone_name, zone_nick, imei, google_reg_id, access_code, tablet_status')
                ->from('tablets')
                ->join('zones', 'tablets.zone_id = zones.zone_id')
                ->where('tablet_status', 1);

        $get_tab_by_qry = $this->db->get();

        return $get_tab_by_qry->result();
    }
	
	function get_tablet_locations() 
	{
        $tablets = $this->get_all_tablets();
        $query = '';
        if (!empty($tablets)) 
		{
            foreach ($tablets as $tab) {
                $query .= '( SELECT id, tablet_id, "' . $tab->zone_name . '" AS zone_name, "' . $tab->zone_nick . '" AS zone_nick, `latitude` , `longitude` , `speed` , `added`  FROM `tablet_locations` WHERE tablet_id = ' . $tab->tablet_id . ' ORDER BY id DESC LIMIT 1 ) UNION ';
				//$query .= '( SELECT id, tablet_id, "' . $tab->zone_name . '" AS zone_name, `latitude` , `longitude` , `speed` , `added`  FROM `tablet_locations` WHERE tablet_id = ' . $tab->tablet_id . ' ORDER BY id DESC LIMIT 1 ) UNION ';
            }
            $query = rtrim($query, ' UNION');
        }

        $get_tablet_locations_qry = $this->db->query($query);
        return $get_tablet_locations_qry->result();
    }
	
	function get_maid_locations()
	{
		$this->db->select('maid_id,maid_name, maid_latitude, maid_longitude')
                ->from('maids')
				->where('maid_latitude is NOT NULL', NULL, FALSE)
				->where('maid_status', 1);

        $get_tab_by_qry = $this->db->get();

        return $get_tab_by_qry->result();
	}

}

