<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Call_logger_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	function getCustomerMobSearch($mobile) {
		$query = $this->db->query("SELECT c.customer_id AS custId, c.customer_name AS custName, c.email_address AS email, c.phone_number as phone, c.balance, c.signed, c.mobile_number_1 AS mobile1, c.mobile_number_2 AS mobile2, c.mobile_number_3 as mobile3,  a.area_name as area, ca.customer_address as address 
			FROM customers as c 
			LEFT JOIN customer_addresses as ca ON ca.customer_id = c.customer_id 
			LEFT JOIN areas as a ON ca.area_id = a.area_id 
			WHERE  c.phone_number = '$mobile' OR c.mobile_number_1 = '$mobile' OR c.mobile_number_2 = '$mobile' OR c.mobile_number_3 = '$mobile' LIMIT 1");
        $custList = $query->result();
		$respArr['custList'] = array();
		foreach ($custList as $detail)
		{
			$detail->url = 'https://booking.emaid.info/elitemaids/customer/view/'. $detail->custId;
			$detail->pending_amount = $detail->balance. $detail->signed;
			array_push($respArr['custList'],$detail);
		}
		return $respArr['custList'];
		
    }
	
	function add_call_detail($data) {
        $this->db->set($data);
        $this->db->insert('call_logger');
        $result = $this->db->insert_id();
        return $result;
    }
	
	function check_call_exist($refid,$phone)
    {
        $this->db->select('*')
                ->from('call_logger')
                ->where('refid',$refid)
                ->where('mobile_no',$phone);
        $get_user_qry = $this->db->get();
        return $get_user_qry->row();
    }
	
	function update_call_detail($refid, $fields = array())
    {
        $this->db->where('refid', $refid);
        $this->db->update('call_logger', $fields); 
        return $this->db->affected_rows();
    }  
}