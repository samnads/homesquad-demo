<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quickbook_model extends CI_Model{
	
	function update_quickbookdetails($data,$id){
        $this->db->where('settings_id', $id);
        $this->db->update('settings', $data);
    }
	
	function get_customers_detail($customer_id)
    {
        $this->db->select('c.customer_id, c.customer_name, c.customer_nick_name, c.mobile_number_1, c.is_company, c.company_name, c.mobile_number_2, c.mobile_number_3, c.phone_number, c.email_address, c.contact_person, c.customer_trn, c.balance, c.quickbook_id, c.quickbook_sync_status, ca.customer_address, ca.building, ca.unit_no, ca.street, a.area_name, z.zone_name')
                ->from('customers c')
                ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id');
        $this->db->where('c.customer_status', 1);
        $this->db->where('c.quickbook_sync_status', 0);
        $this->db->where('c.customer_id', $customer_id);
        $this->db->where('ca.default_address', 1);
        $get_customers_qry = $this->db->get();
        return $get_customers_qry->row();
    }
	
	public function count_all_customers()
    {
        $this->db->select('c.customer_id')
            ->from('customers c')
			->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
			->join('areas a', 'ca.area_id = a.area_id');
		$this->db->where('c.customer_status', 1);
		$this->db->where('c.quickbook_sync_status', 0);
        $num_results = $this->db->count_all_results();
        return $num_results;
    }
	
	public function get_all_customersnew($keywordsearch = NULL)
    {
        $this->db->select('c.customer_id')
            ->from('customers c')
			->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
			->join('areas a', 'ca.area_id = a.area_id');
        if ($keywordsearch != "") {
            $this->db->where('c.customer_name like"%' . $keywordsearch . '%" OR c.mobile_number_1 like"%' . $keywordsearch . '%" OR c.email_address like"%' . $keywordsearch . '%"');
        }
        $this->db->order_by('c.customer_id', 'ASC');
		$this->db->where('c.customer_status', 1);
		$this->db->where('c.quickbook_sync_status', 0);
        $num_results = $this->db->count_all_results();
        return $num_results;
    }
	
	public function get_all_newcustomers($columnName = NULL, $columnSortOrder = NULL, $rowperpage = NULL, $start = NULL, $draw = NULL, $recordsTotal = NULL, $recordsTotalFilter = NULL, $keywordsearch = NULL)
    {
        $response = array();
        $this->db->select('c.customer_id, c.customer_name, c.mobile_number_1, c.customer_added_datetime')
            ->from('customers c')
			->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
			->join('areas a', 'ca.area_id = a.area_id')
            ->order_by('c.customer_id', 'ASC');
			
        if ($keywordsearch != "") {
            $this->db->where('c.customer_name like"%' . $keywordsearch . '%" OR c.mobile_number_1 like"%' . $keywordsearch . '%" OR c.email_address like"%' . $keywordsearch . '%"');
        }
        $this->db->where('c.customer_status', 1);
		$this->db->where('c.quickbook_sync_status', 0);
        if ($columnName != '' && $columnSortOrder != "") {
            $this->db->order_by($columnName, $columnSortOrder);
        }
		$this->db->order_by('c.customer_id', 'ASC');
        $this->db->limit($rowperpage, $start);
        $query = $this->db->get();
        $records = $query->result();
        $data = array();
        $i = 1;

        //print_r($records);die();
        foreach ($records as $record) {
            $data[] = array(
                'slno' => '<center>'.($i + $start).'<center>',
                'name' => '<a href="' . base_url('customer/view/'.$record->customer_id).'" style="text-decoration: none;color:#333;" data-toggle="tooltip">' . $record->customer_name .'<br/></a>',
                'mobile' => $record->mobile_number_1 ?: '-',
                'addeddate' => ($record->customer_added_datetime) ? date("d/m/Y H:i:s", strtotime($record->customer_added_datetime)) : "",
            );
            $i++;
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $recordsTotal,
            "lastcall" => $records[0],
            "iTotalDisplayRecords" => $recordsTotalFilter,
            "aaData" => $data
        );

        return $response;
    }
	
	public function get_all_newcustomers_for_sync()
    {
        $this->db->select('c.customer_id, c.customer_name, c.mobile_number_1, c.email_address, c.quickbook_id, c.quickbook_sync_status, ca.customer_address, ca.building, ca.unit_no, ca.street, a.area_name')
            ->from('customers c')
			->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
			->join('areas a', 'ca.area_id = a.area_id')
            ->order_by('c.customer_id', 'ASC');
		$this->db->where('c.customer_status', 1);
		//$this->db->where('c.customer_id', 1062);
        $this->db->where('c.quickbook_sync_status', 0);
        $this->db->limit(200);
        $query = $this->db->get();
        $records = $query->result();
        $data = array();
        $i = 1;

        foreach ($records as $record) {
			if ($record->building != "") {
				$apartment = ' , Apt No ' . $record->building;
			} else {
				$apartment = "";
			}
			$addresssss = $record->customer_address . $apartment;
            $data[] = array(
				'id' => $record->customer_id,
				'name' => $record->customer_name,
				'mobile' => $record->mobile_number_1,
				'email' => $record->email_address,
				'area' => $record->area_name,
				'apartment' => $record->building,
				'address' => $addresssss,
            );
            $i++;
        }
        return $data;
    }
	
	function update_quickbook_cust_sync_msg($data,$id){
        $this->db->where('tmp_msg_id', $id);
        $this->db->update('qb_sync_tmp_msg', $data);
		return $this->db->affected_rows();
    }
	
	public function count_all_invoices()
    {
        $this->db->select('i.invoice_id')
            ->from('invoice i')
			->join('customers c', 'i.customer_id = c.customer_id')
			->join('invoice_line_items l', 'i.invoice_id = l.invoice_id');
		$this->db->where('i.invoice_status !=', 2);
		$this->db->where('i.invoice_status !=', 0);
		$this->db->where('i.invoice_qb_sync_stat', 0);
		$this->db->where('i.quickbook_sync_show_status', 1);
		$this->db->group_by('i.invoice_id');
        // $num_results = $this->db->count_all_results();
        $query = $this->db->get();
        $records = $query->result();
		// echo $num_results;
		// exit();
        return $records;
    }
	
	public function get_all_invoicesnew($keywordsearch = NULL, $custid = NULL, $regdate = NULL, $regdateto = NULL)
    {
        $this->db->select('i.invoice_id')
            ->from('invoice i')
			->join('customers c', 'i.customer_id = c.customer_id')
			->join('invoice_line_items l', 'i.invoice_id = l.invoice_id');
        if ($keywordsearch != "") {
            $this->db->where('i.invoice_num like"%' . $keywordsearch . '%" OR c.customer_name like"%' . $keywordsearch . '%" OR c.mobile_number_1 like"%' . $keywordsearch . '%" OR c.email_address like"%' . $keywordsearch . '%"');
        }
		if ($custid != "") {
            $this->db->where('c.customer_id', $custid);
        }
		if ($regdate && $regdateto) {
            $this->db->where("`i.invoice_date` BETWEEN '$regdate' AND '$regdateto'");
        }
        if (($regdate != "" && $regdateto == "") || ($regdate == "" && $regdateto != "")) {
            if ($regdate != "") {
                $this->db->where("`i.invoice_date` = '$regdate'");
            }
            if ($regdateto != "") {
                $this->db->where("`i.invoice_date` = '$regdate'");
            }
        }
		// if ($regdate != "") {
			// $this->db->where("`i.invoice_date` = '$regdate'");
		// }
		$this->db->where('i.invoice_status !=', 2);
		$this->db->where('i.invoice_status !=', 0);
		$this->db->where('i.invoice_qb_sync_stat', 0);
		$this->db->where('i.quickbook_sync_show_status', 1);
		$this->db->group_by('i.invoice_id');
        // $num_results = $this->db->count_all_results();
		$query = $this->db->get();
        $records = $query->result();
        return $records;
    }
	
	public function get_all_newinvoices($columnName = NULL, $columnSortOrder = NULL, $rowperpage = NULL, $start = NULL, $draw = NULL, $recordsTotal = NULL, $recordsTotalFilter = NULL, $keywordsearch = NULL, $custid = NULL, $regdate = NULL, $regdateto = NULL)
    {
        $response = array();
        $this->db->select('i.invoice_id, i.invoice_num, c.customer_id, c.customer_name, c.mobile_number_1, i.invoice_date, i.invoice_net_amount, i.received_amount, i.balance_amount, i.invoice_status, i.invoice_paid_status')
            ->from('invoice i')
			->join('customers c', 'i.customer_id = c.customer_id')
			->join('invoice_line_items l', 'i.invoice_id = l.invoice_id')
            ->order_by('i.invoice_id', 'ASC');
		$this->db->group_by('i.invoice_id');	
        if ($keywordsearch != "") {
            $this->db->where('i.invoice_num like"%' . $keywordsearch . '%" OR c.customer_name like"%' . $keywordsearch . '%" OR c.mobile_number_1 like"%' . $keywordsearch . '%" OR c.email_address like"%' . $keywordsearch . '%"');
        }
		if ($custid != "") {
            $this->db->where('c.customer_id', $custid);
        }
		if ($regdate && $regdateto) {
            $this->db->where("`i.invoice_date` BETWEEN '$regdate' AND '$regdateto'");
        }
        if (($regdate != "" && $regdateto == "") || ($regdate == "" && $regdateto != "")) {
            if ($regdate != "") {
                $this->db->where("`i.invoice_date` = '$regdate'");
            }
            if ($regdateto != "") {
                $this->db->where("`i.invoice_date` = '$regdate'");
            }
        }
		// if ($regdate != "") {
			// $this->db->where("`i.invoice_date` = '$regdate'");
		// }
        $this->db->where('i.invoice_status !=', 2);
		$this->db->where('i.invoice_status !=', 0);
		$this->db->where('i.invoice_qb_sync_stat', 0);
		$this->db->where('i.quickbook_sync_show_status', 1);
        if ($columnName != '' && $columnSortOrder != "") {
            $this->db->order_by($columnName, $columnSortOrder);
        }
        $this->db->limit($rowperpage, $start);
        $query = $this->db->get();
        $records = $query->result();
        $data = array();
        $i = 1;

        //print_r($records);die();
        foreach ($records as $record) {
			if($record->invoice_paid_status == 2)
			{
				$paystat = " (Partially Paid)";
			} else {
				$paystat = "";
			}
			if($record->invoice_status == '0')
			{
				$invstatus = "Draft";
			} else if($record->invoice_status == '1')
			{
				$invstatus = "Open";
			} else  if($record->invoice_status == '2'){
				$invstatus = "Cancelled";
			} else  if($record->invoice_status == '3'){
				$invstatus = "Paid";
			}
			$stats = $invstatus.$paystat;
            $data[] = array(
                'slno' => '<center>'.($i + $start).'<center>',
				'invnum' => $record->invoice_num,
				'name' => $record->customer_name,
				'mobile' => $record->mobile_number_1,
				'invdate' => ($record->invoice_date) ? date("d/m/Y", strtotime($record->invoice_date)) : "",
				'amount' => $record->invoice_net_amount,
				'paidamount' => $record->received_amount,
				'dueamount' => $record->balance_amount,
                'status' => $stats,
            );
            $i++;
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $recordsTotal,
            "lastcall" => $records[0],
            "iTotalDisplayRecords" => $recordsTotalFilter,
            "aaData" => $data
        );

        return $response;
    }
	
	public function get_all_newinvoices_for_sync($custid = NULL,$search = NULL,$regdate = NULL, $regdateto = NULL)
    {
        $this->db->select('i.invoice_id, i.invoice_num, i.invoice_date, i.invoice_due_date, i.invoice_net_amount, i.received_amount, i.balance_amount, i.invoice_status, i.invoice_paid_status, i.invoice_qb_id, i.invoice_qb_syncId, i.invoice_qb_sync_stat, c.customer_id, c.customer_name, c.mobile_number_1, c.email_address, c.quickbook_id, c.quickbookTokenId, c.quickbook_sync_status')
            ->from('invoice i')
			->join('customers c', 'i.customer_id = c.customer_id')
			->join('invoice_line_items l', 'i.invoice_id = l.invoice_id')
            ->order_by('i.invoice_id', 'ASC');
		if ($search != "") {
            $this->db->where('i.invoice_num like"%' . $search . '%" OR c.customer_name like"%' . $search . '%" OR c.mobile_number_1 like"%' . $search . '%" OR c.email_address like"%' . $search . '%"');
        }
		if ($custid != "") {
            $this->db->where('c.customer_id', $custid);
        }
		if ($regdate && $regdateto) {
            $this->db->where("`i.invoice_date` BETWEEN '$regdate' AND '$regdateto'");
        }
        if (($regdate != "" && $regdateto == "") || ($regdate == "" && $regdateto != "")) {
            if ($regdate != "") {
                $this->db->where("`i.invoice_date` = '$regdate'");
            }
            if ($regdateto != "") {
                $this->db->where("`i.invoice_date` = '$regdate'");
            }
        }
		// if ($regdate != "") {
			// $this->db->where("`i.invoice_date` = '$regdate'");
		// }
		$this->db->where('i.invoice_status !=', 2);
		$this->db->where('i.invoice_status !=', 0);
		$this->db->where('i.invoice_qb_sync_stat', 0);
		$this->db->where('i.quickbook_sync_show_status', 1);
		$this->db->group_by('i.invoice_id');
        $this->db->limit(100);
        // $this->db->limit(1);
        $query = $this->db->get();
        $records = $query->result();
        $data = array();
        $i = 1;

        foreach ($records as $record) {
			$invoice_detail = $this->get_invoice_detail_sync($record->invoice_id);
			//$invoice_pay_detail = $this->get_invoice_paymentdetail_sync($record->invoice_id);
            $data[] = array(
				'invoice_id' => $record->invoice_id,
				'invoice_num' => $record->invoice_num,
				'invoice_date' => $record->invoice_date,
				'invoice_due_date' => $record->invoice_due_date,
				'invoice_tax_amount' => $record->invoice_tax_amount,
				'invoice_net_amount' => $record->invoice_net_amount,
				'received_amount' => $record->received_amount,
				'balance_amount' => $record->balance_amount,
				'invoice_status' => $record->invoice_status,
				'invoice_paid_status' => $record->invoice_paid_status,
				'invoice_qb_id' => $record->invoice_qb_id,
				'invoice_qb_syncId' => $record->invoice_qb_syncId,
				'invoice_qb_sync_stat' => $record->invoice_qb_sync_stat,
				'customer_name' => $record->customer_name,
				'mobile' => $record->mobile_number_1,
				'email' => $record->email_address,
				'customer_quickbook_id' => $record->quickbook_id,
				'customerquickbookTokenId' => $record->quickbookTokenId,
				'customerquickbook_sync_status' => $record->quickbook_sync_status,
				'invoice_lines' => $invoice_detail,
				//'invoice_payments' => $invoice_pay_detail,
            );
            $i++;
        }
        return $data;
    }
	
	function get_invoice_detail_sync($invoice_id)
	{
		$this->db->select('l.line_amount, l.line_net_amount, l.description, ds.service_date, l.service_hrs, l.inv_unit_price')
                ->from('invoice_line_items l')
				->join('day_services ds','ds.day_service_id = l.day_service_id','LEFT')
                ->where('l.invoice_id',$invoice_id)
                ->where('l.line_status',1);
        $get_invoice_qry = $this->db->get();
        return $get_invoice_qry->result();
	}
	
	function get_invoice_paymentdetail_sync($invoice_id)
	{
		$this->db->select('ip.inv_map_id,ip.pay_reference,ip.payment_amount,ip.paymentId,cp.quickbook_payment_id,cp.quickbookPaymentTokenId,cp.quickbook_payment_sync_status,cp.paid_amount,cp.paid_datetime')
                ->from('InvoicePaymentMapping ip')
				->join('customer_payments cp','ip.paymentId = cp.payment_id')
                ->where('ip.invoiceId',$invoice_id)
                ->where('ip.qb_sync_stat',0);
        $get_invoice_qry = $this->db->get();
        return $get_invoice_qry->result();
	}
	
	function update_invoicepay_detail($invoice,$fields = array())
    {
        $this->db->where('invoice_id', $invoice);
        $this->db->update('invoice', $fields);
        return $this->db->affected_rows();
    }
	
	function update_quick_invoicepay_detail($invoice,$fields = array(),$input_json=NULL,$output=NULL,$jsonheaders=NULL,$quickbook_iddd=NULL)
    {
        $this->db->where('invoice_id', $invoice);
        $this->db->update('invoice', $fields);
        if($this->db->affected_rows())
		{
			$insertarray = array();
			$insertarray['type'] = 2;
			$insertarray['quickbook_id'] = $quickbook_iddd;
			$insertarray['emaid_id'] = $invoice;
			$insertarray['input'] = $input_json;
			$insertarray['header'] = $jsonheaders;
			$insertarray['output'] = $output;
			$this->db->set($insertarray);
			$this->db->insert('quickbook_log');
			return $this->db->insert_id();
		}
    }
	
	function update_invoice_payment_by_ipmid($invoice,$fields = array())
    {
        $this->db->where('inv_map_id', $invoice);
        $this->db->update('InvoicePaymentMapping', $fields);
        return $this->db->affected_rows();
    }
	
	function update_quick_invoice_payment_by_ipmid($invoice,$fields = array(), $input_json=NULL,$output=NULL,$jsonheaders=NULL,$quickbook_iddd=NULL)
    {
        $this->db->where('inv_map_id', $invoice);
        $this->db->update('InvoicePaymentMapping', $fields);
        if($this->db->affected_rows())
		{
			$insertarray = array();
			$insertarray['type'] = 4;
			$insertarray['quickbook_id'] = $quickbook_iddd;
			$insertarray['emaid_id'] = $invoice;
			$insertarray['input'] = $input_json;
			$insertarray['header'] = $jsonheaders;
			$insertarray['output'] = $output;
			$this->db->set($insertarray);
			$this->db->insert('quickbook_log');
			return $this->db->insert_id();
		}
    }
	
	function update_invoice_payment_by_payid($paymentId,$fields = array())
    {
        $this->db->where('payment_id', $paymentId);
        $this->db->update('customer_payments', $fields);
        return $this->db->affected_rows();
    }
	
	function update_quick_invoice_payment_by_payid($paymentId,$fields = array(), $input_json=NULL,$output=NULL,$jsonheaders=NULL,$quickbook_iddd=NULL)
    {
        $this->db->where('payment_id', $paymentId);
        $this->db->update('customer_payments', $fields);
        if($this->db->affected_rows())
		{
			$insertarray = array();
			$insertarray['type'] = 3;
			$insertarray['quickbook_id'] = $quickbook_iddd;
			$insertarray['emaid_id'] = $paymentId;
			$insertarray['input'] = $input_json;
			$insertarray['header'] = $jsonheaders;
			$insertarray['output'] = $output;
			$this->db->set($insertarray);
			$this->db->insert('quickbook_log');
			return $this->db->insert_id();
		}
    }
	
	public function count_all_invoice_payments()
    {
        $this->db->select('ipm.inv_map_id')
            ->from('InvoicePaymentMapping ipm')
			->join('invoice i', 'ipm.invoiceId = i.invoice_id')
			->join('customers c', 'i.customer_id = c.customer_id')
			->join('customer_payments cp', 'ipm.paymentId = cp.payment_id');
			// ->join('invoice_line_items l', 'i.invoice_id = l.invoice_id');
		$this->db->where('i.invoice_status !=', 2);
		$this->db->where('ipm.map_status', 1);
		$this->db->where('ipm.qb_sync_stat', 0);
		$this->db->where('i.quickbook_sync_show_status', 1);
		$this->db->where('cp.quickbook_payment_show_status', 1);
		// $this->db->where("(cp.payment_type = 'P' AND cp.quickbook_payment_sync_status = 1)", NULL, FALSE);
        // $this->db->or_where("(cp.payment_type = 'CM' AND cp.quickbook_payment_sync_status = 0)", NULL, FALSE);
		$this->db->where("((cp.payment_type = 'P' AND cp.quickbook_payment_sync_status = 1) OR (cp.payment_type = 'CM' AND cp.quickbook_payment_sync_status = 0))", NULL, FALSE);
        // $this->db->where('cp.quickbook_payment_sync_status', 1);
		$this->db->where('ipm.qb_sync_show_status', 1);
		$this->db->where('cp.deleted_at',null);
        // $num_results = $this->db->count_all_results();
        $query = $this->db->get();
        $records = $query->result();
		// echo $this->db->last_query();
		// echo $num_results;
		// exit();
        return $records;
    }
	
	public function get_all_invoicepaymentsnew($keywordsearch = NULL, $custid = NULL, $regdate = NULL, $regdateto = NULL)
    {
        $this->db->select('ipm.inv_map_id')
            ->from('InvoicePaymentMapping ipm')
			->join('invoice i', 'ipm.invoiceId = i.invoice_id')
			->join('customers c', 'ipm.inv_customer_id = c.customer_id')
			->join('customer_payments cp', 'ipm.paymentId = cp.payment_id');
        if ($keywordsearch != "") {
            $this->db->where('i.invoice_num like"%' . $keywordsearch . '%" OR c.customer_name like"%' . $keywordsearch . '%" OR c.mobile_number_1 like"%' . $keywordsearch . '%" OR c.email_address like"%' . $keywordsearch . '%"');
        }
		if ($custid != "") {
            $this->db->where('c.customer_id', $custid);
        }
		if ($regdate && $regdateto) {
            $this->db->where("`i.invoice_date` BETWEEN '$regdate' AND '$regdateto'");
        }
        if (($regdate != "" && $regdateto == "") || ($regdate == "" && $regdateto != "")) {
            if ($regdate != "") {
                $this->db->where("`i.invoice_date` = '$regdate'");
            }
            if ($regdateto != "") {
                $this->db->where("`i.invoice_date` = '$regdate'");
            }
        }
		// if ($regdate != "") {
			// $this->db->where("`i.invoice_date` = '$regdate'");
		// }
		$this->db->where('i.invoice_status !=', 2);
		$this->db->where('ipm.map_status', 1);
		$this->db->where('ipm.qb_sync_stat', 0);
		$this->db->where('i.quickbook_sync_show_status', 1);
		$this->db->where('ipm.qb_sync_show_status', 1);
		$this->db->where('cp.quickbook_payment_show_status', 1);
		// $this->db->where("(cp.payment_type = 'P' AND cp.quickbook_payment_sync_status = 1)", NULL, FALSE);
        // $this->db->or_where("(cp.payment_type = 'CM' AND cp.quickbook_payment_sync_status = 0)", NULL, FALSE);
		$this->db->where("((cp.payment_type = 'P' AND cp.quickbook_payment_sync_status = 1) OR (cp.payment_type = 'CM' AND cp.quickbook_payment_sync_status = 0))", NULL, FALSE);
        // $this->db->where('cp.quickbook_payment_sync_status', 1);
		$this->db->where('cp.deleted_at',null);
        // $num_results = $this->db->count_all_results();
		$query = $this->db->get();
        $records = $query->result();
        return $records;
    }
	
	public function get_all_newinvoicepayments($columnName = NULL, $columnSortOrder = NULL, $rowperpage = NULL, $start = NULL, $draw = NULL, $recordsTotal = NULL, $recordsTotalFilter = NULL, $keywordsearch = NULL, $custid = NULL, $regdate = NULL, $regdateto = NULL)
    {
        $response = array();
        $this->db->select('ipm.invoiceId, i.invoice_num, c.customer_id, c.customer_name, c.mobile_number_1, i.invoice_date, i.invoice_net_amount, ipm.pay_reference, cp.paid_amount, ipm.payment_amount, cp.payment_type')
            ->from('InvoicePaymentMapping ipm')
			->join('invoice i', 'ipm.invoiceId = i.invoice_id')
			->join('customers c', 'ipm.inv_customer_id = c.customer_id')
			->join('customer_payments cp', 'ipm.paymentId = cp.payment_id')
            ->order_by('i.invoice_id', 'ASC');
        if ($keywordsearch != "") {
            $this->db->where('i.invoice_num like"%' . $keywordsearch . '%" OR c.customer_name like"%' . $keywordsearch . '%" OR c.mobile_number_1 like"%' . $keywordsearch . '%" OR c.email_address like"%' . $keywordsearch . '%"');
        }
		if ($custid != "") {
            $this->db->where('c.customer_id', $custid);
        }
		if ($regdate && $regdateto) {
            $this->db->where("`i.invoice_date` BETWEEN '$regdate' AND '$regdateto'");
        }
        if (($regdate != "" && $regdateto == "") || ($regdate == "" && $regdateto != "")) {
            if ($regdate != "") {
                $this->db->where("`i.invoice_date` = '$regdate'");
            }
            if ($regdateto != "") {
                $this->db->where("`i.invoice_date` = '$regdate'");
            }
        }
		// if ($regdate != "") {
			// $this->db->where("`i.invoice_date` = '$regdate'");
		// }
        $this->db->where('i.invoice_status !=', 2);
		$this->db->where('ipm.map_status', 1);
		$this->db->where('ipm.qb_sync_stat', 0);
		$this->db->where('i.quickbook_sync_show_status', 1);
		$this->db->where('ipm.qb_sync_show_status', 1);
		$this->db->where('cp.quickbook_payment_show_status', 1);
		// $this->db->where("(cp.payment_type = 'P' AND cp.quickbook_payment_sync_status = 1)", NULL, FALSE);
        // $this->db->or_where("(cp.payment_type = 'CM' AND cp.quickbook_payment_sync_status = 0)", NULL, FALSE);
		$this->db->where("((cp.payment_type = 'P' AND cp.quickbook_payment_sync_status = 1) OR (cp.payment_type = 'CM' AND cp.quickbook_payment_sync_status = 0))", NULL, FALSE);
		// $this->db->where('cp.quickbook_payment_sync_status', 1);
		$this->db->where('cp.deleted_at',null);
        if ($columnName != '' && $columnSortOrder != "") {
            $this->db->order_by($columnName, $columnSortOrder);
        }
        $this->db->limit($rowperpage, $start);
        $query = $this->db->get();
        $records = $query->result();
        $data = array();
        $i = 1;

        //print_r($records);die();
        foreach ($records as $record) {
            $data[] = array(
                'slno' => '<center>'.($i + $start).'<center>',
				'invnum' => $record->invoice_num,
				'name' => $record->customer_name,
				'mobile' => $record->mobile_number_1,
				'invdate' => ($record->invoice_date) ? date("d/m/Y", strtotime($record->invoice_date)) : "",
				'invamount' => $record->invoice_net_amount,
				'reference' => $record->pay_reference.(($record->payment_type == "CM") ? "<br/>Credit Memo" : ""),
				'tottalamount' => $record->paid_amount,
                'allocated' => $record->payment_amount,
            );
            $i++;
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $recordsTotal,
            "lastcall" => $records[0],
            "iTotalDisplayRecords" => $recordsTotalFilter,
            "aaData" => $data
        );

        return $response;
    }
	
	public function get_all_newinvoicepayments_for_sync($custid = NULL,$keywordsearch = NULL, $regdate = NULL, $regdateto = NULL)
    {
        $this->db->select('ipm.inv_map_id, ipm.invoiceId, i.invoice_num, c.customer_id, c.customer_name, c.mobile_number_1, c.email_address, c.quickbook_id, i.invoice_date, i.invoice_net_amount, ipm.pay_reference, cp.paid_amount, cp.paid_datetime, cp.receipt_no, cp.quickbook_payment_id, cp.payment_method, i.invoice_qb_id, ipm.payment_amount,ipm.paymentId,cp.payment_type,cm.quickbook_credit_memo_id')
            ->from('InvoicePaymentMapping ipm')
			->join('invoice i', 'ipm.invoiceId = i.invoice_id')
			->join('customers c', 'ipm.inv_customer_id = c.customer_id')
			->join('customer_payments cp', 'ipm.paymentId = cp.payment_id')
			->join('credit_memo cm', 'cp.creditMemoId = cm.memo_id','left')
            ->order_by('i.invoice_id', 'ASC');
		if ($keywordsearch != "") {
            $this->db->where('i.invoice_num like"%' . $keywordsearch . '%" OR c.customer_name like"%' . $keywordsearch . '%" OR c.mobile_number_1 like"%' . $keywordsearch . '%" OR c.email_address like"%' . $keywordsearch . '%"');
        }
		if ($custid != "") {
            $this->db->where('c.customer_id', $custid);
        }
		if ($regdate && $regdateto) {
            $this->db->where("`i.invoice_date` BETWEEN '$regdate' AND '$regdateto'");
        }
        if (($regdate != "" && $regdateto == "") || ($regdate == "" && $regdateto != "")) {
            if ($regdate != "") {
                $this->db->where("`i.invoice_date` = '$regdate'");
            }
            if ($regdateto != "") {
                $this->db->where("`i.invoice_date` = '$regdate'");
            }
        }
		// if ($regdate != "") {
			// $this->db->where("`i.invoice_date` = '$regdate'");
		// }
		$this->db->where('i.invoice_status !=', 2);
		$this->db->where('ipm.map_status', 1);
		$this->db->where('ipm.qb_sync_stat', 0);
		$this->db->where('i.quickbook_sync_show_status', 1);
		$this->db->where('ipm.qb_sync_show_status', 1);
		$this->db->where('cp.quickbook_payment_show_status', 1);
		$this->db->where('cp.deleted_at',null);
		// $this->db->where("(cp.payment_type = 'P' AND cp.quickbook_payment_sync_status = 1)", NULL, FALSE);
        // $this->db->or_where("(cp.payment_type = 'CM' AND cp.quickbook_payment_sync_status = 0)", NULL, FALSE);
		$this->db->where("((cp.payment_type = 'P' AND cp.quickbook_payment_sync_status = 1) OR (cp.payment_type = 'CM' AND cp.quickbook_payment_sync_status = 0))", NULL, FALSE);
		// $this->db->where('cp.quickbook_payment_sync_status', 1);
        $this->db->limit(100);
        // $this->db->limit(1);
        $query = $this->db->get();
        $records = $query->result();
        $data = array();
        $i = 1;

        foreach ($records as $record) {
            $data[] = array(
				'invoice_pay_id' => $record->inv_map_id,
				'invoice_id' => $record->invoiceId,
				'invoice_num' => $record->invoice_num,
				'invoice_date' => $record->invoice_date,
				'paidtotal' => $record->paid_amount,
				'customer_quickbook_id' => $record->quickbook_id,
				'amountpaiddate' => $record->paid_datetime,
				'receipt' => $record->receipt_no,
				'quickbookpaymentid' => $record->quickbook_payment_id,
				'invoice_qb_id' => $record->invoice_qb_id,
				'allocatedamount' => $record->payment_amount,
				'customer_name' => $record->customer_name,
				'mobile' => $record->mobile_number_1,
				'email' => $record->email_address,
				'paymentId' => $record->paymentId,
				'paymethod' => $record->payment_method,
				'quickbook_credit_memo_id' => $record->quickbook_credit_memo_id,
				'payment_type' => $record->payment_type,
            );
            $i++;
        }
        return $data;
    }
	
	public function count_all_payments()
    {
        $this->db->select('cp.payment_id')
            ->from('customer_payments cp')
			->join('customers c', 'cp.customer_id = c.customer_id');
		$this->db->where('cp.quickbook_payment_sync_status', 0);
		$this->db->where('cp.quickbook_payment_show_status', 1);
		$this->db->where('cp.payment_type', 'P');
		$this->db->where('cp.deleted_at',null);
        // $num_results = $this->db->count_all_results();
        $query = $this->db->get();
        $records = $query->result();
		// echo $num_results;
		// exit();
        return $records;
    }
	
	public function get_all_paymentsnew($keywordsearch = NULL, $custid = NULL, $regdate = NULL, $regdateto = NULL, $paymode = NULL)
    {
        $this->db->select('cp.payment_id')
            ->from('customer_payments cp')
			->join('customers c', 'cp.customer_id = c.customer_id');
        if ($keywordsearch != "") {
            $this->db->where('c.customer_name like"%' . $keywordsearch . '%" OR c.mobile_number_1 like"%' . $keywordsearch . '%" OR c.email_address like"%' . $keywordsearch . '%"');
        }
		if ($custid != "") {
            $this->db->where('c.customer_id', $custid);
        }
		if ($paymode != "") {
            $this->db->where('cp.payment_method', $paymode);
        }
		if ($regdate && $regdateto) {
            $this->db->where("DATE(`paid_datetime`) BETWEEN '$regdate' AND '$regdateto'");
        }
        if (($regdate != "" && $regdateto == "") || ($regdate == "" && $regdateto != "")) {
            if ($regdate != "") {
                $this->db->where("DATE(`paid_datetime`) = '$regdate'");
            }
            if ($regdateto != "") {
                $this->db->where("DATE(`paid_datetime`) = '$regdateto'");
            }
        }
		// if ($regdate != "") {
			// $this->db->where("DATE(`paid_datetime`) = '$regdate'");
		// }
		$this->db->where('cp.quickbook_payment_sync_status', 0);
		$this->db->where('cp.quickbook_payment_show_status', 1);
		$this->db->where('cp.payment_type', 'P');
		$this->db->where('cp.deleted_at',null);
        // $num_results = $this->db->count_all_results();
		$query = $this->db->get();
        $records = $query->result();
        return $records;
    }
	
	public function get_all_newpayments($columnName = NULL, $columnSortOrder = NULL, $rowperpage = NULL, $start = NULL, $draw = NULL, $recordsTotal = NULL, $recordsTotalFilter = NULL, $keywordsearch = NULL, $custid = NULL, $regdate = NULL, $regdateto = NULL, $paymode = NULL)
    {
        $response = array();
        $this->db->select('cp.customer_id, cp.payment_method, cp.paid_datetime, cp.paid_amount, c.customer_name, c.mobile_number_1')
            ->from('customer_payments cp')
			->join('customers c', 'cp.customer_id = c.customer_id');
        if ($keywordsearch != "") {
            $this->db->where('c.customer_name like"%' . $keywordsearch . '%" OR c.mobile_number_1 like"%' . $keywordsearch . '%" OR c.email_address like"%' . $keywordsearch . '%"');
        }
		if ($custid != "") {
            $this->db->where('c.customer_id', $custid);
        }
		if ($paymode != "") {
            $this->db->where('cp.payment_method', $paymode);
        }
		if ($regdate && $regdateto) {
            $this->db->where("DATE(`paid_datetime`) BETWEEN '$regdate' AND '$regdateto'");
        }
        if (($regdate != "" && $regdateto == "") || ($regdate == "" && $regdateto != "")) {
            if ($regdate != "") {
                $this->db->where("DATE(`paid_datetime`) = '$regdate'");
            }
            if ($regdateto != "") {
                $this->db->where("DATE(`paid_datetime`) = '$regdateto'");
            }
        }
		// if ($regdate != "") {
			// $this->db->where("DATE(`paid_datetime`) = '$regdate'");
		// }
        $this->db->where('cp.quickbook_payment_sync_status', 0);
		$this->db->where('cp.quickbook_payment_show_status', 1);
		$this->db->where('cp.payment_type', 'P');
		$this->db->where('cp.deleted_at',null);
        if ($columnName != '' && $columnSortOrder != "") {
            $this->db->order_by($columnName, $columnSortOrder);
        }
        $this->db->limit($rowperpage, $start);
        $query = $this->db->get();
        $records = $query->result();
        $data = array();
        $i = 1;

        //print_r($records);die();
        foreach ($records as $record) {
			if($record->payment_method == 0)
			{
				$paymethod = "Cash";
			} else if($record->payment_method == 1){
				$paymethod = "Card";
			}
			else if($record->payment_method == 2){
				$paymethod = "Cheque";
			}
			else if($record->payment_method == 3){
				$paymethod = "Bank";
			}
			else if($record->payment_method == 4){
				$paymethod = "Credit Card";
			}
			
            $data[] = array(
                'slno' => '<center>'.($i + $start).'<center>',
				'name' => $record->customer_name,
				'mobile' => $record->mobile_number_1,
				'paytype' => $paymethod,
				'paydate' => ($record->paid_datetime) ? date("d/m/Y", strtotime($record->paid_datetime)) : "",
				'amount' => $record->paid_amount,
            );
            $i++;
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $recordsTotal,
            "lastcall" => $records[0],
            "iTotalDisplayRecords" => $recordsTotalFilter,
            "aaData" => $data
        );

        return $response;
    }
	
	public function get_all_newpayments_for_sync($custid = NULL,$search = NULL,$regdate = NULL, $regdateto = NULL, $paymode = NULL)
    {
        $this->db->select('cp.payment_id, cp.customer_id, cp.payment_method, cp.paid_datetime, cp.paid_amount, cp.receipt_no, cp.quickbook_payment_id, cp.quickbook_payment_sync_status, c.customer_name, c.mobile_number_1, c.quickbook_id, c.quickbook_sync_status')
            ->from('customer_payments cp')
			->join('customers c', 'cp.customer_id = c.customer_id');
		if ($search != "") {
            $this->db->where('c.customer_name like"%' . $search . '%" OR c.mobile_number_1 like"%' . $search . '%" OR c.email_address like"%' . $search . '%"');
        }
		if ($custid != "") {
            $this->db->where('c.customer_id', $custid);
        }
		if ($paymode != "") {
            $this->db->where('cp.payment_method', $paymode);
        }
		if ($regdate && $regdateto) {
            $this->db->where("DATE(`paid_datetime`) BETWEEN '$regdate' AND '$regdateto'");
        }
        if (($regdate != "" && $regdateto == "") || ($regdate == "" && $regdateto != "")) {
            if ($regdate != "") {
                $this->db->where("DATE(`paid_datetime`) = '$regdate'");
            }
            if ($regdateto != "") {
                $this->db->where("DATE(`paid_datetime`) = '$regdateto'");
            }
        }
		// if ($regdate != "") {
			// $this->db->where("DATE(`paid_datetime`) = '$regdate'");
		// }
		$this->db->where('cp.quickbook_payment_sync_status', 0);
		$this->db->where('cp.quickbook_payment_show_status', 1);
		$this->db->where('cp.payment_type', 'P');
		$this->db->where('cp.deleted_at',null);
        $this->db->limit(100);
        // $this->db->limit(1);
        $query = $this->db->get();
        $records = $query->result();
        $data = array();
        $i = 1;

        foreach ($records as $record) {
            $data[] = array(
				'customerid' => $record->customer_id,
				'customer_name' => $record->customer_name,
				'mobile' => $record->mobile_number_1,
				'customer_quickbook_id' => $record->quickbook_id,
				'customerquickbook_sync_status' => $record->quickbook_sync_status,
				'paymethod' => $record->payment_method,
				'receipt_no' => $record->receipt_no,
				'paidtime' => $record->paid_datetime,
				'paidamount' => $record->paid_amount,
				'quickbook_payment_id' => $record->quickbook_payment_id,
				'quickbook_payment_sync_status' => $record->quickbook_payment_sync_status,
				'paymentId' => $record->payment_id,
            );
            $i++;
        }
        return $data;
    }
	
	function add_quick_customers($data)
    {
        return $this->db->insert_batch('quickbookCustomers', $data);
    }
	
	function check_quick_customers($quickid)
	{
		$this->db->select('*')
            ->from('quickbookCustomers');
		$this->db->where('quickBookId', $quickid);
        $query = $this->db->get();
        $records = $query->row();
        return $records;
	}
	
	function get_customer_details_for_update($mob)
	{
		$this->db->select('*')
            ->from('quickbookCustomers');
		$this->db->where('RIGHT(PrimaryPhone, 9) =',$mob,true);
		$this->db->where('syncstatus', 0);
        $query = $this->db->get();
        $records = $query->row();
		// echo $this->db->last_query();
		// exit();
        return $records;
	}
	
	function update_customers($fields = array(),$customerId)
    {
        $this->db->where('customer_id', $customerId);
        $this->db->update('customers', $fields);
        return $this->db->affected_rows();
    }
	
	function update_quickbookcustomers($fields = array(),$customerId)
    {
        $this->db->where('id', $customerId);
        $this->db->update('quickbookCustomers', $fields);
        return $this->db->affected_rows();
    }
	
	function update_quick_credit_memo_by_payid($memoId,$fields = array(), $input_json=NULL,$output=NULL,$jsonheaders=NULL,$quickbook_iddd=NULL)
    {
        $this->db->where('memo_id', $memoId);
        $this->db->update('credit_memo', $fields);
        if($this->db->affected_rows())
		{
			$insertarray = array();
			$insertarray['type'] = 5;
			$insertarray['quickbook_id'] = $quickbook_iddd;
			$insertarray['emaid_id'] = $memoId;
			$insertarray['input'] = $input_json;
			$insertarray['header'] = $jsonheaders;
			$insertarray['output'] = $output;
			$this->db->set($insertarray);
			$this->db->insert('quickbook_log');
			return $this->db->insert_id();
		}
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
    // function add_quick_customers($data)
    // {
        // $this->db->set($data);
		// $this->db->insert('quickbook_customer');
        // $result = $this->db->insert_id();
        // return $result;
    // }
	
	
	
	function get_quickbookdetails($id)
	{
		$this->db->select('access_token,refresh_token')
                ->from('quickbook_details')
				->where('id',$id);
		$get_qry = $this->db->get();
        return $get_qry->row();
	}
	
	

    

    function get_quickbook_cust_sync_msg()
    {
        $this->db->select('*')
                ->from('qb_sync_tmp_msg');

        $this->db->where('tmp_msg_id', 1);
        $this->db->where('tmp_msg_stat', 1);
        $get_customers_qry = $this->db->get();
        return $get_customers_qry->row();
    }

    function get_quickbook_inv_sync_msg()
    {
        $this->db->select('*')
                ->from('qb_sync_tmp_msg');

        $this->db->where('tmp_msg_id', 2);
        $this->db->where('tmp_msg_stat', 1);
        $get_customers_qry = $this->db->get();
        return $get_customers_qry->row();
    }

    function get_quickbook_inv_pay_sync_msg()
    {
        $this->db->select('*')
                ->from('qb_sync_tmp_msg');

        $this->db->where('tmp_msg_id', 4);
        $this->db->where('tmp_msg_stat', 1);
        $get_customers_qry = $this->db->get();
        return $get_customers_qry->row();
    }

    function get_quickbook_quo_sync_msg()
    {
        $this->db->select('*')
                ->from('qb_sync_tmp_msg');

        $this->db->where('tmp_msg_id', 3);
        $this->db->where('tmp_msg_stat', 1);
        $get_quickbook_quo_sync_msg_qry = $this->db->get();
        return $get_quickbook_quo_sync_msg_qry->row();
    }

    function get_invoice_detailbyid($invoice_id)
    {
        $this->db->select('c.total_invoice_amount,c.balance,c.total_paid_amount,c.customer_trn,c.email_address,c.quickbook_id,i.invoice_id,i.invoice_num,i.customer_name,i.customer_id,i.bill_address,i.invoice_status, i.invoice_paid_status, i.added, i.service_date, i.invoice_date, i.invoice_due_date, i.invoice_total_amount, i.invoice_tax_amount, i.invoice_net_amount, i.balance_amount, i.received_amount,i.invoice_qb_id,i.invoice_qb_sync_stat, l.maid_name, l.service_from_time, l.service_to_time, l.line_bill_address, l.description, l.line_amount, l.line_vat_amount, l.line_net_amount,l.invoice_line_id,l.service_hrs,l.monthly_product_service, u.user_fullname,st.service_type_name,st.qb_sync_id,st.qb_sync_stat,c.customer_id as custid,l.service_date as jobdate,ds.service_date as newjobdate')
                ->from('invoice i')
                ->join('invoice_line_items l','i.invoice_id = l.invoice_id','INNER')
                ->join('day_services ds','ds.day_service_id = l.day_service_id','LEFT')
                ->join('bookings b','b.booking_id = ds.booking_id','LEFT')
                ->join('service_types st','st.service_type_id = b.service_type_id','LEFT')
                ->join('customers c','c.customer_id = i.customer_id')
                ->join('users u','i.invoice_added_by = u.user_id','LEFT')
                ->where('i.invoice_id',$invoice_id)
                ->where('l.line_status',1);
        $get_invoice_qry = $this->db->get();
        return $get_invoice_qry->result();
    }


    function get_invoice_payment_by_invnum($invnum)
    {
        $this->db->select('ipm.*')
                ->from('InvoicePaymentMapping ipm')
                ->where('ipm.inv_reference',$invnum)
                ->where('ipm.qb_sync_stat','0');
        $get_invoice_payment_by_invnum_qry = $this->db->get();
        return $get_invoice_payment_by_invnum_qry->result();
    }

    

    

    function get_customer_invoices_for_qb_sync($date_from= NULL)
    {
        $this->db->select('i.*,c.email_address')
                ->from('invoice i')
                ->join('customers c','c.customer_id = i.customer_id');

        //$this->db->where('i.invoice_type', 1);
        if($date_from != NULL)
        {
            $this->db->where('i.invoice_date', $date_from);
        }        
        $this->db->where('i.invoice_qb_sync_stat', '0');
        $this->db->where('i.invoice_status !=', '2');//to avoid loading cancelled invoices on default load
        $this->db->where('i.invoice_status !=', '0');
        $this->db->limit(10); 

        $get_invoice_qry = $this->db->get();
        return $get_invoice_qry->result();
    }


    function get_customer_invoices_for_qb_sync_count($date_from= NULL)
    {
        $this->db->select('i.*,c.email_address')
                ->from('invoice i')
                ->join('customers c','c.customer_id = i.customer_id');

        //$this->db->where('i.invoice_type', 1);
        if($date_from != NULL)
        {
            $this->db->where('i.invoice_date', $date_from);
        }        
        $this->db->where('i.invoice_qb_sync_stat', '0');
        $this->db->where('i.invoice_status !=', '2');//to avoid loading cancelled invoices on default load
        $this->db->where('i.invoice_status !=', '0');
        //$this->db->limit(10); 

        $get_invoice_qry = $this->db->get();
        return $get_invoice_qry->num_rows();
    }

    function get_customer_reciepts_for_qb_sync($date_from= NULL)
    {
        $this->db->select('i.*,c.email_address')
                ->from('invoice i')
                ->join('customers c','c.customer_id = i.customer_id');

        $this->db->where('i.invoice_type', 1);
        if($date_from != NULL)
        {
            $this->db->where('i.invoice_date', $date_from);
        }        
        $this->db->where('i.invoice_qb_sync_stat', '0');
        $this->db->where('i.invoice_status !=', '2');//to avoid loading cancelled invoices on default load
        $this->db->where('i.invoice_status !=', '0');
        $this->db->limit(10); 

        $get_invoice_qry = $this->db->get();
        return $get_invoice_qry->result();
    }


    function get_customer_reciepts_for_qb_sync_count($date_from= NULL)
    {
        $this->db->select('i.*,c.email_address')
                ->from('invoice i')
                ->join('customers c','c.customer_id = i.customer_id');

        $this->db->where('i.invoice_type', 1);
        if($date_from != NULL)
        {
            $this->db->where('i.invoice_date', $date_from);
        }        
        $this->db->where('i.invoice_qb_sync_stat', '0');
        $this->db->where('i.invoice_status !=', '2');//to avoid loading cancelled invoices on default load
        $this->db->where('i.invoice_status !=', '0');
        //$this->db->limit(10); 

        $get_invoice_qry = $this->db->get();
        return $get_invoice_qry->num_rows();
    }

    function get_customer_quotations_for_qb_sync($date_from= NULL)
    {
        $this->db->select('q.*,c.email_address,c.customer_name,st.service_type_name')
                ->from('quotation q')
                ->join('customers c','c.customer_id = q.quo_customer_id')
                ->join('service_types st','st.service_type_id = q.quo_servicetype_id','LEFT');

        $this->db->where('q.quo_qb_sync_stat', 0);
        if($date_from != NULL)
        {
            $this->db->where('q.quo_date', $date_from);
        }        
        // $this->db->where('i.invoice_qb_sync_stat', '0');
        // $this->db->where('i.invoice_status !=', '2');//to avoid loading cancelled invoices on default load
        // $this->db->where('i.invoice_status !=', '0');
        $this->db->limit(10); 

        $get_quotation_qry = $this->db->get();
        return $get_quotation_qry->result();
    }


    function get_customer_quotations_for_qb_sync_count($date_from= NULL)
    {
        $this->db->select('q.*,c.email_address,c.customer_name,st.service_type_name')
                ->from('quotation q')
                ->join('customers c','c.customer_id = q.quo_customer_id')
                ->join('service_types st','st.service_type_id = q.quo_servicetype_id','LEFT');

        $this->db->where('q.quo_qb_sync_stat', 0);
        if($date_from != NULL)
        {
            $this->db->where('q.quo_date', $date_from);
        }        
        // $this->db->where('i.invoice_qb_sync_stat', '0');
        // $this->db->where('i.invoice_status !=', '2');//to avoid loading cancelled invoices on default load
        // $this->db->where('i.invoice_status !=', '0');
        

        $get_invoice_qry = $this->db->get();
        return $get_invoice_qry->num_rows();
    }



    function get_quotation_detailbyid($quotation_id)
    {
        $this->db->select('q.*,c.email_address,c.customer_name,c.quickbook_id,st.service_type_name')
                ->from('quotation q')
                ->join('customers c','c.customer_id = q.quo_customer_id')
                ->join('service_types st','st.service_type_id = q.quo_servicetype_id','LEFT');

        $this->db->where('q.quo_id', $quotation_id);        

        $get_invoice_qry = $this->db->get();
        return $get_invoice_qry->row();
    }

    function update_quotation($quo_id,$fields = array())
    {
        $this->db->where('quo_id', $quo_id);
        $this->db->update('quotation', $fields);
        return $this->db->affected_rows();
    }

    function get_invoice_payments_for_qb_sync($date)
    {

        $this->db->select('ipm.*,cp.*,c.customer_name')
                ->from('InvoicePaymentMapping ipm')
                ->join('customer_payments cp','cp.customer_id = ipm.inv_customer_id')
                ->join('customers c','c.customer_id = ipm.inv_customer_id');
        

        
        $this->db->where("date(ipm.payment_added_datetime)", $date);
        $this->db->where("ipm.qb_sync_stat", 0);
        $this->db->where("ipm.map_status", 1);
        $this->db->group_by('ipm.inv_map_id');
        
        $get_pay_qry = $this->db->get();
        return $get_pay_qry->result();
    }



    function get_invoice_payments_for_qb_sync_count($date)
    {

        $this->db->select('ipm.*,cp.*,c.customer_name')
                ->from('InvoicePaymentMapping ipm')
                ->join('customer_payments cp','cp.customer_id = ipm.inv_customer_id')
                ->join('customers c','c.customer_id = ipm.inv_customer_id');
        

        
        $this->db->where("date(ipm.payment_added_datetime)", $date);
        $this->db->where("ipm.qb_sync_stat", 0);
        $this->db->where("ipm.map_status", 1);
        $this->db->group_by('ipm.inv_map_id');
        
        
        $get_pay_qry = $this->db->get();
        return $get_pay_qry->num_rows();
    }

    function get_invoice_payment_detailbyid($ipm_id)
    {
        $this->db->select('ipm.*,cp.*,c.customer_name,c.quickbook_id as cust_quickbook_id ,inv.invoice_qb_sync_stat,inv.invoice_qb_id')
                ->from('InvoicePaymentMapping ipm')
                ->join('customer_payments cp','cp.customer_id = ipm.inv_customer_id')
                ->join('customers c','c.customer_id = ipm.inv_customer_id')
                ->join('invoice inv','inv.invoice_id = ipm.invoiceId');

        $this->db->where("ipm.inv_map_id", $ipm_id);
        $this->db->group_by('ipm.inv_map_id');

        $get_pay_qry = $this->db->get();
        return $get_pay_qry->row();
    }


    public function fetch_service_types_for_quickbook_sync() {

        $this->db->select("st.*")
                ->from('service_types st')
                ->where('st.service_type_status','1')
                ->where('st.qb_sync_stat','0');

        $this->db->limit(10); 
        $query = $this->db->get();

        return $query->result();
    }


    public function fetch_service_types_for_quickbook_count() {

        $this->db->select("st.*")
                ->from('service_types st')
                ->where('st.service_type_status','1')
                ->where('st.qb_sync_stat','0');

        
        $query = $this->db->get();

        return $query->num_rows();
    }

    function get_service_details($service_id)
    {
        $this->db->select('*')->from('service_types')->where('service_type_id',$service_id);
        $query = $this->db->get();
        return $query->row();
    }

    function get_quickbook_servicetype_sync_msg()
    {
        $this->db->select('*')
                ->from('qb_sync_tmp_msg');

        $this->db->where('tmp_msg_id', 5);
        $this->db->where('tmp_msg_stat', 1);
        $get_quickbook_servicetype_sync_msg_qry = $this->db->get();
        return $get_quickbook_servicetype_sync_msg_qry->row();
    }
}