<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 *
 * @author  Samnad. S
 * @since   Version 1.0
 * Created on : 20/10/2023
 */
class Maid_skills_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function get_maid_skills_for_dropdown()
    {
        return $this->db->select('ms.id as value,ms.skill as text')
            ->from('maid_skills as ms')
            ->where('ms.deleted_at', null)
            ->order_by('ms.skill', 'ASC')
            ->get()
            ->result();
    }
    public function maid_skills_by_maid_id($maid_id)
    {
        return $this->db->select('msm.id,msm.maid_skill_id,msm.rating_level_id,ms.skill,rl.rating_level,msm.notes')
		->from('maid_skills_mapping as msm')
		->join('maid_skills as ms', 'msm.maid_skill_id = ms.id')
		->join('rating_levels as rl', 'msm.rating_level_id = rl.rating_level_id')
		->where('msm.maid_id', $maid_id)
		->where('msm.deleted_at', null)
		->where('ms.deleted_at', null)
        ->get()
        ->result();
    }
}
