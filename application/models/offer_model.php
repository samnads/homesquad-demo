<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Offer_model extends CI_Model{
    
    public function add_offer($data)
    {
       $this->db->set($data);
	   $this->db->insert('offers_sb');
       $result = $this->db->insert_id();
       return $result;
    }
    function get_offers()
    {
        $query = $this->db->query("SELECT * FROM `offers_sb`");
        return $query->result_array();
    }
    function remove_offer($id_offers_sb)
    {
        $this->db->where('id_offers_sb',$id_offers_sb);
        $this->db->delete('offers_sb');
    }
    function get_offer_details($id_offers_sb)
    {
        $qr = $this->db->select('*')
                ->from('offers_sb')
                ->where('id_offers_sb',$id_offers_sb);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function update_offer($data,$id_offers_sb)
    {
        $this->db->where('id_offers_sb',$id_offers_sb);
        $this->db->update('offers_sb',$data);
    }
    
    public function change_status()
    {
        $id_offers_sb=$this->input->post('id_offers_sb');
        $query=$this->db->query("SELECT * FROM `offers_sb` WHERE `id_offers_sb`='".$id_offers_sb."'");
        $row=$query->row_array();
        if($row['offer_status']==0){ $q2=$this->db->query("UPDATE `offers_sb` SET `offer_status`='1' WHERE `id_offers_sb`='".$id_offers_sb."' "); echo "Offer Status Changed To Disabled"; }
        else{ $q2=$this->db->query("UPDATE `offers_sb` SET `offer_status`='0' WHERE `id_offers_sb`='".$id_offers_sb."' "); echo "Offer Status Changed To Active"; }
    }
}