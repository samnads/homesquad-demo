<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 *
 * @author  Samnad. S
 * @since   Version 1.0
 * Created on : 20/10/2023
 */
class Rating_levels_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function get_rating_levels_for_dropdown()
    {
        return $this->db->select('rl.rating_level_id as value,rl.rating_level as text')
            ->from('rating_levels as rl')
            ->order_by('rl.rating_level_id', 'ASC')->get()->result();
    }
}
