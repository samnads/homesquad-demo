<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 /** 
  * Day_services_model Class 
  * 
  * @package	Homemaid
  * @author     Geethu
  * @since      Version 1.0
  */
class Invoice_model extends CI_Model
{
    function __construct()
    {
            parent::__construct();
    }

	/**
	 * Get count of customer outstanding invoices
	 */
	function count_customer_outstanding($customer_id){
		$this->db->select('i.*,c.email_address,c.customer_name as userName')
                ->from('invoice i')
                ->join('customers c','c.customer_id = i.customer_id');
		$this->db->where('i.invoice_type', 1);
		$this->db->where('i.showStatus', 1);
		$this->db->where('i.customer_id', $customer_id);
		$this->db->where_in('i.invoice_paid_status',[1,2]);
		$this->db->where_in('i.invoice_status', [0,1]);
        $num_results = $this->db->count_all_results();
        return $num_results;
	}

	/**
	 * Function to retrieve customer outstanding invoices
	 */
	function get_customer_outstanding_invoices($customer_id){
		$this->db->select('i.*,c.email_address,c.customer_name as userName')
                ->from('invoice i')
                ->join('customers c','c.customer_id = i.customer_id');
		$this->db->where('i.invoice_type', 1);
		$this->db->where('i.showStatus', 1);
		$this->db->where('i.customer_id', $customer_id);
		$this->db->where_in('i.invoice_paid_status',[0,1,2]);
		$this->db->where_in('i.invoice_status', [0,1]);
		$getOutInvoiceQry = $this->db->order_by('i.invoice_id','desc')->get();
        return $getOutInvoiceQry->result();
	}
	//----------------------------------------------------------------

	function get_customer_invoices($date_from= NULL,$date_to= NULL,$customer_id,$inv_status)
    {
        $this->db->select('i.*,c.email_address,c.customer_name as userName')
                ->from('invoice i')
                ->join('customers c','c.customer_id = i.customer_id');

        $this->db->where('i.invoice_type', 1);
		$this->db->where('i.showStatus', 1);
		if($date_from != NULL && $date_to != NULL)
		{
			$this->db->where("i.invoice_date BETWEEN '$date_from' AND '$date_to'");
		}
		if($customer_id != NULL)
		{
			$this->db->where('i.customer_id', $customer_id);
		}
		if($inv_status != NULL && $inv_status!=4)
		{
			$this->db->where('i.invoice_status', $inv_status);
		}else if($inv_status != NULL && $inv_status==4)
		{
			$this->db->where('i.invoice_paid_status',0);
			$this->db->where_in('i.invoice_status', [0,1]);
		}
		else
		{
			$this->db->where('i.invoice_status !=', '2');//to avoid loading cancelled invoices on default load
		}
        $get_invoice_qry = $this->db->order_by('i.invoice_id','desc')->get();
        return $get_invoice_qry->result();
    }


    function get_customer_monthly_invoices($date_from= NULL,$date_to= NULL,$customer_id,$inv_status)
    {
        $this->db->select('i.*,c.email_address,c.customer_name as userName')
                ->from('invoice as i')
				->order_by('i.invoice_id','desc')
                ->join('customers as c','c.customer_id = i.customer_id');

        $this->db->where('i.invoice_type', 2);
		$this->db->where('i.showStatus', 1);
		if($date_from != NULL && $date_to != NULL)
		{
			$this->db->where("i.invoice_date BETWEEN '$date_from' AND '$date_to'");
		}
		if($customer_id != NULL)
		{
			$this->db->where('i.customer_id', $customer_id);
		}
		if($inv_status != NULL && $inv_status!=4)
		{
			$this->db->where('i.invoice_status', $inv_status);
		}else if($inv_status != NULL && $inv_status==4)
		{
			$this->db->where('i.invoice_paid_status',0);
			$this->db->where_in('i.invoice_status', [0,1]);
		}
		else
		{
			$this->db->where('i.invoice_status !=', '2');//to avoid loading cancelled invoices on default load
		}
        $get_invoice_qry = $this->db->get();
        return $get_invoice_qry->result();
    }
	
	function get_invoice_detailbyid($invoice_id)
	{
		$this->db->select('c.total_invoice_amount,c.balance,c.total_paid_amount,c.trnnumber,c.email_address,i.invoice_id,i.invoice_num,c.customer_name,i.customer_id,i.bill_address,i.invoice_status, i.invoice_paid_status, i.added, ds.service_date, i.invoice_date, i.invoice_due_date, i.invoice_total_amount, i.invoice_tax_amount, i.invoice_net_amount, i.balance_amount, i.received_amount, m.maid_name, l.service_from_time, l.service_to_time, l.line_bill_address, l.description, l.line_amount, l.line_vat_amount, l.line_net_amount,l.invoice_line_id,l.service_hrs,l.monthly_product_service, u.user_fullname,st.service_type_name,c.customer_id as custid,l.inv_unit_price,i.isNullified')
                ->from('invoice i')
				->join('invoice_line_items l','i.invoice_id = l.invoice_id','INNER')
				->join('day_services ds','ds.day_service_id = l.day_service_id','LEFT')
				->join('maids m','ds.maid_id = m.maid_id','LEFT')
				->join('bookings b','b.booking_id = ds.booking_id','LEFT')
				->join('service_types st','st.service_type_id = b.service_type_id','LEFT')
				->join('customers c','c.customer_id = i.customer_id')
				->join('users u','i.invoice_added_by = u.user_id','LEFT')
				->where('i.invoice_id',$invoice_id)
				->where('l.line_status',1)
				->order_by('ds.service_date','asc');
		$get_invoice_qry = $this->db->get();
        return $get_invoice_qry->result();
	}

	function get_quotation_detailbyid($quotation_id)
	{
		$this->db->select('q.*,c.email_address,c.mobile_number_1,st.service_type_name')
                ->from('quotation q')
				->join('customers c','c.customer_id = q.quo_customer_id')
				->join('users u','q.quo_added_by = u.user_id','LEFT')
				->join('service_types st','q.quo_servicetype_id = st.service_type_id','LEFT')
				->where('q.quo_id',$quotation_id);
		$get_quotation_detailbyid_qry = $this->db->get();
        return $get_quotation_detailbyid_qry->row();
	}

	function get_quotation_line_items_byqid($quotation_id)
	{
		$this->db->select('qli.*,st.service_type_name')
                ->from('quotation_line_items qli')
				->join('service_types st','qli.qli_service_type_id = st.service_type_id')
				->where('qli.qli_quotation_id',$quotation_id);

		$get_quotation_line_items_byqid_qry = $this->db->get();
        return $get_quotation_line_items_byqid_qry->result();
	}
	
	function get_customer_quotations($date_from= NULL,$date_to= NULL,$customer_id)
    {
        $this->db->select('q.*,c.email_address')
                ->from('quotation q')
                ->join('customers c','c.customer_id = q.quo_customer_id');

        //$this->db->where('i.invoice_type', 1);
		if($date_from != NULL && $date_to != NULL)
		{
			$this->db->where("q.quo_date BETWEEN '$date_from' AND '$date_to'");
		}
		if($customer_id != NULL)
		{
			$this->db->where('q.quo_customer_id', $customer_id);
		}
		
        $get_customer_quotations_qry = $this->db->get();
        return $get_customer_quotations_qry->result();
    }

	function get_customer_detail($cust_id)
	{
		$this->db->select('c.total_invoice_amount,c.balance,c.last_invoice_date')
                ->from('customers c')
				->where('c.customer_id',$cust_id);
		$get_invoice_qry = $this->db->get();
        return $get_invoice_qry->row();
	}
	
	function get_customer_list()
	{
		$this->db->select('c.customer_id,c.customer_name')
                ->from('customers c');
		$get_invoice_qry = $this->db->get();
        return $get_invoice_qry->result();
	}

	function get_monthly_customer_list()
	{
		$this->db->select('c.customer_id,c.customer_name')
                ->from('customers c')
                ->where('c.payment_type','M');
		$get_invoice_qry = $this->db->get();
        return $get_invoice_qry->result();
	}

	function get_monthly_bookings_for_invoice($customer_id,$start_date,$end_date)
	{
		$this->db->select("ds.customer_id, ds.booking_id, ds.day_service_id, ds.customer_name, ds.customer_address, ds.service_date, ds.serviceamount,  b.total_amount as total_fee, ds.booking_service_id, ds.material_fee, ds.maid_id, m.maid_name, DATE_FORMAT(b.time_from, '%h:%i %p') AS time_from, DATE_FORMAT(b.time_to, '%h:%i %p') AS time_to, b.time_from as start_time, b.time_to as end_time, b.discount, c.price_hourly", FALSE)/*ds.vatamount,*/
			   ->from('day_services ds')
			   ->join('bookings b','ds.booking_id = b.booking_id')
			   ->join('customers c', 'b.customer_id = c.customer_id')
			   ->join('maids m', 'ds.maid_id = m.maid_id')
			   ->where('ds.service_status',2)
			   ->where('ds.invoice_status',0)
			   ->where('ds.customer_id',$customer_id)
			   ->where('ds.service_date >=', $start_date)
			   ->where('ds.service_date <=', $end_date)
			   ->order_by('ds.service_date');
		$get_service_qry = $this->db->get();
        return $get_service_qry->result();
	}
	
	function update_invoice_detail($invoice_id,$cust_id,$total_invoice,$total_balance)
	{
		$dat = date('Y-m-d');
		$stat_array = array();
		$stat_array['invoice_status'] = 1;
		$this->db->where('invoice_id', $invoice_id);
		$this->db->update('invoice', $stat_array);
		if($this->db->affected_rows())
		{
			$cust_array = array();
			$cust_array['total_invoice_amount'] = $total_invoice;
			$cust_array['balance'] = $total_balance;
			$cust_array['last_invoice_date'] = $dat;
			$this->db->where('customer_id', $cust_id);
			$this->db->update('customers', $cust_array);
			return $this->db->affected_rows();
		}
		
	}

	function update_customer_detail($custid,$fields = array())
	{
		$this->db->where('customer_id', $custid);
		$this->db->update('customers', $fields);
		return $this->db->affected_rows();
	}
	
	function update_invoice_status_detail($invoice_id,$cust_id,$total_invoice,$total_balance,$last_date)
	{
		//$dat = date('Y-m-d');
		$stat_array = array();
		$stat_array['invoice_status'] = 2;
		$this->db->where('invoice_id', $invoice_id);
		$this->db->update('invoice', $stat_array);
		if($this->db->affected_rows())
		{
			$cust_array = array();
			$cust_array['total_invoice_amount'] = $total_invoice;
			$cust_array['balance'] = $total_balance;
			//$cust_array['last_invoice_date'] = $last_date;
			$this->db->where('customer_id', $cust_id);
			$this->db->update('customers', $cust_array);
			if($this->db->affected_rows())
			{
				$cust_array_new = array();
				$cust_array_new['invoice_status'] = 0;
				$this->db->where('serv_invoice_id', $invoice_id);
				$this->db->update('day_services', $cust_array_new);
				return $this->db->affected_rows();
			}
		}
		
	}
	
	function update_invoice_status($invoice_id)
	{
		$stat_array = array();
		$stat_array['invoice_status'] = 2;
		$this->db->where('invoice_id', $invoice_id);
		$this->db->update('invoice', $stat_array);
		if($this->db->affected_rows())
		{
			$cust_array = array();
			$cust_array['invoice_status'] = 0;
			$this->db->where('serv_invoice_id', $invoice_id);
			$this->db->update('day_services', $cust_array);
			return $this->db->affected_rows();
		}
	}
	

	function update_invoicelineitems($invoiceid,$fields = array())
	{
		$this->db->where('invoice_line_id', $invoiceid);
		$this->db->update('invoice_line_items', $fields);
		return $this->db->affected_rows();
	}
	
	function update_invoicepay_detail($invoicelineid,$fields = array())
	{
		$this->db->where('invoice_id', $invoicelineid);
		$this->db->update('invoice', $fields);
		return $this->db->affected_rows();
	}

	function add_payment_invoice($fields = array())
	{
		$this->db->set($fields);
        $this->db->insert('InvoicePaymentMapping'); 
        $map_id = $this->db->insert_id();

        return $map_id;
	}

	function add_quotation($fields = array())
	{
		$this->db->set($fields);
        $this->db->insert('quotation'); 
        $quotation_id = $this->db->insert_id();

        return $quotation_id;
	}

	function add_quotation_line_item($fields = array())
	{
		$this->db->set($fields);
        $this->db->insert('quotation_line_items'); 
        $quotation_line_item_id = $this->db->insert_id();

        return $quotation_line_item_id;
	}

	function update_quotation($quotation_id,$quotation_fields)
	{
		
		$this->db->where('quo_id', $quotation_id);
		$this->db->update('quotation', $quotation_fields);
		return $this->db->affected_rows();
	}
	
	function update_customerpay_detail($payid,$update_cust_array)
	{
		$this->db->where('payment_id', $payid);
		$this->db->update('customer_payments', $update_cust_array);
		return $this->db->affected_rows();
	}
	
	function get_current_allocateamt($pay_id)
	{
		$this->db->select('c.allocated_amount')
                ->from('customer_payments c')
				->where('c.payment_id',$pay_id);
		$get_invoice_qry = $this->db->get();
        return $get_invoice_qry->row();
	}
	
	function get_customer_invoice_address($customer_id)
	{
		$this->db->select('c.customer_name,ca.customer_address')
                ->from('customers c')
				->join('customer_addresses ca','c.customer_id = ca.customer_id')
				->where('c.customer_id', $customer_id)
                ->where('ca.default_address',1)
                ->limit(1);
        $get_invoice_qry = $this->db->get();
        return $get_invoice_qry->row();
	}

    
	
	
	
	
	
	
	
	function add_invoice($fields = array())
    {
        //return $this->db->insert_batch('invoice', $fields);
        
        $this->db->set($fields);
        $this->db->insert('invoice'); 

        $invoice_id = $this->db->insert_id();

        return $invoice_id;
    }
    
    function checkinvoiceexistbydayserviceid($dayserviceid)
    {
//        $this->db->select('invoice_num')
//                ->from('invoice')
//                ->where('day_service_id',$dayserviceid)
//                ->limit(1);
//        $get_invoice_qry = $this->db->get();
//        return $get_invoice_qry->row();
        $query = $this->db->query("select * from invoice where find_in_set('$dayserviceid',day_service_id) <> 0");
        //echo $this->db->last_query();exit();
        return $query->row();
    }
    
    
    
    function get_invoice_num($invoicenum)
    {
        $this->db->select('*')
                ->from('invoice')
                ->where('invoice_num',$invoicenum)
                ->limit(1);
        $get_invoice_qry = $this->db->get();
        return $get_invoice_qry->row();
    }
    
//    function get_invoice($search_date=null)
//    {
//        $query = $this->db->query("SELECT m.maid_name,day_s.customer_name,day_s.customer_address,TIME_FORMAT(b.time_from,'%h:%i%p')as start_time,TIME_FORMAT(b.time_to,'%h:%i%p') AS end_time,day_s.total_fee,day_s.material_fee,cp.paid_amount
//                                    FROM day_services day_s
//                                    LEFT JOIN bookings b ON b.booking_id=day_s.booking_id
//                                    LEFT JOIN maids m ON m.maid_id=day_s.maid_id
//                                    LEFT JOIN customer_payments cp ON cp.day_service_id=day_s.day_service_id 
//                                    WHERE  day_s.service_status=2 AND day_s.service_date =". $this->db->escape($search_date) . "
//                                    ORDER BY day_s.day_service_id DESC"); 
// 
//        return $query->result();
//    }
    
    function get_invoice($search_date=null)
    {
        $this->db->select('i.*,c.customer_name,m.maid_name,DATE_FORMAT(b.time_from, "%h:%i %p") AS time_from, DATE_FORMAT(b.time_to, "%h:%i %p") AS time_to,ca.customer_address', FALSE)
                ->from('invoice i')
                ->join('customers c','i.customer_id = c.customer_id')
                ->join('customer_addresses ca','c.customer_id = ca.customer_id','left')
                ->join('bookings b','i.booking_id = b.booking_id')
                ->join('maids m','b.maid_id = m.maid_id','left')
				->where('b.booking_category', 'C')
                ->where('i.invoice_date',$search_date)
                ->limit(1);
        $get_invoice_qry = $this->db->get();
        
        return $get_invoice_qry->result();
    }
	function get_invoice_row($invoice_id)
    {
        $this->db->select('i.*', FALSE)
                ->from('invoice as i')
                ->where('i.invoice_id',$invoice_id);
        $query = $this->db->get();
        return $query->row();
    }
	function update_invoice_row($invoice_id,$data)
    {
        $this->db->where('invoice_id', $invoice_id);
		$this->db->update('invoice', $data);
    }
    
    function get_due_invoice($search_date=null)
    {
        $date = date('Y-m-d');
        $this->db->select('i.*,c.customer_name,m.maid_name,DATE_FORMAT(b.time_from, "%h:%i %p") AS time_from, DATE_FORMAT(b.time_to, "%h:%i %p") AS time_to,ca.customer_address', FALSE)
                ->from('invoice i')
                ->join('customers c','i.customer_id = c.customer_id')
                ->join('customer_addresses ca','c.customer_id = ca.customer_id','left')
                ->join('bookings b','i.booking_id = b.booking_id')
                ->join('maids m','b.maid_id = m.maid_id','left')
                //->where('i.invoice_date',$search_date)
				->where('b.booking_category', 'C')
                ->where('i.invoice_due_date <', $date)
                ->where('i.received_amount',0)
                ->limit(1);
        $get_invoice_qry = $this->db->get();
        return $get_invoice_qry->result();
    }
    
    function get_paid_invoice($search_date=null)
    {
        //$date = date('Y-m-d');
        $this->db->select('i.*,c.customer_name,m.maid_name,DATE_FORMAT(b.time_from, "%h:%i %p") AS time_from, DATE_FORMAT(b.time_to, "%h:%i %p") AS time_to,ca.customer_address', FALSE)
                ->from('invoice i')
                ->join('customers c','i.customer_id = c.customer_id')
                ->join('customer_addresses ca','c.customer_id = ca.customer_id','left')
                ->join('bookings b','i.booking_id = b.booking_id')
                ->join('maids m','b.maid_id = m.maid_id','left')
                //->where('i.invoice_date',$search_date)
                //->where('i.invoice_due_date <', $date)
				->where('b.booking_category', 'C')
                ->where('i.received_amount !=',0)
                ->limit(1);
        $get_invoice_qry = $this->db->get();
        return $get_invoice_qry->result();
    }
	function get_invoice_line_items_by_invoice_id($invoice_id)
    {
        $this->db->select('ili.*', FALSE)
		->from('invoice_line_items as ili')
		->where('ili.invoice_id',$invoice_id)
		->where('ili.line_status',1);
        $query = $this->db->get();
        return $query->result();
    }
	function get_bookings_by_customer_and_between_dates($customer_id,$start_date,$end_date){
        $this->db->select("b.booking_id,b.reference_id,b.maid_id,b.time_from,b.time_to,b.service_week_day,b.service_start_date,b.service_end,b.service_end_date,b.service_actual_end_date,b.booking_type,b.booking_status,
        (TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from))/3600 as hours,b.price_per_hr,b.discount_price_per_hr,b.net_service_cost,b.discount,b.service_charge,b.vat_charge,b.total_amount,b.cleaning_material,b.cleaning_material_fee", false)
            ->from('bookings as b')
            ->where("b.booking_status", 1)
            ->where("b.customer_id", $customer_id)
            ->where("(b.service_start_date BETWEEN '" . $start_date . "' AND '" . $end_date . "' OR b.service_actual_end_date BETWEEN '" . $start_date . "' AND '" . $end_date . "' OR (b.service_end = 0 AND b.service_start_date < '" . $start_date . "') OR (b.service_start_date <= '" . $start_date . "' AND b.service_actual_end_date >= '" . $end_date . "'))", null, false);
        $query = $this->db->get();
        return $query->result();
    }
	function insert_monthly_advanced_invoice_main($data = array()){
		// insert to main table
		$this->db->set($data);
        $this->db->insert('invoices_monthly_advanced'); 
        $id = $this->db->insert_id();
        return $id;
	}
	function insert_monthly_advanced_invoice_line($data = array()){
		// insert to main table
		$this->db->set($data);
        $this->db->insert('invoices_monthly_advanced_line_items'); 
        $id = $this->db->insert_id();
        return $id;
	}
	function update_monthly_advanced_invoice_main($where = array(),$data = array()){
		// update main table
		$this->db->where($where);
		//$this->db->update('invoices_monthly_advanced', $data);
		foreach($data as $field => $value){
			$this->db->set($field,$value, FALSE);
		}
		$this->db->update('invoices_monthly_advanced');
		return $this->db->affected_rows();

	}
	function get_monthly_advanced_invoice_main_row($where = array()){
		$this->db->select('i.*,c.email_address,c.customer_name as userName, ca.building, ca.customer_address, a.area_name')
		->from('invoices_monthly_advanced as i')
		->join('customers as c','c.customer_id = i.customer_id')
		->join('customer_addresses as ca','ca.customer_id = i.customer_id')
		->join('areas as a','a.area_id = ca.area_id')
		->where("ca.default_address", 1)
		->where($where);
		$query = $this->db->get();
        return $query->row();
	}
	function get_monthly_advanced_invoice_lines($where = array()){
		$this->db->select('i.*,b.time_from as booking_time_from,b.time_to as booking_time_to,st.service_type_name,b.booking_type,(TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from))/3600 as working_hours')
		->from('invoices_monthly_advanced_line_items as i')
		->join('bookings as b', 'i.booking_id = b.booking_id')
		->join('service_types as st', 'b.service_type_id = st.service_type_id')
		->where($where)
		->order_by('i.service_date','asc');
		$query = $this->db->get();
        return $query->result();
	}
	function get_customer_data_for_invoice($customer_id){
        $this->db->select('c.customer_name,c.mobile_number_1,c.email_address,ca.customer_address,ca.building,ca.unit_no,ca.street,a.area_name')
            ->from('customers as c')
			->join('customer_addresses as ca', 'c.customer_id = ca.customer_id')
			->join('areas as a', 'ca.area_id = a.area_id')
            ->where('c.customer_id', $customer_id)
			->where('ca.default_address',1);
        $query = $this->db->get();
        return $query->row();
    }
	function get_customer_monthly_advanced_invoices($date_from= NULL,$date_to= NULL,$customer_id,$inv_status){
        $this->db->select('i.*,c.email_address,c.customer_name as userName')
                ->from('invoices_monthly_advanced as i')
				->order_by('i.id','desc')
                ->join('customers as c','c.customer_id = i.customer_id');
		if($date_from != NULL && $date_to != NULL)
		{
			$this->db->where("i.for_year_month BETWEEN '$date_from' AND '$date_to'");
		}
		if($customer_id != NULL)
		{
			$this->db->where('i.customer_id', $customer_id);
		}
		if($inv_status != NULL)
		{
			$this->db->where('i.status', $inv_status);
		}
        $get_invoice_qry = $this->db->get();
        return $get_invoice_qry->result();
    }
	function get_booking_service_delete_by_date($booking_id,$service_date){
		$this->db->select('bdr.*')
                ->from('booking_delete_remarks as bdr')
				->where('bdr.booking_id',$booking_id)
				->where('bdr.service_date',$service_date);
		$query = $this->db->get();
        return $query->row();
	}
	
	function update_invoice_nullify($invoice_id)
	{
		$stat_array = array();
		$stat_array['isNullified'] = 'Y';
		$this->db->where('invoice_id', $invoice_id);
		$this->db->update('invoice', $stat_array);
		return $this->db->affected_rows();
	}
}