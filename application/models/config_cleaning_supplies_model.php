<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Config_cleaning_supplies_model extends CI_Model {
  public function __construct() {
      parent::__construct();
      $this->load->database(); // Load the database connection
  }

  public function get_all_supplies() {
      $this->db->select('id, name, amount,type');
      return $this->db->get('config_cleaning_supplies')->result();
  }

  public function get_all_supplies_by_type() {
    $this->db->select('id, name, amount, type');
    $this->db->from('config_cleaning_supplies');
    $this->db->group_by('type');
    return $this->db->get()->result();
  }
}