<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Driver_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_tablets_location_by_date($date)
    {
        $date = $date ?: date('d-m-Y');
        $this->db->select('tl.tablet_id,MAX(tl.added) as added,tl.latitude, tl.longitude,t.tablet_driver_name,z.zone_name')
            ->from('tablet_locations as tl')
            ->join('tablets as t', 'tl.tablet_id = t.tablet_id', 'left')
            ->join('zones as z', 't.zone_id = z.zone_id', 'left')
            ->where('DATE(tl.added)', $date)
            ->where('t.tablet_status', 1)
            ->where('z.zone_status', 1)
            ->group_by('tl.tablet_id')
            ->order_by('tl.added', 'asc');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function get_current_tablet_details_by_service($booking_id, $service_date)
    {
        // for getting driver / tablet details for a specific service
        // if its a transferred tablet it's data will taken
        $this->db->select("b.booking_id,'" . $service_date . "' as service_date,
        b.tabletid as booking_tabletid,
        t_b.tablet_driver_name as booking_driver,
        t_btt.tablet_driver_name as transferred_driver,
        btt.transfering_to_tablet as transferred_tabletid", false)
            ->from('bookings as b')
            ->join('booking_transfers_tablet as btt', "b.booking_id = btt.booking_id AND btt.service_date = " . $this->db->escape($service_date), 'left')
            ->join('tablets as t_b', "b.tabletid = t_b.tablet_id", 'left')
            ->join('tablets as t_btt', "btt.transfering_to_tablet = t_btt.tablet_id", 'left')
            ->where('b.booking_id', $booking_id);
        $query = $this->db->get();
        return $query->row_array();
    }
}
