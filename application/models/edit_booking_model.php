<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Edit_booking_model Class
 * 
 * @author Vishnu
 * @package DHK
 * @version Version 1.0
 */
class Edit_booking_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }
	
	function get_all_customers()
	{
        $this->db->select('c.customer_id,CONCAT(c.customer_name," (",c.payment_type,") ")as customer_name',false)
        ->from('customers as c')
        ->where('c.customer_status',1);
		$query = $this->db->get();
        return $query->result();
	}
	
	function get_bookings_by_servicedate($rowperpage = NULL, $start = NULL, $draw = NULL, $service_date=NULL, $customer_id=NULL, $maid_id=NULL)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
		if(isset($deletes))
		{
			foreach ($deletes as $delete) {
				$deleted_bookings[] = $delete->booking_id;
			}
		}

        $this->db->select("
        b.booking_id,
        b.tabletid,
        b.pay_by,
        b.justmop_reference,
        b.customer_id,
        b.customer_address_id,
        b.maid_id,
        b.service_type_id,
        b.service_start_date,
        b.service_week_day,
        b.is_locked,
        b.cleaning_material,
        b.cleaning_material_fee,
        DATE_FORMAT(b.time_from, '%H:%i') AS time_from,
        DATE_FORMAT(b.time_to, '%H:%i') AS time_to,
        ROUND(TIMESTAMPDIFF(MINUTE,b.time_from,b.time_to)/60,2) AS working_hours,
        b.booking_type, b.service_end,
        b.service_end_date, b.service_actual_end_date,
        b.booking_note, b.pending_amount,
        b.discount,
        b.service_charge,
        b.vat_charge,
        b.net_service_cost,
        b.total_amount,
        b.booking_status,
        b.price_per_hr,
        b.discount_price_per_hr,
        b.supervisor_selected,
        b.supervisor_charge_total,
        b.crew_in,
        c.customer_name,
        c.price_hourly,
        c.balance,
        c.signed,
        c.customer_nick_name,
        c.mobile_number_1,
        c.key_given,
        c.payment_type,
        c.payment_mode,
        ca.longitude,
        ca.latitude,
        c.customer_source,
        z.zone_id,
        z.zone_name,
        a.area_name,
        ca.customer_address,
        ca.building,
        m.maid_name,
        m.maid_nationality,
        m.maid_mobile_1,
        m.maid_photo_file,
        u.user_fullname,
        ca.building,
        ca.unit_no,
        ca.street,
        st.service_type_name", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->join('service_types st', 'b.service_type_id = st.service_type_id', 'left')
            ->where('a.area_status', 1)
            ->where('z.zone_status', 1)
            ->where('b.booking_status', 1)
            ->where('b.booking_category', 'C')
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
			->order_by('b.time_from');

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }
		
		if ($customer_id != NULL) {
            $this->db->where('b.customer_id', $customer_id);
        }
		
		if ($maid_id != NULL) {
            $this->db->where('b.maid_id', $maid_id);
        }

        $get_schedule_by_date_qry = $this->db->get();
        $records = $get_schedule_by_date_qry->result();
		$data = array();
        $i = 1;

        foreach ($records as $record) {

            $view = '<a class="btn blue-btn" onclick="newPopup('.$record->booking_id.')"  title="Edit Booking">Edit</a>';
            $delete = '<a class="btn blue-btn" style="margin-left: 10px;" onclick="deleteBooking('.$record->booking_id.')"  title="Delete Booking">Delete</a>';
            
            $data[] = array(
                'slno' => '<center>'.($i + $start).'<center>',
                'bookingid' => $record->booking_id,
                'servicedate' => $service_date,
                'customer' => $record->customer_name,
                'maid' => $record->maid_name,
                'time' => $record->time_from . '-' . $record->time_to,
                'amount' => $record->service_charge,
                'vat' => $record->vat_charge,
                'net' => $record->total_amount,
                'action' => $view.$delete,
            );
            $i++;
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => count($records),
            "lastcall" => isset($records)?$records[0]:[],
            "iTotalDisplayRecords" => count($records),
            "aaData" => $data
        );

        return $response;
    }
	
	function get_booking_deletes_by_date($service_date)
    {
        $this->db->select('booking_id')
            ->from('booking_deletes');
		$this->db->where("service_date", $service_date);


        $get_booking_deletes_by_date_qry = $this->db->get();
        return $get_booking_deletes_by_date_qry->result();
    }
	
	function get_dailyactivities_by_servicedate($rowperpage = NULL, $start = NULL, $draw = NULL, $service_date=NULL, $customer_id=NULL, $maid_id=NULL)
    {
        $this->db->select("ds.day_service_id,ds.booking_id, ds.service_status, c.customer_name, m.maid_name, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.total_amount, ds.invoice_status, ds.serv_invoice_id", FALSE)
            ->from('day_services ds')
			->join('bookings b','b.booking_id=ds.booking_id')
			->join('customers c', 'ds.customer_id = c.customer_id')
			->join('maids m', 'ds.maid_id = m.maid_id');
        $this->db->where("service_date", $service_date);
		//$this->db->where('ds.service_status !=', 3);
		if ($customer_id != NULL) {
            $this->db->where('ds.customer_id', $customer_id);
        }
		
		if ($maid_id != NULL) {
            $this->db->where('ds.maid_id', $maid_id);
        }

        $get_booking_dayservices_by_date_qry = $this->db->get();
        $records = $get_booking_dayservices_by_date_qry->result();
		$data = array();
        $i = 1;
		if(isset($records))
		{
        foreach ($records as $record) {

            if($record->invoice_status == 1)
			{
				$view = '<a class="btn blue-btn" onclick="viewServiceInvoice('.$record->serv_invoice_id.')"  title="View Invoice"><i class="btn-icon-only fa fa-eye"> </i></a>';
            } else {
				$view = '';
			}
			if($record->service_status == 3)
			{
				$delete = '';
			} else {
				$delete = '<a class="btn blue-btn" style="margin-left: 10px;" onclick="deleteService('.$record->day_service_id.')"  title="Cancel Service"><i class="btn-icon-only fa fa-trash"> </i></a>';
			}
			
			if($record->service_status == 0)
			{
				$status = "Pending";
			} else if($record->service_status == 1)
			{
				$status = "Ongoing";
			}  else if($record->service_status == 2)
			{
				$status = "Finished";
			}  else if($record->service_status == 3)
			{
				$status = "Cancelled";
			}
            $data[] = array(
                'slno' => '<center>'.($i + $start).'<center>',
                'bookingid' => $record->booking_id,
                'serviceid' => $record->day_service_id,
                'servicedate' => $service_date,
                'customer' => $record->customer_name,
                'maid' => $record->maid_name,
                'time' => $record->time_from . '-' . $record->time_to,
                'amount' => $record->total_amount,
                'status' => $status,
                'action' => $view.$delete,
            );
            $i++;
        }
		}

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => count($records),
            "lastcall" => isset($records)?$records[0]:[],
            "iTotalDisplayRecords" => count($records),
            "aaData" => $data
        );

        return $response;
    }
	
	function get_all_staff()
	{
		$this->db->select('m.maid_id, m.maid_name')
                ->from('maids m');
		$this->db->where('m.maid_status', 1);
        $get_maids_qry = $this->db->get();
        return $get_maids_qry->result();
	}
	
	function get_invoices_by_serviceid($rowperpage = NULL, $start = NULL, $draw = NULL, $invoiceid=NULL)
	{
		$this->db->select("i.invoice_id, i.invoice_num, i.invoice_status, i.invoice_date, i.invoice_due_date, i.invoice_paid_status, i.invoice_total_amount, i.invoice_tax_amount, i.invoice_net_amount, i.received_amount, i.balance_amount, c.customer_name", FALSE)
            ->from('invoice i')
			->join('customers c', 'i.customer_id = c.customer_id');
        // $this->db->where("ilt.day_service_id", $serviceid);
        $this->db->where("i.invoice_id", $invoiceid);
		$this->db->where('i.invoice_status !=', 2);
		
        $get_booking_dayservices_by_date_qry = $this->db->get();
        $records = $get_booking_dayservices_by_date_qry->result();
		$data = array();
        $i = 1;
		if(isset($records))
		{
        foreach ($records as $record) {

            $view = '<a class="btn blue-btn" onclick="editServiceInvoice('.$record->invoice_id.')"  title="Edit Invoice"><i class="btn-icon-only fa fa-pencil"> </i></a>';
            $delete = '<a class="btn blue-btn" style="margin-left: 10px;" onclick="deleteServiceInvoice('.$record->invoice_id.')"  title="Delete Invoice"><i class="btn-icon-only fa fa-trash"> </i></a>';
            
			// $edit = '<a class="n-btn-icon purple-btn" href="' . base_url() . 'customer/edit/' . $record->customer_id . '" title="Edit Customer"><i class="btn-icon-only icon-pencil"> </i></a>';
			if($record->invoice_status == 0)
			{
				$status = "Draft";
				// $pay = "";
			} else if($record->invoice_status == 1 && $record->invoice_paid_status == 0)
			{
				$status = "Open";
				// $pay = "";
			} else if($record->invoice_status == 1 && $record->invoice_paid_status == 2)
			{
				$status = "Partially Paid";
				// $pay = '<a style="margin-top: 5px;" class="btn blue-btn" onclick="viewInvoicePayment('.$record->invoice_id.')"  title="View Invoice">View Payment</a>';
			} else if($record->invoice_status == 3 && $record->invoice_paid_status == 1)
			{
				$status = "Paid";
				// $pay = '<a style="margin-top: 5px;" class="btn blue-btn" onclick="viewInvoicePayment('.$record->invoice_id.')"  title="View Invoice">View Payment</a>';
			}
            $data[] = array(
                'slno' => '<center>'.($i + $start).'<center>',
                'invnum' => $record->invoice_num,
                'customer' => $record->customer_name,
                'invoicedate' => $record->invoice_date,
                'duedate' => $record->invoice_due_date,
                'amount' => $record->invoice_total_amount,
                'vat' => $record->invoice_tax_amount,
                'totalamount' => $record->invoice_net_amount,
                'paidamt' => $record->received_amount,
                'balanceamount' => $record->balance_amount,
                'status' => $status,
                'action' => $view.$delete,
            );
            $i++;
        }
		}

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => count($records),
            "lastcall" => isset($records)?$records[0]:[],
            "iTotalDisplayRecords" => count($records),
            "aaData" => $data
        );

        return $response;
	}
	
	function get_booking_detail($booking_id)
	{
		$this->db->select("c.customer_name, c.mobile_number_1, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.price_per_hr, b.discount_price_per_hr, b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.booking_type, b.service_end, b.service_actual_end_date, b.booking_status, b.supervisor_selected, b.supervisor_charge_hourly, b.is_free, b.supervisor_charge_total, b.booking_note, b.discount, b.net_service_cost, b.service_charge, b.vat_charge, b.total_amount, m.maid_name, b.service_end_date, b.cleaning_material,b.cleaning_material_fee, b.plan_based_supplies, b.plan_based_supplies_amount, b.custom_supplies, b.custom_supplies_amount, b.booked_from, b.pay_by, b.is_locked, b.booked_by, b.time_from as oldtimefrom, b.time_to as oldtimeto", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id', 'left')
            ->join('zones z', 'a.zone_id = z.zone_id', 'left')
            ->join('maids m', 'b.maid_id = m.maid_id', 'left')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->where('b.booking_id', $booking_id)
            ->where('b.booking_category', 'C')
            ->limit(1);

        $get_bookings_by_id_qry = $this->db->get();
        return $get_bookings_by_id_qry->row();
	}
	
	function get_schedule_by_date_new($service_date, $maid_id = NULL)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("
        b.booking_id,
        b.customer_id,
        b.customer_address_id,
        b.maid_id,
        b.service_type_id,
        b.service_start_date,
        b.service_week_day,
        b.cleaning_material,
        b.cleaning_material_fee,
        DATE_FORMAT(b.time_from, '%H:%i') AS time_from,
        DATE_FORMAT(b.time_to, '%H:%i') AS time_to,
        ROUND(TIMESTAMPDIFF(MINUTE,b.time_from,b.time_to)/60,2) AS working_hours,
        b.booking_type, b.service_end,
        b.service_end_date, b.service_actual_end_date,
        b.booking_note, b.pending_amount,
        b.discount,
        b.service_charge,
        b.vat_charge,
        b.net_service_cost,
        b.total_amount,
        b.booking_status,
        b.price_per_hr,
        b.discount_price_per_hr,
        b.supervisor_selected,
        b.supervisor_charge_total", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->join('service_types st', 'b.service_type_id = st.service_type_id', 'left')
            ->where('a.area_status', 1)
            ->where('z.zone_status', 1)
            ->where('b.booking_status', 1)
            ->where('b.booking_category', 'C')
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
            ->order_by('b.time_from');
		if($maid_id != NULL)
		{
			$this->db->where('b.maid_id', $maid_id);
		}

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();

        $bookings = $get_schedule_by_date_qry->result();

        return $bookings;
    }
	
	function update_booking($booking_id, $fields = array(), $action)
    {
        $this->db->where('booking_id', $booking_id);
        $this->db->update('bookings', $fields);
        $affected = $this->db->affected_rows();
		$this->add_activity($booking_id, $action);
        return $affected;
    }
	
	public function insert_batch_booking_cleaning_supply($data,$booking_id)
	{
		$this->db->where('booking_id', $booking_id);
        $deleteval = $this->db->delete('booking_cleaning_supplies');
		if(!empty($deleteval))
		{
			$this->db->insert_batch('booking_cleaning_supplies', $data);
			return $this->db->insert_id();
		} else {
			return $deleteval;
		}
	}
	
	public function check_day_service_booking_new($booking_id, $service_date)
    {
        $this->db->select("day_service_id,booking_id", FALSE)
            ->from('day_services');
        $this->db->where('booking_id', $booking_id);
        $this->db->where('service_date', $service_date);
        $this->db->where('service_status !=', 3);
        $get_results_query = $this->db->get();
        return $get_results_query->row();
    }
	
	function update_day_service_booking($day_service_id, $fields)
    {
        $this->db->where('day_service_id', $day_service_id);
        $this->db->update('day_services', $fields);
        // echo $this->db->last_query(); 
    }
	
	function add_activity($booking_id, $action_type)
    {
        $booking = $this->get_booking_by_id($booking_id);

        if (!empty($booking)) {
            $data = array();
            $data['added_user'] = user_authenticate();
            $data['booking_type'] = $booking->booking_type == 'OD' ? 'One Day' : 'Every Week';
            $data['shift'] = $booking->time_from . '-' . $booking->time_to;
            $data['action_type'] = $action_type;
            $data['addeddate'] = date('Y-m-d H:i:s');
            //$data['action_content'] = $booking->maid_name . ' to ' . $booking->customer_name . ', for ' . $booking->service_start_date . ' as start date';
            if ($booking->is_admin == "Y") {
                if ($action_type == "Booking_add") {
                    $data['action_content'] = "A new booking for customer: " . $booking->customer_name . " is created.";
                } else if ($action_type == "Booking_update") {
                    $data['action_content'] = "A booking for customer: " . $booking->customer_name . " is updated.";
                } else if ($action_type == "Delete") {
					$data['action_content'] = "A booking for customer: " . $booking->customer_name . " is deleted.";
				}
            } else {
                if ($action_type == "Booking_add") {
                    $data['action_content'] = "A new booking for customer: " . $booking->customer_name . " is created.";
                } else if ($action_type == "Booking_update") {
                    $data['action_content'] = "A booking for customer: " . $booking->customer_name . " is updated.";
                } else if ($action_type == "Delete") {
					$data['action_content'] = "A booking for customer: " . $booking->customer_name . " is deleted.";
				}
            }
            $this->db->set($data);
            $this->db->insert('user_activity');

            return $this->db->insert_id();
        }
    }
	
	function get_booking_by_id($booking_id)
    {
        $this->db->select("b.time_from, b.time_to, z.zone_id, b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.justmop_reference, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_actual_end_date, b.booking_status,b.supervisor_selected, b.supervisor_charge_hourly, b.supervisor_charge_total, b.booking_note, b.price_per_hr, b.discount_price_per_hr, b.discount, b.net_service_cost, b.service_charge, b.vat_charge, b.total_amount, b.pending_amount, c.customer_name,c.mobile_number_1, c.email_address, c.payment_type, c.price_hourly, c.price_extra, c.price_weekend, ca.customer_address, ca.building, ca.unit_no, ca.street, a.area_name,ca.area_id, b.is_locked, b.booked_by, m.maid_name, b.service_end_date, b.cleaning_material,b.cleaning_material_fee,b.plan_based_supplies,b.plan_based_supplies_amount,b.custom_supplies,b.custom_supplies_amount,b.booked_from,b.pay_by,u.is_admin,c.balance,c.signed,b.month_durations,b.tabletid,DATE_FORMAT(b.time_from, '%h:%i %p') AS newtime_from, DATE_FORMAT(b.time_to, '%h:%i %p') AS newtime_to,b.pay_by_cash_charge,b.payment_type as book_payment_type, b.interior_window_clean, b.fridge_cleaning, b.ironing_services, b.oven_cleaning, b.crew_in", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id', 'left')
            ->join('zones z', 'a.zone_id = z.zone_id', 'left')
            ->join('maids m', 'b.maid_id = m.maid_id', 'left')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->where('b.booking_id', $booking_id)
            ->where('b.booking_category', 'C')
            ->limit(1);

        $get_bookings_by_id_qry = $this->db->get();

        return $get_bookings_by_id_qry->row();
    }
	
	function add_booking_delete($booking_id,$fields = array())
    {
        $fields['added_datetime'] = isset($fields['added_datetime']) ? $fields['added_datetime'] : date('Y-m-d H:i:s');

        $this->db->set($fields);
        $this->db->insert('booking_deletes');
        $insert_id = $this->db->insert_id();

        $action = "Booking_update";
		$this->add_activity($booking_id, $action);
        return $insert_id;
    }
	
	function add_booking($fields = array())
    {
        $this->db->set($fields);
        $this->db->insert('bookings');

        $booking_id = $this->db->insert_id();
        return $booking_id;
    }
	
	function get_payments_by_invoiceid($rowperpage = NULL, $start = NULL, $draw = NULL, $invoiceid=NULL)
	{
		$this->db->select("ipm.inv_map_id, ipm.paymentId, ipm.invoiceId, ipm.inv_customer_id, ipm.inv_reference, cp.paid_datetime, ipm.payment_amount, c.customer_name", FALSE)
            ->from('InvoicePaymentMapping ipm')
			->join('customer_payments cp', 'ipm.paymentId = cp.payment_id')
			->join('customers c', 'ipm.inv_customer_id = c.customer_id');
        // $this->db->where("ilt.day_service_id", $serviceid);
        $this->db->where("ipm.invoiceId", $invoiceid);
		
        $get_booking_dayservices_by_date_qry = $this->db->get();
		// echo $this->db->last_query();exit();
        $records = $get_booking_dayservices_by_date_qry->result();
		$data = array();
        $i = 1;
		if(isset($records))
		{
        foreach ($records as $record) {

            $view = '<a class="btn blue-btn" onclick="deleteInvoicePayments('.$record->invoiceId.','.$record->inv_map_id.','.$record->paymentId.','.$record->inv_customer_id.')"  title="Delete Invoice"><i class="btn-icon-only fa fa-trash"> </i></a>';
            
			// $edit = '<a class="n-btn-icon purple-btn" href="' . base_url() . 'customer/edit/' . $record->customer_id . '" title="Edit Customer"><i class="btn-icon-only icon-pencil"> </i></a>';
			
            $data[] = array(
                'slno' => '<center>'.($i + $start).'<center>',
                'invnum' => $record->inv_reference,
                'customer' => $record->customer_name,
                'paymentdate' => date("Y-m-d", strtotime($record->paid_datetime)),
                'amount' => $record->payment_amount,
                'action' => $view,
            );
            $i++;
        }
		}

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => count($records),
            "lastcall" => isset($records)?$records[0]:[],
            "iTotalDisplayRecords" => count($records),
            "aaData" => $data
        );

        return $response;
	}
	
	function get_invoice_detailbyid($invoice_id)
	{
		$this->db->select('l.day_service_id,c.total_invoice_amount,c.balance,c.total_paid_amount,c.trnnumber,c.email_address,i.invoice_id,i.invoice_num,c.customer_name,i.customer_id,i.bill_address,i.invoice_status, i.invoice_paid_status, i.added, ds.service_date, i.invoice_date, i.invoice_due_date, i.invoice_total_amount, i.invoice_tax_amount, i.invoice_net_amount, i.balance_amount, i.received_amount, m.maid_name, l.service_from_time, l.service_to_time, l.line_bill_address, l.description, l.line_amount, l.line_vat_amount, l.line_net_amount,l.invoice_line_id,l.service_hrs,l.monthly_product_service, u.user_fullname,st.service_type_name,c.customer_id as custid,l.inv_unit_price,b.total_amount')
                ->from('invoice i')
				->join('invoice_line_items l','i.invoice_id = l.invoice_id','INNER')
				->join('day_services ds','ds.day_service_id = l.day_service_id','LEFT')
				->join('maids m','ds.maid_id = m.maid_id','LEFT')
				->join('bookings b','b.booking_id = ds.booking_id','LEFT')
				->join('service_types st','st.service_type_id = b.service_type_id','LEFT')
				->join('customers c','c.customer_id = i.customer_id')
				->join('users u','i.invoice_added_by = u.user_id','LEFT')
				->where('i.invoice_id',$invoice_id)
				->where('l.line_status',1)
				->order_by('ds.service_date','asc');
		$get_invoice_qry = $this->db->get();
        return $get_invoice_qry->result();
	}
	
	function update_invoicelineitems($invoiceid,$fields = array())
	{
		$this->db->where('invoice_line_id', $invoiceid);
		$this->db->update('invoice_line_items', $fields);
		return $this->db->affected_rows();
	}
	
	function get_invoice_line_items_by_invoice_id($invoice_id)
    {
        $this->db->select('ili.*', FALSE)
				->from('invoice_line_items as ili')
				->where('ili.invoice_id',$invoice_id)
				->where('ili.line_status',1);
        $query = $this->db->get();
        return $query->result();
    }
	
	function update_invoicepay_detail($invoicelineid,$fields = array())
	{
		$this->db->where('invoice_id', $invoicelineid);
		$this->db->update('invoice', $fields);
		return $this->db->affected_rows();
	}
	
	function get_customer_detail($cust_id)
	{
		$this->db->select('c.total_invoice_amount,c.balance,c.last_invoice_date,c.total_paid_amount')
                ->from('customers c')
				->where('c.customer_id',$cust_id);
		$get_invoice_qry = $this->db->get();
        return $get_invoice_qry->row();
	}
	
	function update_customer_detail($custid,$fields = array())
	{
		$this->db->where('customer_id', $custid);
		$this->db->update('customers', $fields);
		return $this->db->affected_rows();
	}
	
	function delete_invoicelineitems($lineid,$serviceid)
	{
		$cust_array = array();
		$cust_array['invoice_status'] = 0;
		$this->db->where('day_service_id', $serviceid);
		$this->db->update('day_services', $cust_array);
		if($this->db->affected_rows())
		{
			$this->db->where('invoice_line_id', $lineid);
			$deleteval = $this->db->delete('invoice_line_items');
			return $deleteval;
		}
	}
	
	function get_invoicepayments_by_id($invoiceid)
	{
		$this->db->select("ipm.inv_map_id, ipm.inv_customer_id, ipm.paymentId, ipm.invoiceId, ipm.inv_reference, ipm.payment_amount", FALSE)
            ->from('InvoicePaymentMapping ipm');
        $this->db->where("ipm.invoiceId", $invoiceid);
        $this->db->order_by('ipm.inv_map_id','desc');
        $get_booking_dayservices_by_date_qry = $this->db->get();
		// echo $this->db->last_query();exit();
        $records = $get_booking_dayservices_by_date_qry->result();
        return $records;
	}
	
	function delete_invoicepayments_by_id($inv_map_id,$paidamount,$paymentid)
	{
		$this->db->where('inv_map_id', $inv_map_id);
        $deleteval = $this->db->delete('InvoicePaymentMapping');
		if(!empty($deleteval))
		{
			$this->db->select('allocated_amount,balance_amount,paid_amount')
					->from('customer_payments')
					->where('payment_id',$paymentid);
			$get_data_qry = $this->db->get();
			$records = $get_data_qry->row();
			
			$data = array();
            $data['allocated_amount'] = ($records->allocated_amount - $paidamount);
            $data['balance_amount'] = ($records->balance_amount + $paidamount);
			if($records->paid_amount == $data['balance_amount'])
			{
				$data['payment_status'] = 0;
			} else if($data['balance_amount'] > 0 && ($data['balance_amount'] < $records->paid_amount))
			{
				$data['payment_status'] = 2;
			} else if($data['balance_amount'] == 0){
				$data['payment_status'] = 1;
			}
			$data['payment_qb_resync'] = 2;
			$this->db->where('payment_id', $paymentid);
			$this->db->update('customer_payments', $data);
			return $this->db->affected_rows();
		} else {
			return $deleteval;
		}
	}
	
	function update_invoicepayments_by_id($inv_map_id,$paidamount,$paymentid,$updteamt)
	{
		$dataa = array();
		$dataa['payment_amount'] = ($paidamount - $updteamt);
		$this->db->where('inv_map_id', $inv_map_id);
		$this->db->update('InvoicePaymentMapping', $dataa);
		$updateval = $this->db->affected_rows();
		if(!empty($updateval))
		{
			$this->db->select('allocated_amount,balance_amount,paid_amount')
					->from('customer_payments')
					->where('payment_id',$paymentid);
			$get_data_qry = $this->db->get();
			$records = $get_data_qry->row();
			
			$data = array();
            $data['allocated_amount'] = ($records->allocated_amount - $updteamt);
            $data['balance_amount'] = ($records->balance_amount + $updteamt);
			if($records->paid_amount == $data['balance_amount'])
			{
				$data['payment_status'] = 0;
			} else if($data['balance_amount'] > 0 && $data['balance_amount'] < $records->paid_amount)
			{
				$data['payment_status'] = 2;
			} else if($data['balance_amount'] == 0){
				$data['payment_status'] = 1;
			}
			$data['payment_qb_resync'] = 2;
			$this->db->where('payment_id', $paymentid);
			$this->db->update('customer_payments', $data);
			return $this->db->affected_rows();
		} else {
			return $updateval;
		}
	}
	
	public function get_booking_service_detail($booking_id, $service_date)
    {
        $this->db->select("day_service_id,booking_id,invoice_status", FALSE)
            ->from('day_services');
        $this->db->where('booking_id', $booking_id);
        $this->db->where('service_date', $service_date);
        $this->db->where('service_status !=', 3);
        $get_results_query = $this->db->get();
        return $get_results_query->row();
    }
	
	function get_invoice_detail_by_invoice_id($invoice_id,$inv_map_id)
	{
		$this->db->select('ipm.payment_amount,i.received_amount,i.invoice_net_amount,c.total_paid_amount,c.total_invoice_amount')
				->from('InvoicePaymentMapping ipm')
				->join('invoice i','i.invoice_id = ipm.invoiceId')
				->join('customers c','c.customer_id = ipm.inv_customer_id')
				->where('ipm.inv_map_id',$inv_map_id)
				->where('ipm.invoiceId',$invoice_id);
		$get_invoice_qry = $this->db->get();
        return $get_invoice_qry->row();
	}
	
	function get_invoice_by_invoice_id($invoice_id)
	{
		$this->db->select("i.invoice_id, i.invoice_num, i.invoice_status, i.invoice_date, i.invoice_due_date, i.invoice_paid_status, i.invoice_total_amount, i.invoice_tax_amount, i.invoice_net_amount, i.received_amount, i.balance_amount, i.customer_id, c.balance, c.total_invoice_amount, c.total_paid_amount", FALSE)
            ->from('invoice i')
			->join('customers c','c.customer_id = i.customer_id');
        $this->db->where("i.invoice_id", $invoice_id);
		$this->db->where('i.invoice_status !=', 2);
		
        $get_booking_dayservices_by_date_qry = $this->db->get();
		// echo $this->db->last_query();exit();
        return $get_booking_dayservices_by_date_qry->row();
	}
	
	function update_invoice_row($invoice_id,$data)
    {
        $this->db->where('invoice_id', $invoice_id);
		$this->db->update('invoice', $data);
		return $this->db->affected_rows();
    }
	
	function update_day_services_where($where,$data)
	{
		$this->db->where($where);
		$this->db->update('day_services', $data);
		return $this->db->affected_rows();
	}
	
	public function get_service_detail($serviceid)
    {
        $this->db->select("day_service_id,booking_id,invoice_status", FALSE)
            ->from('day_services');
        $this->db->where('day_service_id', $serviceid);
        $get_results_query = $this->db->get();
        return $get_results_query->row();
    }
	
	function update_day_services($serviceid,$data)
    {
        $this->db->where('day_service_id', $serviceid);
		$this->db->update('day_services', $data);
		return $this->db->affected_rows();
    }
	
	function add_booking_delete_new($fields = array())
    {
        $fields['added_datetime'] = isset($fields['added_datetime']) ? $fields['added_datetime'] : date('Y-m-d H:i:s');

        $this->db->set($fields);
        $this->db->insert('booking_deletes');
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
	
	function add_booking_remarks($fields = array())
    {
        $fields['deleted_date_time'] = isset($fields['deleted_date_time']) ? $fields['deleted_date_time'] : date('Y-m-d H:i:s');

        //$fields['service_date'] = date('Y-m-d');
        $this->db->set($fields);
        $this->db->insert('booking_delete_remarks');
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }
}